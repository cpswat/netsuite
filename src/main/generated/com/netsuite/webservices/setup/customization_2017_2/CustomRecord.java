
/**
 * CustomRecord.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.setup.customization_2017_2;
            

            /**
            *  CustomRecord bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class CustomRecord extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = CustomRecord
                Namespace URI = urn:customization_2017_2.setup.webservices.netsuite.com
                Namespace Prefix = ns33
                */
            

                        /**
                        * field for CustomRecordId
                        */

                        
                                    protected java.lang.String localCustomRecordId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomRecordIdTracker = false ;

                           public boolean isCustomRecordIdSpecified(){
                               return localCustomRecordIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCustomRecordId(){
                               return localCustomRecordId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomRecordId
                               */
                               public void setCustomRecordId(java.lang.String param){
                            localCustomRecordIdTracker = param != null;
                                   
                                            this.localCustomRecordId=param;
                                    

                               }
                            

                        /**
                        * field for CustomForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFormTracker = false ;

                           public boolean isCustomFormSpecified(){
                               return localCustomFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomForm(){
                               return localCustomForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomForm
                               */
                               public void setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomFormTracker = param != null;
                                   
                                            this.localCustomForm=param;
                                    

                               }
                            

                        /**
                        * field for IsInactive
                        */

                        
                                    protected boolean localIsInactive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsInactiveTracker = false ;

                           public boolean isIsInactiveSpecified(){
                               return localIsInactiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsInactive(){
                               return localIsInactive;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsInactive
                               */
                               public void setIsInactive(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsInactiveTracker =
                                       true;
                                   
                                            this.localIsInactive=param;
                                    

                               }
                            

                        /**
                        * field for Parent
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localParent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParentTracker = false ;

                           public boolean isParentSpecified(){
                               return localParentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getParent(){
                               return localParent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Parent
                               */
                               public void setParent(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localParentTracker = param != null;
                                   
                                            this.localParent=param;
                                    

                               }
                            

                        /**
                        * field for Disclaimer
                        */

                        
                                    protected java.lang.String localDisclaimer ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDisclaimerTracker = false ;

                           public boolean isDisclaimerSpecified(){
                               return localDisclaimerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDisclaimer(){
                               return localDisclaimer;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Disclaimer
                               */
                               public void setDisclaimer(java.lang.String param){
                            localDisclaimerTracker = param != null;
                                   
                                            this.localDisclaimer=param;
                                    

                               }
                            

                        /**
                        * field for Created
                        */

                        
                                    protected java.util.Calendar localCreated ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedTracker = false ;

                           public boolean isCreatedSpecified(){
                               return localCreatedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getCreated(){
                               return localCreated;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Created
                               */
                               public void setCreated(java.util.Calendar param){
                            localCreatedTracker = param != null;
                                   
                                            this.localCreated=param;
                                    

                               }
                            

                        /**
                        * field for LastModified
                        */

                        
                                    protected java.util.Calendar localLastModified ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedTracker = false ;

                           public boolean isLastModifiedSpecified(){
                               return localLastModifiedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastModified(){
                               return localLastModified;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModified
                               */
                               public void setLastModified(java.util.Calendar param){
                            localLastModifiedTracker = param != null;
                                   
                                            this.localLastModified=param;
                                    

                               }
                            

                        /**
                        * field for Name
                        */

                        
                                    protected java.lang.String localName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNameTracker = false ;

                           public boolean isNameSpecified(){
                               return localNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getName(){
                               return localName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Name
                               */
                               public void setName(java.lang.String param){
                            localNameTracker = param != null;
                                   
                                            this.localName=param;
                                    

                               }
                            

                        /**
                        * field for AutoName
                        */

                        
                                    protected boolean localAutoName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAutoNameTracker = false ;

                           public boolean isAutoNameSpecified(){
                               return localAutoNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getAutoName(){
                               return localAutoName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AutoName
                               */
                               public void setAutoName(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localAutoNameTracker =
                                       true;
                                   
                                            this.localAutoName=param;
                                    

                               }
                            

                        /**
                        * field for AltName
                        */

                        
                                    protected java.lang.String localAltName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAltNameTracker = false ;

                           public boolean isAltNameSpecified(){
                               return localAltNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getAltName(){
                               return localAltName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AltName
                               */
                               public void setAltName(java.lang.String param){
                            localAltNameTracker = param != null;
                                   
                                            this.localAltName=param;
                                    

                               }
                            

                        /**
                        * field for Owner
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localOwner ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOwnerTracker = false ;

                           public boolean isOwnerSpecified(){
                               return localOwnerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getOwner(){
                               return localOwner;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Owner
                               */
                               public void setOwner(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localOwnerTracker = param != null;
                                   
                                            this.localOwner=param;
                                    

                               }
                            

                        /**
                        * field for RecType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRecType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecTypeTracker = false ;

                           public boolean isRecTypeSpecified(){
                               return localRecTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRecType(){
                               return localRecType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecType
                               */
                               public void setRecType(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRecTypeTracker = param != null;
                                   
                                            this.localRecType=param;
                                    

                               }
                            

                        /**
                        * field for EnableNumbering
                        */

                        
                                    protected boolean localEnableNumbering ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEnableNumberingTracker = false ;

                           public boolean isEnableNumberingSpecified(){
                               return localEnableNumberingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getEnableNumbering(){
                               return localEnableNumbering;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EnableNumbering
                               */
                               public void setEnableNumbering(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localEnableNumberingTracker =
                                       true;
                                   
                                            this.localEnableNumbering=param;
                                    

                               }
                            

                        /**
                        * field for NumberingPrefix
                        */

                        
                                    protected java.lang.String localNumberingPrefix ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNumberingPrefixTracker = false ;

                           public boolean isNumberingPrefixSpecified(){
                               return localNumberingPrefixTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getNumberingPrefix(){
                               return localNumberingPrefix;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NumberingPrefix
                               */
                               public void setNumberingPrefix(java.lang.String param){
                            localNumberingPrefixTracker = param != null;
                                   
                                            this.localNumberingPrefix=param;
                                    

                               }
                            

                        /**
                        * field for NumberingSuffix
                        */

                        
                                    protected java.lang.String localNumberingSuffix ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNumberingSuffixTracker = false ;

                           public boolean isNumberingSuffixSpecified(){
                               return localNumberingSuffixTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getNumberingSuffix(){
                               return localNumberingSuffix;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NumberingSuffix
                               */
                               public void setNumberingSuffix(java.lang.String param){
                            localNumberingSuffixTracker = param != null;
                                   
                                            this.localNumberingSuffix=param;
                                    

                               }
                            

                        /**
                        * field for NumberingMinDigits
                        */

                        
                                    protected long localNumberingMinDigits ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNumberingMinDigitsTracker = false ;

                           public boolean isNumberingMinDigitsSpecified(){
                               return localNumberingMinDigitsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getNumberingMinDigits(){
                               return localNumberingMinDigits;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NumberingMinDigits
                               */
                               public void setNumberingMinDigits(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localNumberingMinDigitsTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localNumberingMinDigits=param;
                                    

                               }
                            

                        /**
                        * field for Description
                        */

                        
                                    protected java.lang.String localDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDescriptionTracker = false ;

                           public boolean isDescriptionSpecified(){
                               return localDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDescription(){
                               return localDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Description
                               */
                               public void setDescription(java.lang.String param){
                            localDescriptionTracker = param != null;
                                   
                                            this.localDescription=param;
                                    

                               }
                            

                        /**
                        * field for NumberingInit
                        */

                        
                                    protected long localNumberingInit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNumberingInitTracker = false ;

                           public boolean isNumberingInitSpecified(){
                               return localNumberingInitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getNumberingInit(){
                               return localNumberingInit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NumberingInit
                               */
                               public void setNumberingInit(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localNumberingInitTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localNumberingInit=param;
                                    

                               }
                            

                        /**
                        * field for NumberingCurrentNumber
                        */

                        
                                    protected long localNumberingCurrentNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNumberingCurrentNumberTracker = false ;

                           public boolean isNumberingCurrentNumberSpecified(){
                               return localNumberingCurrentNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getNumberingCurrentNumber(){
                               return localNumberingCurrentNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NumberingCurrentNumber
                               */
                               public void setNumberingCurrentNumber(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localNumberingCurrentNumberTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localNumberingCurrentNumber=param;
                                    

                               }
                            

                        /**
                        * field for AllowNumberingOverride
                        */

                        
                                    protected boolean localAllowNumberingOverride ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAllowNumberingOverrideTracker = false ;

                           public boolean isAllowNumberingOverrideSpecified(){
                               return localAllowNumberingOverrideTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getAllowNumberingOverride(){
                               return localAllowNumberingOverride;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AllowNumberingOverride
                               */
                               public void setAllowNumberingOverride(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localAllowNumberingOverrideTracker =
                                       true;
                                   
                                            this.localAllowNumberingOverride=param;
                                    

                               }
                            

                        /**
                        * field for IsNumberingUpdateable
                        */

                        
                                    protected boolean localIsNumberingUpdateable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsNumberingUpdateableTracker = false ;

                           public boolean isIsNumberingUpdateableSpecified(){
                               return localIsNumberingUpdateableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsNumberingUpdateable(){
                               return localIsNumberingUpdateable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsNumberingUpdateable
                               */
                               public void setIsNumberingUpdateable(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsNumberingUpdateableTracker =
                                       true;
                                   
                                            this.localIsNumberingUpdateable=param;
                                    

                               }
                            

                        /**
                        * field for TranslationsList
                        */

                        
                                    protected com.netsuite.webservices.setup.customization_2017_2.CustomRecordTranslationsList localTranslationsList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranslationsListTracker = false ;

                           public boolean isTranslationsListSpecified(){
                               return localTranslationsListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.setup.customization_2017_2.CustomRecordTranslationsList
                           */
                           public  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTranslationsList getTranslationsList(){
                               return localTranslationsList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranslationsList
                               */
                               public void setTranslationsList(com.netsuite.webservices.setup.customization_2017_2.CustomRecordTranslationsList param){
                            localTranslationsListTracker = param != null;
                                   
                                            this.localTranslationsList=param;
                                    

                               }
                            

                        /**
                        * field for IncludeName
                        */

                        
                                    protected boolean localIncludeName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIncludeNameTracker = false ;

                           public boolean isIncludeNameSpecified(){
                               return localIncludeNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIncludeName(){
                               return localIncludeName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IncludeName
                               */
                               public void setIncludeName(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIncludeNameTracker =
                                       true;
                                   
                                            this.localIncludeName=param;
                                    

                               }
                            

                        /**
                        * field for ShowId
                        */

                        
                                    protected boolean localShowId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShowIdTracker = false ;

                           public boolean isShowIdSpecified(){
                               return localShowIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getShowId(){
                               return localShowId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShowId
                               */
                               public void setShowId(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localShowIdTracker =
                                       true;
                                   
                                            this.localShowId=param;
                                    

                               }
                            

                        /**
                        * field for ShowCreationDate
                        */

                        
                                    protected boolean localShowCreationDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShowCreationDateTracker = false ;

                           public boolean isShowCreationDateSpecified(){
                               return localShowCreationDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getShowCreationDate(){
                               return localShowCreationDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShowCreationDate
                               */
                               public void setShowCreationDate(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localShowCreationDateTracker =
                                       true;
                                   
                                            this.localShowCreationDate=param;
                                    

                               }
                            

                        /**
                        * field for ShowCreationDateOnList
                        */

                        
                                    protected boolean localShowCreationDateOnList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShowCreationDateOnListTracker = false ;

                           public boolean isShowCreationDateOnListSpecified(){
                               return localShowCreationDateOnListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getShowCreationDateOnList(){
                               return localShowCreationDateOnList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShowCreationDateOnList
                               */
                               public void setShowCreationDateOnList(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localShowCreationDateOnListTracker =
                                       true;
                                   
                                            this.localShowCreationDateOnList=param;
                                    

                               }
                            

                        /**
                        * field for ShowLastModified
                        */

                        
                                    protected boolean localShowLastModified ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShowLastModifiedTracker = false ;

                           public boolean isShowLastModifiedSpecified(){
                               return localShowLastModifiedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getShowLastModified(){
                               return localShowLastModified;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShowLastModified
                               */
                               public void setShowLastModified(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localShowLastModifiedTracker =
                                       true;
                                   
                                            this.localShowLastModified=param;
                                    

                               }
                            

                        /**
                        * field for ShowLastModifiedOnList
                        */

                        
                                    protected boolean localShowLastModifiedOnList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShowLastModifiedOnListTracker = false ;

                           public boolean isShowLastModifiedOnListSpecified(){
                               return localShowLastModifiedOnListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getShowLastModifiedOnList(){
                               return localShowLastModifiedOnList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShowLastModifiedOnList
                               */
                               public void setShowLastModifiedOnList(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localShowLastModifiedOnListTracker =
                                       true;
                                   
                                            this.localShowLastModifiedOnList=param;
                                    

                               }
                            

                        /**
                        * field for ShowOwner
                        */

                        
                                    protected boolean localShowOwner ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShowOwnerTracker = false ;

                           public boolean isShowOwnerSpecified(){
                               return localShowOwnerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getShowOwner(){
                               return localShowOwner;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShowOwner
                               */
                               public void setShowOwner(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localShowOwnerTracker =
                                       true;
                                   
                                            this.localShowOwner=param;
                                    

                               }
                            

                        /**
                        * field for ShowOwnerOnList
                        */

                        
                                    protected boolean localShowOwnerOnList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShowOwnerOnListTracker = false ;

                           public boolean isShowOwnerOnListSpecified(){
                               return localShowOwnerOnListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getShowOwnerOnList(){
                               return localShowOwnerOnList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShowOwnerOnList
                               */
                               public void setShowOwnerOnList(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localShowOwnerOnListTracker =
                                       true;
                                   
                                            this.localShowOwnerOnList=param;
                                    

                               }
                            

                        /**
                        * field for ShowOwnerAllowChange
                        */

                        
                                    protected boolean localShowOwnerAllowChange ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShowOwnerAllowChangeTracker = false ;

                           public boolean isShowOwnerAllowChangeSpecified(){
                               return localShowOwnerAllowChangeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getShowOwnerAllowChange(){
                               return localShowOwnerAllowChange;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShowOwnerAllowChange
                               */
                               public void setShowOwnerAllowChange(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localShowOwnerAllowChangeTracker =
                                       true;
                                   
                                            this.localShowOwnerAllowChange=param;
                                    

                               }
                            

                        /**
                        * field for UsePermissions
                        */

                        
                                    protected boolean localUsePermissions ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUsePermissionsTracker = false ;

                           public boolean isUsePermissionsSpecified(){
                               return localUsePermissionsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getUsePermissions(){
                               return localUsePermissions;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UsePermissions
                               */
                               public void setUsePermissions(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localUsePermissionsTracker =
                                       true;
                                   
                                            this.localUsePermissions=param;
                                    

                               }
                            

                        /**
                        * field for AllowAttachments
                        */

                        
                                    protected boolean localAllowAttachments ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAllowAttachmentsTracker = false ;

                           public boolean isAllowAttachmentsSpecified(){
                               return localAllowAttachmentsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getAllowAttachments(){
                               return localAllowAttachments;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AllowAttachments
                               */
                               public void setAllowAttachments(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localAllowAttachmentsTracker =
                                       true;
                                   
                                            this.localAllowAttachments=param;
                                    

                               }
                            

                        /**
                        * field for ShowNotes
                        */

                        
                                    protected boolean localShowNotes ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShowNotesTracker = false ;

                           public boolean isShowNotesSpecified(){
                               return localShowNotesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getShowNotes(){
                               return localShowNotes;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShowNotes
                               */
                               public void setShowNotes(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localShowNotesTracker =
                                       true;
                                   
                                            this.localShowNotes=param;
                                    

                               }
                            

                        /**
                        * field for EnablEmailMerge
                        */

                        
                                    protected boolean localEnablEmailMerge ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEnablEmailMergeTracker = false ;

                           public boolean isEnablEmailMergeSpecified(){
                               return localEnablEmailMergeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getEnablEmailMerge(){
                               return localEnablEmailMerge;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EnablEmailMerge
                               */
                               public void setEnablEmailMerge(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localEnablEmailMergeTracker =
                                       true;
                                   
                                            this.localEnablEmailMerge=param;
                                    

                               }
                            

                        /**
                        * field for IsOrdered
                        */

                        
                                    protected boolean localIsOrdered ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsOrderedTracker = false ;

                           public boolean isIsOrderedSpecified(){
                               return localIsOrderedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsOrdered(){
                               return localIsOrdered;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsOrdered
                               */
                               public void setIsOrdered(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsOrderedTracker =
                                       true;
                                   
                                            this.localIsOrdered=param;
                                    

                               }
                            

                        /**
                        * field for AllowInlineEditing
                        */

                        
                                    protected boolean localAllowInlineEditing ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAllowInlineEditingTracker = false ;

                           public boolean isAllowInlineEditingSpecified(){
                               return localAllowInlineEditingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getAllowInlineEditing(){
                               return localAllowInlineEditing;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AllowInlineEditing
                               */
                               public void setAllowInlineEditing(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localAllowInlineEditingTracker =
                                       true;
                                   
                                            this.localAllowInlineEditing=param;
                                    

                               }
                            

                        /**
                        * field for IsAvailableOffline
                        */

                        
                                    protected boolean localIsAvailableOffline ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsAvailableOfflineTracker = false ;

                           public boolean isIsAvailableOfflineSpecified(){
                               return localIsAvailableOfflineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsAvailableOffline(){
                               return localIsAvailableOffline;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsAvailableOffline
                               */
                               public void setIsAvailableOffline(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsAvailableOfflineTracker =
                                       true;
                                   
                                            this.localIsAvailableOffline=param;
                                    

                               }
                            

                        /**
                        * field for AllowQuickSearch
                        */

                        
                                    protected boolean localAllowQuickSearch ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAllowQuickSearchTracker = false ;

                           public boolean isAllowQuickSearchSpecified(){
                               return localAllowQuickSearchTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getAllowQuickSearch(){
                               return localAllowQuickSearch;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AllowQuickSearch
                               */
                               public void setAllowQuickSearch(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localAllowQuickSearchTracker =
                                       true;
                                   
                                            this.localAllowQuickSearch=param;
                                    

                               }
                            

                        /**
                        * field for RecordName
                        */

                        
                                    protected java.lang.String localRecordName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecordNameTracker = false ;

                           public boolean isRecordNameSpecified(){
                               return localRecordNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRecordName(){
                               return localRecordName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecordName
                               */
                               public void setRecordName(java.lang.String param){
                            localRecordNameTracker = param != null;
                                   
                                            this.localRecordName=param;
                                    

                               }
                            

                        /**
                        * field for ScriptId
                        */

                        
                                    protected java.lang.String localScriptId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localScriptIdTracker = false ;

                           public boolean isScriptIdSpecified(){
                               return localScriptIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getScriptId(){
                               return localScriptId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ScriptId
                               */
                               public void setScriptId(java.lang.String param){
                            localScriptIdTracker = param != null;
                                   
                                            this.localScriptId=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:customization_2017_2.setup.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":CustomRecord",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "CustomRecord",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localCustomRecordIdTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "customRecordId", xmlWriter);
                             

                                          if (localCustomRecordId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("customRecordId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCustomRecordId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCustomFormTracker){
                                            if (localCustomForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                            }
                                           localCustomForm.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","customForm"),
                                               xmlWriter);
                                        } if (localIsInactiveTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isInactive", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localParentTracker){
                                            if (localParent==null){
                                                 throw new org.apache.axis2.databinding.ADBException("parent cannot be null!!");
                                            }
                                           localParent.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","parent"),
                                               xmlWriter);
                                        } if (localDisclaimerTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "disclaimer", xmlWriter);
                             

                                          if (localDisclaimer==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("disclaimer cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDisclaimer);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCreatedTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "created", xmlWriter);
                             

                                          if (localCreated==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("created cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreated));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLastModifiedTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastModified", xmlWriter);
                             

                                          if (localLastModified==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastModified cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModified));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNameTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "name", xmlWriter);
                             

                                          if (localName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAutoNameTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "autoName", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("autoName cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAutoName));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAltNameTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "altName", xmlWriter);
                             

                                          if (localAltName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("altName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localAltName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOwnerTracker){
                                            if (localOwner==null){
                                                 throw new org.apache.axis2.databinding.ADBException("owner cannot be null!!");
                                            }
                                           localOwner.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","owner"),
                                               xmlWriter);
                                        } if (localRecTypeTracker){
                                            if (localRecType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("recType cannot be null!!");
                                            }
                                           localRecType.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","recType"),
                                               xmlWriter);
                                        } if (localEnableNumberingTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "enableNumbering", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("enableNumbering cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEnableNumbering));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNumberingPrefixTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "numberingPrefix", xmlWriter);
                             

                                          if (localNumberingPrefix==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("numberingPrefix cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localNumberingPrefix);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNumberingSuffixTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "numberingSuffix", xmlWriter);
                             

                                          if (localNumberingSuffix==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("numberingSuffix cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localNumberingSuffix);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNumberingMinDigitsTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "numberingMinDigits", xmlWriter);
                             
                                               if (localNumberingMinDigits==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("numberingMinDigits cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumberingMinDigits));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDescriptionTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "description", xmlWriter);
                             

                                          if (localDescription==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDescription);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNumberingInitTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "numberingInit", xmlWriter);
                             
                                               if (localNumberingInit==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("numberingInit cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumberingInit));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNumberingCurrentNumberTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "numberingCurrentNumber", xmlWriter);
                             
                                               if (localNumberingCurrentNumber==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("numberingCurrentNumber cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumberingCurrentNumber));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAllowNumberingOverrideTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "allowNumberingOverride", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("allowNumberingOverride cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAllowNumberingOverride));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsNumberingUpdateableTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isNumberingUpdateable", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isNumberingUpdateable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsNumberingUpdateable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTranslationsListTracker){
                                            if (localTranslationsList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("translationsList cannot be null!!");
                                            }
                                           localTranslationsList.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","translationsList"),
                                               xmlWriter);
                                        } if (localIncludeNameTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "includeName", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("includeName cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIncludeName));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShowIdTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "showId", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("showId cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowId));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShowCreationDateTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "showCreationDate", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("showCreationDate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowCreationDate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShowCreationDateOnListTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "showCreationDateOnList", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("showCreationDateOnList cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowCreationDateOnList));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShowLastModifiedTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "showLastModified", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("showLastModified cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowLastModified));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShowLastModifiedOnListTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "showLastModifiedOnList", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("showLastModifiedOnList cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowLastModifiedOnList));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShowOwnerTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "showOwner", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("showOwner cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowOwner));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShowOwnerOnListTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "showOwnerOnList", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("showOwnerOnList cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowOwnerOnList));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShowOwnerAllowChangeTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "showOwnerAllowChange", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("showOwnerAllowChange cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowOwnerAllowChange));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localUsePermissionsTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "usePermissions", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("usePermissions cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUsePermissions));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAllowAttachmentsTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "allowAttachments", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("allowAttachments cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAllowAttachments));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localShowNotesTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "showNotes", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("showNotes cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowNotes));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEnablEmailMergeTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "enablEmailMerge", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("enablEmailMerge cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEnablEmailMerge));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsOrderedTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isOrdered", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isOrdered cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsOrdered));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAllowInlineEditingTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "allowInlineEditing", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("allowInlineEditing cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAllowInlineEditing));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsAvailableOfflineTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isAvailableOffline", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isAvailableOffline cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsAvailableOffline));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAllowQuickSearchTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "allowQuickSearch", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("allowQuickSearch cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAllowQuickSearch));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRecordNameTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "recordName", xmlWriter);
                             

                                          if (localRecordName==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("recordName cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRecordName);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localScriptIdTracker){
                                    namespace = "urn:customization_2017_2.setup.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "scriptId", xmlWriter);
                             

                                          if (localScriptId==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("scriptId cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localScriptId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:customization_2017_2.setup.webservices.netsuite.com")){
                return "ns33";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","CustomRecord"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localCustomRecordIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "customRecordId"));
                                 
                                        if (localCustomRecordId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCustomRecordId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("customRecordId cannot be null!!");
                                        }
                                    } if (localCustomFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "customForm"));
                            
                            
                                    if (localCustomForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                    }
                                    elementList.add(localCustomForm);
                                } if (localIsInactiveTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "isInactive"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsInactive));
                            } if (localParentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "parent"));
                            
                            
                                    if (localParent==null){
                                         throw new org.apache.axis2.databinding.ADBException("parent cannot be null!!");
                                    }
                                    elementList.add(localParent);
                                } if (localDisclaimerTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "disclaimer"));
                                 
                                        if (localDisclaimer != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDisclaimer));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("disclaimer cannot be null!!");
                                        }
                                    } if (localCreatedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "created"));
                                 
                                        if (localCreated != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreated));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("created cannot be null!!");
                                        }
                                    } if (localLastModifiedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "lastModified"));
                                 
                                        if (localLastModified != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModified));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastModified cannot be null!!");
                                        }
                                    } if (localNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "name"));
                                 
                                        if (localName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                        }
                                    } if (localAutoNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "autoName"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAutoName));
                            } if (localAltNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "altName"));
                                 
                                        if (localAltName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAltName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("altName cannot be null!!");
                                        }
                                    } if (localOwnerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "owner"));
                            
                            
                                    if (localOwner==null){
                                         throw new org.apache.axis2.databinding.ADBException("owner cannot be null!!");
                                    }
                                    elementList.add(localOwner);
                                } if (localRecTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "recType"));
                            
                            
                                    if (localRecType==null){
                                         throw new org.apache.axis2.databinding.ADBException("recType cannot be null!!");
                                    }
                                    elementList.add(localRecType);
                                } if (localEnableNumberingTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "enableNumbering"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEnableNumbering));
                            } if (localNumberingPrefixTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "numberingPrefix"));
                                 
                                        if (localNumberingPrefix != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumberingPrefix));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("numberingPrefix cannot be null!!");
                                        }
                                    } if (localNumberingSuffixTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "numberingSuffix"));
                                 
                                        if (localNumberingSuffix != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumberingSuffix));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("numberingSuffix cannot be null!!");
                                        }
                                    } if (localNumberingMinDigitsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "numberingMinDigits"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumberingMinDigits));
                            } if (localDescriptionTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "description"));
                                 
                                        if (localDescription != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDescription));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                        }
                                    } if (localNumberingInitTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "numberingInit"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumberingInit));
                            } if (localNumberingCurrentNumberTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "numberingCurrentNumber"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumberingCurrentNumber));
                            } if (localAllowNumberingOverrideTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "allowNumberingOverride"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAllowNumberingOverride));
                            } if (localIsNumberingUpdateableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "isNumberingUpdateable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsNumberingUpdateable));
                            } if (localTranslationsListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "translationsList"));
                            
                            
                                    if (localTranslationsList==null){
                                         throw new org.apache.axis2.databinding.ADBException("translationsList cannot be null!!");
                                    }
                                    elementList.add(localTranslationsList);
                                } if (localIncludeNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "includeName"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIncludeName));
                            } if (localShowIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "showId"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowId));
                            } if (localShowCreationDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "showCreationDate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowCreationDate));
                            } if (localShowCreationDateOnListTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "showCreationDateOnList"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowCreationDateOnList));
                            } if (localShowLastModifiedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "showLastModified"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowLastModified));
                            } if (localShowLastModifiedOnListTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "showLastModifiedOnList"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowLastModifiedOnList));
                            } if (localShowOwnerTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "showOwner"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowOwner));
                            } if (localShowOwnerOnListTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "showOwnerOnList"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowOwnerOnList));
                            } if (localShowOwnerAllowChangeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "showOwnerAllowChange"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowOwnerAllowChange));
                            } if (localUsePermissionsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "usePermissions"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUsePermissions));
                            } if (localAllowAttachmentsTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "allowAttachments"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAllowAttachments));
                            } if (localShowNotesTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "showNotes"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localShowNotes));
                            } if (localEnablEmailMergeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "enablEmailMerge"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEnablEmailMerge));
                            } if (localIsOrderedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "isOrdered"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsOrdered));
                            } if (localAllowInlineEditingTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "allowInlineEditing"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAllowInlineEditing));
                            } if (localIsAvailableOfflineTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "isAvailableOffline"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsAvailableOffline));
                            } if (localAllowQuickSearchTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "allowQuickSearch"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAllowQuickSearch));
                            } if (localRecordNameTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "recordName"));
                                 
                                        if (localRecordName != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecordName));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("recordName cannot be null!!");
                                        }
                                    } if (localScriptIdTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "scriptId"));
                                 
                                        if (localScriptId != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localScriptId));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("scriptId cannot be null!!");
                                        }
                                    } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static CustomRecord parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            CustomRecord object =
                new CustomRecord();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"CustomRecord".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (CustomRecord)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","customRecordId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"customRecordId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCustomRecordId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","customForm").equals(reader.getName())){
                                
                                                object.setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","isInactive").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isInactive" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsInactive(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","parent").equals(reader.getName())){
                                
                                                object.setParent(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","disclaimer").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"disclaimer" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDisclaimer(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","created").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"created" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCreated(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","lastModified").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastModified" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastModified(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","name").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"name" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","autoName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"autoName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAutoName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","altName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"altName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAltName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","owner").equals(reader.getName())){
                                
                                                object.setOwner(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","recType").equals(reader.getName())){
                                
                                                object.setRecType(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","enableNumbering").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"enableNumbering" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEnableNumbering(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","numberingPrefix").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"numberingPrefix" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNumberingPrefix(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","numberingSuffix").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"numberingSuffix" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNumberingSuffix(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","numberingMinDigits").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"numberingMinDigits" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNumberingMinDigits(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setNumberingMinDigits(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","description").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"description" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDescription(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","numberingInit").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"numberingInit" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNumberingInit(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setNumberingInit(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","numberingCurrentNumber").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"numberingCurrentNumber" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNumberingCurrentNumber(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setNumberingCurrentNumber(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","allowNumberingOverride").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"allowNumberingOverride" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAllowNumberingOverride(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","isNumberingUpdateable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isNumberingUpdateable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsNumberingUpdateable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","translationsList").equals(reader.getName())){
                                
                                                object.setTranslationsList(com.netsuite.webservices.setup.customization_2017_2.CustomRecordTranslationsList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","includeName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"includeName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIncludeName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","showId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"showId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShowId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","showCreationDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"showCreationDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShowCreationDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","showCreationDateOnList").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"showCreationDateOnList" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShowCreationDateOnList(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","showLastModified").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"showLastModified" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShowLastModified(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","showLastModifiedOnList").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"showLastModifiedOnList" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShowLastModifiedOnList(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","showOwner").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"showOwner" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShowOwner(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","showOwnerOnList").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"showOwnerOnList" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShowOwnerOnList(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","showOwnerAllowChange").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"showOwnerAllowChange" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShowOwnerAllowChange(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","usePermissions").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"usePermissions" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setUsePermissions(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","allowAttachments").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"allowAttachments" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAllowAttachments(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","showNotes").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"showNotes" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setShowNotes(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","enablEmailMerge").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"enablEmailMerge" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEnablEmailMerge(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","isOrdered").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isOrdered" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsOrdered(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","allowInlineEditing").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"allowInlineEditing" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAllowInlineEditing(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","isAvailableOffline").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isAvailableOffline" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsAvailableOffline(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","allowQuickSearch").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"allowQuickSearch" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAllowQuickSearch(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","recordName").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"recordName" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRecordName(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","scriptId").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"scriptId" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setScriptId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:customization_2017_2.setup.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    