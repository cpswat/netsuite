
/**
 * DeletedRecordType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.core_2017_2.types;
            

            /**
            *  DeletedRecordType bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class DeletedRecordType
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.core_2017_2.platform.webservices.netsuite.com",
                "DeletedRecordType",
                "ns1");

            

                        /**
                        * field for DeletedRecordType
                        */

                        
                                    protected java.lang.String localDeletedRecordType ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected DeletedRecordType(java.lang.String value, boolean isRegisterValue) {
                                    localDeletedRecordType = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localDeletedRecordType, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String _advInterCompanyJournalEntry =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("advInterCompanyJournalEntry");
                                
                                    public static final java.lang.String _assemblyBuild =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("assemblyBuild");
                                
                                    public static final java.lang.String _assemblyItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("assemblyItem");
                                
                                    public static final java.lang.String _assemblyUnbuild =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("assemblyUnbuild");
                                
                                    public static final java.lang.String _billingSchedule =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("billingSchedule");
                                
                                    public static final java.lang.String _bin =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("bin");
                                
                                    public static final java.lang.String _binTransfer =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("binTransfer");
                                
                                    public static final java.lang.String _binWorksheet =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("binWorksheet");
                                
                                    public static final java.lang.String _calendarEvent =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("calendarEvent");
                                
                                    public static final java.lang.String _campaign =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("campaign");
                                
                                    public static final java.lang.String _cashRefund =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("cashRefund");
                                
                                    public static final java.lang.String _cashSale =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("cashSale");
                                
                                    public static final java.lang.String _charge =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("charge");
                                
                                    public static final java.lang.String _check =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("check");
                                
                                    public static final java.lang.String _contact =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("contact");
                                
                                    public static final java.lang.String _contactCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("contactCategory");
                                
                                    public static final java.lang.String _costCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("costCategory");
                                
                                    public static final java.lang.String _couponCode =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("couponCode");
                                
                                    public static final java.lang.String _creditMemo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("creditMemo");
                                
                                    public static final java.lang.String _currencyRate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("currencyRate");
                                
                                    public static final java.lang.String _customRecord =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customRecord");
                                
                                    public static final java.lang.String _customTransaction =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customTransaction");
                                
                                    public static final java.lang.String _customer =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customer");
                                
                                    public static final java.lang.String _customerCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customerCategory");
                                
                                    public static final java.lang.String _customerMessage =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customerMessage");
                                
                                    public static final java.lang.String _customerDeposit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customerDeposit");
                                
                                    public static final java.lang.String _customerPayment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customerPayment");
                                
                                    public static final java.lang.String _customerRefund =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customerRefund");
                                
                                    public static final java.lang.String _customerStatus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customerStatus");
                                
                                    public static final java.lang.String _deposit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("deposit");
                                
                                    public static final java.lang.String _depositApplication =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("depositApplication");
                                
                                    public static final java.lang.String _descriptionItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("descriptionItem");
                                
                                    public static final java.lang.String _discountItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("discountItem");
                                
                                    public static final java.lang.String _downloadItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("downloadItem");
                                
                                    public static final java.lang.String _employee =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("employee");
                                
                                    public static final java.lang.String _estimate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("estimate");
                                
                                    public static final java.lang.String _expenseReport =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("expenseReport");
                                
                                    public static final java.lang.String _file =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("file");
                                
                                    public static final java.lang.String _folder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("folder");
                                
                                    public static final java.lang.String _giftCertificateItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("giftCertificateItem");
                                
                                    public static final java.lang.String _globalAccountMapping =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("globalAccountMapping");
                                
                                    public static final java.lang.String _interCompanyJournalEntry =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("interCompanyJournalEntry");
                                
                                    public static final java.lang.String _interCompanyTransferOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("interCompanyTransferOrder");
                                
                                    public static final java.lang.String _inventoryAdjustment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("inventoryAdjustment");
                                
                                    public static final java.lang.String _inventoryCostRevaluation =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("inventoryCostRevaluation");
                                
                                    public static final java.lang.String _inventoryItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("inventoryItem");
                                
                                    public static final java.lang.String _inventoryNumber =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("inventoryNumber");
                                
                                    public static final java.lang.String _inventoryTransfer =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("inventoryTransfer");
                                
                                    public static final java.lang.String _invoice =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("invoice");
                                
                                    public static final java.lang.String _issue =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("issue");
                                
                                    public static final java.lang.String _itemAccountMapping =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemAccountMapping");
                                
                                    public static final java.lang.String _itemDemandPlan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemDemandPlan");
                                
                                    public static final java.lang.String _itemFulfillment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemFulfillment");
                                
                                    public static final java.lang.String _itemSupplyPlan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemSupplyPlan");
                                
                                    public static final java.lang.String _itemGroup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemGroup");
                                
                                    public static final java.lang.String _itemReceipt =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemReceipt");
                                
                                    public static final java.lang.String _itemRevision =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemRevision");
                                
                                    public static final java.lang.String _job =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("job");
                                
                                    public static final java.lang.String _jobStatus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("jobStatus");
                                
                                    public static final java.lang.String _journalEntry =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("journalEntry");
                                
                                    public static final java.lang.String _kitItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("kitItem");
                                
                                    public static final java.lang.String _lotNumberedAssemblyItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lotNumberedAssemblyItem");
                                
                                    public static final java.lang.String _lotNumberedInventoryItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lotNumberedInventoryItem");
                                
                                    public static final java.lang.String _markupItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("markupItem");
                                
                                    public static final java.lang.String _message =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("message");
                                
                                    public static final java.lang.String _manufacturingCostTemplate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("manufacturingCostTemplate");
                                
                                    public static final java.lang.String _manufacturingOperationTask =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("manufacturingOperationTask");
                                
                                    public static final java.lang.String _manufacturingRouting =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("manufacturingRouting");
                                
                                    public static final java.lang.String _nexus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nexus");
                                
                                    public static final java.lang.String _nonInventoryPurchaseItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nonInventoryPurchaseItem");
                                
                                    public static final java.lang.String _nonInventoryResaleItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nonInventoryResaleItem");
                                
                                    public static final java.lang.String _nonInventorySaleItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nonInventorySaleItem");
                                
                                    public static final java.lang.String _note =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("note");
                                
                                    public static final java.lang.String _noteType =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("noteType");
                                
                                    public static final java.lang.String _opportunity =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("opportunity");
                                
                                    public static final java.lang.String _otherChargePurchaseItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("otherChargePurchaseItem");
                                
                                    public static final java.lang.String _otherChargeResaleItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("otherChargeResaleItem");
                                
                                    public static final java.lang.String _otherChargeSaleItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("otherChargeSaleItem");
                                
                                    public static final java.lang.String _otherNameCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("otherNameCategory");
                                
                                    public static final java.lang.String _partner =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("partner");
                                
                                    public static final java.lang.String _paycheck =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("paycheck");
                                
                                    public static final java.lang.String _paymentItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("paymentItem");
                                
                                    public static final java.lang.String _paymentMethod =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("paymentMethod");
                                
                                    public static final java.lang.String _payrollItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("payrollItem");
                                
                                    public static final java.lang.String _phoneCall =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("phoneCall");
                                
                                    public static final java.lang.String _priceLevel =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("priceLevel");
                                
                                    public static final java.lang.String _pricingGroup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("pricingGroup");
                                
                                    public static final java.lang.String _projectTask =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("projectTask");
                                
                                    public static final java.lang.String _promotionCode =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("promotionCode");
                                
                                    public static final java.lang.String _purchaseOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("purchaseOrder");
                                
                                    public static final java.lang.String _purchaseRequisition =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("purchaseRequisition");
                                
                                    public static final java.lang.String _resourceAllocation =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("resourceAllocation");
                                
                                    public static final java.lang.String _returnAuthorization =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("returnAuthorization");
                                
                                    public static final java.lang.String _salesOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("salesOrder");
                                
                                    public static final java.lang.String _salesTaxItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("salesTaxItem");
                                
                                    public static final java.lang.String _serializedAssemblyItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("serializedAssemblyItem");
                                
                                    public static final java.lang.String _serializedInventoryItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("serializedInventoryItem");
                                
                                    public static final java.lang.String _servicePurchaseItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("servicePurchaseItem");
                                
                                    public static final java.lang.String _serviceResaleItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("serviceResaleItem");
                                
                                    public static final java.lang.String _serviceSaleItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("serviceSaleItem");
                                
                                    public static final java.lang.String _statisticalJournalEntry =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("statisticalJournalEntry");
                                
                                    public static final java.lang.String _subtotalItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("subtotalItem");
                                
                                    public static final java.lang.String _supportCase =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("supportCase");
                                
                                    public static final java.lang.String _supportCaseIssue =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("supportCaseIssue");
                                
                                    public static final java.lang.String _supportCaseOrigin =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("supportCaseOrigin");
                                
                                    public static final java.lang.String _supportCasePriority =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("supportCasePriority");
                                
                                    public static final java.lang.String _supportCaseStatus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("supportCaseStatus");
                                
                                    public static final java.lang.String _supportCaseType =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("supportCaseType");
                                
                                    public static final java.lang.String _task =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("task");
                                
                                    public static final java.lang.String _term =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("term");
                                
                                    public static final java.lang.String _timeSheet =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("timeSheet");
                                
                                    public static final java.lang.String _transferOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("transferOrder");
                                
                                    public static final java.lang.String _usage =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("usage");
                                
                                    public static final java.lang.String _vendor =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("vendor");
                                
                                    public static final java.lang.String _vendorBill =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("vendorBill");
                                
                                    public static final java.lang.String _vendorCredit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("vendorCredit");
                                
                                    public static final java.lang.String _vendorPayment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("vendorPayment");
                                
                                    public static final java.lang.String _vendorReturnAuthorization =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("vendorReturnAuthorization");
                                
                                    public static final java.lang.String _winLossReason =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("winLossReason");
                                
                                    public static final java.lang.String _workOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("workOrder");
                                
                                    public static final java.lang.String _workOrderIssue =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("workOrderIssue");
                                
                                    public static final java.lang.String _workOrderCompletion =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("workOrderCompletion");
                                
                                    public static final java.lang.String _workOrderClose =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("workOrderClose");
                                
                                public static final DeletedRecordType advInterCompanyJournalEntry =
                                    new DeletedRecordType(_advInterCompanyJournalEntry,true);
                            
                                public static final DeletedRecordType assemblyBuild =
                                    new DeletedRecordType(_assemblyBuild,true);
                            
                                public static final DeletedRecordType assemblyItem =
                                    new DeletedRecordType(_assemblyItem,true);
                            
                                public static final DeletedRecordType assemblyUnbuild =
                                    new DeletedRecordType(_assemblyUnbuild,true);
                            
                                public static final DeletedRecordType billingSchedule =
                                    new DeletedRecordType(_billingSchedule,true);
                            
                                public static final DeletedRecordType bin =
                                    new DeletedRecordType(_bin,true);
                            
                                public static final DeletedRecordType binTransfer =
                                    new DeletedRecordType(_binTransfer,true);
                            
                                public static final DeletedRecordType binWorksheet =
                                    new DeletedRecordType(_binWorksheet,true);
                            
                                public static final DeletedRecordType calendarEvent =
                                    new DeletedRecordType(_calendarEvent,true);
                            
                                public static final DeletedRecordType campaign =
                                    new DeletedRecordType(_campaign,true);
                            
                                public static final DeletedRecordType cashRefund =
                                    new DeletedRecordType(_cashRefund,true);
                            
                                public static final DeletedRecordType cashSale =
                                    new DeletedRecordType(_cashSale,true);
                            
                                public static final DeletedRecordType charge =
                                    new DeletedRecordType(_charge,true);
                            
                                public static final DeletedRecordType check =
                                    new DeletedRecordType(_check,true);
                            
                                public static final DeletedRecordType contact =
                                    new DeletedRecordType(_contact,true);
                            
                                public static final DeletedRecordType contactCategory =
                                    new DeletedRecordType(_contactCategory,true);
                            
                                public static final DeletedRecordType costCategory =
                                    new DeletedRecordType(_costCategory,true);
                            
                                public static final DeletedRecordType couponCode =
                                    new DeletedRecordType(_couponCode,true);
                            
                                public static final DeletedRecordType creditMemo =
                                    new DeletedRecordType(_creditMemo,true);
                            
                                public static final DeletedRecordType currencyRate =
                                    new DeletedRecordType(_currencyRate,true);
                            
                                public static final DeletedRecordType customRecord =
                                    new DeletedRecordType(_customRecord,true);
                            
                                public static final DeletedRecordType customTransaction =
                                    new DeletedRecordType(_customTransaction,true);
                            
                                public static final DeletedRecordType customer =
                                    new DeletedRecordType(_customer,true);
                            
                                public static final DeletedRecordType customerCategory =
                                    new DeletedRecordType(_customerCategory,true);
                            
                                public static final DeletedRecordType customerMessage =
                                    new DeletedRecordType(_customerMessage,true);
                            
                                public static final DeletedRecordType customerDeposit =
                                    new DeletedRecordType(_customerDeposit,true);
                            
                                public static final DeletedRecordType customerPayment =
                                    new DeletedRecordType(_customerPayment,true);
                            
                                public static final DeletedRecordType customerRefund =
                                    new DeletedRecordType(_customerRefund,true);
                            
                                public static final DeletedRecordType customerStatus =
                                    new DeletedRecordType(_customerStatus,true);
                            
                                public static final DeletedRecordType deposit =
                                    new DeletedRecordType(_deposit,true);
                            
                                public static final DeletedRecordType depositApplication =
                                    new DeletedRecordType(_depositApplication,true);
                            
                                public static final DeletedRecordType descriptionItem =
                                    new DeletedRecordType(_descriptionItem,true);
                            
                                public static final DeletedRecordType discountItem =
                                    new DeletedRecordType(_discountItem,true);
                            
                                public static final DeletedRecordType downloadItem =
                                    new DeletedRecordType(_downloadItem,true);
                            
                                public static final DeletedRecordType employee =
                                    new DeletedRecordType(_employee,true);
                            
                                public static final DeletedRecordType estimate =
                                    new DeletedRecordType(_estimate,true);
                            
                                public static final DeletedRecordType expenseReport =
                                    new DeletedRecordType(_expenseReport,true);
                            
                                public static final DeletedRecordType file =
                                    new DeletedRecordType(_file,true);
                            
                                public static final DeletedRecordType folder =
                                    new DeletedRecordType(_folder,true);
                            
                                public static final DeletedRecordType giftCertificateItem =
                                    new DeletedRecordType(_giftCertificateItem,true);
                            
                                public static final DeletedRecordType globalAccountMapping =
                                    new DeletedRecordType(_globalAccountMapping,true);
                            
                                public static final DeletedRecordType interCompanyJournalEntry =
                                    new DeletedRecordType(_interCompanyJournalEntry,true);
                            
                                public static final DeletedRecordType interCompanyTransferOrder =
                                    new DeletedRecordType(_interCompanyTransferOrder,true);
                            
                                public static final DeletedRecordType inventoryAdjustment =
                                    new DeletedRecordType(_inventoryAdjustment,true);
                            
                                public static final DeletedRecordType inventoryCostRevaluation =
                                    new DeletedRecordType(_inventoryCostRevaluation,true);
                            
                                public static final DeletedRecordType inventoryItem =
                                    new DeletedRecordType(_inventoryItem,true);
                            
                                public static final DeletedRecordType inventoryNumber =
                                    new DeletedRecordType(_inventoryNumber,true);
                            
                                public static final DeletedRecordType inventoryTransfer =
                                    new DeletedRecordType(_inventoryTransfer,true);
                            
                                public static final DeletedRecordType invoice =
                                    new DeletedRecordType(_invoice,true);
                            
                                public static final DeletedRecordType issue =
                                    new DeletedRecordType(_issue,true);
                            
                                public static final DeletedRecordType itemAccountMapping =
                                    new DeletedRecordType(_itemAccountMapping,true);
                            
                                public static final DeletedRecordType itemDemandPlan =
                                    new DeletedRecordType(_itemDemandPlan,true);
                            
                                public static final DeletedRecordType itemFulfillment =
                                    new DeletedRecordType(_itemFulfillment,true);
                            
                                public static final DeletedRecordType itemSupplyPlan =
                                    new DeletedRecordType(_itemSupplyPlan,true);
                            
                                public static final DeletedRecordType itemGroup =
                                    new DeletedRecordType(_itemGroup,true);
                            
                                public static final DeletedRecordType itemReceipt =
                                    new DeletedRecordType(_itemReceipt,true);
                            
                                public static final DeletedRecordType itemRevision =
                                    new DeletedRecordType(_itemRevision,true);
                            
                                public static final DeletedRecordType job =
                                    new DeletedRecordType(_job,true);
                            
                                public static final DeletedRecordType jobStatus =
                                    new DeletedRecordType(_jobStatus,true);
                            
                                public static final DeletedRecordType journalEntry =
                                    new DeletedRecordType(_journalEntry,true);
                            
                                public static final DeletedRecordType kitItem =
                                    new DeletedRecordType(_kitItem,true);
                            
                                public static final DeletedRecordType lotNumberedAssemblyItem =
                                    new DeletedRecordType(_lotNumberedAssemblyItem,true);
                            
                                public static final DeletedRecordType lotNumberedInventoryItem =
                                    new DeletedRecordType(_lotNumberedInventoryItem,true);
                            
                                public static final DeletedRecordType markupItem =
                                    new DeletedRecordType(_markupItem,true);
                            
                                public static final DeletedRecordType message =
                                    new DeletedRecordType(_message,true);
                            
                                public static final DeletedRecordType manufacturingCostTemplate =
                                    new DeletedRecordType(_manufacturingCostTemplate,true);
                            
                                public static final DeletedRecordType manufacturingOperationTask =
                                    new DeletedRecordType(_manufacturingOperationTask,true);
                            
                                public static final DeletedRecordType manufacturingRouting =
                                    new DeletedRecordType(_manufacturingRouting,true);
                            
                                public static final DeletedRecordType nexus =
                                    new DeletedRecordType(_nexus,true);
                            
                                public static final DeletedRecordType nonInventoryPurchaseItem =
                                    new DeletedRecordType(_nonInventoryPurchaseItem,true);
                            
                                public static final DeletedRecordType nonInventoryResaleItem =
                                    new DeletedRecordType(_nonInventoryResaleItem,true);
                            
                                public static final DeletedRecordType nonInventorySaleItem =
                                    new DeletedRecordType(_nonInventorySaleItem,true);
                            
                                public static final DeletedRecordType note =
                                    new DeletedRecordType(_note,true);
                            
                                public static final DeletedRecordType noteType =
                                    new DeletedRecordType(_noteType,true);
                            
                                public static final DeletedRecordType opportunity =
                                    new DeletedRecordType(_opportunity,true);
                            
                                public static final DeletedRecordType otherChargePurchaseItem =
                                    new DeletedRecordType(_otherChargePurchaseItem,true);
                            
                                public static final DeletedRecordType otherChargeResaleItem =
                                    new DeletedRecordType(_otherChargeResaleItem,true);
                            
                                public static final DeletedRecordType otherChargeSaleItem =
                                    new DeletedRecordType(_otherChargeSaleItem,true);
                            
                                public static final DeletedRecordType otherNameCategory =
                                    new DeletedRecordType(_otherNameCategory,true);
                            
                                public static final DeletedRecordType partner =
                                    new DeletedRecordType(_partner,true);
                            
                                public static final DeletedRecordType paycheck =
                                    new DeletedRecordType(_paycheck,true);
                            
                                public static final DeletedRecordType paymentItem =
                                    new DeletedRecordType(_paymentItem,true);
                            
                                public static final DeletedRecordType paymentMethod =
                                    new DeletedRecordType(_paymentMethod,true);
                            
                                public static final DeletedRecordType payrollItem =
                                    new DeletedRecordType(_payrollItem,true);
                            
                                public static final DeletedRecordType phoneCall =
                                    new DeletedRecordType(_phoneCall,true);
                            
                                public static final DeletedRecordType priceLevel =
                                    new DeletedRecordType(_priceLevel,true);
                            
                                public static final DeletedRecordType pricingGroup =
                                    new DeletedRecordType(_pricingGroup,true);
                            
                                public static final DeletedRecordType projectTask =
                                    new DeletedRecordType(_projectTask,true);
                            
                                public static final DeletedRecordType promotionCode =
                                    new DeletedRecordType(_promotionCode,true);
                            
                                public static final DeletedRecordType purchaseOrder =
                                    new DeletedRecordType(_purchaseOrder,true);
                            
                                public static final DeletedRecordType purchaseRequisition =
                                    new DeletedRecordType(_purchaseRequisition,true);
                            
                                public static final DeletedRecordType resourceAllocation =
                                    new DeletedRecordType(_resourceAllocation,true);
                            
                                public static final DeletedRecordType returnAuthorization =
                                    new DeletedRecordType(_returnAuthorization,true);
                            
                                public static final DeletedRecordType salesOrder =
                                    new DeletedRecordType(_salesOrder,true);
                            
                                public static final DeletedRecordType salesTaxItem =
                                    new DeletedRecordType(_salesTaxItem,true);
                            
                                public static final DeletedRecordType serializedAssemblyItem =
                                    new DeletedRecordType(_serializedAssemblyItem,true);
                            
                                public static final DeletedRecordType serializedInventoryItem =
                                    new DeletedRecordType(_serializedInventoryItem,true);
                            
                                public static final DeletedRecordType servicePurchaseItem =
                                    new DeletedRecordType(_servicePurchaseItem,true);
                            
                                public static final DeletedRecordType serviceResaleItem =
                                    new DeletedRecordType(_serviceResaleItem,true);
                            
                                public static final DeletedRecordType serviceSaleItem =
                                    new DeletedRecordType(_serviceSaleItem,true);
                            
                                public static final DeletedRecordType statisticalJournalEntry =
                                    new DeletedRecordType(_statisticalJournalEntry,true);
                            
                                public static final DeletedRecordType subtotalItem =
                                    new DeletedRecordType(_subtotalItem,true);
                            
                                public static final DeletedRecordType supportCase =
                                    new DeletedRecordType(_supportCase,true);
                            
                                public static final DeletedRecordType supportCaseIssue =
                                    new DeletedRecordType(_supportCaseIssue,true);
                            
                                public static final DeletedRecordType supportCaseOrigin =
                                    new DeletedRecordType(_supportCaseOrigin,true);
                            
                                public static final DeletedRecordType supportCasePriority =
                                    new DeletedRecordType(_supportCasePriority,true);
                            
                                public static final DeletedRecordType supportCaseStatus =
                                    new DeletedRecordType(_supportCaseStatus,true);
                            
                                public static final DeletedRecordType supportCaseType =
                                    new DeletedRecordType(_supportCaseType,true);
                            
                                public static final DeletedRecordType task =
                                    new DeletedRecordType(_task,true);
                            
                                public static final DeletedRecordType term =
                                    new DeletedRecordType(_term,true);
                            
                                public static final DeletedRecordType timeSheet =
                                    new DeletedRecordType(_timeSheet,true);
                            
                                public static final DeletedRecordType transferOrder =
                                    new DeletedRecordType(_transferOrder,true);
                            
                                public static final DeletedRecordType usage =
                                    new DeletedRecordType(_usage,true);
                            
                                public static final DeletedRecordType vendor =
                                    new DeletedRecordType(_vendor,true);
                            
                                public static final DeletedRecordType vendorBill =
                                    new DeletedRecordType(_vendorBill,true);
                            
                                public static final DeletedRecordType vendorCredit =
                                    new DeletedRecordType(_vendorCredit,true);
                            
                                public static final DeletedRecordType vendorPayment =
                                    new DeletedRecordType(_vendorPayment,true);
                            
                                public static final DeletedRecordType vendorReturnAuthorization =
                                    new DeletedRecordType(_vendorReturnAuthorization,true);
                            
                                public static final DeletedRecordType winLossReason =
                                    new DeletedRecordType(_winLossReason,true);
                            
                                public static final DeletedRecordType workOrder =
                                    new DeletedRecordType(_workOrder,true);
                            
                                public static final DeletedRecordType workOrderIssue =
                                    new DeletedRecordType(_workOrderIssue,true);
                            
                                public static final DeletedRecordType workOrderCompletion =
                                    new DeletedRecordType(_workOrderCompletion,true);
                            
                                public static final DeletedRecordType workOrderClose =
                                    new DeletedRecordType(_workOrderClose,true);
                            

                                public java.lang.String getValue() { return localDeletedRecordType;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localDeletedRecordType.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.core_2017_2.platform.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":DeletedRecordType",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "DeletedRecordType",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localDeletedRecordType==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("DeletedRecordType cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localDeletedRecordType);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.core_2017_2.platform.webservices.netsuite.com")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDeletedRecordType)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static DeletedRecordType fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    DeletedRecordType enumeration = (DeletedRecordType)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static DeletedRecordType fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static DeletedRecordType fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return DeletedRecordType.Factory.fromString(content,namespaceUri);
                    } else {
                       return DeletedRecordType.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static DeletedRecordType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            DeletedRecordType object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"DeletedRecordType" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = DeletedRecordType.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = DeletedRecordType.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    