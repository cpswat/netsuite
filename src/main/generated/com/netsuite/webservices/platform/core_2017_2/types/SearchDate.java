
/**
 * SearchDate.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.core_2017_2.types;
            

            /**
            *  SearchDate bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class SearchDate
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.core_2017_2.platform.webservices.netsuite.com",
                "SearchDate",
                "ns1");

            

                        /**
                        * field for SearchDate
                        */

                        
                                    protected java.lang.String localSearchDate ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected SearchDate(java.lang.String value, boolean isRegisterValue) {
                                    localSearchDate = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localSearchDate, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String _fiscalHalfBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("fiscalHalfBeforeLast");
                                
                                    public static final java.lang.String _fiscalHalfBeforeLastToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("fiscalHalfBeforeLastToDate");
                                
                                    public static final java.lang.String _fiscalQuarterBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("fiscalQuarterBeforeLast");
                                
                                    public static final java.lang.String _fiscalQuarterBeforeLastToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("fiscalQuarterBeforeLastToDate");
                                
                                    public static final java.lang.String _fiscalYearBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("fiscalYearBeforeLast");
                                
                                    public static final java.lang.String _fiscalYearBeforeLastToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("fiscalYearBeforeLastToDate");
                                
                                    public static final java.lang.String _fiveDaysAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("fiveDaysAgo");
                                
                                    public static final java.lang.String _fiveDaysFromNow =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("fiveDaysFromNow");
                                
                                    public static final java.lang.String _fourDaysAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("fourDaysAgo");
                                
                                    public static final java.lang.String _fourDaysFromNow =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("fourDaysFromNow");
                                
                                    public static final java.lang.String _fourWeeksStartingThisWeek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("fourWeeksStartingThisWeek");
                                
                                    public static final java.lang.String _lastBusinessWeek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastBusinessWeek");
                                
                                    public static final java.lang.String _lastFiscalHalf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastFiscalHalf");
                                
                                    public static final java.lang.String _lastFiscalHalfOneFiscalYearAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastFiscalHalfOneFiscalYearAgo");
                                
                                    public static final java.lang.String _lastFiscalHalfToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastFiscalHalfToDate");
                                
                                    public static final java.lang.String _lastFiscalQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastFiscalQuarter");
                                
                                    public static final java.lang.String _lastFiscalQuarterOneFiscalYearAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastFiscalQuarterOneFiscalYearAgo");
                                
                                    public static final java.lang.String _lastFiscalQuarterToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastFiscalQuarterToDate");
                                
                                    public static final java.lang.String _lastFiscalQuarterTwoFiscalYearsAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastFiscalQuarterTwoFiscalYearsAgo");
                                
                                    public static final java.lang.String _lastFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastFiscalYear");
                                
                                    public static final java.lang.String _lastFiscalYearToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastFiscalYearToDate");
                                
                                    public static final java.lang.String _lastMonth =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastMonth");
                                
                                    public static final java.lang.String _lastMonthOneFiscalQuarterAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastMonthOneFiscalQuarterAgo");
                                
                                    public static final java.lang.String _lastMonthOneFiscalYearAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastMonthOneFiscalYearAgo");
                                
                                    public static final java.lang.String _lastMonthToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastMonthToDate");
                                
                                    public static final java.lang.String _lastMonthTwoFiscalQuartersAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastMonthTwoFiscalQuartersAgo");
                                
                                    public static final java.lang.String _lastMonthTwoFiscalYearsAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastMonthTwoFiscalYearsAgo");
                                
                                    public static final java.lang.String _lastRollingHalf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastRollingHalf");
                                
                                    public static final java.lang.String _lastRollingQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastRollingQuarter");
                                
                                    public static final java.lang.String _lastRollingYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastRollingYear");
                                
                                    public static final java.lang.String _lastWeek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastWeek");
                                
                                    public static final java.lang.String _lastWeekToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lastWeekToDate");
                                
                                    public static final java.lang.String _monthAfterNext =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("monthAfterNext");
                                
                                    public static final java.lang.String _monthAfterNextToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("monthAfterNextToDate");
                                
                                    public static final java.lang.String _monthBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("monthBeforeLast");
                                
                                    public static final java.lang.String _monthBeforeLastToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("monthBeforeLastToDate");
                                
                                    public static final java.lang.String _nextBusinessWeek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nextBusinessWeek");
                                
                                    public static final java.lang.String _nextFiscalHalf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nextFiscalHalf");
                                
                                    public static final java.lang.String _nextFiscalQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nextFiscalQuarter");
                                
                                    public static final java.lang.String _nextFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nextFiscalYear");
                                
                                    public static final java.lang.String _nextFourWeeks =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nextFourWeeks");
                                
                                    public static final java.lang.String _nextMonth =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nextMonth");
                                
                                    public static final java.lang.String _nextOneHalf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nextOneHalf");
                                
                                    public static final java.lang.String _nextOneMonth =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nextOneMonth");
                                
                                    public static final java.lang.String _nextOneQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nextOneQuarter");
                                
                                    public static final java.lang.String _nextOneWeek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nextOneWeek");
                                
                                    public static final java.lang.String _nextOneYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nextOneYear");
                                
                                    public static final java.lang.String _nextWeek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nextWeek");
                                
                                    public static final java.lang.String _ninetyDaysAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ninetyDaysAgo");
                                
                                    public static final java.lang.String _ninetyDaysFromNow =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ninetyDaysFromNow");
                                
                                    public static final java.lang.String _oneYearBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("oneYearBeforeLast");
                                
                                    public static final java.lang.String _previousFiscalQuartersLastFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousFiscalQuartersLastFiscalYear");
                                
                                    public static final java.lang.String _previousFiscalQuartersThisFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousFiscalQuartersThisFiscalYear");
                                
                                    public static final java.lang.String _previousMonthsLastFiscalHalf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousMonthsLastFiscalHalf");
                                
                                    public static final java.lang.String _previousMonthsLastFiscalQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousMonthsLastFiscalQuarter");
                                
                                    public static final java.lang.String _previousMonthsLastFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousMonthsLastFiscalYear");
                                
                                    public static final java.lang.String _previousMonthsSameFiscalHalfLastFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousMonthsSameFiscalHalfLastFiscalYear");
                                
                                    public static final java.lang.String _previousMonthsSameFiscalQuarterLastFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousMonthsSameFiscalQuarterLastFiscalYear");
                                
                                    public static final java.lang.String _previousMonthsThisFiscalHalf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousMonthsThisFiscalHalf");
                                
                                    public static final java.lang.String _previousMonthsThisFiscalQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousMonthsThisFiscalQuarter");
                                
                                    public static final java.lang.String _previousMonthsThisFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousMonthsThisFiscalYear");
                                
                                    public static final java.lang.String _previousOneDay =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousOneDay");
                                
                                    public static final java.lang.String _previousOneHalf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousOneHalf");
                                
                                    public static final java.lang.String _previousOneMonth =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousOneMonth");
                                
                                    public static final java.lang.String _previousOneQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousOneQuarter");
                                
                                    public static final java.lang.String _previousOneWeek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousOneWeek");
                                
                                    public static final java.lang.String _previousOneYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousOneYear");
                                
                                    public static final java.lang.String _previousRollingHalf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousRollingHalf");
                                
                                    public static final java.lang.String _previousRollingQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousRollingQuarter");
                                
                                    public static final java.lang.String _previousRollingYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("previousRollingYear");
                                
                                    public static final java.lang.String _sameDayFiscalQuarterBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameDayFiscalQuarterBeforeLast");
                                
                                    public static final java.lang.String _sameDayFiscalYearBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameDayFiscalYearBeforeLast");
                                
                                    public static final java.lang.String _sameDayLastFiscalQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameDayLastFiscalQuarter");
                                
                                    public static final java.lang.String _sameDayLastFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameDayLastFiscalYear");
                                
                                    public static final java.lang.String _sameDayLastMonth =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameDayLastMonth");
                                
                                    public static final java.lang.String _sameDayLastWeek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameDayLastWeek");
                                
                                    public static final java.lang.String _sameDayMonthBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameDayMonthBeforeLast");
                                
                                    public static final java.lang.String _sameDayWeekBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameDayWeekBeforeLast");
                                
                                    public static final java.lang.String _sameFiscalHalfLastFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameFiscalHalfLastFiscalYear");
                                
                                    public static final java.lang.String _sameFiscalHalfLastFiscalYearToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameFiscalHalfLastFiscalYearToDate");
                                
                                    public static final java.lang.String _sameFiscalQuarterFiscalYearBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameFiscalQuarterFiscalYearBeforeLast");
                                
                                    public static final java.lang.String _sameFiscalQuarterLastFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameFiscalQuarterLastFiscalYear");
                                
                                    public static final java.lang.String _sameFiscalQuarterLastFiscalYearToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameFiscalQuarterLastFiscalYearToDate");
                                
                                    public static final java.lang.String _sameMonthFiscalQuarterBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameMonthFiscalQuarterBeforeLast");
                                
                                    public static final java.lang.String _sameMonthFiscalYearBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameMonthFiscalYearBeforeLast");
                                
                                    public static final java.lang.String _sameMonthLastFiscalQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameMonthLastFiscalQuarter");
                                
                                    public static final java.lang.String _sameMonthLastFiscalQuarterToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameMonthLastFiscalQuarterToDate");
                                
                                    public static final java.lang.String _sameMonthLastFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameMonthLastFiscalYear");
                                
                                    public static final java.lang.String _sameMonthLastFiscalYearToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameMonthLastFiscalYearToDate");
                                
                                    public static final java.lang.String _sameWeekFiscalYearBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameWeekFiscalYearBeforeLast");
                                
                                    public static final java.lang.String _sameWeekLastFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sameWeekLastFiscalYear");
                                
                                    public static final java.lang.String _sixtyDaysAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sixtyDaysAgo");
                                
                                    public static final java.lang.String _sixtyDaysFromNow =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("sixtyDaysFromNow");
                                
                                    public static final java.lang.String _startOfFiscalHalfBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfFiscalHalfBeforeLast");
                                
                                    public static final java.lang.String _startOfFiscalQuarterBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfFiscalQuarterBeforeLast");
                                
                                    public static final java.lang.String _startOfFiscalYearBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfFiscalYearBeforeLast");
                                
                                    public static final java.lang.String _startOfLastBusinessWeek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfLastBusinessWeek");
                                
                                    public static final java.lang.String _startOfLastFiscalHalf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfLastFiscalHalf");
                                
                                    public static final java.lang.String _startOfLastFiscalHalfOneFiscalYearAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfLastFiscalHalfOneFiscalYearAgo");
                                
                                    public static final java.lang.String _startOfLastFiscalQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfLastFiscalQuarter");
                                
                                    public static final java.lang.String _startOfLastFiscalQuarterOneFiscalYearAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfLastFiscalQuarterOneFiscalYearAgo");
                                
                                    public static final java.lang.String _startOfLastFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfLastFiscalYear");
                                
                                    public static final java.lang.String _startOfLastMonth =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfLastMonth");
                                
                                    public static final java.lang.String _startOfLastMonthOneFiscalQuarterAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfLastMonthOneFiscalQuarterAgo");
                                
                                    public static final java.lang.String _startOfLastMonthOneFiscalYearAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfLastMonthOneFiscalYearAgo");
                                
                                    public static final java.lang.String _startOfLastRollingHalf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfLastRollingHalf");
                                
                                    public static final java.lang.String _startOfLastRollingQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfLastRollingQuarter");
                                
                                    public static final java.lang.String _startOfLastRollingYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfLastRollingYear");
                                
                                    public static final java.lang.String _startOfLastWeek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfLastWeek");
                                
                                    public static final java.lang.String _startOfMonthBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfMonthBeforeLast");
                                
                                    public static final java.lang.String _startOfNextBusinessWeek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfNextBusinessWeek");
                                
                                    public static final java.lang.String _startOfNextFiscalHalf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfNextFiscalHalf");
                                
                                    public static final java.lang.String _startOfNextFiscalQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfNextFiscalQuarter");
                                
                                    public static final java.lang.String _startOfNextFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfNextFiscalYear");
                                
                                    public static final java.lang.String _startOfNextMonth =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfNextMonth");
                                
                                    public static final java.lang.String _startOfNextWeek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfNextWeek");
                                
                                    public static final java.lang.String _startOfPreviousRollingHalf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfPreviousRollingHalf");
                                
                                    public static final java.lang.String _startOfPreviousRollingQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfPreviousRollingQuarter");
                                
                                    public static final java.lang.String _startOfPreviousRollingYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfPreviousRollingYear");
                                
                                    public static final java.lang.String _startOfSameFiscalHalfLastFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfSameFiscalHalfLastFiscalYear");
                                
                                    public static final java.lang.String _startOfSameFiscalQuarterLastFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfSameFiscalQuarterLastFiscalYear");
                                
                                    public static final java.lang.String _startOfSameMonthLastFiscalQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfSameMonthLastFiscalQuarter");
                                
                                    public static final java.lang.String _startOfSameMonthLastFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfSameMonthLastFiscalYear");
                                
                                    public static final java.lang.String _startOfThisBusinessWeek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfThisBusinessWeek");
                                
                                    public static final java.lang.String _startOfThisFiscalHalf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfThisFiscalHalf");
                                
                                    public static final java.lang.String _startOfThisFiscalQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfThisFiscalQuarter");
                                
                                    public static final java.lang.String _startOfThisFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfThisFiscalYear");
                                
                                    public static final java.lang.String _startOfThisMonth =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfThisMonth");
                                
                                    public static final java.lang.String _startOfThisWeek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfThisWeek");
                                
                                    public static final java.lang.String _startOfThisYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfThisYear");
                                
                                    public static final java.lang.String _startOfWeekBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("startOfWeekBeforeLast");
                                
                                    public static final java.lang.String _tenDaysAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("tenDaysAgo");
                                
                                    public static final java.lang.String _tenDaysFromNow =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("tenDaysFromNow");
                                
                                    public static final java.lang.String _thirtyDaysAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("thirtyDaysAgo");
                                
                                    public static final java.lang.String _thirtyDaysFromNow =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("thirtyDaysFromNow");
                                
                                    public static final java.lang.String _thisBusinessWeek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("thisBusinessWeek");
                                
                                    public static final java.lang.String _thisFiscalHalf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("thisFiscalHalf");
                                
                                    public static final java.lang.String _thisFiscalHalfToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("thisFiscalHalfToDate");
                                
                                    public static final java.lang.String _thisFiscalQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("thisFiscalQuarter");
                                
                                    public static final java.lang.String _thisFiscalQuarterToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("thisFiscalQuarterToDate");
                                
                                    public static final java.lang.String _thisFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("thisFiscalYear");
                                
                                    public static final java.lang.String _thisFiscalYearToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("thisFiscalYearToDate");
                                
                                    public static final java.lang.String _thisMonth =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("thisMonth");
                                
                                    public static final java.lang.String _thisMonthToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("thisMonthToDate");
                                
                                    public static final java.lang.String _thisRollingHalf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("thisRollingHalf");
                                
                                    public static final java.lang.String _thisRollingQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("thisRollingQuarter");
                                
                                    public static final java.lang.String _thisRollingYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("thisRollingYear");
                                
                                    public static final java.lang.String _thisWeek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("thisWeek");
                                
                                    public static final java.lang.String _thisWeekToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("thisWeekToDate");
                                
                                    public static final java.lang.String _thisYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("thisYear");
                                
                                    public static final java.lang.String _threeDaysAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("threeDaysAgo");
                                
                                    public static final java.lang.String _threeDaysFromNow =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("threeDaysFromNow");
                                
                                    public static final java.lang.String _threeFiscalQuartersAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("threeFiscalQuartersAgo");
                                
                                    public static final java.lang.String _threeFiscalQuartersAgoToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("threeFiscalQuartersAgoToDate");
                                
                                    public static final java.lang.String _threeFiscalYearsAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("threeFiscalYearsAgo");
                                
                                    public static final java.lang.String _threeFiscalYearsAgoToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("threeFiscalYearsAgoToDate");
                                
                                    public static final java.lang.String _threeMonthsAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("threeMonthsAgo");
                                
                                    public static final java.lang.String _threeMonthsAgoToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("threeMonthsAgoToDate");
                                
                                    public static final java.lang.String _today =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("today");
                                
                                    public static final java.lang.String _tomorrow =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("tomorrow");
                                
                                    public static final java.lang.String _twoDaysAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("twoDaysAgo");
                                
                                    public static final java.lang.String _twoDaysFromNow =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("twoDaysFromNow");
                                
                                    public static final java.lang.String _weekAfterNext =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("weekAfterNext");
                                
                                    public static final java.lang.String _weekAfterNextToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("weekAfterNextToDate");
                                
                                    public static final java.lang.String _weekBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("weekBeforeLast");
                                
                                    public static final java.lang.String _weekBeforeLastToDate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("weekBeforeLastToDate");
                                
                                    public static final java.lang.String _yesterday =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("yesterday");
                                
                                public static final SearchDate fiscalHalfBeforeLast =
                                    new SearchDate(_fiscalHalfBeforeLast,true);
                            
                                public static final SearchDate fiscalHalfBeforeLastToDate =
                                    new SearchDate(_fiscalHalfBeforeLastToDate,true);
                            
                                public static final SearchDate fiscalQuarterBeforeLast =
                                    new SearchDate(_fiscalQuarterBeforeLast,true);
                            
                                public static final SearchDate fiscalQuarterBeforeLastToDate =
                                    new SearchDate(_fiscalQuarterBeforeLastToDate,true);
                            
                                public static final SearchDate fiscalYearBeforeLast =
                                    new SearchDate(_fiscalYearBeforeLast,true);
                            
                                public static final SearchDate fiscalYearBeforeLastToDate =
                                    new SearchDate(_fiscalYearBeforeLastToDate,true);
                            
                                public static final SearchDate fiveDaysAgo =
                                    new SearchDate(_fiveDaysAgo,true);
                            
                                public static final SearchDate fiveDaysFromNow =
                                    new SearchDate(_fiveDaysFromNow,true);
                            
                                public static final SearchDate fourDaysAgo =
                                    new SearchDate(_fourDaysAgo,true);
                            
                                public static final SearchDate fourDaysFromNow =
                                    new SearchDate(_fourDaysFromNow,true);
                            
                                public static final SearchDate fourWeeksStartingThisWeek =
                                    new SearchDate(_fourWeeksStartingThisWeek,true);
                            
                                public static final SearchDate lastBusinessWeek =
                                    new SearchDate(_lastBusinessWeek,true);
                            
                                public static final SearchDate lastFiscalHalf =
                                    new SearchDate(_lastFiscalHalf,true);
                            
                                public static final SearchDate lastFiscalHalfOneFiscalYearAgo =
                                    new SearchDate(_lastFiscalHalfOneFiscalYearAgo,true);
                            
                                public static final SearchDate lastFiscalHalfToDate =
                                    new SearchDate(_lastFiscalHalfToDate,true);
                            
                                public static final SearchDate lastFiscalQuarter =
                                    new SearchDate(_lastFiscalQuarter,true);
                            
                                public static final SearchDate lastFiscalQuarterOneFiscalYearAgo =
                                    new SearchDate(_lastFiscalQuarterOneFiscalYearAgo,true);
                            
                                public static final SearchDate lastFiscalQuarterToDate =
                                    new SearchDate(_lastFiscalQuarterToDate,true);
                            
                                public static final SearchDate lastFiscalQuarterTwoFiscalYearsAgo =
                                    new SearchDate(_lastFiscalQuarterTwoFiscalYearsAgo,true);
                            
                                public static final SearchDate lastFiscalYear =
                                    new SearchDate(_lastFiscalYear,true);
                            
                                public static final SearchDate lastFiscalYearToDate =
                                    new SearchDate(_lastFiscalYearToDate,true);
                            
                                public static final SearchDate lastMonth =
                                    new SearchDate(_lastMonth,true);
                            
                                public static final SearchDate lastMonthOneFiscalQuarterAgo =
                                    new SearchDate(_lastMonthOneFiscalQuarterAgo,true);
                            
                                public static final SearchDate lastMonthOneFiscalYearAgo =
                                    new SearchDate(_lastMonthOneFiscalYearAgo,true);
                            
                                public static final SearchDate lastMonthToDate =
                                    new SearchDate(_lastMonthToDate,true);
                            
                                public static final SearchDate lastMonthTwoFiscalQuartersAgo =
                                    new SearchDate(_lastMonthTwoFiscalQuartersAgo,true);
                            
                                public static final SearchDate lastMonthTwoFiscalYearsAgo =
                                    new SearchDate(_lastMonthTwoFiscalYearsAgo,true);
                            
                                public static final SearchDate lastRollingHalf =
                                    new SearchDate(_lastRollingHalf,true);
                            
                                public static final SearchDate lastRollingQuarter =
                                    new SearchDate(_lastRollingQuarter,true);
                            
                                public static final SearchDate lastRollingYear =
                                    new SearchDate(_lastRollingYear,true);
                            
                                public static final SearchDate lastWeek =
                                    new SearchDate(_lastWeek,true);
                            
                                public static final SearchDate lastWeekToDate =
                                    new SearchDate(_lastWeekToDate,true);
                            
                                public static final SearchDate monthAfterNext =
                                    new SearchDate(_monthAfterNext,true);
                            
                                public static final SearchDate monthAfterNextToDate =
                                    new SearchDate(_monthAfterNextToDate,true);
                            
                                public static final SearchDate monthBeforeLast =
                                    new SearchDate(_monthBeforeLast,true);
                            
                                public static final SearchDate monthBeforeLastToDate =
                                    new SearchDate(_monthBeforeLastToDate,true);
                            
                                public static final SearchDate nextBusinessWeek =
                                    new SearchDate(_nextBusinessWeek,true);
                            
                                public static final SearchDate nextFiscalHalf =
                                    new SearchDate(_nextFiscalHalf,true);
                            
                                public static final SearchDate nextFiscalQuarter =
                                    new SearchDate(_nextFiscalQuarter,true);
                            
                                public static final SearchDate nextFiscalYear =
                                    new SearchDate(_nextFiscalYear,true);
                            
                                public static final SearchDate nextFourWeeks =
                                    new SearchDate(_nextFourWeeks,true);
                            
                                public static final SearchDate nextMonth =
                                    new SearchDate(_nextMonth,true);
                            
                                public static final SearchDate nextOneHalf =
                                    new SearchDate(_nextOneHalf,true);
                            
                                public static final SearchDate nextOneMonth =
                                    new SearchDate(_nextOneMonth,true);
                            
                                public static final SearchDate nextOneQuarter =
                                    new SearchDate(_nextOneQuarter,true);
                            
                                public static final SearchDate nextOneWeek =
                                    new SearchDate(_nextOneWeek,true);
                            
                                public static final SearchDate nextOneYear =
                                    new SearchDate(_nextOneYear,true);
                            
                                public static final SearchDate nextWeek =
                                    new SearchDate(_nextWeek,true);
                            
                                public static final SearchDate ninetyDaysAgo =
                                    new SearchDate(_ninetyDaysAgo,true);
                            
                                public static final SearchDate ninetyDaysFromNow =
                                    new SearchDate(_ninetyDaysFromNow,true);
                            
                                public static final SearchDate oneYearBeforeLast =
                                    new SearchDate(_oneYearBeforeLast,true);
                            
                                public static final SearchDate previousFiscalQuartersLastFiscalYear =
                                    new SearchDate(_previousFiscalQuartersLastFiscalYear,true);
                            
                                public static final SearchDate previousFiscalQuartersThisFiscalYear =
                                    new SearchDate(_previousFiscalQuartersThisFiscalYear,true);
                            
                                public static final SearchDate previousMonthsLastFiscalHalf =
                                    new SearchDate(_previousMonthsLastFiscalHalf,true);
                            
                                public static final SearchDate previousMonthsLastFiscalQuarter =
                                    new SearchDate(_previousMonthsLastFiscalQuarter,true);
                            
                                public static final SearchDate previousMonthsLastFiscalYear =
                                    new SearchDate(_previousMonthsLastFiscalYear,true);
                            
                                public static final SearchDate previousMonthsSameFiscalHalfLastFiscalYear =
                                    new SearchDate(_previousMonthsSameFiscalHalfLastFiscalYear,true);
                            
                                public static final SearchDate previousMonthsSameFiscalQuarterLastFiscalYear =
                                    new SearchDate(_previousMonthsSameFiscalQuarterLastFiscalYear,true);
                            
                                public static final SearchDate previousMonthsThisFiscalHalf =
                                    new SearchDate(_previousMonthsThisFiscalHalf,true);
                            
                                public static final SearchDate previousMonthsThisFiscalQuarter =
                                    new SearchDate(_previousMonthsThisFiscalQuarter,true);
                            
                                public static final SearchDate previousMonthsThisFiscalYear =
                                    new SearchDate(_previousMonthsThisFiscalYear,true);
                            
                                public static final SearchDate previousOneDay =
                                    new SearchDate(_previousOneDay,true);
                            
                                public static final SearchDate previousOneHalf =
                                    new SearchDate(_previousOneHalf,true);
                            
                                public static final SearchDate previousOneMonth =
                                    new SearchDate(_previousOneMonth,true);
                            
                                public static final SearchDate previousOneQuarter =
                                    new SearchDate(_previousOneQuarter,true);
                            
                                public static final SearchDate previousOneWeek =
                                    new SearchDate(_previousOneWeek,true);
                            
                                public static final SearchDate previousOneYear =
                                    new SearchDate(_previousOneYear,true);
                            
                                public static final SearchDate previousRollingHalf =
                                    new SearchDate(_previousRollingHalf,true);
                            
                                public static final SearchDate previousRollingQuarter =
                                    new SearchDate(_previousRollingQuarter,true);
                            
                                public static final SearchDate previousRollingYear =
                                    new SearchDate(_previousRollingYear,true);
                            
                                public static final SearchDate sameDayFiscalQuarterBeforeLast =
                                    new SearchDate(_sameDayFiscalQuarterBeforeLast,true);
                            
                                public static final SearchDate sameDayFiscalYearBeforeLast =
                                    new SearchDate(_sameDayFiscalYearBeforeLast,true);
                            
                                public static final SearchDate sameDayLastFiscalQuarter =
                                    new SearchDate(_sameDayLastFiscalQuarter,true);
                            
                                public static final SearchDate sameDayLastFiscalYear =
                                    new SearchDate(_sameDayLastFiscalYear,true);
                            
                                public static final SearchDate sameDayLastMonth =
                                    new SearchDate(_sameDayLastMonth,true);
                            
                                public static final SearchDate sameDayLastWeek =
                                    new SearchDate(_sameDayLastWeek,true);
                            
                                public static final SearchDate sameDayMonthBeforeLast =
                                    new SearchDate(_sameDayMonthBeforeLast,true);
                            
                                public static final SearchDate sameDayWeekBeforeLast =
                                    new SearchDate(_sameDayWeekBeforeLast,true);
                            
                                public static final SearchDate sameFiscalHalfLastFiscalYear =
                                    new SearchDate(_sameFiscalHalfLastFiscalYear,true);
                            
                                public static final SearchDate sameFiscalHalfLastFiscalYearToDate =
                                    new SearchDate(_sameFiscalHalfLastFiscalYearToDate,true);
                            
                                public static final SearchDate sameFiscalQuarterFiscalYearBeforeLast =
                                    new SearchDate(_sameFiscalQuarterFiscalYearBeforeLast,true);
                            
                                public static final SearchDate sameFiscalQuarterLastFiscalYear =
                                    new SearchDate(_sameFiscalQuarterLastFiscalYear,true);
                            
                                public static final SearchDate sameFiscalQuarterLastFiscalYearToDate =
                                    new SearchDate(_sameFiscalQuarterLastFiscalYearToDate,true);
                            
                                public static final SearchDate sameMonthFiscalQuarterBeforeLast =
                                    new SearchDate(_sameMonthFiscalQuarterBeforeLast,true);
                            
                                public static final SearchDate sameMonthFiscalYearBeforeLast =
                                    new SearchDate(_sameMonthFiscalYearBeforeLast,true);
                            
                                public static final SearchDate sameMonthLastFiscalQuarter =
                                    new SearchDate(_sameMonthLastFiscalQuarter,true);
                            
                                public static final SearchDate sameMonthLastFiscalQuarterToDate =
                                    new SearchDate(_sameMonthLastFiscalQuarterToDate,true);
                            
                                public static final SearchDate sameMonthLastFiscalYear =
                                    new SearchDate(_sameMonthLastFiscalYear,true);
                            
                                public static final SearchDate sameMonthLastFiscalYearToDate =
                                    new SearchDate(_sameMonthLastFiscalYearToDate,true);
                            
                                public static final SearchDate sameWeekFiscalYearBeforeLast =
                                    new SearchDate(_sameWeekFiscalYearBeforeLast,true);
                            
                                public static final SearchDate sameWeekLastFiscalYear =
                                    new SearchDate(_sameWeekLastFiscalYear,true);
                            
                                public static final SearchDate sixtyDaysAgo =
                                    new SearchDate(_sixtyDaysAgo,true);
                            
                                public static final SearchDate sixtyDaysFromNow =
                                    new SearchDate(_sixtyDaysFromNow,true);
                            
                                public static final SearchDate startOfFiscalHalfBeforeLast =
                                    new SearchDate(_startOfFiscalHalfBeforeLast,true);
                            
                                public static final SearchDate startOfFiscalQuarterBeforeLast =
                                    new SearchDate(_startOfFiscalQuarterBeforeLast,true);
                            
                                public static final SearchDate startOfFiscalYearBeforeLast =
                                    new SearchDate(_startOfFiscalYearBeforeLast,true);
                            
                                public static final SearchDate startOfLastBusinessWeek =
                                    new SearchDate(_startOfLastBusinessWeek,true);
                            
                                public static final SearchDate startOfLastFiscalHalf =
                                    new SearchDate(_startOfLastFiscalHalf,true);
                            
                                public static final SearchDate startOfLastFiscalHalfOneFiscalYearAgo =
                                    new SearchDate(_startOfLastFiscalHalfOneFiscalYearAgo,true);
                            
                                public static final SearchDate startOfLastFiscalQuarter =
                                    new SearchDate(_startOfLastFiscalQuarter,true);
                            
                                public static final SearchDate startOfLastFiscalQuarterOneFiscalYearAgo =
                                    new SearchDate(_startOfLastFiscalQuarterOneFiscalYearAgo,true);
                            
                                public static final SearchDate startOfLastFiscalYear =
                                    new SearchDate(_startOfLastFiscalYear,true);
                            
                                public static final SearchDate startOfLastMonth =
                                    new SearchDate(_startOfLastMonth,true);
                            
                                public static final SearchDate startOfLastMonthOneFiscalQuarterAgo =
                                    new SearchDate(_startOfLastMonthOneFiscalQuarterAgo,true);
                            
                                public static final SearchDate startOfLastMonthOneFiscalYearAgo =
                                    new SearchDate(_startOfLastMonthOneFiscalYearAgo,true);
                            
                                public static final SearchDate startOfLastRollingHalf =
                                    new SearchDate(_startOfLastRollingHalf,true);
                            
                                public static final SearchDate startOfLastRollingQuarter =
                                    new SearchDate(_startOfLastRollingQuarter,true);
                            
                                public static final SearchDate startOfLastRollingYear =
                                    new SearchDate(_startOfLastRollingYear,true);
                            
                                public static final SearchDate startOfLastWeek =
                                    new SearchDate(_startOfLastWeek,true);
                            
                                public static final SearchDate startOfMonthBeforeLast =
                                    new SearchDate(_startOfMonthBeforeLast,true);
                            
                                public static final SearchDate startOfNextBusinessWeek =
                                    new SearchDate(_startOfNextBusinessWeek,true);
                            
                                public static final SearchDate startOfNextFiscalHalf =
                                    new SearchDate(_startOfNextFiscalHalf,true);
                            
                                public static final SearchDate startOfNextFiscalQuarter =
                                    new SearchDate(_startOfNextFiscalQuarter,true);
                            
                                public static final SearchDate startOfNextFiscalYear =
                                    new SearchDate(_startOfNextFiscalYear,true);
                            
                                public static final SearchDate startOfNextMonth =
                                    new SearchDate(_startOfNextMonth,true);
                            
                                public static final SearchDate startOfNextWeek =
                                    new SearchDate(_startOfNextWeek,true);
                            
                                public static final SearchDate startOfPreviousRollingHalf =
                                    new SearchDate(_startOfPreviousRollingHalf,true);
                            
                                public static final SearchDate startOfPreviousRollingQuarter =
                                    new SearchDate(_startOfPreviousRollingQuarter,true);
                            
                                public static final SearchDate startOfPreviousRollingYear =
                                    new SearchDate(_startOfPreviousRollingYear,true);
                            
                                public static final SearchDate startOfSameFiscalHalfLastFiscalYear =
                                    new SearchDate(_startOfSameFiscalHalfLastFiscalYear,true);
                            
                                public static final SearchDate startOfSameFiscalQuarterLastFiscalYear =
                                    new SearchDate(_startOfSameFiscalQuarterLastFiscalYear,true);
                            
                                public static final SearchDate startOfSameMonthLastFiscalQuarter =
                                    new SearchDate(_startOfSameMonthLastFiscalQuarter,true);
                            
                                public static final SearchDate startOfSameMonthLastFiscalYear =
                                    new SearchDate(_startOfSameMonthLastFiscalYear,true);
                            
                                public static final SearchDate startOfThisBusinessWeek =
                                    new SearchDate(_startOfThisBusinessWeek,true);
                            
                                public static final SearchDate startOfThisFiscalHalf =
                                    new SearchDate(_startOfThisFiscalHalf,true);
                            
                                public static final SearchDate startOfThisFiscalQuarter =
                                    new SearchDate(_startOfThisFiscalQuarter,true);
                            
                                public static final SearchDate startOfThisFiscalYear =
                                    new SearchDate(_startOfThisFiscalYear,true);
                            
                                public static final SearchDate startOfThisMonth =
                                    new SearchDate(_startOfThisMonth,true);
                            
                                public static final SearchDate startOfThisWeek =
                                    new SearchDate(_startOfThisWeek,true);
                            
                                public static final SearchDate startOfThisYear =
                                    new SearchDate(_startOfThisYear,true);
                            
                                public static final SearchDate startOfWeekBeforeLast =
                                    new SearchDate(_startOfWeekBeforeLast,true);
                            
                                public static final SearchDate tenDaysAgo =
                                    new SearchDate(_tenDaysAgo,true);
                            
                                public static final SearchDate tenDaysFromNow =
                                    new SearchDate(_tenDaysFromNow,true);
                            
                                public static final SearchDate thirtyDaysAgo =
                                    new SearchDate(_thirtyDaysAgo,true);
                            
                                public static final SearchDate thirtyDaysFromNow =
                                    new SearchDate(_thirtyDaysFromNow,true);
                            
                                public static final SearchDate thisBusinessWeek =
                                    new SearchDate(_thisBusinessWeek,true);
                            
                                public static final SearchDate thisFiscalHalf =
                                    new SearchDate(_thisFiscalHalf,true);
                            
                                public static final SearchDate thisFiscalHalfToDate =
                                    new SearchDate(_thisFiscalHalfToDate,true);
                            
                                public static final SearchDate thisFiscalQuarter =
                                    new SearchDate(_thisFiscalQuarter,true);
                            
                                public static final SearchDate thisFiscalQuarterToDate =
                                    new SearchDate(_thisFiscalQuarterToDate,true);
                            
                                public static final SearchDate thisFiscalYear =
                                    new SearchDate(_thisFiscalYear,true);
                            
                                public static final SearchDate thisFiscalYearToDate =
                                    new SearchDate(_thisFiscalYearToDate,true);
                            
                                public static final SearchDate thisMonth =
                                    new SearchDate(_thisMonth,true);
                            
                                public static final SearchDate thisMonthToDate =
                                    new SearchDate(_thisMonthToDate,true);
                            
                                public static final SearchDate thisRollingHalf =
                                    new SearchDate(_thisRollingHalf,true);
                            
                                public static final SearchDate thisRollingQuarter =
                                    new SearchDate(_thisRollingQuarter,true);
                            
                                public static final SearchDate thisRollingYear =
                                    new SearchDate(_thisRollingYear,true);
                            
                                public static final SearchDate thisWeek =
                                    new SearchDate(_thisWeek,true);
                            
                                public static final SearchDate thisWeekToDate =
                                    new SearchDate(_thisWeekToDate,true);
                            
                                public static final SearchDate thisYear =
                                    new SearchDate(_thisYear,true);
                            
                                public static final SearchDate threeDaysAgo =
                                    new SearchDate(_threeDaysAgo,true);
                            
                                public static final SearchDate threeDaysFromNow =
                                    new SearchDate(_threeDaysFromNow,true);
                            
                                public static final SearchDate threeFiscalQuartersAgo =
                                    new SearchDate(_threeFiscalQuartersAgo,true);
                            
                                public static final SearchDate threeFiscalQuartersAgoToDate =
                                    new SearchDate(_threeFiscalQuartersAgoToDate,true);
                            
                                public static final SearchDate threeFiscalYearsAgo =
                                    new SearchDate(_threeFiscalYearsAgo,true);
                            
                                public static final SearchDate threeFiscalYearsAgoToDate =
                                    new SearchDate(_threeFiscalYearsAgoToDate,true);
                            
                                public static final SearchDate threeMonthsAgo =
                                    new SearchDate(_threeMonthsAgo,true);
                            
                                public static final SearchDate threeMonthsAgoToDate =
                                    new SearchDate(_threeMonthsAgoToDate,true);
                            
                                public static final SearchDate today =
                                    new SearchDate(_today,true);
                            
                                public static final SearchDate tomorrow =
                                    new SearchDate(_tomorrow,true);
                            
                                public static final SearchDate twoDaysAgo =
                                    new SearchDate(_twoDaysAgo,true);
                            
                                public static final SearchDate twoDaysFromNow =
                                    new SearchDate(_twoDaysFromNow,true);
                            
                                public static final SearchDate weekAfterNext =
                                    new SearchDate(_weekAfterNext,true);
                            
                                public static final SearchDate weekAfterNextToDate =
                                    new SearchDate(_weekAfterNextToDate,true);
                            
                                public static final SearchDate weekBeforeLast =
                                    new SearchDate(_weekBeforeLast,true);
                            
                                public static final SearchDate weekBeforeLastToDate =
                                    new SearchDate(_weekBeforeLastToDate,true);
                            
                                public static final SearchDate yesterday =
                                    new SearchDate(_yesterday,true);
                            

                                public java.lang.String getValue() { return localSearchDate;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localSearchDate.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.core_2017_2.platform.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":SearchDate",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "SearchDate",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localSearchDate==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("SearchDate cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localSearchDate);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.core_2017_2.platform.webservices.netsuite.com")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSearchDate)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static SearchDate fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    SearchDate enumeration = (SearchDate)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static SearchDate fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static SearchDate fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return SearchDate.Factory.fromString(content,namespaceUri);
                    } else {
                       return SearchDate.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static SearchDate parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            SearchDate object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"SearchDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = SearchDate.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = SearchDate.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    