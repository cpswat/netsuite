
/**
 * RecordType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.core_2017_2.types;
            

            /**
            *  RecordType bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class RecordType
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.core_2017_2.platform.webservices.netsuite.com",
                "RecordType",
                "ns1");

            

                        /**
                        * field for RecordType
                        */

                        
                                    protected java.lang.String localRecordType ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected RecordType(java.lang.String value, boolean isRegisterValue) {
                                    localRecordType = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localRecordType, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String _account =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("account");
                                
                                    public static final java.lang.String _accountingPeriod =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("accountingPeriod");
                                
                                    public static final java.lang.String _advInterCompanyJournalEntry =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("advInterCompanyJournalEntry");
                                
                                    public static final java.lang.String _assemblyBuild =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("assemblyBuild");
                                
                                    public static final java.lang.String _assemblyUnbuild =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("assemblyUnbuild");
                                
                                    public static final java.lang.String _assemblyItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("assemblyItem");
                                
                                    public static final java.lang.String _billingAccount =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("billingAccount");
                                
                                    public static final java.lang.String _billingSchedule =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("billingSchedule");
                                
                                    public static final java.lang.String _bin =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("bin");
                                
                                    public static final java.lang.String _binTransfer =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("binTransfer");
                                
                                    public static final java.lang.String _binWorksheet =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("binWorksheet");
                                
                                    public static final java.lang.String _budget =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("budget");
                                
                                    public static final java.lang.String _budgetCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("budgetCategory");
                                
                                    public static final java.lang.String _calendarEvent =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("calendarEvent");
                                
                                    public static final java.lang.String _campaign =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("campaign");
                                
                                    public static final java.lang.String _campaignAudience =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("campaignAudience");
                                
                                    public static final java.lang.String _campaignCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("campaignCategory");
                                
                                    public static final java.lang.String _campaignChannel =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("campaignChannel");
                                
                                    public static final java.lang.String _campaignFamily =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("campaignFamily");
                                
                                    public static final java.lang.String _campaignOffer =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("campaignOffer");
                                
                                    public static final java.lang.String _campaignResponse =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("campaignResponse");
                                
                                    public static final java.lang.String _campaignSearchEngine =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("campaignSearchEngine");
                                
                                    public static final java.lang.String _campaignSubscription =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("campaignSubscription");
                                
                                    public static final java.lang.String _campaignVertical =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("campaignVertical");
                                
                                    public static final java.lang.String _cashRefund =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("cashRefund");
                                
                                    public static final java.lang.String _cashSale =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("cashSale");
                                
                                    public static final java.lang.String _check =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("check");
                                
                                    public static final java.lang.String _charge =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("charge");
                                
                                    public static final java.lang.String _classification =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("classification");
                                
                                    public static final java.lang.String _consolidatedExchangeRate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("consolidatedExchangeRate");
                                
                                    public static final java.lang.String _contact =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("contact");
                                
                                    public static final java.lang.String _contactCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("contactCategory");
                                
                                    public static final java.lang.String _contactRole =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("contactRole");
                                
                                    public static final java.lang.String _costCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("costCategory");
                                
                                    public static final java.lang.String _couponCode =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("couponCode");
                                
                                    public static final java.lang.String _creditMemo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("creditMemo");
                                
                                    public static final java.lang.String _crmCustomField =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("crmCustomField");
                                
                                    public static final java.lang.String _currency =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("currency");
                                
                                    public static final java.lang.String _currencyRate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("currencyRate");
                                
                                    public static final java.lang.String _customList =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customList");
                                
                                    public static final java.lang.String _customRecord =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customRecord");
                                
                                    public static final java.lang.String _customRecordCustomField =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customRecordCustomField");
                                
                                    public static final java.lang.String _customRecordType =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customRecordType");
                                
                                    public static final java.lang.String _customTransaction =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customTransaction");
                                
                                    public static final java.lang.String _customTransactionType =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customTransactionType");
                                
                                    public static final java.lang.String _customer =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customer");
                                
                                    public static final java.lang.String _customerCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customerCategory");
                                
                                    public static final java.lang.String _customerDeposit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customerDeposit");
                                
                                    public static final java.lang.String _customerMessage =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customerMessage");
                                
                                    public static final java.lang.String _customerPayment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customerPayment");
                                
                                    public static final java.lang.String _customerRefund =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customerRefund");
                                
                                    public static final java.lang.String _customerStatus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customerStatus");
                                
                                    public static final java.lang.String _deposit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("deposit");
                                
                                    public static final java.lang.String _depositApplication =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("depositApplication");
                                
                                    public static final java.lang.String _department =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("department");
                                
                                    public static final java.lang.String _descriptionItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("descriptionItem");
                                
                                    public static final java.lang.String _discountItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("discountItem");
                                
                                    public static final java.lang.String _downloadItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("downloadItem");
                                
                                    public static final java.lang.String _employee =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("employee");
                                
                                    public static final java.lang.String _entityCustomField =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("entityCustomField");
                                
                                    public static final java.lang.String _entityGroup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("entityGroup");
                                
                                    public static final java.lang.String _estimate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("estimate");
                                
                                    public static final java.lang.String _expenseCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("expenseCategory");
                                
                                    public static final java.lang.String _expenseReport =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("expenseReport");
                                
                                    public static final java.lang.String _fairValuePrice =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("fairValuePrice");
                                
                                    public static final java.lang.String _file =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("file");
                                
                                    public static final java.lang.String _folder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("folder");
                                
                                    public static final java.lang.String _giftCertificate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("giftCertificate");
                                
                                    public static final java.lang.String _giftCertificateItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("giftCertificateItem");
                                
                                    public static final java.lang.String _globalAccountMapping =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("globalAccountMapping");
                                
                                    public static final java.lang.String _hcmJob =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("hcmJob");
                                
                                    public static final java.lang.String _inboundShipment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("inboundShipment");
                                
                                    public static final java.lang.String _interCompanyJournalEntry =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("interCompanyJournalEntry");
                                
                                    public static final java.lang.String _interCompanyTransferOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("interCompanyTransferOrder");
                                
                                    public static final java.lang.String _inventoryAdjustment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("inventoryAdjustment");
                                
                                    public static final java.lang.String _inventoryCostRevaluation =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("inventoryCostRevaluation");
                                
                                    public static final java.lang.String _inventoryItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("inventoryItem");
                                
                                    public static final java.lang.String _inventoryNumber =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("inventoryNumber");
                                
                                    public static final java.lang.String _inventoryTransfer =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("inventoryTransfer");
                                
                                    public static final java.lang.String _invoice =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("invoice");
                                
                                    public static final java.lang.String _itemAccountMapping =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemAccountMapping");
                                
                                    public static final java.lang.String _itemCustomField =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemCustomField");
                                
                                    public static final java.lang.String _itemDemandPlan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemDemandPlan");
                                
                                    public static final java.lang.String _itemFulfillment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemFulfillment");
                                
                                    public static final java.lang.String _itemGroup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemGroup");
                                
                                    public static final java.lang.String _itemNumberCustomField =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemNumberCustomField");
                                
                                    public static final java.lang.String _itemOptionCustomField =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemOptionCustomField");
                                
                                    public static final java.lang.String _itemSupplyPlan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemSupplyPlan");
                                
                                    public static final java.lang.String _itemRevision =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemRevision");
                                
                                    public static final java.lang.String _issue =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("issue");
                                
                                    public static final java.lang.String _job =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("job");
                                
                                    public static final java.lang.String _jobStatus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("jobStatus");
                                
                                    public static final java.lang.String _jobType =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("jobType");
                                
                                    public static final java.lang.String _itemReceipt =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemReceipt");
                                
                                    public static final java.lang.String _journalEntry =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("journalEntry");
                                
                                    public static final java.lang.String _kitItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("kitItem");
                                
                                    public static final java.lang.String _leadSource =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("leadSource");
                                
                                    public static final java.lang.String _location =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("location");
                                
                                    public static final java.lang.String _lotNumberedInventoryItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lotNumberedInventoryItem");
                                
                                    public static final java.lang.String _lotNumberedAssemblyItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lotNumberedAssemblyItem");
                                
                                    public static final java.lang.String _markupItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("markupItem");
                                
                                    public static final java.lang.String _message =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("message");
                                
                                    public static final java.lang.String _manufacturingCostTemplate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("manufacturingCostTemplate");
                                
                                    public static final java.lang.String _manufacturingOperationTask =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("manufacturingOperationTask");
                                
                                    public static final java.lang.String _manufacturingRouting =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("manufacturingRouting");
                                
                                    public static final java.lang.String _nexus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nexus");
                                
                                    public static final java.lang.String _nonInventoryPurchaseItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nonInventoryPurchaseItem");
                                
                                    public static final java.lang.String _nonInventoryResaleItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nonInventoryResaleItem");
                                
                                    public static final java.lang.String _nonInventorySaleItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nonInventorySaleItem");
                                
                                    public static final java.lang.String _note =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("note");
                                
                                    public static final java.lang.String _noteType =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("noteType");
                                
                                    public static final java.lang.String _opportunity =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("opportunity");
                                
                                    public static final java.lang.String _otherChargePurchaseItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("otherChargePurchaseItem");
                                
                                    public static final java.lang.String _otherChargeResaleItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("otherChargeResaleItem");
                                
                                    public static final java.lang.String _otherChargeSaleItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("otherChargeSaleItem");
                                
                                    public static final java.lang.String _otherCustomField =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("otherCustomField");
                                
                                    public static final java.lang.String _otherNameCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("otherNameCategory");
                                
                                    public static final java.lang.String _partner =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("partner");
                                
                                    public static final java.lang.String _partnerCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("partnerCategory");
                                
                                    public static final java.lang.String _paycheck =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("paycheck");
                                
                                    public static final java.lang.String _paycheckJournal =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("paycheckJournal");
                                
                                    public static final java.lang.String _paymentItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("paymentItem");
                                
                                    public static final java.lang.String _paymentMethod =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("paymentMethod");
                                
                                    public static final java.lang.String _payrollItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("payrollItem");
                                
                                    public static final java.lang.String _phoneCall =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("phoneCall");
                                
                                    public static final java.lang.String _priceLevel =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("priceLevel");
                                
                                    public static final java.lang.String _pricingGroup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("pricingGroup");
                                
                                    public static final java.lang.String _projectTask =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("projectTask");
                                
                                    public static final java.lang.String _promotionCode =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("promotionCode");
                                
                                    public static final java.lang.String _purchaseOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("purchaseOrder");
                                
                                    public static final java.lang.String _purchaseRequisition =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("purchaseRequisition");
                                
                                    public static final java.lang.String _resourceAllocation =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("resourceAllocation");
                                
                                    public static final java.lang.String _returnAuthorization =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("returnAuthorization");
                                
                                    public static final java.lang.String _revRecSchedule =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("revRecSchedule");
                                
                                    public static final java.lang.String _revRecTemplate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("revRecTemplate");
                                
                                    public static final java.lang.String _salesOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("salesOrder");
                                
                                    public static final java.lang.String _salesRole =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("salesRole");
                                
                                    public static final java.lang.String _salesTaxItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("salesTaxItem");
                                
                                    public static final java.lang.String _serializedInventoryItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("serializedInventoryItem");
                                
                                    public static final java.lang.String _serializedAssemblyItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("serializedAssemblyItem");
                                
                                    public static final java.lang.String _servicePurchaseItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("servicePurchaseItem");
                                
                                    public static final java.lang.String _serviceResaleItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("serviceResaleItem");
                                
                                    public static final java.lang.String _serviceSaleItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("serviceSaleItem");
                                
                                    public static final java.lang.String _solution =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("solution");
                                
                                    public static final java.lang.String _siteCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("siteCategory");
                                
                                    public static final java.lang.String _state =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("state");
                                
                                    public static final java.lang.String _statisticalJournalEntry =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("statisticalJournalEntry");
                                
                                    public static final java.lang.String _subsidiary =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("subsidiary");
                                
                                    public static final java.lang.String _subtotalItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("subtotalItem");
                                
                                    public static final java.lang.String _supportCase =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("supportCase");
                                
                                    public static final java.lang.String _supportCaseIssue =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("supportCaseIssue");
                                
                                    public static final java.lang.String _supportCaseOrigin =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("supportCaseOrigin");
                                
                                    public static final java.lang.String _supportCasePriority =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("supportCasePriority");
                                
                                    public static final java.lang.String _supportCaseStatus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("supportCaseStatus");
                                
                                    public static final java.lang.String _supportCaseType =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("supportCaseType");
                                
                                    public static final java.lang.String _task =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("task");
                                
                                    public static final java.lang.String _taxAcct =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("taxAcct");
                                
                                    public static final java.lang.String _taxGroup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("taxGroup");
                                
                                    public static final java.lang.String _taxType =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("taxType");
                                
                                    public static final java.lang.String _term =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("term");
                                
                                    public static final java.lang.String _timeBill =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("timeBill");
                                
                                    public static final java.lang.String _timeSheet =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("timeSheet");
                                
                                    public static final java.lang.String _topic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("topic");
                                
                                    public static final java.lang.String _transferOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("transferOrder");
                                
                                    public static final java.lang.String _transactionBodyCustomField =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("transactionBodyCustomField");
                                
                                    public static final java.lang.String _transactionColumnCustomField =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("transactionColumnCustomField");
                                
                                    public static final java.lang.String _unitsType =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("unitsType");
                                
                                    public static final java.lang.String _usage =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("usage");
                                
                                    public static final java.lang.String _vendor =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("vendor");
                                
                                    public static final java.lang.String _vendorCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("vendorCategory");
                                
                                    public static final java.lang.String _vendorBill =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("vendorBill");
                                
                                    public static final java.lang.String _vendorCredit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("vendorCredit");
                                
                                    public static final java.lang.String _vendorPayment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("vendorPayment");
                                
                                    public static final java.lang.String _vendorReturnAuthorization =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("vendorReturnAuthorization");
                                
                                    public static final java.lang.String _winLossReason =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("winLossReason");
                                
                                    public static final java.lang.String _workOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("workOrder");
                                
                                    public static final java.lang.String _workOrderIssue =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("workOrderIssue");
                                
                                    public static final java.lang.String _workOrderCompletion =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("workOrderCompletion");
                                
                                    public static final java.lang.String _workOrderClose =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("workOrderClose");
                                
                                public static final RecordType account =
                                    new RecordType(_account,true);
                            
                                public static final RecordType accountingPeriod =
                                    new RecordType(_accountingPeriod,true);
                            
                                public static final RecordType advInterCompanyJournalEntry =
                                    new RecordType(_advInterCompanyJournalEntry,true);
                            
                                public static final RecordType assemblyBuild =
                                    new RecordType(_assemblyBuild,true);
                            
                                public static final RecordType assemblyUnbuild =
                                    new RecordType(_assemblyUnbuild,true);
                            
                                public static final RecordType assemblyItem =
                                    new RecordType(_assemblyItem,true);
                            
                                public static final RecordType billingAccount =
                                    new RecordType(_billingAccount,true);
                            
                                public static final RecordType billingSchedule =
                                    new RecordType(_billingSchedule,true);
                            
                                public static final RecordType bin =
                                    new RecordType(_bin,true);
                            
                                public static final RecordType binTransfer =
                                    new RecordType(_binTransfer,true);
                            
                                public static final RecordType binWorksheet =
                                    new RecordType(_binWorksheet,true);
                            
                                public static final RecordType budget =
                                    new RecordType(_budget,true);
                            
                                public static final RecordType budgetCategory =
                                    new RecordType(_budgetCategory,true);
                            
                                public static final RecordType calendarEvent =
                                    new RecordType(_calendarEvent,true);
                            
                                public static final RecordType campaign =
                                    new RecordType(_campaign,true);
                            
                                public static final RecordType campaignAudience =
                                    new RecordType(_campaignAudience,true);
                            
                                public static final RecordType campaignCategory =
                                    new RecordType(_campaignCategory,true);
                            
                                public static final RecordType campaignChannel =
                                    new RecordType(_campaignChannel,true);
                            
                                public static final RecordType campaignFamily =
                                    new RecordType(_campaignFamily,true);
                            
                                public static final RecordType campaignOffer =
                                    new RecordType(_campaignOffer,true);
                            
                                public static final RecordType campaignResponse =
                                    new RecordType(_campaignResponse,true);
                            
                                public static final RecordType campaignSearchEngine =
                                    new RecordType(_campaignSearchEngine,true);
                            
                                public static final RecordType campaignSubscription =
                                    new RecordType(_campaignSubscription,true);
                            
                                public static final RecordType campaignVertical =
                                    new RecordType(_campaignVertical,true);
                            
                                public static final RecordType cashRefund =
                                    new RecordType(_cashRefund,true);
                            
                                public static final RecordType cashSale =
                                    new RecordType(_cashSale,true);
                            
                                public static final RecordType check =
                                    new RecordType(_check,true);
                            
                                public static final RecordType charge =
                                    new RecordType(_charge,true);
                            
                                public static final RecordType classification =
                                    new RecordType(_classification,true);
                            
                                public static final RecordType consolidatedExchangeRate =
                                    new RecordType(_consolidatedExchangeRate,true);
                            
                                public static final RecordType contact =
                                    new RecordType(_contact,true);
                            
                                public static final RecordType contactCategory =
                                    new RecordType(_contactCategory,true);
                            
                                public static final RecordType contactRole =
                                    new RecordType(_contactRole,true);
                            
                                public static final RecordType costCategory =
                                    new RecordType(_costCategory,true);
                            
                                public static final RecordType couponCode =
                                    new RecordType(_couponCode,true);
                            
                                public static final RecordType creditMemo =
                                    new RecordType(_creditMemo,true);
                            
                                public static final RecordType crmCustomField =
                                    new RecordType(_crmCustomField,true);
                            
                                public static final RecordType currency =
                                    new RecordType(_currency,true);
                            
                                public static final RecordType currencyRate =
                                    new RecordType(_currencyRate,true);
                            
                                public static final RecordType customList =
                                    new RecordType(_customList,true);
                            
                                public static final RecordType customRecord =
                                    new RecordType(_customRecord,true);
                            
                                public static final RecordType customRecordCustomField =
                                    new RecordType(_customRecordCustomField,true);
                            
                                public static final RecordType customRecordType =
                                    new RecordType(_customRecordType,true);
                            
                                public static final RecordType customTransaction =
                                    new RecordType(_customTransaction,true);
                            
                                public static final RecordType customTransactionType =
                                    new RecordType(_customTransactionType,true);
                            
                                public static final RecordType customer =
                                    new RecordType(_customer,true);
                            
                                public static final RecordType customerCategory =
                                    new RecordType(_customerCategory,true);
                            
                                public static final RecordType customerDeposit =
                                    new RecordType(_customerDeposit,true);
                            
                                public static final RecordType customerMessage =
                                    new RecordType(_customerMessage,true);
                            
                                public static final RecordType customerPayment =
                                    new RecordType(_customerPayment,true);
                            
                                public static final RecordType customerRefund =
                                    new RecordType(_customerRefund,true);
                            
                                public static final RecordType customerStatus =
                                    new RecordType(_customerStatus,true);
                            
                                public static final RecordType deposit =
                                    new RecordType(_deposit,true);
                            
                                public static final RecordType depositApplication =
                                    new RecordType(_depositApplication,true);
                            
                                public static final RecordType department =
                                    new RecordType(_department,true);
                            
                                public static final RecordType descriptionItem =
                                    new RecordType(_descriptionItem,true);
                            
                                public static final RecordType discountItem =
                                    new RecordType(_discountItem,true);
                            
                                public static final RecordType downloadItem =
                                    new RecordType(_downloadItem,true);
                            
                                public static final RecordType employee =
                                    new RecordType(_employee,true);
                            
                                public static final RecordType entityCustomField =
                                    new RecordType(_entityCustomField,true);
                            
                                public static final RecordType entityGroup =
                                    new RecordType(_entityGroup,true);
                            
                                public static final RecordType estimate =
                                    new RecordType(_estimate,true);
                            
                                public static final RecordType expenseCategory =
                                    new RecordType(_expenseCategory,true);
                            
                                public static final RecordType expenseReport =
                                    new RecordType(_expenseReport,true);
                            
                                public static final RecordType fairValuePrice =
                                    new RecordType(_fairValuePrice,true);
                            
                                public static final RecordType file =
                                    new RecordType(_file,true);
                            
                                public static final RecordType folder =
                                    new RecordType(_folder,true);
                            
                                public static final RecordType giftCertificate =
                                    new RecordType(_giftCertificate,true);
                            
                                public static final RecordType giftCertificateItem =
                                    new RecordType(_giftCertificateItem,true);
                            
                                public static final RecordType globalAccountMapping =
                                    new RecordType(_globalAccountMapping,true);
                            
                                public static final RecordType hcmJob =
                                    new RecordType(_hcmJob,true);
                            
                                public static final RecordType inboundShipment =
                                    new RecordType(_inboundShipment,true);
                            
                                public static final RecordType interCompanyJournalEntry =
                                    new RecordType(_interCompanyJournalEntry,true);
                            
                                public static final RecordType interCompanyTransferOrder =
                                    new RecordType(_interCompanyTransferOrder,true);
                            
                                public static final RecordType inventoryAdjustment =
                                    new RecordType(_inventoryAdjustment,true);
                            
                                public static final RecordType inventoryCostRevaluation =
                                    new RecordType(_inventoryCostRevaluation,true);
                            
                                public static final RecordType inventoryItem =
                                    new RecordType(_inventoryItem,true);
                            
                                public static final RecordType inventoryNumber =
                                    new RecordType(_inventoryNumber,true);
                            
                                public static final RecordType inventoryTransfer =
                                    new RecordType(_inventoryTransfer,true);
                            
                                public static final RecordType invoice =
                                    new RecordType(_invoice,true);
                            
                                public static final RecordType itemAccountMapping =
                                    new RecordType(_itemAccountMapping,true);
                            
                                public static final RecordType itemCustomField =
                                    new RecordType(_itemCustomField,true);
                            
                                public static final RecordType itemDemandPlan =
                                    new RecordType(_itemDemandPlan,true);
                            
                                public static final RecordType itemFulfillment =
                                    new RecordType(_itemFulfillment,true);
                            
                                public static final RecordType itemGroup =
                                    new RecordType(_itemGroup,true);
                            
                                public static final RecordType itemNumberCustomField =
                                    new RecordType(_itemNumberCustomField,true);
                            
                                public static final RecordType itemOptionCustomField =
                                    new RecordType(_itemOptionCustomField,true);
                            
                                public static final RecordType itemSupplyPlan =
                                    new RecordType(_itemSupplyPlan,true);
                            
                                public static final RecordType itemRevision =
                                    new RecordType(_itemRevision,true);
                            
                                public static final RecordType issue =
                                    new RecordType(_issue,true);
                            
                                public static final RecordType job =
                                    new RecordType(_job,true);
                            
                                public static final RecordType jobStatus =
                                    new RecordType(_jobStatus,true);
                            
                                public static final RecordType jobType =
                                    new RecordType(_jobType,true);
                            
                                public static final RecordType itemReceipt =
                                    new RecordType(_itemReceipt,true);
                            
                                public static final RecordType journalEntry =
                                    new RecordType(_journalEntry,true);
                            
                                public static final RecordType kitItem =
                                    new RecordType(_kitItem,true);
                            
                                public static final RecordType leadSource =
                                    new RecordType(_leadSource,true);
                            
                                public static final RecordType location =
                                    new RecordType(_location,true);
                            
                                public static final RecordType lotNumberedInventoryItem =
                                    new RecordType(_lotNumberedInventoryItem,true);
                            
                                public static final RecordType lotNumberedAssemblyItem =
                                    new RecordType(_lotNumberedAssemblyItem,true);
                            
                                public static final RecordType markupItem =
                                    new RecordType(_markupItem,true);
                            
                                public static final RecordType message =
                                    new RecordType(_message,true);
                            
                                public static final RecordType manufacturingCostTemplate =
                                    new RecordType(_manufacturingCostTemplate,true);
                            
                                public static final RecordType manufacturingOperationTask =
                                    new RecordType(_manufacturingOperationTask,true);
                            
                                public static final RecordType manufacturingRouting =
                                    new RecordType(_manufacturingRouting,true);
                            
                                public static final RecordType nexus =
                                    new RecordType(_nexus,true);
                            
                                public static final RecordType nonInventoryPurchaseItem =
                                    new RecordType(_nonInventoryPurchaseItem,true);
                            
                                public static final RecordType nonInventoryResaleItem =
                                    new RecordType(_nonInventoryResaleItem,true);
                            
                                public static final RecordType nonInventorySaleItem =
                                    new RecordType(_nonInventorySaleItem,true);
                            
                                public static final RecordType note =
                                    new RecordType(_note,true);
                            
                                public static final RecordType noteType =
                                    new RecordType(_noteType,true);
                            
                                public static final RecordType opportunity =
                                    new RecordType(_opportunity,true);
                            
                                public static final RecordType otherChargePurchaseItem =
                                    new RecordType(_otherChargePurchaseItem,true);
                            
                                public static final RecordType otherChargeResaleItem =
                                    new RecordType(_otherChargeResaleItem,true);
                            
                                public static final RecordType otherChargeSaleItem =
                                    new RecordType(_otherChargeSaleItem,true);
                            
                                public static final RecordType otherCustomField =
                                    new RecordType(_otherCustomField,true);
                            
                                public static final RecordType otherNameCategory =
                                    new RecordType(_otherNameCategory,true);
                            
                                public static final RecordType partner =
                                    new RecordType(_partner,true);
                            
                                public static final RecordType partnerCategory =
                                    new RecordType(_partnerCategory,true);
                            
                                public static final RecordType paycheck =
                                    new RecordType(_paycheck,true);
                            
                                public static final RecordType paycheckJournal =
                                    new RecordType(_paycheckJournal,true);
                            
                                public static final RecordType paymentItem =
                                    new RecordType(_paymentItem,true);
                            
                                public static final RecordType paymentMethod =
                                    new RecordType(_paymentMethod,true);
                            
                                public static final RecordType payrollItem =
                                    new RecordType(_payrollItem,true);
                            
                                public static final RecordType phoneCall =
                                    new RecordType(_phoneCall,true);
                            
                                public static final RecordType priceLevel =
                                    new RecordType(_priceLevel,true);
                            
                                public static final RecordType pricingGroup =
                                    new RecordType(_pricingGroup,true);
                            
                                public static final RecordType projectTask =
                                    new RecordType(_projectTask,true);
                            
                                public static final RecordType promotionCode =
                                    new RecordType(_promotionCode,true);
                            
                                public static final RecordType purchaseOrder =
                                    new RecordType(_purchaseOrder,true);
                            
                                public static final RecordType purchaseRequisition =
                                    new RecordType(_purchaseRequisition,true);
                            
                                public static final RecordType resourceAllocation =
                                    new RecordType(_resourceAllocation,true);
                            
                                public static final RecordType returnAuthorization =
                                    new RecordType(_returnAuthorization,true);
                            
                                public static final RecordType revRecSchedule =
                                    new RecordType(_revRecSchedule,true);
                            
                                public static final RecordType revRecTemplate =
                                    new RecordType(_revRecTemplate,true);
                            
                                public static final RecordType salesOrder =
                                    new RecordType(_salesOrder,true);
                            
                                public static final RecordType salesRole =
                                    new RecordType(_salesRole,true);
                            
                                public static final RecordType salesTaxItem =
                                    new RecordType(_salesTaxItem,true);
                            
                                public static final RecordType serializedInventoryItem =
                                    new RecordType(_serializedInventoryItem,true);
                            
                                public static final RecordType serializedAssemblyItem =
                                    new RecordType(_serializedAssemblyItem,true);
                            
                                public static final RecordType servicePurchaseItem =
                                    new RecordType(_servicePurchaseItem,true);
                            
                                public static final RecordType serviceResaleItem =
                                    new RecordType(_serviceResaleItem,true);
                            
                                public static final RecordType serviceSaleItem =
                                    new RecordType(_serviceSaleItem,true);
                            
                                public static final RecordType solution =
                                    new RecordType(_solution,true);
                            
                                public static final RecordType siteCategory =
                                    new RecordType(_siteCategory,true);
                            
                                public static final RecordType state =
                                    new RecordType(_state,true);
                            
                                public static final RecordType statisticalJournalEntry =
                                    new RecordType(_statisticalJournalEntry,true);
                            
                                public static final RecordType subsidiary =
                                    new RecordType(_subsidiary,true);
                            
                                public static final RecordType subtotalItem =
                                    new RecordType(_subtotalItem,true);
                            
                                public static final RecordType supportCase =
                                    new RecordType(_supportCase,true);
                            
                                public static final RecordType supportCaseIssue =
                                    new RecordType(_supportCaseIssue,true);
                            
                                public static final RecordType supportCaseOrigin =
                                    new RecordType(_supportCaseOrigin,true);
                            
                                public static final RecordType supportCasePriority =
                                    new RecordType(_supportCasePriority,true);
                            
                                public static final RecordType supportCaseStatus =
                                    new RecordType(_supportCaseStatus,true);
                            
                                public static final RecordType supportCaseType =
                                    new RecordType(_supportCaseType,true);
                            
                                public static final RecordType task =
                                    new RecordType(_task,true);
                            
                                public static final RecordType taxAcct =
                                    new RecordType(_taxAcct,true);
                            
                                public static final RecordType taxGroup =
                                    new RecordType(_taxGroup,true);
                            
                                public static final RecordType taxType =
                                    new RecordType(_taxType,true);
                            
                                public static final RecordType term =
                                    new RecordType(_term,true);
                            
                                public static final RecordType timeBill =
                                    new RecordType(_timeBill,true);
                            
                                public static final RecordType timeSheet =
                                    new RecordType(_timeSheet,true);
                            
                                public static final RecordType topic =
                                    new RecordType(_topic,true);
                            
                                public static final RecordType transferOrder =
                                    new RecordType(_transferOrder,true);
                            
                                public static final RecordType transactionBodyCustomField =
                                    new RecordType(_transactionBodyCustomField,true);
                            
                                public static final RecordType transactionColumnCustomField =
                                    new RecordType(_transactionColumnCustomField,true);
                            
                                public static final RecordType unitsType =
                                    new RecordType(_unitsType,true);
                            
                                public static final RecordType usage =
                                    new RecordType(_usage,true);
                            
                                public static final RecordType vendor =
                                    new RecordType(_vendor,true);
                            
                                public static final RecordType vendorCategory =
                                    new RecordType(_vendorCategory,true);
                            
                                public static final RecordType vendorBill =
                                    new RecordType(_vendorBill,true);
                            
                                public static final RecordType vendorCredit =
                                    new RecordType(_vendorCredit,true);
                            
                                public static final RecordType vendorPayment =
                                    new RecordType(_vendorPayment,true);
                            
                                public static final RecordType vendorReturnAuthorization =
                                    new RecordType(_vendorReturnAuthorization,true);
                            
                                public static final RecordType winLossReason =
                                    new RecordType(_winLossReason,true);
                            
                                public static final RecordType workOrder =
                                    new RecordType(_workOrder,true);
                            
                                public static final RecordType workOrderIssue =
                                    new RecordType(_workOrderIssue,true);
                            
                                public static final RecordType workOrderCompletion =
                                    new RecordType(_workOrderCompletion,true);
                            
                                public static final RecordType workOrderClose =
                                    new RecordType(_workOrderClose,true);
                            

                                public java.lang.String getValue() { return localRecordType;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localRecordType.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.core_2017_2.platform.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":RecordType",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "RecordType",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localRecordType==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("RecordType cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localRecordType);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.core_2017_2.platform.webservices.netsuite.com")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecordType)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static RecordType fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    RecordType enumeration = (RecordType)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static RecordType fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static RecordType fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return RecordType.Factory.fromString(content,namespaceUri);
                    } else {
                       return RecordType.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static RecordType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            RecordType object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"RecordType" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = RecordType.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = RecordType.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    