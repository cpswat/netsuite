
/**
 * ItemAvailability.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.core_2017_2;
            

            /**
            *  ItemAvailability bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ItemAvailability
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ItemAvailability
                Namespace URI = urn:core_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns3
                */
            

                        /**
                        * field for Item
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localItem ;
                                

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getItem(){
                               return localItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Item
                               */
                               public void setItem(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            
                                            this.localItem=param;
                                    

                               }
                            

                        /**
                        * field for LastQtyAvailableChange
                        */

                        
                                    protected java.util.Calendar localLastQtyAvailableChange ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastQtyAvailableChangeTracker = false ;

                           public boolean isLastQtyAvailableChangeSpecified(){
                               return localLastQtyAvailableChangeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastQtyAvailableChange(){
                               return localLastQtyAvailableChange;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastQtyAvailableChange
                               */
                               public void setLastQtyAvailableChange(java.util.Calendar param){
                            localLastQtyAvailableChangeTracker = param != null;
                                   
                                            this.localLastQtyAvailableChange=param;
                                    

                               }
                            

                        /**
                        * field for LocationId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localLocationId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationIdTracker = false ;

                           public boolean isLocationIdSpecified(){
                               return localLocationIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getLocationId(){
                               return localLocationId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationId
                               */
                               public void setLocationId(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localLocationIdTracker = param != null;
                                   
                                            this.localLocationId=param;
                                    

                               }
                            

                        /**
                        * field for QuantityOnHand
                        */

                        
                                    protected double localQuantityOnHand ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityOnHandTracker = false ;

                           public boolean isQuantityOnHandSpecified(){
                               return localQuantityOnHandTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityOnHand(){
                               return localQuantityOnHand;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityOnHand
                               */
                               public void setQuantityOnHand(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityOnHandTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityOnHand=param;
                                    

                               }
                            

                        /**
                        * field for OnHandValueMli
                        */

                        
                                    protected double localOnHandValueMli ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOnHandValueMliTracker = false ;

                           public boolean isOnHandValueMliSpecified(){
                               return localOnHandValueMliTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getOnHandValueMli(){
                               return localOnHandValueMli;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OnHandValueMli
                               */
                               public void setOnHandValueMli(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localOnHandValueMliTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localOnHandValueMli=param;
                                    

                               }
                            

                        /**
                        * field for ReorderPoint
                        */

                        
                                    protected double localReorderPoint ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReorderPointTracker = false ;

                           public boolean isReorderPointSpecified(){
                               return localReorderPointTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getReorderPoint(){
                               return localReorderPoint;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReorderPoint
                               */
                               public void setReorderPoint(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localReorderPointTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localReorderPoint=param;
                                    

                               }
                            

                        /**
                        * field for PreferredStockLevel
                        */

                        
                                    protected double localPreferredStockLevel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPreferredStockLevelTracker = false ;

                           public boolean isPreferredStockLevelSpecified(){
                               return localPreferredStockLevelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPreferredStockLevel(){
                               return localPreferredStockLevel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PreferredStockLevel
                               */
                               public void setPreferredStockLevel(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPreferredStockLevelTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPreferredStockLevel=param;
                                    

                               }
                            

                        /**
                        * field for QuantityOnOrder
                        */

                        
                                    protected double localQuantityOnOrder ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityOnOrderTracker = false ;

                           public boolean isQuantityOnOrderSpecified(){
                               return localQuantityOnOrderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityOnOrder(){
                               return localQuantityOnOrder;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityOnOrder
                               */
                               public void setQuantityOnOrder(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityOnOrderTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityOnOrder=param;
                                    

                               }
                            

                        /**
                        * field for QuantityCommitted
                        */

                        
                                    protected double localQuantityCommitted ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityCommittedTracker = false ;

                           public boolean isQuantityCommittedSpecified(){
                               return localQuantityCommittedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityCommitted(){
                               return localQuantityCommitted;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityCommitted
                               */
                               public void setQuantityCommitted(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityCommittedTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityCommitted=param;
                                    

                               }
                            

                        /**
                        * field for QuantityBackOrdered
                        */

                        
                                    protected double localQuantityBackOrdered ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityBackOrderedTracker = false ;

                           public boolean isQuantityBackOrderedSpecified(){
                               return localQuantityBackOrderedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityBackOrdered(){
                               return localQuantityBackOrdered;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityBackOrdered
                               */
                               public void setQuantityBackOrdered(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityBackOrderedTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityBackOrdered=param;
                                    

                               }
                            

                        /**
                        * field for QuantityAvailable
                        */

                        
                                    protected double localQuantityAvailable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityAvailableTracker = false ;

                           public boolean isQuantityAvailableSpecified(){
                               return localQuantityAvailableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getQuantityAvailable(){
                               return localQuantityAvailable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityAvailable
                               */
                               public void setQuantityAvailable(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localQuantityAvailableTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localQuantityAvailable=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:core_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ItemAvailability",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ItemAvailability",
                           xmlWriter);
                   }

               
                   }
               
                                            if (localItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                            }
                                           localItem.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","item"),
                                               xmlWriter);
                                         if (localLastQtyAvailableChangeTracker){
                                    namespace = "urn:core_2017_2.platform.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastQtyAvailableChange", xmlWriter);
                             

                                          if (localLastQtyAvailableChange==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastQtyAvailableChange cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastQtyAvailableChange));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationIdTracker){
                                            if (localLocationId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("locationId cannot be null!!");
                                            }
                                           localLocationId.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","locationId"),
                                               xmlWriter);
                                        } if (localQuantityOnHandTracker){
                                    namespace = "urn:core_2017_2.platform.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityOnHand", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityOnHand)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityOnHand cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityOnHand));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOnHandValueMliTracker){
                                    namespace = "urn:core_2017_2.platform.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "onHandValueMli", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localOnHandValueMli)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("onHandValueMli cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOnHandValueMli));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localReorderPointTracker){
                                    namespace = "urn:core_2017_2.platform.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "reorderPoint", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localReorderPoint)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("reorderPoint cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReorderPoint));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPreferredStockLevelTracker){
                                    namespace = "urn:core_2017_2.platform.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "preferredStockLevel", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPreferredStockLevel)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("preferredStockLevel cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPreferredStockLevel));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityOnOrderTracker){
                                    namespace = "urn:core_2017_2.platform.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityOnOrder", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityOnOrder)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityOnOrder cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityOnOrder));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityCommittedTracker){
                                    namespace = "urn:core_2017_2.platform.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityCommitted", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityCommitted)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityCommitted cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityCommitted));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityBackOrderedTracker){
                                    namespace = "urn:core_2017_2.platform.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityBackOrdered", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityBackOrdered)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityBackOrdered cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityBackOrdered));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localQuantityAvailableTracker){
                                    namespace = "urn:core_2017_2.platform.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "quantityAvailable", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localQuantityAvailable)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("quantityAvailable cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityAvailable));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:core_2017_2.platform.webservices.netsuite.com")){
                return "ns3";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "item"));
                            
                            
                                    if (localItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                    }
                                    elementList.add(localItem);
                                 if (localLastQtyAvailableChangeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "lastQtyAvailableChange"));
                                 
                                        if (localLastQtyAvailableChange != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastQtyAvailableChange));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastQtyAvailableChange cannot be null!!");
                                        }
                                    } if (localLocationIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "locationId"));
                            
                            
                                    if (localLocationId==null){
                                         throw new org.apache.axis2.databinding.ADBException("locationId cannot be null!!");
                                    }
                                    elementList.add(localLocationId);
                                } if (localQuantityOnHandTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "quantityOnHand"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityOnHand));
                            } if (localOnHandValueMliTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "onHandValueMli"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOnHandValueMli));
                            } if (localReorderPointTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "reorderPoint"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localReorderPoint));
                            } if (localPreferredStockLevelTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "preferredStockLevel"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPreferredStockLevel));
                            } if (localQuantityOnOrderTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "quantityOnOrder"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityOnOrder));
                            } if (localQuantityCommittedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "quantityCommitted"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityCommitted));
                            } if (localQuantityBackOrderedTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "quantityBackOrdered"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityBackOrdered));
                            } if (localQuantityAvailableTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "quantityAvailable"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localQuantityAvailable));
                            }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ItemAvailability parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ItemAvailability object =
                new ItemAvailability();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ItemAvailability".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ItemAvailability)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","item").equals(reader.getName())){
                                
                                                object.setItem(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                else{
                                    // A start element we are not expecting indicates an invalid parameter was passed
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                                }
                            
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","lastQtyAvailableChange").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastQtyAvailableChange" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastQtyAvailableChange(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","locationId").equals(reader.getName())){
                                
                                                object.setLocationId(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","quantityOnHand").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityOnHand" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityOnHand(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityOnHand(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","onHandValueMli").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"onHandValueMli" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOnHandValueMli(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setOnHandValueMli(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","reorderPoint").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"reorderPoint" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setReorderPoint(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setReorderPoint(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","preferredStockLevel").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"preferredStockLevel" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPreferredStockLevel(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPreferredStockLevel(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","quantityOnOrder").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityOnOrder" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityOnOrder(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityOnOrder(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","quantityCommitted").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityCommitted" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityCommitted(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityCommitted(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","quantityBackOrdered").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityBackOrdered" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityBackOrdered(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityBackOrdered(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","quantityAvailable").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"quantityAvailable" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setQuantityAvailable(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setQuantityAvailable(java.lang.Double.NaN);
                                           
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    