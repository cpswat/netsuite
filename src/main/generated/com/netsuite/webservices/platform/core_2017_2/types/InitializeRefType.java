
/**
 * InitializeRefType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.core_2017_2.types;
            

            /**
            *  InitializeRefType bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class InitializeRefType
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.core_2017_2.platform.webservices.netsuite.com",
                "InitializeRefType",
                "ns1");

            

                        /**
                        * field for InitializeRefType
                        */

                        
                                    protected java.lang.String localInitializeRefType ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected InitializeRefType(java.lang.String value, boolean isRegisterValue) {
                                    localInitializeRefType = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localInitializeRefType, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String _assemblyItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("assemblyItem");
                                
                                    public static final java.lang.String _assemblyBuild =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("assemblyBuild");
                                
                                    public static final java.lang.String _cashSale =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("cashSale");
                                
                                    public static final java.lang.String _creditMemo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("creditMemo");
                                
                                    public static final java.lang.String _customer =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customer");
                                
                                    public static final java.lang.String _customerDeposit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customerDeposit");
                                
                                    public static final java.lang.String _employee =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("employee");
                                
                                    public static final java.lang.String _estimate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("estimate");
                                
                                    public static final java.lang.String _interCompanyTransferOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("interCompanyTransferOrder");
                                
                                    public static final java.lang.String _invoice =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("invoice");
                                
                                    public static final java.lang.String _location =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("location");
                                
                                    public static final java.lang.String _lotNumberedAssemblyItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("lotNumberedAssemblyItem");
                                
                                    public static final java.lang.String _opportunity =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("opportunity");
                                
                                    public static final java.lang.String _purchaseOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("purchaseOrder");
                                
                                    public static final java.lang.String _purchaseRequisition =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("purchaseRequisition");
                                
                                    public static final java.lang.String _returnAuthorization =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("returnAuthorization");
                                
                                    public static final java.lang.String _salesOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("salesOrder");
                                
                                    public static final java.lang.String _serializedAssemblyItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("serializedAssemblyItem");
                                
                                    public static final java.lang.String _transferOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("transferOrder");
                                
                                    public static final java.lang.String _vendor =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("vendor");
                                
                                    public static final java.lang.String _vendorBill =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("vendorBill");
                                
                                    public static final java.lang.String _vendorReturnAuthorization =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("vendorReturnAuthorization");
                                
                                    public static final java.lang.String _workOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("workOrder");
                                
                                public static final InitializeRefType assemblyItem =
                                    new InitializeRefType(_assemblyItem,true);
                            
                                public static final InitializeRefType assemblyBuild =
                                    new InitializeRefType(_assemblyBuild,true);
                            
                                public static final InitializeRefType cashSale =
                                    new InitializeRefType(_cashSale,true);
                            
                                public static final InitializeRefType creditMemo =
                                    new InitializeRefType(_creditMemo,true);
                            
                                public static final InitializeRefType customer =
                                    new InitializeRefType(_customer,true);
                            
                                public static final InitializeRefType customerDeposit =
                                    new InitializeRefType(_customerDeposit,true);
                            
                                public static final InitializeRefType employee =
                                    new InitializeRefType(_employee,true);
                            
                                public static final InitializeRefType estimate =
                                    new InitializeRefType(_estimate,true);
                            
                                public static final InitializeRefType interCompanyTransferOrder =
                                    new InitializeRefType(_interCompanyTransferOrder,true);
                            
                                public static final InitializeRefType invoice =
                                    new InitializeRefType(_invoice,true);
                            
                                public static final InitializeRefType location =
                                    new InitializeRefType(_location,true);
                            
                                public static final InitializeRefType lotNumberedAssemblyItem =
                                    new InitializeRefType(_lotNumberedAssemblyItem,true);
                            
                                public static final InitializeRefType opportunity =
                                    new InitializeRefType(_opportunity,true);
                            
                                public static final InitializeRefType purchaseOrder =
                                    new InitializeRefType(_purchaseOrder,true);
                            
                                public static final InitializeRefType purchaseRequisition =
                                    new InitializeRefType(_purchaseRequisition,true);
                            
                                public static final InitializeRefType returnAuthorization =
                                    new InitializeRefType(_returnAuthorization,true);
                            
                                public static final InitializeRefType salesOrder =
                                    new InitializeRefType(_salesOrder,true);
                            
                                public static final InitializeRefType serializedAssemblyItem =
                                    new InitializeRefType(_serializedAssemblyItem,true);
                            
                                public static final InitializeRefType transferOrder =
                                    new InitializeRefType(_transferOrder,true);
                            
                                public static final InitializeRefType vendor =
                                    new InitializeRefType(_vendor,true);
                            
                                public static final InitializeRefType vendorBill =
                                    new InitializeRefType(_vendorBill,true);
                            
                                public static final InitializeRefType vendorReturnAuthorization =
                                    new InitializeRefType(_vendorReturnAuthorization,true);
                            
                                public static final InitializeRefType workOrder =
                                    new InitializeRefType(_workOrder,true);
                            

                                public java.lang.String getValue() { return localInitializeRefType;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localInitializeRefType.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.core_2017_2.platform.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":InitializeRefType",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "InitializeRefType",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localInitializeRefType==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("InitializeRefType cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localInitializeRefType);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.core_2017_2.platform.webservices.netsuite.com")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInitializeRefType)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static InitializeRefType fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    InitializeRefType enumeration = (InitializeRefType)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static InitializeRefType fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static InitializeRefType fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return InitializeRefType.Factory.fromString(content,namespaceUri);
                    } else {
                       return InitializeRefType.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static InitializeRefType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            InitializeRefType object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"InitializeRefType" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = InitializeRefType.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = InitializeRefType.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    