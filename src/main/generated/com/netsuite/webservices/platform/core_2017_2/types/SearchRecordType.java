
/**
 * SearchRecordType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.core_2017_2.types;
            

            /**
            *  SearchRecordType bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class SearchRecordType
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.core_2017_2.platform.webservices.netsuite.com",
                "SearchRecordType",
                "ns1");

            

                        /**
                        * field for SearchRecordType
                        */

                        
                                    protected java.lang.String localSearchRecordType ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected SearchRecordType(java.lang.String value, boolean isRegisterValue) {
                                    localSearchRecordType = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localSearchRecordType, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String _account =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("account");
                                
                                    public static final java.lang.String _accountingPeriod =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("accountingPeriod");
                                
                                    public static final java.lang.String _accountingTransaction =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("accountingTransaction");
                                
                                    public static final java.lang.String _billingAccount =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("billingAccount");
                                
                                    public static final java.lang.String _billingSchedule =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("billingSchedule");
                                
                                    public static final java.lang.String _bin =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("bin");
                                
                                    public static final java.lang.String _budget =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("budget");
                                
                                    public static final java.lang.String _calendarEvent =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("calendarEvent");
                                
                                    public static final java.lang.String _campaign =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("campaign");
                                
                                    public static final java.lang.String _charge =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("charge");
                                
                                    public static final java.lang.String _classification =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("classification");
                                
                                    public static final java.lang.String _contact =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("contact");
                                
                                    public static final java.lang.String _contactCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("contactCategory");
                                
                                    public static final java.lang.String _contactRole =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("contactRole");
                                
                                    public static final java.lang.String _costCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("costCategory");
                                
                                    public static final java.lang.String _consolidatedExchangeRate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("consolidatedExchangeRate");
                                
                                    public static final java.lang.String _couponCode =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("couponCode");
                                
                                    public static final java.lang.String _currencyRate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("currencyRate");
                                
                                    public static final java.lang.String _customer =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customer");
                                
                                    public static final java.lang.String _customerCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customerCategory");
                                
                                    public static final java.lang.String _customerMessage =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customerMessage");
                                
                                    public static final java.lang.String _customerStatus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customerStatus");
                                
                                    public static final java.lang.String _customList =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customList");
                                
                                    public static final java.lang.String _customRecord =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("customRecord");
                                
                                    public static final java.lang.String _department =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("department");
                                
                                    public static final java.lang.String _employee =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("employee");
                                
                                    public static final java.lang.String _entityGroup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("entityGroup");
                                
                                    public static final java.lang.String _expenseCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("expenseCategory");
                                
                                    public static final java.lang.String _fairValuePrice =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("fairValuePrice");
                                
                                    public static final java.lang.String _file =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("file");
                                
                                    public static final java.lang.String _folder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("folder");
                                
                                    public static final java.lang.String _giftCertificate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("giftCertificate");
                                
                                    public static final java.lang.String _globalAccountMapping =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("globalAccountMapping");
                                
                                    public static final java.lang.String _hcmJob =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("hcmJob");
                                
                                    public static final java.lang.String _inboundShipment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("inboundShipment");
                                
                                    public static final java.lang.String _inventoryNumber =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("inventoryNumber");
                                
                                    public static final java.lang.String _item =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("item");
                                
                                    public static final java.lang.String _itemAccountMapping =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemAccountMapping");
                                
                                    public static final java.lang.String _itemDemandPlan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemDemandPlan");
                                
                                    public static final java.lang.String _itemRevision =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemRevision");
                                
                                    public static final java.lang.String _itemSupplyPlan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("itemSupplyPlan");
                                
                                    public static final java.lang.String _issue =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("issue");
                                
                                    public static final java.lang.String _job =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("job");
                                
                                    public static final java.lang.String _jobStatus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("jobStatus");
                                
                                    public static final java.lang.String _jobType =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("jobType");
                                
                                    public static final java.lang.String _location =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("location");
                                
                                    public static final java.lang.String _manufacturingCostTemplate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("manufacturingCostTemplate");
                                
                                    public static final java.lang.String _manufacturingOperationTask =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("manufacturingOperationTask");
                                
                                    public static final java.lang.String _manufacturingRouting =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("manufacturingRouting");
                                
                                    public static final java.lang.String _message =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("message");
                                
                                    public static final java.lang.String _nexus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("nexus");
                                
                                    public static final java.lang.String _note =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("note");
                                
                                    public static final java.lang.String _noteType =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("noteType");
                                
                                    public static final java.lang.String _opportunity =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("opportunity");
                                
                                    public static final java.lang.String _otherNameCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("otherNameCategory");
                                
                                    public static final java.lang.String _partner =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("partner");
                                
                                    public static final java.lang.String _partnerCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("partnerCategory");
                                
                                    public static final java.lang.String _paycheck =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("paycheck");
                                
                                    public static final java.lang.String _paymentMethod =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("paymentMethod");
                                
                                    public static final java.lang.String _payrollItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("payrollItem");
                                
                                    public static final java.lang.String _phoneCall =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("phoneCall");
                                
                                    public static final java.lang.String _priceLevel =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("priceLevel");
                                
                                    public static final java.lang.String _pricingGroup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("pricingGroup");
                                
                                    public static final java.lang.String _projectTask =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("projectTask");
                                
                                    public static final java.lang.String _promotionCode =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("promotionCode");
                                
                                    public static final java.lang.String _resourceAllocation =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("resourceAllocation");
                                
                                    public static final java.lang.String _revRecSchedule =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("revRecSchedule");
                                
                                    public static final java.lang.String _revRecTemplate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("revRecTemplate");
                                
                                    public static final java.lang.String _salesRole =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("salesRole");
                                
                                    public static final java.lang.String _salesTaxItem =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("salesTaxItem");
                                
                                    public static final java.lang.String _solution =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("solution");
                                
                                    public static final java.lang.String _siteCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("siteCategory");
                                
                                    public static final java.lang.String _subsidiary =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("subsidiary");
                                
                                    public static final java.lang.String _supportCase =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("supportCase");
                                
                                    public static final java.lang.String _task =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("task");
                                
                                    public static final java.lang.String _taxGroup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("taxGroup");
                                
                                    public static final java.lang.String _taxType =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("taxType");
                                
                                    public static final java.lang.String _term =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("term");
                                
                                    public static final java.lang.String _timeBill =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("timeBill");
                                
                                    public static final java.lang.String _timeSheet =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("timeSheet");
                                
                                    public static final java.lang.String _topic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("topic");
                                
                                    public static final java.lang.String _transaction =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("transaction");
                                
                                    public static final java.lang.String _unitsType =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("unitsType");
                                
                                    public static final java.lang.String _usage =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("usage");
                                
                                    public static final java.lang.String _vendor =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("vendor");
                                
                                    public static final java.lang.String _vendorCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("vendorCategory");
                                
                                    public static final java.lang.String _winLossReason =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("winLossReason");
                                
                                public static final SearchRecordType account =
                                    new SearchRecordType(_account,true);
                            
                                public static final SearchRecordType accountingPeriod =
                                    new SearchRecordType(_accountingPeriod,true);
                            
                                public static final SearchRecordType accountingTransaction =
                                    new SearchRecordType(_accountingTransaction,true);
                            
                                public static final SearchRecordType billingAccount =
                                    new SearchRecordType(_billingAccount,true);
                            
                                public static final SearchRecordType billingSchedule =
                                    new SearchRecordType(_billingSchedule,true);
                            
                                public static final SearchRecordType bin =
                                    new SearchRecordType(_bin,true);
                            
                                public static final SearchRecordType budget =
                                    new SearchRecordType(_budget,true);
                            
                                public static final SearchRecordType calendarEvent =
                                    new SearchRecordType(_calendarEvent,true);
                            
                                public static final SearchRecordType campaign =
                                    new SearchRecordType(_campaign,true);
                            
                                public static final SearchRecordType charge =
                                    new SearchRecordType(_charge,true);
                            
                                public static final SearchRecordType classification =
                                    new SearchRecordType(_classification,true);
                            
                                public static final SearchRecordType contact =
                                    new SearchRecordType(_contact,true);
                            
                                public static final SearchRecordType contactCategory =
                                    new SearchRecordType(_contactCategory,true);
                            
                                public static final SearchRecordType contactRole =
                                    new SearchRecordType(_contactRole,true);
                            
                                public static final SearchRecordType costCategory =
                                    new SearchRecordType(_costCategory,true);
                            
                                public static final SearchRecordType consolidatedExchangeRate =
                                    new SearchRecordType(_consolidatedExchangeRate,true);
                            
                                public static final SearchRecordType couponCode =
                                    new SearchRecordType(_couponCode,true);
                            
                                public static final SearchRecordType currencyRate =
                                    new SearchRecordType(_currencyRate,true);
                            
                                public static final SearchRecordType customer =
                                    new SearchRecordType(_customer,true);
                            
                                public static final SearchRecordType customerCategory =
                                    new SearchRecordType(_customerCategory,true);
                            
                                public static final SearchRecordType customerMessage =
                                    new SearchRecordType(_customerMessage,true);
                            
                                public static final SearchRecordType customerStatus =
                                    new SearchRecordType(_customerStatus,true);
                            
                                public static final SearchRecordType customList =
                                    new SearchRecordType(_customList,true);
                            
                                public static final SearchRecordType customRecord =
                                    new SearchRecordType(_customRecord,true);
                            
                                public static final SearchRecordType department =
                                    new SearchRecordType(_department,true);
                            
                                public static final SearchRecordType employee =
                                    new SearchRecordType(_employee,true);
                            
                                public static final SearchRecordType entityGroup =
                                    new SearchRecordType(_entityGroup,true);
                            
                                public static final SearchRecordType expenseCategory =
                                    new SearchRecordType(_expenseCategory,true);
                            
                                public static final SearchRecordType fairValuePrice =
                                    new SearchRecordType(_fairValuePrice,true);
                            
                                public static final SearchRecordType file =
                                    new SearchRecordType(_file,true);
                            
                                public static final SearchRecordType folder =
                                    new SearchRecordType(_folder,true);
                            
                                public static final SearchRecordType giftCertificate =
                                    new SearchRecordType(_giftCertificate,true);
                            
                                public static final SearchRecordType globalAccountMapping =
                                    new SearchRecordType(_globalAccountMapping,true);
                            
                                public static final SearchRecordType hcmJob =
                                    new SearchRecordType(_hcmJob,true);
                            
                                public static final SearchRecordType inboundShipment =
                                    new SearchRecordType(_inboundShipment,true);
                            
                                public static final SearchRecordType inventoryNumber =
                                    new SearchRecordType(_inventoryNumber,true);
                            
                                public static final SearchRecordType item =
                                    new SearchRecordType(_item,true);
                            
                                public static final SearchRecordType itemAccountMapping =
                                    new SearchRecordType(_itemAccountMapping,true);
                            
                                public static final SearchRecordType itemDemandPlan =
                                    new SearchRecordType(_itemDemandPlan,true);
                            
                                public static final SearchRecordType itemRevision =
                                    new SearchRecordType(_itemRevision,true);
                            
                                public static final SearchRecordType itemSupplyPlan =
                                    new SearchRecordType(_itemSupplyPlan,true);
                            
                                public static final SearchRecordType issue =
                                    new SearchRecordType(_issue,true);
                            
                                public static final SearchRecordType job =
                                    new SearchRecordType(_job,true);
                            
                                public static final SearchRecordType jobStatus =
                                    new SearchRecordType(_jobStatus,true);
                            
                                public static final SearchRecordType jobType =
                                    new SearchRecordType(_jobType,true);
                            
                                public static final SearchRecordType location =
                                    new SearchRecordType(_location,true);
                            
                                public static final SearchRecordType manufacturingCostTemplate =
                                    new SearchRecordType(_manufacturingCostTemplate,true);
                            
                                public static final SearchRecordType manufacturingOperationTask =
                                    new SearchRecordType(_manufacturingOperationTask,true);
                            
                                public static final SearchRecordType manufacturingRouting =
                                    new SearchRecordType(_manufacturingRouting,true);
                            
                                public static final SearchRecordType message =
                                    new SearchRecordType(_message,true);
                            
                                public static final SearchRecordType nexus =
                                    new SearchRecordType(_nexus,true);
                            
                                public static final SearchRecordType note =
                                    new SearchRecordType(_note,true);
                            
                                public static final SearchRecordType noteType =
                                    new SearchRecordType(_noteType,true);
                            
                                public static final SearchRecordType opportunity =
                                    new SearchRecordType(_opportunity,true);
                            
                                public static final SearchRecordType otherNameCategory =
                                    new SearchRecordType(_otherNameCategory,true);
                            
                                public static final SearchRecordType partner =
                                    new SearchRecordType(_partner,true);
                            
                                public static final SearchRecordType partnerCategory =
                                    new SearchRecordType(_partnerCategory,true);
                            
                                public static final SearchRecordType paycheck =
                                    new SearchRecordType(_paycheck,true);
                            
                                public static final SearchRecordType paymentMethod =
                                    new SearchRecordType(_paymentMethod,true);
                            
                                public static final SearchRecordType payrollItem =
                                    new SearchRecordType(_payrollItem,true);
                            
                                public static final SearchRecordType phoneCall =
                                    new SearchRecordType(_phoneCall,true);
                            
                                public static final SearchRecordType priceLevel =
                                    new SearchRecordType(_priceLevel,true);
                            
                                public static final SearchRecordType pricingGroup =
                                    new SearchRecordType(_pricingGroup,true);
                            
                                public static final SearchRecordType projectTask =
                                    new SearchRecordType(_projectTask,true);
                            
                                public static final SearchRecordType promotionCode =
                                    new SearchRecordType(_promotionCode,true);
                            
                                public static final SearchRecordType resourceAllocation =
                                    new SearchRecordType(_resourceAllocation,true);
                            
                                public static final SearchRecordType revRecSchedule =
                                    new SearchRecordType(_revRecSchedule,true);
                            
                                public static final SearchRecordType revRecTemplate =
                                    new SearchRecordType(_revRecTemplate,true);
                            
                                public static final SearchRecordType salesRole =
                                    new SearchRecordType(_salesRole,true);
                            
                                public static final SearchRecordType salesTaxItem =
                                    new SearchRecordType(_salesTaxItem,true);
                            
                                public static final SearchRecordType solution =
                                    new SearchRecordType(_solution,true);
                            
                                public static final SearchRecordType siteCategory =
                                    new SearchRecordType(_siteCategory,true);
                            
                                public static final SearchRecordType subsidiary =
                                    new SearchRecordType(_subsidiary,true);
                            
                                public static final SearchRecordType supportCase =
                                    new SearchRecordType(_supportCase,true);
                            
                                public static final SearchRecordType task =
                                    new SearchRecordType(_task,true);
                            
                                public static final SearchRecordType taxGroup =
                                    new SearchRecordType(_taxGroup,true);
                            
                                public static final SearchRecordType taxType =
                                    new SearchRecordType(_taxType,true);
                            
                                public static final SearchRecordType term =
                                    new SearchRecordType(_term,true);
                            
                                public static final SearchRecordType timeBill =
                                    new SearchRecordType(_timeBill,true);
                            
                                public static final SearchRecordType timeSheet =
                                    new SearchRecordType(_timeSheet,true);
                            
                                public static final SearchRecordType topic =
                                    new SearchRecordType(_topic,true);
                            
                                public static final SearchRecordType transaction =
                                    new SearchRecordType(_transaction,true);
                            
                                public static final SearchRecordType unitsType =
                                    new SearchRecordType(_unitsType,true);
                            
                                public static final SearchRecordType usage =
                                    new SearchRecordType(_usage,true);
                            
                                public static final SearchRecordType vendor =
                                    new SearchRecordType(_vendor,true);
                            
                                public static final SearchRecordType vendorCategory =
                                    new SearchRecordType(_vendorCategory,true);
                            
                                public static final SearchRecordType winLossReason =
                                    new SearchRecordType(_winLossReason,true);
                            

                                public java.lang.String getValue() { return localSearchRecordType;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localSearchRecordType.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.core_2017_2.platform.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":SearchRecordType",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "SearchRecordType",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localSearchRecordType==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("SearchRecordType cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localSearchRecordType);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.core_2017_2.platform.webservices.netsuite.com")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSearchRecordType)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static SearchRecordType fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    SearchRecordType enumeration = (SearchRecordType)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static SearchRecordType fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static SearchRecordType fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return SearchRecordType.Factory.fromString(content,namespaceUri);
                    } else {
                       return SearchRecordType.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static SearchRecordType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            SearchRecordType object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"SearchRecordType" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = SearchRecordType.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = SearchRecordType.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    