
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

        
            package com.netsuite.webservices.platform.core_2017_2.types;
        
            /**
            *  ExtensionMapper class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "UnitsTypeSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.UnitsTypeSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:filecabinet_2017_2.documents.webservices.netsuite.com".equals(namespaceURI) &&
                  "FileSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.documents.filecabinet_2017_2.FileSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageFedExPackagingFedEx".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExPackagingFedEx.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "BudgetExchangeRateList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.BudgetExchangeRateList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SerializedInventoryItemNumbers".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SerializedInventoryItemNumbers.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemOutOfStockBehavior".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemOutOfStockBehavior.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeForms".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeForms.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CostCategory".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.CostCategory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingOperationTaskPredecessorPredecessorType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.types.ManufacturingOperationTaskPredecessorPredecessorType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.types.CampaignResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "WriteResponseList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.WriteResponseList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:website_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SiteCategorySearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.website_2017_2.SiteCategorySearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionColumnCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.TransactionColumnCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryDetailSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.InventoryDetailSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeBillSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.TimeBillSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TokenPassportSignature".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.TokenPassportSignature.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:filecabinet_2017_2.documents.webservices.netsuite.com".equals(namespaceURI) &&
                  "FileSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.documents.filecabinet_2017_2.FileSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PermissionLevel".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.PermissionLevel.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeItemTimeType".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.TimeItemTimeType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetListRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetListRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignVertical".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignVertical.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesTaxItemAvailable".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.SalesTaxItemAvailable.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactRoleSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ContactRoleSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignFamily".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignFamily.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "UpsertResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.UpsertResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemReceiptExpense".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.ItemReceiptExpense.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "GlobalAccountMappingSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.GlobalAccountMappingSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "FolderSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.FolderSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobMilestonesList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobMilestonesList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ClassificationSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ClassificationSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LocationGeolocationMethod".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.LocationGeolocationMethod.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecScheduleSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.RevRecScheduleSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "getSelectValueResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetSelectValueResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxGroupSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TaxGroupSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.filecabinet_2017_2.documents.webservices.netsuite.com".equals(namespaceURI) &&
                  "FileEncoding".equals(typeName)){
                   
                            return  com.netsuite.webservices.documents.filecabinet_2017_2.types.FileEncoding.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ChargeSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.ChargeSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "WsRoleList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.WsRoleList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "Preferences".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.Preferences.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorBillItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorBillItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:general_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "AdvInterCompanyJournalEntryAccountingBookDetail".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.general_2017_2.AdvInterCompanyJournalEntryAccountingBookDetail.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobTypeSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.JobTypeSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "IssueRelationship".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.types.IssueRelationship.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ReturnAuthorizationItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountingPeriodSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.AccountingPeriodSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PromotionCodeApplyDiscountTo".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.types.PromotionCodeApplyDiscountTo.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomFieldTranslations".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomFieldTranslations.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "NexusSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.NexusSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SsoPassport".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SsoPassport.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecord".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecord.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerStatusSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerStatusSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PaycheckSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "Language".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.Language.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "OpportunitySalesTeam".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.OpportunitySalesTeam.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerAddressbook".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerAddressbook.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Job".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.Job.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeCompanyContributionList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeCompanyContributionList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:general_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "AdvInterCompanyJournalEntryLineList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.general_2017_2.AdvInterCompanyJournalEntryLineList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:communication_2017_2.general.webservices.netsuite.com".equals(namespaceURI) &&
                  "Note".equals(typeName)){
                   
                            return  com.netsuite.webservices.general.communication_2017_2.Note.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PromotionCodeItems".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.PromotionCodeItems.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetSavedSearchResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetSavedSearchResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "RecordList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.RecordList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomizationFieldType".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.types.CustomizationFieldType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PostingTransactionSummaryFilter".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.PostingTransactionSummaryFilter.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CouponCodeSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CouponCodeSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecTemplateSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.RevRecTemplateSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetServerTimeRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetServerTimeRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingAccountSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.BillingAccountSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.AccountSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PartnerPromoCodeList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.PartnerPromoCodeList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LocationRegionsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.LocationRegionsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "WorkOrderItemItemCommitInventory".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.types.WorkOrderItemItemCommitInventory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckPayDeduct".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckPayDeduct.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "AssemblyItemBillOfMaterialsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.AssemblyItemBillOfMaterialsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSupplyPlanOrderType".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.types.ItemSupplyPlanOrderType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "WriteResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.WriteResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "FileSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.FileSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.communication_2017_2.general.webservices.netsuite.com".equals(namespaceURI) &&
                  "NoteDirection".equals(typeName)){
                   
                            return  com.netsuite.webservices.general.communication_2017_2.types.NoteDirection.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "UpdateInviteeStatusReference".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.UpdateInviteeStatusReference.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "OtherChargeResaleItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.OtherChargeResaleItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignChannel".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignChannel.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactRoleSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ContactRoleSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemAccountingBookDetail".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemAccountingBookDetail.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaymentMethodSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PaymentMethodSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountingTransactionSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.AccountingTransactionSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PresentationItemType".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.PresentationItemType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaymentMethodSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PaymentMethodSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "EstimateItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.EstimateItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PromotionCodeSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.PromotionCodeSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "UnitsTypeSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.UnitsTypeSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemOptionCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.ItemOptionCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SerializedAssemblyItemBillOfMaterialsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SerializedAssemblyItemBillOfMaterialsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ConsolidatedExchangeRateSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ConsolidatedExchangeRateSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetCurrencyRateResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.GetCurrencyRateResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CostCategorySearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.CostCategorySearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemAccountingBookDetailList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemAccountingBookDetailList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CurrencySymbolPlacement".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.CurrencySymbolPlacement.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "NoteTypeSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.NoteTypeSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "NexusSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.NexusSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InvoicePartnersList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.InvoicePartnersList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InitializeRefList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.InitializeRefList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchDoubleCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchDoubleCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "CalendarEventTimeItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventTimeItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecScheduleAmortizationType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.RevRecScheduleAmortizationType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeEmergencyContactList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeEmergencyContactList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.AccountSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnStringField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeHcmPosition".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeHcmPosition.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorCreditExpenseList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorCreditExpenseList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "IssueVersionList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.IssueVersionList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentThirdPartyTypeUps".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentThirdPartyTypeUps.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SubsidiarySearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SubsidiarySearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PartnerSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PartnerSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomFieldRoleAccess".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomFieldRoleAccess.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransferOrderItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.TransferOrderItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PayrollItemSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.PayrollItemSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageUpsCodMethodUps".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUpsCodMethodUps.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DetachReference".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.DetachReference.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountingPeriodSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.AccountingPeriodSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ConsolidatedExchangeRateSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ConsolidatedExchangeRateSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "MapSsoResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.MapSsoResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemAvailabilityList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.ItemAvailabilityList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaskSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TaskSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionColumnCustomFieldFilter".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.TransactionColumnCustomFieldFilter.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SiteCategorySearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.SiteCategorySearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecTemplate".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.RevRecTemplate.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionShipGroup".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.TransactionShipGroup.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckJournalEarningList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckJournalEarningList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "MultiSelectCustomFieldRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.MultiSelectCustomFieldRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckPaySummary".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckPaySummary.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "BinWorksheetItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.BinWorksheetItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PhoneCallSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesTeamList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.SalesTeamList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashRefund".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CashRefund.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.AccountSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeEntrySearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.TimeEntrySearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountingBookStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.AccountingBookStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:faults_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ExceededRequestLimitFault".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFault.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorReturnAuthorizationItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorReturnAuthorizationItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemNumberCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.ItemNumberCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SupportCaseSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SupportCaseSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ClassTranslation".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ClassTranslation.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncStatusType".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.AsyncStatusType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AttachReference".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.AttachReference.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionPaymentEventResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.TransactionPaymentEventResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SerializedInventoryItemLocations".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SerializedInventoryItemLocations.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ProjectTaskTimeItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskTimeItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemVendor".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemVendor.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:bank_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "DepositPayment".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.bank_2017_2.DepositPayment.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ChargeSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ChargeSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeePayFrequency".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.types.EmployeePayFrequency.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CustomRecordSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerPaymentApply".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CustomerPaymentApply.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ProjectTaskSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "DownloadItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.DownloadItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PurchaseRequisitionExpense".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.PurchaseRequisitionExpense.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CalendarEventSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckJournalEmployeeTaxList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckJournalEmployeeTaxList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "EntitySearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.EntitySearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "Charge".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.Charge.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesOrder".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.SalesOrder.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignEventList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignEventList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PaycheckSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomTransaction".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomTransaction.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleRecurrenceRecurrenceUnits".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRecurrenceRecurrenceUnits.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.TransactionStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomizationRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.CustomizationRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSupplyPlanSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.ItemSupplyPlanSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "OtherNameCategorySearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.OtherNameCategorySearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SupportCaseEscalateToList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SupportCaseEscalateToList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeBillTimeType".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.types.TimeBillTimeType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckPayExpList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckPayExpList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "OriginatingLeadSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.OriginatingLeadSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerPaymentCreditList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CustomerPaymentCreditList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Topic".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.Topic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TransactionSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.TransactionSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemRevisionSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ItemRevisionSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SerializedAssemblyItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SerializedAssemblyItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncSearchRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AsyncSearchRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SupportCaseIssue".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SupportCaseIssue.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingCostTemplateSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ManufacturingCostTemplateSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorReturnAuthorizationExpense".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorReturnAuthorizationExpense.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentShipStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentShipStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetServerTimeResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetServerTimeResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryNumberLocationsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.InventoryNumberLocationsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerDownload".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerDownload.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactRoleSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ContactRoleSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemAccountMappingSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ItemAccountMappingSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PeriodicLotSizeType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.PeriodicLotSizeType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "NoteType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.NoteType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignDirectMail".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignDirectMail.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BinSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.BinSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetSavedSearchRecord".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.GetSavedSearchRecord.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "IssueRelatedIssues".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.IssueRelatedIssues.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeAccessType".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.types.CustomRecordTypeAccessType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "GiftCertificateItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.GiftCertificateItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "WinLossReasonSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.WinLossReasonSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashSaleExpCostList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.CashSaleExpCostList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemNumberCustomFieldFilter".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.ItemNumberCustomFieldFilter.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "UpdateListResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.UpdateListResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerGroupPricing".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerGroupPricing.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchDate".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.SearchDate.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:bank_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CheckItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.bank_2017_2.CheckItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "DepositApplication".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.DepositApplication.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSupplyPlanOrderList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.ItemSupplyPlanOrderList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "WorkOrderCompletion".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.WorkOrderCompletion.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomListSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomListSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "BaseRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.BaseRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ProjectTaskStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.ProjectTaskStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DetachRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.DetachRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ClassTranslationList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ClassTranslationList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxGroup".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TaxGroup.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LocationAutoAssignmentRegionSetting".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.LocationAutoAssignmentRegionSetting.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "GiftCertRedemption".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.GiftCertRedemption.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "OpportunityCompetitorsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.OpportunityCompetitorsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CheckAsyncStatusRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatusRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignCampaignEmailStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.types.CampaignCampaignEmailStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "Duration".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.Duration.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSubType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemSubType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SupportCaseSolutions".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SupportCaseSolutions.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageFedExDeliveryConfFedEx".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExDeliveryConfFedEx.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "IssueSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.IssueSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InvoiceSalesTeamList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.InvoiceSalesTeamList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeSublistsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeSublistsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CreditMemoApplyList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CreditMemoApplyList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactAccessRolesList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.ContactAccessRolesList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ProjectTask".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.ProjectTask.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "MessageSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.MessageSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerPaymentDepositList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CustomerPaymentDepositList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeUseTimeData".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.types.EmployeeUseTimeData.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ChargeSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.ChargeSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignAudience".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignAudience.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ConsolidatedExchangeRateSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ConsolidatedExchangeRateSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeEarning".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeEarning.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxGroupSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TaxGroupSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "PhoneCallSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.PhoneCallSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PromotionsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.PromotionsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PricingGroupSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PricingGroupSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactCategorySearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ContactCategorySearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetDeletedResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetDeletedResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:faults_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SoapFault".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.faults_2017_2.SoapFault.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobBillingType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.types.JobBillingType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordType".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionLineType".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.TransactionLineType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "WorkOrderCompletionComponentList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.WorkOrderCompletionComponentList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnBooleanCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "NoteSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "DemandPlan".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.DemandPlan.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchStringCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchStringCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerNegativeNumberFormat".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.types.CustomerNegativeNumberFormat.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "GiftCertRedemptionList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.GiftCertRedemptionList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "OpportunitySearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.OpportunitySearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ServicePurchaseItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ServicePurchaseItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeEntrySearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TimeEntrySearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "LandedCostDataList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.LandedCostDataList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LocationRegions".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.LocationRegions.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Account".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.Account.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SupportCaseStatusStage".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.types.SupportCaseStatusStage.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeSheetTimeGridList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.TimeSheetTimeGridList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncUpdateListRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateListRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeSubscriptions".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeSubscriptions.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "OriginatingLeadSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.OriginatingLeadSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerPaymentApplyList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CustomerPaymentApplyList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchDoubleField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountingPeriodSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.AccountingPeriodSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "EstimateSalesTeamList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.EstimateSalesTeamList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "LanguageValueList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.LanguageValueList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "CalendarEventAttendeeAttendance".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventAttendeeAttendance.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomListTranslations".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomListTranslations.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ProjectTaskPredecessorList".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskPredecessorList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerCategorySearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.CustomerCategorySearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Translation".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.Translation.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CampaignSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecTemplateRecurrenceList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.RevRecTemplateRecurrenceList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "BinWorksheet".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.BinWorksheet.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetSelectValueFieldDescription".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.GetSelectValueFieldDescription.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "Passport".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.Passport.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeHcmPositionList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeHcmPositionList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InitializeListResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.InitializeListResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeChildrenList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeChildrenList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemVsoeSopGroup".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemVsoeSopGroup.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InitializeRefType".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.InitializeRefType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BinSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.BinSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingAccountSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.BillingAccountSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingCostDetail".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingCostDetail.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SupportCaseSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SupportCaseSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.faults_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "StatusDetailCodeType".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.faults_2017_2.types.StatusDetailCodeType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxTypeNexusesTax".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TaxTypeNexusesTax.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesRoleSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SalesRoleSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "FairValuePriceSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.FairValuePriceSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ChargeSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ChargeSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTranslationsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTranslationsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorCategorySearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.VendorCategorySearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorPaymentCredit".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorPaymentCredit.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "WorkOrderIssue".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.WorkOrderIssue.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "RecurrenceFrequency".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.RecurrenceFrequency.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TermSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TermSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryNumberBinSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.InventoryNumberBinSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeBillSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TimeBillSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "OtherCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.OtherCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "PhoneCallReminderMinutes".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.PhoneCallReminderMinutes.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorReturnAuthorizationOrderStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.types.VendorReturnAuthorizationOrderStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomizationSearchLevel".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.types.CustomizationSearchLevel.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleYearDow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearDow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckPayEarnList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckPayEarnList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemAvailabilityFilter".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.ItemAvailabilityFilter.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorPricingScheduleList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.VendorPricingScheduleList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemRevisionSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemRevisionSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SubsidiarySearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SubsidiarySearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingAccountFrequency".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.types.BillingAccountFrequency.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaskContact".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.TaskContact.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "WinLossReasonSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.WinLossReasonSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetAllRecordType".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.GetAllRecordType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerPaymentDeposit".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CustomerPaymentDeposit.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "GiftCertificateSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.GiftCertificateSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PriceLevelSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PriceLevelSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "AssemblyBuild".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.AssemblyBuild.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchLongField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetDataCenterUrlsRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrlsRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchEnumMultiSelectFieldOperator".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.SearchEnumMultiSelectFieldOperator.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingAccountSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.BillingAccountSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "UpdateInviteeStatusListRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusListRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PayrollItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.PayrollItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchMultiSelectFieldOperator".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.SearchMultiSelectFieldOperator.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerPayment".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CustomerPayment.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingRoutingRoutingStepList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingRoutingRoutingStepList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemPreferenceCriterion".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemPreferenceCriterion.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorCurrencyList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.VendorCurrencyList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorCreditItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorCreditItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSupplyLotSizingMethod".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemSupplyLotSizingMethod.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountingTransactionSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.AccountingTransactionSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashSaleSalesTeam".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.CashSaleSalesTeam.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.InventoryItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryCostRevaluation".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.InventoryCostRevaluation.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageFedEx".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageFedEx.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "EntityGroupSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.EntityGroupSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:filecabinet_2017_2.documents.webservices.netsuite.com".equals(namespaceURI) &&
                  "FolderSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.documents.filecabinet_2017_2.FolderSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "LoginRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.LoginRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "LandedCostData".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.LandedCostData.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerStatusSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CustomerStatusSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ExpenseCategory".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ExpenseCategory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorBillExpense".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorBillExpense.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "OtherCustomFieldFilter".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.OtherCustomFieldFilter.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ReturnAuthorizationSalesTeam".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationSalesTeam.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeBillSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.TimeBillSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PayrollItemSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PayrollItemSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ConsolidatedExchangeRate".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ConsolidatedExchangeRate.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CurrencyRateSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.CurrencyRateSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "PhoneCallPriority".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.PhoneCallPriority.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ResourceAllocation".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.ResourceAllocation.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleYearMonth".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearMonth.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CreditMemoApply".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CreditMemoApply.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordCustomFieldFilterList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordCustomFieldFilterList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InitializeType".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.InitializeType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorAddressbookList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.VendorAddressbookList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InboundShipmentItemsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.InboundShipmentItemsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "Source".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.Source.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingCostTemplateSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingCostTemplateSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PurchaseOrderExpenseList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.PurchaseOrderExpenseList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignResponseResponsesList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignResponseResponsesList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DetachBasicReference".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.DetachBasicReference.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesTaxItemSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SalesTaxItemSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "FileSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.FileSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InvoiceTimeList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.InvoiceTimeList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingRoutingRoutingComponentList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingRoutingRoutingComponentList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InitializeListRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.InitializeListRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxDetailsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TaxDetailsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryTransferInventoryList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.InventoryTransferInventoryList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CreditMemoSalesTeam".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CreditMemoSalesTeam.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeTabsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeTabsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ResourceAllocationApprovalStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.ResourceAllocationApprovalStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobCreditCardsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobCreditCardsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchTextNumberFieldOperator".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.SearchTextNumberFieldOperator.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "FairValuePriceSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.FairValuePriceSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "DemandPlanMatrix".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.DemandPlanMatrix.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetItemAvailabilityResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.GetItemAvailabilityResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeEntrySearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.TimeEntrySearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InterCompanyTransferOrderItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.InterCompanyTransferOrderItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BinSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.BinSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryItemLocations".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.InventoryItemLocations.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AddRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AddRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemDemandPlanProjectionMethod".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.types.ItemDemandPlanProjectionMethod.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ClassificationSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ClassificationSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SolutionSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.SolutionSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "LanguageValue".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.LanguageValue.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeSubscriptionsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeSubscriptionsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerCategorySearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.CustomerCategorySearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LocationSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.LocationSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PartnerCategorySearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PartnerCategorySearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactAccessRoles".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.ContactAccessRoles.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PromotionCodePartners".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.PromotionCodePartners.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckPayExp".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckPayExp.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomListSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomListSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PurchLandedCostList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.PurchLandedCostList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LotNumberedAssemblyItemBillOfMaterials".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.LotNumberedAssemblyItemBillOfMaterials.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionType".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.TransactionType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ReadResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.ReadResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PricingGroup".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PricingGroup.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionBodyCustomFieldFilter".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.TransactionBodyCustomFieldFilter.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "BaseRefList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.BaseRefList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobAddressbookList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobAddressbookList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "VsoeDeferral".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.VsoeDeferral.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:website_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SiteCategoryPresentationItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.website_2017_2.SiteCategoryPresentationItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeTerminationCategory".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.types.EmployeeTerminationCategory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeParents".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeParents.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncSearchResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AsyncSearchResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxTypeNexusAccounts".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TaxTypeNexusAccounts.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeEntry".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.TimeEntry.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesOrderSalesTeamList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.SalesOrderSalesTeamList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "UnitsTypeUomList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.UnitsTypeUomList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:general_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "JournalEntryLineList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.general_2017_2.JournalEntryLineList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeCommissionPaymentPreference".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.types.EmployeeCommissionPaymentPreference.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "DepositApplicationApplyList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.DepositApplicationApplyList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemDemandPlan".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.ItemDemandPlan.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentMethodOfTransportUps".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentMethodOfTransportUps.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecTemplateSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.RevRecTemplateSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageFedExSignatureOptionsFedEx".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExSignatureOptionsFedEx.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:general_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "StatisticalJournalEntryLine".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.general_2017_2.StatisticalJournalEntryLine.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeTranslationsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeTranslationsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingRatesMatrix".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.BillingRatesMatrix.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckJournalEmployeeTax".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckJournalEmployeeTax.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ShippingCarrier".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.ShippingCarrier.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ChargeSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.ChargeSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "HcmJobSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.HcmJobSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignEmailList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignEmailList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaymentMethodVisuals".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PaymentMethodVisuals.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SolutionTopicsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SolutionTopicsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckPaySummaryList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckPaySummaryList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchMultiSelectCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerDeposit".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CustomerDeposit.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobStatusSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobStatusSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "getSelectValueRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetSelectValueRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:communication_2017_2.general.webservices.netsuite.com".equals(namespaceURI) &&
                  "NoteSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.general.communication_2017_2.NoteSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ChargeUse".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.types.ChargeUse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetPostingTransactionSummaryRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummaryRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetCustomizationIdResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.GetCustomizationIdResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeLinks".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeLinks.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Partner".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.Partner.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InboundShipmentSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.InboundShipmentSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryNumber".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.InventoryNumber.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerCurrency".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerCurrency.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ChangePasswordRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.ChangePasswordRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CurrencyRateSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.CurrencyRateSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SelectDimensionRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SelectDimensionRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AttachBasicReference".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.AttachBasicReference.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "GlobalAccountMappingSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.GlobalAccountMappingSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.SearchRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PromotionCodeSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.PromotionCodeSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "IssueRelatedIssuesList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.IssueRelatedIssuesList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PromotionCodeUseType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.types.PromotionCodeUseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "HcmJobSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.HcmJobSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnSelectField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSupplyPlan".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.ItemSupplyPlan.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerMessageSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.CustomerMessageSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerMessageSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.CustomerMessageSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ProjectTaskSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentExportTypeUps".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentExportTypeUps.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncDeleteListResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteListResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemMemberList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemMemberList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "GeneralRateType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.GeneralRateType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetPostingTransactionSummaryResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummaryResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxDetails".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TaxDetails.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncGetListResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AsyncGetListResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.VendorSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorPaymentApplyList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorPaymentApplyList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Issue".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.Issue.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "FldFilterSelList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.FldFilterSelList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "IntercoStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.IntercoStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeWorkAssignment".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.types.EmployeeWorkAssignment.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ReturnAuthorizationItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.TransactionSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ServiceItemTaskTemplates".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ServiceItemTaskTemplates.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "MarkupItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.MarkupItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:filecabinet_2017_2.documents.webservices.netsuite.com".equals(namespaceURI) &&
                  "FolderSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.documents.filecabinet_2017_2.FolderSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeOnlineFormsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeOnlineFormsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "EntityGroupSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.EntityGroupSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaymentMethodSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PaymentMethodSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeRolesList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeRolesList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "BinSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.BinSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:bank_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "DepositPaymentList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.bank_2017_2.DepositPaymentList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:communication_2017_2.general.webservices.netsuite.com".equals(namespaceURI) &&
                  "MessageSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.general.communication_2017_2.MessageSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerCategory".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.CustomerCategory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeParentsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeParentsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeCompensationCurrency".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.types.EmployeeCompensationCurrency.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "DemandPlanCalendarType".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.types.DemandPlanCalendarType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerMessageSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CustomerMessageSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SubsidiaryNexus".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SubsidiaryNexus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactAddressbook".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.ContactAddressbook.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ClassificationSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ClassificationSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetDataCenterUrlsResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.GetDataCenterUrlsResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchRecord".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchRecord.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorCurrency".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.VendorCurrency.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TopicSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.TopicSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckJournalEarning".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckJournalEarning.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorPaymentApply".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorPaymentApply.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "NoteTypeSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.NoteTypeSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransferOrderItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.TransferOrderItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "GiftCertificateSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.GiftCertificateSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GlobalAccountMappingSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.GlobalAccountMappingSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignEvent".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignEvent.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CurrencyRateList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.CurrencyRateList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "WinLossReasonSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.WinLossReasonSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "AssemblyItemEffectiveBomControl".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.AssemblyItemEffectiveBomControl.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SupportCaseType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SupportCaseType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SubscriptionsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.SubscriptionsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PromotionCodeCurrency".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.PromotionCodeCurrency.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "BudgetSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.BudgetSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "UpdateInviteeStatusListResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusListResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Contact".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.Contact.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetAllResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.GetAllResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetDataCenterUrlsResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrlsResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashSaleItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.CashSaleItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AttachContactReference".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.AttachContactReference.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemGroup".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemGroup.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "Opportunity".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.Opportunity.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DeletedRecord".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.DeletedRecord.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobResourcesList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobResourcesList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:communication_2017_2.general.webservices.netsuite.com".equals(namespaceURI) &&
                  "MessageSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.general.communication_2017_2.MessageSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeBillSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.TimeBillSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesOrderItemCreatePo".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderItemCreatePo.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorCreditItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorCreditItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.ItemsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "OriginatingLeadSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.OriginatingLeadSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerStatusSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CustomerStatusSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomizationFilterCompareType".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.types.CustomizationFilterCompareType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomListSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CustomListSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DimensionRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.DimensionRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "RateList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.RateList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PartnerOtherRelationships".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.types.PartnerOtherRelationships.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerOtherRelationships".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.types.CustomerOtherRelationships.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemDemandPlanSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ItemDemandPlanSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerSalesTeam".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerSalesTeam.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TranslationList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TranslationList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchNextRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.SearchNextRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleRecurrencePattern".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRecurrencePattern.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncUpsertListResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertListResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemInvtClassification".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemInvtClassification.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Term".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.Term.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerSalesTeam".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CustomerSalesTeam.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CustomerSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ExpenseCategorySearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ExpenseCategorySearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSupplyPlanSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.ItemSupplyPlanSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "Usage".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.Usage.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobStatusSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.JobStatusSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecScheduleRecurrenceList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.RevRecScheduleRecurrenceList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "BudgetExchangeRate".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.BudgetExchangeRate.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaskSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.TaskSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Solution".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.Solution.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentThirdPartyTypeFedEx".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentThirdPartyTypeFedEx.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PayrollItemSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.PayrollItemSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "CalendarEventAttendee".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventAttendee.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobTypeSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobTypeSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "PhoneCallContactList".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.PhoneCallContactList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PartnerCategory".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PartnerCategory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "OtherNameCategorySearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.OtherNameCategorySearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionPaymentEventHoldReason".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.TransactionPaymentEventHoldReason.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ExpenseCategoryRates".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ExpenseCategoryRates.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillment".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillment.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingOperationTaskSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ManufacturingOperationTaskSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:faults_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InvalidCredentialsFault".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFault.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetCustomizationIdResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetCustomizationIdResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:filecabinet_2017_2.documents.webservices.netsuite.com".equals(namespaceURI) &&
                  "FileSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.documents.filecabinet_2017_2.FileSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InvoiceSalesTeam".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.InvoiceSalesTeam.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "UpdateListRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.UpdateListRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:bank_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CheckLandedCostList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.bank_2017_2.CheckLandedCostList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxDetailSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TaxDetailSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "Paycheck".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.Paycheck.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobStatusSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobStatusSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignDirectMailList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignDirectMailList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ResourceAllocationSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.ResourceAllocationSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingRoutingSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingRoutingSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PeriodDemandPlan".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.PeriodDemandPlan.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomFieldRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.CustomFieldRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecScheduleRecurrenceType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.RevRecScheduleRecurrenceType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InterCompanyTransferOrder".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.InterCompanyTransferOrder.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "HcmJob".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.HcmJob.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "CalendarEventAccessLevel".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventAccessLevel.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryNumberSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.InventoryNumberSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobCreditCards".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobCreditCards.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignResponseCategory".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.types.CampaignResponseCategory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryNumberSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.InventoryNumberSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetItemAvailabilityRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetItemAvailabilityRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchDateCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchDateCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "DayOfTheWeek".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.types.DayOfTheWeek.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DocumentInfo".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.DocumentInfo.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobResources".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobResources.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SolutionTopics".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SolutionTopics.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactCategory".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ContactCategory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AsyncResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TopicSolution".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.TopicSolution.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AssemblyItemBomSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.AssemblyItemBomSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Solutions".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.Solutions.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingRoutingRoutingComponent".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingRoutingRoutingComponent.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetCustomizationIdRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetCustomizationIdRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.BillingScheduleSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "OriginatingLeadSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.OriginatingLeadSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AlcoholRecipientType".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.AlcoholRecipientType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.EmployeeSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingRoutingSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingRoutingSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ProjectTaskSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "UpdateInviteeStatusRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InvoiceItemCostList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.InvoiceItemCostList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "WsRole".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.WsRole.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InvoiceItemCost".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.InvoiceItemCost.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetSavedSearchRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetSavedSearchRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PostingTransactionSummary".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.PostingTransactionSummary.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetDeletedRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetDeletedRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PriceLevelSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PriceLevelSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnBooleanField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "OtherNameCategorySearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.OtherNameCategorySearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "OpportunityPartnersList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.OpportunityPartnersList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryTransferInventory".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.InventoryTransferInventory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeRates".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeRates.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ConsolidatedRate".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ConsolidatedRate.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "UpdateRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.UpdateRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "LoginResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.LoginResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PayrollItemSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PayrollItemSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:website_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SiteCategory".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.website_2017_2.SiteCategory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomListTranslationsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomListTranslationsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchTextNumberField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchTextNumberField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "CalendarEventSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DeleteResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.DeleteResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "WorkOrderItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.WorkOrderItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaymentMethod".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PaymentMethod.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemCostAccountingStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemCostAccountingStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AddListResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AddListResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TermSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TermSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "EntitySearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.EntitySearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerGroupPricingList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerGroupPricingList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PartnerCategorySearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PartnerCategorySearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomFieldType".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomFieldType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "RecordType".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.RecordType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DetachResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.DetachResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TopicSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.TopicSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchRecordBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchRecordBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobAddressbook".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobAddressbook.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemDemandPlanSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.ItemDemandPlanSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CreditMemoPartnersList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CreditMemoPartnersList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SupportCaseStage".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.types.SupportCaseStage.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecScheduleRecurrence".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.RevRecScheduleRecurrence.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchMoreWithIdRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithIdRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DeleteRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.DeleteRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeSheetSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.TimeSheetSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EntityGroupType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.types.EntityGroupType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionBodyCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.TransactionBodyCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SubsidiaryNexusList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SubsidiaryNexusList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SignatureAlgorithm".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.SignatureAlgorithm.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "RecurrenceDow".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.RecurrenceDow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeDirectDeposit".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeDirectDeposit.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesRoleSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SalesRoleSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetSelectFilterByFieldValue".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.GetSelectFilterByFieldValue.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ExpenseCategorySearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ExpenseCategorySearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashSaleItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.CashSaleItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomListCustomValue".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomListCustomValue.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "GiftCertificateSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.GiftCertificateSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckJournalDeduction".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckJournalDeduction.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InboundShipmentSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.InboundShipmentSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "WinLossReasonSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.WinLossReasonSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.types.CampaignStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "LandedCostSummary".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.LandedCostSummary.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountLocalizationsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.AccountLocalizationsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "CalendarEventResourceList".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventResourceList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchRecordType".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.SearchRecordType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Classification".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.Classification.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomTransactionLine".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomTransactionLine.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.AccountSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.BillingScheduleSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentB13AFilingOptionFedEx".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentB13AFilingOptionFedEx.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnStringCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PurchaseOrderOrderStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.types.PurchaseOrderOrderStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxDetailSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TaxDetailSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionChargeType".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.TransactionChargeType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:bank_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CheckExpense".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.bank_2017_2.CheckExpense.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorAddressbook".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.VendorAddressbook.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:bank_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CheckExpenseList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.bank_2017_2.CheckExpenseList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomFieldDepartmentAccess".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomFieldDepartmentAccess.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnSelectCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemDemandPlanSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.ItemDemandPlanSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.AccountSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Employee".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.Employee.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemNumberCustomFieldFilterList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.ItemNumberCustomFieldFilterList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SubsidiaryTaxRegistrationList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SubsidiaryTaxRegistrationList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GiftCertificateSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.GiftCertificateSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaskSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.TaskSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "Status".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.Status.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ListOrRecordRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.ListOrRecordRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "GlobalAccountMapping".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.GlobalAccountMapping.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DeletedRecordType".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.DeletedRecordType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ServiceItemTaskTemplatesList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ServiceItemTaskTemplatesList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "WorkOrderClose".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.WorkOrderClose.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingAccount".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.BillingAccount.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DeletedRecordList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.DeletedRecordList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SubsidiaryAccountingBookDetailList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SubsidiaryAccountingBookDetailList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SolutionSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SolutionSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "WinLossReason".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.WinLossReason.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TopicSolutionList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.TopicSolutionList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PartnerCategorySearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PartnerCategorySearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingRoutingSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingRoutingSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleYearDowim".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearDowim.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PostingTransactionSummaryField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.PostingTransactionSummaryField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AttachResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AttachResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "NoteSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.NoteSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CategoryList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CategoryList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PartnerSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.PartnerSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Customer".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.Customer.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "DiscountItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.DiscountItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SerializedInventoryItemLocationsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SerializedInventoryItemLocationsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:faults_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InvalidVersionFault".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.faults_2017_2.InvalidVersionFault.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SubtotalItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SubtotalItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:filecabinet_2017_2.documents.webservices.netsuite.com".equals(namespaceURI) &&
                  "Folder".equals(typeName)){
                   
                            return  com.netsuite.webservices.documents.filecabinet_2017_2.Folder.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TopicSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.TopicSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LotNumberedInventoryItemLocations".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.LotNumberedInventoryItemLocations.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetBudgetExchangeRateResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.GetBudgetExchangeRateResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesOrderItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.SalesOrderItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeSheetSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.TimeSheetSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransferOrder".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.TransferOrder.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeManagersList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeManagersList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "NoteTypeSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.NoteTypeSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemRevisionSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemRevisionSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecScheduleRecogIntervalSrc".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.RevRecScheduleRecogIntervalSrc.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "LocationSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.LocationSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "DepartmentSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.DepartmentSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxRounding".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.types.TaxRounding.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerDepositApplyList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CustomerDepositApplyList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchStringFieldOperator".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.SearchStringFieldOperator.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TaxType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesTaxItemSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SalesTaxItemSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PurchaseRequisitionItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.PurchaseRequisitionItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PriceLevelSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PriceLevelSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryAssignmentList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.InventoryAssignmentList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerPaymentCredit".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CustomerPaymentCredit.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeDirectDepositList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeDirectDepositList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "Record".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.Record.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingCostDetailList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingCostDetailList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecTemplateSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.RevRecTemplateSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "EntityCustomFieldFilterList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.EntityCustomFieldFilterList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BudgetCategory".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.BudgetCategory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaskReminderMinutes".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.TaskReminderMinutes.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageUspsDeliveryConfUsps".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUspsDeliveryConfUsps.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SerializedAssemblyItemBillOfMaterials".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SerializedAssemblyItemBillOfMaterials.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesOrderShipGroupList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.SalesOrderShipGroupList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashRefundSalesTeam".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CashRefundSalesTeam.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchDoubleFieldOperator".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.SearchDoubleFieldOperator.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "UsageSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.UsageSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleRecurrenceList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.BillingScheduleRecurrenceList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.types.ContactType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "DepositApplicationApply".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.DepositApplicationApply.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryNumberSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.InventoryNumberSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobTypeSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobTypeSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountingTransactionSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.AccountingTransactionSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "VsoePermitDiscount".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.VsoePermitDiscount.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:bank_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "DepositOtherList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.bank_2017_2.DepositOtherList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckJournalCompanyTaxList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckJournalCompanyTaxList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSupplyPlanSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.ItemSupplyPlanSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesTaxItemSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.SalesTaxItemSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "MatrixOptionList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.MatrixOptionList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransferOrderItemCommitInventory".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.types.TransferOrderItemCommitInventory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "Country".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.Country.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingRouting".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingRouting.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchDateField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GiftCertificateSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.GiftCertificateSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingOperationTaskPredecessorList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingOperationTaskPredecessorList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnTextNumberField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnTextNumberField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "FairValuePriceSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.FairValuePriceSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "HcmJobSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.HcmJobSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmailEmployeesList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.EmailEmployeesList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SiteCategory".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SiteCategory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.communication_2017_2.general.webservices.netsuite.com".equals(namespaceURI) &&
                  "MessageMessageType".equals(typeName)){
                   
                            return  com.netsuite.webservices.general.communication_2017_2.types.MessageMessageType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingOperationTaskStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.types.ManufacturingOperationTaskStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "UpsertListRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.UpsertListRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashSaleItemCost".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.CashSaleItemCost.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetCurrencyRateRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRateRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingOperationTaskSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingOperationTaskSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemReceipt".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.ItemReceipt.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "StringDimensionRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.StringDimensionRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "OtherChargePurchaseItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.OtherChargePurchaseItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomizationAccessLevel".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.types.CustomizationAccessLevel.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentHazmatTypeFedEx".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentHazmatTypeFedEx.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:general_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "StatisticalJournalEntryLineList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.general_2017_2.StatisticalJournalEntryLineList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:communication_2017_2.general.webservices.netsuite.com".equals(namespaceURI) &&
                  "NoteSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.general.communication_2017_2.NoteSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ResourceAllocationSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.ResourceAllocationSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransferOrderOrderStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.types.TransferOrderOrderStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetDeletedResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.GetDeletedResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckPayContribList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckPayContribList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignResponseResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.types.CampaignResponseResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleRepeatEvery".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRepeatEvery.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.VendorSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemAccountMappingSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemAccountMappingSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:bank_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CheckItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.bank_2017_2.CheckItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CustomRecordSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentTermsOfSaleFedEx".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentTermsOfSaleFedEx.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SupportCaseSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomizationDisplayType".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.types.CustomizationDisplayType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PartnersList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.PartnersList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "WorkOrderCompletionOperationList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.WorkOrderCompletionOperationList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeRoles".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeRoles.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesOrderOrderStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderOrderStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InvoiceItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.InvoiceItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaskSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.TaskSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PayrollItemSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.PayrollItemSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobStatusSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobStatusSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxGroupSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TaxGroupSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PromotionCodeItemsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.PromotionCodeItemsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryNumberLocations".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.InventoryNumberLocations.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerDepositApply".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CustomerDepositApply.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobPercentCompleteOverride".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobPercentCompleteOverride.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ItemSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InitializeRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.InitializeRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemAccountMappingItemAccount".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemAccountMappingItemAccount.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "NonInventorySaleItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.NonInventorySaleItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxGroupSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TaxGroupSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobPlStatementList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobPlStatementList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountingTransactionRevCommitStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.AccountingTransactionRevCommitStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SupportCaseSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.SupportCaseSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ExpenseCategorySearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ExpenseCategorySearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevenueCommitStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.RevenueCommitStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeBillSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TimeBillSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LeadSource".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.LeadSource.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Price".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.Price.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchEnumMultiSelectCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncAddListResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AsyncAddListResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.TransactionSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetListResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetListResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DateCustomFieldRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.DateCustomFieldRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "LandedCostSource".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.LandedCostSource.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordCustomFieldFilter".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordCustomFieldFilter.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "UnitsTypeSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.UnitsTypeSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemCostEstimateType".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.ItemCostEstimateType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetSelectValueFilter".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.GetSelectValueFilter.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerMessage".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.CustomerMessage.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SubsidiaryTaxRegistration".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SubsidiaryTaxRegistration.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckPayEarn".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckPayEarn.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingCostTemplateSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingCostTemplateSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryItemFraudRisk".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.InventoryItemFraudRisk.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionColumnCustomFieldFilterList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.TransactionColumnCustomFieldFilterList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetCustomizationType".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.GetCustomizationType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncGetListRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AsyncGetListRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackage".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackage.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignCategory".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignCategory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesRole".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SalesRole.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PeriodDemandPlanList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.PeriodDemandPlanList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentHomeDeliveryTypeFedEx".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentHomeDeliveryTypeFedEx.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountingPeriodSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.AccountingPeriodSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ChargeStage".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.types.ChargeStage.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InboundShipmentSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.InboundShipmentSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryAdjustmentInventoryList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.InventoryAdjustmentInventoryList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashRefundSalesTeamList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CashRefundSalesTeamList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesOrderItemFulfillmentChoice".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderItemFulfillmentChoice.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetSelectValueFilterOperator".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.GetSelectValueFilterOperator.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ExpenseReportExpenseList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.ExpenseReportExpenseList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CampaignSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "DepartmentSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.DepartmentSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:communication_2017_2.general.webservices.netsuite.com".equals(namespaceURI) &&
                  "NoteSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.general.communication_2017_2.NoteSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomListSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CustomListSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchRowList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchRowList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "IssueSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.IssueSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PromotionCodeSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PromotionCodeSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetServerTimeResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.GetServerTimeResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerCreditHoldOverride".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.types.CustomerCreditHoldOverride.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:financial_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "BudgetSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.financial_2017_2.BudgetSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecScheduleAmortizationStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.RevRecScheduleAmortizationStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "MessageSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.MessageSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.ContactSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PartnerInfo".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.PartnerInfo.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "FairValuePriceSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.FairValuePriceSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSource".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.ItemSource.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CalendarEventAttendeeResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.CalendarEventAttendeeResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PricingSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PricingSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "Task".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.Task.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ProjectTaskSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PostingPeriodDate".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.PostingPeriodDate.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PartnerSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.PartnerSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.BillingScheduleSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "DemandPlanMonth".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.types.DemandPlanMonth.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeSheetSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.TimeSheetSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "LogoutResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.LogoutResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignCampaignDirectMailStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.types.CampaignCampaignDirectMailStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PriceLevel".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PriceLevel.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TokenPassport".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.TokenPassport.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactRole".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ContactRole.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxTypeSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TaxTypeSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomizationDynamicDefault".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.types.CustomizationDynamicDefault.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetAllRecord".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.GetAllRecord.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:website_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SiteCategorySearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.website_2017_2.SiteCategorySearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxGroupTaxItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TaxGroupTaxItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:faults_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InvalidAccountFault".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.faults_2017_2.InvalidAccountFault.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountingBookDetail".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.AccountingBookDetail.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "BinTransfer".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.BinTransfer.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignCampaignEventStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.types.CampaignCampaignEventStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemDemandPlanSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ItemDemandPlanSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PurchaseOrderItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.PurchaseOrderItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckPayPto".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckPayPto.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PresentationItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PresentationItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SiteCategoryList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SiteCategoryList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncUpsertListRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertListRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InterCompanyTransferOrderItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.InterCompanyTransferOrderItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SolutionSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SolutionSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTranslations".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTranslations.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:website_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SiteCategoryTranslation".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.website_2017_2.SiteCategoryTranslation.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorReturnAuthorizationExpenseList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorReturnAuthorizationExpenseList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageUps".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageUps.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorReturnAuthorization".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorReturnAuthorization.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PromotionCodeCurrencyList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.PromotionCodeCurrencyList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ProductFeedList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ProductFeedList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "CalendarEventResource".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventResource.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxGroupSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TaxGroupSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PurchaseRequisitionExpenseList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.PurchaseRequisitionExpenseList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:general_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "AdvInterCompanyJournalEntryLine".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.general_2017_2.AdvInterCompanyJournalEntryLine.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "RecordRefList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.RecordRefList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleMilestoneList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.BillingScheduleMilestoneList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LotNumberedAssemblyItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.LotNumberedAssemblyItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerMessageSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.CustomerMessageSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LocationBusinessHours".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.LocationBusinessHours.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashSaleTimeList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.CashSaleTimeList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckJournalCompanyTax".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckJournalCompanyTax.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeBill".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.TimeBill.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxFractionUnit".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.types.TaxFractionUnit.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleMonthDow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleMonthDow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CostCategorySearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.CostCategorySearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InboundShipmentShipmentStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.types.InboundShipmentShipmentStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchNextResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.SearchNextResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.faults_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "FaultCodeType".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.faults_2017_2.types.FaultCodeType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorCategorySearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.VendorCategorySearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:bank_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "Deposit".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.bank_2017_2.Deposit.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SupportCaseEscalateTo".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SupportCaseEscalateTo.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchStringField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ExpenseCategorySearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ExpenseCategorySearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:financial_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "BudgetSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.financial_2017_2.BudgetSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AddListRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AddListRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnLongField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "WorkOrderCompletionOperation".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.WorkOrderCompletionOperation.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncAddListRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AsyncAddListRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecScheduleSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.RevRecScheduleSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemCostingMethod".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemCostingMethod.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeTabs".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeTabs.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetAsyncResultRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetAsyncResultRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeFieldList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeFieldList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomizationType".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.CustomizationType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckJournal".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckJournal.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingLagType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.types.ManufacturingLagType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "Partners".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.Partners.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "CalendarEventReminderType".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventReminderType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.filecabinet_2017_2.documents.webservices.netsuite.com".equals(namespaceURI) &&
                  "TextFileEncoding".equals(typeName)){
                   
                            return  com.netsuite.webservices.documents.filecabinet_2017_2.types.TextFileEncoding.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AttachRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AttachRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "MseSubsidiarySearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.MseSubsidiarySearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ResourceAllocationSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.ResourceAllocationSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LotNumberedInventoryItemNumbers".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.LotNumberedInventoryItemNumbers.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Bin".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.Bin.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:general_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "StatisticalJournalEntry".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.general_2017_2.StatisticalJournalEntry.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "IssueVersion".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.IssueVersion.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SerializedInventoryItemNumbersList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SerializedInventoryItemNumbersList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "UsageSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.UsageSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecTemplateRecurrence".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.RevRecTemplateRecurrence.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorCreditApplyList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorCreditApplyList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeDirectDepositAccountStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.types.EmployeeDirectDepositAccountStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchLongFieldOperator".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.SearchLongFieldOperator.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CostCategorySearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CostCategorySearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "GiftCertificateItemAuthCodesList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.GiftCertificateItemAuthCodesList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashSaleExpCost".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.CashSaleExpCost.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CreditMemo".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CreditMemo.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerRefundDepositList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CustomerRefundDepositList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaymentMethodSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PaymentMethodSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "StatusDetail".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.StatusDetail.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.financial_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "BudgetBudgetType".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.financial_2017_2.types.BudgetBudgetType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "NoteTypeSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.NoteTypeSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CurrencyRateFilter".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.CurrencyRateFilter.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobMilestones".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobMilestones.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InboundShipmentSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.InboundShipmentSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnDateField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleYearDowimMonth".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleYearDowimMonth.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "EntityCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.EntityCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:faults_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InsufficientPermissionFault".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFault.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxAcctType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.TaxAcctType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PartnerAddressbook".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.PartnerAddressbook.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "OtherNameCategorySearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.OtherNameCategorySearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchPreferences".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.SearchPreferences.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:general_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "JournalEntryLine".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.general_2017_2.JournalEntryLine.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ScheduleBCode".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ScheduleBCode.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InvoiceExpCostList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.InvoiceExpCostList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesOrderItemCommitInventory".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.SalesOrderItemCommitInventory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "OpportunitySearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.OpportunitySearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "UnitsTypeSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.UnitsTypeSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ReadResponseList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.ReadResponseList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SolutionSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.SolutionSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ExpenseCategoryRatesList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ExpenseCategoryRatesList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SupportCaseStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SupportCaseStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ProjectTaskPredecessor".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskPredecessor.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageFedExCodFreightTypeFedEx".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExCodFreightTypeFedEx.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EntityGroupSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.EntityGroupSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemRevision".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemRevision.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PhoneCallSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PhoneCallSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ResourceAllocationSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ResourceAllocationSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EntityGroupSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.EntityGroupSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PricingSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PricingSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PromotionCodePartnersList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.PromotionCodePartnersList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerAddressbookList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerAddressbookList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSupplyPlanOrder".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.ItemSupplyPlanOrder.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "IssueEventStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.types.IssueEventStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LotNumberedInventoryItemLocationsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.LotNumberedInventoryItemLocationsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CreditMemoItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CreditMemoItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:website_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SiteCategoryTranslationList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.website_2017_2.SiteCategoryTranslationList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "CalendarEventSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerCategorySearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CustomerCategorySearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomSearchJoin".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CustomSearchJoin.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountingPeriodFiscalCalendars".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.AccountingPeriodFiscalCalendars.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InitializeRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.InitializeRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashRefundItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CashRefundItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerRefund".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CustomerRefund.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SubsidiaryAccountingBookDetail".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SubsidiaryAccountingBookDetail.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "GlobalAccountMappingSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.GlobalAccountMappingSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeEntrySearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.TimeEntrySearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:faults_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "UnexpectedErrorFault".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFault.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageUpsPackagingUps".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUpsPackagingUps.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Nexus".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.Nexus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CurrencyCurrencyPrecision".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.CurrencyCurrencyPrecision.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemVendorList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemVendorList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnEnumMultiSelectCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumMultiSelectCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AssemblyItemBomSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.AssemblyItemBomSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeAccruedTimeAccrualMethod".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.types.EmployeeAccruedTimeAccrualMethod.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Pricing".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.Pricing.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "NonInventoryResaleItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.NonInventoryResaleItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "EstimateShipGroupList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.EstimateShipGroupList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "RecordRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AddressSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.AddressSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:filecabinet_2017_2.documents.webservices.netsuite.com".equals(namespaceURI) &&
                  "FolderSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.documents.filecabinet_2017_2.FolderSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemMatrixType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemMatrixType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.CustomRecordRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "DepartmentSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.DepartmentSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorBillItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorBillItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CscMatchCode".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.CscMatchCode.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CouponCode".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CouponCode.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemDemandSource".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemDemandSource.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "IssueTrackCode".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.types.IssueTrackCode.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ProjectTaskPredecessorPredecessorType".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.ProjectTaskPredecessorPredecessorType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "WorkOrderCompletionComponent".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.WorkOrderCompletionComponent.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "Promotions".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.Promotions.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "Estimate".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.Estimate.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingOperationTaskSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingOperationTaskSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeSheetSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TimeSheetSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EntityType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.types.EntityType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "WorkOrderOrderStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.types.WorkOrderOrderStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemEbayRelistingOption".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemEbayRelistingOption.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:faults_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ExceededRecordCountFault".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFault.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SupportCase".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SupportCase.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashFlowRateType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.CashFlowRateType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesTaxItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SalesTaxItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "IssueSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.IssueSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "UsageSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.UsageSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashRefundPartnersList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CashRefundPartnersList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesRoleSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SalesRoleSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PurchaseRequisition".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.PurchaseRequisition.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SupportCaseTimeItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SupportCaseTimeItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SessionResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.SessionResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorCredit".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorCredit.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentAccessibilityTypeFedEx".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentAccessibilityTypeFedEx.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InitializeAuxRefType".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.InitializeAuxRefType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactRoleSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ContactRoleSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "OpportunitySalesTeamList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.OpportunitySalesTeamList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "LongCustomFieldRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.LongCustomFieldRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashSaleItemCostList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.CashSaleItemCostList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InboundShipmentSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.InboundShipmentSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CouponCodeSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CouponCodeSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ProjectTaskSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerStatusSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerStatusSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckPayTime".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckPayTime.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "CalendarEventSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ProjectTaskConstraintType".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.ProjectTaskConstraintType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ChangeEmailRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.ChangeEmailRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "FolderSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.FolderSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SsoLoginResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.SsoLoginResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetSelectFilterByFieldValueList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.GetSelectFilterByFieldValueList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ContactSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "OpportunityStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.OpportunityStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemBinNumberSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ItemBinNumberSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountingPeriodSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.AccountingPeriodSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecSchedule".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.RevRecSchedule.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemAccountMappingSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ItemAccountMappingSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InvoiceItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.InvoiceItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LotNumberedAssemblyItemBillOfMaterialsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.LotNumberedAssemblyItemBillOfMaterialsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "IssueSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.IssueSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckPayContrib".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckPayContrib.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Gender".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.types.Gender.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "UnitsType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.UnitsType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CurrencyRateSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.CurrencyRateSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypePermissionsPermittedLevel".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.types.CustomRecordTypePermissionsPermittedLevel.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "FairValuePrice".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.FairValuePrice.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LotNumberedInventoryItemNumbersList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.LotNumberedInventoryItemNumbersList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetPostingTransactionSummaryResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.GetPostingTransactionSummaryResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Vendor".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.Vendor.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "NoteTypeSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.NoteTypeSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashSalePartnersList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.CashSalePartnersList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InvoiceShipGroupList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.InvoiceShipGroupList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "AssemblyComponentList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.AssemblyComponentList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:faults_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ExceededUsageLimitFault".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFault.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnEnumSelectField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobTypeSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.JobTypeSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemEbayAuctionDuration".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemEbayAuctionDuration.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionApprovalStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.TransactionApprovalStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerRefundApplyList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CustomerRefundApplyList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Subsidiary".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.Subsidiary.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LocationSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.LocationSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ReturnAuthorizationPartnersList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationPartnersList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorRoles".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.VendorRoles.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DeletionReason".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.DeletionReason.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CalendarEventSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CalendarEventSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemAccountMappingSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemAccountMappingSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ResourceAllocationSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ResourceAllocationSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:general_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InterCompanyJournalEntryLineList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.general_2017_2.InterCompanyJournalEntryLineList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecScheduleSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.RevRecScheduleSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SubsidiarySearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.SubsidiarySearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeEntrySearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TimeEntrySearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorPayment".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorPayment.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchMoreResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.SearchMoreResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CouponCodeSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CouponCodeSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryTransfer".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.InventoryTransfer.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "CalendarEventAttendeeList".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventAttendeeList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InitializeRecord".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.InitializeRecord.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ItemSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomFieldTranslationsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomFieldTranslationsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentAncillaryEndorsementFedEx".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentAncillaryEndorsementFedEx.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "HcmJobSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.HcmJobSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeSheet".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.TimeSheet.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryItemBinNumber".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.InventoryItemBinNumber.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PresentationItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PresentationItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "WorkOrderIssueComponentList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.WorkOrderIssueComponentList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "UpdateResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.UpdateResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomFieldDepartmentAccessList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomFieldDepartmentAccessList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncInitializeListResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeListResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeAddressbook".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeAddressbook.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnDoubleField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ForecastType".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ForecastType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxTypeSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TaxTypeSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerRefundDeposit".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CustomerRefundDeposit.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:general_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "AdvInterCompanyJournalEntry".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.general_2017_2.AdvInterCompanyJournalEntry.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "UnitsTypeUom".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.UnitsTypeUom.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashSale".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.CashSale.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Location".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.Location.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingOperationTaskSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingOperationTaskSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingRoutingRoutingStep".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingRoutingRoutingStep.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ApplicationInfo".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.ApplicationInfo.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "OtherChargeSaleItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.OtherChargeSaleItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:general_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "JournalEntry".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.general_2017_2.JournalEntry.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "NonInventoryPurchaseItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.NonInventoryPurchaseItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "HcmJobSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.HcmJobSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionPaymentEventType".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.TransactionPaymentEventType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemReceiptExpenseList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.ItemReceiptExpenseList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeAccruedTime".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeAccruedTime.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetAllResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetAllResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SelectCustomFieldRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SelectCustomFieldRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryItemBinNumberList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.InventoryItemBinNumberList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryCostRevaluationCostComponentList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.InventoryCostRevaluationCostComponentList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Department".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.Department.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "IssueSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.IssueSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemReceiptItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.ItemReceiptItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ProjectTaskAssignmentSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ProjectTaskAssignmentSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SupportCasePriority".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SupportCasePriority.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CostCategoryItemCostType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.CostCategoryItemCostType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "WorkOrderIssueComponent".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.WorkOrderIssueComponent.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "LandedCost".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.LandedCost.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemOptionCustomFieldFilter".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.ItemOptionCustomFieldFilter.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemOptionCustomFieldFilterList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.ItemOptionCustomFieldFilterList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "PhoneCallSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.PhoneCallSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "LogoutRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.LogoutRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "BudgetSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.BudgetSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemBinNumberSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ItemBinNumberSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountingPeriod".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.AccountingPeriod.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PurchaseRequisitionItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.PurchaseRequisitionItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxTypeNexusAccountsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TaxTypeNexusAccountsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchBooleanField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeBaseWageType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.types.EmployeeBaseWageType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryDetail".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.InventoryDetail.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignChannelEventType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.types.CampaignChannelEventType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "VsoeSopGroup".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.VsoeSopGroup.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TopicSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TopicSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashRefundItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CashRefundItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetItemAvailabilityResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetItemAvailabilityResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryAssignment".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.InventoryAssignment.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ChangePassword".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.ChangePassword.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignEmail".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignEmail.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncStatusResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.AsyncStatusResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageUpsDeliveryConfUps".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUpsDeliveryConfUps.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "GiftCertificateItemAuthCodes".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.GiftCertificateItemAuthCodes.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnDateCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorCreditExpense".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorCreditExpense.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DeleteListResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.DeleteListResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DepartmentSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.DepartmentSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:general_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InterCompanyJournalEntry".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.general_2017_2.InterCompanyJournalEntry.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CurrencyFxRateUpdateTimezone".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.CurrencyFxRateUpdateTimezone.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CouponCodeSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CouponCodeSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "GiftCertificate".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.GiftCertificate.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentLicenseExceptionUps".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentLicenseExceptionUps.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "RecurrenceDowMaskList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.RecurrenceDowMaskList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:filecabinet_2017_2.documents.webservices.netsuite.com".equals(namespaceURI) &&
                  "File".equals(typeName)){
                   
                            return  com.netsuite.webservices.documents.filecabinet_2017_2.File.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorOtherRelationships".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.types.VendorOtherRelationships.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryNumberBinSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.InventoryNumberBinSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomFieldSubAccessList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomFieldSubAccessList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.VendorSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.JobSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeSublists".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeSublists.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PriceLevelSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PriceLevelSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageUspsPackagingUsps".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageUspsPackagingUsps.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerMessageSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CustomerMessageSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesRoleSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.SalesRoleSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ExclusionDateList".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.ExclusionDateList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaymentItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PaymentItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ChangePasswordResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.ChangePasswordResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorCategory".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.VendorCategory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeOnlineForms".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeOnlineForms.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeTranslations".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeTranslations.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerNumberFormat".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.types.CustomerNumberFormat.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CurrencyLocale".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.CurrencyLocale.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:demandplanning_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemDemandPlanSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.demandplanning_2017_2.ItemDemandPlanSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesOrderSalesTeam".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.SalesOrderSalesTeam.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnLongCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnLongCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "LandedCostMethod".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.LandedCostMethod.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountingTransactionRevenueStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.AccountingTransactionRevenueStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerStatusStage".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.types.CustomerStatusStage.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "BinWorksheetItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.BinWorksheetItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PriceList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PriceList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PurchaseOrderExpense".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.PurchaseOrderExpense.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchEnumMultiSelectField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PromotionCodeSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.PromotionCodeSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaymentMethodSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PaymentMethodSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "UpdateInviteeStatusResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "OtherNameCategory".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.OtherNameCategory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PartnerCategorySearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PartnerCategorySearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "HazmatPackingGroup".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.HazmatPackingGroup.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckPayDisburse".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckPayDisburse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "UpsertListResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.UpsertListResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomListSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomListSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingRates".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.BillingRates.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.filecabinet_2017_2.documents.webservices.netsuite.com".equals(namespaceURI) &&
                  "FileAttachFrom".equals(typeName)){
                   
                            return  com.netsuite.webservices.documents.filecabinet_2017_2.types.FileAttachFrom.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryDetailSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.InventoryDetailSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "OpportunitySearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.OpportunitySearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ExpenseCategorySearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ExpenseCategorySearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "PhoneCallStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.PhoneCallStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:financial_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "Budget".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.financial_2017_2.Budget.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxAcct".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TaxAcct.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingSchedule".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.BillingSchedule.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ReturnAuthorizationOrderStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.types.ReturnAuthorizationOrderStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ChangeEmail".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.ChangeEmail.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CostCategorySearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CostCategorySearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchBooleanCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchBooleanCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecTemplateSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.RevRecTemplateSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InitializeResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.InitializeResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaskTimeItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.TaskTimeItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "NexusSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.NexusSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemCustomFieldItemSubType".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.types.ItemCustomFieldItemSubType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ClassificationSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ClassificationSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevenueStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.RevenueStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.ContactSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorCategorySearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.VendorCategorySearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerItemPricing".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerItemPricing.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemCustomFieldFilter".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.ItemCustomFieldFilter.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PartnerSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PartnerSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxGroupTaxItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TaxGroupTaxItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CrmCustomFieldFilterList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CrmCustomFieldFilterList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeManagers".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeManagers.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetBudgetExchangeRateRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRateRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactCategorySearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ContactCategorySearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignResponseResponses".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignResponseResponses.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnDoubleCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CurrencyRate".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.CurrencyRate.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CouponCodeSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CouponCodeSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Campaign".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.Campaign.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ProjectTaskAssigneeList".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskAssigneeList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSupplyReplenishmentMethod".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemSupplyReplenishmentMethod.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobPercentCompleteOverrideList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobPercentCompleteOverrideList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignSearchEngine".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignSearchEngine.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchMoreWithIdResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithIdResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GroupMemberSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.GroupMemberSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleFrequency".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleFrequency.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CurrencyRateSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CurrencyRateSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "PhoneCallTimeItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.PhoneCallTimeItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Rate".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.Rate.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerPartnersList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerPartnersList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TimeItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncUpdateListResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateListResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PricingGroupSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PricingGroupSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:filecabinet_2017_2.documents.webservices.netsuite.com".equals(namespaceURI) &&
                  "FileSiteCategory".equals(typeName)){
                   
                            return  com.netsuite.webservices.documents.filecabinet_2017_2.FileSiteCategory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemMember".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemMember.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageUpsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageUpsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "UnitsTypeSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.UnitsTypeSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountLocalizations".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.AccountLocalizations.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetDeletedFilter".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.GetDeletedFilter.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CurrencyRateSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CurrencyRateSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InboundShipment".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.InboundShipment.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TermSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TermSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ReturnAuthorizationSalesTeamList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorizationSalesTeamList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecTemplateSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.RevRecTemplateSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DurationUnit".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.DurationUnit.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CrmCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CrmCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LotNumberedInventoryItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.LotNumberedInventoryItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ConsolidatedExchangeRateSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ConsolidatedExchangeRateSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchMoreRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.SearchMoreRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckPayPtoList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckPayPtoList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:bank_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "DepositCashBack".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.bank_2017_2.DepositCashBack.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckPayTimeList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckPayTimeList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeAddressbookList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeAddressbookList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleMilestone".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.BillingScheduleMilestone.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:general_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InterCompanyJournalEntryAccountingBookDetail".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.general_2017_2.InterCompanyJournalEntryAccountingBookDetail.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnCustomFieldList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "RecurrenceDowim".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.RecurrenceDowim.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.AccountType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageUspsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageUspsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemReceiptItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.ItemReceiptItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PromotionCode".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.PromotionCode.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SitemapPriority".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.SitemapPriority.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckJournalDeductionList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckJournalDeductionList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingRoutingSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ManufacturingRoutingSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "NexusSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.NexusSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DataCenterUrls".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.DataCenterUrls.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "UpsertRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.UpsertRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ServiceResaleItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ServiceResaleItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetSavedSearchResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.GetSavedSearchResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeEmergencyContact".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeEmergencyContact.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "BinTransferInventory".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.BinTransferInventory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "State".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.State.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactCategorySearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ContactCategorySearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "NullField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorRolesList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.VendorRolesList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SupportCaseOrigin".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SupportCaseOrigin.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ProjectTaskPriority".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.ProjectTaskPriority.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeSheetSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TimeSheetSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EntityGroup".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.EntityGroup.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerCategorySearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CustomerCategorySearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "UsageSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.UsageSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionBillVarianceStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.types.TransactionBillVarianceStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PurchaseOrder".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.PurchaseOrder.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.filecabinet_2017_2.documents.webservices.netsuite.com".equals(namespaceURI) &&
                  "MediaType".equals(typeName)){
                   
                            return  com.netsuite.webservices.documents.filecabinet_2017_2.types.MediaType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemWeightUnit".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemWeightUnit.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeHrEducationList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeHrEducationList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.BillingScheduleSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SolutionsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SolutionsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaskStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.TaskStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.ContactSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomListCustomValueList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomListCustomValueList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeAccruedTimeList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeAccruedTimeList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "FairValuePriceSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.FairValuePriceSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryCostRevaluationCostComponent".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.InventoryCostRevaluationCostComponent.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingCostTemplateSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingCostTemplateSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DeleteListRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.DeleteListRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxTypeNexusesTaxList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TaxTypeNexusesTaxList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountingTransactionSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.AccountingTransactionSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerRefundApply".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CustomerRefundApply.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ConsolidatedExchangeRateSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ConsolidatedExchangeRateSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SsoCredentials".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SsoCredentials.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerMonthlyClosing".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.types.CustomerMonthlyClosing.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobPlStatement".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobPlStatement.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "WinLossReasonSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.WinLossReasonSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ServiceSaleItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ServiceSaleItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "CalendarEventReminderMinutes".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventReminderMinutes.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.VendorSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.filecabinet_2017_2.documents.webservices.netsuite.com".equals(namespaceURI) &&
                  "FolderFolderType".equals(typeName)){
                   
                            return  com.netsuite.webservices.documents.filecabinet_2017_2.types.FolderFolderType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:bank_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "DepositCashBackList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.bank_2017_2.DepositCashBackList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckPayTaxList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckPayTaxList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorBill".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorBill.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeTerminationRegretted".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.types.EmployeeTerminationRegretted.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerCurrencyList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerCurrencyList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:general_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InterCompanyJournalEntryLine".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.general_2017_2.InterCompanyJournalEntryLine.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:bank_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "DepositOther".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.bank_2017_2.DepositOther.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxTypeSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TaxTypeSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "MseSubsidiarySearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.MseSubsidiarySearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemAtpMethod".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemAtpMethod.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.CustomerSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxTypeSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TaxTypeSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckPayTax".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckPayTax.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomTransactionRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.CustomTransactionRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobStatusSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.JobStatusSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PayrollItemItemTypeNoHierarchy".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.types.PayrollItemItemTypeNoHierarchy.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageFedExPriorityAlertTypeFedEx".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExPriorityAlertTypeFedEx.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeEarningList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeEarningList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactAddressbookList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.ContactAddressbookList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemOptionsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemOptionsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "WorkOrderItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.WorkOrderItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LocationBusinessHoursList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.LocationBusinessHoursList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.BillingScheduleSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncStatusResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AsyncStatusResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "EntityCustomFieldFilter".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.EntityCustomFieldFilter.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashSaleShipGroupList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.CashSaleShipGroupList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSupplyPlanSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ItemSupplyPlanSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchDateFieldOperator".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.types.SearchDateFieldOperator.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageFedExAdmPackageTypeFedEx".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExAdmPackageTypeFedEx.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.faults_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "StatusDetailType".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.faults_2017_2.types.StatusDetailType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckJournalCompanyContribution".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckJournalCompanyContribution.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SupportCaseSolutionsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SupportCaseSolutionsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorPaymentCreditList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorPaymentCreditList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypePermissions".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypePermissions.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountingPeriodFiscalCalendarsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.AccountingPeriodFiscalCalendarsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InitializeAuxRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.InitializeAuxRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleRecurrence".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.BillingScheduleRecurrence.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerCategorySearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.CustomerCategorySearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeRatesList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeRatesList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "EstimateSalesTeam".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.EstimateSalesTeam.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchMultiSelectField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignEventResponseList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignEventResponseList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorCategorySearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.VendorCategorySearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DoubleCustomFieldRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.DoubleCustomFieldRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "BooleanCustomFieldRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.BooleanCustomFieldRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.SearchResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchColumnMultiSelectCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchColumnMultiSelectCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "BinSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.BinSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorPricingSchedule".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.VendorPricingSchedule.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AddressSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.AddressSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckJournalCompanyContributionList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckJournalCompanyContributionList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PartnerPromoCode".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.PartnerPromoCode.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "MapSsoRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.MapSsoRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:website_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SiteCategorySearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.website_2017_2.SiteCategorySearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TermSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TermSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "UsageSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.UsageSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "PhoneCallSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.PhoneCallSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LocationType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.LocationType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "OpportunitySearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.OpportunitySearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingAccountSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.BillingAccountSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "OpportunitySearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.OpportunitySearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "Address".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.Address.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomizationRefList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.CustomizationRefList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "EstimateItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.EstimateItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ExpenseReportExpense".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.ExpenseReportExpense.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "OtherNameCategorySearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.OtherNameCategorySearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemAccountMappingSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemAccountMappingSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomTransactionLineList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomTransactionLineList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PricingMatrix".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PricingMatrix.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaskContactList".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.TaskContactList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashSaleTime".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.CashSaleTime.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "PhoneCallContact".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.PhoneCallContact.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AddResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AddResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GlobalSubscriptionStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.GlobalSubscriptionStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSupplyPlanSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ItemSupplyPlanSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InboundShipmentItems".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.InboundShipmentItems.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PricingGroupSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PricingGroupSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeHrEducation".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeHrEducation.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignCampaignEventType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.types.CampaignCampaignEventType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EntityGroupSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.EntityGroupSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerItemPricingList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerItemPricingList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "KitItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.KitItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorReturnAuthorizationItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorReturnAuthorizationItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerStatusSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerStatusSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "CalendarEventStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "FairValuePriceFairValueRangePolicy".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.FairValuePriceFairValueRangePolicy.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryNumberSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.InventoryNumberSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PartnerCategorySearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PartnerCategorySearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CreditMemoSalesTeamList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CreditMemoSalesTeamList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PostingTransactionSummaryList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.PostingTransactionSummaryList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleMonthDowim".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleMonthDowim.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaskReminderType".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.TaskReminderType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerSalesTeamList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerSalesTeamList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemOverheadType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemOverheadType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ChangeEmailResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.ChangeEmailResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesTaxItemSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.SalesTaxItemSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactCategorySearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ContactCategorySearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ReturnAuthorization".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.ReturnAuthorization.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckPayDisburseList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckPayDisburseList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:communication_2017_2.general.webservices.netsuite.com".equals(namespaceURI) &&
                  "MessageMediaItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.general.communication_2017_2.MessageMediaItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncDeleteListRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteListRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignEventResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignEventResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionBodyCustomFieldFilterList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.TransactionBodyCustomFieldFilterList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "AssemblyComponent".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.AssemblyComponent.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeFormsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeFormsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchLongCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchLongCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SearchCustomFieldList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryItemLocationsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.InventoryItemLocationsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:general_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InterCompanyJournalEntryAccountingBookDetailList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.general_2017_2.InterCompanyJournalEntryAccountingBookDetailList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingCostTemplateSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ManufacturingCostTemplateSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LocationTimeZone".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.LocationTimeZone.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerCreditCardsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerCreditCardsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomFieldSubAccess".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomFieldSubAccess.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeChildren".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeChildren.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "EstimatePartnersList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.EstimatePartnersList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "LocationSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.LocationSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaskPriority".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.TaskPriority.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InvoiceExpCost".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.InvoiceExpCost.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorBillExpenseList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorBillExpenseList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemCustomFieldFilterList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.ItemCustomFieldFilterList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SsoLoginRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.SsoLoginRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PermissionCode".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.PermissionCode.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.VendorSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "WorkOrder".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.WorkOrder.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "BinTransferInventoryList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.BinTransferInventoryList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "AssemblyItemBillOfMaterials".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.AssemblyItemBillOfMaterials.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerCreditCards".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerCreditCards.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:faults_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ExceededRequestSizeFault".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFault.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GlobalAccountMappingSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.GlobalAccountMappingSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaxTypeSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.TaxTypeSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "StringCustomFieldRef".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.StringCustomFieldRef.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaymentMethodVisualsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PaymentMethodVisualsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemRevisionSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemRevisionSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncInitializeListRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeListRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:faults_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "InvalidSessionFault".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFault.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SupportCaseSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SupportCaseSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingCostTemplate".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingCostTemplate.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeDeductionList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeDeductionList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "OtherCustomFieldFilterList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.OtherCustomFieldFilterList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:communication_2017_2.general.webservices.netsuite.com".equals(namespaceURI) &&
                  "MessageSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.general.communication_2017_2.MessageSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemOverallQuantityPricingType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemOverallQuantityPricingType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemAccountMapping".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ItemAccountMapping.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CostCategorySearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.CostCategorySearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PricingGroupSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PricingGroupSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageFedExList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageFedExList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PartnerAddressbookList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.PartnerAddressbookList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AvsMatchCode".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.types.AvsMatchCode.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PartnerSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.PartnerSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryAdjustment".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.InventoryAdjustment.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingAccountSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.BillingAccountSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "JobTypeSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.JobTypeSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomFieldRoleAccessList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomFieldRoleAccessList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TermSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TermSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypePermissionsRestriction".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.types.CustomRecordTypePermissionsRestriction.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DimensionList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.DimensionList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ClassificationSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ClassificationSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignSubscription".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignSubscription.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TaskSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TaskSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryAdjustmentInventory".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.InventoryAdjustmentInventory.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeDeduction".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeDeduction.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:faults_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ExceededConcurrentRequestLimitFault".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFault.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "OpportunityItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.OpportunityItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorCategorySearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.VendorCategorySearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesOrderPartnersList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.SalesOrderPartnersList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ProjectTaskAssignmentSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ProjectTaskAssignmentSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "BillingScheduleRecurrenceMode".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.BillingScheduleRecurrenceMode.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetSelectValueResult".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.GetSelectValueResult.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "Invoice".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.Invoice.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customers_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CreditMemoItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.customers_2017_2.CreditMemoItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PricingGroupSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PricingGroupSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemSupplyType".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemSupplyType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetAllRequest".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetAllRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecScheduleSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.RevRecScheduleSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemCustomField".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.ItemCustomField.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "InventoryNumberSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.InventoryNumberSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:general_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "AdvInterCompanyJournalEntryAccountingBookDetailList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.general_2017_2.AdvInterCompanyJournalEntryAccountingBookDetailList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ResourceAllocationAllocationUnit".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.ResourceAllocationAllocationUnit.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "WorkOrderSchedulingMethod".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.types.WorkOrderSchedulingMethod.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PaycheckPayDeductList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.PaycheckPayDeductList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerDownloadList".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.CustomerDownloadList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:financial_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "BudgetSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.financial_2017_2.BudgetSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:filecabinet_2017_2.documents.webservices.netsuite.com".equals(namespaceURI) &&
                  "FileSiteCategoryList".equals(typeName)){
                   
                            return  com.netsuite.webservices.documents.filecabinet_2017_2.FileSiteCategoryList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "AssemblyItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.AssemblyItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomerStage".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.types.CustomerStage.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "NexusSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.NexusSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SerializedInventoryItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SerializedInventoryItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingRoutingSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ManufacturingRoutingSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:communication_2017_2.general.webservices.netsuite.com".equals(namespaceURI) &&
                  "Message".equals(typeName)){
                   
                            return  com.netsuite.webservices.general.communication_2017_2.Message.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "InvoiceTime".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.InvoiceTime.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountingBookDetailList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.AccountingBookDetailList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageUsps".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.ItemFulfillmentPackageUsps.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:marketing_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "CampaignOffer".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.marketing_2017_2.CampaignOffer.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactCategorySearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.ContactCategorySearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "PromotionCodeSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.PromotionCodeSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetCurrencyRateResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRateResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "CashSaleSalesTeamList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.CashSaleSalesTeamList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "ProjectTaskAssignee".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskAssignee.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactRoleSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ContactRoleSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "VendorCreditApply".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.VendorCreditApply.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SubsidiarySearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SubsidiarySearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ExpenseReport".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.ExpenseReport.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Currency".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.Currency.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SolutionStatus".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.types.SolutionStatus.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingOperationTaskPredecessor".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingOperationTaskPredecessor.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemFulfillmentPackageFedExCodMethodFedEx".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.ItemFulfillmentPackageFedExCodMethodFedEx.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesTaxItemSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.SalesTaxItemSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ContactSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ContactSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:supplychain_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingOperationTask".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.supplychain_2017_2.ManufacturingOperationTask.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemAvailability".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.ItemAvailability.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CurrencyRate".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.CurrencyRate.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TimeSheetTimeGrid".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.employees_2017_2.TimeSheetTimeGrid.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesOrderItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.SalesOrderItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ManufacturingOperationTaskSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ManufacturingOperationTaskSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CrmCustomFieldFilter".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CrmCustomFieldFilter.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetAsyncResultResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetAsyncResultResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypeLinksList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypeLinksList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "PhoneCallReminderType".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.types.PhoneCallReminderType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SalesRoleSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.SalesRoleSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "TransactionLinkType".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.types.TransactionLinkType.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:purchases_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "PurchaseOrderItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.purchases_2017_2.PurchaseOrderItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "TopicSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.TopicSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "DescriptionItem".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.DescriptionItem.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "Subscriptions".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.Subscriptions.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "PriceLevelSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.PriceLevelSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemRevisionSearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.ItemRevisionSearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomFieldList".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:employees_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmployeeCompanyContribution".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.employees_2017_2.EmployeeCompanyContribution.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "RevRecScheduleSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.RevRecScheduleSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:support_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "SolutionSearch".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.support_2017_2.SolutionSearch.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "OpportunityItemList".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.OpportunityItemList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "CalendarEvent".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.CalendarEvent.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:faults_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "AsyncFault".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.faults_2017_2.AsyncFault.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SiteCategorySearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.SiteCategorySearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:bank_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "Check".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.bank_2017_2.Check.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "DepartmentSearchRowBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.DepartmentSearchRowBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "AccountingTransactionSearchRow".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.AccountingTransactionSearchRow.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "LocationSearchAdvanced".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.LocationSearchAdvanced.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:scheduling_2017_2.activities.webservices.netsuite.com".equals(namespaceURI) &&
                  "PhoneCall".equals(typeName)){
                   
                            return  com.netsuite.webservices.activities.scheduling_2017_2.PhoneCall.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:customization_2017_2.setup.webservices.netsuite.com".equals(namespaceURI) &&
                  "CustomRecordTypePermissionsList".equals(typeName)){
                   
                            return  com.netsuite.webservices.setup.customization_2017_2.CustomRecordTypePermissionsList.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:common_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "SubsidiarySearchBasic".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.common_2017_2.SubsidiarySearchBasic.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:messages_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "GetBudgetExchangeRateResponse".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRateResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.accounting_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "ItemProductFeed".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.accounting_2017_2.types.ItemProductFeed.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:sales_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "OpportunityCompetitors".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.sales_2017_2.OpportunityCompetitors.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:core_2017_2.platform.webservices.netsuite.com".equals(namespaceURI) &&
                  "BudgetExchangeRateFilter".equals(typeName)){
                   
                            return  com.netsuite.webservices.platform.core_2017_2.BudgetExchangeRateFilter.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:types.relationships_2017_2.lists.webservices.netsuite.com".equals(namespaceURI) &&
                  "EmailPreference".equals(typeName)){
                   
                            return  com.netsuite.webservices.lists.relationships_2017_2.types.EmailPreference.Factory.parse(reader);
                        

                  }

              
                  if (
                  "urn:inventory_2017_2.transactions.webservices.netsuite.com".equals(namespaceURI) &&
                  "AssemblyUnbuild".equals(typeName)){
                   
                            return  com.netsuite.webservices.transactions.inventory_2017_2.AssemblyUnbuild.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    