
/**
 * EmployeeSearchBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  EmployeeSearchBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class EmployeeSearchBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRecordBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = EmployeeSearchBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for Address
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddressTracker = false ;

                           public boolean isAddressSpecified(){
                               return localAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAddress(){
                               return localAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Address
                               */
                               public void setAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAddressTracker = param != null;
                                   
                                            this.localAddress=param;
                                    

                               }
                            

                        /**
                        * field for Addressee
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAddressee ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddresseeTracker = false ;

                           public boolean isAddresseeSpecified(){
                               return localAddresseeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAddressee(){
                               return localAddressee;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Addressee
                               */
                               public void setAddressee(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAddresseeTracker = param != null;
                                   
                                            this.localAddressee=param;
                                    

                               }
                            

                        /**
                        * field for AddressLabel
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAddressLabel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddressLabelTracker = false ;

                           public boolean isAddressLabelSpecified(){
                               return localAddressLabelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAddressLabel(){
                               return localAddressLabel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AddressLabel
                               */
                               public void setAddressLabel(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAddressLabelTracker = param != null;
                                   
                                            this.localAddressLabel=param;
                                    

                               }
                            

                        /**
                        * field for AddressPhone
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAddressPhone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddressPhoneTracker = false ;

                           public boolean isAddressPhoneSpecified(){
                               return localAddressPhoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAddressPhone(){
                               return localAddressPhone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AddressPhone
                               */
                               public void setAddressPhone(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAddressPhoneTracker = param != null;
                                   
                                            this.localAddressPhone=param;
                                    

                               }
                            

                        /**
                        * field for AlienNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAlienNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAlienNumberTracker = false ;

                           public boolean isAlienNumberSpecified(){
                               return localAlienNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAlienNumber(){
                               return localAlienNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AlienNumber
                               */
                               public void setAlienNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAlienNumberTracker = param != null;
                                   
                                            this.localAlienNumber=param;
                                    

                               }
                            

                        /**
                        * field for Allocation
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localAllocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAllocationTracker = false ;

                           public boolean isAllocationSpecified(){
                               return localAllocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getAllocation(){
                               return localAllocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Allocation
                               */
                               public void setAllocation(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localAllocationTracker = param != null;
                                   
                                            this.localAllocation=param;
                                    

                               }
                            

                        /**
                        * field for Anniversary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localAnniversary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAnniversaryTracker = false ;

                           public boolean isAnniversarySpecified(){
                               return localAnniversaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getAnniversary(){
                               return localAnniversary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Anniversary
                               */
                               public void setAnniversary(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localAnniversaryTracker = param != null;
                                   
                                            this.localAnniversary=param;
                                    

                               }
                            

                        /**
                        * field for ApprovalLimit
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localApprovalLimit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localApprovalLimitTracker = false ;

                           public boolean isApprovalLimitSpecified(){
                               return localApprovalLimitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getApprovalLimit(){
                               return localApprovalLimit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ApprovalLimit
                               */
                               public void setApprovalLimit(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localApprovalLimitTracker = param != null;
                                   
                                            this.localApprovalLimit=param;
                                    

                               }
                            

                        /**
                        * field for Approver
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localApprover ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localApproverTracker = false ;

                           public boolean isApproverSpecified(){
                               return localApproverTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getApprover(){
                               return localApprover;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Approver
                               */
                               public void setApprover(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localApproverTracker = param != null;
                                   
                                            this.localApprover=param;
                                    

                               }
                            

                        /**
                        * field for Attention
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAttention ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAttentionTracker = false ;

                           public boolean isAttentionSpecified(){
                               return localAttentionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAttention(){
                               return localAttention;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Attention
                               */
                               public void setAttention(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAttentionTracker = param != null;
                                   
                                            this.localAttention=param;
                                    

                               }
                            

                        /**
                        * field for AuthWorkDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localAuthWorkDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAuthWorkDateTracker = false ;

                           public boolean isAuthWorkDateSpecified(){
                               return localAuthWorkDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getAuthWorkDate(){
                               return localAuthWorkDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AuthWorkDate
                               */
                               public void setAuthWorkDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localAuthWorkDateTracker = param != null;
                                   
                                            this.localAuthWorkDate=param;
                                    

                               }
                            

                        /**
                        * field for BaseWage
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localBaseWage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBaseWageTracker = false ;

                           public boolean isBaseWageSpecified(){
                               return localBaseWageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getBaseWage(){
                               return localBaseWage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BaseWage
                               */
                               public void setBaseWage(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localBaseWageTracker = param != null;
                                   
                                            this.localBaseWage=param;
                                    

                               }
                            

                        /**
                        * field for BaseWageType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localBaseWageType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBaseWageTypeTracker = false ;

                           public boolean isBaseWageTypeSpecified(){
                               return localBaseWageTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getBaseWageType(){
                               return localBaseWageType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BaseWageType
                               */
                               public void setBaseWageType(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localBaseWageTypeTracker = param != null;
                                   
                                            this.localBaseWageType=param;
                                    

                               }
                            

                        /**
                        * field for BillAddress
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localBillAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillAddressTracker = false ;

                           public boolean isBillAddressSpecified(){
                               return localBillAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getBillAddress(){
                               return localBillAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillAddress
                               */
                               public void setBillAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localBillAddressTracker = param != null;
                                   
                                            this.localBillAddress=param;
                                    

                               }
                            

                        /**
                        * field for BillingClass
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localBillingClass ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillingClassTracker = false ;

                           public boolean isBillingClassSpecified(){
                               return localBillingClassTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getBillingClass(){
                               return localBillingClass;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillingClass
                               */
                               public void setBillingClass(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localBillingClassTracker = param != null;
                                   
                                            this.localBillingClass=param;
                                    

                               }
                            

                        /**
                        * field for BirthDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localBirthDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBirthDateTracker = false ;

                           public boolean isBirthDateSpecified(){
                               return localBirthDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getBirthDate(){
                               return localBirthDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BirthDate
                               */
                               public void setBirthDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localBirthDateTracker = param != null;
                                   
                                            this.localBirthDate=param;
                                    

                               }
                            

                        /**
                        * field for BirthDay
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localBirthDay ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBirthDayTracker = false ;

                           public boolean isBirthDaySpecified(){
                               return localBirthDayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getBirthDay(){
                               return localBirthDay;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BirthDay
                               */
                               public void setBirthDay(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localBirthDayTracker = param != null;
                                   
                                            this.localBirthDay=param;
                                    

                               }
                            

                        /**
                        * field for CContribution
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCContribution ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCContributionTracker = false ;

                           public boolean isCContributionSpecified(){
                               return localCContributionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCContribution(){
                               return localCContribution;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CContribution
                               */
                               public void setCContribution(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCContributionTracker = param != null;
                                   
                                            this.localCContribution=param;
                                    

                               }
                            

                        /**
                        * field for City
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localCity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCityTracker = false ;

                           public boolean isCitySpecified(){
                               return localCityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getCity(){
                               return localCity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param City
                               */
                               public void setCity(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCityTracker = param != null;
                                   
                                            this.localCity=param;
                                    

                               }
                            

                        /**
                        * field for _class
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField get_class(){
                               return local_class;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param _class
                               */
                               public void set_class(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            local_classTracker = param != null;
                                   
                                            this.local_class=param;
                                    

                               }
                            

                        /**
                        * field for Comments
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localComments ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCommentsTracker = false ;

                           public boolean isCommentsSpecified(){
                               return localCommentsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getComments(){
                               return localComments;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Comments
                               */
                               public void setComments(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCommentsTracker = param != null;
                                   
                                            this.localComments=param;
                                    

                               }
                            

                        /**
                        * field for CommissionPlan
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCommissionPlan ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCommissionPlanTracker = false ;

                           public boolean isCommissionPlanSpecified(){
                               return localCommissionPlanTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCommissionPlan(){
                               return localCommissionPlan;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CommissionPlan
                               */
                               public void setCommissionPlan(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCommissionPlanTracker = param != null;
                                   
                                            this.localCommissionPlan=param;
                                    

                               }
                            

                        /**
                        * field for CompensationCurrency
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localCompensationCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCompensationCurrencyTracker = false ;

                           public boolean isCompensationCurrencySpecified(){
                               return localCompensationCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getCompensationCurrency(){
                               return localCompensationCurrency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CompensationCurrency
                               */
                               public void setCompensationCurrency(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localCompensationCurrencyTracker = param != null;
                                   
                                            this.localCompensationCurrency=param;
                                    

                               }
                            

                        /**
                        * field for ConcurrentWebServicesUser
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localConcurrentWebServicesUser ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localConcurrentWebServicesUserTracker = false ;

                           public boolean isConcurrentWebServicesUserSpecified(){
                               return localConcurrentWebServicesUserTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getConcurrentWebServicesUser(){
                               return localConcurrentWebServicesUser;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ConcurrentWebServicesUser
                               */
                               public void setConcurrentWebServicesUser(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localConcurrentWebServicesUserTracker = param != null;
                                   
                                            this.localConcurrentWebServicesUser=param;
                                    

                               }
                            

                        /**
                        * field for Country
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localCountry ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCountryTracker = false ;

                           public boolean isCountrySpecified(){
                               return localCountryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getCountry(){
                               return localCountry;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Country
                               */
                               public void setCountry(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localCountryTracker = param != null;
                                   
                                            this.localCountry=param;
                                    

                               }
                            

                        /**
                        * field for County
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localCounty ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCountyTracker = false ;

                           public boolean isCountySpecified(){
                               return localCountyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getCounty(){
                               return localCounty;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param County
                               */
                               public void setCounty(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCountyTracker = param != null;
                                   
                                            this.localCounty=param;
                                    

                               }
                            

                        /**
                        * field for DateCreated
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localDateCreated ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDateCreatedTracker = false ;

                           public boolean isDateCreatedSpecified(){
                               return localDateCreatedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getDateCreated(){
                               return localDateCreated;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DateCreated
                               */
                               public void setDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localDateCreatedTracker = param != null;
                                   
                                            this.localDateCreated=param;
                                    

                               }
                            

                        /**
                        * field for Deduction
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localDeduction ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeductionTracker = false ;

                           public boolean isDeductionSpecified(){
                               return localDeductionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getDeduction(){
                               return localDeduction;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Deduction
                               */
                               public void setDeduction(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localDeductionTracker = param != null;
                                   
                                            this.localDeduction=param;
                                    

                               }
                            

                        /**
                        * field for Department
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getDepartment(){
                               return localDepartment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Department
                               */
                               public void setDepartment(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localDepartmentTracker = param != null;
                                   
                                            this.localDepartment=param;
                                    

                               }
                            

                        /**
                        * field for Earning
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localEarning ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEarningTracker = false ;

                           public boolean isEarningSpecified(){
                               return localEarningTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getEarning(){
                               return localEarning;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Earning
                               */
                               public void setEarning(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localEarningTracker = param != null;
                                   
                                            this.localEarning=param;
                                    

                               }
                            

                        /**
                        * field for Education
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localEducation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEducationTracker = false ;

                           public boolean isEducationSpecified(){
                               return localEducationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getEducation(){
                               return localEducation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Education
                               */
                               public void setEducation(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localEducationTracker = param != null;
                                   
                                            this.localEducation=param;
                                    

                               }
                            

                        /**
                        * field for EligibleForCommission
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localEligibleForCommission ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEligibleForCommissionTracker = false ;

                           public boolean isEligibleForCommissionSpecified(){
                               return localEligibleForCommissionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getEligibleForCommission(){
                               return localEligibleForCommission;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EligibleForCommission
                               */
                               public void setEligibleForCommission(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localEligibleForCommissionTracker = param != null;
                                   
                                            this.localEligibleForCommission=param;
                                    

                               }
                            

                        /**
                        * field for Email
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailTracker = false ;

                           public boolean isEmailSpecified(){
                               return localEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getEmail(){
                               return localEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Email
                               */
                               public void setEmail(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localEmailTracker = param != null;
                                   
                                            this.localEmail=param;
                                    

                               }
                            

                        /**
                        * field for EmployeeStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localEmployeeStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmployeeStatusTracker = false ;

                           public boolean isEmployeeStatusSpecified(){
                               return localEmployeeStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getEmployeeStatus(){
                               return localEmployeeStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmployeeStatus
                               */
                               public void setEmployeeStatus(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localEmployeeStatusTracker = param != null;
                                   
                                            this.localEmployeeStatus=param;
                                    

                               }
                            

                        /**
                        * field for EmployeeType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localEmployeeType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmployeeTypeTracker = false ;

                           public boolean isEmployeeTypeSpecified(){
                               return localEmployeeTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getEmployeeType(){
                               return localEmployeeType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmployeeType
                               */
                               public void setEmployeeType(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localEmployeeTypeTracker = param != null;
                                   
                                            this.localEmployeeType=param;
                                    

                               }
                            

                        /**
                        * field for EmployeeTypeKpi
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localEmployeeTypeKpi ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmployeeTypeKpiTracker = false ;

                           public boolean isEmployeeTypeKpiSpecified(){
                               return localEmployeeTypeKpiTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getEmployeeTypeKpi(){
                               return localEmployeeTypeKpi;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmployeeTypeKpi
                               */
                               public void setEmployeeTypeKpi(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localEmployeeTypeKpiTracker = param != null;
                                   
                                            this.localEmployeeTypeKpi=param;
                                    

                               }
                            

                        /**
                        * field for EntityId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localEntityId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityIdTracker = false ;

                           public boolean isEntityIdSpecified(){
                               return localEntityIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getEntityId(){
                               return localEntityId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EntityId
                               */
                               public void setEntityId(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localEntityIdTracker = param != null;
                                   
                                            this.localEntityId=param;
                                    

                               }
                            

                        /**
                        * field for Ethnicity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localEthnicity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEthnicityTracker = false ;

                           public boolean isEthnicitySpecified(){
                               return localEthnicityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getEthnicity(){
                               return localEthnicity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Ethnicity
                               */
                               public void setEthnicity(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localEthnicityTracker = param != null;
                                   
                                            this.localEthnicity=param;
                                    

                               }
                            

                        /**
                        * field for ExpenseLimit
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localExpenseLimit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExpenseLimitTracker = false ;

                           public boolean isExpenseLimitSpecified(){
                               return localExpenseLimitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getExpenseLimit(){
                               return localExpenseLimit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExpenseLimit
                               */
                               public void setExpenseLimit(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localExpenseLimitTracker = param != null;
                                   
                                            this.localExpenseLimit=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localExternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdTracker = false ;

                           public boolean isExternalIdSpecified(){
                               return localExternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localExternalIdTracker = param != null;
                                   
                                            this.localExternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalIdString
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localExternalIdString ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdStringTracker = false ;

                           public boolean isExternalIdStringSpecified(){
                               return localExternalIdStringTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getExternalIdString(){
                               return localExternalIdString;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalIdString
                               */
                               public void setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localExternalIdStringTracker = param != null;
                                   
                                            this.localExternalIdString=param;
                                    

                               }
                            

                        /**
                        * field for Fax
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localFax ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFaxTracker = false ;

                           public boolean isFaxSpecified(){
                               return localFaxTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getFax(){
                               return localFax;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Fax
                               */
                               public void setFax(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localFaxTracker = param != null;
                                   
                                            this.localFax=param;
                                    

                               }
                            

                        /**
                        * field for FirstName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localFirstName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFirstNameTracker = false ;

                           public boolean isFirstNameSpecified(){
                               return localFirstNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getFirstName(){
                               return localFirstName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FirstName
                               */
                               public void setFirstName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localFirstNameTracker = param != null;
                                   
                                            this.localFirstName=param;
                                    

                               }
                            

                        /**
                        * field for Gender
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localGender ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGenderTracker = false ;

                           public boolean isGenderSpecified(){
                               return localGenderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getGender(){
                               return localGender;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Gender
                               */
                               public void setGender(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localGenderTracker = param != null;
                                   
                                            this.localGender=param;
                                    

                               }
                            

                        /**
                        * field for GiveAccess
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localGiveAccess ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiveAccessTracker = false ;

                           public boolean isGiveAccessSpecified(){
                               return localGiveAccessTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getGiveAccess(){
                               return localGiveAccess;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiveAccess
                               */
                               public void setGiveAccess(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localGiveAccessTracker = param != null;
                                   
                                            this.localGiveAccess=param;
                                    

                               }
                            

                        /**
                        * field for GlobalSubscriptionStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localGlobalSubscriptionStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGlobalSubscriptionStatusTracker = false ;

                           public boolean isGlobalSubscriptionStatusSpecified(){
                               return localGlobalSubscriptionStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getGlobalSubscriptionStatus(){
                               return localGlobalSubscriptionStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GlobalSubscriptionStatus
                               */
                               public void setGlobalSubscriptionStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localGlobalSubscriptionStatusTracker = param != null;
                                   
                                            this.localGlobalSubscriptionStatus=param;
                                    

                               }
                            

                        /**
                        * field for Group
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGroupTracker = false ;

                           public boolean isGroupSpecified(){
                               return localGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getGroup(){
                               return localGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Group
                               */
                               public void setGroup(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localGroupTracker = param != null;
                                   
                                            this.localGroup=param;
                                    

                               }
                            

                        /**
                        * field for HireDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localHireDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHireDateTracker = false ;

                           public boolean isHireDateSpecified(){
                               return localHireDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getHireDate(){
                               return localHireDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HireDate
                               */
                               public void setHireDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localHireDateTracker = param != null;
                                   
                                            this.localHireDate=param;
                                    

                               }
                            

                        /**
                        * field for I9Verified
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localI9Verified ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localI9VerifiedTracker = false ;

                           public boolean isI9VerifiedSpecified(){
                               return localI9VerifiedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getI9Verified(){
                               return localI9Verified;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param I9Verified
                               */
                               public void setI9Verified(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localI9VerifiedTracker = param != null;
                                   
                                            this.localI9Verified=param;
                                    

                               }
                            

                        /**
                        * field for Image
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localImage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localImageTracker = false ;

                           public boolean isImageSpecified(){
                               return localImageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getImage(){
                               return localImage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Image
                               */
                               public void setImage(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localImageTracker = param != null;
                                   
                                            this.localImage=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localInternalIdTracker = param != null;
                                   
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for InternalIdNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localInternalIdNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdNumberTracker = false ;

                           public boolean isInternalIdNumberSpecified(){
                               return localInternalIdNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getInternalIdNumber(){
                               return localInternalIdNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalIdNumber
                               */
                               public void setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localInternalIdNumberTracker = param != null;
                                   
                                            this.localInternalIdNumber=param;
                                    

                               }
                            

                        /**
                        * field for IsDefaultBilling
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsDefaultBilling ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsDefaultBillingTracker = false ;

                           public boolean isIsDefaultBillingSpecified(){
                               return localIsDefaultBillingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsDefaultBilling(){
                               return localIsDefaultBilling;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsDefaultBilling
                               */
                               public void setIsDefaultBilling(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsDefaultBillingTracker = param != null;
                                   
                                            this.localIsDefaultBilling=param;
                                    

                               }
                            

                        /**
                        * field for IsDefaultShipping
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsDefaultShipping ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsDefaultShippingTracker = false ;

                           public boolean isIsDefaultShippingSpecified(){
                               return localIsDefaultShippingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsDefaultShipping(){
                               return localIsDefaultShipping;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsDefaultShipping
                               */
                               public void setIsDefaultShipping(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsDefaultShippingTracker = param != null;
                                   
                                            this.localIsDefaultShipping=param;
                                    

                               }
                            

                        /**
                        * field for IsInactive
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsInactive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsInactiveTracker = false ;

                           public boolean isIsInactiveSpecified(){
                               return localIsInactiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsInactive(){
                               return localIsInactive;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsInactive
                               */
                               public void setIsInactive(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsInactiveTracker = param != null;
                                   
                                            this.localIsInactive=param;
                                    

                               }
                            

                        /**
                        * field for IsJobResource
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsJobResource ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsJobResourceTracker = false ;

                           public boolean isIsJobResourceSpecified(){
                               return localIsJobResourceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsJobResource(){
                               return localIsJobResource;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsJobResource
                               */
                               public void setIsJobResource(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsJobResourceTracker = param != null;
                                   
                                            this.localIsJobResource=param;
                                    

                               }
                            

                        /**
                        * field for IsTemplate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsTemplate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsTemplateTracker = false ;

                           public boolean isIsTemplateSpecified(){
                               return localIsTemplateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsTemplate(){
                               return localIsTemplate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsTemplate
                               */
                               public void setIsTemplate(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsTemplateTracker = param != null;
                                   
                                            this.localIsTemplate=param;
                                    

                               }
                            

                        /**
                        * field for Job
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localJob ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localJobTracker = false ;

                           public boolean isJobSpecified(){
                               return localJobTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getJob(){
                               return localJob;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Job
                               */
                               public void setJob(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localJobTracker = param != null;
                                   
                                            this.localJob=param;
                                    

                               }
                            

                        /**
                        * field for JobDescription
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localJobDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localJobDescriptionTracker = false ;

                           public boolean isJobDescriptionSpecified(){
                               return localJobDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getJobDescription(){
                               return localJobDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param JobDescription
                               */
                               public void setJobDescription(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localJobDescriptionTracker = param != null;
                                   
                                            this.localJobDescription=param;
                                    

                               }
                            

                        /**
                        * field for LaborCost
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localLaborCost ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLaborCostTracker = false ;

                           public boolean isLaborCostSpecified(){
                               return localLaborCostTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getLaborCost(){
                               return localLaborCost;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LaborCost
                               */
                               public void setLaborCost(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localLaborCostTracker = param != null;
                                   
                                            this.localLaborCost=param;
                                    

                               }
                            

                        /**
                        * field for Language
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localLanguage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLanguageTracker = false ;

                           public boolean isLanguageSpecified(){
                               return localLanguageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getLanguage(){
                               return localLanguage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Language
                               */
                               public void setLanguage(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localLanguageTracker = param != null;
                                   
                                            this.localLanguage=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for LastName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localLastName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastNameTracker = false ;

                           public boolean isLastNameSpecified(){
                               return localLastNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getLastName(){
                               return localLastName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastName
                               */
                               public void setLastName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localLastNameTracker = param != null;
                                   
                                            this.localLastName=param;
                                    

                               }
                            

                        /**
                        * field for LastPaidDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localLastPaidDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastPaidDateTracker = false ;

                           public boolean isLastPaidDateSpecified(){
                               return localLastPaidDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getLastPaidDate(){
                               return localLastPaidDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastPaidDate
                               */
                               public void setLastPaidDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localLastPaidDateTracker = param != null;
                                   
                                            this.localLastPaidDate=param;
                                    

                               }
                            

                        /**
                        * field for LastReviewDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localLastReviewDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastReviewDateTracker = false ;

                           public boolean isLastReviewDateSpecified(){
                               return localLastReviewDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getLastReviewDate(){
                               return localLastReviewDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastReviewDate
                               */
                               public void setLastReviewDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localLastReviewDateTracker = param != null;
                                   
                                            this.localLastReviewDate=param;
                                    

                               }
                            

                        /**
                        * field for Level
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localLevel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLevelTracker = false ;

                           public boolean isLevelSpecified(){
                               return localLevelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getLevel(){
                               return localLevel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Level
                               */
                               public void setLevel(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localLevelTracker = param != null;
                                   
                                            this.localLevel=param;
                                    

                               }
                            

                        /**
                        * field for Location
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for MaritalStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localMaritalStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMaritalStatusTracker = false ;

                           public boolean isMaritalStatusSpecified(){
                               return localMaritalStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getMaritalStatus(){
                               return localMaritalStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MaritalStatus
                               */
                               public void setMaritalStatus(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localMaritalStatusTracker = param != null;
                                   
                                            this.localMaritalStatus=param;
                                    

                               }
                            

                        /**
                        * field for MiddleName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localMiddleName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMiddleNameTracker = false ;

                           public boolean isMiddleNameSpecified(){
                               return localMiddleNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getMiddleName(){
                               return localMiddleName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MiddleName
                               */
                               public void setMiddleName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localMiddleNameTracker = param != null;
                                   
                                            this.localMiddleName=param;
                                    

                               }
                            

                        /**
                        * field for NextReviewDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localNextReviewDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNextReviewDateTracker = false ;

                           public boolean isNextReviewDateSpecified(){
                               return localNextReviewDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getNextReviewDate(){
                               return localNextReviewDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NextReviewDate
                               */
                               public void setNextReviewDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localNextReviewDateTracker = param != null;
                                   
                                            this.localNextReviewDate=param;
                                    

                               }
                            

                        /**
                        * field for OfflineAccess
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localOfflineAccess ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOfflineAccessTracker = false ;

                           public boolean isOfflineAccessSpecified(){
                               return localOfflineAccessTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getOfflineAccess(){
                               return localOfflineAccess;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OfflineAccess
                               */
                               public void setOfflineAccess(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localOfflineAccessTracker = param != null;
                                   
                                            this.localOfflineAccess=param;
                                    

                               }
                            

                        /**
                        * field for PayFrequency
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localPayFrequency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPayFrequencyTracker = false ;

                           public boolean isPayFrequencySpecified(){
                               return localPayFrequencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getPayFrequency(){
                               return localPayFrequency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PayFrequency
                               */
                               public void setPayFrequency(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localPayFrequencyTracker = param != null;
                                   
                                            this.localPayFrequency=param;
                                    

                               }
                            

                        /**
                        * field for PermChangeDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localPermChangeDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPermChangeDateTracker = false ;

                           public boolean isPermChangeDateSpecified(){
                               return localPermChangeDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getPermChangeDate(){
                               return localPermChangeDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PermChangeDate
                               */
                               public void setPermChangeDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localPermChangeDateTracker = param != null;
                                   
                                            this.localPermChangeDate=param;
                                    

                               }
                            

                        /**
                        * field for Permission
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localPermission ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPermissionTracker = false ;

                           public boolean isPermissionSpecified(){
                               return localPermissionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getPermission(){
                               return localPermission;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Permission
                               */
                               public void setPermission(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localPermissionTracker = param != null;
                                   
                                            this.localPermission=param;
                                    

                               }
                            

                        /**
                        * field for PermissionChange
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localPermissionChange ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPermissionChangeTracker = false ;

                           public boolean isPermissionChangeSpecified(){
                               return localPermissionChangeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getPermissionChange(){
                               return localPermissionChange;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PermissionChange
                               */
                               public void setPermissionChange(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localPermissionChangeTracker = param != null;
                                   
                                            this.localPermissionChange=param;
                                    

                               }
                            

                        /**
                        * field for Phone
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localPhone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPhoneTracker = false ;

                           public boolean isPhoneSpecified(){
                               return localPhoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getPhone(){
                               return localPhone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Phone
                               */
                               public void setPhone(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localPhoneTracker = param != null;
                                   
                                            this.localPhone=param;
                                    

                               }
                            

                        /**
                        * field for PhoneticName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localPhoneticName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPhoneticNameTracker = false ;

                           public boolean isPhoneticNameSpecified(){
                               return localPhoneticNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getPhoneticName(){
                               return localPhoneticName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PhoneticName
                               */
                               public void setPhoneticName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localPhoneticNameTracker = param != null;
                                   
                                            this.localPhoneticName=param;
                                    

                               }
                            

                        /**
                        * field for PositionTitle
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localPositionTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPositionTitleTracker = false ;

                           public boolean isPositionTitleSpecified(){
                               return localPositionTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getPositionTitle(){
                               return localPositionTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PositionTitle
                               */
                               public void setPositionTitle(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localPositionTitleTracker = param != null;
                                   
                                            this.localPositionTitle=param;
                                    

                               }
                            

                        /**
                        * field for PrimaryEarningAmount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localPrimaryEarningAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPrimaryEarningAmountTracker = false ;

                           public boolean isPrimaryEarningAmountSpecified(){
                               return localPrimaryEarningAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getPrimaryEarningAmount(){
                               return localPrimaryEarningAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PrimaryEarningAmount
                               */
                               public void setPrimaryEarningAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localPrimaryEarningAmountTracker = param != null;
                                   
                                            this.localPrimaryEarningAmount=param;
                                    

                               }
                            

                        /**
                        * field for PrimaryEarningItem
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localPrimaryEarningItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPrimaryEarningItemTracker = false ;

                           public boolean isPrimaryEarningItemSpecified(){
                               return localPrimaryEarningItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getPrimaryEarningItem(){
                               return localPrimaryEarningItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PrimaryEarningItem
                               */
                               public void setPrimaryEarningItem(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localPrimaryEarningItemTracker = param != null;
                                   
                                            this.localPrimaryEarningItem=param;
                                    

                               }
                            

                        /**
                        * field for PrimaryEarningType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localPrimaryEarningType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPrimaryEarningTypeTracker = false ;

                           public boolean isPrimaryEarningTypeSpecified(){
                               return localPrimaryEarningTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getPrimaryEarningType(){
                               return localPrimaryEarningType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PrimaryEarningType
                               */
                               public void setPrimaryEarningType(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localPrimaryEarningTypeTracker = param != null;
                                   
                                            this.localPrimaryEarningType=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseOrderApprovalLimit
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localPurchaseOrderApprovalLimit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseOrderApprovalLimitTracker = false ;

                           public boolean isPurchaseOrderApprovalLimitSpecified(){
                               return localPurchaseOrderApprovalLimitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getPurchaseOrderApprovalLimit(){
                               return localPurchaseOrderApprovalLimit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseOrderApprovalLimit
                               */
                               public void setPurchaseOrderApprovalLimit(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localPurchaseOrderApprovalLimitTracker = param != null;
                                   
                                            this.localPurchaseOrderApprovalLimit=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseOrderApprover
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localPurchaseOrderApprover ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseOrderApproverTracker = false ;

                           public boolean isPurchaseOrderApproverSpecified(){
                               return localPurchaseOrderApproverTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getPurchaseOrderApprover(){
                               return localPurchaseOrderApprover;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseOrderApprover
                               */
                               public void setPurchaseOrderApprover(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localPurchaseOrderApproverTracker = param != null;
                                   
                                            this.localPurchaseOrderApprover=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseOrderLimit
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localPurchaseOrderLimit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseOrderLimitTracker = false ;

                           public boolean isPurchaseOrderLimitSpecified(){
                               return localPurchaseOrderLimitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getPurchaseOrderLimit(){
                               return localPurchaseOrderLimit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseOrderLimit
                               */
                               public void setPurchaseOrderLimit(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localPurchaseOrderLimitTracker = param != null;
                                   
                                            this.localPurchaseOrderLimit=param;
                                    

                               }
                            

                        /**
                        * field for ReleaseDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localReleaseDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReleaseDateTracker = false ;

                           public boolean isReleaseDateSpecified(){
                               return localReleaseDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getReleaseDate(){
                               return localReleaseDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReleaseDate
                               */
                               public void setReleaseDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localReleaseDateTracker = param != null;
                                   
                                            this.localReleaseDate=param;
                                    

                               }
                            

                        /**
                        * field for ResidentStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localResidentStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localResidentStatusTracker = false ;

                           public boolean isResidentStatusSpecified(){
                               return localResidentStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getResidentStatus(){
                               return localResidentStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ResidentStatus
                               */
                               public void setResidentStatus(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localResidentStatusTracker = param != null;
                                   
                                            this.localResidentStatus=param;
                                    

                               }
                            

                        /**
                        * field for Role
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localRole ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRoleTracker = false ;

                           public boolean isRoleSpecified(){
                               return localRoleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getRole(){
                               return localRole;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Role
                               */
                               public void setRole(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localRoleTracker = param != null;
                                   
                                            this.localRole=param;
                                    

                               }
                            

                        /**
                        * field for RoleChange
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localRoleChange ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRoleChangeTracker = false ;

                           public boolean isRoleChangeSpecified(){
                               return localRoleChangeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getRoleChange(){
                               return localRoleChange;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RoleChange
                               */
                               public void setRoleChange(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localRoleChangeTracker = param != null;
                                   
                                            this.localRoleChange=param;
                                    

                               }
                            

                        /**
                        * field for RoleChangeDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localRoleChangeDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRoleChangeDateTracker = false ;

                           public boolean isRoleChangeDateSpecified(){
                               return localRoleChangeDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getRoleChangeDate(){
                               return localRoleChangeDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RoleChangeDate
                               */
                               public void setRoleChangeDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localRoleChangeDateTracker = param != null;
                                   
                                            this.localRoleChangeDate=param;
                                    

                               }
                            

                        /**
                        * field for SalesRep
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localSalesRep ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesRepTracker = false ;

                           public boolean isSalesRepSpecified(){
                               return localSalesRepTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getSalesRep(){
                               return localSalesRep;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesRep
                               */
                               public void setSalesRep(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localSalesRepTracker = param != null;
                                   
                                            this.localSalesRep=param;
                                    

                               }
                            

                        /**
                        * field for SalesRole
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSalesRole ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesRoleTracker = false ;

                           public boolean isSalesRoleSpecified(){
                               return localSalesRoleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSalesRole(){
                               return localSalesRole;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesRole
                               */
                               public void setSalesRole(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSalesRoleTracker = param != null;
                                   
                                            this.localSalesRole=param;
                                    

                               }
                            

                        /**
                        * field for Salutation
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localSalutation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalutationTracker = false ;

                           public boolean isSalutationSpecified(){
                               return localSalutationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getSalutation(){
                               return localSalutation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Salutation
                               */
                               public void setSalutation(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localSalutationTracker = param != null;
                                   
                                            this.localSalutation=param;
                                    

                               }
                            

                        /**
                        * field for SocialSecurityNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localSocialSecurityNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSocialSecurityNumberTracker = false ;

                           public boolean isSocialSecurityNumberSpecified(){
                               return localSocialSecurityNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getSocialSecurityNumber(){
                               return localSocialSecurityNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SocialSecurityNumber
                               */
                               public void setSocialSecurityNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localSocialSecurityNumberTracker = param != null;
                                   
                                            this.localSocialSecurityNumber=param;
                                    

                               }
                            

                        /**
                        * field for StartDateTimeOffCalc
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localStartDateTimeOffCalc ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateTimeOffCalcTracker = false ;

                           public boolean isStartDateTimeOffCalcSpecified(){
                               return localStartDateTimeOffCalcTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getStartDateTimeOffCalc(){
                               return localStartDateTimeOffCalc;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDateTimeOffCalc
                               */
                               public void setStartDateTimeOffCalc(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localStartDateTimeOffCalcTracker = param != null;
                                   
                                            this.localStartDateTimeOffCalc=param;
                                    

                               }
                            

                        /**
                        * field for State
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localState ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStateTracker = false ;

                           public boolean isStateSpecified(){
                               return localStateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getState(){
                               return localState;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param State
                               */
                               public void setState(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localStateTracker = param != null;
                                   
                                            this.localState=param;
                                    

                               }
                            

                        /**
                        * field for Subsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Subsidiary
                               */
                               public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSubsidiaryTracker = param != null;
                                   
                                            this.localSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for Supervisor
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSupervisor ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSupervisorTracker = false ;

                           public boolean isSupervisorSpecified(){
                               return localSupervisorTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSupervisor(){
                               return localSupervisor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Supervisor
                               */
                               public void setSupervisor(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSupervisorTracker = param != null;
                                   
                                            this.localSupervisor=param;
                                    

                               }
                            

                        /**
                        * field for SupportRep
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localSupportRep ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSupportRepTracker = false ;

                           public boolean isSupportRepSpecified(){
                               return localSupportRepTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getSupportRep(){
                               return localSupportRep;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SupportRep
                               */
                               public void setSupportRep(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localSupportRepTracker = param != null;
                                   
                                            this.localSupportRep=param;
                                    

                               }
                            

                        /**
                        * field for TerminationCategory
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localTerminationCategory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTerminationCategoryTracker = false ;

                           public boolean isTerminationCategorySpecified(){
                               return localTerminationCategoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getTerminationCategory(){
                               return localTerminationCategory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TerminationCategory
                               */
                               public void setTerminationCategory(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localTerminationCategoryTracker = param != null;
                                   
                                            this.localTerminationCategory=param;
                                    

                               }
                            

                        /**
                        * field for TerminationDetails
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localTerminationDetails ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTerminationDetailsTracker = false ;

                           public boolean isTerminationDetailsSpecified(){
                               return localTerminationDetailsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getTerminationDetails(){
                               return localTerminationDetails;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TerminationDetails
                               */
                               public void setTerminationDetails(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localTerminationDetailsTracker = param != null;
                                   
                                            this.localTerminationDetails=param;
                                    

                               }
                            

                        /**
                        * field for TerminationReason
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localTerminationReason ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTerminationReasonTracker = false ;

                           public boolean isTerminationReasonSpecified(){
                               return localTerminationReasonTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getTerminationReason(){
                               return localTerminationReason;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TerminationReason
                               */
                               public void setTerminationReason(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localTerminationReasonTracker = param != null;
                                   
                                            this.localTerminationReason=param;
                                    

                               }
                            

                        /**
                        * field for TerminationRegretted
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localTerminationRegretted ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTerminationRegrettedTracker = false ;

                           public boolean isTerminationRegrettedSpecified(){
                               return localTerminationRegrettedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getTerminationRegretted(){
                               return localTerminationRegretted;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TerminationRegretted
                               */
                               public void setTerminationRegretted(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localTerminationRegrettedTracker = param != null;
                                   
                                            this.localTerminationRegretted=param;
                                    

                               }
                            

                        /**
                        * field for TimeApprover
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localTimeApprover ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeApproverTracker = false ;

                           public boolean isTimeApproverSpecified(){
                               return localTimeApproverTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getTimeApprover(){
                               return localTimeApprover;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimeApprover
                               */
                               public void setTimeApprover(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localTimeApproverTracker = param != null;
                                   
                                            this.localTimeApprover=param;
                                    

                               }
                            

                        /**
                        * field for TimeOffPlan
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localTimeOffPlan ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeOffPlanTracker = false ;

                           public boolean isTimeOffPlanSpecified(){
                               return localTimeOffPlanTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getTimeOffPlan(){
                               return localTimeOffPlan;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimeOffPlan
                               */
                               public void setTimeOffPlan(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localTimeOffPlanTracker = param != null;
                                   
                                            this.localTimeOffPlan=param;
                                    

                               }
                            

                        /**
                        * field for Title
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTitleTracker = false ;

                           public boolean isTitleSpecified(){
                               return localTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getTitle(){
                               return localTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Title
                               */
                               public void setTitle(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localTitleTracker = param != null;
                                   
                                            this.localTitle=param;
                                    

                               }
                            

                        /**
                        * field for UsePerquest
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localUsePerquest ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUsePerquestTracker = false ;

                           public boolean isUsePerquestSpecified(){
                               return localUsePerquestTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getUsePerquest(){
                               return localUsePerquest;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UsePerquest
                               */
                               public void setUsePerquest(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localUsePerquestTracker = param != null;
                                   
                                            this.localUsePerquest=param;
                                    

                               }
                            

                        /**
                        * field for UseTimeData
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localUseTimeData ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUseTimeDataTracker = false ;

                           public boolean isUseTimeDataSpecified(){
                               return localUseTimeDataTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getUseTimeData(){
                               return localUseTimeData;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UseTimeData
                               */
                               public void setUseTimeData(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localUseTimeDataTracker = param != null;
                                   
                                            this.localUseTimeData=param;
                                    

                               }
                            

                        /**
                        * field for VisaExpDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localVisaExpDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVisaExpDateTracker = false ;

                           public boolean isVisaExpDateSpecified(){
                               return localVisaExpDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getVisaExpDate(){
                               return localVisaExpDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VisaExpDate
                               */
                               public void setVisaExpDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localVisaExpDateTracker = param != null;
                                   
                                            this.localVisaExpDate=param;
                                    

                               }
                            

                        /**
                        * field for VisaType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localVisaType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVisaTypeTracker = false ;

                           public boolean isVisaTypeSpecified(){
                               return localVisaTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getVisaType(){
                               return localVisaType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VisaType
                               */
                               public void setVisaType(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localVisaTypeTracker = param != null;
                                   
                                            this.localVisaType=param;
                                    

                               }
                            

                        /**
                        * field for Withholding
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localWithholding ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWithholdingTracker = false ;

                           public boolean isWithholdingSpecified(){
                               return localWithholdingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getWithholding(){
                               return localWithholding;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Withholding
                               */
                               public void setWithholding(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localWithholdingTracker = param != null;
                                   
                                            this.localWithholding=param;
                                    

                               }
                            

                        /**
                        * field for WorkCalendar
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localWorkCalendar ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWorkCalendarTracker = false ;

                           public boolean isWorkCalendarSpecified(){
                               return localWorkCalendarTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getWorkCalendar(){
                               return localWorkCalendar;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param WorkCalendar
                               */
                               public void setWorkCalendar(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localWorkCalendarTracker = param != null;
                                   
                                            this.localWorkCalendar=param;
                                    

                               }
                            

                        /**
                        * field for Workplace
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localWorkplace ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWorkplaceTracker = false ;

                           public boolean isWorkplaceSpecified(){
                               return localWorkplaceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getWorkplace(){
                               return localWorkplace;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Workplace
                               */
                               public void setWorkplace(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localWorkplaceTracker = param != null;
                                   
                                            this.localWorkplace=param;
                                    

                               }
                            

                        /**
                        * field for ZipCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localZipCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localZipCodeTracker = false ;

                           public boolean isZipCodeSpecified(){
                               return localZipCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getZipCode(){
                               return localZipCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ZipCode
                               */
                               public void setZipCode(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localZipCodeTracker = param != null;
                                   
                                            this.localZipCode=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":EmployeeSearchBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "EmployeeSearchBasic",
                           xmlWriter);
                   }

                if (localAddressTracker){
                                            if (localAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("address cannot be null!!");
                                            }
                                           localAddress.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address"),
                                               xmlWriter);
                                        } if (localAddresseeTracker){
                                            if (localAddressee==null){
                                                 throw new org.apache.axis2.databinding.ADBException("addressee cannot be null!!");
                                            }
                                           localAddressee.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressee"),
                                               xmlWriter);
                                        } if (localAddressLabelTracker){
                                            if (localAddressLabel==null){
                                                 throw new org.apache.axis2.databinding.ADBException("addressLabel cannot be null!!");
                                            }
                                           localAddressLabel.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressLabel"),
                                               xmlWriter);
                                        } if (localAddressPhoneTracker){
                                            if (localAddressPhone==null){
                                                 throw new org.apache.axis2.databinding.ADBException("addressPhone cannot be null!!");
                                            }
                                           localAddressPhone.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressPhone"),
                                               xmlWriter);
                                        } if (localAlienNumberTracker){
                                            if (localAlienNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("alienNumber cannot be null!!");
                                            }
                                           localAlienNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","alienNumber"),
                                               xmlWriter);
                                        } if (localAllocationTracker){
                                            if (localAllocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("allocation cannot be null!!");
                                            }
                                           localAllocation.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","allocation"),
                                               xmlWriter);
                                        } if (localAnniversaryTracker){
                                            if (localAnniversary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("anniversary cannot be null!!");
                                            }
                                           localAnniversary.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","anniversary"),
                                               xmlWriter);
                                        } if (localApprovalLimitTracker){
                                            if (localApprovalLimit==null){
                                                 throw new org.apache.axis2.databinding.ADBException("approvalLimit cannot be null!!");
                                            }
                                           localApprovalLimit.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","approvalLimit"),
                                               xmlWriter);
                                        } if (localApproverTracker){
                                            if (localApprover==null){
                                                 throw new org.apache.axis2.databinding.ADBException("approver cannot be null!!");
                                            }
                                           localApprover.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","approver"),
                                               xmlWriter);
                                        } if (localAttentionTracker){
                                            if (localAttention==null){
                                                 throw new org.apache.axis2.databinding.ADBException("attention cannot be null!!");
                                            }
                                           localAttention.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","attention"),
                                               xmlWriter);
                                        } if (localAuthWorkDateTracker){
                                            if (localAuthWorkDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("authWorkDate cannot be null!!");
                                            }
                                           localAuthWorkDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","authWorkDate"),
                                               xmlWriter);
                                        } if (localBaseWageTracker){
                                            if (localBaseWage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("baseWage cannot be null!!");
                                            }
                                           localBaseWage.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","baseWage"),
                                               xmlWriter);
                                        } if (localBaseWageTypeTracker){
                                            if (localBaseWageType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("baseWageType cannot be null!!");
                                            }
                                           localBaseWageType.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","baseWageType"),
                                               xmlWriter);
                                        } if (localBillAddressTracker){
                                            if (localBillAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billAddress cannot be null!!");
                                            }
                                           localBillAddress.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billAddress"),
                                               xmlWriter);
                                        } if (localBillingClassTracker){
                                            if (localBillingClass==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billingClass cannot be null!!");
                                            }
                                           localBillingClass.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billingClass"),
                                               xmlWriter);
                                        } if (localBirthDateTracker){
                                            if (localBirthDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("birthDate cannot be null!!");
                                            }
                                           localBirthDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","birthDate"),
                                               xmlWriter);
                                        } if (localBirthDayTracker){
                                            if (localBirthDay==null){
                                                 throw new org.apache.axis2.databinding.ADBException("birthDay cannot be null!!");
                                            }
                                           localBirthDay.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","birthDay"),
                                               xmlWriter);
                                        } if (localCContributionTracker){
                                            if (localCContribution==null){
                                                 throw new org.apache.axis2.databinding.ADBException("cContribution cannot be null!!");
                                            }
                                           localCContribution.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","cContribution"),
                                               xmlWriter);
                                        } if (localCityTracker){
                                            if (localCity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("city cannot be null!!");
                                            }
                                           localCity.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","city"),
                                               xmlWriter);
                                        } if (local_classTracker){
                                            if (local_class==null){
                                                 throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                            }
                                           local_class.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","class"),
                                               xmlWriter);
                                        } if (localCommentsTracker){
                                            if (localComments==null){
                                                 throw new org.apache.axis2.databinding.ADBException("comments cannot be null!!");
                                            }
                                           localComments.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","comments"),
                                               xmlWriter);
                                        } if (localCommissionPlanTracker){
                                            if (localCommissionPlan==null){
                                                 throw new org.apache.axis2.databinding.ADBException("commissionPlan cannot be null!!");
                                            }
                                           localCommissionPlan.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","commissionPlan"),
                                               xmlWriter);
                                        } if (localCompensationCurrencyTracker){
                                            if (localCompensationCurrency==null){
                                                 throw new org.apache.axis2.databinding.ADBException("compensationCurrency cannot be null!!");
                                            }
                                           localCompensationCurrency.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","compensationCurrency"),
                                               xmlWriter);
                                        } if (localConcurrentWebServicesUserTracker){
                                            if (localConcurrentWebServicesUser==null){
                                                 throw new org.apache.axis2.databinding.ADBException("concurrentWebServicesUser cannot be null!!");
                                            }
                                           localConcurrentWebServicesUser.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","concurrentWebServicesUser"),
                                               xmlWriter);
                                        } if (localCountryTracker){
                                            if (localCountry==null){
                                                 throw new org.apache.axis2.databinding.ADBException("country cannot be null!!");
                                            }
                                           localCountry.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","country"),
                                               xmlWriter);
                                        } if (localCountyTracker){
                                            if (localCounty==null){
                                                 throw new org.apache.axis2.databinding.ADBException("county cannot be null!!");
                                            }
                                           localCounty.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","county"),
                                               xmlWriter);
                                        } if (localDateCreatedTracker){
                                            if (localDateCreated==null){
                                                 throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                            }
                                           localDateCreated.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated"),
                                               xmlWriter);
                                        } if (localDeductionTracker){
                                            if (localDeduction==null){
                                                 throw new org.apache.axis2.databinding.ADBException("deduction cannot be null!!");
                                            }
                                           localDeduction.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","deduction"),
                                               xmlWriter);
                                        } if (localDepartmentTracker){
                                            if (localDepartment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                            }
                                           localDepartment.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","department"),
                                               xmlWriter);
                                        } if (localEarningTracker){
                                            if (localEarning==null){
                                                 throw new org.apache.axis2.databinding.ADBException("earning cannot be null!!");
                                            }
                                           localEarning.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","earning"),
                                               xmlWriter);
                                        } if (localEducationTracker){
                                            if (localEducation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("education cannot be null!!");
                                            }
                                           localEducation.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","education"),
                                               xmlWriter);
                                        } if (localEligibleForCommissionTracker){
                                            if (localEligibleForCommission==null){
                                                 throw new org.apache.axis2.databinding.ADBException("eligibleForCommission cannot be null!!");
                                            }
                                           localEligibleForCommission.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","eligibleForCommission"),
                                               xmlWriter);
                                        } if (localEmailTracker){
                                            if (localEmail==null){
                                                 throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                            }
                                           localEmail.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","email"),
                                               xmlWriter);
                                        } if (localEmployeeStatusTracker){
                                            if (localEmployeeStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("employeeStatus cannot be null!!");
                                            }
                                           localEmployeeStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","employeeStatus"),
                                               xmlWriter);
                                        } if (localEmployeeTypeTracker){
                                            if (localEmployeeType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("employeeType cannot be null!!");
                                            }
                                           localEmployeeType.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","employeeType"),
                                               xmlWriter);
                                        } if (localEmployeeTypeKpiTracker){
                                            if (localEmployeeTypeKpi==null){
                                                 throw new org.apache.axis2.databinding.ADBException("employeeTypeKpi cannot be null!!");
                                            }
                                           localEmployeeTypeKpi.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","employeeTypeKpi"),
                                               xmlWriter);
                                        } if (localEntityIdTracker){
                                            if (localEntityId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("entityId cannot be null!!");
                                            }
                                           localEntityId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entityId"),
                                               xmlWriter);
                                        } if (localEthnicityTracker){
                                            if (localEthnicity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ethnicity cannot be null!!");
                                            }
                                           localEthnicity.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ethnicity"),
                                               xmlWriter);
                                        } if (localExpenseLimitTracker){
                                            if (localExpenseLimit==null){
                                                 throw new org.apache.axis2.databinding.ADBException("expenseLimit cannot be null!!");
                                            }
                                           localExpenseLimit.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","expenseLimit"),
                                               xmlWriter);
                                        } if (localExternalIdTracker){
                                            if (localExternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                            }
                                           localExternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId"),
                                               xmlWriter);
                                        } if (localExternalIdStringTracker){
                                            if (localExternalIdString==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                            }
                                           localExternalIdString.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString"),
                                               xmlWriter);
                                        } if (localFaxTracker){
                                            if (localFax==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fax cannot be null!!");
                                            }
                                           localFax.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fax"),
                                               xmlWriter);
                                        } if (localFirstNameTracker){
                                            if (localFirstName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("firstName cannot be null!!");
                                            }
                                           localFirstName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","firstName"),
                                               xmlWriter);
                                        } if (localGenderTracker){
                                            if (localGender==null){
                                                 throw new org.apache.axis2.databinding.ADBException("gender cannot be null!!");
                                            }
                                           localGender.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","gender"),
                                               xmlWriter);
                                        } if (localGiveAccessTracker){
                                            if (localGiveAccess==null){
                                                 throw new org.apache.axis2.databinding.ADBException("giveAccess cannot be null!!");
                                            }
                                           localGiveAccess.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","giveAccess"),
                                               xmlWriter);
                                        } if (localGlobalSubscriptionStatusTracker){
                                            if (localGlobalSubscriptionStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("globalSubscriptionStatus cannot be null!!");
                                            }
                                           localGlobalSubscriptionStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","globalSubscriptionStatus"),
                                               xmlWriter);
                                        } if (localGroupTracker){
                                            if (localGroup==null){
                                                 throw new org.apache.axis2.databinding.ADBException("group cannot be null!!");
                                            }
                                           localGroup.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","group"),
                                               xmlWriter);
                                        } if (localHireDateTracker){
                                            if (localHireDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("hireDate cannot be null!!");
                                            }
                                           localHireDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","hireDate"),
                                               xmlWriter);
                                        } if (localI9VerifiedTracker){
                                            if (localI9Verified==null){
                                                 throw new org.apache.axis2.databinding.ADBException("I9Verified cannot be null!!");
                                            }
                                           localI9Verified.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","I9Verified"),
                                               xmlWriter);
                                        } if (localImageTracker){
                                            if (localImage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("image cannot be null!!");
                                            }
                                           localImage.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","image"),
                                               xmlWriter);
                                        } if (localInternalIdTracker){
                                            if (localInternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                            }
                                           localInternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                               xmlWriter);
                                        } if (localInternalIdNumberTracker){
                                            if (localInternalIdNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                            }
                                           localInternalIdNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber"),
                                               xmlWriter);
                                        } if (localIsDefaultBillingTracker){
                                            if (localIsDefaultBilling==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isDefaultBilling cannot be null!!");
                                            }
                                           localIsDefaultBilling.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isDefaultBilling"),
                                               xmlWriter);
                                        } if (localIsDefaultShippingTracker){
                                            if (localIsDefaultShipping==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isDefaultShipping cannot be null!!");
                                            }
                                           localIsDefaultShipping.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isDefaultShipping"),
                                               xmlWriter);
                                        } if (localIsInactiveTracker){
                                            if (localIsInactive==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                            }
                                           localIsInactive.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isInactive"),
                                               xmlWriter);
                                        } if (localIsJobResourceTracker){
                                            if (localIsJobResource==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isJobResource cannot be null!!");
                                            }
                                           localIsJobResource.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isJobResource"),
                                               xmlWriter);
                                        } if (localIsTemplateTracker){
                                            if (localIsTemplate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isTemplate cannot be null!!");
                                            }
                                           localIsTemplate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isTemplate"),
                                               xmlWriter);
                                        } if (localJobTracker){
                                            if (localJob==null){
                                                 throw new org.apache.axis2.databinding.ADBException("job cannot be null!!");
                                            }
                                           localJob.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","job"),
                                               xmlWriter);
                                        } if (localJobDescriptionTracker){
                                            if (localJobDescription==null){
                                                 throw new org.apache.axis2.databinding.ADBException("jobDescription cannot be null!!");
                                            }
                                           localJobDescription.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","jobDescription"),
                                               xmlWriter);
                                        } if (localLaborCostTracker){
                                            if (localLaborCost==null){
                                                 throw new org.apache.axis2.databinding.ADBException("laborCost cannot be null!!");
                                            }
                                           localLaborCost.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","laborCost"),
                                               xmlWriter);
                                        } if (localLanguageTracker){
                                            if (localLanguage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("language cannot be null!!");
                                            }
                                           localLanguage.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","language"),
                                               xmlWriter);
                                        } if (localLastModifiedDateTracker){
                                            if (localLastModifiedDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                            }
                                           localLastModifiedDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate"),
                                               xmlWriter);
                                        } if (localLastNameTracker){
                                            if (localLastName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lastName cannot be null!!");
                                            }
                                           localLastName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastName"),
                                               xmlWriter);
                                        } if (localLastPaidDateTracker){
                                            if (localLastPaidDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lastPaidDate cannot be null!!");
                                            }
                                           localLastPaidDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastPaidDate"),
                                               xmlWriter);
                                        } if (localLastReviewDateTracker){
                                            if (localLastReviewDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lastReviewDate cannot be null!!");
                                            }
                                           localLastReviewDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastReviewDate"),
                                               xmlWriter);
                                        } if (localLevelTracker){
                                            if (localLevel==null){
                                                 throw new org.apache.axis2.databinding.ADBException("level cannot be null!!");
                                            }
                                           localLevel.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","level"),
                                               xmlWriter);
                                        } if (localLocationTracker){
                                            if (localLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                            }
                                           localLocation.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","location"),
                                               xmlWriter);
                                        } if (localMaritalStatusTracker){
                                            if (localMaritalStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("maritalStatus cannot be null!!");
                                            }
                                           localMaritalStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","maritalStatus"),
                                               xmlWriter);
                                        } if (localMiddleNameTracker){
                                            if (localMiddleName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("middleName cannot be null!!");
                                            }
                                           localMiddleName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","middleName"),
                                               xmlWriter);
                                        } if (localNextReviewDateTracker){
                                            if (localNextReviewDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nextReviewDate cannot be null!!");
                                            }
                                           localNextReviewDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nextReviewDate"),
                                               xmlWriter);
                                        } if (localOfflineAccessTracker){
                                            if (localOfflineAccess==null){
                                                 throw new org.apache.axis2.databinding.ADBException("offlineAccess cannot be null!!");
                                            }
                                           localOfflineAccess.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","offlineAccess"),
                                               xmlWriter);
                                        } if (localPayFrequencyTracker){
                                            if (localPayFrequency==null){
                                                 throw new org.apache.axis2.databinding.ADBException("payFrequency cannot be null!!");
                                            }
                                           localPayFrequency.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","payFrequency"),
                                               xmlWriter);
                                        } if (localPermChangeDateTracker){
                                            if (localPermChangeDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("permChangeDate cannot be null!!");
                                            }
                                           localPermChangeDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","permChangeDate"),
                                               xmlWriter);
                                        } if (localPermissionTracker){
                                            if (localPermission==null){
                                                 throw new org.apache.axis2.databinding.ADBException("permission cannot be null!!");
                                            }
                                           localPermission.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","permission"),
                                               xmlWriter);
                                        } if (localPermissionChangeTracker){
                                            if (localPermissionChange==null){
                                                 throw new org.apache.axis2.databinding.ADBException("permissionChange cannot be null!!");
                                            }
                                           localPermissionChange.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","permissionChange"),
                                               xmlWriter);
                                        } if (localPhoneTracker){
                                            if (localPhone==null){
                                                 throw new org.apache.axis2.databinding.ADBException("phone cannot be null!!");
                                            }
                                           localPhone.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phone"),
                                               xmlWriter);
                                        } if (localPhoneticNameTracker){
                                            if (localPhoneticName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("phoneticName cannot be null!!");
                                            }
                                           localPhoneticName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phoneticName"),
                                               xmlWriter);
                                        } if (localPositionTitleTracker){
                                            if (localPositionTitle==null){
                                                 throw new org.apache.axis2.databinding.ADBException("positionTitle cannot be null!!");
                                            }
                                           localPositionTitle.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","positionTitle"),
                                               xmlWriter);
                                        } if (localPrimaryEarningAmountTracker){
                                            if (localPrimaryEarningAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("primaryEarningAmount cannot be null!!");
                                            }
                                           localPrimaryEarningAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","primaryEarningAmount"),
                                               xmlWriter);
                                        } if (localPrimaryEarningItemTracker){
                                            if (localPrimaryEarningItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("primaryEarningItem cannot be null!!");
                                            }
                                           localPrimaryEarningItem.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","primaryEarningItem"),
                                               xmlWriter);
                                        } if (localPrimaryEarningTypeTracker){
                                            if (localPrimaryEarningType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("primaryEarningType cannot be null!!");
                                            }
                                           localPrimaryEarningType.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","primaryEarningType"),
                                               xmlWriter);
                                        } if (localPurchaseOrderApprovalLimitTracker){
                                            if (localPurchaseOrderApprovalLimit==null){
                                                 throw new org.apache.axis2.databinding.ADBException("purchaseOrderApprovalLimit cannot be null!!");
                                            }
                                           localPurchaseOrderApprovalLimit.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","purchaseOrderApprovalLimit"),
                                               xmlWriter);
                                        } if (localPurchaseOrderApproverTracker){
                                            if (localPurchaseOrderApprover==null){
                                                 throw new org.apache.axis2.databinding.ADBException("purchaseOrderApprover cannot be null!!");
                                            }
                                           localPurchaseOrderApprover.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","purchaseOrderApprover"),
                                               xmlWriter);
                                        } if (localPurchaseOrderLimitTracker){
                                            if (localPurchaseOrderLimit==null){
                                                 throw new org.apache.axis2.databinding.ADBException("purchaseOrderLimit cannot be null!!");
                                            }
                                           localPurchaseOrderLimit.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","purchaseOrderLimit"),
                                               xmlWriter);
                                        } if (localReleaseDateTracker){
                                            if (localReleaseDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("releaseDate cannot be null!!");
                                            }
                                           localReleaseDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","releaseDate"),
                                               xmlWriter);
                                        } if (localResidentStatusTracker){
                                            if (localResidentStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("residentStatus cannot be null!!");
                                            }
                                           localResidentStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","residentStatus"),
                                               xmlWriter);
                                        } if (localRoleTracker){
                                            if (localRole==null){
                                                 throw new org.apache.axis2.databinding.ADBException("role cannot be null!!");
                                            }
                                           localRole.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","role"),
                                               xmlWriter);
                                        } if (localRoleChangeTracker){
                                            if (localRoleChange==null){
                                                 throw new org.apache.axis2.databinding.ADBException("roleChange cannot be null!!");
                                            }
                                           localRoleChange.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","roleChange"),
                                               xmlWriter);
                                        } if (localRoleChangeDateTracker){
                                            if (localRoleChangeDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("roleChangeDate cannot be null!!");
                                            }
                                           localRoleChangeDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","roleChangeDate"),
                                               xmlWriter);
                                        } if (localSalesRepTracker){
                                            if (localSalesRep==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesRep cannot be null!!");
                                            }
                                           localSalesRep.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesRep"),
                                               xmlWriter);
                                        } if (localSalesRoleTracker){
                                            if (localSalesRole==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesRole cannot be null!!");
                                            }
                                           localSalesRole.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesRole"),
                                               xmlWriter);
                                        } if (localSalutationTracker){
                                            if (localSalutation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salutation cannot be null!!");
                                            }
                                           localSalutation.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salutation"),
                                               xmlWriter);
                                        } if (localSocialSecurityNumberTracker){
                                            if (localSocialSecurityNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("socialSecurityNumber cannot be null!!");
                                            }
                                           localSocialSecurityNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","socialSecurityNumber"),
                                               xmlWriter);
                                        } if (localStartDateTimeOffCalcTracker){
                                            if (localStartDateTimeOffCalc==null){
                                                 throw new org.apache.axis2.databinding.ADBException("startDateTimeOffCalc cannot be null!!");
                                            }
                                           localStartDateTimeOffCalc.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDateTimeOffCalc"),
                                               xmlWriter);
                                        } if (localStateTracker){
                                            if (localState==null){
                                                 throw new org.apache.axis2.databinding.ADBException("state cannot be null!!");
                                            }
                                           localState.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","state"),
                                               xmlWriter);
                                        } if (localSubsidiaryTracker){
                                            if (localSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                            }
                                           localSubsidiary.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary"),
                                               xmlWriter);
                                        } if (localSupervisorTracker){
                                            if (localSupervisor==null){
                                                 throw new org.apache.axis2.databinding.ADBException("supervisor cannot be null!!");
                                            }
                                           localSupervisor.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","supervisor"),
                                               xmlWriter);
                                        } if (localSupportRepTracker){
                                            if (localSupportRep==null){
                                                 throw new org.apache.axis2.databinding.ADBException("supportRep cannot be null!!");
                                            }
                                           localSupportRep.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","supportRep"),
                                               xmlWriter);
                                        } if (localTerminationCategoryTracker){
                                            if (localTerminationCategory==null){
                                                 throw new org.apache.axis2.databinding.ADBException("terminationCategory cannot be null!!");
                                            }
                                           localTerminationCategory.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","terminationCategory"),
                                               xmlWriter);
                                        } if (localTerminationDetailsTracker){
                                            if (localTerminationDetails==null){
                                                 throw new org.apache.axis2.databinding.ADBException("terminationDetails cannot be null!!");
                                            }
                                           localTerminationDetails.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","terminationDetails"),
                                               xmlWriter);
                                        } if (localTerminationReasonTracker){
                                            if (localTerminationReason==null){
                                                 throw new org.apache.axis2.databinding.ADBException("terminationReason cannot be null!!");
                                            }
                                           localTerminationReason.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","terminationReason"),
                                               xmlWriter);
                                        } if (localTerminationRegrettedTracker){
                                            if (localTerminationRegretted==null){
                                                 throw new org.apache.axis2.databinding.ADBException("terminationRegretted cannot be null!!");
                                            }
                                           localTerminationRegretted.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","terminationRegretted"),
                                               xmlWriter);
                                        } if (localTimeApproverTracker){
                                            if (localTimeApprover==null){
                                                 throw new org.apache.axis2.databinding.ADBException("timeApprover cannot be null!!");
                                            }
                                           localTimeApprover.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","timeApprover"),
                                               xmlWriter);
                                        } if (localTimeOffPlanTracker){
                                            if (localTimeOffPlan==null){
                                                 throw new org.apache.axis2.databinding.ADBException("timeOffPlan cannot be null!!");
                                            }
                                           localTimeOffPlan.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","timeOffPlan"),
                                               xmlWriter);
                                        } if (localTitleTracker){
                                            if (localTitle==null){
                                                 throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                            }
                                           localTitle.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","title"),
                                               xmlWriter);
                                        } if (localUsePerquestTracker){
                                            if (localUsePerquest==null){
                                                 throw new org.apache.axis2.databinding.ADBException("usePerquest cannot be null!!");
                                            }
                                           localUsePerquest.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","usePerquest"),
                                               xmlWriter);
                                        } if (localUseTimeDataTracker){
                                            if (localUseTimeData==null){
                                                 throw new org.apache.axis2.databinding.ADBException("useTimeData cannot be null!!");
                                            }
                                           localUseTimeData.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","useTimeData"),
                                               xmlWriter);
                                        } if (localVisaExpDateTracker){
                                            if (localVisaExpDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("visaExpDate cannot be null!!");
                                            }
                                           localVisaExpDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","visaExpDate"),
                                               xmlWriter);
                                        } if (localVisaTypeTracker){
                                            if (localVisaType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("visaType cannot be null!!");
                                            }
                                           localVisaType.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","visaType"),
                                               xmlWriter);
                                        } if (localWithholdingTracker){
                                            if (localWithholding==null){
                                                 throw new org.apache.axis2.databinding.ADBException("withholding cannot be null!!");
                                            }
                                           localWithholding.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","withholding"),
                                               xmlWriter);
                                        } if (localWorkCalendarTracker){
                                            if (localWorkCalendar==null){
                                                 throw new org.apache.axis2.databinding.ADBException("workCalendar cannot be null!!");
                                            }
                                           localWorkCalendar.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","workCalendar"),
                                               xmlWriter);
                                        } if (localWorkplaceTracker){
                                            if (localWorkplace==null){
                                                 throw new org.apache.axis2.databinding.ADBException("workplace cannot be null!!");
                                            }
                                           localWorkplace.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","workplace"),
                                               xmlWriter);
                                        } if (localZipCodeTracker){
                                            if (localZipCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("zipCode cannot be null!!");
                                            }
                                           localZipCode.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","zipCode"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","EmployeeSearchBasic"));
                 if (localAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "address"));
                            
                            
                                    if (localAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("address cannot be null!!");
                                    }
                                    elementList.add(localAddress);
                                } if (localAddresseeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "addressee"));
                            
                            
                                    if (localAddressee==null){
                                         throw new org.apache.axis2.databinding.ADBException("addressee cannot be null!!");
                                    }
                                    elementList.add(localAddressee);
                                } if (localAddressLabelTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "addressLabel"));
                            
                            
                                    if (localAddressLabel==null){
                                         throw new org.apache.axis2.databinding.ADBException("addressLabel cannot be null!!");
                                    }
                                    elementList.add(localAddressLabel);
                                } if (localAddressPhoneTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "addressPhone"));
                            
                            
                                    if (localAddressPhone==null){
                                         throw new org.apache.axis2.databinding.ADBException("addressPhone cannot be null!!");
                                    }
                                    elementList.add(localAddressPhone);
                                } if (localAlienNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "alienNumber"));
                            
                            
                                    if (localAlienNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("alienNumber cannot be null!!");
                                    }
                                    elementList.add(localAlienNumber);
                                } if (localAllocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "allocation"));
                            
                            
                                    if (localAllocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("allocation cannot be null!!");
                                    }
                                    elementList.add(localAllocation);
                                } if (localAnniversaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "anniversary"));
                            
                            
                                    if (localAnniversary==null){
                                         throw new org.apache.axis2.databinding.ADBException("anniversary cannot be null!!");
                                    }
                                    elementList.add(localAnniversary);
                                } if (localApprovalLimitTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "approvalLimit"));
                            
                            
                                    if (localApprovalLimit==null){
                                         throw new org.apache.axis2.databinding.ADBException("approvalLimit cannot be null!!");
                                    }
                                    elementList.add(localApprovalLimit);
                                } if (localApproverTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "approver"));
                            
                            
                                    if (localApprover==null){
                                         throw new org.apache.axis2.databinding.ADBException("approver cannot be null!!");
                                    }
                                    elementList.add(localApprover);
                                } if (localAttentionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "attention"));
                            
                            
                                    if (localAttention==null){
                                         throw new org.apache.axis2.databinding.ADBException("attention cannot be null!!");
                                    }
                                    elementList.add(localAttention);
                                } if (localAuthWorkDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "authWorkDate"));
                            
                            
                                    if (localAuthWorkDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("authWorkDate cannot be null!!");
                                    }
                                    elementList.add(localAuthWorkDate);
                                } if (localBaseWageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "baseWage"));
                            
                            
                                    if (localBaseWage==null){
                                         throw new org.apache.axis2.databinding.ADBException("baseWage cannot be null!!");
                                    }
                                    elementList.add(localBaseWage);
                                } if (localBaseWageTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "baseWageType"));
                            
                            
                                    if (localBaseWageType==null){
                                         throw new org.apache.axis2.databinding.ADBException("baseWageType cannot be null!!");
                                    }
                                    elementList.add(localBaseWageType);
                                } if (localBillAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "billAddress"));
                            
                            
                                    if (localBillAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("billAddress cannot be null!!");
                                    }
                                    elementList.add(localBillAddress);
                                } if (localBillingClassTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "billingClass"));
                            
                            
                                    if (localBillingClass==null){
                                         throw new org.apache.axis2.databinding.ADBException("billingClass cannot be null!!");
                                    }
                                    elementList.add(localBillingClass);
                                } if (localBirthDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "birthDate"));
                            
                            
                                    if (localBirthDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("birthDate cannot be null!!");
                                    }
                                    elementList.add(localBirthDate);
                                } if (localBirthDayTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "birthDay"));
                            
                            
                                    if (localBirthDay==null){
                                         throw new org.apache.axis2.databinding.ADBException("birthDay cannot be null!!");
                                    }
                                    elementList.add(localBirthDay);
                                } if (localCContributionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "cContribution"));
                            
                            
                                    if (localCContribution==null){
                                         throw new org.apache.axis2.databinding.ADBException("cContribution cannot be null!!");
                                    }
                                    elementList.add(localCContribution);
                                } if (localCityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "city"));
                            
                            
                                    if (localCity==null){
                                         throw new org.apache.axis2.databinding.ADBException("city cannot be null!!");
                                    }
                                    elementList.add(localCity);
                                } if (local_classTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "class"));
                            
                            
                                    if (local_class==null){
                                         throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    }
                                    elementList.add(local_class);
                                } if (localCommentsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "comments"));
                            
                            
                                    if (localComments==null){
                                         throw new org.apache.axis2.databinding.ADBException("comments cannot be null!!");
                                    }
                                    elementList.add(localComments);
                                } if (localCommissionPlanTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "commissionPlan"));
                            
                            
                                    if (localCommissionPlan==null){
                                         throw new org.apache.axis2.databinding.ADBException("commissionPlan cannot be null!!");
                                    }
                                    elementList.add(localCommissionPlan);
                                } if (localCompensationCurrencyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "compensationCurrency"));
                            
                            
                                    if (localCompensationCurrency==null){
                                         throw new org.apache.axis2.databinding.ADBException("compensationCurrency cannot be null!!");
                                    }
                                    elementList.add(localCompensationCurrency);
                                } if (localConcurrentWebServicesUserTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "concurrentWebServicesUser"));
                            
                            
                                    if (localConcurrentWebServicesUser==null){
                                         throw new org.apache.axis2.databinding.ADBException("concurrentWebServicesUser cannot be null!!");
                                    }
                                    elementList.add(localConcurrentWebServicesUser);
                                } if (localCountryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "country"));
                            
                            
                                    if (localCountry==null){
                                         throw new org.apache.axis2.databinding.ADBException("country cannot be null!!");
                                    }
                                    elementList.add(localCountry);
                                } if (localCountyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "county"));
                            
                            
                                    if (localCounty==null){
                                         throw new org.apache.axis2.databinding.ADBException("county cannot be null!!");
                                    }
                                    elementList.add(localCounty);
                                } if (localDateCreatedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "dateCreated"));
                            
                            
                                    if (localDateCreated==null){
                                         throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                    }
                                    elementList.add(localDateCreated);
                                } if (localDeductionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "deduction"));
                            
                            
                                    if (localDeduction==null){
                                         throw new org.apache.axis2.databinding.ADBException("deduction cannot be null!!");
                                    }
                                    elementList.add(localDeduction);
                                } if (localDepartmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "department"));
                            
                            
                                    if (localDepartment==null){
                                         throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    }
                                    elementList.add(localDepartment);
                                } if (localEarningTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "earning"));
                            
                            
                                    if (localEarning==null){
                                         throw new org.apache.axis2.databinding.ADBException("earning cannot be null!!");
                                    }
                                    elementList.add(localEarning);
                                } if (localEducationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "education"));
                            
                            
                                    if (localEducation==null){
                                         throw new org.apache.axis2.databinding.ADBException("education cannot be null!!");
                                    }
                                    elementList.add(localEducation);
                                } if (localEligibleForCommissionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "eligibleForCommission"));
                            
                            
                                    if (localEligibleForCommission==null){
                                         throw new org.apache.axis2.databinding.ADBException("eligibleForCommission cannot be null!!");
                                    }
                                    elementList.add(localEligibleForCommission);
                                } if (localEmailTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "email"));
                            
                            
                                    if (localEmail==null){
                                         throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                    }
                                    elementList.add(localEmail);
                                } if (localEmployeeStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "employeeStatus"));
                            
                            
                                    if (localEmployeeStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("employeeStatus cannot be null!!");
                                    }
                                    elementList.add(localEmployeeStatus);
                                } if (localEmployeeTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "employeeType"));
                            
                            
                                    if (localEmployeeType==null){
                                         throw new org.apache.axis2.databinding.ADBException("employeeType cannot be null!!");
                                    }
                                    elementList.add(localEmployeeType);
                                } if (localEmployeeTypeKpiTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "employeeTypeKpi"));
                            
                            
                                    if (localEmployeeTypeKpi==null){
                                         throw new org.apache.axis2.databinding.ADBException("employeeTypeKpi cannot be null!!");
                                    }
                                    elementList.add(localEmployeeTypeKpi);
                                } if (localEntityIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "entityId"));
                            
                            
                                    if (localEntityId==null){
                                         throw new org.apache.axis2.databinding.ADBException("entityId cannot be null!!");
                                    }
                                    elementList.add(localEntityId);
                                } if (localEthnicityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "ethnicity"));
                            
                            
                                    if (localEthnicity==null){
                                         throw new org.apache.axis2.databinding.ADBException("ethnicity cannot be null!!");
                                    }
                                    elementList.add(localEthnicity);
                                } if (localExpenseLimitTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "expenseLimit"));
                            
                            
                                    if (localExpenseLimit==null){
                                         throw new org.apache.axis2.databinding.ADBException("expenseLimit cannot be null!!");
                                    }
                                    elementList.add(localExpenseLimit);
                                } if (localExternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalId"));
                            
                            
                                    if (localExternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                    }
                                    elementList.add(localExternalId);
                                } if (localExternalIdStringTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalIdString"));
                            
                            
                                    if (localExternalIdString==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                    }
                                    elementList.add(localExternalIdString);
                                } if (localFaxTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "fax"));
                            
                            
                                    if (localFax==null){
                                         throw new org.apache.axis2.databinding.ADBException("fax cannot be null!!");
                                    }
                                    elementList.add(localFax);
                                } if (localFirstNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "firstName"));
                            
                            
                                    if (localFirstName==null){
                                         throw new org.apache.axis2.databinding.ADBException("firstName cannot be null!!");
                                    }
                                    elementList.add(localFirstName);
                                } if (localGenderTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "gender"));
                            
                            
                                    if (localGender==null){
                                         throw new org.apache.axis2.databinding.ADBException("gender cannot be null!!");
                                    }
                                    elementList.add(localGender);
                                } if (localGiveAccessTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "giveAccess"));
                            
                            
                                    if (localGiveAccess==null){
                                         throw new org.apache.axis2.databinding.ADBException("giveAccess cannot be null!!");
                                    }
                                    elementList.add(localGiveAccess);
                                } if (localGlobalSubscriptionStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "globalSubscriptionStatus"));
                            
                            
                                    if (localGlobalSubscriptionStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("globalSubscriptionStatus cannot be null!!");
                                    }
                                    elementList.add(localGlobalSubscriptionStatus);
                                } if (localGroupTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "group"));
                            
                            
                                    if (localGroup==null){
                                         throw new org.apache.axis2.databinding.ADBException("group cannot be null!!");
                                    }
                                    elementList.add(localGroup);
                                } if (localHireDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "hireDate"));
                            
                            
                                    if (localHireDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("hireDate cannot be null!!");
                                    }
                                    elementList.add(localHireDate);
                                } if (localI9VerifiedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "I9Verified"));
                            
                            
                                    if (localI9Verified==null){
                                         throw new org.apache.axis2.databinding.ADBException("I9Verified cannot be null!!");
                                    }
                                    elementList.add(localI9Verified);
                                } if (localImageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "image"));
                            
                            
                                    if (localImage==null){
                                         throw new org.apache.axis2.databinding.ADBException("image cannot be null!!");
                                    }
                                    elementList.add(localImage);
                                } if (localInternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalId"));
                            
                            
                                    if (localInternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    }
                                    elementList.add(localInternalId);
                                } if (localInternalIdNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalIdNumber"));
                            
                            
                                    if (localInternalIdNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                    }
                                    elementList.add(localInternalIdNumber);
                                } if (localIsDefaultBillingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isDefaultBilling"));
                            
                            
                                    if (localIsDefaultBilling==null){
                                         throw new org.apache.axis2.databinding.ADBException("isDefaultBilling cannot be null!!");
                                    }
                                    elementList.add(localIsDefaultBilling);
                                } if (localIsDefaultShippingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isDefaultShipping"));
                            
                            
                                    if (localIsDefaultShipping==null){
                                         throw new org.apache.axis2.databinding.ADBException("isDefaultShipping cannot be null!!");
                                    }
                                    elementList.add(localIsDefaultShipping);
                                } if (localIsInactiveTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isInactive"));
                            
                            
                                    if (localIsInactive==null){
                                         throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                    }
                                    elementList.add(localIsInactive);
                                } if (localIsJobResourceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isJobResource"));
                            
                            
                                    if (localIsJobResource==null){
                                         throw new org.apache.axis2.databinding.ADBException("isJobResource cannot be null!!");
                                    }
                                    elementList.add(localIsJobResource);
                                } if (localIsTemplateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isTemplate"));
                            
                            
                                    if (localIsTemplate==null){
                                         throw new org.apache.axis2.databinding.ADBException("isTemplate cannot be null!!");
                                    }
                                    elementList.add(localIsTemplate);
                                } if (localJobTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "job"));
                            
                            
                                    if (localJob==null){
                                         throw new org.apache.axis2.databinding.ADBException("job cannot be null!!");
                                    }
                                    elementList.add(localJob);
                                } if (localJobDescriptionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "jobDescription"));
                            
                            
                                    if (localJobDescription==null){
                                         throw new org.apache.axis2.databinding.ADBException("jobDescription cannot be null!!");
                                    }
                                    elementList.add(localJobDescription);
                                } if (localLaborCostTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "laborCost"));
                            
                            
                                    if (localLaborCost==null){
                                         throw new org.apache.axis2.databinding.ADBException("laborCost cannot be null!!");
                                    }
                                    elementList.add(localLaborCost);
                                } if (localLanguageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "language"));
                            
                            
                                    if (localLanguage==null){
                                         throw new org.apache.axis2.databinding.ADBException("language cannot be null!!");
                                    }
                                    elementList.add(localLanguage);
                                } if (localLastModifiedDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                            
                            
                                    if (localLastModifiedDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                    }
                                    elementList.add(localLastModifiedDate);
                                } if (localLastNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lastName"));
                            
                            
                                    if (localLastName==null){
                                         throw new org.apache.axis2.databinding.ADBException("lastName cannot be null!!");
                                    }
                                    elementList.add(localLastName);
                                } if (localLastPaidDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lastPaidDate"));
                            
                            
                                    if (localLastPaidDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("lastPaidDate cannot be null!!");
                                    }
                                    elementList.add(localLastPaidDate);
                                } if (localLastReviewDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lastReviewDate"));
                            
                            
                                    if (localLastReviewDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("lastReviewDate cannot be null!!");
                                    }
                                    elementList.add(localLastReviewDate);
                                } if (localLevelTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "level"));
                            
                            
                                    if (localLevel==null){
                                         throw new org.apache.axis2.databinding.ADBException("level cannot be null!!");
                                    }
                                    elementList.add(localLevel);
                                } if (localLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "location"));
                            
                            
                                    if (localLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    }
                                    elementList.add(localLocation);
                                } if (localMaritalStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "maritalStatus"));
                            
                            
                                    if (localMaritalStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("maritalStatus cannot be null!!");
                                    }
                                    elementList.add(localMaritalStatus);
                                } if (localMiddleNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "middleName"));
                            
                            
                                    if (localMiddleName==null){
                                         throw new org.apache.axis2.databinding.ADBException("middleName cannot be null!!");
                                    }
                                    elementList.add(localMiddleName);
                                } if (localNextReviewDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "nextReviewDate"));
                            
                            
                                    if (localNextReviewDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("nextReviewDate cannot be null!!");
                                    }
                                    elementList.add(localNextReviewDate);
                                } if (localOfflineAccessTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "offlineAccess"));
                            
                            
                                    if (localOfflineAccess==null){
                                         throw new org.apache.axis2.databinding.ADBException("offlineAccess cannot be null!!");
                                    }
                                    elementList.add(localOfflineAccess);
                                } if (localPayFrequencyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "payFrequency"));
                            
                            
                                    if (localPayFrequency==null){
                                         throw new org.apache.axis2.databinding.ADBException("payFrequency cannot be null!!");
                                    }
                                    elementList.add(localPayFrequency);
                                } if (localPermChangeDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "permChangeDate"));
                            
                            
                                    if (localPermChangeDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("permChangeDate cannot be null!!");
                                    }
                                    elementList.add(localPermChangeDate);
                                } if (localPermissionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "permission"));
                            
                            
                                    if (localPermission==null){
                                         throw new org.apache.axis2.databinding.ADBException("permission cannot be null!!");
                                    }
                                    elementList.add(localPermission);
                                } if (localPermissionChangeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "permissionChange"));
                            
                            
                                    if (localPermissionChange==null){
                                         throw new org.apache.axis2.databinding.ADBException("permissionChange cannot be null!!");
                                    }
                                    elementList.add(localPermissionChange);
                                } if (localPhoneTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "phone"));
                            
                            
                                    if (localPhone==null){
                                         throw new org.apache.axis2.databinding.ADBException("phone cannot be null!!");
                                    }
                                    elementList.add(localPhone);
                                } if (localPhoneticNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "phoneticName"));
                            
                            
                                    if (localPhoneticName==null){
                                         throw new org.apache.axis2.databinding.ADBException("phoneticName cannot be null!!");
                                    }
                                    elementList.add(localPhoneticName);
                                } if (localPositionTitleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "positionTitle"));
                            
                            
                                    if (localPositionTitle==null){
                                         throw new org.apache.axis2.databinding.ADBException("positionTitle cannot be null!!");
                                    }
                                    elementList.add(localPositionTitle);
                                } if (localPrimaryEarningAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "primaryEarningAmount"));
                            
                            
                                    if (localPrimaryEarningAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("primaryEarningAmount cannot be null!!");
                                    }
                                    elementList.add(localPrimaryEarningAmount);
                                } if (localPrimaryEarningItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "primaryEarningItem"));
                            
                            
                                    if (localPrimaryEarningItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("primaryEarningItem cannot be null!!");
                                    }
                                    elementList.add(localPrimaryEarningItem);
                                } if (localPrimaryEarningTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "primaryEarningType"));
                            
                            
                                    if (localPrimaryEarningType==null){
                                         throw new org.apache.axis2.databinding.ADBException("primaryEarningType cannot be null!!");
                                    }
                                    elementList.add(localPrimaryEarningType);
                                } if (localPurchaseOrderApprovalLimitTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "purchaseOrderApprovalLimit"));
                            
                            
                                    if (localPurchaseOrderApprovalLimit==null){
                                         throw new org.apache.axis2.databinding.ADBException("purchaseOrderApprovalLimit cannot be null!!");
                                    }
                                    elementList.add(localPurchaseOrderApprovalLimit);
                                } if (localPurchaseOrderApproverTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "purchaseOrderApprover"));
                            
                            
                                    if (localPurchaseOrderApprover==null){
                                         throw new org.apache.axis2.databinding.ADBException("purchaseOrderApprover cannot be null!!");
                                    }
                                    elementList.add(localPurchaseOrderApprover);
                                } if (localPurchaseOrderLimitTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "purchaseOrderLimit"));
                            
                            
                                    if (localPurchaseOrderLimit==null){
                                         throw new org.apache.axis2.databinding.ADBException("purchaseOrderLimit cannot be null!!");
                                    }
                                    elementList.add(localPurchaseOrderLimit);
                                } if (localReleaseDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "releaseDate"));
                            
                            
                                    if (localReleaseDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("releaseDate cannot be null!!");
                                    }
                                    elementList.add(localReleaseDate);
                                } if (localResidentStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "residentStatus"));
                            
                            
                                    if (localResidentStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("residentStatus cannot be null!!");
                                    }
                                    elementList.add(localResidentStatus);
                                } if (localRoleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "role"));
                            
                            
                                    if (localRole==null){
                                         throw new org.apache.axis2.databinding.ADBException("role cannot be null!!");
                                    }
                                    elementList.add(localRole);
                                } if (localRoleChangeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "roleChange"));
                            
                            
                                    if (localRoleChange==null){
                                         throw new org.apache.axis2.databinding.ADBException("roleChange cannot be null!!");
                                    }
                                    elementList.add(localRoleChange);
                                } if (localRoleChangeDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "roleChangeDate"));
                            
                            
                                    if (localRoleChangeDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("roleChangeDate cannot be null!!");
                                    }
                                    elementList.add(localRoleChangeDate);
                                } if (localSalesRepTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "salesRep"));
                            
                            
                                    if (localSalesRep==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesRep cannot be null!!");
                                    }
                                    elementList.add(localSalesRep);
                                } if (localSalesRoleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "salesRole"));
                            
                            
                                    if (localSalesRole==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesRole cannot be null!!");
                                    }
                                    elementList.add(localSalesRole);
                                } if (localSalutationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "salutation"));
                            
                            
                                    if (localSalutation==null){
                                         throw new org.apache.axis2.databinding.ADBException("salutation cannot be null!!");
                                    }
                                    elementList.add(localSalutation);
                                } if (localSocialSecurityNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "socialSecurityNumber"));
                            
                            
                                    if (localSocialSecurityNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("socialSecurityNumber cannot be null!!");
                                    }
                                    elementList.add(localSocialSecurityNumber);
                                } if (localStartDateTimeOffCalcTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "startDateTimeOffCalc"));
                            
                            
                                    if (localStartDateTimeOffCalc==null){
                                         throw new org.apache.axis2.databinding.ADBException("startDateTimeOffCalc cannot be null!!");
                                    }
                                    elementList.add(localStartDateTimeOffCalc);
                                } if (localStateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "state"));
                            
                            
                                    if (localState==null){
                                         throw new org.apache.axis2.databinding.ADBException("state cannot be null!!");
                                    }
                                    elementList.add(localState);
                                } if (localSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "subsidiary"));
                            
                            
                                    if (localSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    }
                                    elementList.add(localSubsidiary);
                                } if (localSupervisorTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "supervisor"));
                            
                            
                                    if (localSupervisor==null){
                                         throw new org.apache.axis2.databinding.ADBException("supervisor cannot be null!!");
                                    }
                                    elementList.add(localSupervisor);
                                } if (localSupportRepTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "supportRep"));
                            
                            
                                    if (localSupportRep==null){
                                         throw new org.apache.axis2.databinding.ADBException("supportRep cannot be null!!");
                                    }
                                    elementList.add(localSupportRep);
                                } if (localTerminationCategoryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "terminationCategory"));
                            
                            
                                    if (localTerminationCategory==null){
                                         throw new org.apache.axis2.databinding.ADBException("terminationCategory cannot be null!!");
                                    }
                                    elementList.add(localTerminationCategory);
                                } if (localTerminationDetailsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "terminationDetails"));
                            
                            
                                    if (localTerminationDetails==null){
                                         throw new org.apache.axis2.databinding.ADBException("terminationDetails cannot be null!!");
                                    }
                                    elementList.add(localTerminationDetails);
                                } if (localTerminationReasonTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "terminationReason"));
                            
                            
                                    if (localTerminationReason==null){
                                         throw new org.apache.axis2.databinding.ADBException("terminationReason cannot be null!!");
                                    }
                                    elementList.add(localTerminationReason);
                                } if (localTerminationRegrettedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "terminationRegretted"));
                            
                            
                                    if (localTerminationRegretted==null){
                                         throw new org.apache.axis2.databinding.ADBException("terminationRegretted cannot be null!!");
                                    }
                                    elementList.add(localTerminationRegretted);
                                } if (localTimeApproverTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "timeApprover"));
                            
                            
                                    if (localTimeApprover==null){
                                         throw new org.apache.axis2.databinding.ADBException("timeApprover cannot be null!!");
                                    }
                                    elementList.add(localTimeApprover);
                                } if (localTimeOffPlanTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "timeOffPlan"));
                            
                            
                                    if (localTimeOffPlan==null){
                                         throw new org.apache.axis2.databinding.ADBException("timeOffPlan cannot be null!!");
                                    }
                                    elementList.add(localTimeOffPlan);
                                } if (localTitleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "title"));
                            
                            
                                    if (localTitle==null){
                                         throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                    }
                                    elementList.add(localTitle);
                                } if (localUsePerquestTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "usePerquest"));
                            
                            
                                    if (localUsePerquest==null){
                                         throw new org.apache.axis2.databinding.ADBException("usePerquest cannot be null!!");
                                    }
                                    elementList.add(localUsePerquest);
                                } if (localUseTimeDataTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "useTimeData"));
                            
                            
                                    if (localUseTimeData==null){
                                         throw new org.apache.axis2.databinding.ADBException("useTimeData cannot be null!!");
                                    }
                                    elementList.add(localUseTimeData);
                                } if (localVisaExpDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "visaExpDate"));
                            
                            
                                    if (localVisaExpDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("visaExpDate cannot be null!!");
                                    }
                                    elementList.add(localVisaExpDate);
                                } if (localVisaTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "visaType"));
                            
                            
                                    if (localVisaType==null){
                                         throw new org.apache.axis2.databinding.ADBException("visaType cannot be null!!");
                                    }
                                    elementList.add(localVisaType);
                                } if (localWithholdingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "withholding"));
                            
                            
                                    if (localWithholding==null){
                                         throw new org.apache.axis2.databinding.ADBException("withholding cannot be null!!");
                                    }
                                    elementList.add(localWithholding);
                                } if (localWorkCalendarTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "workCalendar"));
                            
                            
                                    if (localWorkCalendar==null){
                                         throw new org.apache.axis2.databinding.ADBException("workCalendar cannot be null!!");
                                    }
                                    elementList.add(localWorkCalendar);
                                } if (localWorkplaceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "workplace"));
                            
                            
                                    if (localWorkplace==null){
                                         throw new org.apache.axis2.databinding.ADBException("workplace cannot be null!!");
                                    }
                                    elementList.add(localWorkplace);
                                } if (localZipCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "zipCode"));
                            
                            
                                    if (localZipCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("zipCode cannot be null!!");
                                    }
                                    elementList.add(localZipCode);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static EmployeeSearchBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            EmployeeSearchBasic object =
                new EmployeeSearchBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"EmployeeSearchBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (EmployeeSearchBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address").equals(reader.getName())){
                                
                                                object.setAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressee").equals(reader.getName())){
                                
                                                object.setAddressee(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressLabel").equals(reader.getName())){
                                
                                                object.setAddressLabel(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressPhone").equals(reader.getName())){
                                
                                                object.setAddressPhone(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","alienNumber").equals(reader.getName())){
                                
                                                object.setAlienNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","allocation").equals(reader.getName())){
                                
                                                object.setAllocation(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","anniversary").equals(reader.getName())){
                                
                                                object.setAnniversary(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","approvalLimit").equals(reader.getName())){
                                
                                                object.setApprovalLimit(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","approver").equals(reader.getName())){
                                
                                                object.setApprover(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","attention").equals(reader.getName())){
                                
                                                object.setAttention(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","authWorkDate").equals(reader.getName())){
                                
                                                object.setAuthWorkDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","baseWage").equals(reader.getName())){
                                
                                                object.setBaseWage(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","baseWageType").equals(reader.getName())){
                                
                                                object.setBaseWageType(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billAddress").equals(reader.getName())){
                                
                                                object.setBillAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billingClass").equals(reader.getName())){
                                
                                                object.setBillingClass(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","birthDate").equals(reader.getName())){
                                
                                                object.setBirthDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","birthDay").equals(reader.getName())){
                                
                                                object.setBirthDay(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","cContribution").equals(reader.getName())){
                                
                                                object.setCContribution(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","city").equals(reader.getName())){
                                
                                                object.setCity(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                                object.set_class(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","comments").equals(reader.getName())){
                                
                                                object.setComments(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","commissionPlan").equals(reader.getName())){
                                
                                                object.setCommissionPlan(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","compensationCurrency").equals(reader.getName())){
                                
                                                object.setCompensationCurrency(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","concurrentWebServicesUser").equals(reader.getName())){
                                
                                                object.setConcurrentWebServicesUser(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","country").equals(reader.getName())){
                                
                                                object.setCountry(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","county").equals(reader.getName())){
                                
                                                object.setCounty(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated").equals(reader.getName())){
                                
                                                object.setDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","deduction").equals(reader.getName())){
                                
                                                object.setDeduction(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                                object.setDepartment(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","earning").equals(reader.getName())){
                                
                                                object.setEarning(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","education").equals(reader.getName())){
                                
                                                object.setEducation(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","eligibleForCommission").equals(reader.getName())){
                                
                                                object.setEligibleForCommission(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","email").equals(reader.getName())){
                                
                                                object.setEmail(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","employeeStatus").equals(reader.getName())){
                                
                                                object.setEmployeeStatus(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","employeeType").equals(reader.getName())){
                                
                                                object.setEmployeeType(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","employeeTypeKpi").equals(reader.getName())){
                                
                                                object.setEmployeeTypeKpi(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entityId").equals(reader.getName())){
                                
                                                object.setEntityId(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ethnicity").equals(reader.getName())){
                                
                                                object.setEthnicity(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","expenseLimit").equals(reader.getName())){
                                
                                                object.setExpenseLimit(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                
                                                object.setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString").equals(reader.getName())){
                                
                                                object.setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fax").equals(reader.getName())){
                                
                                                object.setFax(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","firstName").equals(reader.getName())){
                                
                                                object.setFirstName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","gender").equals(reader.getName())){
                                
                                                object.setGender(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","giveAccess").equals(reader.getName())){
                                
                                                object.setGiveAccess(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","globalSubscriptionStatus").equals(reader.getName())){
                                
                                                object.setGlobalSubscriptionStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","group").equals(reader.getName())){
                                
                                                object.setGroup(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","hireDate").equals(reader.getName())){
                                
                                                object.setHireDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","I9Verified").equals(reader.getName())){
                                
                                                object.setI9Verified(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","image").equals(reader.getName())){
                                
                                                object.setImage(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                                object.setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber").equals(reader.getName())){
                                
                                                object.setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isDefaultBilling").equals(reader.getName())){
                                
                                                object.setIsDefaultBilling(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isDefaultShipping").equals(reader.getName())){
                                
                                                object.setIsDefaultShipping(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isInactive").equals(reader.getName())){
                                
                                                object.setIsInactive(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isJobResource").equals(reader.getName())){
                                
                                                object.setIsJobResource(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isTemplate").equals(reader.getName())){
                                
                                                object.setIsTemplate(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","job").equals(reader.getName())){
                                
                                                object.setJob(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","jobDescription").equals(reader.getName())){
                                
                                                object.setJobDescription(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","laborCost").equals(reader.getName())){
                                
                                                object.setLaborCost(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","language").equals(reader.getName())){
                                
                                                object.setLanguage(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                                object.setLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastName").equals(reader.getName())){
                                
                                                object.setLastName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastPaidDate").equals(reader.getName())){
                                
                                                object.setLastPaidDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastReviewDate").equals(reader.getName())){
                                
                                                object.setLastReviewDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","level").equals(reader.getName())){
                                
                                                object.setLevel(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                                object.setLocation(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","maritalStatus").equals(reader.getName())){
                                
                                                object.setMaritalStatus(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","middleName").equals(reader.getName())){
                                
                                                object.setMiddleName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nextReviewDate").equals(reader.getName())){
                                
                                                object.setNextReviewDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","offlineAccess").equals(reader.getName())){
                                
                                                object.setOfflineAccess(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","payFrequency").equals(reader.getName())){
                                
                                                object.setPayFrequency(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","permChangeDate").equals(reader.getName())){
                                
                                                object.setPermChangeDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","permission").equals(reader.getName())){
                                
                                                object.setPermission(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","permissionChange").equals(reader.getName())){
                                
                                                object.setPermissionChange(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phone").equals(reader.getName())){
                                
                                                object.setPhone(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phoneticName").equals(reader.getName())){
                                
                                                object.setPhoneticName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","positionTitle").equals(reader.getName())){
                                
                                                object.setPositionTitle(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","primaryEarningAmount").equals(reader.getName())){
                                
                                                object.setPrimaryEarningAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","primaryEarningItem").equals(reader.getName())){
                                
                                                object.setPrimaryEarningItem(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","primaryEarningType").equals(reader.getName())){
                                
                                                object.setPrimaryEarningType(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","purchaseOrderApprovalLimit").equals(reader.getName())){
                                
                                                object.setPurchaseOrderApprovalLimit(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","purchaseOrderApprover").equals(reader.getName())){
                                
                                                object.setPurchaseOrderApprover(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","purchaseOrderLimit").equals(reader.getName())){
                                
                                                object.setPurchaseOrderLimit(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","releaseDate").equals(reader.getName())){
                                
                                                object.setReleaseDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","residentStatus").equals(reader.getName())){
                                
                                                object.setResidentStatus(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","role").equals(reader.getName())){
                                
                                                object.setRole(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","roleChange").equals(reader.getName())){
                                
                                                object.setRoleChange(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","roleChangeDate").equals(reader.getName())){
                                
                                                object.setRoleChangeDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesRep").equals(reader.getName())){
                                
                                                object.setSalesRep(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesRole").equals(reader.getName())){
                                
                                                object.setSalesRole(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salutation").equals(reader.getName())){
                                
                                                object.setSalutation(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","socialSecurityNumber").equals(reader.getName())){
                                
                                                object.setSocialSecurityNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDateTimeOffCalc").equals(reader.getName())){
                                
                                                object.setStartDateTimeOffCalc(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","state").equals(reader.getName())){
                                
                                                object.setState(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                                object.setSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","supervisor").equals(reader.getName())){
                                
                                                object.setSupervisor(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","supportRep").equals(reader.getName())){
                                
                                                object.setSupportRep(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","terminationCategory").equals(reader.getName())){
                                
                                                object.setTerminationCategory(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","terminationDetails").equals(reader.getName())){
                                
                                                object.setTerminationDetails(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","terminationReason").equals(reader.getName())){
                                
                                                object.setTerminationReason(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","terminationRegretted").equals(reader.getName())){
                                
                                                object.setTerminationRegretted(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","timeApprover").equals(reader.getName())){
                                
                                                object.setTimeApprover(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","timeOffPlan").equals(reader.getName())){
                                
                                                object.setTimeOffPlan(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","title").equals(reader.getName())){
                                
                                                object.setTitle(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","usePerquest").equals(reader.getName())){
                                
                                                object.setUsePerquest(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","useTimeData").equals(reader.getName())){
                                
                                                object.setUseTimeData(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","visaExpDate").equals(reader.getName())){
                                
                                                object.setVisaExpDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","visaType").equals(reader.getName())){
                                
                                                object.setVisaType(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","withholding").equals(reader.getName())){
                                
                                                object.setWithholding(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","workCalendar").equals(reader.getName())){
                                
                                                object.setWorkCalendar(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","workplace").equals(reader.getName())){
                                
                                                object.setWorkplace(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","zipCode").equals(reader.getName())){
                                
                                                object.setZipCode(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    