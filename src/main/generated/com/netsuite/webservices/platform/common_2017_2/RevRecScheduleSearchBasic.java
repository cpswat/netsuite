
/**
 * RevRecScheduleSearchBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  RevRecScheduleSearchBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class RevRecScheduleSearchBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRecordBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = RevRecScheduleSearchBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for AccountingBook
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localAccountingBook ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountingBookTracker = false ;

                           public boolean isAccountingBookSpecified(){
                               return localAccountingBookTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getAccountingBook(){
                               return localAccountingBook;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccountingBook
                               */
                               public void setAccountingBook(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localAccountingBookTracker = param != null;
                                   
                                            this.localAccountingBook=param;
                                    

                               }
                            

                        /**
                        * field for AmorStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localAmorStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmorStatusTracker = false ;

                           public boolean isAmorStatusSpecified(){
                               return localAmorStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getAmorStatus(){
                               return localAmorStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AmorStatus
                               */
                               public void setAmorStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localAmorStatusTracker = param != null;
                                   
                                            this.localAmorStatus=param;
                                    

                               }
                            

                        /**
                        * field for AmortizedAmount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localAmortizedAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmortizedAmountTracker = false ;

                           public boolean isAmortizedAmountSpecified(){
                               return localAmortizedAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getAmortizedAmount(){
                               return localAmortizedAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AmortizedAmount
                               */
                               public void setAmortizedAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localAmortizedAmountTracker = param != null;
                                   
                                            this.localAmortizedAmount=param;
                                    

                               }
                            

                        /**
                        * field for AmorType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localAmorType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmorTypeTracker = false ;

                           public boolean isAmorTypeSpecified(){
                               return localAmorTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getAmorType(){
                               return localAmorType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AmorType
                               */
                               public void setAmorType(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localAmorTypeTracker = param != null;
                                   
                                            this.localAmorType=param;
                                    

                               }
                            

                        /**
                        * field for Amount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmountTracker = false ;

                           public boolean isAmountSpecified(){
                               return localAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getAmount(){
                               return localAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Amount
                               */
                               public void setAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localAmountTracker = param != null;
                                   
                                            this.localAmount=param;
                                    

                               }
                            

                        /**
                        * field for Currency
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrencyTracker = false ;

                           public boolean isCurrencySpecified(){
                               return localCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCurrency(){
                               return localCurrency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Currency
                               */
                               public void setCurrency(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCurrencyTracker = param != null;
                                   
                                            this.localCurrency=param;
                                    

                               }
                            

                        /**
                        * field for DeferredAmount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localDeferredAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeferredAmountTracker = false ;

                           public boolean isDeferredAmountSpecified(){
                               return localDeferredAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getDeferredAmount(){
                               return localDeferredAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeferredAmount
                               */
                               public void setDeferredAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localDeferredAmountTracker = param != null;
                                   
                                            this.localDeferredAmount=param;
                                    

                               }
                            

                        /**
                        * field for DestAcct
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localDestAcct ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDestAcctTracker = false ;

                           public boolean isDestAcctSpecified(){
                               return localDestAcctTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getDestAcct(){
                               return localDestAcct;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DestAcct
                               */
                               public void setDestAcct(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localDestAcctTracker = param != null;
                                   
                                            this.localDestAcct=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localExternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdTracker = false ;

                           public boolean isExternalIdSpecified(){
                               return localExternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localExternalIdTracker = param != null;
                                   
                                            this.localExternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalIdString
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localExternalIdString ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdStringTracker = false ;

                           public boolean isExternalIdStringSpecified(){
                               return localExternalIdStringTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getExternalIdString(){
                               return localExternalIdString;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalIdString
                               */
                               public void setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localExternalIdStringTracker = param != null;
                                   
                                            this.localExternalIdString=param;
                                    

                               }
                            

                        /**
                        * field for InitialAmt
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localInitialAmt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInitialAmtTracker = false ;

                           public boolean isInitialAmtSpecified(){
                               return localInitialAmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getInitialAmt(){
                               return localInitialAmt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InitialAmt
                               */
                               public void setInitialAmt(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localInitialAmtTracker = param != null;
                                   
                                            this.localInitialAmt=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localInternalIdTracker = param != null;
                                   
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for InternalIdNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localInternalIdNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdNumberTracker = false ;

                           public boolean isInternalIdNumberSpecified(){
                               return localInternalIdNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getInternalIdNumber(){
                               return localInternalIdNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalIdNumber
                               */
                               public void setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localInternalIdNumberTracker = param != null;
                                   
                                            this.localInternalIdNumber=param;
                                    

                               }
                            

                        /**
                        * field for IsRecognized
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsRecognized ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsRecognizedTracker = false ;

                           public boolean isIsRecognizedSpecified(){
                               return localIsRecognizedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsRecognized(){
                               return localIsRecognized;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsRecognized
                               */
                               public void setIsRecognized(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsRecognizedTracker = param != null;
                                   
                                            this.localIsRecognized=param;
                                    

                               }
                            

                        /**
                        * field for JeDoc
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localJeDoc ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localJeDocTracker = false ;

                           public boolean isJeDocSpecified(){
                               return localJeDocTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getJeDoc(){
                               return localJeDoc;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param JeDoc
                               */
                               public void setJeDoc(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localJeDocTracker = param != null;
                                   
                                            this.localJeDoc=param;
                                    

                               }
                            

                        /**
                        * field for Name
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNameTracker = false ;

                           public boolean isNameSpecified(){
                               return localNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getName(){
                               return localName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Name
                               */
                               public void setName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localNameTracker = param != null;
                                   
                                            this.localName=param;
                                    

                               }
                            

                        /**
                        * field for PctComplete
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localPctComplete ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPctCompleteTracker = false ;

                           public boolean isPctCompleteSpecified(){
                               return localPctCompleteTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getPctComplete(){
                               return localPctComplete;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PctComplete
                               */
                               public void setPctComplete(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localPctCompleteTracker = param != null;
                                   
                                            this.localPctComplete=param;
                                    

                               }
                            

                        /**
                        * field for PctRecognition
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localPctRecognition ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPctRecognitionTracker = false ;

                           public boolean isPctRecognitionSpecified(){
                               return localPctRecognitionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getPctRecognition(){
                               return localPctRecognition;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PctRecognition
                               */
                               public void setPctRecognition(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localPctRecognitionTracker = param != null;
                                   
                                            this.localPctRecognition=param;
                                    

                               }
                            

                        /**
                        * field for PeriodOffset
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localPeriodOffset ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodOffsetTracker = false ;

                           public boolean isPeriodOffsetSpecified(){
                               return localPeriodOffsetTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getPeriodOffset(){
                               return localPeriodOffset;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PeriodOffset
                               */
                               public void setPeriodOffset(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localPeriodOffsetTracker = param != null;
                                   
                                            this.localPeriodOffset=param;
                                    

                               }
                            

                        /**
                        * field for PostPeriod
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localPostPeriod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPostPeriodTracker = false ;

                           public boolean isPostPeriodSpecified(){
                               return localPostPeriodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getPostPeriod(){
                               return localPostPeriod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PostPeriod
                               */
                               public void setPostPeriod(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localPostPeriodTracker = param != null;
                                   
                                            this.localPostPeriod=param;
                                    

                               }
                            

                        /**
                        * field for SchedDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localSchedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSchedDateTracker = false ;

                           public boolean isSchedDateSpecified(){
                               return localSchedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getSchedDate(){
                               return localSchedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SchedDate
                               */
                               public void setSchedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localSchedDateTracker = param != null;
                                   
                                            this.localSchedDate=param;
                                    

                               }
                            

                        /**
                        * field for ScheduleNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localScheduleNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localScheduleNumberTracker = false ;

                           public boolean isScheduleNumberSpecified(){
                               return localScheduleNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getScheduleNumber(){
                               return localScheduleNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ScheduleNumber
                               */
                               public void setScheduleNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localScheduleNumberTracker = param != null;
                                   
                                            this.localScheduleNumber=param;
                                    

                               }
                            

                        /**
                        * field for ScheduleNumberText
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localScheduleNumberText ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localScheduleNumberTextTracker = false ;

                           public boolean isScheduleNumberTextSpecified(){
                               return localScheduleNumberTextTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getScheduleNumberText(){
                               return localScheduleNumberText;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ScheduleNumberText
                               */
                               public void setScheduleNumberText(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localScheduleNumberTextTracker = param != null;
                                   
                                            this.localScheduleNumberText=param;
                                    

                               }
                            

                        /**
                        * field for SourceAcct
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSourceAcct ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSourceAcctTracker = false ;

                           public boolean isSourceAcctSpecified(){
                               return localSourceAcctTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSourceAcct(){
                               return localSourceAcct;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SourceAcct
                               */
                               public void setSourceAcct(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSourceAcctTracker = param != null;
                                   
                                            this.localSourceAcct=param;
                                    

                               }
                            

                        /**
                        * field for SrcTranPostPeriod
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSrcTranPostPeriod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSrcTranPostPeriodTracker = false ;

                           public boolean isSrcTranPostPeriodSpecified(){
                               return localSrcTranPostPeriodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSrcTranPostPeriod(){
                               return localSrcTranPostPeriod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SrcTranPostPeriod
                               */
                               public void setSrcTranPostPeriod(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSrcTranPostPeriodTracker = param != null;
                                   
                                            this.localSrcTranPostPeriod=param;
                                    

                               }
                            

                        /**
                        * field for SrcTranType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSrcTranType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSrcTranTypeTracker = false ;

                           public boolean isSrcTranTypeSpecified(){
                               return localSrcTranTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSrcTranType(){
                               return localSrcTranType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SrcTranType
                               */
                               public void setSrcTranType(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSrcTranTypeTracker = param != null;
                                   
                                            this.localSrcTranType=param;
                                    

                               }
                            

                        /**
                        * field for StartOffset
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localStartOffset ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartOffsetTracker = false ;

                           public boolean isStartOffsetSpecified(){
                               return localStartOffsetTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getStartOffset(){
                               return localStartOffset;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartOffset
                               */
                               public void setStartOffset(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localStartOffsetTracker = param != null;
                                   
                                            this.localStartOffset=param;
                                    

                               }
                            

                        /**
                        * field for TemplateName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localTemplateName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTemplateNameTracker = false ;

                           public boolean isTemplateNameSpecified(){
                               return localTemplateNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getTemplateName(){
                               return localTemplateName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TemplateName
                               */
                               public void setTemplateName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localTemplateNameTracker = param != null;
                                   
                                            this.localTemplateName=param;
                                    

                               }
                            

                        /**
                        * field for UseForeignAmounts
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localUseForeignAmounts ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUseForeignAmountsTracker = false ;

                           public boolean isUseForeignAmountsSpecified(){
                               return localUseForeignAmountsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getUseForeignAmounts(){
                               return localUseForeignAmounts;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UseForeignAmounts
                               */
                               public void setUseForeignAmounts(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localUseForeignAmountsTracker = param != null;
                                   
                                            this.localUseForeignAmounts=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":RevRecScheduleSearchBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "RevRecScheduleSearchBasic",
                           xmlWriter);
                   }

                if (localAccountingBookTracker){
                                            if (localAccountingBook==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accountingBook cannot be null!!");
                                            }
                                           localAccountingBook.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","accountingBook"),
                                               xmlWriter);
                                        } if (localAmorStatusTracker){
                                            if (localAmorStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("amorStatus cannot be null!!");
                                            }
                                           localAmorStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","amorStatus"),
                                               xmlWriter);
                                        } if (localAmortizedAmountTracker){
                                            if (localAmortizedAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("amortizedAmount cannot be null!!");
                                            }
                                           localAmortizedAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","amortizedAmount"),
                                               xmlWriter);
                                        } if (localAmorTypeTracker){
                                            if (localAmorType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("amorType cannot be null!!");
                                            }
                                           localAmorType.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","amorType"),
                                               xmlWriter);
                                        } if (localAmountTracker){
                                            if (localAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("amount cannot be null!!");
                                            }
                                           localAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","amount"),
                                               xmlWriter);
                                        } if (localCurrencyTracker){
                                            if (localCurrency==null){
                                                 throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                            }
                                           localCurrency.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency"),
                                               xmlWriter);
                                        } if (localDeferredAmountTracker){
                                            if (localDeferredAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("deferredAmount cannot be null!!");
                                            }
                                           localDeferredAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","deferredAmount"),
                                               xmlWriter);
                                        } if (localDestAcctTracker){
                                            if (localDestAcct==null){
                                                 throw new org.apache.axis2.databinding.ADBException("destAcct cannot be null!!");
                                            }
                                           localDestAcct.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","destAcct"),
                                               xmlWriter);
                                        } if (localExternalIdTracker){
                                            if (localExternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                            }
                                           localExternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId"),
                                               xmlWriter);
                                        } if (localExternalIdStringTracker){
                                            if (localExternalIdString==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                            }
                                           localExternalIdString.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString"),
                                               xmlWriter);
                                        } if (localInitialAmtTracker){
                                            if (localInitialAmt==null){
                                                 throw new org.apache.axis2.databinding.ADBException("initialAmt cannot be null!!");
                                            }
                                           localInitialAmt.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","initialAmt"),
                                               xmlWriter);
                                        } if (localInternalIdTracker){
                                            if (localInternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                            }
                                           localInternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                               xmlWriter);
                                        } if (localInternalIdNumberTracker){
                                            if (localInternalIdNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                            }
                                           localInternalIdNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber"),
                                               xmlWriter);
                                        } if (localIsRecognizedTracker){
                                            if (localIsRecognized==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isRecognized cannot be null!!");
                                            }
                                           localIsRecognized.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isRecognized"),
                                               xmlWriter);
                                        } if (localJeDocTracker){
                                            if (localJeDoc==null){
                                                 throw new org.apache.axis2.databinding.ADBException("jeDoc cannot be null!!");
                                            }
                                           localJeDoc.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","jeDoc"),
                                               xmlWriter);
                                        } if (localNameTracker){
                                            if (localName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                            }
                                           localName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","name"),
                                               xmlWriter);
                                        } if (localPctCompleteTracker){
                                            if (localPctComplete==null){
                                                 throw new org.apache.axis2.databinding.ADBException("pctComplete cannot be null!!");
                                            }
                                           localPctComplete.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","pctComplete"),
                                               xmlWriter);
                                        } if (localPctRecognitionTracker){
                                            if (localPctRecognition==null){
                                                 throw new org.apache.axis2.databinding.ADBException("pctRecognition cannot be null!!");
                                            }
                                           localPctRecognition.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","pctRecognition"),
                                               xmlWriter);
                                        } if (localPeriodOffsetTracker){
                                            if (localPeriodOffset==null){
                                                 throw new org.apache.axis2.databinding.ADBException("periodOffset cannot be null!!");
                                            }
                                           localPeriodOffset.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","periodOffset"),
                                               xmlWriter);
                                        } if (localPostPeriodTracker){
                                            if (localPostPeriod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("postPeriod cannot be null!!");
                                            }
                                           localPostPeriod.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","postPeriod"),
                                               xmlWriter);
                                        } if (localSchedDateTracker){
                                            if (localSchedDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("schedDate cannot be null!!");
                                            }
                                           localSchedDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","schedDate"),
                                               xmlWriter);
                                        } if (localScheduleNumberTracker){
                                            if (localScheduleNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("scheduleNumber cannot be null!!");
                                            }
                                           localScheduleNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","scheduleNumber"),
                                               xmlWriter);
                                        } if (localScheduleNumberTextTracker){
                                            if (localScheduleNumberText==null){
                                                 throw new org.apache.axis2.databinding.ADBException("scheduleNumberText cannot be null!!");
                                            }
                                           localScheduleNumberText.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","scheduleNumberText"),
                                               xmlWriter);
                                        } if (localSourceAcctTracker){
                                            if (localSourceAcct==null){
                                                 throw new org.apache.axis2.databinding.ADBException("sourceAcct cannot be null!!");
                                            }
                                           localSourceAcct.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","sourceAcct"),
                                               xmlWriter);
                                        } if (localSrcTranPostPeriodTracker){
                                            if (localSrcTranPostPeriod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("srcTranPostPeriod cannot be null!!");
                                            }
                                           localSrcTranPostPeriod.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","srcTranPostPeriod"),
                                               xmlWriter);
                                        } if (localSrcTranTypeTracker){
                                            if (localSrcTranType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("srcTranType cannot be null!!");
                                            }
                                           localSrcTranType.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","srcTranType"),
                                               xmlWriter);
                                        } if (localStartOffsetTracker){
                                            if (localStartOffset==null){
                                                 throw new org.apache.axis2.databinding.ADBException("startOffset cannot be null!!");
                                            }
                                           localStartOffset.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startOffset"),
                                               xmlWriter);
                                        } if (localTemplateNameTracker){
                                            if (localTemplateName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("templateName cannot be null!!");
                                            }
                                           localTemplateName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","templateName"),
                                               xmlWriter);
                                        } if (localUseForeignAmountsTracker){
                                            if (localUseForeignAmounts==null){
                                                 throw new org.apache.axis2.databinding.ADBException("useForeignAmounts cannot be null!!");
                                            }
                                           localUseForeignAmounts.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","useForeignAmounts"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","RevRecScheduleSearchBasic"));
                 if (localAccountingBookTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "accountingBook"));
                            
                            
                                    if (localAccountingBook==null){
                                         throw new org.apache.axis2.databinding.ADBException("accountingBook cannot be null!!");
                                    }
                                    elementList.add(localAccountingBook);
                                } if (localAmorStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "amorStatus"));
                            
                            
                                    if (localAmorStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("amorStatus cannot be null!!");
                                    }
                                    elementList.add(localAmorStatus);
                                } if (localAmortizedAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "amortizedAmount"));
                            
                            
                                    if (localAmortizedAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("amortizedAmount cannot be null!!");
                                    }
                                    elementList.add(localAmortizedAmount);
                                } if (localAmorTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "amorType"));
                            
                            
                                    if (localAmorType==null){
                                         throw new org.apache.axis2.databinding.ADBException("amorType cannot be null!!");
                                    }
                                    elementList.add(localAmorType);
                                } if (localAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "amount"));
                            
                            
                                    if (localAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("amount cannot be null!!");
                                    }
                                    elementList.add(localAmount);
                                } if (localCurrencyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "currency"));
                            
                            
                                    if (localCurrency==null){
                                         throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                    }
                                    elementList.add(localCurrency);
                                } if (localDeferredAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "deferredAmount"));
                            
                            
                                    if (localDeferredAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("deferredAmount cannot be null!!");
                                    }
                                    elementList.add(localDeferredAmount);
                                } if (localDestAcctTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "destAcct"));
                            
                            
                                    if (localDestAcct==null){
                                         throw new org.apache.axis2.databinding.ADBException("destAcct cannot be null!!");
                                    }
                                    elementList.add(localDestAcct);
                                } if (localExternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalId"));
                            
                            
                                    if (localExternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                    }
                                    elementList.add(localExternalId);
                                } if (localExternalIdStringTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalIdString"));
                            
                            
                                    if (localExternalIdString==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                    }
                                    elementList.add(localExternalIdString);
                                } if (localInitialAmtTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "initialAmt"));
                            
                            
                                    if (localInitialAmt==null){
                                         throw new org.apache.axis2.databinding.ADBException("initialAmt cannot be null!!");
                                    }
                                    elementList.add(localInitialAmt);
                                } if (localInternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalId"));
                            
                            
                                    if (localInternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    }
                                    elementList.add(localInternalId);
                                } if (localInternalIdNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalIdNumber"));
                            
                            
                                    if (localInternalIdNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                    }
                                    elementList.add(localInternalIdNumber);
                                } if (localIsRecognizedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isRecognized"));
                            
                            
                                    if (localIsRecognized==null){
                                         throw new org.apache.axis2.databinding.ADBException("isRecognized cannot be null!!");
                                    }
                                    elementList.add(localIsRecognized);
                                } if (localJeDocTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "jeDoc"));
                            
                            
                                    if (localJeDoc==null){
                                         throw new org.apache.axis2.databinding.ADBException("jeDoc cannot be null!!");
                                    }
                                    elementList.add(localJeDoc);
                                } if (localNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "name"));
                            
                            
                                    if (localName==null){
                                         throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                    }
                                    elementList.add(localName);
                                } if (localPctCompleteTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "pctComplete"));
                            
                            
                                    if (localPctComplete==null){
                                         throw new org.apache.axis2.databinding.ADBException("pctComplete cannot be null!!");
                                    }
                                    elementList.add(localPctComplete);
                                } if (localPctRecognitionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "pctRecognition"));
                            
                            
                                    if (localPctRecognition==null){
                                         throw new org.apache.axis2.databinding.ADBException("pctRecognition cannot be null!!");
                                    }
                                    elementList.add(localPctRecognition);
                                } if (localPeriodOffsetTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "periodOffset"));
                            
                            
                                    if (localPeriodOffset==null){
                                         throw new org.apache.axis2.databinding.ADBException("periodOffset cannot be null!!");
                                    }
                                    elementList.add(localPeriodOffset);
                                } if (localPostPeriodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "postPeriod"));
                            
                            
                                    if (localPostPeriod==null){
                                         throw new org.apache.axis2.databinding.ADBException("postPeriod cannot be null!!");
                                    }
                                    elementList.add(localPostPeriod);
                                } if (localSchedDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "schedDate"));
                            
                            
                                    if (localSchedDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("schedDate cannot be null!!");
                                    }
                                    elementList.add(localSchedDate);
                                } if (localScheduleNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "scheduleNumber"));
                            
                            
                                    if (localScheduleNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("scheduleNumber cannot be null!!");
                                    }
                                    elementList.add(localScheduleNumber);
                                } if (localScheduleNumberTextTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "scheduleNumberText"));
                            
                            
                                    if (localScheduleNumberText==null){
                                         throw new org.apache.axis2.databinding.ADBException("scheduleNumberText cannot be null!!");
                                    }
                                    elementList.add(localScheduleNumberText);
                                } if (localSourceAcctTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "sourceAcct"));
                            
                            
                                    if (localSourceAcct==null){
                                         throw new org.apache.axis2.databinding.ADBException("sourceAcct cannot be null!!");
                                    }
                                    elementList.add(localSourceAcct);
                                } if (localSrcTranPostPeriodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "srcTranPostPeriod"));
                            
                            
                                    if (localSrcTranPostPeriod==null){
                                         throw new org.apache.axis2.databinding.ADBException("srcTranPostPeriod cannot be null!!");
                                    }
                                    elementList.add(localSrcTranPostPeriod);
                                } if (localSrcTranTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "srcTranType"));
                            
                            
                                    if (localSrcTranType==null){
                                         throw new org.apache.axis2.databinding.ADBException("srcTranType cannot be null!!");
                                    }
                                    elementList.add(localSrcTranType);
                                } if (localStartOffsetTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "startOffset"));
                            
                            
                                    if (localStartOffset==null){
                                         throw new org.apache.axis2.databinding.ADBException("startOffset cannot be null!!");
                                    }
                                    elementList.add(localStartOffset);
                                } if (localTemplateNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "templateName"));
                            
                            
                                    if (localTemplateName==null){
                                         throw new org.apache.axis2.databinding.ADBException("templateName cannot be null!!");
                                    }
                                    elementList.add(localTemplateName);
                                } if (localUseForeignAmountsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "useForeignAmounts"));
                            
                            
                                    if (localUseForeignAmounts==null){
                                         throw new org.apache.axis2.databinding.ADBException("useForeignAmounts cannot be null!!");
                                    }
                                    elementList.add(localUseForeignAmounts);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static RevRecScheduleSearchBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            RevRecScheduleSearchBasic object =
                new RevRecScheduleSearchBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"RevRecScheduleSearchBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (RevRecScheduleSearchBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","accountingBook").equals(reader.getName())){
                                
                                                object.setAccountingBook(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","amorStatus").equals(reader.getName())){
                                
                                                object.setAmorStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","amortizedAmount").equals(reader.getName())){
                                
                                                object.setAmortizedAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","amorType").equals(reader.getName())){
                                
                                                object.setAmorType(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","amount").equals(reader.getName())){
                                
                                                object.setAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency").equals(reader.getName())){
                                
                                                object.setCurrency(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","deferredAmount").equals(reader.getName())){
                                
                                                object.setDeferredAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","destAcct").equals(reader.getName())){
                                
                                                object.setDestAcct(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                
                                                object.setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString").equals(reader.getName())){
                                
                                                object.setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","initialAmt").equals(reader.getName())){
                                
                                                object.setInitialAmt(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                                object.setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber").equals(reader.getName())){
                                
                                                object.setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isRecognized").equals(reader.getName())){
                                
                                                object.setIsRecognized(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","jeDoc").equals(reader.getName())){
                                
                                                object.setJeDoc(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","name").equals(reader.getName())){
                                
                                                object.setName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","pctComplete").equals(reader.getName())){
                                
                                                object.setPctComplete(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","pctRecognition").equals(reader.getName())){
                                
                                                object.setPctRecognition(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","periodOffset").equals(reader.getName())){
                                
                                                object.setPeriodOffset(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","postPeriod").equals(reader.getName())){
                                
                                                object.setPostPeriod(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","schedDate").equals(reader.getName())){
                                
                                                object.setSchedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","scheduleNumber").equals(reader.getName())){
                                
                                                object.setScheduleNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","scheduleNumberText").equals(reader.getName())){
                                
                                                object.setScheduleNumberText(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","sourceAcct").equals(reader.getName())){
                                
                                                object.setSourceAcct(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","srcTranPostPeriod").equals(reader.getName())){
                                
                                                object.setSrcTranPostPeriod(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","srcTranType").equals(reader.getName())){
                                
                                                object.setSrcTranType(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startOffset").equals(reader.getName())){
                                
                                                object.setStartOffset(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","templateName").equals(reader.getName())){
                                
                                                object.setTemplateName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","useForeignAmounts").equals(reader.getName())){
                                
                                                object.setUseForeignAmounts(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    