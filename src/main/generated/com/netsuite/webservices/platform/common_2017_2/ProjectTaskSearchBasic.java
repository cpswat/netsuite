
/**
 * ProjectTaskSearchBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  ProjectTaskSearchBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ProjectTaskSearchBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRecordBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ProjectTaskSearchBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for ActualWork
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localActualWork ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localActualWorkTracker = false ;

                           public boolean isActualWorkSpecified(){
                               return localActualWorkTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getActualWork(){
                               return localActualWork;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ActualWork
                               */
                               public void setActualWork(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localActualWorkTracker = param != null;
                                   
                                            this.localActualWork=param;
                                    

                               }
                            

                        /**
                        * field for Assignee
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localAssignee ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAssigneeTracker = false ;

                           public boolean isAssigneeSpecified(){
                               return localAssigneeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getAssignee(){
                               return localAssignee;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Assignee
                               */
                               public void setAssignee(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localAssigneeTracker = param != null;
                                   
                                            this.localAssignee=param;
                                    

                               }
                            

                        /**
                        * field for Company
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCompany ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCompanyTracker = false ;

                           public boolean isCompanySpecified(){
                               return localCompanyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCompany(){
                               return localCompany;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Company
                               */
                               public void setCompany(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCompanyTracker = param != null;
                                   
                                            this.localCompany=param;
                                    

                               }
                            

                        /**
                        * field for ConstraintType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localConstraintType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localConstraintTypeTracker = false ;

                           public boolean isConstraintTypeSpecified(){
                               return localConstraintTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getConstraintType(){
                               return localConstraintType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ConstraintType
                               */
                               public void setConstraintType(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localConstraintTypeTracker = param != null;
                                   
                                            this.localConstraintType=param;
                                    

                               }
                            

                        /**
                        * field for Contact
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localContact ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContactTracker = false ;

                           public boolean isContactSpecified(){
                               return localContactTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getContact(){
                               return localContact;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Contact
                               */
                               public void setContact(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localContactTracker = param != null;
                                   
                                            this.localContact=param;
                                    

                               }
                            

                        /**
                        * field for Cost
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localCost ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostTracker = false ;

                           public boolean isCostSpecified(){
                               return localCostTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getCost(){
                               return localCost;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Cost
                               */
                               public void setCost(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localCostTracker = param != null;
                                   
                                            this.localCost=param;
                                    

                               }
                            

                        /**
                        * field for CostBase
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localCostBase ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostBaseTracker = false ;

                           public boolean isCostBaseSpecified(){
                               return localCostBaseTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getCostBase(){
                               return localCostBase;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostBase
                               */
                               public void setCostBase(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localCostBaseTracker = param != null;
                                   
                                            this.localCostBase=param;
                                    

                               }
                            

                        /**
                        * field for CostBaseBaseline
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localCostBaseBaseline ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostBaseBaselineTracker = false ;

                           public boolean isCostBaseBaselineSpecified(){
                               return localCostBaseBaselineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getCostBaseBaseline(){
                               return localCostBaseBaseline;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostBaseBaseline
                               */
                               public void setCostBaseBaseline(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localCostBaseBaselineTracker = param != null;
                                   
                                            this.localCostBaseBaseline=param;
                                    

                               }
                            

                        /**
                        * field for CostBaseline
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localCostBaseline ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostBaselineTracker = false ;

                           public boolean isCostBaselineSpecified(){
                               return localCostBaselineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getCostBaseline(){
                               return localCostBaseline;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostBaseline
                               */
                               public void setCostBaseline(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localCostBaselineTracker = param != null;
                                   
                                            this.localCostBaseline=param;
                                    

                               }
                            

                        /**
                        * field for CostBaseVariance
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localCostBaseVariance ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostBaseVarianceTracker = false ;

                           public boolean isCostBaseVarianceSpecified(){
                               return localCostBaseVarianceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getCostBaseVariance(){
                               return localCostBaseVariance;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostBaseVariance
                               */
                               public void setCostBaseVariance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localCostBaseVarianceTracker = param != null;
                                   
                                            this.localCostBaseVariance=param;
                                    

                               }
                            

                        /**
                        * field for CostVariance
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localCostVariance ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostVarianceTracker = false ;

                           public boolean isCostVarianceSpecified(){
                               return localCostVarianceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getCostVariance(){
                               return localCostVariance;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostVariance
                               */
                               public void setCostVariance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localCostVarianceTracker = param != null;
                                   
                                            this.localCostVariance=param;
                                    

                               }
                            

                        /**
                        * field for CostVariancePercent
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localCostVariancePercent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCostVariancePercentTracker = false ;

                           public boolean isCostVariancePercentSpecified(){
                               return localCostVariancePercentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getCostVariancePercent(){
                               return localCostVariancePercent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CostVariancePercent
                               */
                               public void setCostVariancePercent(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localCostVariancePercentTracker = param != null;
                                   
                                            this.localCostVariancePercent=param;
                                    

                               }
                            

                        /**
                        * field for CreatedDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localCreatedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedDateTracker = false ;

                           public boolean isCreatedDateSpecified(){
                               return localCreatedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getCreatedDate(){
                               return localCreatedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedDate
                               */
                               public void setCreatedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localCreatedDateTracker = param != null;
                                   
                                            this.localCreatedDate=param;
                                    

                               }
                            

                        /**
                        * field for EndDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndDateTracker = false ;

                           public boolean isEndDateSpecified(){
                               return localEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getEndDate(){
                               return localEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndDate
                               */
                               public void setEndDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localEndDateTracker = param != null;
                                   
                                            this.localEndDate=param;
                                    

                               }
                            

                        /**
                        * field for EndDateBaseline
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localEndDateBaseline ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndDateBaselineTracker = false ;

                           public boolean isEndDateBaselineSpecified(){
                               return localEndDateBaselineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getEndDateBaseline(){
                               return localEndDateBaseline;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndDateBaseline
                               */
                               public void setEndDateBaseline(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localEndDateBaselineTracker = param != null;
                                   
                                            this.localEndDateBaseline=param;
                                    

                               }
                            

                        /**
                        * field for EndDateVariance
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localEndDateVariance ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndDateVarianceTracker = false ;

                           public boolean isEndDateVarianceSpecified(){
                               return localEndDateVarianceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getEndDateVariance(){
                               return localEndDateVariance;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndDateVariance
                               */
                               public void setEndDateVariance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localEndDateVarianceTracker = param != null;
                                   
                                            this.localEndDateVariance=param;
                                    

                               }
                            

                        /**
                        * field for EstimatedWork
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localEstimatedWork ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstimatedWorkTracker = false ;

                           public boolean isEstimatedWorkSpecified(){
                               return localEstimatedWorkTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getEstimatedWork(){
                               return localEstimatedWork;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstimatedWork
                               */
                               public void setEstimatedWork(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localEstimatedWorkTracker = param != null;
                                   
                                            this.localEstimatedWork=param;
                                    

                               }
                            

                        /**
                        * field for EstimatedWorkBaseline
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localEstimatedWorkBaseline ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstimatedWorkBaselineTracker = false ;

                           public boolean isEstimatedWorkBaselineSpecified(){
                               return localEstimatedWorkBaselineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getEstimatedWorkBaseline(){
                               return localEstimatedWorkBaseline;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstimatedWorkBaseline
                               */
                               public void setEstimatedWorkBaseline(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localEstimatedWorkBaselineTracker = param != null;
                                   
                                            this.localEstimatedWorkBaseline=param;
                                    

                               }
                            

                        /**
                        * field for EstimatedWorkVariance
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localEstimatedWorkVariance ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstimatedWorkVarianceTracker = false ;

                           public boolean isEstimatedWorkVarianceSpecified(){
                               return localEstimatedWorkVarianceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getEstimatedWorkVariance(){
                               return localEstimatedWorkVariance;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstimatedWorkVariance
                               */
                               public void setEstimatedWorkVariance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localEstimatedWorkVarianceTracker = param != null;
                                   
                                            this.localEstimatedWorkVariance=param;
                                    

                               }
                            

                        /**
                        * field for EstimatedWorkVariancePercent
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localEstimatedWorkVariancePercent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstimatedWorkVariancePercentTracker = false ;

                           public boolean isEstimatedWorkVariancePercentSpecified(){
                               return localEstimatedWorkVariancePercentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getEstimatedWorkVariancePercent(){
                               return localEstimatedWorkVariancePercent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstimatedWorkVariancePercent
                               */
                               public void setEstimatedWorkVariancePercent(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localEstimatedWorkVariancePercentTracker = param != null;
                                   
                                            this.localEstimatedWorkVariancePercent=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localExternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdTracker = false ;

                           public boolean isExternalIdSpecified(){
                               return localExternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localExternalIdTracker = param != null;
                                   
                                            this.localExternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalIdString
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localExternalIdString ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdStringTracker = false ;

                           public boolean isExternalIdStringSpecified(){
                               return localExternalIdStringTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getExternalIdString(){
                               return localExternalIdString;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalIdString
                               */
                               public void setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localExternalIdStringTracker = param != null;
                                   
                                            this.localExternalIdString=param;
                                    

                               }
                            

                        /**
                        * field for FinishByDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localFinishByDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFinishByDateTracker = false ;

                           public boolean isFinishByDateSpecified(){
                               return localFinishByDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getFinishByDate(){
                               return localFinishByDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FinishByDate
                               */
                               public void setFinishByDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localFinishByDateTracker = param != null;
                                   
                                            this.localFinishByDate=param;
                                    

                               }
                            

                        /**
                        * field for Id
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIdTracker = false ;

                           public boolean isIdSpecified(){
                               return localIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getId(){
                               return localId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Id
                               */
                               public void setId(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localIdTracker = param != null;
                                   
                                            this.localId=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localInternalIdTracker = param != null;
                                   
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for InternalIdNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localInternalIdNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdNumberTracker = false ;

                           public boolean isInternalIdNumberSpecified(){
                               return localInternalIdNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getInternalIdNumber(){
                               return localInternalIdNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalIdNumber
                               */
                               public void setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localInternalIdNumberTracker = param != null;
                                   
                                            this.localInternalIdNumber=param;
                                    

                               }
                            

                        /**
                        * field for IsMilestone
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsMilestone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsMilestoneTracker = false ;

                           public boolean isIsMilestoneSpecified(){
                               return localIsMilestoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsMilestone(){
                               return localIsMilestone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsMilestone
                               */
                               public void setIsMilestone(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsMilestoneTracker = param != null;
                                   
                                            this.localIsMilestone=param;
                                    

                               }
                            

                        /**
                        * field for IsSummaryTask
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsSummaryTask ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsSummaryTaskTracker = false ;

                           public boolean isIsSummaryTaskSpecified(){
                               return localIsSummaryTaskTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsSummaryTask(){
                               return localIsSummaryTask;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsSummaryTask
                               */
                               public void setIsSummaryTask(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsSummaryTaskTracker = param != null;
                                   
                                            this.localIsSummaryTask=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for NonBillableTask
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localNonBillableTask ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNonBillableTaskTracker = false ;

                           public boolean isNonBillableTaskSpecified(){
                               return localNonBillableTaskTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getNonBillableTask(){
                               return localNonBillableTask;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NonBillableTask
                               */
                               public void setNonBillableTask(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localNonBillableTaskTracker = param != null;
                                   
                                            this.localNonBillableTask=param;
                                    

                               }
                            

                        /**
                        * field for Owner
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localOwner ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOwnerTracker = false ;

                           public boolean isOwnerSpecified(){
                               return localOwnerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getOwner(){
                               return localOwner;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Owner
                               */
                               public void setOwner(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localOwnerTracker = param != null;
                                   
                                            this.localOwner=param;
                                    

                               }
                            

                        /**
                        * field for Parent
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localParent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParentTracker = false ;

                           public boolean isParentSpecified(){
                               return localParentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getParent(){
                               return localParent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Parent
                               */
                               public void setParent(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localParentTracker = param != null;
                                   
                                            this.localParent=param;
                                    

                               }
                            

                        /**
                        * field for PercentWorkComplete
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localPercentWorkComplete ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPercentWorkCompleteTracker = false ;

                           public boolean isPercentWorkCompleteSpecified(){
                               return localPercentWorkCompleteTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getPercentWorkComplete(){
                               return localPercentWorkComplete;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PercentWorkComplete
                               */
                               public void setPercentWorkComplete(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localPercentWorkCompleteTracker = param != null;
                                   
                                            this.localPercentWorkComplete=param;
                                    

                               }
                            

                        /**
                        * field for Predecessor
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localPredecessor ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPredecessorTracker = false ;

                           public boolean isPredecessorSpecified(){
                               return localPredecessorTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getPredecessor(){
                               return localPredecessor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Predecessor
                               */
                               public void setPredecessor(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localPredecessorTracker = param != null;
                                   
                                            this.localPredecessor=param;
                                    

                               }
                            

                        /**
                        * field for Predecessors
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localPredecessors ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPredecessorsTracker = false ;

                           public boolean isPredecessorsSpecified(){
                               return localPredecessorsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getPredecessors(){
                               return localPredecessors;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Predecessors
                               */
                               public void setPredecessors(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localPredecessorsTracker = param != null;
                                   
                                            this.localPredecessors=param;
                                    

                               }
                            

                        /**
                        * field for Priority
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localPriority ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPriorityTracker = false ;

                           public boolean isPrioritySpecified(){
                               return localPriorityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getPriority(){
                               return localPriority;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Priority
                               */
                               public void setPriority(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localPriorityTracker = param != null;
                                   
                                            this.localPriority=param;
                                    

                               }
                            

                        /**
                        * field for RemainingWork
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localRemainingWork ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRemainingWorkTracker = false ;

                           public boolean isRemainingWorkSpecified(){
                               return localRemainingWorkTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getRemainingWork(){
                               return localRemainingWork;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RemainingWork
                               */
                               public void setRemainingWork(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localRemainingWorkTracker = param != null;
                                   
                                            this.localRemainingWork=param;
                                    

                               }
                            

                        /**
                        * field for StartDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateTracker = false ;

                           public boolean isStartDateSpecified(){
                               return localStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getStartDate(){
                               return localStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDate
                               */
                               public void setStartDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localStartDateTracker = param != null;
                                   
                                            this.localStartDate=param;
                                    

                               }
                            

                        /**
                        * field for StartDateBaseline
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localStartDateBaseline ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateBaselineTracker = false ;

                           public boolean isStartDateBaselineSpecified(){
                               return localStartDateBaselineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getStartDateBaseline(){
                               return localStartDateBaseline;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDateBaseline
                               */
                               public void setStartDateBaseline(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localStartDateBaselineTracker = param != null;
                                   
                                            this.localStartDateBaseline=param;
                                    

                               }
                            

                        /**
                        * field for StartDateVariance
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localStartDateVariance ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateVarianceTracker = false ;

                           public boolean isStartDateVarianceSpecified(){
                               return localStartDateVarianceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getStartDateVariance(){
                               return localStartDateVariance;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDateVariance
                               */
                               public void setStartDateVariance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localStartDateVarianceTracker = param != null;
                                   
                                            this.localStartDateVariance=param;
                                    

                               }
                            

                        /**
                        * field for Status
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStatusTracker = false ;

                           public boolean isStatusSpecified(){
                               return localStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getStatus(){
                               return localStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Status
                               */
                               public void setStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localStatusTracker = param != null;
                                   
                                            this.localStatus=param;
                                    

                               }
                            

                        /**
                        * field for Successor
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSuccessor ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSuccessorTracker = false ;

                           public boolean isSuccessorSpecified(){
                               return localSuccessorTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSuccessor(){
                               return localSuccessor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Successor
                               */
                               public void setSuccessor(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSuccessorTracker = param != null;
                                   
                                            this.localSuccessor=param;
                                    

                               }
                            

                        /**
                        * field for Title
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTitleTracker = false ;

                           public boolean isTitleSpecified(){
                               return localTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getTitle(){
                               return localTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Title
                               */
                               public void setTitle(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localTitleTracker = param != null;
                                   
                                            this.localTitle=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ProjectTaskSearchBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ProjectTaskSearchBasic",
                           xmlWriter);
                   }

                if (localActualWorkTracker){
                                            if (localActualWork==null){
                                                 throw new org.apache.axis2.databinding.ADBException("actualWork cannot be null!!");
                                            }
                                           localActualWork.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","actualWork"),
                                               xmlWriter);
                                        } if (localAssigneeTracker){
                                            if (localAssignee==null){
                                                 throw new org.apache.axis2.databinding.ADBException("assignee cannot be null!!");
                                            }
                                           localAssignee.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","assignee"),
                                               xmlWriter);
                                        } if (localCompanyTracker){
                                            if (localCompany==null){
                                                 throw new org.apache.axis2.databinding.ADBException("company cannot be null!!");
                                            }
                                           localCompany.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","company"),
                                               xmlWriter);
                                        } if (localConstraintTypeTracker){
                                            if (localConstraintType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("constraintType cannot be null!!");
                                            }
                                           localConstraintType.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","constraintType"),
                                               xmlWriter);
                                        } if (localContactTracker){
                                            if (localContact==null){
                                                 throw new org.apache.axis2.databinding.ADBException("contact cannot be null!!");
                                            }
                                           localContact.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","contact"),
                                               xmlWriter);
                                        } if (localCostTracker){
                                            if (localCost==null){
                                                 throw new org.apache.axis2.databinding.ADBException("cost cannot be null!!");
                                            }
                                           localCost.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","cost"),
                                               xmlWriter);
                                        } if (localCostBaseTracker){
                                            if (localCostBase==null){
                                                 throw new org.apache.axis2.databinding.ADBException("costBase cannot be null!!");
                                            }
                                           localCostBase.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","costBase"),
                                               xmlWriter);
                                        } if (localCostBaseBaselineTracker){
                                            if (localCostBaseBaseline==null){
                                                 throw new org.apache.axis2.databinding.ADBException("costBaseBaseline cannot be null!!");
                                            }
                                           localCostBaseBaseline.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","costBaseBaseline"),
                                               xmlWriter);
                                        } if (localCostBaselineTracker){
                                            if (localCostBaseline==null){
                                                 throw new org.apache.axis2.databinding.ADBException("costBaseline cannot be null!!");
                                            }
                                           localCostBaseline.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","costBaseline"),
                                               xmlWriter);
                                        } if (localCostBaseVarianceTracker){
                                            if (localCostBaseVariance==null){
                                                 throw new org.apache.axis2.databinding.ADBException("costBaseVariance cannot be null!!");
                                            }
                                           localCostBaseVariance.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","costBaseVariance"),
                                               xmlWriter);
                                        } if (localCostVarianceTracker){
                                            if (localCostVariance==null){
                                                 throw new org.apache.axis2.databinding.ADBException("costVariance cannot be null!!");
                                            }
                                           localCostVariance.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","costVariance"),
                                               xmlWriter);
                                        } if (localCostVariancePercentTracker){
                                            if (localCostVariancePercent==null){
                                                 throw new org.apache.axis2.databinding.ADBException("costVariancePercent cannot be null!!");
                                            }
                                           localCostVariancePercent.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","costVariancePercent"),
                                               xmlWriter);
                                        } if (localCreatedDateTracker){
                                            if (localCreatedDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                            }
                                           localCreatedDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","createdDate"),
                                               xmlWriter);
                                        } if (localEndDateTracker){
                                            if (localEndDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                            }
                                           localEndDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endDate"),
                                               xmlWriter);
                                        } if (localEndDateBaselineTracker){
                                            if (localEndDateBaseline==null){
                                                 throw new org.apache.axis2.databinding.ADBException("endDateBaseline cannot be null!!");
                                            }
                                           localEndDateBaseline.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endDateBaseline"),
                                               xmlWriter);
                                        } if (localEndDateVarianceTracker){
                                            if (localEndDateVariance==null){
                                                 throw new org.apache.axis2.databinding.ADBException("endDateVariance cannot be null!!");
                                            }
                                           localEndDateVariance.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endDateVariance"),
                                               xmlWriter);
                                        } if (localEstimatedWorkTracker){
                                            if (localEstimatedWork==null){
                                                 throw new org.apache.axis2.databinding.ADBException("estimatedWork cannot be null!!");
                                            }
                                           localEstimatedWork.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedWork"),
                                               xmlWriter);
                                        } if (localEstimatedWorkBaselineTracker){
                                            if (localEstimatedWorkBaseline==null){
                                                 throw new org.apache.axis2.databinding.ADBException("estimatedWorkBaseline cannot be null!!");
                                            }
                                           localEstimatedWorkBaseline.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedWorkBaseline"),
                                               xmlWriter);
                                        } if (localEstimatedWorkVarianceTracker){
                                            if (localEstimatedWorkVariance==null){
                                                 throw new org.apache.axis2.databinding.ADBException("estimatedWorkVariance cannot be null!!");
                                            }
                                           localEstimatedWorkVariance.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedWorkVariance"),
                                               xmlWriter);
                                        } if (localEstimatedWorkVariancePercentTracker){
                                            if (localEstimatedWorkVariancePercent==null){
                                                 throw new org.apache.axis2.databinding.ADBException("estimatedWorkVariancePercent cannot be null!!");
                                            }
                                           localEstimatedWorkVariancePercent.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedWorkVariancePercent"),
                                               xmlWriter);
                                        } if (localExternalIdTracker){
                                            if (localExternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                            }
                                           localExternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId"),
                                               xmlWriter);
                                        } if (localExternalIdStringTracker){
                                            if (localExternalIdString==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                            }
                                           localExternalIdString.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString"),
                                               xmlWriter);
                                        } if (localFinishByDateTracker){
                                            if (localFinishByDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("finishByDate cannot be null!!");
                                            }
                                           localFinishByDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","finishByDate"),
                                               xmlWriter);
                                        } if (localIdTracker){
                                            if (localId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("id cannot be null!!");
                                            }
                                           localId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","id"),
                                               xmlWriter);
                                        } if (localInternalIdTracker){
                                            if (localInternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                            }
                                           localInternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                               xmlWriter);
                                        } if (localInternalIdNumberTracker){
                                            if (localInternalIdNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                            }
                                           localInternalIdNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber"),
                                               xmlWriter);
                                        } if (localIsMilestoneTracker){
                                            if (localIsMilestone==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isMilestone cannot be null!!");
                                            }
                                           localIsMilestone.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isMilestone"),
                                               xmlWriter);
                                        } if (localIsSummaryTaskTracker){
                                            if (localIsSummaryTask==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isSummaryTask cannot be null!!");
                                            }
                                           localIsSummaryTask.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isSummaryTask"),
                                               xmlWriter);
                                        } if (localLastModifiedDateTracker){
                                            if (localLastModifiedDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                            }
                                           localLastModifiedDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate"),
                                               xmlWriter);
                                        } if (localNonBillableTaskTracker){
                                            if (localNonBillableTask==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nonBillableTask cannot be null!!");
                                            }
                                           localNonBillableTask.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nonBillableTask"),
                                               xmlWriter);
                                        } if (localOwnerTracker){
                                            if (localOwner==null){
                                                 throw new org.apache.axis2.databinding.ADBException("owner cannot be null!!");
                                            }
                                           localOwner.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","owner"),
                                               xmlWriter);
                                        } if (localParentTracker){
                                            if (localParent==null){
                                                 throw new org.apache.axis2.databinding.ADBException("parent cannot be null!!");
                                            }
                                           localParent.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","parent"),
                                               xmlWriter);
                                        } if (localPercentWorkCompleteTracker){
                                            if (localPercentWorkComplete==null){
                                                 throw new org.apache.axis2.databinding.ADBException("percentWorkComplete cannot be null!!");
                                            }
                                           localPercentWorkComplete.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","percentWorkComplete"),
                                               xmlWriter);
                                        } if (localPredecessorTracker){
                                            if (localPredecessor==null){
                                                 throw new org.apache.axis2.databinding.ADBException("predecessor cannot be null!!");
                                            }
                                           localPredecessor.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","predecessor"),
                                               xmlWriter);
                                        } if (localPredecessorsTracker){
                                            if (localPredecessors==null){
                                                 throw new org.apache.axis2.databinding.ADBException("predecessors cannot be null!!");
                                            }
                                           localPredecessors.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","predecessors"),
                                               xmlWriter);
                                        } if (localPriorityTracker){
                                            if (localPriority==null){
                                                 throw new org.apache.axis2.databinding.ADBException("priority cannot be null!!");
                                            }
                                           localPriority.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","priority"),
                                               xmlWriter);
                                        } if (localRemainingWorkTracker){
                                            if (localRemainingWork==null){
                                                 throw new org.apache.axis2.databinding.ADBException("remainingWork cannot be null!!");
                                            }
                                           localRemainingWork.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","remainingWork"),
                                               xmlWriter);
                                        } if (localStartDateTracker){
                                            if (localStartDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                            }
                                           localStartDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDate"),
                                               xmlWriter);
                                        } if (localStartDateBaselineTracker){
                                            if (localStartDateBaseline==null){
                                                 throw new org.apache.axis2.databinding.ADBException("startDateBaseline cannot be null!!");
                                            }
                                           localStartDateBaseline.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDateBaseline"),
                                               xmlWriter);
                                        } if (localStartDateVarianceTracker){
                                            if (localStartDateVariance==null){
                                                 throw new org.apache.axis2.databinding.ADBException("startDateVariance cannot be null!!");
                                            }
                                           localStartDateVariance.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDateVariance"),
                                               xmlWriter);
                                        } if (localStatusTracker){
                                            if (localStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                            }
                                           localStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","status"),
                                               xmlWriter);
                                        } if (localSuccessorTracker){
                                            if (localSuccessor==null){
                                                 throw new org.apache.axis2.databinding.ADBException("successor cannot be null!!");
                                            }
                                           localSuccessor.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","successor"),
                                               xmlWriter);
                                        } if (localTitleTracker){
                                            if (localTitle==null){
                                                 throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                            }
                                           localTitle.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","title"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ProjectTaskSearchBasic"));
                 if (localActualWorkTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "actualWork"));
                            
                            
                                    if (localActualWork==null){
                                         throw new org.apache.axis2.databinding.ADBException("actualWork cannot be null!!");
                                    }
                                    elementList.add(localActualWork);
                                } if (localAssigneeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "assignee"));
                            
                            
                                    if (localAssignee==null){
                                         throw new org.apache.axis2.databinding.ADBException("assignee cannot be null!!");
                                    }
                                    elementList.add(localAssignee);
                                } if (localCompanyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "company"));
                            
                            
                                    if (localCompany==null){
                                         throw new org.apache.axis2.databinding.ADBException("company cannot be null!!");
                                    }
                                    elementList.add(localCompany);
                                } if (localConstraintTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "constraintType"));
                            
                            
                                    if (localConstraintType==null){
                                         throw new org.apache.axis2.databinding.ADBException("constraintType cannot be null!!");
                                    }
                                    elementList.add(localConstraintType);
                                } if (localContactTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "contact"));
                            
                            
                                    if (localContact==null){
                                         throw new org.apache.axis2.databinding.ADBException("contact cannot be null!!");
                                    }
                                    elementList.add(localContact);
                                } if (localCostTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "cost"));
                            
                            
                                    if (localCost==null){
                                         throw new org.apache.axis2.databinding.ADBException("cost cannot be null!!");
                                    }
                                    elementList.add(localCost);
                                } if (localCostBaseTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "costBase"));
                            
                            
                                    if (localCostBase==null){
                                         throw new org.apache.axis2.databinding.ADBException("costBase cannot be null!!");
                                    }
                                    elementList.add(localCostBase);
                                } if (localCostBaseBaselineTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "costBaseBaseline"));
                            
                            
                                    if (localCostBaseBaseline==null){
                                         throw new org.apache.axis2.databinding.ADBException("costBaseBaseline cannot be null!!");
                                    }
                                    elementList.add(localCostBaseBaseline);
                                } if (localCostBaselineTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "costBaseline"));
                            
                            
                                    if (localCostBaseline==null){
                                         throw new org.apache.axis2.databinding.ADBException("costBaseline cannot be null!!");
                                    }
                                    elementList.add(localCostBaseline);
                                } if (localCostBaseVarianceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "costBaseVariance"));
                            
                            
                                    if (localCostBaseVariance==null){
                                         throw new org.apache.axis2.databinding.ADBException("costBaseVariance cannot be null!!");
                                    }
                                    elementList.add(localCostBaseVariance);
                                } if (localCostVarianceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "costVariance"));
                            
                            
                                    if (localCostVariance==null){
                                         throw new org.apache.axis2.databinding.ADBException("costVariance cannot be null!!");
                                    }
                                    elementList.add(localCostVariance);
                                } if (localCostVariancePercentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "costVariancePercent"));
                            
                            
                                    if (localCostVariancePercent==null){
                                         throw new org.apache.axis2.databinding.ADBException("costVariancePercent cannot be null!!");
                                    }
                                    elementList.add(localCostVariancePercent);
                                } if (localCreatedDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "createdDate"));
                            
                            
                                    if (localCreatedDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                    }
                                    elementList.add(localCreatedDate);
                                } if (localEndDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "endDate"));
                            
                            
                                    if (localEndDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                    }
                                    elementList.add(localEndDate);
                                } if (localEndDateBaselineTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "endDateBaseline"));
                            
                            
                                    if (localEndDateBaseline==null){
                                         throw new org.apache.axis2.databinding.ADBException("endDateBaseline cannot be null!!");
                                    }
                                    elementList.add(localEndDateBaseline);
                                } if (localEndDateVarianceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "endDateVariance"));
                            
                            
                                    if (localEndDateVariance==null){
                                         throw new org.apache.axis2.databinding.ADBException("endDateVariance cannot be null!!");
                                    }
                                    elementList.add(localEndDateVariance);
                                } if (localEstimatedWorkTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "estimatedWork"));
                            
                            
                                    if (localEstimatedWork==null){
                                         throw new org.apache.axis2.databinding.ADBException("estimatedWork cannot be null!!");
                                    }
                                    elementList.add(localEstimatedWork);
                                } if (localEstimatedWorkBaselineTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "estimatedWorkBaseline"));
                            
                            
                                    if (localEstimatedWorkBaseline==null){
                                         throw new org.apache.axis2.databinding.ADBException("estimatedWorkBaseline cannot be null!!");
                                    }
                                    elementList.add(localEstimatedWorkBaseline);
                                } if (localEstimatedWorkVarianceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "estimatedWorkVariance"));
                            
                            
                                    if (localEstimatedWorkVariance==null){
                                         throw new org.apache.axis2.databinding.ADBException("estimatedWorkVariance cannot be null!!");
                                    }
                                    elementList.add(localEstimatedWorkVariance);
                                } if (localEstimatedWorkVariancePercentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "estimatedWorkVariancePercent"));
                            
                            
                                    if (localEstimatedWorkVariancePercent==null){
                                         throw new org.apache.axis2.databinding.ADBException("estimatedWorkVariancePercent cannot be null!!");
                                    }
                                    elementList.add(localEstimatedWorkVariancePercent);
                                } if (localExternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalId"));
                            
                            
                                    if (localExternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                    }
                                    elementList.add(localExternalId);
                                } if (localExternalIdStringTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalIdString"));
                            
                            
                                    if (localExternalIdString==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                    }
                                    elementList.add(localExternalIdString);
                                } if (localFinishByDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "finishByDate"));
                            
                            
                                    if (localFinishByDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("finishByDate cannot be null!!");
                                    }
                                    elementList.add(localFinishByDate);
                                } if (localIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "id"));
                            
                            
                                    if (localId==null){
                                         throw new org.apache.axis2.databinding.ADBException("id cannot be null!!");
                                    }
                                    elementList.add(localId);
                                } if (localInternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalId"));
                            
                            
                                    if (localInternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    }
                                    elementList.add(localInternalId);
                                } if (localInternalIdNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalIdNumber"));
                            
                            
                                    if (localInternalIdNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                    }
                                    elementList.add(localInternalIdNumber);
                                } if (localIsMilestoneTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isMilestone"));
                            
                            
                                    if (localIsMilestone==null){
                                         throw new org.apache.axis2.databinding.ADBException("isMilestone cannot be null!!");
                                    }
                                    elementList.add(localIsMilestone);
                                } if (localIsSummaryTaskTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isSummaryTask"));
                            
                            
                                    if (localIsSummaryTask==null){
                                         throw new org.apache.axis2.databinding.ADBException("isSummaryTask cannot be null!!");
                                    }
                                    elementList.add(localIsSummaryTask);
                                } if (localLastModifiedDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                            
                            
                                    if (localLastModifiedDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                    }
                                    elementList.add(localLastModifiedDate);
                                } if (localNonBillableTaskTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "nonBillableTask"));
                            
                            
                                    if (localNonBillableTask==null){
                                         throw new org.apache.axis2.databinding.ADBException("nonBillableTask cannot be null!!");
                                    }
                                    elementList.add(localNonBillableTask);
                                } if (localOwnerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "owner"));
                            
                            
                                    if (localOwner==null){
                                         throw new org.apache.axis2.databinding.ADBException("owner cannot be null!!");
                                    }
                                    elementList.add(localOwner);
                                } if (localParentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "parent"));
                            
                            
                                    if (localParent==null){
                                         throw new org.apache.axis2.databinding.ADBException("parent cannot be null!!");
                                    }
                                    elementList.add(localParent);
                                } if (localPercentWorkCompleteTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "percentWorkComplete"));
                            
                            
                                    if (localPercentWorkComplete==null){
                                         throw new org.apache.axis2.databinding.ADBException("percentWorkComplete cannot be null!!");
                                    }
                                    elementList.add(localPercentWorkComplete);
                                } if (localPredecessorTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "predecessor"));
                            
                            
                                    if (localPredecessor==null){
                                         throw new org.apache.axis2.databinding.ADBException("predecessor cannot be null!!");
                                    }
                                    elementList.add(localPredecessor);
                                } if (localPredecessorsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "predecessors"));
                            
                            
                                    if (localPredecessors==null){
                                         throw new org.apache.axis2.databinding.ADBException("predecessors cannot be null!!");
                                    }
                                    elementList.add(localPredecessors);
                                } if (localPriorityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "priority"));
                            
                            
                                    if (localPriority==null){
                                         throw new org.apache.axis2.databinding.ADBException("priority cannot be null!!");
                                    }
                                    elementList.add(localPriority);
                                } if (localRemainingWorkTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "remainingWork"));
                            
                            
                                    if (localRemainingWork==null){
                                         throw new org.apache.axis2.databinding.ADBException("remainingWork cannot be null!!");
                                    }
                                    elementList.add(localRemainingWork);
                                } if (localStartDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "startDate"));
                            
                            
                                    if (localStartDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                    }
                                    elementList.add(localStartDate);
                                } if (localStartDateBaselineTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "startDateBaseline"));
                            
                            
                                    if (localStartDateBaseline==null){
                                         throw new org.apache.axis2.databinding.ADBException("startDateBaseline cannot be null!!");
                                    }
                                    elementList.add(localStartDateBaseline);
                                } if (localStartDateVarianceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "startDateVariance"));
                            
                            
                                    if (localStartDateVariance==null){
                                         throw new org.apache.axis2.databinding.ADBException("startDateVariance cannot be null!!");
                                    }
                                    elementList.add(localStartDateVariance);
                                } if (localStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "status"));
                            
                            
                                    if (localStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                    }
                                    elementList.add(localStatus);
                                } if (localSuccessorTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "successor"));
                            
                            
                                    if (localSuccessor==null){
                                         throw new org.apache.axis2.databinding.ADBException("successor cannot be null!!");
                                    }
                                    elementList.add(localSuccessor);
                                } if (localTitleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "title"));
                            
                            
                                    if (localTitle==null){
                                         throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                    }
                                    elementList.add(localTitle);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ProjectTaskSearchBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ProjectTaskSearchBasic object =
                new ProjectTaskSearchBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ProjectTaskSearchBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ProjectTaskSearchBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","actualWork").equals(reader.getName())){
                                
                                                object.setActualWork(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","assignee").equals(reader.getName())){
                                
                                                object.setAssignee(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","company").equals(reader.getName())){
                                
                                                object.setCompany(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","constraintType").equals(reader.getName())){
                                
                                                object.setConstraintType(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","contact").equals(reader.getName())){
                                
                                                object.setContact(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","cost").equals(reader.getName())){
                                
                                                object.setCost(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","costBase").equals(reader.getName())){
                                
                                                object.setCostBase(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","costBaseBaseline").equals(reader.getName())){
                                
                                                object.setCostBaseBaseline(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","costBaseline").equals(reader.getName())){
                                
                                                object.setCostBaseline(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","costBaseVariance").equals(reader.getName())){
                                
                                                object.setCostBaseVariance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","costVariance").equals(reader.getName())){
                                
                                                object.setCostVariance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","costVariancePercent").equals(reader.getName())){
                                
                                                object.setCostVariancePercent(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","createdDate").equals(reader.getName())){
                                
                                                object.setCreatedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endDate").equals(reader.getName())){
                                
                                                object.setEndDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endDateBaseline").equals(reader.getName())){
                                
                                                object.setEndDateBaseline(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endDateVariance").equals(reader.getName())){
                                
                                                object.setEndDateVariance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedWork").equals(reader.getName())){
                                
                                                object.setEstimatedWork(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedWorkBaseline").equals(reader.getName())){
                                
                                                object.setEstimatedWorkBaseline(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedWorkVariance").equals(reader.getName())){
                                
                                                object.setEstimatedWorkVariance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedWorkVariancePercent").equals(reader.getName())){
                                
                                                object.setEstimatedWorkVariancePercent(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                
                                                object.setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString").equals(reader.getName())){
                                
                                                object.setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","finishByDate").equals(reader.getName())){
                                
                                                object.setFinishByDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","id").equals(reader.getName())){
                                
                                                object.setId(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                                object.setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber").equals(reader.getName())){
                                
                                                object.setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isMilestone").equals(reader.getName())){
                                
                                                object.setIsMilestone(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isSummaryTask").equals(reader.getName())){
                                
                                                object.setIsSummaryTask(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                                object.setLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nonBillableTask").equals(reader.getName())){
                                
                                                object.setNonBillableTask(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","owner").equals(reader.getName())){
                                
                                                object.setOwner(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","parent").equals(reader.getName())){
                                
                                                object.setParent(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","percentWorkComplete").equals(reader.getName())){
                                
                                                object.setPercentWorkComplete(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","predecessor").equals(reader.getName())){
                                
                                                object.setPredecessor(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","predecessors").equals(reader.getName())){
                                
                                                object.setPredecessors(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","priority").equals(reader.getName())){
                                
                                                object.setPriority(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","remainingWork").equals(reader.getName())){
                                
                                                object.setRemainingWork(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDate").equals(reader.getName())){
                                
                                                object.setStartDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDateBaseline").equals(reader.getName())){
                                
                                                object.setStartDateBaseline(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDateVariance").equals(reader.getName())){
                                
                                                object.setStartDateVariance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","status").equals(reader.getName())){
                                
                                                object.setStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","successor").equals(reader.getName())){
                                
                                                object.setSuccessor(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","title").equals(reader.getName())){
                                
                                                object.setTitle(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    