
/**
 * BillingAccountSearchRowBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  BillingAccountSearchRowBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class BillingAccountSearchRowBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRowBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = BillingAccountSearchRowBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for BillingSchedule
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localBillingSchedule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillingScheduleTracker = false ;

                           public boolean isBillingScheduleSpecified(){
                               return localBillingScheduleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getBillingSchedule(){
                               return localBillingSchedule;
                           }

                           
                        


                               
                              /**
                               * validate the array for BillingSchedule
                               */
                              protected void validateBillingSchedule(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param BillingSchedule
                              */
                              public void setBillingSchedule(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateBillingSchedule(param);

                               localBillingScheduleTracker = param != null;
                                      
                                      this.localBillingSchedule=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addBillingSchedule(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localBillingSchedule == null){
                                   localBillingSchedule = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localBillingScheduleTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localBillingSchedule);
                               list.add(param);
                               this.localBillingSchedule =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for CashSaleForm
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localCashSaleForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCashSaleFormTracker = false ;

                           public boolean isCashSaleFormSpecified(){
                               return localCashSaleFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getCashSaleForm(){
                               return localCashSaleForm;
                           }

                           
                        


                               
                              /**
                               * validate the array for CashSaleForm
                               */
                              protected void validateCashSaleForm(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param CashSaleForm
                              */
                              public void setCashSaleForm(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateCashSaleForm(param);

                               localCashSaleFormTracker = param != null;
                                      
                                      this.localCashSaleForm=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addCashSaleForm(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localCashSaleForm == null){
                                   localCashSaleForm = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localCashSaleFormTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCashSaleForm);
                               list.add(param);
                               this.localCashSaleForm =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for _class
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] get_class(){
                               return local_class;
                           }

                           
                        


                               
                              /**
                               * validate the array for _class
                               */
                              protected void validate_class(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param _class
                              */
                              public void set_class(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validate_class(param);

                               local_classTracker = param != null;
                                      
                                      this.local_class=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void add_class(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (local_class == null){
                                   local_class = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                local_classTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(local_class);
                               list.add(param);
                               this.local_class =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for Currency
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrencyTracker = false ;

                           public boolean isCurrencySpecified(){
                               return localCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getCurrency(){
                               return localCurrency;
                           }

                           
                        


                               
                              /**
                               * validate the array for Currency
                               */
                              protected void validateCurrency(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Currency
                              */
                              public void setCurrency(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateCurrency(param);

                               localCurrencyTracker = param != null;
                                      
                                      this.localCurrency=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addCurrency(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localCurrency == null){
                                   localCurrency = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localCurrencyTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCurrency);
                               list.add(param);
                               this.localCurrency =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for Customer
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localCustomer ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomerTracker = false ;

                           public boolean isCustomerSpecified(){
                               return localCustomerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getCustomer(){
                               return localCustomer;
                           }

                           
                        


                               
                              /**
                               * validate the array for Customer
                               */
                              protected void validateCustomer(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Customer
                              */
                              public void setCustomer(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateCustomer(param);

                               localCustomerTracker = param != null;
                                      
                                      this.localCustomer=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addCustomer(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localCustomer == null){
                                   localCustomer = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localCustomerTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCustomer);
                               list.add(param);
                               this.localCustomer =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for CustomerDefault
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localCustomerDefault ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomerDefaultTracker = false ;

                           public boolean isCustomerDefaultSpecified(){
                               return localCustomerDefaultTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getCustomerDefault(){
                               return localCustomerDefault;
                           }

                           
                        


                               
                              /**
                               * validate the array for CustomerDefault
                               */
                              protected void validateCustomerDefault(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param CustomerDefault
                              */
                              public void setCustomerDefault(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateCustomerDefault(param);

                               localCustomerDefaultTracker = param != null;
                                      
                                      this.localCustomerDefault=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addCustomerDefault(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localCustomerDefault == null){
                                   localCustomerDefault = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localCustomerDefaultTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCustomerDefault);
                               list.add(param);
                               this.localCustomerDefault =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for DateCreated
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localDateCreated ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDateCreatedTracker = false ;

                           public boolean isDateCreatedSpecified(){
                               return localDateCreatedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getDateCreated(){
                               return localDateCreated;
                           }

                           
                        


                               
                              /**
                               * validate the array for DateCreated
                               */
                              protected void validateDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param DateCreated
                              */
                              public void setDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateDateCreated(param);

                               localDateCreatedTracker = param != null;
                                      
                                      this.localDateCreated=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localDateCreated == null){
                                   localDateCreated = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localDateCreatedTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localDateCreated);
                               list.add(param);
                               this.localDateCreated =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for Department
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getDepartment(){
                               return localDepartment;
                           }

                           
                        


                               
                              /**
                               * validate the array for Department
                               */
                              protected void validateDepartment(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Department
                              */
                              public void setDepartment(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateDepartment(param);

                               localDepartmentTracker = param != null;
                                      
                                      this.localDepartment=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addDepartment(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localDepartment == null){
                                   localDepartment = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localDepartmentTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localDepartment);
                               list.add(param);
                               this.localDepartment =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for ExternalId
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localExternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdTracker = false ;

                           public boolean isExternalIdSpecified(){
                               return localExternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getExternalId(){
                               return localExternalId;
                           }

                           
                        


                               
                              /**
                               * validate the array for ExternalId
                               */
                              protected void validateExternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param ExternalId
                              */
                              public void setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateExternalId(param);

                               localExternalIdTracker = param != null;
                                      
                                      this.localExternalId=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addExternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localExternalId == null){
                                   localExternalId = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localExternalIdTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localExternalId);
                               list.add(param);
                               this.localExternalId =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for Frequency
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] localFrequency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFrequencyTracker = false ;

                           public boolean isFrequencySpecified(){
                               return localFrequencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] getFrequency(){
                               return localFrequency;
                           }

                           
                        


                               
                              /**
                               * validate the array for Frequency
                               */
                              protected void validateFrequency(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Frequency
                              */
                              public void setFrequency(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                              
                                   validateFrequency(param);

                               localFrequencyTracker = param != null;
                                      
                                      this.localFrequency=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField
                             */
                             public void addFrequency(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField param){
                                   if (localFrequency == null){
                                   localFrequency = new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localFrequencyTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localFrequency);
                               list.add(param);
                               this.localFrequency =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[list.size()]);

                             }
                             

                        /**
                        * field for IdNumber
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localIdNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIdNumberTracker = false ;

                           public boolean isIdNumberSpecified(){
                               return localIdNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getIdNumber(){
                               return localIdNumber;
                           }

                           
                        


                               
                              /**
                               * validate the array for IdNumber
                               */
                              protected void validateIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param IdNumber
                              */
                              public void setIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateIdNumber(param);

                               localIdNumberTracker = param != null;
                                      
                                      this.localIdNumber=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localIdNumber == null){
                                   localIdNumber = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localIdNumberTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localIdNumber);
                               list.add(param);
                               this.localIdNumber =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for Inactive
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localInactive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInactiveTracker = false ;

                           public boolean isInactiveSpecified(){
                               return localInactiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getInactive(){
                               return localInactive;
                           }

                           
                        


                               
                              /**
                               * validate the array for Inactive
                               */
                              protected void validateInactive(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Inactive
                              */
                              public void setInactive(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateInactive(param);

                               localInactiveTracker = param != null;
                                      
                                      this.localInactive=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addInactive(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localInactive == null){
                                   localInactive = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localInactiveTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localInactive);
                               list.add(param);
                               this.localInactive =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for InternalId
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getInternalId(){
                               return localInternalId;
                           }

                           
                        


                               
                              /**
                               * validate the array for InternalId
                               */
                              protected void validateInternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param InternalId
                              */
                              public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateInternalId(param);

                               localInternalIdTracker = param != null;
                                      
                                      this.localInternalId=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addInternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localInternalId == null){
                                   localInternalId = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localInternalIdTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localInternalId);
                               list.add(param);
                               this.localInternalId =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for InvoiceForm
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localInvoiceForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInvoiceFormTracker = false ;

                           public boolean isInvoiceFormSpecified(){
                               return localInvoiceFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getInvoiceForm(){
                               return localInvoiceForm;
                           }

                           
                        


                               
                              /**
                               * validate the array for InvoiceForm
                               */
                              protected void validateInvoiceForm(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param InvoiceForm
                              */
                              public void setInvoiceForm(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateInvoiceForm(param);

                               localInvoiceFormTracker = param != null;
                                      
                                      this.localInvoiceForm=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addInvoiceForm(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localInvoiceForm == null){
                                   localInvoiceForm = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localInvoiceFormTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localInvoiceForm);
                               list.add(param);
                               this.localInvoiceForm =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for LastBillCycleDate
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localLastBillCycleDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastBillCycleDateTracker = false ;

                           public boolean isLastBillCycleDateSpecified(){
                               return localLastBillCycleDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getLastBillCycleDate(){
                               return localLastBillCycleDate;
                           }

                           
                        


                               
                              /**
                               * validate the array for LastBillCycleDate
                               */
                              protected void validateLastBillCycleDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param LastBillCycleDate
                              */
                              public void setLastBillCycleDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateLastBillCycleDate(param);

                               localLastBillCycleDateTracker = param != null;
                                      
                                      this.localLastBillCycleDate=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addLastBillCycleDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localLastBillCycleDate == null){
                                   localLastBillCycleDate = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localLastBillCycleDateTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localLastBillCycleDate);
                               list.add(param);
                               this.localLastBillCycleDate =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for LastBillDate
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localLastBillDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastBillDateTracker = false ;

                           public boolean isLastBillDateSpecified(){
                               return localLastBillDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getLastBillDate(){
                               return localLastBillDate;
                           }

                           
                        


                               
                              /**
                               * validate the array for LastBillDate
                               */
                              protected void validateLastBillDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param LastBillDate
                              */
                              public void setLastBillDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateLastBillDate(param);

                               localLastBillDateTracker = param != null;
                                      
                                      this.localLastBillDate=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addLastBillDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localLastBillDate == null){
                                   localLastBillDate = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localLastBillDateTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localLastBillDate);
                               list.add(param);
                               this.localLastBillDate =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for Location
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getLocation(){
                               return localLocation;
                           }

                           
                        


                               
                              /**
                               * validate the array for Location
                               */
                              protected void validateLocation(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Location
                              */
                              public void setLocation(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateLocation(param);

                               localLocationTracker = param != null;
                                      
                                      this.localLocation=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addLocation(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localLocation == null){
                                   localLocation = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localLocationTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localLocation);
                               list.add(param);
                               this.localLocation =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for Memo
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localMemo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMemoTracker = false ;

                           public boolean isMemoSpecified(){
                               return localMemoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getMemo(){
                               return localMemo;
                           }

                           
                        


                               
                              /**
                               * validate the array for Memo
                               */
                              protected void validateMemo(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Memo
                              */
                              public void setMemo(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateMemo(param);

                               localMemoTracker = param != null;
                                      
                                      this.localMemo=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addMemo(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localMemo == null){
                                   localMemo = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localMemoTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localMemo);
                               list.add(param);
                               this.localMemo =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for Name
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNameTracker = false ;

                           public boolean isNameSpecified(){
                               return localNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getName(){
                               return localName;
                           }

                           
                        


                               
                              /**
                               * validate the array for Name
                               */
                              protected void validateName(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Name
                              */
                              public void setName(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateName(param);

                               localNameTracker = param != null;
                                      
                                      this.localName=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addName(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localName == null){
                                   localName = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localNameTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localName);
                               list.add(param);
                               this.localName =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for NextBillCycleDate
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localNextBillCycleDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNextBillCycleDateTracker = false ;

                           public boolean isNextBillCycleDateSpecified(){
                               return localNextBillCycleDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getNextBillCycleDate(){
                               return localNextBillCycleDate;
                           }

                           
                        


                               
                              /**
                               * validate the array for NextBillCycleDate
                               */
                              protected void validateNextBillCycleDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param NextBillCycleDate
                              */
                              public void setNextBillCycleDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateNextBillCycleDate(param);

                               localNextBillCycleDateTracker = param != null;
                                      
                                      this.localNextBillCycleDate=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addNextBillCycleDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localNextBillCycleDate == null){
                                   localNextBillCycleDate = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localNextBillCycleDateTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localNextBillCycleDate);
                               list.add(param);
                               this.localNextBillCycleDate =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for StartDate
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateTracker = false ;

                           public boolean isStartDateSpecified(){
                               return localStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getStartDate(){
                               return localStartDate;
                           }

                           
                        


                               
                              /**
                               * validate the array for StartDate
                               */
                              protected void validateStartDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param StartDate
                              */
                              public void setStartDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateStartDate(param);

                               localStartDateTracker = param != null;
                                      
                                      this.localStartDate=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addStartDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localStartDate == null){
                                   localStartDate = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localStartDateTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localStartDate);
                               list.add(param);
                               this.localStartDate =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for Subsidiary
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        


                               
                              /**
                               * validate the array for Subsidiary
                               */
                              protected void validateSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Subsidiary
                              */
                              public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateSubsidiary(param);

                               localSubsidiaryTracker = param != null;
                                      
                                      this.localSubsidiary=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localSubsidiary == null){
                                   localSubsidiary = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localSubsidiaryTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localSubsidiary);
                               list.add(param);
                               this.localSubsidiary =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":BillingAccountSearchRowBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "BillingAccountSearchRowBasic",
                           xmlWriter);
                   }

                if (localBillingScheduleTracker){
                                       if (localBillingSchedule!=null){
                                            for (int i = 0;i < localBillingSchedule.length;i++){
                                                if (localBillingSchedule[i] != null){
                                                 localBillingSchedule[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billingSchedule"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("billingSchedule cannot be null!!");
                                        
                                    }
                                 } if (localCashSaleFormTracker){
                                       if (localCashSaleForm!=null){
                                            for (int i = 0;i < localCashSaleForm.length;i++){
                                                if (localCashSaleForm[i] != null){
                                                 localCashSaleForm[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","cashSaleForm"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("cashSaleForm cannot be null!!");
                                        
                                    }
                                 } if (local_classTracker){
                                       if (local_class!=null){
                                            for (int i = 0;i < local_class.length;i++){
                                                if (local_class[i] != null){
                                                 local_class[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","class"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                        
                                    }
                                 } if (localCurrencyTracker){
                                       if (localCurrency!=null){
                                            for (int i = 0;i < localCurrency.length;i++){
                                                if (localCurrency[i] != null){
                                                 localCurrency[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                        
                                    }
                                 } if (localCustomerTracker){
                                       if (localCustomer!=null){
                                            for (int i = 0;i < localCustomer.length;i++){
                                                if (localCustomer[i] != null){
                                                 localCustomer[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customer"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("customer cannot be null!!");
                                        
                                    }
                                 } if (localCustomerDefaultTracker){
                                       if (localCustomerDefault!=null){
                                            for (int i = 0;i < localCustomerDefault.length;i++){
                                                if (localCustomerDefault[i] != null){
                                                 localCustomerDefault[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customerDefault"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("customerDefault cannot be null!!");
                                        
                                    }
                                 } if (localDateCreatedTracker){
                                       if (localDateCreated!=null){
                                            for (int i = 0;i < localDateCreated.length;i++){
                                                if (localDateCreated[i] != null){
                                                 localDateCreated[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                        
                                    }
                                 } if (localDepartmentTracker){
                                       if (localDepartment!=null){
                                            for (int i = 0;i < localDepartment.length;i++){
                                                if (localDepartment[i] != null){
                                                 localDepartment[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","department"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                        
                                    }
                                 } if (localExternalIdTracker){
                                       if (localExternalId!=null){
                                            for (int i = 0;i < localExternalId.length;i++){
                                                if (localExternalId[i] != null){
                                                 localExternalId[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                        
                                    }
                                 } if (localFrequencyTracker){
                                       if (localFrequency!=null){
                                            for (int i = 0;i < localFrequency.length;i++){
                                                if (localFrequency[i] != null){
                                                 localFrequency[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","frequency"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("frequency cannot be null!!");
                                        
                                    }
                                 } if (localIdNumberTracker){
                                       if (localIdNumber!=null){
                                            for (int i = 0;i < localIdNumber.length;i++){
                                                if (localIdNumber[i] != null){
                                                 localIdNumber[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","idNumber"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("idNumber cannot be null!!");
                                        
                                    }
                                 } if (localInactiveTracker){
                                       if (localInactive!=null){
                                            for (int i = 0;i < localInactive.length;i++){
                                                if (localInactive[i] != null){
                                                 localInactive[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","inactive"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("inactive cannot be null!!");
                                        
                                    }
                                 } if (localInternalIdTracker){
                                       if (localInternalId!=null){
                                            for (int i = 0;i < localInternalId.length;i++){
                                                if (localInternalId[i] != null){
                                                 localInternalId[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                        
                                    }
                                 } if (localInvoiceFormTracker){
                                       if (localInvoiceForm!=null){
                                            for (int i = 0;i < localInvoiceForm.length;i++){
                                                if (localInvoiceForm[i] != null){
                                                 localInvoiceForm[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","invoiceForm"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("invoiceForm cannot be null!!");
                                        
                                    }
                                 } if (localLastBillCycleDateTracker){
                                       if (localLastBillCycleDate!=null){
                                            for (int i = 0;i < localLastBillCycleDate.length;i++){
                                                if (localLastBillCycleDate[i] != null){
                                                 localLastBillCycleDate[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastBillCycleDate"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("lastBillCycleDate cannot be null!!");
                                        
                                    }
                                 } if (localLastBillDateTracker){
                                       if (localLastBillDate!=null){
                                            for (int i = 0;i < localLastBillDate.length;i++){
                                                if (localLastBillDate[i] != null){
                                                 localLastBillDate[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastBillDate"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("lastBillDate cannot be null!!");
                                        
                                    }
                                 } if (localLocationTracker){
                                       if (localLocation!=null){
                                            for (int i = 0;i < localLocation.length;i++){
                                                if (localLocation[i] != null){
                                                 localLocation[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","location"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                        
                                    }
                                 } if (localMemoTracker){
                                       if (localMemo!=null){
                                            for (int i = 0;i < localMemo.length;i++){
                                                if (localMemo[i] != null){
                                                 localMemo[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","memo"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                        
                                    }
                                 } if (localNameTracker){
                                       if (localName!=null){
                                            for (int i = 0;i < localName.length;i++){
                                                if (localName[i] != null){
                                                 localName[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","name"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                        
                                    }
                                 } if (localNextBillCycleDateTracker){
                                       if (localNextBillCycleDate!=null){
                                            for (int i = 0;i < localNextBillCycleDate.length;i++){
                                                if (localNextBillCycleDate[i] != null){
                                                 localNextBillCycleDate[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nextBillCycleDate"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("nextBillCycleDate cannot be null!!");
                                        
                                    }
                                 } if (localStartDateTracker){
                                       if (localStartDate!=null){
                                            for (int i = 0;i < localStartDate.length;i++){
                                                if (localStartDate[i] != null){
                                                 localStartDate[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDate"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                        
                                    }
                                 } if (localSubsidiaryTracker){
                                       if (localSubsidiary!=null){
                                            for (int i = 0;i < localSubsidiary.length;i++){
                                                if (localSubsidiary[i] != null){
                                                 localSubsidiary[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                        
                                    }
                                 } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","BillingAccountSearchRowBasic"));
                 if (localBillingScheduleTracker){
                             if (localBillingSchedule!=null) {
                                 for (int i = 0;i < localBillingSchedule.length;i++){

                                    if (localBillingSchedule[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "billingSchedule"));
                                         elementList.add(localBillingSchedule[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("billingSchedule cannot be null!!");
                                    
                             }

                        } if (localCashSaleFormTracker){
                             if (localCashSaleForm!=null) {
                                 for (int i = 0;i < localCashSaleForm.length;i++){

                                    if (localCashSaleForm[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "cashSaleForm"));
                                         elementList.add(localCashSaleForm[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("cashSaleForm cannot be null!!");
                                    
                             }

                        } if (local_classTracker){
                             if (local_class!=null) {
                                 for (int i = 0;i < local_class.length;i++){

                                    if (local_class[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "class"));
                                         elementList.add(local_class[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    
                             }

                        } if (localCurrencyTracker){
                             if (localCurrency!=null) {
                                 for (int i = 0;i < localCurrency.length;i++){

                                    if (localCurrency[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "currency"));
                                         elementList.add(localCurrency[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                    
                             }

                        } if (localCustomerTracker){
                             if (localCustomer!=null) {
                                 for (int i = 0;i < localCustomer.length;i++){

                                    if (localCustomer[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "customer"));
                                         elementList.add(localCustomer[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("customer cannot be null!!");
                                    
                             }

                        } if (localCustomerDefaultTracker){
                             if (localCustomerDefault!=null) {
                                 for (int i = 0;i < localCustomerDefault.length;i++){

                                    if (localCustomerDefault[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "customerDefault"));
                                         elementList.add(localCustomerDefault[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("customerDefault cannot be null!!");
                                    
                             }

                        } if (localDateCreatedTracker){
                             if (localDateCreated!=null) {
                                 for (int i = 0;i < localDateCreated.length;i++){

                                    if (localDateCreated[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "dateCreated"));
                                         elementList.add(localDateCreated[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                    
                             }

                        } if (localDepartmentTracker){
                             if (localDepartment!=null) {
                                 for (int i = 0;i < localDepartment.length;i++){

                                    if (localDepartment[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "department"));
                                         elementList.add(localDepartment[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    
                             }

                        } if (localExternalIdTracker){
                             if (localExternalId!=null) {
                                 for (int i = 0;i < localExternalId.length;i++){

                                    if (localExternalId[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "externalId"));
                                         elementList.add(localExternalId[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                    
                             }

                        } if (localFrequencyTracker){
                             if (localFrequency!=null) {
                                 for (int i = 0;i < localFrequency.length;i++){

                                    if (localFrequency[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "frequency"));
                                         elementList.add(localFrequency[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("frequency cannot be null!!");
                                    
                             }

                        } if (localIdNumberTracker){
                             if (localIdNumber!=null) {
                                 for (int i = 0;i < localIdNumber.length;i++){

                                    if (localIdNumber[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "idNumber"));
                                         elementList.add(localIdNumber[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("idNumber cannot be null!!");
                                    
                             }

                        } if (localInactiveTracker){
                             if (localInactive!=null) {
                                 for (int i = 0;i < localInactive.length;i++){

                                    if (localInactive[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "inactive"));
                                         elementList.add(localInactive[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("inactive cannot be null!!");
                                    
                             }

                        } if (localInternalIdTracker){
                             if (localInternalId!=null) {
                                 for (int i = 0;i < localInternalId.length;i++){

                                    if (localInternalId[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "internalId"));
                                         elementList.add(localInternalId[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    
                             }

                        } if (localInvoiceFormTracker){
                             if (localInvoiceForm!=null) {
                                 for (int i = 0;i < localInvoiceForm.length;i++){

                                    if (localInvoiceForm[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "invoiceForm"));
                                         elementList.add(localInvoiceForm[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("invoiceForm cannot be null!!");
                                    
                             }

                        } if (localLastBillCycleDateTracker){
                             if (localLastBillCycleDate!=null) {
                                 for (int i = 0;i < localLastBillCycleDate.length;i++){

                                    if (localLastBillCycleDate[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "lastBillCycleDate"));
                                         elementList.add(localLastBillCycleDate[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("lastBillCycleDate cannot be null!!");
                                    
                             }

                        } if (localLastBillDateTracker){
                             if (localLastBillDate!=null) {
                                 for (int i = 0;i < localLastBillDate.length;i++){

                                    if (localLastBillDate[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "lastBillDate"));
                                         elementList.add(localLastBillDate[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("lastBillDate cannot be null!!");
                                    
                             }

                        } if (localLocationTracker){
                             if (localLocation!=null) {
                                 for (int i = 0;i < localLocation.length;i++){

                                    if (localLocation[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "location"));
                                         elementList.add(localLocation[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    
                             }

                        } if (localMemoTracker){
                             if (localMemo!=null) {
                                 for (int i = 0;i < localMemo.length;i++){

                                    if (localMemo[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "memo"));
                                         elementList.add(localMemo[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                    
                             }

                        } if (localNameTracker){
                             if (localName!=null) {
                                 for (int i = 0;i < localName.length;i++){

                                    if (localName[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "name"));
                                         elementList.add(localName[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                    
                             }

                        } if (localNextBillCycleDateTracker){
                             if (localNextBillCycleDate!=null) {
                                 for (int i = 0;i < localNextBillCycleDate.length;i++){

                                    if (localNextBillCycleDate[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "nextBillCycleDate"));
                                         elementList.add(localNextBillCycleDate[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("nextBillCycleDate cannot be null!!");
                                    
                             }

                        } if (localStartDateTracker){
                             if (localStartDate!=null) {
                                 for (int i = 0;i < localStartDate.length;i++){

                                    if (localStartDate[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "startDate"));
                                         elementList.add(localStartDate[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                    
                             }

                        } if (localSubsidiaryTracker){
                             if (localSubsidiary!=null) {
                                 for (int i = 0;i < localSubsidiary.length;i++){

                                    if (localSubsidiary[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "subsidiary"));
                                         elementList.add(localSubsidiary[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    
                             }

                        } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static BillingAccountSearchRowBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            BillingAccountSearchRowBasic object =
                new BillingAccountSearchRowBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"BillingAccountSearchRowBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (BillingAccountSearchRowBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                        java.util.ArrayList list1 = new java.util.ArrayList();
                    
                        java.util.ArrayList list2 = new java.util.ArrayList();
                    
                        java.util.ArrayList list3 = new java.util.ArrayList();
                    
                        java.util.ArrayList list4 = new java.util.ArrayList();
                    
                        java.util.ArrayList list5 = new java.util.ArrayList();
                    
                        java.util.ArrayList list6 = new java.util.ArrayList();
                    
                        java.util.ArrayList list7 = new java.util.ArrayList();
                    
                        java.util.ArrayList list8 = new java.util.ArrayList();
                    
                        java.util.ArrayList list9 = new java.util.ArrayList();
                    
                        java.util.ArrayList list10 = new java.util.ArrayList();
                    
                        java.util.ArrayList list11 = new java.util.ArrayList();
                    
                        java.util.ArrayList list12 = new java.util.ArrayList();
                    
                        java.util.ArrayList list13 = new java.util.ArrayList();
                    
                        java.util.ArrayList list14 = new java.util.ArrayList();
                    
                        java.util.ArrayList list15 = new java.util.ArrayList();
                    
                        java.util.ArrayList list16 = new java.util.ArrayList();
                    
                        java.util.ArrayList list17 = new java.util.ArrayList();
                    
                        java.util.ArrayList list18 = new java.util.ArrayList();
                    
                        java.util.ArrayList list19 = new java.util.ArrayList();
                    
                        java.util.ArrayList list20 = new java.util.ArrayList();
                    
                        java.util.ArrayList list21 = new java.util.ArrayList();
                    
                        java.util.ArrayList list22 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billingSchedule").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list1.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone1 = false;
                                                        while(!loopDone1){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone1 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billingSchedule").equals(reader.getName())){
                                                                    list1.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone1 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setBillingSchedule((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list1));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","cashSaleForm").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list2.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone2 = false;
                                                        while(!loopDone2){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone2 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","cashSaleForm").equals(reader.getName())){
                                                                    list2.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone2 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCashSaleForm((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list2));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list3.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone3 = false;
                                                        while(!loopDone3){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone3 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","class").equals(reader.getName())){
                                                                    list3.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone3 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.set_class((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list3));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list4.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone4 = false;
                                                        while(!loopDone4){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone4 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency").equals(reader.getName())){
                                                                    list4.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone4 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCurrency((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list4));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customer").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list5.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone5 = false;
                                                        while(!loopDone5){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone5 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customer").equals(reader.getName())){
                                                                    list5.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone5 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCustomer((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list5));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customerDefault").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list6.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone6 = false;
                                                        while(!loopDone6){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone6 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customerDefault").equals(reader.getName())){
                                                                    list6.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone6 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCustomerDefault((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list6));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list7.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone7 = false;
                                                        while(!loopDone7){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone7 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated").equals(reader.getName())){
                                                                    list7.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone7 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setDateCreated((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list7));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list8.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone8 = false;
                                                        while(!loopDone8){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone8 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","department").equals(reader.getName())){
                                                                    list8.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone8 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setDepartment((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list8));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list9.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone9 = false;
                                                        while(!loopDone9){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone9 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                                                    list9.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone9 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setExternalId((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list9));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","frequency").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list10.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone10 = false;
                                                        while(!loopDone10){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone10 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","frequency").equals(reader.getName())){
                                                                    list10.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone10 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setFrequency((com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.class,
                                                                list10));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","idNumber").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list11.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone11 = false;
                                                        while(!loopDone11){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone11 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","idNumber").equals(reader.getName())){
                                                                    list11.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone11 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setIdNumber((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list11));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","inactive").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list12.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone12 = false;
                                                        while(!loopDone12){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone12 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","inactive").equals(reader.getName())){
                                                                    list12.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone12 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setInactive((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list12));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list13.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone13 = false;
                                                        while(!loopDone13){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone13 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                                                    list13.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone13 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setInternalId((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list13));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","invoiceForm").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list14.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone14 = false;
                                                        while(!loopDone14){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone14 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","invoiceForm").equals(reader.getName())){
                                                                    list14.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone14 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setInvoiceForm((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list14));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastBillCycleDate").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list15.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone15 = false;
                                                        while(!loopDone15){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone15 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastBillCycleDate").equals(reader.getName())){
                                                                    list15.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone15 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setLastBillCycleDate((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list15));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastBillDate").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list16.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone16 = false;
                                                        while(!loopDone16){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone16 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastBillDate").equals(reader.getName())){
                                                                    list16.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone16 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setLastBillDate((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list16));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list17.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone17 = false;
                                                        while(!loopDone17){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone17 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","location").equals(reader.getName())){
                                                                    list17.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone17 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setLocation((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list17));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","memo").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list18.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone18 = false;
                                                        while(!loopDone18){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone18 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","memo").equals(reader.getName())){
                                                                    list18.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone18 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setMemo((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list18));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","name").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list19.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone19 = false;
                                                        while(!loopDone19){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone19 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","name").equals(reader.getName())){
                                                                    list19.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone19 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setName((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list19));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nextBillCycleDate").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list20.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone20 = false;
                                                        while(!loopDone20){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone20 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nextBillCycleDate").equals(reader.getName())){
                                                                    list20.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone20 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setNextBillCycleDate((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list20));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDate").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list21.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone21 = false;
                                                        while(!loopDone21){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone21 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDate").equals(reader.getName())){
                                                                    list21.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone21 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setStartDate((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list21));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list22.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone22 = false;
                                                        while(!loopDone22){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone22 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                                                    list22.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone22 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setSubsidiary((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list22));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    