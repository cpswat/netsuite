
/**
 * InboundShipmentSearchBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  InboundShipmentSearchBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class InboundShipmentSearchBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRecordBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = InboundShipmentSearchBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for ActualDeliveryDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localActualDeliveryDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localActualDeliveryDateTracker = false ;

                           public boolean isActualDeliveryDateSpecified(){
                               return localActualDeliveryDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getActualDeliveryDate(){
                               return localActualDeliveryDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ActualDeliveryDate
                               */
                               public void setActualDeliveryDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localActualDeliveryDateTracker = param != null;
                                   
                                            this.localActualDeliveryDate=param;
                                    

                               }
                            

                        /**
                        * field for ActualShippingDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localActualShippingDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localActualShippingDateTracker = false ;

                           public boolean isActualShippingDateSpecified(){
                               return localActualShippingDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getActualShippingDate(){
                               return localActualShippingDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ActualShippingDate
                               */
                               public void setActualShippingDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localActualShippingDateTracker = param != null;
                                   
                                            this.localActualShippingDate=param;
                                    

                               }
                            

                        /**
                        * field for BillOfLading
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localBillOfLading ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillOfLadingTracker = false ;

                           public boolean isBillOfLadingSpecified(){
                               return localBillOfLadingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getBillOfLading(){
                               return localBillOfLading;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillOfLading
                               */
                               public void setBillOfLading(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localBillOfLadingTracker = param != null;
                                   
                                            this.localBillOfLading=param;
                                    

                               }
                            

                        /**
                        * field for CreatedDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localCreatedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedDateTracker = false ;

                           public boolean isCreatedDateSpecified(){
                               return localCreatedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getCreatedDate(){
                               return localCreatedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedDate
                               */
                               public void setCreatedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localCreatedDateTracker = param != null;
                                   
                                            this.localCreatedDate=param;
                                    

                               }
                            

                        /**
                        * field for Currency
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrencyTracker = false ;

                           public boolean isCurrencySpecified(){
                               return localCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCurrency(){
                               return localCurrency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Currency
                               */
                               public void setCurrency(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCurrencyTracker = param != null;
                                   
                                            this.localCurrency=param;
                                    

                               }
                            

                        /**
                        * field for Description
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localDescription ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDescriptionTracker = false ;

                           public boolean isDescriptionSpecified(){
                               return localDescriptionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getDescription(){
                               return localDescription;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Description
                               */
                               public void setDescription(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localDescriptionTracker = param != null;
                                   
                                            this.localDescription=param;
                                    

                               }
                            

                        /**
                        * field for ExpectedDeliveryDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localExpectedDeliveryDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExpectedDeliveryDateTracker = false ;

                           public boolean isExpectedDeliveryDateSpecified(){
                               return localExpectedDeliveryDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getExpectedDeliveryDate(){
                               return localExpectedDeliveryDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExpectedDeliveryDate
                               */
                               public void setExpectedDeliveryDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localExpectedDeliveryDateTracker = param != null;
                                   
                                            this.localExpectedDeliveryDate=param;
                                    

                               }
                            

                        /**
                        * field for ExpectedRate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localExpectedRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExpectedRateTracker = false ;

                           public boolean isExpectedRateSpecified(){
                               return localExpectedRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getExpectedRate(){
                               return localExpectedRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExpectedRate
                               */
                               public void setExpectedRate(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localExpectedRateTracker = param != null;
                                   
                                            this.localExpectedRate=param;
                                    

                               }
                            

                        /**
                        * field for ExpectedShippingDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localExpectedShippingDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExpectedShippingDateTracker = false ;

                           public boolean isExpectedShippingDateSpecified(){
                               return localExpectedShippingDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getExpectedShippingDate(){
                               return localExpectedShippingDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExpectedShippingDate
                               */
                               public void setExpectedShippingDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localExpectedShippingDateTracker = param != null;
                                   
                                            this.localExpectedShippingDate=param;
                                    

                               }
                            

                        /**
                        * field for ExternalDocumentNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localExternalDocumentNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalDocumentNumberTracker = false ;

                           public boolean isExternalDocumentNumberSpecified(){
                               return localExternalDocumentNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getExternalDocumentNumber(){
                               return localExternalDocumentNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalDocumentNumber
                               */
                               public void setExternalDocumentNumber(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localExternalDocumentNumberTracker = param != null;
                                   
                                            this.localExternalDocumentNumber=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localExternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdTracker = false ;

                           public boolean isExternalIdSpecified(){
                               return localExternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localExternalIdTracker = param != null;
                                   
                                            this.localExternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalIdString
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localExternalIdString ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdStringTracker = false ;

                           public boolean isExternalIdStringSpecified(){
                               return localExternalIdStringTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getExternalIdString(){
                               return localExternalIdString;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalIdString
                               */
                               public void setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localExternalIdStringTracker = param != null;
                                   
                                            this.localExternalIdString=param;
                                    

                               }
                            

                        /**
                        * field for Incoterm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localIncoterm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIncotermTracker = false ;

                           public boolean isIncotermSpecified(){
                               return localIncotermTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getIncoterm(){
                               return localIncoterm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Incoterm
                               */
                               public void setIncoterm(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localIncotermTracker = param != null;
                                   
                                            this.localIncoterm=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localInternalIdTracker = param != null;
                                   
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for InternalIdNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localInternalIdNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdNumberTracker = false ;

                           public boolean isInternalIdNumberSpecified(){
                               return localInternalIdNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getInternalIdNumber(){
                               return localInternalIdNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalIdNumber
                               */
                               public void setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localInternalIdNumberTracker = param != null;
                                   
                                            this.localInternalIdNumber=param;
                                    

                               }
                            

                        /**
                        * field for Item
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemTracker = false ;

                           public boolean isItemSpecified(){
                               return localItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getItem(){
                               return localItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Item
                               */
                               public void setItem(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localItemTracker = param != null;
                                   
                                            this.localItem=param;
                                    

                               }
                            

                        /**
                        * field for Memo
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localMemo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMemoTracker = false ;

                           public boolean isMemoSpecified(){
                               return localMemoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getMemo(){
                               return localMemo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Memo
                               */
                               public void setMemo(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localMemoTracker = param != null;
                                   
                                            this.localMemo=param;
                                    

                               }
                            

                        /**
                        * field for PoAmount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localPoAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPoAmountTracker = false ;

                           public boolean isPoAmountSpecified(){
                               return localPoAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getPoAmount(){
                               return localPoAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PoAmount
                               */
                               public void setPoAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localPoAmountTracker = param != null;
                                   
                                            this.localPoAmount=param;
                                    

                               }
                            

                        /**
                        * field for PoRate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localPoRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPoRateTracker = false ;

                           public boolean isPoRateSpecified(){
                               return localPoRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getPoRate(){
                               return localPoRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PoRate
                               */
                               public void setPoRate(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localPoRateTracker = param != null;
                                   
                                            this.localPoRate=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseOrder
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localPurchaseOrder ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseOrderTracker = false ;

                           public boolean isPurchaseOrderSpecified(){
                               return localPurchaseOrderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getPurchaseOrder(){
                               return localPurchaseOrder;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseOrder
                               */
                               public void setPurchaseOrder(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localPurchaseOrderTracker = param != null;
                                   
                                            this.localPurchaseOrder=param;
                                    

                               }
                            

                        /**
                        * field for QuantityBilled
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localQuantityBilled ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityBilledTracker = false ;

                           public boolean isQuantityBilledSpecified(){
                               return localQuantityBilledTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getQuantityBilled(){
                               return localQuantityBilled;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityBilled
                               */
                               public void setQuantityBilled(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localQuantityBilledTracker = param != null;
                                   
                                            this.localQuantityBilled=param;
                                    

                               }
                            

                        /**
                        * field for QuantityExpected
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localQuantityExpected ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityExpectedTracker = false ;

                           public boolean isQuantityExpectedSpecified(){
                               return localQuantityExpectedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getQuantityExpected(){
                               return localQuantityExpected;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityExpected
                               */
                               public void setQuantityExpected(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localQuantityExpectedTracker = param != null;
                                   
                                            this.localQuantityExpected=param;
                                    

                               }
                            

                        /**
                        * field for QuantityReceived
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localQuantityReceived ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityReceivedTracker = false ;

                           public boolean isQuantityReceivedSpecified(){
                               return localQuantityReceivedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getQuantityReceived(){
                               return localQuantityReceived;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityReceived
                               */
                               public void setQuantityReceived(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localQuantityReceivedTracker = param != null;
                                   
                                            this.localQuantityReceived=param;
                                    

                               }
                            

                        /**
                        * field for QuantityRemaining
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localQuantityRemaining ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityRemainingTracker = false ;

                           public boolean isQuantityRemainingSpecified(){
                               return localQuantityRemainingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getQuantityRemaining(){
                               return localQuantityRemaining;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityRemaining
                               */
                               public void setQuantityRemaining(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localQuantityRemainingTracker = param != null;
                                   
                                            this.localQuantityRemaining=param;
                                    

                               }
                            

                        /**
                        * field for ReceivingLocation
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localReceivingLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReceivingLocationTracker = false ;

                           public boolean isReceivingLocationSpecified(){
                               return localReceivingLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getReceivingLocation(){
                               return localReceivingLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReceivingLocation
                               */
                               public void setReceivingLocation(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localReceivingLocationTracker = param != null;
                                   
                                            this.localReceivingLocation=param;
                                    

                               }
                            

                        /**
                        * field for ShipmentNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localShipmentNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipmentNumberTracker = false ;

                           public boolean isShipmentNumberSpecified(){
                               return localShipmentNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getShipmentNumber(){
                               return localShipmentNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipmentNumber
                               */
                               public void setShipmentNumber(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localShipmentNumberTracker = param != null;
                                   
                                            this.localShipmentNumber=param;
                                    

                               }
                            

                        /**
                        * field for Status
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStatusTracker = false ;

                           public boolean isStatusSpecified(){
                               return localStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getStatus(){
                               return localStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Status
                               */
                               public void setStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localStatusTracker = param != null;
                                   
                                            this.localStatus=param;
                                    

                               }
                            

                        /**
                        * field for Unit
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localUnit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUnitTracker = false ;

                           public boolean isUnitSpecified(){
                               return localUnitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getUnit(){
                               return localUnit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Unit
                               */
                               public void setUnit(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localUnitTracker = param != null;
                                   
                                            this.localUnit=param;
                                    

                               }
                            

                        /**
                        * field for Vendor
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localVendor ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVendorTracker = false ;

                           public boolean isVendorSpecified(){
                               return localVendorTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getVendor(){
                               return localVendor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Vendor
                               */
                               public void setVendor(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localVendorTracker = param != null;
                                   
                                            this.localVendor=param;
                                    

                               }
                            

                        /**
                        * field for VesselNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localVesselNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVesselNumberTracker = false ;

                           public boolean isVesselNumberSpecified(){
                               return localVesselNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getVesselNumber(){
                               return localVesselNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VesselNumber
                               */
                               public void setVesselNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localVesselNumberTracker = param != null;
                                   
                                            this.localVesselNumber=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":InboundShipmentSearchBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "InboundShipmentSearchBasic",
                           xmlWriter);
                   }

                if (localActualDeliveryDateTracker){
                                            if (localActualDeliveryDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("actualDeliveryDate cannot be null!!");
                                            }
                                           localActualDeliveryDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","actualDeliveryDate"),
                                               xmlWriter);
                                        } if (localActualShippingDateTracker){
                                            if (localActualShippingDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("actualShippingDate cannot be null!!");
                                            }
                                           localActualShippingDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","actualShippingDate"),
                                               xmlWriter);
                                        } if (localBillOfLadingTracker){
                                            if (localBillOfLading==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billOfLading cannot be null!!");
                                            }
                                           localBillOfLading.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billOfLading"),
                                               xmlWriter);
                                        } if (localCreatedDateTracker){
                                            if (localCreatedDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                            }
                                           localCreatedDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","createdDate"),
                                               xmlWriter);
                                        } if (localCurrencyTracker){
                                            if (localCurrency==null){
                                                 throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                            }
                                           localCurrency.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency"),
                                               xmlWriter);
                                        } if (localDescriptionTracker){
                                            if (localDescription==null){
                                                 throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                            }
                                           localDescription.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","description"),
                                               xmlWriter);
                                        } if (localExpectedDeliveryDateTracker){
                                            if (localExpectedDeliveryDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("expectedDeliveryDate cannot be null!!");
                                            }
                                           localExpectedDeliveryDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","expectedDeliveryDate"),
                                               xmlWriter);
                                        } if (localExpectedRateTracker){
                                            if (localExpectedRate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("expectedRate cannot be null!!");
                                            }
                                           localExpectedRate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","expectedRate"),
                                               xmlWriter);
                                        } if (localExpectedShippingDateTracker){
                                            if (localExpectedShippingDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("expectedShippingDate cannot be null!!");
                                            }
                                           localExpectedShippingDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","expectedShippingDate"),
                                               xmlWriter);
                                        } if (localExternalDocumentNumberTracker){
                                            if (localExternalDocumentNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalDocumentNumber cannot be null!!");
                                            }
                                           localExternalDocumentNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalDocumentNumber"),
                                               xmlWriter);
                                        } if (localExternalIdTracker){
                                            if (localExternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                            }
                                           localExternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId"),
                                               xmlWriter);
                                        } if (localExternalIdStringTracker){
                                            if (localExternalIdString==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                            }
                                           localExternalIdString.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString"),
                                               xmlWriter);
                                        } if (localIncotermTracker){
                                            if (localIncoterm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("incoterm cannot be null!!");
                                            }
                                           localIncoterm.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","incoterm"),
                                               xmlWriter);
                                        } if (localInternalIdTracker){
                                            if (localInternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                            }
                                           localInternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                               xmlWriter);
                                        } if (localInternalIdNumberTracker){
                                            if (localInternalIdNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                            }
                                           localInternalIdNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber"),
                                               xmlWriter);
                                        } if (localItemTracker){
                                            if (localItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                            }
                                           localItem.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","item"),
                                               xmlWriter);
                                        } if (localMemoTracker){
                                            if (localMemo==null){
                                                 throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                            }
                                           localMemo.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","memo"),
                                               xmlWriter);
                                        } if (localPoAmountTracker){
                                            if (localPoAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("poAmount cannot be null!!");
                                            }
                                           localPoAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","poAmount"),
                                               xmlWriter);
                                        } if (localPoRateTracker){
                                            if (localPoRate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("poRate cannot be null!!");
                                            }
                                           localPoRate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","poRate"),
                                               xmlWriter);
                                        } if (localPurchaseOrderTracker){
                                            if (localPurchaseOrder==null){
                                                 throw new org.apache.axis2.databinding.ADBException("purchaseOrder cannot be null!!");
                                            }
                                           localPurchaseOrder.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","purchaseOrder"),
                                               xmlWriter);
                                        } if (localQuantityBilledTracker){
                                            if (localQuantityBilled==null){
                                                 throw new org.apache.axis2.databinding.ADBException("quantityBilled cannot be null!!");
                                            }
                                           localQuantityBilled.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","quantityBilled"),
                                               xmlWriter);
                                        } if (localQuantityExpectedTracker){
                                            if (localQuantityExpected==null){
                                                 throw new org.apache.axis2.databinding.ADBException("quantityExpected cannot be null!!");
                                            }
                                           localQuantityExpected.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","quantityExpected"),
                                               xmlWriter);
                                        } if (localQuantityReceivedTracker){
                                            if (localQuantityReceived==null){
                                                 throw new org.apache.axis2.databinding.ADBException("quantityReceived cannot be null!!");
                                            }
                                           localQuantityReceived.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","quantityReceived"),
                                               xmlWriter);
                                        } if (localQuantityRemainingTracker){
                                            if (localQuantityRemaining==null){
                                                 throw new org.apache.axis2.databinding.ADBException("quantityRemaining cannot be null!!");
                                            }
                                           localQuantityRemaining.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","quantityRemaining"),
                                               xmlWriter);
                                        } if (localReceivingLocationTracker){
                                            if (localReceivingLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("receivingLocation cannot be null!!");
                                            }
                                           localReceivingLocation.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","receivingLocation"),
                                               xmlWriter);
                                        } if (localShipmentNumberTracker){
                                            if (localShipmentNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shipmentNumber cannot be null!!");
                                            }
                                           localShipmentNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","shipmentNumber"),
                                               xmlWriter);
                                        } if (localStatusTracker){
                                            if (localStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                            }
                                           localStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","status"),
                                               xmlWriter);
                                        } if (localUnitTracker){
                                            if (localUnit==null){
                                                 throw new org.apache.axis2.databinding.ADBException("unit cannot be null!!");
                                            }
                                           localUnit.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","unit"),
                                               xmlWriter);
                                        } if (localVendorTracker){
                                            if (localVendor==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vendor cannot be null!!");
                                            }
                                           localVendor.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","vendor"),
                                               xmlWriter);
                                        } if (localVesselNumberTracker){
                                            if (localVesselNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vesselNumber cannot be null!!");
                                            }
                                           localVesselNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","vesselNumber"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","InboundShipmentSearchBasic"));
                 if (localActualDeliveryDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "actualDeliveryDate"));
                            
                            
                                    if (localActualDeliveryDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("actualDeliveryDate cannot be null!!");
                                    }
                                    elementList.add(localActualDeliveryDate);
                                } if (localActualShippingDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "actualShippingDate"));
                            
                            
                                    if (localActualShippingDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("actualShippingDate cannot be null!!");
                                    }
                                    elementList.add(localActualShippingDate);
                                } if (localBillOfLadingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "billOfLading"));
                            
                            
                                    if (localBillOfLading==null){
                                         throw new org.apache.axis2.databinding.ADBException("billOfLading cannot be null!!");
                                    }
                                    elementList.add(localBillOfLading);
                                } if (localCreatedDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "createdDate"));
                            
                            
                                    if (localCreatedDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                    }
                                    elementList.add(localCreatedDate);
                                } if (localCurrencyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "currency"));
                            
                            
                                    if (localCurrency==null){
                                         throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                    }
                                    elementList.add(localCurrency);
                                } if (localDescriptionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "description"));
                            
                            
                                    if (localDescription==null){
                                         throw new org.apache.axis2.databinding.ADBException("description cannot be null!!");
                                    }
                                    elementList.add(localDescription);
                                } if (localExpectedDeliveryDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "expectedDeliveryDate"));
                            
                            
                                    if (localExpectedDeliveryDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("expectedDeliveryDate cannot be null!!");
                                    }
                                    elementList.add(localExpectedDeliveryDate);
                                } if (localExpectedRateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "expectedRate"));
                            
                            
                                    if (localExpectedRate==null){
                                         throw new org.apache.axis2.databinding.ADBException("expectedRate cannot be null!!");
                                    }
                                    elementList.add(localExpectedRate);
                                } if (localExpectedShippingDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "expectedShippingDate"));
                            
                            
                                    if (localExpectedShippingDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("expectedShippingDate cannot be null!!");
                                    }
                                    elementList.add(localExpectedShippingDate);
                                } if (localExternalDocumentNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalDocumentNumber"));
                            
                            
                                    if (localExternalDocumentNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalDocumentNumber cannot be null!!");
                                    }
                                    elementList.add(localExternalDocumentNumber);
                                } if (localExternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalId"));
                            
                            
                                    if (localExternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                    }
                                    elementList.add(localExternalId);
                                } if (localExternalIdStringTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalIdString"));
                            
                            
                                    if (localExternalIdString==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                    }
                                    elementList.add(localExternalIdString);
                                } if (localIncotermTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "incoterm"));
                            
                            
                                    if (localIncoterm==null){
                                         throw new org.apache.axis2.databinding.ADBException("incoterm cannot be null!!");
                                    }
                                    elementList.add(localIncoterm);
                                } if (localInternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalId"));
                            
                            
                                    if (localInternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    }
                                    elementList.add(localInternalId);
                                } if (localInternalIdNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalIdNumber"));
                            
                            
                                    if (localInternalIdNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                    }
                                    elementList.add(localInternalIdNumber);
                                } if (localItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "item"));
                            
                            
                                    if (localItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                    }
                                    elementList.add(localItem);
                                } if (localMemoTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "memo"));
                            
                            
                                    if (localMemo==null){
                                         throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                    }
                                    elementList.add(localMemo);
                                } if (localPoAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "poAmount"));
                            
                            
                                    if (localPoAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("poAmount cannot be null!!");
                                    }
                                    elementList.add(localPoAmount);
                                } if (localPoRateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "poRate"));
                            
                            
                                    if (localPoRate==null){
                                         throw new org.apache.axis2.databinding.ADBException("poRate cannot be null!!");
                                    }
                                    elementList.add(localPoRate);
                                } if (localPurchaseOrderTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "purchaseOrder"));
                            
                            
                                    if (localPurchaseOrder==null){
                                         throw new org.apache.axis2.databinding.ADBException("purchaseOrder cannot be null!!");
                                    }
                                    elementList.add(localPurchaseOrder);
                                } if (localQuantityBilledTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "quantityBilled"));
                            
                            
                                    if (localQuantityBilled==null){
                                         throw new org.apache.axis2.databinding.ADBException("quantityBilled cannot be null!!");
                                    }
                                    elementList.add(localQuantityBilled);
                                } if (localQuantityExpectedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "quantityExpected"));
                            
                            
                                    if (localQuantityExpected==null){
                                         throw new org.apache.axis2.databinding.ADBException("quantityExpected cannot be null!!");
                                    }
                                    elementList.add(localQuantityExpected);
                                } if (localQuantityReceivedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "quantityReceived"));
                            
                            
                                    if (localQuantityReceived==null){
                                         throw new org.apache.axis2.databinding.ADBException("quantityReceived cannot be null!!");
                                    }
                                    elementList.add(localQuantityReceived);
                                } if (localQuantityRemainingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "quantityRemaining"));
                            
                            
                                    if (localQuantityRemaining==null){
                                         throw new org.apache.axis2.databinding.ADBException("quantityRemaining cannot be null!!");
                                    }
                                    elementList.add(localQuantityRemaining);
                                } if (localReceivingLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "receivingLocation"));
                            
                            
                                    if (localReceivingLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("receivingLocation cannot be null!!");
                                    }
                                    elementList.add(localReceivingLocation);
                                } if (localShipmentNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "shipmentNumber"));
                            
                            
                                    if (localShipmentNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("shipmentNumber cannot be null!!");
                                    }
                                    elementList.add(localShipmentNumber);
                                } if (localStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "status"));
                            
                            
                                    if (localStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                    }
                                    elementList.add(localStatus);
                                } if (localUnitTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "unit"));
                            
                            
                                    if (localUnit==null){
                                         throw new org.apache.axis2.databinding.ADBException("unit cannot be null!!");
                                    }
                                    elementList.add(localUnit);
                                } if (localVendorTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "vendor"));
                            
                            
                                    if (localVendor==null){
                                         throw new org.apache.axis2.databinding.ADBException("vendor cannot be null!!");
                                    }
                                    elementList.add(localVendor);
                                } if (localVesselNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "vesselNumber"));
                            
                            
                                    if (localVesselNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("vesselNumber cannot be null!!");
                                    }
                                    elementList.add(localVesselNumber);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static InboundShipmentSearchBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            InboundShipmentSearchBasic object =
                new InboundShipmentSearchBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"InboundShipmentSearchBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (InboundShipmentSearchBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","actualDeliveryDate").equals(reader.getName())){
                                
                                                object.setActualDeliveryDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","actualShippingDate").equals(reader.getName())){
                                
                                                object.setActualShippingDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billOfLading").equals(reader.getName())){
                                
                                                object.setBillOfLading(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","createdDate").equals(reader.getName())){
                                
                                                object.setCreatedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency").equals(reader.getName())){
                                
                                                object.setCurrency(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","description").equals(reader.getName())){
                                
                                                object.setDescription(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","expectedDeliveryDate").equals(reader.getName())){
                                
                                                object.setExpectedDeliveryDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","expectedRate").equals(reader.getName())){
                                
                                                object.setExpectedRate(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","expectedShippingDate").equals(reader.getName())){
                                
                                                object.setExpectedShippingDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalDocumentNumber").equals(reader.getName())){
                                
                                                object.setExternalDocumentNumber(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                
                                                object.setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString").equals(reader.getName())){
                                
                                                object.setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","incoterm").equals(reader.getName())){
                                
                                                object.setIncoterm(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                                object.setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber").equals(reader.getName())){
                                
                                                object.setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","item").equals(reader.getName())){
                                
                                                object.setItem(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","memo").equals(reader.getName())){
                                
                                                object.setMemo(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","poAmount").equals(reader.getName())){
                                
                                                object.setPoAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","poRate").equals(reader.getName())){
                                
                                                object.setPoRate(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","purchaseOrder").equals(reader.getName())){
                                
                                                object.setPurchaseOrder(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","quantityBilled").equals(reader.getName())){
                                
                                                object.setQuantityBilled(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","quantityExpected").equals(reader.getName())){
                                
                                                object.setQuantityExpected(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","quantityReceived").equals(reader.getName())){
                                
                                                object.setQuantityReceived(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","quantityRemaining").equals(reader.getName())){
                                
                                                object.setQuantityRemaining(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","receivingLocation").equals(reader.getName())){
                                
                                                object.setReceivingLocation(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","shipmentNumber").equals(reader.getName())){
                                
                                                object.setShipmentNumber(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","status").equals(reader.getName())){
                                
                                                object.setStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","unit").equals(reader.getName())){
                                
                                                object.setUnit(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","vendor").equals(reader.getName())){
                                
                                                object.setVendor(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","vesselNumber").equals(reader.getName())){
                                
                                                object.setVesselNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    