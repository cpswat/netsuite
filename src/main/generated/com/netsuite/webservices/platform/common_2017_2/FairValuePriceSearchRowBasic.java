
/**
 * FairValuePriceSearchRowBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  FairValuePriceSearchRowBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class FairValuePriceSearchRowBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRowBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = FairValuePriceSearchRowBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for Currency
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrencyTracker = false ;

                           public boolean isCurrencySpecified(){
                               return localCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getCurrency(){
                               return localCurrency;
                           }

                           
                        


                               
                              /**
                               * validate the array for Currency
                               */
                              protected void validateCurrency(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Currency
                              */
                              public void setCurrency(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateCurrency(param);

                               localCurrencyTracker = param != null;
                                      
                                      this.localCurrency=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addCurrency(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localCurrency == null){
                                   localCurrency = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localCurrencyTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCurrency);
                               list.add(param);
                               this.localCurrency =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for EndDate
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndDateTracker = false ;

                           public boolean isEndDateSpecified(){
                               return localEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getEndDate(){
                               return localEndDate;
                           }

                           
                        


                               
                              /**
                               * validate the array for EndDate
                               */
                              protected void validateEndDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param EndDate
                              */
                              public void setEndDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateEndDate(param);

                               localEndDateTracker = param != null;
                                      
                                      this.localEndDate=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addEndDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localEndDate == null){
                                   localEndDate = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localEndDateTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localEndDate);
                               list.add(param);
                               this.localEndDate =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for ExternalId
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localExternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdTracker = false ;

                           public boolean isExternalIdSpecified(){
                               return localExternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getExternalId(){
                               return localExternalId;
                           }

                           
                        


                               
                              /**
                               * validate the array for ExternalId
                               */
                              protected void validateExternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param ExternalId
                              */
                              public void setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateExternalId(param);

                               localExternalIdTracker = param != null;
                                      
                                      this.localExternalId=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addExternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localExternalId == null){
                                   localExternalId = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localExternalIdTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localExternalId);
                               list.add(param);
                               this.localExternalId =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for FairValue
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localFairValue ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFairValueTracker = false ;

                           public boolean isFairValueSpecified(){
                               return localFairValueTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getFairValue(){
                               return localFairValue;
                           }

                           
                        


                               
                              /**
                               * validate the array for FairValue
                               */
                              protected void validateFairValue(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param FairValue
                              */
                              public void setFairValue(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateFairValue(param);

                               localFairValueTracker = param != null;
                                      
                                      this.localFairValue=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addFairValue(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localFairValue == null){
                                   localFairValue = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localFairValueTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localFairValue);
                               list.add(param);
                               this.localFairValue =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for FairValueFormula
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localFairValueFormula ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFairValueFormulaTracker = false ;

                           public boolean isFairValueFormulaSpecified(){
                               return localFairValueFormulaTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getFairValueFormula(){
                               return localFairValueFormula;
                           }

                           
                        


                               
                              /**
                               * validate the array for FairValueFormula
                               */
                              protected void validateFairValueFormula(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param FairValueFormula
                              */
                              public void setFairValueFormula(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateFairValueFormula(param);

                               localFairValueFormulaTracker = param != null;
                                      
                                      this.localFairValueFormula=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addFairValueFormula(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localFairValueFormula == null){
                                   localFairValueFormula = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localFairValueFormulaTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localFairValueFormula);
                               list.add(param);
                               this.localFairValueFormula =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for FairValueRangePolicy
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] localFairValueRangePolicy ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFairValueRangePolicyTracker = false ;

                           public boolean isFairValueRangePolicySpecified(){
                               return localFairValueRangePolicyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] getFairValueRangePolicy(){
                               return localFairValueRangePolicy;
                           }

                           
                        


                               
                              /**
                               * validate the array for FairValueRangePolicy
                               */
                              protected void validateFairValueRangePolicy(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param FairValueRangePolicy
                              */
                              public void setFairValueRangePolicy(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                              
                                   validateFairValueRangePolicy(param);

                               localFairValueRangePolicyTracker = param != null;
                                      
                                      this.localFairValueRangePolicy=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField
                             */
                             public void addFairValueRangePolicy(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField param){
                                   if (localFairValueRangePolicy == null){
                                   localFairValueRangePolicy = new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localFairValueRangePolicyTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localFairValueRangePolicy);
                               list.add(param);
                               this.localFairValueRangePolicy =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[list.size()]);

                             }
                             

                        /**
                        * field for HighValue
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localHighValue ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHighValueTracker = false ;

                           public boolean isHighValueSpecified(){
                               return localHighValueTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getHighValue(){
                               return localHighValue;
                           }

                           
                        


                               
                              /**
                               * validate the array for HighValue
                               */
                              protected void validateHighValue(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param HighValue
                              */
                              public void setHighValue(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateHighValue(param);

                               localHighValueTracker = param != null;
                                      
                                      this.localHighValue=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addHighValue(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localHighValue == null){
                                   localHighValue = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localHighValueTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localHighValue);
                               list.add(param);
                               this.localHighValue =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for HighValuePercent
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localHighValuePercent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHighValuePercentTracker = false ;

                           public boolean isHighValuePercentSpecified(){
                               return localHighValuePercentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getHighValuePercent(){
                               return localHighValuePercent;
                           }

                           
                        


                               
                              /**
                               * validate the array for HighValuePercent
                               */
                              protected void validateHighValuePercent(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param HighValuePercent
                              */
                              public void setHighValuePercent(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateHighValuePercent(param);

                               localHighValuePercentTracker = param != null;
                                      
                                      this.localHighValuePercent=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addHighValuePercent(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localHighValuePercent == null){
                                   localHighValuePercent = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localHighValuePercentTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localHighValuePercent);
                               list.add(param);
                               this.localHighValuePercent =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for InternalId
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getInternalId(){
                               return localInternalId;
                           }

                           
                        


                               
                              /**
                               * validate the array for InternalId
                               */
                              protected void validateInternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param InternalId
                              */
                              public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateInternalId(param);

                               localInternalIdTracker = param != null;
                                      
                                      this.localInternalId=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addInternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localInternalId == null){
                                   localInternalId = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localInternalIdTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localInternalId);
                               list.add(param);
                               this.localInternalId =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for IsVsoePrice
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localIsVsoePrice ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsVsoePriceTracker = false ;

                           public boolean isIsVsoePriceSpecified(){
                               return localIsVsoePriceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getIsVsoePrice(){
                               return localIsVsoePrice;
                           }

                           
                        


                               
                              /**
                               * validate the array for IsVsoePrice
                               */
                              protected void validateIsVsoePrice(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param IsVsoePrice
                              */
                              public void setIsVsoePrice(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateIsVsoePrice(param);

                               localIsVsoePriceTracker = param != null;
                                      
                                      this.localIsVsoePrice=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addIsVsoePrice(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localIsVsoePrice == null){
                                   localIsVsoePrice = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localIsVsoePriceTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localIsVsoePrice);
                               list.add(param);
                               this.localIsVsoePrice =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for Item
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemTracker = false ;

                           public boolean isItemSpecified(){
                               return localItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getItem(){
                               return localItem;
                           }

                           
                        


                               
                              /**
                               * validate the array for Item
                               */
                              protected void validateItem(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Item
                              */
                              public void setItem(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateItem(param);

                               localItemTracker = param != null;
                                      
                                      this.localItem=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addItem(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localItem == null){
                                   localItem = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localItemTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localItem);
                               list.add(param);
                               this.localItem =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for ItemRevenueCategory
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localItemRevenueCategory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemRevenueCategoryTracker = false ;

                           public boolean isItemRevenueCategorySpecified(){
                               return localItemRevenueCategoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getItemRevenueCategory(){
                               return localItemRevenueCategory;
                           }

                           
                        


                               
                              /**
                               * validate the array for ItemRevenueCategory
                               */
                              protected void validateItemRevenueCategory(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param ItemRevenueCategory
                              */
                              public void setItemRevenueCategory(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateItemRevenueCategory(param);

                               localItemRevenueCategoryTracker = param != null;
                                      
                                      this.localItemRevenueCategory=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addItemRevenueCategory(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localItemRevenueCategory == null){
                                   localItemRevenueCategory = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localItemRevenueCategoryTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localItemRevenueCategory);
                               list.add(param);
                               this.localItemRevenueCategory =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for LowValue
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localLowValue ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLowValueTracker = false ;

                           public boolean isLowValueSpecified(){
                               return localLowValueTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getLowValue(){
                               return localLowValue;
                           }

                           
                        


                               
                              /**
                               * validate the array for LowValue
                               */
                              protected void validateLowValue(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param LowValue
                              */
                              public void setLowValue(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateLowValue(param);

                               localLowValueTracker = param != null;
                                      
                                      this.localLowValue=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addLowValue(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localLowValue == null){
                                   localLowValue = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localLowValueTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localLowValue);
                               list.add(param);
                               this.localLowValue =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for LowValuePercent
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localLowValuePercent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLowValuePercentTracker = false ;

                           public boolean isLowValuePercentSpecified(){
                               return localLowValuePercentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getLowValuePercent(){
                               return localLowValuePercent;
                           }

                           
                        


                               
                              /**
                               * validate the array for LowValuePercent
                               */
                              protected void validateLowValuePercent(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param LowValuePercent
                              */
                              public void setLowValuePercent(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateLowValuePercent(param);

                               localLowValuePercentTracker = param != null;
                                      
                                      this.localLowValuePercent=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addLowValuePercent(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localLowValuePercent == null){
                                   localLowValuePercent = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localLowValuePercentTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localLowValuePercent);
                               list.add(param);
                               this.localLowValuePercent =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for StartDate
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateTracker = false ;

                           public boolean isStartDateSpecified(){
                               return localStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getStartDate(){
                               return localStartDate;
                           }

                           
                        


                               
                              /**
                               * validate the array for StartDate
                               */
                              protected void validateStartDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param StartDate
                              */
                              public void setStartDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateStartDate(param);

                               localStartDateTracker = param != null;
                                      
                                      this.localStartDate=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addStartDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localStartDate == null){
                                   localStartDate = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localStartDateTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localStartDate);
                               list.add(param);
                               this.localStartDate =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for Units
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localUnits ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUnitsTracker = false ;

                           public boolean isUnitsSpecified(){
                               return localUnitsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getUnits(){
                               return localUnits;
                           }

                           
                        


                               
                              /**
                               * validate the array for Units
                               */
                              protected void validateUnits(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Units
                              */
                              public void setUnits(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateUnits(param);

                               localUnitsTracker = param != null;
                                      
                                      this.localUnits=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addUnits(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localUnits == null){
                                   localUnits = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localUnitsTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localUnits);
                               list.add(param);
                               this.localUnits =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for UnitsType
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localUnitsType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUnitsTypeTracker = false ;

                           public boolean isUnitsTypeSpecified(){
                               return localUnitsTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getUnitsType(){
                               return localUnitsType;
                           }

                           
                        


                               
                              /**
                               * validate the array for UnitsType
                               */
                              protected void validateUnitsType(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param UnitsType
                              */
                              public void setUnitsType(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateUnitsType(param);

                               localUnitsTypeTracker = param != null;
                                      
                                      this.localUnitsType=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addUnitsType(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localUnitsType == null){
                                   localUnitsType = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localUnitsTypeTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localUnitsType);
                               list.add(param);
                               this.localUnitsType =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":FairValuePriceSearchRowBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "FairValuePriceSearchRowBasic",
                           xmlWriter);
                   }

                if (localCurrencyTracker){
                                       if (localCurrency!=null){
                                            for (int i = 0;i < localCurrency.length;i++){
                                                if (localCurrency[i] != null){
                                                 localCurrency[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                        
                                    }
                                 } if (localEndDateTracker){
                                       if (localEndDate!=null){
                                            for (int i = 0;i < localEndDate.length;i++){
                                                if (localEndDate[i] != null){
                                                 localEndDate[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endDate"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                        
                                    }
                                 } if (localExternalIdTracker){
                                       if (localExternalId!=null){
                                            for (int i = 0;i < localExternalId.length;i++){
                                                if (localExternalId[i] != null){
                                                 localExternalId[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                        
                                    }
                                 } if (localFairValueTracker){
                                       if (localFairValue!=null){
                                            for (int i = 0;i < localFairValue.length;i++){
                                                if (localFairValue[i] != null){
                                                 localFairValue[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fairValue"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("fairValue cannot be null!!");
                                        
                                    }
                                 } if (localFairValueFormulaTracker){
                                       if (localFairValueFormula!=null){
                                            for (int i = 0;i < localFairValueFormula.length;i++){
                                                if (localFairValueFormula[i] != null){
                                                 localFairValueFormula[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fairValueFormula"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("fairValueFormula cannot be null!!");
                                        
                                    }
                                 } if (localFairValueRangePolicyTracker){
                                       if (localFairValueRangePolicy!=null){
                                            for (int i = 0;i < localFairValueRangePolicy.length;i++){
                                                if (localFairValueRangePolicy[i] != null){
                                                 localFairValueRangePolicy[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fairValueRangePolicy"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("fairValueRangePolicy cannot be null!!");
                                        
                                    }
                                 } if (localHighValueTracker){
                                       if (localHighValue!=null){
                                            for (int i = 0;i < localHighValue.length;i++){
                                                if (localHighValue[i] != null){
                                                 localHighValue[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","highValue"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("highValue cannot be null!!");
                                        
                                    }
                                 } if (localHighValuePercentTracker){
                                       if (localHighValuePercent!=null){
                                            for (int i = 0;i < localHighValuePercent.length;i++){
                                                if (localHighValuePercent[i] != null){
                                                 localHighValuePercent[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","highValuePercent"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("highValuePercent cannot be null!!");
                                        
                                    }
                                 } if (localInternalIdTracker){
                                       if (localInternalId!=null){
                                            for (int i = 0;i < localInternalId.length;i++){
                                                if (localInternalId[i] != null){
                                                 localInternalId[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                        
                                    }
                                 } if (localIsVsoePriceTracker){
                                       if (localIsVsoePrice!=null){
                                            for (int i = 0;i < localIsVsoePrice.length;i++){
                                                if (localIsVsoePrice[i] != null){
                                                 localIsVsoePrice[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isVsoePrice"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("isVsoePrice cannot be null!!");
                                        
                                    }
                                 } if (localItemTracker){
                                       if (localItem!=null){
                                            for (int i = 0;i < localItem.length;i++){
                                                if (localItem[i] != null){
                                                 localItem[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","item"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                        
                                    }
                                 } if (localItemRevenueCategoryTracker){
                                       if (localItemRevenueCategory!=null){
                                            for (int i = 0;i < localItemRevenueCategory.length;i++){
                                                if (localItemRevenueCategory[i] != null){
                                                 localItemRevenueCategory[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","itemRevenueCategory"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("itemRevenueCategory cannot be null!!");
                                        
                                    }
                                 } if (localLowValueTracker){
                                       if (localLowValue!=null){
                                            for (int i = 0;i < localLowValue.length;i++){
                                                if (localLowValue[i] != null){
                                                 localLowValue[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lowValue"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("lowValue cannot be null!!");
                                        
                                    }
                                 } if (localLowValuePercentTracker){
                                       if (localLowValuePercent!=null){
                                            for (int i = 0;i < localLowValuePercent.length;i++){
                                                if (localLowValuePercent[i] != null){
                                                 localLowValuePercent[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lowValuePercent"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("lowValuePercent cannot be null!!");
                                        
                                    }
                                 } if (localStartDateTracker){
                                       if (localStartDate!=null){
                                            for (int i = 0;i < localStartDate.length;i++){
                                                if (localStartDate[i] != null){
                                                 localStartDate[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDate"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                        
                                    }
                                 } if (localUnitsTracker){
                                       if (localUnits!=null){
                                            for (int i = 0;i < localUnits.length;i++){
                                                if (localUnits[i] != null){
                                                 localUnits[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","units"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("units cannot be null!!");
                                        
                                    }
                                 } if (localUnitsTypeTracker){
                                       if (localUnitsType!=null){
                                            for (int i = 0;i < localUnitsType.length;i++){
                                                if (localUnitsType[i] != null){
                                                 localUnitsType[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","unitsType"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("unitsType cannot be null!!");
                                        
                                    }
                                 }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","FairValuePriceSearchRowBasic"));
                 if (localCurrencyTracker){
                             if (localCurrency!=null) {
                                 for (int i = 0;i < localCurrency.length;i++){

                                    if (localCurrency[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "currency"));
                                         elementList.add(localCurrency[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                    
                             }

                        } if (localEndDateTracker){
                             if (localEndDate!=null) {
                                 for (int i = 0;i < localEndDate.length;i++){

                                    if (localEndDate[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "endDate"));
                                         elementList.add(localEndDate[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                    
                             }

                        } if (localExternalIdTracker){
                             if (localExternalId!=null) {
                                 for (int i = 0;i < localExternalId.length;i++){

                                    if (localExternalId[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "externalId"));
                                         elementList.add(localExternalId[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                    
                             }

                        } if (localFairValueTracker){
                             if (localFairValue!=null) {
                                 for (int i = 0;i < localFairValue.length;i++){

                                    if (localFairValue[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "fairValue"));
                                         elementList.add(localFairValue[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("fairValue cannot be null!!");
                                    
                             }

                        } if (localFairValueFormulaTracker){
                             if (localFairValueFormula!=null) {
                                 for (int i = 0;i < localFairValueFormula.length;i++){

                                    if (localFairValueFormula[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "fairValueFormula"));
                                         elementList.add(localFairValueFormula[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("fairValueFormula cannot be null!!");
                                    
                             }

                        } if (localFairValueRangePolicyTracker){
                             if (localFairValueRangePolicy!=null) {
                                 for (int i = 0;i < localFairValueRangePolicy.length;i++){

                                    if (localFairValueRangePolicy[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "fairValueRangePolicy"));
                                         elementList.add(localFairValueRangePolicy[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("fairValueRangePolicy cannot be null!!");
                                    
                             }

                        } if (localHighValueTracker){
                             if (localHighValue!=null) {
                                 for (int i = 0;i < localHighValue.length;i++){

                                    if (localHighValue[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "highValue"));
                                         elementList.add(localHighValue[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("highValue cannot be null!!");
                                    
                             }

                        } if (localHighValuePercentTracker){
                             if (localHighValuePercent!=null) {
                                 for (int i = 0;i < localHighValuePercent.length;i++){

                                    if (localHighValuePercent[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "highValuePercent"));
                                         elementList.add(localHighValuePercent[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("highValuePercent cannot be null!!");
                                    
                             }

                        } if (localInternalIdTracker){
                             if (localInternalId!=null) {
                                 for (int i = 0;i < localInternalId.length;i++){

                                    if (localInternalId[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "internalId"));
                                         elementList.add(localInternalId[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    
                             }

                        } if (localIsVsoePriceTracker){
                             if (localIsVsoePrice!=null) {
                                 for (int i = 0;i < localIsVsoePrice.length;i++){

                                    if (localIsVsoePrice[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "isVsoePrice"));
                                         elementList.add(localIsVsoePrice[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("isVsoePrice cannot be null!!");
                                    
                             }

                        } if (localItemTracker){
                             if (localItem!=null) {
                                 for (int i = 0;i < localItem.length;i++){

                                    if (localItem[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "item"));
                                         elementList.add(localItem[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                    
                             }

                        } if (localItemRevenueCategoryTracker){
                             if (localItemRevenueCategory!=null) {
                                 for (int i = 0;i < localItemRevenueCategory.length;i++){

                                    if (localItemRevenueCategory[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "itemRevenueCategory"));
                                         elementList.add(localItemRevenueCategory[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("itemRevenueCategory cannot be null!!");
                                    
                             }

                        } if (localLowValueTracker){
                             if (localLowValue!=null) {
                                 for (int i = 0;i < localLowValue.length;i++){

                                    if (localLowValue[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "lowValue"));
                                         elementList.add(localLowValue[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("lowValue cannot be null!!");
                                    
                             }

                        } if (localLowValuePercentTracker){
                             if (localLowValuePercent!=null) {
                                 for (int i = 0;i < localLowValuePercent.length;i++){

                                    if (localLowValuePercent[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "lowValuePercent"));
                                         elementList.add(localLowValuePercent[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("lowValuePercent cannot be null!!");
                                    
                             }

                        } if (localStartDateTracker){
                             if (localStartDate!=null) {
                                 for (int i = 0;i < localStartDate.length;i++){

                                    if (localStartDate[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "startDate"));
                                         elementList.add(localStartDate[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                    
                             }

                        } if (localUnitsTracker){
                             if (localUnits!=null) {
                                 for (int i = 0;i < localUnits.length;i++){

                                    if (localUnits[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "units"));
                                         elementList.add(localUnits[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("units cannot be null!!");
                                    
                             }

                        } if (localUnitsTypeTracker){
                             if (localUnitsType!=null) {
                                 for (int i = 0;i < localUnitsType.length;i++){

                                    if (localUnitsType[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "unitsType"));
                                         elementList.add(localUnitsType[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("unitsType cannot be null!!");
                                    
                             }

                        }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static FairValuePriceSearchRowBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            FairValuePriceSearchRowBasic object =
                new FairValuePriceSearchRowBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"FairValuePriceSearchRowBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (FairValuePriceSearchRowBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                        java.util.ArrayList list1 = new java.util.ArrayList();
                    
                        java.util.ArrayList list2 = new java.util.ArrayList();
                    
                        java.util.ArrayList list3 = new java.util.ArrayList();
                    
                        java.util.ArrayList list4 = new java.util.ArrayList();
                    
                        java.util.ArrayList list5 = new java.util.ArrayList();
                    
                        java.util.ArrayList list6 = new java.util.ArrayList();
                    
                        java.util.ArrayList list7 = new java.util.ArrayList();
                    
                        java.util.ArrayList list8 = new java.util.ArrayList();
                    
                        java.util.ArrayList list9 = new java.util.ArrayList();
                    
                        java.util.ArrayList list10 = new java.util.ArrayList();
                    
                        java.util.ArrayList list11 = new java.util.ArrayList();
                    
                        java.util.ArrayList list12 = new java.util.ArrayList();
                    
                        java.util.ArrayList list13 = new java.util.ArrayList();
                    
                        java.util.ArrayList list14 = new java.util.ArrayList();
                    
                        java.util.ArrayList list15 = new java.util.ArrayList();
                    
                        java.util.ArrayList list16 = new java.util.ArrayList();
                    
                        java.util.ArrayList list17 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list1.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone1 = false;
                                                        while(!loopDone1){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone1 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency").equals(reader.getName())){
                                                                    list1.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone1 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCurrency((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list1));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endDate").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list2.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone2 = false;
                                                        while(!loopDone2){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone2 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endDate").equals(reader.getName())){
                                                                    list2.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone2 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setEndDate((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list2));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list3.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone3 = false;
                                                        while(!loopDone3){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone3 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                                                    list3.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone3 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setExternalId((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list3));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fairValue").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list4.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone4 = false;
                                                        while(!loopDone4){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone4 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fairValue").equals(reader.getName())){
                                                                    list4.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone4 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setFairValue((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list4));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fairValueFormula").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list5.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone5 = false;
                                                        while(!loopDone5){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone5 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fairValueFormula").equals(reader.getName())){
                                                                    list5.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone5 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setFairValueFormula((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list5));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fairValueRangePolicy").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list6.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone6 = false;
                                                        while(!loopDone6){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone6 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fairValueRangePolicy").equals(reader.getName())){
                                                                    list6.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone6 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setFairValueRangePolicy((com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.class,
                                                                list6));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","highValue").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list7.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone7 = false;
                                                        while(!loopDone7){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone7 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","highValue").equals(reader.getName())){
                                                                    list7.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone7 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setHighValue((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list7));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","highValuePercent").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list8.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone8 = false;
                                                        while(!loopDone8){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone8 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","highValuePercent").equals(reader.getName())){
                                                                    list8.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone8 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setHighValuePercent((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list8));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list9.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone9 = false;
                                                        while(!loopDone9){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone9 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                                                    list9.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone9 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setInternalId((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list9));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isVsoePrice").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list10.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone10 = false;
                                                        while(!loopDone10){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone10 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isVsoePrice").equals(reader.getName())){
                                                                    list10.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone10 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setIsVsoePrice((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list10));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","item").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list11.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone11 = false;
                                                        while(!loopDone11){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone11 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","item").equals(reader.getName())){
                                                                    list11.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone11 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setItem((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list11));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","itemRevenueCategory").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list12.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone12 = false;
                                                        while(!loopDone12){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone12 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","itemRevenueCategory").equals(reader.getName())){
                                                                    list12.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone12 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setItemRevenueCategory((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list12));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lowValue").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list13.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone13 = false;
                                                        while(!loopDone13){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone13 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lowValue").equals(reader.getName())){
                                                                    list13.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone13 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setLowValue((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list13));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lowValuePercent").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list14.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone14 = false;
                                                        while(!loopDone14){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone14 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lowValuePercent").equals(reader.getName())){
                                                                    list14.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone14 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setLowValuePercent((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list14));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDate").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list15.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone15 = false;
                                                        while(!loopDone15){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone15 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDate").equals(reader.getName())){
                                                                    list15.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone15 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setStartDate((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list15));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","units").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list16.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone16 = false;
                                                        while(!loopDone16){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone16 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","units").equals(reader.getName())){
                                                                    list16.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone16 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setUnits((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list16));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","unitsType").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list17.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone17 = false;
                                                        while(!loopDone17){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone17 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","unitsType").equals(reader.getName())){
                                                                    list17.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone17 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setUnitsType((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list17));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    