
/**
 * ManufacturingOperationTaskSearchBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  ManufacturingOperationTaskSearchBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ManufacturingOperationTaskSearchBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRecordBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ManufacturingOperationTaskSearchBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for ActualRunTime
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localActualRunTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localActualRunTimeTracker = false ;

                           public boolean isActualRunTimeSpecified(){
                               return localActualRunTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getActualRunTime(){
                               return localActualRunTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ActualRunTime
                               */
                               public void setActualRunTime(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localActualRunTimeTracker = param != null;
                                   
                                            this.localActualRunTime=param;
                                    

                               }
                            

                        /**
                        * field for ActualSetupTime
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localActualSetupTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localActualSetupTimeTracker = false ;

                           public boolean isActualSetupTimeSpecified(){
                               return localActualSetupTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getActualSetupTime(){
                               return localActualSetupTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ActualSetupTime
                               */
                               public void setActualSetupTime(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localActualSetupTimeTracker = param != null;
                                   
                                            this.localActualSetupTime=param;
                                    

                               }
                            

                        /**
                        * field for CompletedQuantity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localCompletedQuantity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCompletedQuantityTracker = false ;

                           public boolean isCompletedQuantitySpecified(){
                               return localCompletedQuantityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getCompletedQuantity(){
                               return localCompletedQuantity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CompletedQuantity
                               */
                               public void setCompletedQuantity(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localCompletedQuantityTracker = param != null;
                                   
                                            this.localCompletedQuantity=param;
                                    

                               }
                            

                        /**
                        * field for EndDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndDateTracker = false ;

                           public boolean isEndDateSpecified(){
                               return localEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getEndDate(){
                               return localEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndDate
                               */
                               public void setEndDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localEndDateTracker = param != null;
                                   
                                            this.localEndDate=param;
                                    

                               }
                            

                        /**
                        * field for EstimatedWork
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localEstimatedWork ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstimatedWorkTracker = false ;

                           public boolean isEstimatedWorkSpecified(){
                               return localEstimatedWorkTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getEstimatedWork(){
                               return localEstimatedWork;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstimatedWork
                               */
                               public void setEstimatedWork(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localEstimatedWorkTracker = param != null;
                                   
                                            this.localEstimatedWork=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localExternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdTracker = false ;

                           public boolean isExternalIdSpecified(){
                               return localExternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localExternalIdTracker = param != null;
                                   
                                            this.localExternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalIdString
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localExternalIdString ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdStringTracker = false ;

                           public boolean isExternalIdStringSpecified(){
                               return localExternalIdStringTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getExternalIdString(){
                               return localExternalIdString;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalIdString
                               */
                               public void setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localExternalIdStringTracker = param != null;
                                   
                                            this.localExternalIdString=param;
                                    

                               }
                            

                        /**
                        * field for Id
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIdTracker = false ;

                           public boolean isIdSpecified(){
                               return localIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getId(){
                               return localId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Id
                               */
                               public void setId(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localIdTracker = param != null;
                                   
                                            this.localId=param;
                                    

                               }
                            

                        /**
                        * field for InputQuantity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localInputQuantity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInputQuantityTracker = false ;

                           public boolean isInputQuantitySpecified(){
                               return localInputQuantityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getInputQuantity(){
                               return localInputQuantity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InputQuantity
                               */
                               public void setInputQuantity(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localInputQuantityTracker = param != null;
                                   
                                            this.localInputQuantity=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localInternalIdTracker = param != null;
                                   
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for InternalIdNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localInternalIdNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdNumberTracker = false ;

                           public boolean isInternalIdNumberSpecified(){
                               return localInternalIdNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getInternalIdNumber(){
                               return localInternalIdNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalIdNumber
                               */
                               public void setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localInternalIdNumberTracker = param != null;
                                   
                                            this.localInternalIdNumber=param;
                                    

                               }
                            

                        /**
                        * field for LaborResources
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localLaborResources ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLaborResourcesTracker = false ;

                           public boolean isLaborResourcesSpecified(){
                               return localLaborResourcesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getLaborResources(){
                               return localLaborResources;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LaborResources
                               */
                               public void setLaborResources(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localLaborResourcesTracker = param != null;
                                   
                                            this.localLaborResources=param;
                                    

                               }
                            

                        /**
                        * field for LagAmount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localLagAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLagAmountTracker = false ;

                           public boolean isLagAmountSpecified(){
                               return localLagAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getLagAmount(){
                               return localLagAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LagAmount
                               */
                               public void setLagAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localLagAmountTracker = param != null;
                                   
                                            this.localLagAmount=param;
                                    

                               }
                            

                        /**
                        * field for LagType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localLagType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLagTypeTracker = false ;

                           public boolean isLagTypeSpecified(){
                               return localLagTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getLagType(){
                               return localLagType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LagType
                               */
                               public void setLagType(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localLagTypeTracker = param != null;
                                   
                                            this.localLagType=param;
                                    

                               }
                            

                        /**
                        * field for LagUnits
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localLagUnits ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLagUnitsTracker = false ;

                           public boolean isLagUnitsSpecified(){
                               return localLagUnitsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getLagUnits(){
                               return localLagUnits;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LagUnits
                               */
                               public void setLagUnits(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localLagUnitsTracker = param != null;
                                   
                                            this.localLagUnits=param;
                                    

                               }
                            

                        /**
                        * field for MachineResources
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localMachineResources ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMachineResourcesTracker = false ;

                           public boolean isMachineResourcesSpecified(){
                               return localMachineResourcesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getMachineResources(){
                               return localMachineResources;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MachineResources
                               */
                               public void setMachineResources(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localMachineResourcesTracker = param != null;
                                   
                                            this.localMachineResources=param;
                                    

                               }
                            

                        /**
                        * field for ManufacturingCostTemplate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localManufacturingCostTemplate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localManufacturingCostTemplateTracker = false ;

                           public boolean isManufacturingCostTemplateSpecified(){
                               return localManufacturingCostTemplateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getManufacturingCostTemplate(){
                               return localManufacturingCostTemplate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ManufacturingCostTemplate
                               */
                               public void setManufacturingCostTemplate(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localManufacturingCostTemplateTracker = param != null;
                                   
                                            this.localManufacturingCostTemplate=param;
                                    

                               }
                            

                        /**
                        * field for ManufacturingWorkCenter
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localManufacturingWorkCenter ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localManufacturingWorkCenterTracker = false ;

                           public boolean isManufacturingWorkCenterSpecified(){
                               return localManufacturingWorkCenterTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getManufacturingWorkCenter(){
                               return localManufacturingWorkCenter;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ManufacturingWorkCenter
                               */
                               public void setManufacturingWorkCenter(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localManufacturingWorkCenterTracker = param != null;
                                   
                                            this.localManufacturingWorkCenter=param;
                                    

                               }
                            

                        /**
                        * field for Name
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNameTracker = false ;

                           public boolean isNameSpecified(){
                               return localNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getName(){
                               return localName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Name
                               */
                               public void setName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localNameTracker = param != null;
                                   
                                            this.localName=param;
                                    

                               }
                            

                        /**
                        * field for Order
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localOrder ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOrderTracker = false ;

                           public boolean isOrderSpecified(){
                               return localOrderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getOrder(){
                               return localOrder;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Order
                               */
                               public void setOrder(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localOrderTracker = param != null;
                                   
                                            this.localOrder=param;
                                    

                               }
                            

                        /**
                        * field for Predecessor
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localPredecessor ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPredecessorTracker = false ;

                           public boolean isPredecessorSpecified(){
                               return localPredecessorTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getPredecessor(){
                               return localPredecessor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Predecessor
                               */
                               public void setPredecessor(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localPredecessorTracker = param != null;
                                   
                                            this.localPredecessor=param;
                                    

                               }
                            

                        /**
                        * field for RemainingQuantity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localRemainingQuantity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRemainingQuantityTracker = false ;

                           public boolean isRemainingQuantitySpecified(){
                               return localRemainingQuantityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getRemainingQuantity(){
                               return localRemainingQuantity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RemainingQuantity
                               */
                               public void setRemainingQuantity(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localRemainingQuantityTracker = param != null;
                                   
                                            this.localRemainingQuantity=param;
                                    

                               }
                            

                        /**
                        * field for RunRate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localRunRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRunRateTracker = false ;

                           public boolean isRunRateSpecified(){
                               return localRunRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getRunRate(){
                               return localRunRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RunRate
                               */
                               public void setRunRate(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localRunRateTracker = param != null;
                                   
                                            this.localRunRate=param;
                                    

                               }
                            

                        /**
                        * field for RunTime
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localRunTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRunTimeTracker = false ;

                           public boolean isRunTimeSpecified(){
                               return localRunTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getRunTime(){
                               return localRunTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RunTime
                               */
                               public void setRunTime(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localRunTimeTracker = param != null;
                                   
                                            this.localRunTime=param;
                                    

                               }
                            

                        /**
                        * field for Sequence
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localSequence ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSequenceTracker = false ;

                           public boolean isSequenceSpecified(){
                               return localSequenceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getSequence(){
                               return localSequence;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Sequence
                               */
                               public void setSequence(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localSequenceTracker = param != null;
                                   
                                            this.localSequence=param;
                                    

                               }
                            

                        /**
                        * field for SetupTime
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localSetupTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSetupTimeTracker = false ;

                           public boolean isSetupTimeSpecified(){
                               return localSetupTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getSetupTime(){
                               return localSetupTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SetupTime
                               */
                               public void setSetupTime(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localSetupTimeTracker = param != null;
                                   
                                            this.localSetupTime=param;
                                    

                               }
                            

                        /**
                        * field for StartDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateTracker = false ;

                           public boolean isStartDateSpecified(){
                               return localStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getStartDate(){
                               return localStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDate
                               */
                               public void setStartDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localStartDateTracker = param != null;
                                   
                                            this.localStartDate=param;
                                    

                               }
                            

                        /**
                        * field for Status
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStatusTracker = false ;

                           public boolean isStatusSpecified(){
                               return localStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getStatus(){
                               return localStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Status
                               */
                               public void setStatus(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localStatusTracker = param != null;
                                   
                                            this.localStatus=param;
                                    

                               }
                            

                        /**
                        * field for WorkOrder
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localWorkOrder ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWorkOrderTracker = false ;

                           public boolean isWorkOrderSpecified(){
                               return localWorkOrderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getWorkOrder(){
                               return localWorkOrder;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param WorkOrder
                               */
                               public void setWorkOrder(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localWorkOrderTracker = param != null;
                                   
                                            this.localWorkOrder=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ManufacturingOperationTaskSearchBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ManufacturingOperationTaskSearchBasic",
                           xmlWriter);
                   }

                if (localActualRunTimeTracker){
                                            if (localActualRunTime==null){
                                                 throw new org.apache.axis2.databinding.ADBException("actualRunTime cannot be null!!");
                                            }
                                           localActualRunTime.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","actualRunTime"),
                                               xmlWriter);
                                        } if (localActualSetupTimeTracker){
                                            if (localActualSetupTime==null){
                                                 throw new org.apache.axis2.databinding.ADBException("actualSetupTime cannot be null!!");
                                            }
                                           localActualSetupTime.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","actualSetupTime"),
                                               xmlWriter);
                                        } if (localCompletedQuantityTracker){
                                            if (localCompletedQuantity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("completedQuantity cannot be null!!");
                                            }
                                           localCompletedQuantity.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","completedQuantity"),
                                               xmlWriter);
                                        } if (localEndDateTracker){
                                            if (localEndDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                            }
                                           localEndDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endDate"),
                                               xmlWriter);
                                        } if (localEstimatedWorkTracker){
                                            if (localEstimatedWork==null){
                                                 throw new org.apache.axis2.databinding.ADBException("estimatedWork cannot be null!!");
                                            }
                                           localEstimatedWork.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedWork"),
                                               xmlWriter);
                                        } if (localExternalIdTracker){
                                            if (localExternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                            }
                                           localExternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId"),
                                               xmlWriter);
                                        } if (localExternalIdStringTracker){
                                            if (localExternalIdString==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                            }
                                           localExternalIdString.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString"),
                                               xmlWriter);
                                        } if (localIdTracker){
                                            if (localId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("id cannot be null!!");
                                            }
                                           localId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","id"),
                                               xmlWriter);
                                        } if (localInputQuantityTracker){
                                            if (localInputQuantity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("inputQuantity cannot be null!!");
                                            }
                                           localInputQuantity.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","inputQuantity"),
                                               xmlWriter);
                                        } if (localInternalIdTracker){
                                            if (localInternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                            }
                                           localInternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                               xmlWriter);
                                        } if (localInternalIdNumberTracker){
                                            if (localInternalIdNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                            }
                                           localInternalIdNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber"),
                                               xmlWriter);
                                        } if (localLaborResourcesTracker){
                                            if (localLaborResources==null){
                                                 throw new org.apache.axis2.databinding.ADBException("laborResources cannot be null!!");
                                            }
                                           localLaborResources.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","laborResources"),
                                               xmlWriter);
                                        } if (localLagAmountTracker){
                                            if (localLagAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lagAmount cannot be null!!");
                                            }
                                           localLagAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lagAmount"),
                                               xmlWriter);
                                        } if (localLagTypeTracker){
                                            if (localLagType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lagType cannot be null!!");
                                            }
                                           localLagType.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lagType"),
                                               xmlWriter);
                                        } if (localLagUnitsTracker){
                                            if (localLagUnits==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lagUnits cannot be null!!");
                                            }
                                           localLagUnits.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lagUnits"),
                                               xmlWriter);
                                        } if (localMachineResourcesTracker){
                                            if (localMachineResources==null){
                                                 throw new org.apache.axis2.databinding.ADBException("machineResources cannot be null!!");
                                            }
                                           localMachineResources.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","machineResources"),
                                               xmlWriter);
                                        } if (localManufacturingCostTemplateTracker){
                                            if (localManufacturingCostTemplate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("manufacturingCostTemplate cannot be null!!");
                                            }
                                           localManufacturingCostTemplate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","manufacturingCostTemplate"),
                                               xmlWriter);
                                        } if (localManufacturingWorkCenterTracker){
                                            if (localManufacturingWorkCenter==null){
                                                 throw new org.apache.axis2.databinding.ADBException("manufacturingWorkCenter cannot be null!!");
                                            }
                                           localManufacturingWorkCenter.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","manufacturingWorkCenter"),
                                               xmlWriter);
                                        } if (localNameTracker){
                                            if (localName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                            }
                                           localName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","name"),
                                               xmlWriter);
                                        } if (localOrderTracker){
                                            if (localOrder==null){
                                                 throw new org.apache.axis2.databinding.ADBException("order cannot be null!!");
                                            }
                                           localOrder.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","order"),
                                               xmlWriter);
                                        } if (localPredecessorTracker){
                                            if (localPredecessor==null){
                                                 throw new org.apache.axis2.databinding.ADBException("predecessor cannot be null!!");
                                            }
                                           localPredecessor.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","predecessor"),
                                               xmlWriter);
                                        } if (localRemainingQuantityTracker){
                                            if (localRemainingQuantity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("remainingQuantity cannot be null!!");
                                            }
                                           localRemainingQuantity.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","remainingQuantity"),
                                               xmlWriter);
                                        } if (localRunRateTracker){
                                            if (localRunRate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("runRate cannot be null!!");
                                            }
                                           localRunRate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","runRate"),
                                               xmlWriter);
                                        } if (localRunTimeTracker){
                                            if (localRunTime==null){
                                                 throw new org.apache.axis2.databinding.ADBException("runTime cannot be null!!");
                                            }
                                           localRunTime.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","runTime"),
                                               xmlWriter);
                                        } if (localSequenceTracker){
                                            if (localSequence==null){
                                                 throw new org.apache.axis2.databinding.ADBException("sequence cannot be null!!");
                                            }
                                           localSequence.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","sequence"),
                                               xmlWriter);
                                        } if (localSetupTimeTracker){
                                            if (localSetupTime==null){
                                                 throw new org.apache.axis2.databinding.ADBException("setupTime cannot be null!!");
                                            }
                                           localSetupTime.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","setupTime"),
                                               xmlWriter);
                                        } if (localStartDateTracker){
                                            if (localStartDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                            }
                                           localStartDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDate"),
                                               xmlWriter);
                                        } if (localStatusTracker){
                                            if (localStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                            }
                                           localStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","status"),
                                               xmlWriter);
                                        } if (localWorkOrderTracker){
                                            if (localWorkOrder==null){
                                                 throw new org.apache.axis2.databinding.ADBException("workOrder cannot be null!!");
                                            }
                                           localWorkOrder.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","workOrder"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ManufacturingOperationTaskSearchBasic"));
                 if (localActualRunTimeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "actualRunTime"));
                            
                            
                                    if (localActualRunTime==null){
                                         throw new org.apache.axis2.databinding.ADBException("actualRunTime cannot be null!!");
                                    }
                                    elementList.add(localActualRunTime);
                                } if (localActualSetupTimeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "actualSetupTime"));
                            
                            
                                    if (localActualSetupTime==null){
                                         throw new org.apache.axis2.databinding.ADBException("actualSetupTime cannot be null!!");
                                    }
                                    elementList.add(localActualSetupTime);
                                } if (localCompletedQuantityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "completedQuantity"));
                            
                            
                                    if (localCompletedQuantity==null){
                                         throw new org.apache.axis2.databinding.ADBException("completedQuantity cannot be null!!");
                                    }
                                    elementList.add(localCompletedQuantity);
                                } if (localEndDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "endDate"));
                            
                            
                                    if (localEndDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                    }
                                    elementList.add(localEndDate);
                                } if (localEstimatedWorkTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "estimatedWork"));
                            
                            
                                    if (localEstimatedWork==null){
                                         throw new org.apache.axis2.databinding.ADBException("estimatedWork cannot be null!!");
                                    }
                                    elementList.add(localEstimatedWork);
                                } if (localExternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalId"));
                            
                            
                                    if (localExternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                    }
                                    elementList.add(localExternalId);
                                } if (localExternalIdStringTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalIdString"));
                            
                            
                                    if (localExternalIdString==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                    }
                                    elementList.add(localExternalIdString);
                                } if (localIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "id"));
                            
                            
                                    if (localId==null){
                                         throw new org.apache.axis2.databinding.ADBException("id cannot be null!!");
                                    }
                                    elementList.add(localId);
                                } if (localInputQuantityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "inputQuantity"));
                            
                            
                                    if (localInputQuantity==null){
                                         throw new org.apache.axis2.databinding.ADBException("inputQuantity cannot be null!!");
                                    }
                                    elementList.add(localInputQuantity);
                                } if (localInternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalId"));
                            
                            
                                    if (localInternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    }
                                    elementList.add(localInternalId);
                                } if (localInternalIdNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalIdNumber"));
                            
                            
                                    if (localInternalIdNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                    }
                                    elementList.add(localInternalIdNumber);
                                } if (localLaborResourcesTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "laborResources"));
                            
                            
                                    if (localLaborResources==null){
                                         throw new org.apache.axis2.databinding.ADBException("laborResources cannot be null!!");
                                    }
                                    elementList.add(localLaborResources);
                                } if (localLagAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lagAmount"));
                            
                            
                                    if (localLagAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("lagAmount cannot be null!!");
                                    }
                                    elementList.add(localLagAmount);
                                } if (localLagTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lagType"));
                            
                            
                                    if (localLagType==null){
                                         throw new org.apache.axis2.databinding.ADBException("lagType cannot be null!!");
                                    }
                                    elementList.add(localLagType);
                                } if (localLagUnitsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lagUnits"));
                            
                            
                                    if (localLagUnits==null){
                                         throw new org.apache.axis2.databinding.ADBException("lagUnits cannot be null!!");
                                    }
                                    elementList.add(localLagUnits);
                                } if (localMachineResourcesTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "machineResources"));
                            
                            
                                    if (localMachineResources==null){
                                         throw new org.apache.axis2.databinding.ADBException("machineResources cannot be null!!");
                                    }
                                    elementList.add(localMachineResources);
                                } if (localManufacturingCostTemplateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "manufacturingCostTemplate"));
                            
                            
                                    if (localManufacturingCostTemplate==null){
                                         throw new org.apache.axis2.databinding.ADBException("manufacturingCostTemplate cannot be null!!");
                                    }
                                    elementList.add(localManufacturingCostTemplate);
                                } if (localManufacturingWorkCenterTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "manufacturingWorkCenter"));
                            
                            
                                    if (localManufacturingWorkCenter==null){
                                         throw new org.apache.axis2.databinding.ADBException("manufacturingWorkCenter cannot be null!!");
                                    }
                                    elementList.add(localManufacturingWorkCenter);
                                } if (localNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "name"));
                            
                            
                                    if (localName==null){
                                         throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                    }
                                    elementList.add(localName);
                                } if (localOrderTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "order"));
                            
                            
                                    if (localOrder==null){
                                         throw new org.apache.axis2.databinding.ADBException("order cannot be null!!");
                                    }
                                    elementList.add(localOrder);
                                } if (localPredecessorTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "predecessor"));
                            
                            
                                    if (localPredecessor==null){
                                         throw new org.apache.axis2.databinding.ADBException("predecessor cannot be null!!");
                                    }
                                    elementList.add(localPredecessor);
                                } if (localRemainingQuantityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "remainingQuantity"));
                            
                            
                                    if (localRemainingQuantity==null){
                                         throw new org.apache.axis2.databinding.ADBException("remainingQuantity cannot be null!!");
                                    }
                                    elementList.add(localRemainingQuantity);
                                } if (localRunRateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "runRate"));
                            
                            
                                    if (localRunRate==null){
                                         throw new org.apache.axis2.databinding.ADBException("runRate cannot be null!!");
                                    }
                                    elementList.add(localRunRate);
                                } if (localRunTimeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "runTime"));
                            
                            
                                    if (localRunTime==null){
                                         throw new org.apache.axis2.databinding.ADBException("runTime cannot be null!!");
                                    }
                                    elementList.add(localRunTime);
                                } if (localSequenceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "sequence"));
                            
                            
                                    if (localSequence==null){
                                         throw new org.apache.axis2.databinding.ADBException("sequence cannot be null!!");
                                    }
                                    elementList.add(localSequence);
                                } if (localSetupTimeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "setupTime"));
                            
                            
                                    if (localSetupTime==null){
                                         throw new org.apache.axis2.databinding.ADBException("setupTime cannot be null!!");
                                    }
                                    elementList.add(localSetupTime);
                                } if (localStartDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "startDate"));
                            
                            
                                    if (localStartDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                    }
                                    elementList.add(localStartDate);
                                } if (localStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "status"));
                            
                            
                                    if (localStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                    }
                                    elementList.add(localStatus);
                                } if (localWorkOrderTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "workOrder"));
                            
                            
                                    if (localWorkOrder==null){
                                         throw new org.apache.axis2.databinding.ADBException("workOrder cannot be null!!");
                                    }
                                    elementList.add(localWorkOrder);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ManufacturingOperationTaskSearchBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ManufacturingOperationTaskSearchBasic object =
                new ManufacturingOperationTaskSearchBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ManufacturingOperationTaskSearchBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ManufacturingOperationTaskSearchBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","actualRunTime").equals(reader.getName())){
                                
                                                object.setActualRunTime(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","actualSetupTime").equals(reader.getName())){
                                
                                                object.setActualSetupTime(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","completedQuantity").equals(reader.getName())){
                                
                                                object.setCompletedQuantity(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endDate").equals(reader.getName())){
                                
                                                object.setEndDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedWork").equals(reader.getName())){
                                
                                                object.setEstimatedWork(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                
                                                object.setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString").equals(reader.getName())){
                                
                                                object.setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","id").equals(reader.getName())){
                                
                                                object.setId(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","inputQuantity").equals(reader.getName())){
                                
                                                object.setInputQuantity(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                                object.setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber").equals(reader.getName())){
                                
                                                object.setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","laborResources").equals(reader.getName())){
                                
                                                object.setLaborResources(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lagAmount").equals(reader.getName())){
                                
                                                object.setLagAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lagType").equals(reader.getName())){
                                
                                                object.setLagType(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lagUnits").equals(reader.getName())){
                                
                                                object.setLagUnits(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","machineResources").equals(reader.getName())){
                                
                                                object.setMachineResources(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","manufacturingCostTemplate").equals(reader.getName())){
                                
                                                object.setManufacturingCostTemplate(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","manufacturingWorkCenter").equals(reader.getName())){
                                
                                                object.setManufacturingWorkCenter(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","name").equals(reader.getName())){
                                
                                                object.setName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","order").equals(reader.getName())){
                                
                                                object.setOrder(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","predecessor").equals(reader.getName())){
                                
                                                object.setPredecessor(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","remainingQuantity").equals(reader.getName())){
                                
                                                object.setRemainingQuantity(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","runRate").equals(reader.getName())){
                                
                                                object.setRunRate(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","runTime").equals(reader.getName())){
                                
                                                object.setRunTime(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","sequence").equals(reader.getName())){
                                
                                                object.setSequence(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","setupTime").equals(reader.getName())){
                                
                                                object.setSetupTime(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDate").equals(reader.getName())){
                                
                                                object.setStartDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","status").equals(reader.getName())){
                                
                                                object.setStatus(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","workOrder").equals(reader.getName())){
                                
                                                object.setWorkOrder(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    