
/**
 * PermissionCode.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2.types;
            

            /**
            *  PermissionCode bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class PermissionCode
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.common_2017_2.platform.webservices.netsuite.com",
                "PermissionCode",
                "ns6");

            

                        /**
                        * field for PermissionCode
                        */

                        
                                    protected java.lang.String localPermissionCode ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected PermissionCode(java.lang.String value, boolean isRegisterValue) {
                                    localPermissionCode = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localPermissionCode, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String __accessPaymentAuditLog =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_accessPaymentAuditLog");
                                
                                    public static final java.lang.String __accessTokenManagement =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_accessTokenManagement");
                                
                                    public static final java.lang.String __accountDetail =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_accountDetail");
                                
                                    public static final java.lang.String __accounting =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_accounting");
                                
                                    public static final java.lang.String __accountingBook =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_accountingBook");
                                
                                    public static final java.lang.String __accountingLists =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_accountingLists");
                                
                                    public static final java.lang.String __accounts =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_accounts");
                                
                                    public static final java.lang.String __accountsPayable =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_accountsPayable");
                                
                                    public static final java.lang.String __accountsPayableGraphing =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_accountsPayableGraphing");
                                
                                    public static final java.lang.String __accountsPayableRegister =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_accountsPayableRegister");
                                
                                    public static final java.lang.String __accountsReceivable =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_accountsReceivable");
                                
                                    public static final java.lang.String __accountsReceivableGraphing =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_accountsReceivableGraphing");
                                
                                    public static final java.lang.String __accountsReceivableRegister =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_accountsReceivableRegister");
                                
                                    public static final java.lang.String __accountsReceivableUnbilled =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_accountsReceivableUnbilled");
                                
                                    public static final java.lang.String __adjustInventory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_adjustInventory");
                                
                                    public static final java.lang.String __adjustInventoryWorksheet =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_adjustInventoryWorksheet");
                                
                                    public static final java.lang.String __admindocs =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_admindocs");
                                
                                    public static final java.lang.String __adpImportData =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_adpImportData");
                                
                                    public static final java.lang.String __adpSetup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_adpSetup");
                                
                                    public static final java.lang.String __advancedAnalytics =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_advancedAnalytics");
                                
                                    public static final java.lang.String __advancedGovernmentIssuedIds =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_advancedGovernmentIssuedIds");
                                
                                    public static final java.lang.String __advancedOrderManagement =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_advancedOrderManagement");
                                
                                    public static final java.lang.String __advancedPDFHTMLTemplates =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_advancedPDFHTMLTemplates");
                                
                                    public static final java.lang.String __allocationSchedules =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_allocationSchedules");
                                
                                    public static final java.lang.String __allowNonGLChanges =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_allowNonGLChanges");
                                
                                    public static final java.lang.String __allowPendingBookJournalEntry =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_allowPendingBookJournalEntry");
                                
                                    public static final java.lang.String __amendW4 =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_amendW4");
                                
                                    public static final java.lang.String __amortizationReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_amortizationReports");
                                
                                    public static final java.lang.String __amortizationSchedules =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_amortizationSchedules");
                                
                                    public static final java.lang.String __applicationPublishers =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_applicationPublishers");
                                
                                    public static final java.lang.String __approveDirectDeposit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_approveDirectDeposit");
                                
                                    public static final java.lang.String __approveEFT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_approveEFT");
                                
                                    public static final java.lang.String __approveOnlineBillPayments =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_approveOnlineBillPayments");
                                
                                    public static final java.lang.String __approveVendorPayments =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_approveVendorPayments");
                                
                                    public static final java.lang.String __auditTrail =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_auditTrail");
                                
                                    public static final java.lang.String __backupYourData =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_backupYourData");
                                
                                    public static final java.lang.String __balanceLocationCostingGroupAccounts =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_balanceLocationCostingGroupAccounts");
                                
                                    public static final java.lang.String __balanceSheet =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_balanceSheet");
                                
                                    public static final java.lang.String __bankAccountRegisters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bankAccountRegisters");
                                
                                    public static final java.lang.String __basicGovernmentIssuedIds =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_basicGovernmentIssuedIds");
                                
                                    public static final java.lang.String __billInboundShipment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_billInboundShipment");
                                
                                    public static final java.lang.String __billingInformation =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_billingInformation");
                                
                                    public static final java.lang.String __billingSchedules =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_billingSchedules");
                                
                                    public static final java.lang.String __billOfDistribution =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_billOfDistribution");
                                
                                    public static final java.lang.String __billOfMaterials =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_billOfMaterials");
                                
                                    public static final java.lang.String __billOfMaterialsInquiry =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_billOfMaterialsInquiry");
                                
                                    public static final java.lang.String __billPurchaseOrders =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_billPurchaseOrders");
                                
                                    public static final java.lang.String __bills =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bills");
                                
                                    public static final java.lang.String __billSalesOrders =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_billSalesOrders");
                                
                                    public static final java.lang.String __bins =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bins");
                                
                                    public static final java.lang.String __binTransfer =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_binTransfer");
                                
                                    public static final java.lang.String __binWorksheet =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_binWorksheet");
                                
                                    public static final java.lang.String __blanketPurchaseOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_blanketPurchaseOrder");
                                
                                    public static final java.lang.String __blanketPurchaseOrderApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_blanketPurchaseOrderApproval");
                                
                                    public static final java.lang.String __budget =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_budget");
                                
                                    public static final java.lang.String __buildAssemblies =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_buildAssemblies");
                                
                                    public static final java.lang.String __buildWorkOrders =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_buildWorkOrders");
                                
                                    public static final java.lang.String __calculateTime =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_calculateTime");
                                
                                    public static final java.lang.String __calendar =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_calendar");
                                
                                    public static final java.lang.String __campaignHistory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_campaignHistory");
                                
                                    public static final java.lang.String __caseAlerts =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_caseAlerts");
                                
                                    public static final java.lang.String __cases =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cases");
                                
                                    public static final java.lang.String __cashFlowStatement =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cashFlowStatement");
                                
                                    public static final java.lang.String __cashSale =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cashSale");
                                
                                    public static final java.lang.String __cashSaleRefund =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cashSaleRefund");
                                
                                    public static final java.lang.String __changeEmailOrPassword =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_changeEmailOrPassword");
                                
                                    public static final java.lang.String __changeRole =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_changeRole");
                                
                                    public static final java.lang.String __chargeRule =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_chargeRule");
                                
                                    public static final java.lang.String __chargeRunRules =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_chargeRunRules");
                                
                                    public static final java.lang.String __check =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_check");
                                
                                    public static final java.lang.String __checkItemAvailability =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_checkItemAvailability");
                                
                                    public static final java.lang.String __classes =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_classes");
                                
                                    public static final java.lang.String __classSegmentMapping =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_classSegmentMapping");
                                
                                    public static final java.lang.String __closeAccount =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_closeAccount");
                                
                                    public static final java.lang.String __closeWorkOrders =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_closeWorkOrders");
                                
                                    public static final java.lang.String __colorThemes =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_colorThemes");
                                
                                    public static final java.lang.String __commerceCategories =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_commerceCategories");
                                
                                    public static final java.lang.String __commissionFeatureSetup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_commissionFeatureSetup");
                                
                                    public static final java.lang.String __commissionReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_commissionReports");
                                
                                    public static final java.lang.String __commitOrders =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_commitOrders");
                                
                                    public static final java.lang.String __commitPayroll =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_commitPayroll");
                                
                                    public static final java.lang.String __companies =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_companies");
                                
                                    public static final java.lang.String __companyInformation =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_companyInformation");
                                
                                    public static final java.lang.String __companyPreferences =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_companyPreferences");
                                
                                    public static final java.lang.String __competitors =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_competitors");
                                
                                    public static final java.lang.String __componentWhereUsed =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_componentWhereUsed");
                                
                                    public static final java.lang.String __contactRoles =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_contactRoles");
                                
                                    public static final java.lang.String __contacts =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_contacts");
                                
                                    public static final java.lang.String __controlSuitescriptAndWorkflowTriggersInWebServicesRequest =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_controlSuitescriptAndWorkflowTriggersInWebServicesRequest");
                                
                                    public static final java.lang.String __controlSuitescriptAndWorkflowTriggersPerCsvImport =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_controlSuitescriptAndWorkflowTriggersPerCsvImport");
                                
                                    public static final java.lang.String __convertClassesToDepartments =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_convertClassesToDepartments");
                                
                                    public static final java.lang.String __convertClassesToLocations =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_convertClassesToLocations");
                                
                                    public static final java.lang.String __copyBudgets =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_copyBudgets");
                                
                                    public static final java.lang.String __copyChartOfAccountsToNewCompany =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_copyChartOfAccountsToNewCompany");
                                
                                    public static final java.lang.String __copyProjectTasks =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_copyProjectTasks");
                                
                                    public static final java.lang.String __costedBillOfMaterialsInquiry =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_costedBillOfMaterialsInquiry");
                                
                                    public static final java.lang.String __costOfGoodsSoldRegisters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_costOfGoodsSoldRegisters");
                                
                                    public static final java.lang.String __countInventory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_countInventory");
                                
                                    public static final java.lang.String __createAllocationSchedules =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_createAllocationSchedules");
                                
                                    public static final java.lang.String __createConsolidationCompany =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_createConsolidationCompany");
                                
                                    public static final java.lang.String __createFiscalCalendar =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_createFiscalCalendar");
                                
                                    public static final java.lang.String __createInventoryCounts =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_createInventoryCounts");
                                
                                    public static final java.lang.String __creditCard =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_creditCard");
                                
                                    public static final java.lang.String __creditCardProcessing =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_creditCardProcessing");
                                
                                    public static final java.lang.String __creditCardRefund =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_creditCardRefund");
                                
                                    public static final java.lang.String __creditCardRegisters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_creditCardRegisters");
                                
                                    public static final java.lang.String __creditMemo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_creditMemo");
                                
                                    public static final java.lang.String __creditReturns =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_creditReturns");
                                
                                    public static final java.lang.String __crmGroups =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_crmGroups");
                                
                                    public static final java.lang.String __crmLists =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_crmLists");
                                
                                    public static final java.lang.String __cspSetup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cspSetup");
                                
                                    public static final java.lang.String __currency =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_currency");
                                
                                    public static final java.lang.String __currencyAdjustmentJournal =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_currencyAdjustmentJournal");
                                
                                    public static final java.lang.String __currencyRevaluation =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_currencyRevaluation");
                                
                                    public static final java.lang.String __customAddressForm =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customAddressForm");
                                
                                    public static final java.lang.String __customBodyFields =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customBodyFields");
                                
                                    public static final java.lang.String __customCenterCategories =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customCenterCategories");
                                
                                    public static final java.lang.String __customCenterLinks =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customCenterLinks");
                                
                                    public static final java.lang.String __customCenters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customCenters");
                                
                                    public static final java.lang.String __customCenterTabs =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customCenterTabs");
                                
                                    public static final java.lang.String __customColumnFields =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customColumnFields");
                                
                                    public static final java.lang.String __customEntityFields =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customEntityFields");
                                
                                    public static final java.lang.String __customEntryForms =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customEntryForms");
                                
                                    public static final java.lang.String __customerCharge =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customerCharge");
                                
                                    public static final java.lang.String __customerDeposit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customerDeposit");
                                
                                    public static final java.lang.String __customerPayment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customerPayment");
                                
                                    public static final java.lang.String __customerPaymentAuthorization =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customerPaymentAuthorization");
                                
                                    public static final java.lang.String __customerProfile =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customerProfile");
                                
                                    public static final java.lang.String __customerRefund =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customerRefund");
                                
                                    public static final java.lang.String __customers =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customers");
                                
                                    public static final java.lang.String __customerStatus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customerStatus");
                                
                                    public static final java.lang.String __customEventFields =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customEventFields");
                                
                                    public static final java.lang.String __customFields =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customFields");
                                
                                    public static final java.lang.String __customGlLinesPlugInAuditLog =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customGlLinesPlugInAuditLog");
                                
                                    public static final java.lang.String __customGlLinesPlugInAuditLogSegments =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customGlLinesPlugInAuditLogSegments");
                                
                                    public static final java.lang.String __customHTMLLayouts =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customHTMLLayouts");
                                
                                    public static final java.lang.String __customItemFields =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customItemFields");
                                
                                    public static final java.lang.String __customItemNumberFields =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customItemNumberFields");
                                
                                    public static final java.lang.String __customizePage =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customizePage");
                                
                                    public static final java.lang.String __customLists =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customLists");
                                
                                    public static final java.lang.String __customPDFLayouts =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customPDFLayouts");
                                
                                    public static final java.lang.String __customRecordEntries =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customRecordEntries");
                                
                                    public static final java.lang.String __customRecordTypes =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customRecordTypes");
                                
                                    public static final java.lang.String __customSegments =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customSegments");
                                
                                    public static final java.lang.String __customSublist =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customSublist");
                                
                                    public static final java.lang.String __customSublists =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customSublists");
                                
                                    public static final java.lang.String __customSubtabs =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customSubtabs");
                                
                                    public static final java.lang.String __customTransactionFields =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customTransactionFields");
                                
                                    public static final java.lang.String __customTransactionForms =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customTransactionForms");
                                
                                    public static final java.lang.String __customTransactionTypes =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_customTransactionTypes");
                                
                                    public static final java.lang.String __deferredExpenseRegisters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_deferredExpenseRegisters");
                                
                                    public static final java.lang.String __deferredExpenseReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_deferredExpenseReports");
                                
                                    public static final java.lang.String __deferredRevenueRegisters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_deferredRevenueRegisters");
                                
                                    public static final java.lang.String __deleteAllData =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_deleteAllData");
                                
                                    public static final java.lang.String __deletedRecords =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_deletedRecords");
                                
                                    public static final java.lang.String __deleteEvent =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_deleteEvent");
                                
                                    public static final java.lang.String __departments =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_departments");
                                
                                    public static final java.lang.String __departmentSegmentMapping =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_departmentSegmentMapping");
                                
                                    public static final java.lang.String __deposit =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_deposit");
                                
                                    public static final java.lang.String __depositApplication =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_depositApplication");
                                
                                    public static final java.lang.String __deviceIdManagement =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_deviceIdManagement");
                                
                                    public static final java.lang.String __directDepositStatus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_directDepositStatus");
                                
                                    public static final java.lang.String __distributeInventory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_distributeInventory");
                                
                                    public static final java.lang.String __distributionNetwork =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_distributionNetwork");
                                
                                    public static final java.lang.String __documentsAndFiles =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_documentsAndFiles");
                                
                                    public static final java.lang.String __duplicateCaseManagement =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_duplicateCaseManagement");
                                
                                    public static final java.lang.String __duplicateDetectionSetup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_duplicateDetectionSetup");
                                
                                    public static final java.lang.String __duplicateRecordManagement =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_duplicateRecordManagement");
                                
                                    public static final java.lang.String __ebayExportImport =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ebayExportImport");
                                
                                    public static final java.lang.String __editForecast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_editForecast");
                                
                                    public static final java.lang.String __editManagerForecast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_editManagerForecast");
                                
                                    public static final java.lang.String __editProfile =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_editProfile");
                                
                                    public static final java.lang.String __eftStatus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_eftStatus");
                                
                                    public static final java.lang.String __emailReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_emailReports");
                                
                                    public static final java.lang.String __emailTemplate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_emailTemplate");
                                
                                    public static final java.lang.String __employeeAdministration =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_employeeAdministration");
                                
                                    public static final java.lang.String __employeeCenterPublishing =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_employeeCenterPublishing");
                                
                                    public static final java.lang.String __employeeChangeReason =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_employeeChangeReason");
                                
                                    public static final java.lang.String __employeeCommissionSchedulesPlans =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_employeeCommissionSchedulesPlans");
                                
                                    public static final java.lang.String __employeeCommissionTransaction =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_employeeCommissionTransaction");
                                
                                    public static final java.lang.String __employeeCommissionTransactionApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_employeeCommissionTransactionApproval");
                                
                                    public static final java.lang.String __employeeConfidential =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_employeeConfidential");
                                
                                    public static final java.lang.String __employeeEffectiveDating =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_employeeEffectiveDating");
                                
                                    public static final java.lang.String __employeeNavigation =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_employeeNavigation");
                                
                                    public static final java.lang.String __employeePublic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_employeePublic");
                                
                                    public static final java.lang.String __employeeReminders =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_employeeReminders");
                                
                                    public static final java.lang.String __employees =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_employees");
                                
                                    public static final java.lang.String __employeeSearch =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_employeeSearch");
                                
                                    public static final java.lang.String __employeeSocialSecurityNumbers =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_employeeSocialSecurityNumbers");
                                
                                    public static final java.lang.String __enableFeatures =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_enableFeatures");
                                
                                    public static final java.lang.String __enterCompletions =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_enterCompletions");
                                
                                    public static final java.lang.String __enterOpeningBalances =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_enterOpeningBalances");
                                
                                    public static final java.lang.String __enterVendorCredits =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_enterVendorCredits");
                                
                                    public static final java.lang.String __enterYearToDatePayrollAdjustments =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_enterYearToDatePayrollAdjustments");
                                
                                    public static final java.lang.String __entityAccountMapping =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_entityAccountMapping");
                                
                                    public static final java.lang.String __equityRegisters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_equityRegisters");
                                
                                    public static final java.lang.String __escalationAssignment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_escalationAssignment");
                                
                                    public static final java.lang.String __escalationAssignmentRule =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_escalationAssignmentRule");
                                
                                    public static final java.lang.String __establishQuotas =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_establishQuotas");
                                
                                    public static final java.lang.String __estimate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_estimate");
                                
                                    public static final java.lang.String __events =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_events");
                                
                                    public static final java.lang.String __expenseCategories =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_expenseCategories");
                                
                                    public static final java.lang.String __expenseRegisters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_expenseRegisters");
                                
                                    public static final java.lang.String __expenseReport =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_expenseReport");
                                
                                    public static final java.lang.String __expenses =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_expenses");
                                
                                    public static final java.lang.String __exportAsIIF =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_exportAsIIF");
                                
                                    public static final java.lang.String __exportLists =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_exportLists");
                                
                                    public static final java.lang.String __fairValueDimension =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_fairValueDimension");
                                
                                    public static final java.lang.String __fairValueFormula =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_fairValueFormula");
                                
                                    public static final java.lang.String __fairValuePrice =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_fairValuePrice");
                                
                                    public static final java.lang.String __faxMessages =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_faxMessages");
                                
                                    public static final java.lang.String __faxTemplate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_faxTemplate");
                                
                                    public static final java.lang.String __features =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_features");
                                
                                    public static final java.lang.String __financeCharge =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_financeCharge");
                                
                                    public static final java.lang.String __financeChargePreferences =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_financeChargePreferences");
                                
                                    public static final java.lang.String __financialHistory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_financialHistory");
                                
                                    public static final java.lang.String __financialStatementLayouts =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_financialStatementLayouts");
                                
                                    public static final java.lang.String __financialStatements =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_financialStatements");
                                
                                    public static final java.lang.String __financialStatementSections =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_financialStatementSections");
                                
                                    public static final java.lang.String __findTransaction =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_findTransaction");
                                
                                    public static final java.lang.String __fiscalCalendars =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_fiscalCalendars");
                                
                                    public static final java.lang.String __fixedAssetRegisters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_fixedAssetRegisters");
                                
                                    public static final java.lang.String __foreignCurrencyVarianceMapping =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_foreignCurrencyVarianceMapping");
                                
                                    public static final java.lang.String __form1099FederalMiscellaneousIncome =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_form1099FederalMiscellaneousIncome");
                                
                                    public static final java.lang.String __form940EmployersAnnualFederalUnemploymentTaxReturn =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_form940EmployersAnnualFederalUnemploymentTaxReturn");
                                
                                    public static final java.lang.String __form941EmployersQuarterlyFederalTaxReturn =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_form941EmployersQuarterlyFederalTaxReturn");
                                
                                    public static final java.lang.String __formW2WageAndTaxStatement =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_formW2WageAndTaxStatement");
                                
                                    public static final java.lang.String __formW4EmployeesWithholdingAllowanceCertificate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_formW4EmployeesWithholdingAllowanceCertificate");
                                
                                    public static final java.lang.String __fulfillmentRequest =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_fulfillmentRequest");
                                
                                    public static final java.lang.String __fulfillOrders =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_fulfillOrders");
                                
                                    public static final java.lang.String __generalLedger =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_generalLedger");
                                
                                    public static final java.lang.String __generatePriceLists =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_generatePriceLists");
                                
                                    public static final java.lang.String __generateRevenueCommitment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_generateRevenueCommitment");
                                
                                    public static final java.lang.String __generateRevenueCommitmentReversals =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_generateRevenueCommitmentReversals");
                                
                                    public static final java.lang.String __generateSingleOrderRevenueContracts =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_generateSingleOrderRevenueContracts");
                                
                                    public static final java.lang.String __generateStatements =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_generateStatements");
                                
                                    public static final java.lang.String __genericAdminPermission =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_genericAdminPermission");
                                
                                    public static final java.lang.String __genericResources =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_genericResources");
                                
                                    public static final java.lang.String __globalAccountMapping =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_globalAccountMapping");
                                
                                    public static final java.lang.String __governmentIssuedIdTypes =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_governmentIssuedIdTypes");
                                
                                    public static final java.lang.String __grantingAccessToReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_grantingAccessToReports");
                                
                                    public static final java.lang.String __gstSummaryReport =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gstSummaryReport");
                                
                                    public static final java.lang.String __hideEmployeeInformationOnFinancialReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_hideEmployeeInformationOnFinancialReports");
                                
                                    public static final java.lang.String __importCSVFile =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_importCSVFile");
                                
                                    public static final java.lang.String __importOnlineBankingQIFFile =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_importOnlineBankingQIFFile");
                                
                                    public static final java.lang.String __importStateSalesTax =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_importStateSalesTax");
                                
                                    public static final java.lang.String __inboundShipment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_inboundShipment");
                                
                                    public static final java.lang.String __income =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_income");
                                
                                    public static final java.lang.String __incomeRegisters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_incomeRegisters");
                                
                                    public static final java.lang.String __incomeStatement =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_incomeStatement");
                                
                                    public static final java.lang.String __individualPaycheck =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_individualPaycheck");
                                
                                    public static final java.lang.String __integration =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_integration");
                                
                                    public static final java.lang.String __integrationApplication =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_integrationApplication");
                                
                                    public static final java.lang.String __integrationApplications =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_integrationApplications");
                                
                                    public static final java.lang.String __intercompanyAdjustments =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_intercompanyAdjustments");
                                
                                    public static final java.lang.String __internalPublisher =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_internalPublisher");
                                
                                    public static final java.lang.String __inventory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_inventory");
                                
                                    public static final java.lang.String __inventoryCostTemplate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_inventoryCostTemplate");
                                
                                    public static final java.lang.String __inventoryStatus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_inventoryStatus");
                                
                                    public static final java.lang.String __inventoryStatusChange =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_inventoryStatusChange");
                                
                                    public static final java.lang.String __invoice =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_invoice");
                                
                                    public static final java.lang.String __invoiceApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_invoiceApproval");
                                
                                    public static final java.lang.String __issueComponents =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_issueComponents");
                                
                                    public static final java.lang.String __issueReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_issueReports");
                                
                                    public static final java.lang.String __issues =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_issues");
                                
                                    public static final java.lang.String __issueSetup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_issueSetup");
                                
                                    public static final java.lang.String __itemAccountMapping =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_itemAccountMapping");
                                
                                    public static final java.lang.String __itemCategoryLayouts =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_itemCategoryLayouts");
                                
                                    public static final java.lang.String __itemDemandPlan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_itemDemandPlan");
                                
                                    public static final java.lang.String __itemFulfillment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_itemFulfillment");
                                
                                    public static final java.lang.String __itemReceipt =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_itemReceipt");
                                
                                    public static final java.lang.String __itemRevenueCategory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_itemRevenueCategory");
                                
                                    public static final java.lang.String __itemRevisions =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_itemRevisions");
                                
                                    public static final java.lang.String __items =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_items");
                                
                                    public static final java.lang.String __itemSupplyPlan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_itemSupplyPlan");
                                
                                    public static final java.lang.String __itemTemplates =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_itemTemplates");
                                
                                    public static final java.lang.String __jobManagement =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_jobManagement");
                                
                                    public static final java.lang.String __jobRequisitions =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_jobRequisitions");
                                
                                    public static final java.lang.String __jobs =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_jobs");
                                
                                    public static final java.lang.String __journalApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_journalApproval");
                                
                                    public static final java.lang.String __knowledgeBase =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_knowledgeBase");
                                
                                    public static final java.lang.String __kpiScorecards =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kpiScorecards");
                                
                                    public static final java.lang.String __kudos =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kudos");
                                
                                    public static final java.lang.String __leadConversion =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_leadConversion");
                                
                                    public static final java.lang.String __leadConversionMapping =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_leadConversionMapping");
                                
                                    public static final java.lang.String __leadSnapshotReminders =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_leadSnapshotReminders");
                                
                                    public static final java.lang.String __letterMessages =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_letterMessages");
                                
                                    public static final java.lang.String __letterTemplate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_letterTemplate");
                                
                                    public static final java.lang.String __loadSampleData =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_loadSampleData");
                                
                                    public static final java.lang.String __locationCostingGroup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_locationCostingGroup");
                                
                                    public static final java.lang.String __locations =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_locations");
                                
                                    public static final java.lang.String __locationSegmentMapping =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_locationSegmentMapping");
                                
                                    public static final java.lang.String __lockTransactions =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lockTransactions");
                                
                                    public static final java.lang.String __logInUsingAccessTokens =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_logInUsingAccessTokens");
                                
                                    public static final java.lang.String __longTermLiabilityRegisters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_longTermLiabilityRegisters");
                                
                                    public static final java.lang.String __mailMerge =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mailMerge");
                                
                                    public static final java.lang.String __makeJournalEntry =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_makeJournalEntry");
                                
                                    public static final java.lang.String __manageAccountingPeriods =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_manageAccountingPeriods");
                                
                                    public static final java.lang.String __manageCustomPermissions =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_manageCustomPermissions");
                                
                                    public static final java.lang.String __managePayroll =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_managePayroll");
                                
                                    public static final java.lang.String __manageRoles =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_manageRoles");
                                
                                    public static final java.lang.String __manageTaxReportingPeriods =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_manageTaxReportingPeriods");
                                
                                    public static final java.lang.String __manageUsers =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_manageUsers");
                                
                                    public static final java.lang.String __manufacturingCostTemplate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_manufacturingCostTemplate");
                                
                                    public static final java.lang.String __manufacturingRouting =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_manufacturingRouting");
                                
                                    public static final java.lang.String __marketingCampaignReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_marketingCampaignReports");
                                
                                    public static final java.lang.String __marketingCampaigns =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_marketingCampaigns");
                                
                                    public static final java.lang.String __marketingTemplate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_marketingTemplate");
                                
                                    public static final java.lang.String __markIssueAsShowStopper =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_markIssueAsShowStopper");
                                
                                    public static final java.lang.String __markWorkOrdersBuilt =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_markWorkOrdersBuilt");
                                
                                    public static final java.lang.String __markWorkOrdersFirmed =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_markWorkOrdersFirmed");
                                
                                    public static final java.lang.String __markWorkOrdersReleased =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_markWorkOrdersReleased");
                                
                                    public static final java.lang.String __massUpdates =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_massUpdates");
                                
                                    public static final java.lang.String __matchingRulesForOnlineBanking =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_matchingRulesForOnlineBanking");
                                
                                    public static final java.lang.String __mediaFolders =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mediaFolders");
                                
                                    public static final java.lang.String __memorizedTransactions =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_memorizedTransactions");
                                
                                    public static final java.lang.String __mobileDeviceAccess =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mobileDeviceAccess");
                                
                                    public static final java.lang.String __netWorth =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_netWorth");
                                
                                    public static final java.lang.String __nextGenAnalytics =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_nextGenAnalytics");
                                
                                    public static final java.lang.String __nonPostingRegisters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_nonPostingRegisters");
                                
                                    public static final java.lang.String __noPermissionNecessary =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_noPermissionNecessary");
                                
                                    public static final java.lang.String __notesTab =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_notesTab");
                                
                                    public static final java.lang.String __notifications =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_notifications");
                                
                                    public static final java.lang.String __offlineClient =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_offlineClient");
                                
                                    public static final java.lang.String __onlineCaseForm =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_onlineCaseForm");
                                
                                    public static final java.lang.String __onlineCustomerForm =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_onlineCustomerForm");
                                
                                    public static final java.lang.String __onlineCustomRecordForm =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_onlineCustomRecordForm");
                                
                                    public static final java.lang.String __openidSingleSignOn =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_openidSingleSignOn");
                                
                                    public static final java.lang.String __opportunity =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_opportunity");
                                
                                    public static final java.lang.String __organizationValue =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_organizationValue");
                                
                                    public static final java.lang.String __otherAssetRegisters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_otherAssetRegisters");
                                
                                    public static final java.lang.String __otherCurrentAssetRegisters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_otherCurrentAssetRegisters");
                                
                                    public static final java.lang.String __otherCurrentLiabilityRegisters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_otherCurrentLiabilityRegisters");
                                
                                    public static final java.lang.String __otherCustomFields =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_otherCustomFields");
                                
                                    public static final java.lang.String __otherExpenseRegisters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_otherExpenseRegisters");
                                
                                    public static final java.lang.String __otherIncomeRegisters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_otherIncomeRegisters");
                                
                                    public static final java.lang.String __otherLists =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_otherLists");
                                
                                    public static final java.lang.String __otherNames =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_otherNames");
                                
                                    public static final java.lang.String __outlookIntegration =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_outlookIntegration");
                                
                                    public static final java.lang.String __outlookIntegration3 =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_outlookIntegration3");
                                
                                    public static final java.lang.String __overrideEstimatedCostOnTransactions =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_overrideEstimatedCostOnTransactions");
                                
                                    public static final java.lang.String __overridePaymentHold =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_overridePaymentHold");
                                
                                    public static final java.lang.String __overridePeriodRestrictions =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_overridePeriodRestrictions");
                                
                                    public static final java.lang.String __ownershipTransfer =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ownershipTransfer");
                                
                                    public static final java.lang.String __partnerAuthorizedCommissionReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_partnerAuthorizedCommissionReports");
                                
                                    public static final java.lang.String __partnerCommissionReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_partnerCommissionReports");
                                
                                    public static final java.lang.String __partnerCommissionSchedulesPlans =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_partnerCommissionSchedulesPlans");
                                
                                    public static final java.lang.String __partnerCommissionTransaction =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_partnerCommissionTransaction");
                                
                                    public static final java.lang.String __partnerCommissionTransactionApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_partnerCommissionTransactionApproval");
                                
                                    public static final java.lang.String __partnerContribution =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_partnerContribution");
                                
                                    public static final java.lang.String __partners =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_partners");
                                
                                    public static final java.lang.String __payBills =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_payBills");
                                
                                    public static final java.lang.String __paycheckJournal =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paycheckJournal");
                                
                                    public static final java.lang.String __paychecks =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paychecks");
                                
                                    public static final java.lang.String __paymentMethods =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paymentMethods");
                                
                                    public static final java.lang.String __payrollCheckRegister =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_payrollCheckRegister");
                                
                                    public static final java.lang.String __payrollHoursAndEarnings =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_payrollHoursAndEarnings");
                                
                                    public static final java.lang.String __payrollItems =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_payrollItems");
                                
                                    public static final java.lang.String __payrollJournalReport =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_payrollJournalReport");
                                
                                    public static final java.lang.String __payrollLiabilityPayments =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_payrollLiabilityPayments");
                                
                                    public static final java.lang.String __payrollLiabilityReport =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_payrollLiabilityReport");
                                
                                    public static final java.lang.String __payrollStateWithholding =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_payrollStateWithholding");
                                
                                    public static final java.lang.String __payrollSummaryAndDetailReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_payrollSummaryAndDetailReports");
                                
                                    public static final java.lang.String __paySalesTax =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paySalesTax");
                                
                                    public static final java.lang.String __payTaxLiability =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_payTaxLiability");
                                
                                    public static final java.lang.String __pdfMessages =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pdfMessages");
                                
                                    public static final java.lang.String __pdfTemplate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pdfTemplate");
                                
                                    public static final java.lang.String __performSearch =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_performSearch");
                                
                                    public static final java.lang.String __periodClosingManagement =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_periodClosingManagement");
                                
                                    public static final java.lang.String __persistSearch =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_persistSearch");
                                
                                    public static final java.lang.String __phasedProcesses =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_phasedProcesses");
                                
                                    public static final java.lang.String __phoneCalls =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_phoneCalls");
                                
                                    public static final java.lang.String __plannedRevenue =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_plannedRevenue");
                                
                                    public static final java.lang.String __plannedStandardCost =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_plannedStandardCost");
                                
                                    public static final java.lang.String __positions =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_positions");
                                
                                    public static final java.lang.String __postingPeriodOnTransactions =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_postingPeriodOnTransactions");
                                
                                    public static final java.lang.String __postTime =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_postTime");
                                
                                    public static final java.lang.String __postVendorBillVariances =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_postVendorBillVariances");
                                
                                    public static final java.lang.String __presentationCategories =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_presentationCategories");
                                
                                    public static final java.lang.String __printChecksAndForms =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_printChecksAndForms");
                                
                                    public static final java.lang.String __printEmailFax =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_printEmailFax");
                                
                                    public static final java.lang.String __printShipmentDocuments =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_printShipmentDocuments");
                                
                                    public static final java.lang.String __processGSTRefund =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_processGSTRefund");
                                
                                    public static final java.lang.String __processPayroll =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_processPayroll");
                                
                                    public static final java.lang.String __projectProfitability =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_projectProfitability");
                                
                                    public static final java.lang.String __projectProfitabilitySetup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_projectProfitabilitySetup");
                                
                                    public static final java.lang.String __projectProjectTemplateConversion =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_projectProjectTemplateConversion");
                                
                                    public static final java.lang.String __projectRevenueRules =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_projectRevenueRules");
                                
                                    public static final java.lang.String __projectTasks =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_projectTasks");
                                
                                    public static final java.lang.String __projectTemplates =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_projectTemplates");
                                
                                    public static final java.lang.String __promotionCode =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_promotionCode");
                                
                                    public static final java.lang.String __provisioning =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_provisioning");
                                
                                    public static final java.lang.String __provisioningForQa =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_provisioningForQa");
                                
                                    public static final java.lang.String __provisionNewAccountOnTestdrive =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_provisionNewAccountOnTestdrive");
                                
                                    public static final java.lang.String __provisionTestDrive =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_provisionTestDrive");
                                
                                    public static final java.lang.String __pstSummaryReport =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pstSummaryReport");
                                
                                    public static final java.lang.String __publicTemplateCategories =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_publicTemplateCategories");
                                
                                    public static final java.lang.String __publishDashboards =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_publishDashboards");
                                
                                    public static final java.lang.String __publishEmployeeList =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_publishEmployeeList");
                                
                                    public static final java.lang.String __publishForms =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_publishForms");
                                
                                    public static final java.lang.String __publishKnowledgeBase =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_publishKnowledgeBase");
                                
                                    public static final java.lang.String __publishRSSFeeds =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_publishRSSFeeds");
                                
                                    public static final java.lang.String __publishSearch =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_publishSearch");
                                
                                    public static final java.lang.String __purchaseContract =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchaseContract");
                                
                                    public static final java.lang.String __purchaseContractApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchaseContractApproval");
                                
                                    public static final java.lang.String __purchaseOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchaseOrder");
                                
                                    public static final java.lang.String __purchaseOrderReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchaseOrderReports");
                                
                                    public static final java.lang.String __purchases =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_purchases");
                                
                                    public static final java.lang.String __quantityPricingSchedules =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_quantityPricingSchedules");
                                
                                    public static final java.lang.String __quotaReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_quotaReports");
                                
                                    public static final java.lang.String __receiveOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_receiveOrder");
                                
                                    public static final java.lang.String __receiveReturns =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_receiveReturns");
                                
                                    public static final java.lang.String __recognizeGiftCertificateIncome =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_recognizeGiftCertificateIncome");
                                
                                    public static final java.lang.String __reconcile =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_reconcile");
                                
                                    public static final java.lang.String __reconcileReporting =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_reconcileReporting");
                                
                                    public static final java.lang.String __recordCustomField =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_recordCustomField");
                                
                                    public static final java.lang.String __refundReturns =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_refundReturns");
                                
                                    public static final java.lang.String __relatedItems =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_relatedItems");
                                
                                    public static final java.lang.String __reportCustomization =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_reportCustomization");
                                
                                    public static final java.lang.String __reportScheduling =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_reportScheduling");
                                
                                    public static final java.lang.String __requestForQuote =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_requestForQuote");
                                
                                    public static final java.lang.String __requisition =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_requisition");
                                
                                    public static final java.lang.String __requisitionApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_requisitionApproval");
                                
                                    public static final java.lang.String __resource =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_resource");
                                
                                    public static final java.lang.String __resourceAllocationApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_resourceAllocationApproval");
                                
                                    public static final java.lang.String __resourceAllocationReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_resourceAllocationReports");
                                
                                    public static final java.lang.String __resourceAllocations =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_resourceAllocations");
                                
                                    public static final java.lang.String __returnAuthApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_returnAuthApproval");
                                
                                    public static final java.lang.String __returnAuthorization =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_returnAuthorization");
                                
                                    public static final java.lang.String __returnAuthorizationReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_returnAuthorizationReports");
                                
                                    public static final java.lang.String __revalueInventoryCost =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_revalueInventoryCost");
                                
                                    public static final java.lang.String __revenueArrangement =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_revenueArrangement");
                                
                                    public static final java.lang.String __revenueArrangementApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_revenueArrangementApproval");
                                
                                    public static final java.lang.String __revenueCommitment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_revenueCommitment");
                                
                                    public static final java.lang.String __revenueCommitmentReversal =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_revenueCommitmentReversal");
                                
                                    public static final java.lang.String __revenueContracts =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_revenueContracts");
                                
                                    public static final java.lang.String __revenueElement =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_revenueElement");
                                
                                    public static final java.lang.String __revenueManagementVSOE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_revenueManagementVSOE");
                                
                                    public static final java.lang.String __revenueRecognitionFieldMapping =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_revenueRecognitionFieldMapping");
                                
                                    public static final java.lang.String __revenueRecognitionPlan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_revenueRecognitionPlan");
                                
                                    public static final java.lang.String __revenueRecognitionReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_revenueRecognitionReports");
                                
                                    public static final java.lang.String __revenueRecognitionRule =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_revenueRecognitionRule");
                                
                                    public static final java.lang.String __revenueRecognitionSchedules =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_revenueRecognitionSchedules");
                                
                                    public static final java.lang.String __reviewCustomGlPlugInExecutions =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_reviewCustomGlPlugInExecutions");
                                
                                    public static final java.lang.String __roles =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_roles");
                                
                                    public static final java.lang.String __runPayroll =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_runPayroll");
                                
                                    public static final java.lang.String __sales =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sales");
                                
                                    public static final java.lang.String __salesByPartner =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesByPartner");
                                
                                    public static final java.lang.String __salesByPromotionCode =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesByPromotionCode");
                                
                                    public static final java.lang.String __salesCampaigns =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesCampaigns");
                                
                                    public static final java.lang.String __salesForceAutomation =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesForceAutomation");
                                
                                    public static final java.lang.String __salesForceAutomationSetup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesForceAutomationSetup");
                                
                                    public static final java.lang.String __salesOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesOrder");
                                
                                    public static final java.lang.String __salesOrderApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesOrderApproval");
                                
                                    public static final java.lang.String __salesOrderFulfillmentReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesOrderFulfillmentReports");
                                
                                    public static final java.lang.String __salesOrderReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesOrderReports");
                                
                                    public static final java.lang.String __salesOrderTransactionReport =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesOrderTransactionReport");
                                
                                    public static final java.lang.String __salesRoles =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesRoles");
                                
                                    public static final java.lang.String __salesTerritory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesTerritory");
                                
                                    public static final java.lang.String __salesTerritoryRule =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_salesTerritoryRule");
                                
                                    public static final java.lang.String __samlSingleSignOn =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_samlSingleSignOn");
                                
                                    public static final java.lang.String __scheduleMassUpdates =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_scheduleMassUpdates");
                                
                                    public static final java.lang.String __setUpAccounting =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpAccounting");
                                
                                    public static final java.lang.String __setUpAchProcessing =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpAchProcessing");
                                
                                    public static final java.lang.String __setUpAdpPayroll =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpAdpPayroll");
                                
                                    public static final java.lang.String __setUpBillPay =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpBillPay");
                                
                                    public static final java.lang.String __setUpBudgets =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpBudgets");
                                
                                    public static final java.lang.String __setUpCampaignEmailAddresses =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpCampaignEmailAddresses");
                                
                                    public static final java.lang.String __setupCampaigns =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setupCampaigns");
                                
                                    public static final java.lang.String __setUpCompany =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpCompany");
                                
                                    public static final java.lang.String __setUpCsvPreferences =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpCsvPreferences");
                                
                                    public static final java.lang.String __setUpDomains =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpDomains");
                                
                                    public static final java.lang.String __setUpEbay =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpEbay");
                                
                                    public static final java.lang.String __setUpImageResizing =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpImageResizing");
                                
                                    public static final java.lang.String __setUpOpenidSingleSignOn =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpOpenidSingleSignOn");
                                
                                    public static final java.lang.String __setUpPayroll =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpPayroll");
                                
                                    public static final java.lang.String __setUpReminders =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpReminders");
                                
                                    public static final java.lang.String __setUpSamlSingleSignOn =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpSamlSingleSignOn");
                                
                                    public static final java.lang.String __setUpSnapshots =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpSnapshots");
                                
                                    public static final java.lang.String __setUpSynchronization =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpSynchronization");
                                
                                    public static final java.lang.String __setUpWebServices =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpWebServices");
                                
                                    public static final java.lang.String __setUpWebSite =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpWebSite");
                                
                                    public static final java.lang.String __setUpYearStatus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_setUpYearStatus");
                                
                                    public static final java.lang.String __shippingItems =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_shippingItems");
                                
                                    public static final java.lang.String __shippingPartnerPackage =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_shippingPartnerPackage");
                                
                                    public static final java.lang.String __shippingPartnerRegistration =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_shippingPartnerRegistration");
                                
                                    public static final java.lang.String __shippingPartnerShipment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_shippingPartnerShipment");
                                
                                    public static final java.lang.String __shortcuts =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_shortcuts");
                                
                                    public static final java.lang.String __standardCostVersion =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_standardCostVersion");
                                
                                    public static final java.lang.String __statementCharge =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_statementCharge");
                                
                                    public static final java.lang.String __statisticalAccountRegisters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_statisticalAccountRegisters");
                                
                                    public static final java.lang.String __storeCategories =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_storeCategories");
                                
                                    public static final java.lang.String __storeContentCategories =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_storeContentCategories");
                                
                                    public static final java.lang.String __storeContentItems =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_storeContentItems");
                                
                                    public static final java.lang.String __storeLogoUpload =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_storeLogoUpload");
                                
                                    public static final java.lang.String __storePickupFulfillment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_storePickupFulfillment");
                                
                                    public static final java.lang.String __storeTabs =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_storeTabs");
                                
                                    public static final java.lang.String __subscriptionPlan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_subscriptionPlan");
                                
                                    public static final java.lang.String __subscriptions =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_subscriptions");
                                
                                    public static final java.lang.String __subsidiaries =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_subsidiaries");
                                
                                    public static final java.lang.String __subsidiaryTaxEngineSelection =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_subsidiaryTaxEngineSelection");
                                
                                    public static final java.lang.String __suiteAppDeployment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_suiteAppDeployment");
                                
                                    public static final java.lang.String __suiteBundler =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_suiteBundler");
                                
                                    public static final java.lang.String __suiteBundlerAuditTrail =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_suiteBundlerAuditTrail");
                                
                                    public static final java.lang.String __suiteBundlerUpgrades =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_suiteBundlerUpgrades");
                                
                                    public static final java.lang.String __suiteScript =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_suiteScript");
                                
                                    public static final java.lang.String __suiteScriptNlCorpManagement =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_suiteScriptNlCorpManagement");
                                
                                    public static final java.lang.String __suiteScriptScheduling =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_suiteScriptScheduling");
                                
                                    public static final java.lang.String __suiteSignon =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_suiteSignon");
                                
                                    public static final java.lang.String __support =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_support");
                                
                                    public static final java.lang.String __supportCaseIssue =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_supportCaseIssue");
                                
                                    public static final java.lang.String __supportCaseOrigin =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_supportCaseOrigin");
                                
                                    public static final java.lang.String __supportCasePriority =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_supportCasePriority");
                                
                                    public static final java.lang.String __supportCaseSnapshotReminders =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_supportCaseSnapshotReminders");
                                
                                    public static final java.lang.String __supportCaseStatus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_supportCaseStatus");
                                
                                    public static final java.lang.String __supportCaseTerritory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_supportCaseTerritory");
                                
                                    public static final java.lang.String __supportCaseTerritoryRule =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_supportCaseTerritoryRule");
                                
                                    public static final java.lang.String __supportCaseType =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_supportCaseType");
                                
                                    public static final java.lang.String __supportSetup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_supportSetup");
                                
                                    public static final java.lang.String __swapPricesBetweenPriceLevels =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_swapPricesBetweenPriceLevels");
                                
                                    public static final java.lang.String __systemEmailTemplate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_systemEmailTemplate");
                                
                                    public static final java.lang.String __systemStatus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_systemStatus");
                                
                                    public static final java.lang.String __tableauWorkbookExport =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tableauWorkbookExport");
                                
                                    public static final java.lang.String __tasks =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tasks");
                                
                                    public static final java.lang.String __tax =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tax");
                                
                                    public static final java.lang.String __taxDetailsTab =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_taxDetailsTab");
                                
                                    public static final java.lang.String __taxItems =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_taxItems");
                                
                                    public static final java.lang.String __taxReports =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_taxReports");
                                
                                    public static final java.lang.String __taxSchedules =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_taxSchedules");
                                
                                    public static final java.lang.String __teamSellingContribution =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_teamSellingContribution");
                                
                                    public static final java.lang.String __tegataAccounts =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tegataAccounts");
                                
                                    public static final java.lang.String __tegataPayable =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tegataPayable");
                                
                                    public static final java.lang.String __tegataReceivable =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tegataReceivable");
                                
                                    public static final java.lang.String __telephonyIntegration =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_telephonyIntegration");
                                
                                    public static final java.lang.String __templateCategories =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_templateCategories");
                                
                                    public static final java.lang.String __terminationReasons =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_terminationReasons");
                                
                                    public static final java.lang.String __testdriveMasters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_testdriveMasters");
                                
                                    public static final java.lang.String __timeOffAdministration =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_timeOffAdministration");
                                
                                    public static final java.lang.String __timer =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_timer");
                                
                                    public static final java.lang.String __timeTracking =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_timeTracking");
                                
                                    public static final java.lang.String __trackMessages =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_trackMessages");
                                
                                    public static final java.lang.String __trackTime =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_trackTime");
                                
                                    public static final java.lang.String __transactionDetail =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_transactionDetail");
                                
                                    public static final java.lang.String __transactionNumberingAuditLog =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_transactionNumberingAuditLog");
                                
                                    public static final java.lang.String __transactionReceiveInboundShipment =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_transactionReceiveInboundShipment");
                                
                                    public static final java.lang.String __transferFunds =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_transferFunds");
                                
                                    public static final java.lang.String __transferInventory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_transferInventory");
                                
                                    public static final java.lang.String __transferOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_transferOrder");
                                
                                    public static final java.lang.String __transferOrderApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_transferOrderApproval");
                                
                                    public static final java.lang.String __translation =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_translation");
                                
                                    public static final java.lang.String __trialBalance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_trialBalance");
                                
                                    public static final java.lang.String __twoFactorAuthentication =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_twoFactorAuthentication");
                                
                                    public static final java.lang.String __twoFactorAuthenticationBase =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_twoFactorAuthenticationBase");
                                
                                    public static final java.lang.String __unbilledReceivableRegisters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_unbilledReceivableRegisters");
                                
                                    public static final java.lang.String __unbuildAssemblies =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_unbuildAssemblies");
                                
                                    public static final java.lang.String __uncategorizedPresentationItems =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_uncategorizedPresentationItems");
                                
                                    public static final java.lang.String __undeliveredEmails =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_undeliveredEmails");
                                
                                    public static final java.lang.String __units =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_units");
                                
                                    public static final java.lang.String __updatePrices =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_updatePrices");
                                
                                    public static final java.lang.String __upsellAssistant =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_upsellAssistant");
                                
                                    public static final java.lang.String __upsellSetup =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_upsellSetup");
                                
                                    public static final java.lang.String __upsellWizard =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_upsellWizard");
                                
                                    public static final java.lang.String __usage =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_usage");
                                
                                    public static final java.lang.String __userAccessTokens =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_userAccessTokens");
                                
                                    public static final java.lang.String __userPreferences =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_userPreferences");
                                
                                    public static final java.lang.String __usersAndPasswords =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_usersAndPasswords");
                                
                                    public static final java.lang.String __vendorBillApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorBillApproval");
                                
                                    public static final java.lang.String __vendorPaymentStatus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorPaymentStatus");
                                
                                    public static final java.lang.String __vendorRequestForQuote =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorRequestForQuote");
                                
                                    public static final java.lang.String __vendorReturnAuthApproval =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorReturnAuthApproval");
                                
                                    public static final java.lang.String __vendorReturnAuthorization =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorReturnAuthorization");
                                
                                    public static final java.lang.String __vendorReturns =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendorReturns");
                                
                                    public static final java.lang.String __vendors =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vendors");
                                
                                    public static final java.lang.String __viewGatewayAsynchronousNotifications =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_viewGatewayAsynchronousNotifications");
                                
                                    public static final java.lang.String __viewLoginAuditTrail =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_viewLoginAuditTrail");
                                
                                    public static final java.lang.String __viewOnlineBillPayStatus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_viewOnlineBillPayStatus");
                                
                                    public static final java.lang.String __viewPaymentEvents =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_viewPaymentEvents");
                                
                                    public static final java.lang.String __viewUnencryptedCreditCards =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_viewUnencryptedCreditCards");
                                
                                    public static final java.lang.String __viewWebServicesLogs =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_viewWebServicesLogs");
                                
                                    public static final java.lang.String __webServices =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_webServices");
                                
                                    public static final java.lang.String __webSiteExternalPublisher =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_webSiteExternalPublisher");
                                
                                    public static final java.lang.String __webSiteManagement =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_webSiteManagement");
                                
                                    public static final java.lang.String __webSiteReport =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_webSiteReport");
                                
                                    public static final java.lang.String __webStoreEmailTemplate =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_webStoreEmailTemplate");
                                
                                    public static final java.lang.String __webStoreReport =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_webStoreReport");
                                
                                    public static final java.lang.String __workCalendar =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workCalendar");
                                
                                    public static final java.lang.String __workflow =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workflow");
                                
                                    public static final java.lang.String __workOrder =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workOrder");
                                
                                    public static final java.lang.String __workOrderClose =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workOrderClose");
                                
                                    public static final java.lang.String __workOrderCompletion =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workOrderCompletion");
                                
                                    public static final java.lang.String __workOrderIssue =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workOrderIssue");
                                
                                    public static final java.lang.String __workplaces =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_workplaces");
                                
                                public static final PermissionCode _accessPaymentAuditLog =
                                    new PermissionCode(__accessPaymentAuditLog,true);
                            
                                public static final PermissionCode _accessTokenManagement =
                                    new PermissionCode(__accessTokenManagement,true);
                            
                                public static final PermissionCode _accountDetail =
                                    new PermissionCode(__accountDetail,true);
                            
                                public static final PermissionCode _accounting =
                                    new PermissionCode(__accounting,true);
                            
                                public static final PermissionCode _accountingBook =
                                    new PermissionCode(__accountingBook,true);
                            
                                public static final PermissionCode _accountingLists =
                                    new PermissionCode(__accountingLists,true);
                            
                                public static final PermissionCode _accounts =
                                    new PermissionCode(__accounts,true);
                            
                                public static final PermissionCode _accountsPayable =
                                    new PermissionCode(__accountsPayable,true);
                            
                                public static final PermissionCode _accountsPayableGraphing =
                                    new PermissionCode(__accountsPayableGraphing,true);
                            
                                public static final PermissionCode _accountsPayableRegister =
                                    new PermissionCode(__accountsPayableRegister,true);
                            
                                public static final PermissionCode _accountsReceivable =
                                    new PermissionCode(__accountsReceivable,true);
                            
                                public static final PermissionCode _accountsReceivableGraphing =
                                    new PermissionCode(__accountsReceivableGraphing,true);
                            
                                public static final PermissionCode _accountsReceivableRegister =
                                    new PermissionCode(__accountsReceivableRegister,true);
                            
                                public static final PermissionCode _accountsReceivableUnbilled =
                                    new PermissionCode(__accountsReceivableUnbilled,true);
                            
                                public static final PermissionCode _adjustInventory =
                                    new PermissionCode(__adjustInventory,true);
                            
                                public static final PermissionCode _adjustInventoryWorksheet =
                                    new PermissionCode(__adjustInventoryWorksheet,true);
                            
                                public static final PermissionCode _admindocs =
                                    new PermissionCode(__admindocs,true);
                            
                                public static final PermissionCode _adpImportData =
                                    new PermissionCode(__adpImportData,true);
                            
                                public static final PermissionCode _adpSetup =
                                    new PermissionCode(__adpSetup,true);
                            
                                public static final PermissionCode _advancedAnalytics =
                                    new PermissionCode(__advancedAnalytics,true);
                            
                                public static final PermissionCode _advancedGovernmentIssuedIds =
                                    new PermissionCode(__advancedGovernmentIssuedIds,true);
                            
                                public static final PermissionCode _advancedOrderManagement =
                                    new PermissionCode(__advancedOrderManagement,true);
                            
                                public static final PermissionCode _advancedPDFHTMLTemplates =
                                    new PermissionCode(__advancedPDFHTMLTemplates,true);
                            
                                public static final PermissionCode _allocationSchedules =
                                    new PermissionCode(__allocationSchedules,true);
                            
                                public static final PermissionCode _allowNonGLChanges =
                                    new PermissionCode(__allowNonGLChanges,true);
                            
                                public static final PermissionCode _allowPendingBookJournalEntry =
                                    new PermissionCode(__allowPendingBookJournalEntry,true);
                            
                                public static final PermissionCode _amendW4 =
                                    new PermissionCode(__amendW4,true);
                            
                                public static final PermissionCode _amortizationReports =
                                    new PermissionCode(__amortizationReports,true);
                            
                                public static final PermissionCode _amortizationSchedules =
                                    new PermissionCode(__amortizationSchedules,true);
                            
                                public static final PermissionCode _applicationPublishers =
                                    new PermissionCode(__applicationPublishers,true);
                            
                                public static final PermissionCode _approveDirectDeposit =
                                    new PermissionCode(__approveDirectDeposit,true);
                            
                                public static final PermissionCode _approveEFT =
                                    new PermissionCode(__approveEFT,true);
                            
                                public static final PermissionCode _approveOnlineBillPayments =
                                    new PermissionCode(__approveOnlineBillPayments,true);
                            
                                public static final PermissionCode _approveVendorPayments =
                                    new PermissionCode(__approveVendorPayments,true);
                            
                                public static final PermissionCode _auditTrail =
                                    new PermissionCode(__auditTrail,true);
                            
                                public static final PermissionCode _backupYourData =
                                    new PermissionCode(__backupYourData,true);
                            
                                public static final PermissionCode _balanceLocationCostingGroupAccounts =
                                    new PermissionCode(__balanceLocationCostingGroupAccounts,true);
                            
                                public static final PermissionCode _balanceSheet =
                                    new PermissionCode(__balanceSheet,true);
                            
                                public static final PermissionCode _bankAccountRegisters =
                                    new PermissionCode(__bankAccountRegisters,true);
                            
                                public static final PermissionCode _basicGovernmentIssuedIds =
                                    new PermissionCode(__basicGovernmentIssuedIds,true);
                            
                                public static final PermissionCode _billInboundShipment =
                                    new PermissionCode(__billInboundShipment,true);
                            
                                public static final PermissionCode _billingInformation =
                                    new PermissionCode(__billingInformation,true);
                            
                                public static final PermissionCode _billingSchedules =
                                    new PermissionCode(__billingSchedules,true);
                            
                                public static final PermissionCode _billOfDistribution =
                                    new PermissionCode(__billOfDistribution,true);
                            
                                public static final PermissionCode _billOfMaterials =
                                    new PermissionCode(__billOfMaterials,true);
                            
                                public static final PermissionCode _billOfMaterialsInquiry =
                                    new PermissionCode(__billOfMaterialsInquiry,true);
                            
                                public static final PermissionCode _billPurchaseOrders =
                                    new PermissionCode(__billPurchaseOrders,true);
                            
                                public static final PermissionCode _bills =
                                    new PermissionCode(__bills,true);
                            
                                public static final PermissionCode _billSalesOrders =
                                    new PermissionCode(__billSalesOrders,true);
                            
                                public static final PermissionCode _bins =
                                    new PermissionCode(__bins,true);
                            
                                public static final PermissionCode _binTransfer =
                                    new PermissionCode(__binTransfer,true);
                            
                                public static final PermissionCode _binWorksheet =
                                    new PermissionCode(__binWorksheet,true);
                            
                                public static final PermissionCode _blanketPurchaseOrder =
                                    new PermissionCode(__blanketPurchaseOrder,true);
                            
                                public static final PermissionCode _blanketPurchaseOrderApproval =
                                    new PermissionCode(__blanketPurchaseOrderApproval,true);
                            
                                public static final PermissionCode _budget =
                                    new PermissionCode(__budget,true);
                            
                                public static final PermissionCode _buildAssemblies =
                                    new PermissionCode(__buildAssemblies,true);
                            
                                public static final PermissionCode _buildWorkOrders =
                                    new PermissionCode(__buildWorkOrders,true);
                            
                                public static final PermissionCode _calculateTime =
                                    new PermissionCode(__calculateTime,true);
                            
                                public static final PermissionCode _calendar =
                                    new PermissionCode(__calendar,true);
                            
                                public static final PermissionCode _campaignHistory =
                                    new PermissionCode(__campaignHistory,true);
                            
                                public static final PermissionCode _caseAlerts =
                                    new PermissionCode(__caseAlerts,true);
                            
                                public static final PermissionCode _cases =
                                    new PermissionCode(__cases,true);
                            
                                public static final PermissionCode _cashFlowStatement =
                                    new PermissionCode(__cashFlowStatement,true);
                            
                                public static final PermissionCode _cashSale =
                                    new PermissionCode(__cashSale,true);
                            
                                public static final PermissionCode _cashSaleRefund =
                                    new PermissionCode(__cashSaleRefund,true);
                            
                                public static final PermissionCode _changeEmailOrPassword =
                                    new PermissionCode(__changeEmailOrPassword,true);
                            
                                public static final PermissionCode _changeRole =
                                    new PermissionCode(__changeRole,true);
                            
                                public static final PermissionCode _chargeRule =
                                    new PermissionCode(__chargeRule,true);
                            
                                public static final PermissionCode _chargeRunRules =
                                    new PermissionCode(__chargeRunRules,true);
                            
                                public static final PermissionCode _check =
                                    new PermissionCode(__check,true);
                            
                                public static final PermissionCode _checkItemAvailability =
                                    new PermissionCode(__checkItemAvailability,true);
                            
                                public static final PermissionCode _classes =
                                    new PermissionCode(__classes,true);
                            
                                public static final PermissionCode _classSegmentMapping =
                                    new PermissionCode(__classSegmentMapping,true);
                            
                                public static final PermissionCode _closeAccount =
                                    new PermissionCode(__closeAccount,true);
                            
                                public static final PermissionCode _closeWorkOrders =
                                    new PermissionCode(__closeWorkOrders,true);
                            
                                public static final PermissionCode _colorThemes =
                                    new PermissionCode(__colorThemes,true);
                            
                                public static final PermissionCode _commerceCategories =
                                    new PermissionCode(__commerceCategories,true);
                            
                                public static final PermissionCode _commissionFeatureSetup =
                                    new PermissionCode(__commissionFeatureSetup,true);
                            
                                public static final PermissionCode _commissionReports =
                                    new PermissionCode(__commissionReports,true);
                            
                                public static final PermissionCode _commitOrders =
                                    new PermissionCode(__commitOrders,true);
                            
                                public static final PermissionCode _commitPayroll =
                                    new PermissionCode(__commitPayroll,true);
                            
                                public static final PermissionCode _companies =
                                    new PermissionCode(__companies,true);
                            
                                public static final PermissionCode _companyInformation =
                                    new PermissionCode(__companyInformation,true);
                            
                                public static final PermissionCode _companyPreferences =
                                    new PermissionCode(__companyPreferences,true);
                            
                                public static final PermissionCode _competitors =
                                    new PermissionCode(__competitors,true);
                            
                                public static final PermissionCode _componentWhereUsed =
                                    new PermissionCode(__componentWhereUsed,true);
                            
                                public static final PermissionCode _contactRoles =
                                    new PermissionCode(__contactRoles,true);
                            
                                public static final PermissionCode _contacts =
                                    new PermissionCode(__contacts,true);
                            
                                public static final PermissionCode _controlSuitescriptAndWorkflowTriggersInWebServicesRequest =
                                    new PermissionCode(__controlSuitescriptAndWorkflowTriggersInWebServicesRequest,true);
                            
                                public static final PermissionCode _controlSuitescriptAndWorkflowTriggersPerCsvImport =
                                    new PermissionCode(__controlSuitescriptAndWorkflowTriggersPerCsvImport,true);
                            
                                public static final PermissionCode _convertClassesToDepartments =
                                    new PermissionCode(__convertClassesToDepartments,true);
                            
                                public static final PermissionCode _convertClassesToLocations =
                                    new PermissionCode(__convertClassesToLocations,true);
                            
                                public static final PermissionCode _copyBudgets =
                                    new PermissionCode(__copyBudgets,true);
                            
                                public static final PermissionCode _copyChartOfAccountsToNewCompany =
                                    new PermissionCode(__copyChartOfAccountsToNewCompany,true);
                            
                                public static final PermissionCode _copyProjectTasks =
                                    new PermissionCode(__copyProjectTasks,true);
                            
                                public static final PermissionCode _costedBillOfMaterialsInquiry =
                                    new PermissionCode(__costedBillOfMaterialsInquiry,true);
                            
                                public static final PermissionCode _costOfGoodsSoldRegisters =
                                    new PermissionCode(__costOfGoodsSoldRegisters,true);
                            
                                public static final PermissionCode _countInventory =
                                    new PermissionCode(__countInventory,true);
                            
                                public static final PermissionCode _createAllocationSchedules =
                                    new PermissionCode(__createAllocationSchedules,true);
                            
                                public static final PermissionCode _createConsolidationCompany =
                                    new PermissionCode(__createConsolidationCompany,true);
                            
                                public static final PermissionCode _createFiscalCalendar =
                                    new PermissionCode(__createFiscalCalendar,true);
                            
                                public static final PermissionCode _createInventoryCounts =
                                    new PermissionCode(__createInventoryCounts,true);
                            
                                public static final PermissionCode _creditCard =
                                    new PermissionCode(__creditCard,true);
                            
                                public static final PermissionCode _creditCardProcessing =
                                    new PermissionCode(__creditCardProcessing,true);
                            
                                public static final PermissionCode _creditCardRefund =
                                    new PermissionCode(__creditCardRefund,true);
                            
                                public static final PermissionCode _creditCardRegisters =
                                    new PermissionCode(__creditCardRegisters,true);
                            
                                public static final PermissionCode _creditMemo =
                                    new PermissionCode(__creditMemo,true);
                            
                                public static final PermissionCode _creditReturns =
                                    new PermissionCode(__creditReturns,true);
                            
                                public static final PermissionCode _crmGroups =
                                    new PermissionCode(__crmGroups,true);
                            
                                public static final PermissionCode _crmLists =
                                    new PermissionCode(__crmLists,true);
                            
                                public static final PermissionCode _cspSetup =
                                    new PermissionCode(__cspSetup,true);
                            
                                public static final PermissionCode _currency =
                                    new PermissionCode(__currency,true);
                            
                                public static final PermissionCode _currencyAdjustmentJournal =
                                    new PermissionCode(__currencyAdjustmentJournal,true);
                            
                                public static final PermissionCode _currencyRevaluation =
                                    new PermissionCode(__currencyRevaluation,true);
                            
                                public static final PermissionCode _customAddressForm =
                                    new PermissionCode(__customAddressForm,true);
                            
                                public static final PermissionCode _customBodyFields =
                                    new PermissionCode(__customBodyFields,true);
                            
                                public static final PermissionCode _customCenterCategories =
                                    new PermissionCode(__customCenterCategories,true);
                            
                                public static final PermissionCode _customCenterLinks =
                                    new PermissionCode(__customCenterLinks,true);
                            
                                public static final PermissionCode _customCenters =
                                    new PermissionCode(__customCenters,true);
                            
                                public static final PermissionCode _customCenterTabs =
                                    new PermissionCode(__customCenterTabs,true);
                            
                                public static final PermissionCode _customColumnFields =
                                    new PermissionCode(__customColumnFields,true);
                            
                                public static final PermissionCode _customEntityFields =
                                    new PermissionCode(__customEntityFields,true);
                            
                                public static final PermissionCode _customEntryForms =
                                    new PermissionCode(__customEntryForms,true);
                            
                                public static final PermissionCode _customerCharge =
                                    new PermissionCode(__customerCharge,true);
                            
                                public static final PermissionCode _customerDeposit =
                                    new PermissionCode(__customerDeposit,true);
                            
                                public static final PermissionCode _customerPayment =
                                    new PermissionCode(__customerPayment,true);
                            
                                public static final PermissionCode _customerPaymentAuthorization =
                                    new PermissionCode(__customerPaymentAuthorization,true);
                            
                                public static final PermissionCode _customerProfile =
                                    new PermissionCode(__customerProfile,true);
                            
                                public static final PermissionCode _customerRefund =
                                    new PermissionCode(__customerRefund,true);
                            
                                public static final PermissionCode _customers =
                                    new PermissionCode(__customers,true);
                            
                                public static final PermissionCode _customerStatus =
                                    new PermissionCode(__customerStatus,true);
                            
                                public static final PermissionCode _customEventFields =
                                    new PermissionCode(__customEventFields,true);
                            
                                public static final PermissionCode _customFields =
                                    new PermissionCode(__customFields,true);
                            
                                public static final PermissionCode _customGlLinesPlugInAuditLog =
                                    new PermissionCode(__customGlLinesPlugInAuditLog,true);
                            
                                public static final PermissionCode _customGlLinesPlugInAuditLogSegments =
                                    new PermissionCode(__customGlLinesPlugInAuditLogSegments,true);
                            
                                public static final PermissionCode _customHTMLLayouts =
                                    new PermissionCode(__customHTMLLayouts,true);
                            
                                public static final PermissionCode _customItemFields =
                                    new PermissionCode(__customItemFields,true);
                            
                                public static final PermissionCode _customItemNumberFields =
                                    new PermissionCode(__customItemNumberFields,true);
                            
                                public static final PermissionCode _customizePage =
                                    new PermissionCode(__customizePage,true);
                            
                                public static final PermissionCode _customLists =
                                    new PermissionCode(__customLists,true);
                            
                                public static final PermissionCode _customPDFLayouts =
                                    new PermissionCode(__customPDFLayouts,true);
                            
                                public static final PermissionCode _customRecordEntries =
                                    new PermissionCode(__customRecordEntries,true);
                            
                                public static final PermissionCode _customRecordTypes =
                                    new PermissionCode(__customRecordTypes,true);
                            
                                public static final PermissionCode _customSegments =
                                    new PermissionCode(__customSegments,true);
                            
                                public static final PermissionCode _customSublist =
                                    new PermissionCode(__customSublist,true);
                            
                                public static final PermissionCode _customSublists =
                                    new PermissionCode(__customSublists,true);
                            
                                public static final PermissionCode _customSubtabs =
                                    new PermissionCode(__customSubtabs,true);
                            
                                public static final PermissionCode _customTransactionFields =
                                    new PermissionCode(__customTransactionFields,true);
                            
                                public static final PermissionCode _customTransactionForms =
                                    new PermissionCode(__customTransactionForms,true);
                            
                                public static final PermissionCode _customTransactionTypes =
                                    new PermissionCode(__customTransactionTypes,true);
                            
                                public static final PermissionCode _deferredExpenseRegisters =
                                    new PermissionCode(__deferredExpenseRegisters,true);
                            
                                public static final PermissionCode _deferredExpenseReports =
                                    new PermissionCode(__deferredExpenseReports,true);
                            
                                public static final PermissionCode _deferredRevenueRegisters =
                                    new PermissionCode(__deferredRevenueRegisters,true);
                            
                                public static final PermissionCode _deleteAllData =
                                    new PermissionCode(__deleteAllData,true);
                            
                                public static final PermissionCode _deletedRecords =
                                    new PermissionCode(__deletedRecords,true);
                            
                                public static final PermissionCode _deleteEvent =
                                    new PermissionCode(__deleteEvent,true);
                            
                                public static final PermissionCode _departments =
                                    new PermissionCode(__departments,true);
                            
                                public static final PermissionCode _departmentSegmentMapping =
                                    new PermissionCode(__departmentSegmentMapping,true);
                            
                                public static final PermissionCode _deposit =
                                    new PermissionCode(__deposit,true);
                            
                                public static final PermissionCode _depositApplication =
                                    new PermissionCode(__depositApplication,true);
                            
                                public static final PermissionCode _deviceIdManagement =
                                    new PermissionCode(__deviceIdManagement,true);
                            
                                public static final PermissionCode _directDepositStatus =
                                    new PermissionCode(__directDepositStatus,true);
                            
                                public static final PermissionCode _distributeInventory =
                                    new PermissionCode(__distributeInventory,true);
                            
                                public static final PermissionCode _distributionNetwork =
                                    new PermissionCode(__distributionNetwork,true);
                            
                                public static final PermissionCode _documentsAndFiles =
                                    new PermissionCode(__documentsAndFiles,true);
                            
                                public static final PermissionCode _duplicateCaseManagement =
                                    new PermissionCode(__duplicateCaseManagement,true);
                            
                                public static final PermissionCode _duplicateDetectionSetup =
                                    new PermissionCode(__duplicateDetectionSetup,true);
                            
                                public static final PermissionCode _duplicateRecordManagement =
                                    new PermissionCode(__duplicateRecordManagement,true);
                            
                                public static final PermissionCode _ebayExportImport =
                                    new PermissionCode(__ebayExportImport,true);
                            
                                public static final PermissionCode _editForecast =
                                    new PermissionCode(__editForecast,true);
                            
                                public static final PermissionCode _editManagerForecast =
                                    new PermissionCode(__editManagerForecast,true);
                            
                                public static final PermissionCode _editProfile =
                                    new PermissionCode(__editProfile,true);
                            
                                public static final PermissionCode _eftStatus =
                                    new PermissionCode(__eftStatus,true);
                            
                                public static final PermissionCode _emailReports =
                                    new PermissionCode(__emailReports,true);
                            
                                public static final PermissionCode _emailTemplate =
                                    new PermissionCode(__emailTemplate,true);
                            
                                public static final PermissionCode _employeeAdministration =
                                    new PermissionCode(__employeeAdministration,true);
                            
                                public static final PermissionCode _employeeCenterPublishing =
                                    new PermissionCode(__employeeCenterPublishing,true);
                            
                                public static final PermissionCode _employeeChangeReason =
                                    new PermissionCode(__employeeChangeReason,true);
                            
                                public static final PermissionCode _employeeCommissionSchedulesPlans =
                                    new PermissionCode(__employeeCommissionSchedulesPlans,true);
                            
                                public static final PermissionCode _employeeCommissionTransaction =
                                    new PermissionCode(__employeeCommissionTransaction,true);
                            
                                public static final PermissionCode _employeeCommissionTransactionApproval =
                                    new PermissionCode(__employeeCommissionTransactionApproval,true);
                            
                                public static final PermissionCode _employeeConfidential =
                                    new PermissionCode(__employeeConfidential,true);
                            
                                public static final PermissionCode _employeeEffectiveDating =
                                    new PermissionCode(__employeeEffectiveDating,true);
                            
                                public static final PermissionCode _employeeNavigation =
                                    new PermissionCode(__employeeNavigation,true);
                            
                                public static final PermissionCode _employeePublic =
                                    new PermissionCode(__employeePublic,true);
                            
                                public static final PermissionCode _employeeReminders =
                                    new PermissionCode(__employeeReminders,true);
                            
                                public static final PermissionCode _employees =
                                    new PermissionCode(__employees,true);
                            
                                public static final PermissionCode _employeeSearch =
                                    new PermissionCode(__employeeSearch,true);
                            
                                public static final PermissionCode _employeeSocialSecurityNumbers =
                                    new PermissionCode(__employeeSocialSecurityNumbers,true);
                            
                                public static final PermissionCode _enableFeatures =
                                    new PermissionCode(__enableFeatures,true);
                            
                                public static final PermissionCode _enterCompletions =
                                    new PermissionCode(__enterCompletions,true);
                            
                                public static final PermissionCode _enterOpeningBalances =
                                    new PermissionCode(__enterOpeningBalances,true);
                            
                                public static final PermissionCode _enterVendorCredits =
                                    new PermissionCode(__enterVendorCredits,true);
                            
                                public static final PermissionCode _enterYearToDatePayrollAdjustments =
                                    new PermissionCode(__enterYearToDatePayrollAdjustments,true);
                            
                                public static final PermissionCode _entityAccountMapping =
                                    new PermissionCode(__entityAccountMapping,true);
                            
                                public static final PermissionCode _equityRegisters =
                                    new PermissionCode(__equityRegisters,true);
                            
                                public static final PermissionCode _escalationAssignment =
                                    new PermissionCode(__escalationAssignment,true);
                            
                                public static final PermissionCode _escalationAssignmentRule =
                                    new PermissionCode(__escalationAssignmentRule,true);
                            
                                public static final PermissionCode _establishQuotas =
                                    new PermissionCode(__establishQuotas,true);
                            
                                public static final PermissionCode _estimate =
                                    new PermissionCode(__estimate,true);
                            
                                public static final PermissionCode _events =
                                    new PermissionCode(__events,true);
                            
                                public static final PermissionCode _expenseCategories =
                                    new PermissionCode(__expenseCategories,true);
                            
                                public static final PermissionCode _expenseRegisters =
                                    new PermissionCode(__expenseRegisters,true);
                            
                                public static final PermissionCode _expenseReport =
                                    new PermissionCode(__expenseReport,true);
                            
                                public static final PermissionCode _expenses =
                                    new PermissionCode(__expenses,true);
                            
                                public static final PermissionCode _exportAsIIF =
                                    new PermissionCode(__exportAsIIF,true);
                            
                                public static final PermissionCode _exportLists =
                                    new PermissionCode(__exportLists,true);
                            
                                public static final PermissionCode _fairValueDimension =
                                    new PermissionCode(__fairValueDimension,true);
                            
                                public static final PermissionCode _fairValueFormula =
                                    new PermissionCode(__fairValueFormula,true);
                            
                                public static final PermissionCode _fairValuePrice =
                                    new PermissionCode(__fairValuePrice,true);
                            
                                public static final PermissionCode _faxMessages =
                                    new PermissionCode(__faxMessages,true);
                            
                                public static final PermissionCode _faxTemplate =
                                    new PermissionCode(__faxTemplate,true);
                            
                                public static final PermissionCode _features =
                                    new PermissionCode(__features,true);
                            
                                public static final PermissionCode _financeCharge =
                                    new PermissionCode(__financeCharge,true);
                            
                                public static final PermissionCode _financeChargePreferences =
                                    new PermissionCode(__financeChargePreferences,true);
                            
                                public static final PermissionCode _financialHistory =
                                    new PermissionCode(__financialHistory,true);
                            
                                public static final PermissionCode _financialStatementLayouts =
                                    new PermissionCode(__financialStatementLayouts,true);
                            
                                public static final PermissionCode _financialStatements =
                                    new PermissionCode(__financialStatements,true);
                            
                                public static final PermissionCode _financialStatementSections =
                                    new PermissionCode(__financialStatementSections,true);
                            
                                public static final PermissionCode _findTransaction =
                                    new PermissionCode(__findTransaction,true);
                            
                                public static final PermissionCode _fiscalCalendars =
                                    new PermissionCode(__fiscalCalendars,true);
                            
                                public static final PermissionCode _fixedAssetRegisters =
                                    new PermissionCode(__fixedAssetRegisters,true);
                            
                                public static final PermissionCode _foreignCurrencyVarianceMapping =
                                    new PermissionCode(__foreignCurrencyVarianceMapping,true);
                            
                                public static final PermissionCode _form1099FederalMiscellaneousIncome =
                                    new PermissionCode(__form1099FederalMiscellaneousIncome,true);
                            
                                public static final PermissionCode _form940EmployersAnnualFederalUnemploymentTaxReturn =
                                    new PermissionCode(__form940EmployersAnnualFederalUnemploymentTaxReturn,true);
                            
                                public static final PermissionCode _form941EmployersQuarterlyFederalTaxReturn =
                                    new PermissionCode(__form941EmployersQuarterlyFederalTaxReturn,true);
                            
                                public static final PermissionCode _formW2WageAndTaxStatement =
                                    new PermissionCode(__formW2WageAndTaxStatement,true);
                            
                                public static final PermissionCode _formW4EmployeesWithholdingAllowanceCertificate =
                                    new PermissionCode(__formW4EmployeesWithholdingAllowanceCertificate,true);
                            
                                public static final PermissionCode _fulfillmentRequest =
                                    new PermissionCode(__fulfillmentRequest,true);
                            
                                public static final PermissionCode _fulfillOrders =
                                    new PermissionCode(__fulfillOrders,true);
                            
                                public static final PermissionCode _generalLedger =
                                    new PermissionCode(__generalLedger,true);
                            
                                public static final PermissionCode _generatePriceLists =
                                    new PermissionCode(__generatePriceLists,true);
                            
                                public static final PermissionCode _generateRevenueCommitment =
                                    new PermissionCode(__generateRevenueCommitment,true);
                            
                                public static final PermissionCode _generateRevenueCommitmentReversals =
                                    new PermissionCode(__generateRevenueCommitmentReversals,true);
                            
                                public static final PermissionCode _generateSingleOrderRevenueContracts =
                                    new PermissionCode(__generateSingleOrderRevenueContracts,true);
                            
                                public static final PermissionCode _generateStatements =
                                    new PermissionCode(__generateStatements,true);
                            
                                public static final PermissionCode _genericAdminPermission =
                                    new PermissionCode(__genericAdminPermission,true);
                            
                                public static final PermissionCode _genericResources =
                                    new PermissionCode(__genericResources,true);
                            
                                public static final PermissionCode _globalAccountMapping =
                                    new PermissionCode(__globalAccountMapping,true);
                            
                                public static final PermissionCode _governmentIssuedIdTypes =
                                    new PermissionCode(__governmentIssuedIdTypes,true);
                            
                                public static final PermissionCode _grantingAccessToReports =
                                    new PermissionCode(__grantingAccessToReports,true);
                            
                                public static final PermissionCode _gstSummaryReport =
                                    new PermissionCode(__gstSummaryReport,true);
                            
                                public static final PermissionCode _hideEmployeeInformationOnFinancialReports =
                                    new PermissionCode(__hideEmployeeInformationOnFinancialReports,true);
                            
                                public static final PermissionCode _importCSVFile =
                                    new PermissionCode(__importCSVFile,true);
                            
                                public static final PermissionCode _importOnlineBankingQIFFile =
                                    new PermissionCode(__importOnlineBankingQIFFile,true);
                            
                                public static final PermissionCode _importStateSalesTax =
                                    new PermissionCode(__importStateSalesTax,true);
                            
                                public static final PermissionCode _inboundShipment =
                                    new PermissionCode(__inboundShipment,true);
                            
                                public static final PermissionCode _income =
                                    new PermissionCode(__income,true);
                            
                                public static final PermissionCode _incomeRegisters =
                                    new PermissionCode(__incomeRegisters,true);
                            
                                public static final PermissionCode _incomeStatement =
                                    new PermissionCode(__incomeStatement,true);
                            
                                public static final PermissionCode _individualPaycheck =
                                    new PermissionCode(__individualPaycheck,true);
                            
                                public static final PermissionCode _integration =
                                    new PermissionCode(__integration,true);
                            
                                public static final PermissionCode _integrationApplication =
                                    new PermissionCode(__integrationApplication,true);
                            
                                public static final PermissionCode _integrationApplications =
                                    new PermissionCode(__integrationApplications,true);
                            
                                public static final PermissionCode _intercompanyAdjustments =
                                    new PermissionCode(__intercompanyAdjustments,true);
                            
                                public static final PermissionCode _internalPublisher =
                                    new PermissionCode(__internalPublisher,true);
                            
                                public static final PermissionCode _inventory =
                                    new PermissionCode(__inventory,true);
                            
                                public static final PermissionCode _inventoryCostTemplate =
                                    new PermissionCode(__inventoryCostTemplate,true);
                            
                                public static final PermissionCode _inventoryStatus =
                                    new PermissionCode(__inventoryStatus,true);
                            
                                public static final PermissionCode _inventoryStatusChange =
                                    new PermissionCode(__inventoryStatusChange,true);
                            
                                public static final PermissionCode _invoice =
                                    new PermissionCode(__invoice,true);
                            
                                public static final PermissionCode _invoiceApproval =
                                    new PermissionCode(__invoiceApproval,true);
                            
                                public static final PermissionCode _issueComponents =
                                    new PermissionCode(__issueComponents,true);
                            
                                public static final PermissionCode _issueReports =
                                    new PermissionCode(__issueReports,true);
                            
                                public static final PermissionCode _issues =
                                    new PermissionCode(__issues,true);
                            
                                public static final PermissionCode _issueSetup =
                                    new PermissionCode(__issueSetup,true);
                            
                                public static final PermissionCode _itemAccountMapping =
                                    new PermissionCode(__itemAccountMapping,true);
                            
                                public static final PermissionCode _itemCategoryLayouts =
                                    new PermissionCode(__itemCategoryLayouts,true);
                            
                                public static final PermissionCode _itemDemandPlan =
                                    new PermissionCode(__itemDemandPlan,true);
                            
                                public static final PermissionCode _itemFulfillment =
                                    new PermissionCode(__itemFulfillment,true);
                            
                                public static final PermissionCode _itemReceipt =
                                    new PermissionCode(__itemReceipt,true);
                            
                                public static final PermissionCode _itemRevenueCategory =
                                    new PermissionCode(__itemRevenueCategory,true);
                            
                                public static final PermissionCode _itemRevisions =
                                    new PermissionCode(__itemRevisions,true);
                            
                                public static final PermissionCode _items =
                                    new PermissionCode(__items,true);
                            
                                public static final PermissionCode _itemSupplyPlan =
                                    new PermissionCode(__itemSupplyPlan,true);
                            
                                public static final PermissionCode _itemTemplates =
                                    new PermissionCode(__itemTemplates,true);
                            
                                public static final PermissionCode _jobManagement =
                                    new PermissionCode(__jobManagement,true);
                            
                                public static final PermissionCode _jobRequisitions =
                                    new PermissionCode(__jobRequisitions,true);
                            
                                public static final PermissionCode _jobs =
                                    new PermissionCode(__jobs,true);
                            
                                public static final PermissionCode _journalApproval =
                                    new PermissionCode(__journalApproval,true);
                            
                                public static final PermissionCode _knowledgeBase =
                                    new PermissionCode(__knowledgeBase,true);
                            
                                public static final PermissionCode _kpiScorecards =
                                    new PermissionCode(__kpiScorecards,true);
                            
                                public static final PermissionCode _kudos =
                                    new PermissionCode(__kudos,true);
                            
                                public static final PermissionCode _leadConversion =
                                    new PermissionCode(__leadConversion,true);
                            
                                public static final PermissionCode _leadConversionMapping =
                                    new PermissionCode(__leadConversionMapping,true);
                            
                                public static final PermissionCode _leadSnapshotReminders =
                                    new PermissionCode(__leadSnapshotReminders,true);
                            
                                public static final PermissionCode _letterMessages =
                                    new PermissionCode(__letterMessages,true);
                            
                                public static final PermissionCode _letterTemplate =
                                    new PermissionCode(__letterTemplate,true);
                            
                                public static final PermissionCode _loadSampleData =
                                    new PermissionCode(__loadSampleData,true);
                            
                                public static final PermissionCode _locationCostingGroup =
                                    new PermissionCode(__locationCostingGroup,true);
                            
                                public static final PermissionCode _locations =
                                    new PermissionCode(__locations,true);
                            
                                public static final PermissionCode _locationSegmentMapping =
                                    new PermissionCode(__locationSegmentMapping,true);
                            
                                public static final PermissionCode _lockTransactions =
                                    new PermissionCode(__lockTransactions,true);
                            
                                public static final PermissionCode _logInUsingAccessTokens =
                                    new PermissionCode(__logInUsingAccessTokens,true);
                            
                                public static final PermissionCode _longTermLiabilityRegisters =
                                    new PermissionCode(__longTermLiabilityRegisters,true);
                            
                                public static final PermissionCode _mailMerge =
                                    new PermissionCode(__mailMerge,true);
                            
                                public static final PermissionCode _makeJournalEntry =
                                    new PermissionCode(__makeJournalEntry,true);
                            
                                public static final PermissionCode _manageAccountingPeriods =
                                    new PermissionCode(__manageAccountingPeriods,true);
                            
                                public static final PermissionCode _manageCustomPermissions =
                                    new PermissionCode(__manageCustomPermissions,true);
                            
                                public static final PermissionCode _managePayroll =
                                    new PermissionCode(__managePayroll,true);
                            
                                public static final PermissionCode _manageRoles =
                                    new PermissionCode(__manageRoles,true);
                            
                                public static final PermissionCode _manageTaxReportingPeriods =
                                    new PermissionCode(__manageTaxReportingPeriods,true);
                            
                                public static final PermissionCode _manageUsers =
                                    new PermissionCode(__manageUsers,true);
                            
                                public static final PermissionCode _manufacturingCostTemplate =
                                    new PermissionCode(__manufacturingCostTemplate,true);
                            
                                public static final PermissionCode _manufacturingRouting =
                                    new PermissionCode(__manufacturingRouting,true);
                            
                                public static final PermissionCode _marketingCampaignReports =
                                    new PermissionCode(__marketingCampaignReports,true);
                            
                                public static final PermissionCode _marketingCampaigns =
                                    new PermissionCode(__marketingCampaigns,true);
                            
                                public static final PermissionCode _marketingTemplate =
                                    new PermissionCode(__marketingTemplate,true);
                            
                                public static final PermissionCode _markIssueAsShowStopper =
                                    new PermissionCode(__markIssueAsShowStopper,true);
                            
                                public static final PermissionCode _markWorkOrdersBuilt =
                                    new PermissionCode(__markWorkOrdersBuilt,true);
                            
                                public static final PermissionCode _markWorkOrdersFirmed =
                                    new PermissionCode(__markWorkOrdersFirmed,true);
                            
                                public static final PermissionCode _markWorkOrdersReleased =
                                    new PermissionCode(__markWorkOrdersReleased,true);
                            
                                public static final PermissionCode _massUpdates =
                                    new PermissionCode(__massUpdates,true);
                            
                                public static final PermissionCode _matchingRulesForOnlineBanking =
                                    new PermissionCode(__matchingRulesForOnlineBanking,true);
                            
                                public static final PermissionCode _mediaFolders =
                                    new PermissionCode(__mediaFolders,true);
                            
                                public static final PermissionCode _memorizedTransactions =
                                    new PermissionCode(__memorizedTransactions,true);
                            
                                public static final PermissionCode _mobileDeviceAccess =
                                    new PermissionCode(__mobileDeviceAccess,true);
                            
                                public static final PermissionCode _netWorth =
                                    new PermissionCode(__netWorth,true);
                            
                                public static final PermissionCode _nextGenAnalytics =
                                    new PermissionCode(__nextGenAnalytics,true);
                            
                                public static final PermissionCode _nonPostingRegisters =
                                    new PermissionCode(__nonPostingRegisters,true);
                            
                                public static final PermissionCode _noPermissionNecessary =
                                    new PermissionCode(__noPermissionNecessary,true);
                            
                                public static final PermissionCode _notesTab =
                                    new PermissionCode(__notesTab,true);
                            
                                public static final PermissionCode _notifications =
                                    new PermissionCode(__notifications,true);
                            
                                public static final PermissionCode _offlineClient =
                                    new PermissionCode(__offlineClient,true);
                            
                                public static final PermissionCode _onlineCaseForm =
                                    new PermissionCode(__onlineCaseForm,true);
                            
                                public static final PermissionCode _onlineCustomerForm =
                                    new PermissionCode(__onlineCustomerForm,true);
                            
                                public static final PermissionCode _onlineCustomRecordForm =
                                    new PermissionCode(__onlineCustomRecordForm,true);
                            
                                public static final PermissionCode _openidSingleSignOn =
                                    new PermissionCode(__openidSingleSignOn,true);
                            
                                public static final PermissionCode _opportunity =
                                    new PermissionCode(__opportunity,true);
                            
                                public static final PermissionCode _organizationValue =
                                    new PermissionCode(__organizationValue,true);
                            
                                public static final PermissionCode _otherAssetRegisters =
                                    new PermissionCode(__otherAssetRegisters,true);
                            
                                public static final PermissionCode _otherCurrentAssetRegisters =
                                    new PermissionCode(__otherCurrentAssetRegisters,true);
                            
                                public static final PermissionCode _otherCurrentLiabilityRegisters =
                                    new PermissionCode(__otherCurrentLiabilityRegisters,true);
                            
                                public static final PermissionCode _otherCustomFields =
                                    new PermissionCode(__otherCustomFields,true);
                            
                                public static final PermissionCode _otherExpenseRegisters =
                                    new PermissionCode(__otherExpenseRegisters,true);
                            
                                public static final PermissionCode _otherIncomeRegisters =
                                    new PermissionCode(__otherIncomeRegisters,true);
                            
                                public static final PermissionCode _otherLists =
                                    new PermissionCode(__otherLists,true);
                            
                                public static final PermissionCode _otherNames =
                                    new PermissionCode(__otherNames,true);
                            
                                public static final PermissionCode _outlookIntegration =
                                    new PermissionCode(__outlookIntegration,true);
                            
                                public static final PermissionCode _outlookIntegration3 =
                                    new PermissionCode(__outlookIntegration3,true);
                            
                                public static final PermissionCode _overrideEstimatedCostOnTransactions =
                                    new PermissionCode(__overrideEstimatedCostOnTransactions,true);
                            
                                public static final PermissionCode _overridePaymentHold =
                                    new PermissionCode(__overridePaymentHold,true);
                            
                                public static final PermissionCode _overridePeriodRestrictions =
                                    new PermissionCode(__overridePeriodRestrictions,true);
                            
                                public static final PermissionCode _ownershipTransfer =
                                    new PermissionCode(__ownershipTransfer,true);
                            
                                public static final PermissionCode _partnerAuthorizedCommissionReports =
                                    new PermissionCode(__partnerAuthorizedCommissionReports,true);
                            
                                public static final PermissionCode _partnerCommissionReports =
                                    new PermissionCode(__partnerCommissionReports,true);
                            
                                public static final PermissionCode _partnerCommissionSchedulesPlans =
                                    new PermissionCode(__partnerCommissionSchedulesPlans,true);
                            
                                public static final PermissionCode _partnerCommissionTransaction =
                                    new PermissionCode(__partnerCommissionTransaction,true);
                            
                                public static final PermissionCode _partnerCommissionTransactionApproval =
                                    new PermissionCode(__partnerCommissionTransactionApproval,true);
                            
                                public static final PermissionCode _partnerContribution =
                                    new PermissionCode(__partnerContribution,true);
                            
                                public static final PermissionCode _partners =
                                    new PermissionCode(__partners,true);
                            
                                public static final PermissionCode _payBills =
                                    new PermissionCode(__payBills,true);
                            
                                public static final PermissionCode _paycheckJournal =
                                    new PermissionCode(__paycheckJournal,true);
                            
                                public static final PermissionCode _paychecks =
                                    new PermissionCode(__paychecks,true);
                            
                                public static final PermissionCode _paymentMethods =
                                    new PermissionCode(__paymentMethods,true);
                            
                                public static final PermissionCode _payrollCheckRegister =
                                    new PermissionCode(__payrollCheckRegister,true);
                            
                                public static final PermissionCode _payrollHoursAndEarnings =
                                    new PermissionCode(__payrollHoursAndEarnings,true);
                            
                                public static final PermissionCode _payrollItems =
                                    new PermissionCode(__payrollItems,true);
                            
                                public static final PermissionCode _payrollJournalReport =
                                    new PermissionCode(__payrollJournalReport,true);
                            
                                public static final PermissionCode _payrollLiabilityPayments =
                                    new PermissionCode(__payrollLiabilityPayments,true);
                            
                                public static final PermissionCode _payrollLiabilityReport =
                                    new PermissionCode(__payrollLiabilityReport,true);
                            
                                public static final PermissionCode _payrollStateWithholding =
                                    new PermissionCode(__payrollStateWithholding,true);
                            
                                public static final PermissionCode _payrollSummaryAndDetailReports =
                                    new PermissionCode(__payrollSummaryAndDetailReports,true);
                            
                                public static final PermissionCode _paySalesTax =
                                    new PermissionCode(__paySalesTax,true);
                            
                                public static final PermissionCode _payTaxLiability =
                                    new PermissionCode(__payTaxLiability,true);
                            
                                public static final PermissionCode _pdfMessages =
                                    new PermissionCode(__pdfMessages,true);
                            
                                public static final PermissionCode _pdfTemplate =
                                    new PermissionCode(__pdfTemplate,true);
                            
                                public static final PermissionCode _performSearch =
                                    new PermissionCode(__performSearch,true);
                            
                                public static final PermissionCode _periodClosingManagement =
                                    new PermissionCode(__periodClosingManagement,true);
                            
                                public static final PermissionCode _persistSearch =
                                    new PermissionCode(__persistSearch,true);
                            
                                public static final PermissionCode _phasedProcesses =
                                    new PermissionCode(__phasedProcesses,true);
                            
                                public static final PermissionCode _phoneCalls =
                                    new PermissionCode(__phoneCalls,true);
                            
                                public static final PermissionCode _plannedRevenue =
                                    new PermissionCode(__plannedRevenue,true);
                            
                                public static final PermissionCode _plannedStandardCost =
                                    new PermissionCode(__plannedStandardCost,true);
                            
                                public static final PermissionCode _positions =
                                    new PermissionCode(__positions,true);
                            
                                public static final PermissionCode _postingPeriodOnTransactions =
                                    new PermissionCode(__postingPeriodOnTransactions,true);
                            
                                public static final PermissionCode _postTime =
                                    new PermissionCode(__postTime,true);
                            
                                public static final PermissionCode _postVendorBillVariances =
                                    new PermissionCode(__postVendorBillVariances,true);
                            
                                public static final PermissionCode _presentationCategories =
                                    new PermissionCode(__presentationCategories,true);
                            
                                public static final PermissionCode _printChecksAndForms =
                                    new PermissionCode(__printChecksAndForms,true);
                            
                                public static final PermissionCode _printEmailFax =
                                    new PermissionCode(__printEmailFax,true);
                            
                                public static final PermissionCode _printShipmentDocuments =
                                    new PermissionCode(__printShipmentDocuments,true);
                            
                                public static final PermissionCode _processGSTRefund =
                                    new PermissionCode(__processGSTRefund,true);
                            
                                public static final PermissionCode _processPayroll =
                                    new PermissionCode(__processPayroll,true);
                            
                                public static final PermissionCode _projectProfitability =
                                    new PermissionCode(__projectProfitability,true);
                            
                                public static final PermissionCode _projectProfitabilitySetup =
                                    new PermissionCode(__projectProfitabilitySetup,true);
                            
                                public static final PermissionCode _projectProjectTemplateConversion =
                                    new PermissionCode(__projectProjectTemplateConversion,true);
                            
                                public static final PermissionCode _projectRevenueRules =
                                    new PermissionCode(__projectRevenueRules,true);
                            
                                public static final PermissionCode _projectTasks =
                                    new PermissionCode(__projectTasks,true);
                            
                                public static final PermissionCode _projectTemplates =
                                    new PermissionCode(__projectTemplates,true);
                            
                                public static final PermissionCode _promotionCode =
                                    new PermissionCode(__promotionCode,true);
                            
                                public static final PermissionCode _provisioning =
                                    new PermissionCode(__provisioning,true);
                            
                                public static final PermissionCode _provisioningForQa =
                                    new PermissionCode(__provisioningForQa,true);
                            
                                public static final PermissionCode _provisionNewAccountOnTestdrive =
                                    new PermissionCode(__provisionNewAccountOnTestdrive,true);
                            
                                public static final PermissionCode _provisionTestDrive =
                                    new PermissionCode(__provisionTestDrive,true);
                            
                                public static final PermissionCode _pstSummaryReport =
                                    new PermissionCode(__pstSummaryReport,true);
                            
                                public static final PermissionCode _publicTemplateCategories =
                                    new PermissionCode(__publicTemplateCategories,true);
                            
                                public static final PermissionCode _publishDashboards =
                                    new PermissionCode(__publishDashboards,true);
                            
                                public static final PermissionCode _publishEmployeeList =
                                    new PermissionCode(__publishEmployeeList,true);
                            
                                public static final PermissionCode _publishForms =
                                    new PermissionCode(__publishForms,true);
                            
                                public static final PermissionCode _publishKnowledgeBase =
                                    new PermissionCode(__publishKnowledgeBase,true);
                            
                                public static final PermissionCode _publishRSSFeeds =
                                    new PermissionCode(__publishRSSFeeds,true);
                            
                                public static final PermissionCode _publishSearch =
                                    new PermissionCode(__publishSearch,true);
                            
                                public static final PermissionCode _purchaseContract =
                                    new PermissionCode(__purchaseContract,true);
                            
                                public static final PermissionCode _purchaseContractApproval =
                                    new PermissionCode(__purchaseContractApproval,true);
                            
                                public static final PermissionCode _purchaseOrder =
                                    new PermissionCode(__purchaseOrder,true);
                            
                                public static final PermissionCode _purchaseOrderReports =
                                    new PermissionCode(__purchaseOrderReports,true);
                            
                                public static final PermissionCode _purchases =
                                    new PermissionCode(__purchases,true);
                            
                                public static final PermissionCode _quantityPricingSchedules =
                                    new PermissionCode(__quantityPricingSchedules,true);
                            
                                public static final PermissionCode _quotaReports =
                                    new PermissionCode(__quotaReports,true);
                            
                                public static final PermissionCode _receiveOrder =
                                    new PermissionCode(__receiveOrder,true);
                            
                                public static final PermissionCode _receiveReturns =
                                    new PermissionCode(__receiveReturns,true);
                            
                                public static final PermissionCode _recognizeGiftCertificateIncome =
                                    new PermissionCode(__recognizeGiftCertificateIncome,true);
                            
                                public static final PermissionCode _reconcile =
                                    new PermissionCode(__reconcile,true);
                            
                                public static final PermissionCode _reconcileReporting =
                                    new PermissionCode(__reconcileReporting,true);
                            
                                public static final PermissionCode _recordCustomField =
                                    new PermissionCode(__recordCustomField,true);
                            
                                public static final PermissionCode _refundReturns =
                                    new PermissionCode(__refundReturns,true);
                            
                                public static final PermissionCode _relatedItems =
                                    new PermissionCode(__relatedItems,true);
                            
                                public static final PermissionCode _reportCustomization =
                                    new PermissionCode(__reportCustomization,true);
                            
                                public static final PermissionCode _reportScheduling =
                                    new PermissionCode(__reportScheduling,true);
                            
                                public static final PermissionCode _requestForQuote =
                                    new PermissionCode(__requestForQuote,true);
                            
                                public static final PermissionCode _requisition =
                                    new PermissionCode(__requisition,true);
                            
                                public static final PermissionCode _requisitionApproval =
                                    new PermissionCode(__requisitionApproval,true);
                            
                                public static final PermissionCode _resource =
                                    new PermissionCode(__resource,true);
                            
                                public static final PermissionCode _resourceAllocationApproval =
                                    new PermissionCode(__resourceAllocationApproval,true);
                            
                                public static final PermissionCode _resourceAllocationReports =
                                    new PermissionCode(__resourceAllocationReports,true);
                            
                                public static final PermissionCode _resourceAllocations =
                                    new PermissionCode(__resourceAllocations,true);
                            
                                public static final PermissionCode _returnAuthApproval =
                                    new PermissionCode(__returnAuthApproval,true);
                            
                                public static final PermissionCode _returnAuthorization =
                                    new PermissionCode(__returnAuthorization,true);
                            
                                public static final PermissionCode _returnAuthorizationReports =
                                    new PermissionCode(__returnAuthorizationReports,true);
                            
                                public static final PermissionCode _revalueInventoryCost =
                                    new PermissionCode(__revalueInventoryCost,true);
                            
                                public static final PermissionCode _revenueArrangement =
                                    new PermissionCode(__revenueArrangement,true);
                            
                                public static final PermissionCode _revenueArrangementApproval =
                                    new PermissionCode(__revenueArrangementApproval,true);
                            
                                public static final PermissionCode _revenueCommitment =
                                    new PermissionCode(__revenueCommitment,true);
                            
                                public static final PermissionCode _revenueCommitmentReversal =
                                    new PermissionCode(__revenueCommitmentReversal,true);
                            
                                public static final PermissionCode _revenueContracts =
                                    new PermissionCode(__revenueContracts,true);
                            
                                public static final PermissionCode _revenueElement =
                                    new PermissionCode(__revenueElement,true);
                            
                                public static final PermissionCode _revenueManagementVSOE =
                                    new PermissionCode(__revenueManagementVSOE,true);
                            
                                public static final PermissionCode _revenueRecognitionFieldMapping =
                                    new PermissionCode(__revenueRecognitionFieldMapping,true);
                            
                                public static final PermissionCode _revenueRecognitionPlan =
                                    new PermissionCode(__revenueRecognitionPlan,true);
                            
                                public static final PermissionCode _revenueRecognitionReports =
                                    new PermissionCode(__revenueRecognitionReports,true);
                            
                                public static final PermissionCode _revenueRecognitionRule =
                                    new PermissionCode(__revenueRecognitionRule,true);
                            
                                public static final PermissionCode _revenueRecognitionSchedules =
                                    new PermissionCode(__revenueRecognitionSchedules,true);
                            
                                public static final PermissionCode _reviewCustomGlPlugInExecutions =
                                    new PermissionCode(__reviewCustomGlPlugInExecutions,true);
                            
                                public static final PermissionCode _roles =
                                    new PermissionCode(__roles,true);
                            
                                public static final PermissionCode _runPayroll =
                                    new PermissionCode(__runPayroll,true);
                            
                                public static final PermissionCode _sales =
                                    new PermissionCode(__sales,true);
                            
                                public static final PermissionCode _salesByPartner =
                                    new PermissionCode(__salesByPartner,true);
                            
                                public static final PermissionCode _salesByPromotionCode =
                                    new PermissionCode(__salesByPromotionCode,true);
                            
                                public static final PermissionCode _salesCampaigns =
                                    new PermissionCode(__salesCampaigns,true);
                            
                                public static final PermissionCode _salesForceAutomation =
                                    new PermissionCode(__salesForceAutomation,true);
                            
                                public static final PermissionCode _salesForceAutomationSetup =
                                    new PermissionCode(__salesForceAutomationSetup,true);
                            
                                public static final PermissionCode _salesOrder =
                                    new PermissionCode(__salesOrder,true);
                            
                                public static final PermissionCode _salesOrderApproval =
                                    new PermissionCode(__salesOrderApproval,true);
                            
                                public static final PermissionCode _salesOrderFulfillmentReports =
                                    new PermissionCode(__salesOrderFulfillmentReports,true);
                            
                                public static final PermissionCode _salesOrderReports =
                                    new PermissionCode(__salesOrderReports,true);
                            
                                public static final PermissionCode _salesOrderTransactionReport =
                                    new PermissionCode(__salesOrderTransactionReport,true);
                            
                                public static final PermissionCode _salesRoles =
                                    new PermissionCode(__salesRoles,true);
                            
                                public static final PermissionCode _salesTerritory =
                                    new PermissionCode(__salesTerritory,true);
                            
                                public static final PermissionCode _salesTerritoryRule =
                                    new PermissionCode(__salesTerritoryRule,true);
                            
                                public static final PermissionCode _samlSingleSignOn =
                                    new PermissionCode(__samlSingleSignOn,true);
                            
                                public static final PermissionCode _scheduleMassUpdates =
                                    new PermissionCode(__scheduleMassUpdates,true);
                            
                                public static final PermissionCode _setUpAccounting =
                                    new PermissionCode(__setUpAccounting,true);
                            
                                public static final PermissionCode _setUpAchProcessing =
                                    new PermissionCode(__setUpAchProcessing,true);
                            
                                public static final PermissionCode _setUpAdpPayroll =
                                    new PermissionCode(__setUpAdpPayroll,true);
                            
                                public static final PermissionCode _setUpBillPay =
                                    new PermissionCode(__setUpBillPay,true);
                            
                                public static final PermissionCode _setUpBudgets =
                                    new PermissionCode(__setUpBudgets,true);
                            
                                public static final PermissionCode _setUpCampaignEmailAddresses =
                                    new PermissionCode(__setUpCampaignEmailAddresses,true);
                            
                                public static final PermissionCode _setupCampaigns =
                                    new PermissionCode(__setupCampaigns,true);
                            
                                public static final PermissionCode _setUpCompany =
                                    new PermissionCode(__setUpCompany,true);
                            
                                public static final PermissionCode _setUpCsvPreferences =
                                    new PermissionCode(__setUpCsvPreferences,true);
                            
                                public static final PermissionCode _setUpDomains =
                                    new PermissionCode(__setUpDomains,true);
                            
                                public static final PermissionCode _setUpEbay =
                                    new PermissionCode(__setUpEbay,true);
                            
                                public static final PermissionCode _setUpImageResizing =
                                    new PermissionCode(__setUpImageResizing,true);
                            
                                public static final PermissionCode _setUpOpenidSingleSignOn =
                                    new PermissionCode(__setUpOpenidSingleSignOn,true);
                            
                                public static final PermissionCode _setUpPayroll =
                                    new PermissionCode(__setUpPayroll,true);
                            
                                public static final PermissionCode _setUpReminders =
                                    new PermissionCode(__setUpReminders,true);
                            
                                public static final PermissionCode _setUpSamlSingleSignOn =
                                    new PermissionCode(__setUpSamlSingleSignOn,true);
                            
                                public static final PermissionCode _setUpSnapshots =
                                    new PermissionCode(__setUpSnapshots,true);
                            
                                public static final PermissionCode _setUpSynchronization =
                                    new PermissionCode(__setUpSynchronization,true);
                            
                                public static final PermissionCode _setUpWebServices =
                                    new PermissionCode(__setUpWebServices,true);
                            
                                public static final PermissionCode _setUpWebSite =
                                    new PermissionCode(__setUpWebSite,true);
                            
                                public static final PermissionCode _setUpYearStatus =
                                    new PermissionCode(__setUpYearStatus,true);
                            
                                public static final PermissionCode _shippingItems =
                                    new PermissionCode(__shippingItems,true);
                            
                                public static final PermissionCode _shippingPartnerPackage =
                                    new PermissionCode(__shippingPartnerPackage,true);
                            
                                public static final PermissionCode _shippingPartnerRegistration =
                                    new PermissionCode(__shippingPartnerRegistration,true);
                            
                                public static final PermissionCode _shippingPartnerShipment =
                                    new PermissionCode(__shippingPartnerShipment,true);
                            
                                public static final PermissionCode _shortcuts =
                                    new PermissionCode(__shortcuts,true);
                            
                                public static final PermissionCode _standardCostVersion =
                                    new PermissionCode(__standardCostVersion,true);
                            
                                public static final PermissionCode _statementCharge =
                                    new PermissionCode(__statementCharge,true);
                            
                                public static final PermissionCode _statisticalAccountRegisters =
                                    new PermissionCode(__statisticalAccountRegisters,true);
                            
                                public static final PermissionCode _storeCategories =
                                    new PermissionCode(__storeCategories,true);
                            
                                public static final PermissionCode _storeContentCategories =
                                    new PermissionCode(__storeContentCategories,true);
                            
                                public static final PermissionCode _storeContentItems =
                                    new PermissionCode(__storeContentItems,true);
                            
                                public static final PermissionCode _storeLogoUpload =
                                    new PermissionCode(__storeLogoUpload,true);
                            
                                public static final PermissionCode _storePickupFulfillment =
                                    new PermissionCode(__storePickupFulfillment,true);
                            
                                public static final PermissionCode _storeTabs =
                                    new PermissionCode(__storeTabs,true);
                            
                                public static final PermissionCode _subscriptionPlan =
                                    new PermissionCode(__subscriptionPlan,true);
                            
                                public static final PermissionCode _subscriptions =
                                    new PermissionCode(__subscriptions,true);
                            
                                public static final PermissionCode _subsidiaries =
                                    new PermissionCode(__subsidiaries,true);
                            
                                public static final PermissionCode _subsidiaryTaxEngineSelection =
                                    new PermissionCode(__subsidiaryTaxEngineSelection,true);
                            
                                public static final PermissionCode _suiteAppDeployment =
                                    new PermissionCode(__suiteAppDeployment,true);
                            
                                public static final PermissionCode _suiteBundler =
                                    new PermissionCode(__suiteBundler,true);
                            
                                public static final PermissionCode _suiteBundlerAuditTrail =
                                    new PermissionCode(__suiteBundlerAuditTrail,true);
                            
                                public static final PermissionCode _suiteBundlerUpgrades =
                                    new PermissionCode(__suiteBundlerUpgrades,true);
                            
                                public static final PermissionCode _suiteScript =
                                    new PermissionCode(__suiteScript,true);
                            
                                public static final PermissionCode _suiteScriptNlCorpManagement =
                                    new PermissionCode(__suiteScriptNlCorpManagement,true);
                            
                                public static final PermissionCode _suiteScriptScheduling =
                                    new PermissionCode(__suiteScriptScheduling,true);
                            
                                public static final PermissionCode _suiteSignon =
                                    new PermissionCode(__suiteSignon,true);
                            
                                public static final PermissionCode _support =
                                    new PermissionCode(__support,true);
                            
                                public static final PermissionCode _supportCaseIssue =
                                    new PermissionCode(__supportCaseIssue,true);
                            
                                public static final PermissionCode _supportCaseOrigin =
                                    new PermissionCode(__supportCaseOrigin,true);
                            
                                public static final PermissionCode _supportCasePriority =
                                    new PermissionCode(__supportCasePriority,true);
                            
                                public static final PermissionCode _supportCaseSnapshotReminders =
                                    new PermissionCode(__supportCaseSnapshotReminders,true);
                            
                                public static final PermissionCode _supportCaseStatus =
                                    new PermissionCode(__supportCaseStatus,true);
                            
                                public static final PermissionCode _supportCaseTerritory =
                                    new PermissionCode(__supportCaseTerritory,true);
                            
                                public static final PermissionCode _supportCaseTerritoryRule =
                                    new PermissionCode(__supportCaseTerritoryRule,true);
                            
                                public static final PermissionCode _supportCaseType =
                                    new PermissionCode(__supportCaseType,true);
                            
                                public static final PermissionCode _supportSetup =
                                    new PermissionCode(__supportSetup,true);
                            
                                public static final PermissionCode _swapPricesBetweenPriceLevels =
                                    new PermissionCode(__swapPricesBetweenPriceLevels,true);
                            
                                public static final PermissionCode _systemEmailTemplate =
                                    new PermissionCode(__systemEmailTemplate,true);
                            
                                public static final PermissionCode _systemStatus =
                                    new PermissionCode(__systemStatus,true);
                            
                                public static final PermissionCode _tableauWorkbookExport =
                                    new PermissionCode(__tableauWorkbookExport,true);
                            
                                public static final PermissionCode _tasks =
                                    new PermissionCode(__tasks,true);
                            
                                public static final PermissionCode _tax =
                                    new PermissionCode(__tax,true);
                            
                                public static final PermissionCode _taxDetailsTab =
                                    new PermissionCode(__taxDetailsTab,true);
                            
                                public static final PermissionCode _taxItems =
                                    new PermissionCode(__taxItems,true);
                            
                                public static final PermissionCode _taxReports =
                                    new PermissionCode(__taxReports,true);
                            
                                public static final PermissionCode _taxSchedules =
                                    new PermissionCode(__taxSchedules,true);
                            
                                public static final PermissionCode _teamSellingContribution =
                                    new PermissionCode(__teamSellingContribution,true);
                            
                                public static final PermissionCode _tegataAccounts =
                                    new PermissionCode(__tegataAccounts,true);
                            
                                public static final PermissionCode _tegataPayable =
                                    new PermissionCode(__tegataPayable,true);
                            
                                public static final PermissionCode _tegataReceivable =
                                    new PermissionCode(__tegataReceivable,true);
                            
                                public static final PermissionCode _telephonyIntegration =
                                    new PermissionCode(__telephonyIntegration,true);
                            
                                public static final PermissionCode _templateCategories =
                                    new PermissionCode(__templateCategories,true);
                            
                                public static final PermissionCode _terminationReasons =
                                    new PermissionCode(__terminationReasons,true);
                            
                                public static final PermissionCode _testdriveMasters =
                                    new PermissionCode(__testdriveMasters,true);
                            
                                public static final PermissionCode _timeOffAdministration =
                                    new PermissionCode(__timeOffAdministration,true);
                            
                                public static final PermissionCode _timer =
                                    new PermissionCode(__timer,true);
                            
                                public static final PermissionCode _timeTracking =
                                    new PermissionCode(__timeTracking,true);
                            
                                public static final PermissionCode _trackMessages =
                                    new PermissionCode(__trackMessages,true);
                            
                                public static final PermissionCode _trackTime =
                                    new PermissionCode(__trackTime,true);
                            
                                public static final PermissionCode _transactionDetail =
                                    new PermissionCode(__transactionDetail,true);
                            
                                public static final PermissionCode _transactionNumberingAuditLog =
                                    new PermissionCode(__transactionNumberingAuditLog,true);
                            
                                public static final PermissionCode _transactionReceiveInboundShipment =
                                    new PermissionCode(__transactionReceiveInboundShipment,true);
                            
                                public static final PermissionCode _transferFunds =
                                    new PermissionCode(__transferFunds,true);
                            
                                public static final PermissionCode _transferInventory =
                                    new PermissionCode(__transferInventory,true);
                            
                                public static final PermissionCode _transferOrder =
                                    new PermissionCode(__transferOrder,true);
                            
                                public static final PermissionCode _transferOrderApproval =
                                    new PermissionCode(__transferOrderApproval,true);
                            
                                public static final PermissionCode _translation =
                                    new PermissionCode(__translation,true);
                            
                                public static final PermissionCode _trialBalance =
                                    new PermissionCode(__trialBalance,true);
                            
                                public static final PermissionCode _twoFactorAuthentication =
                                    new PermissionCode(__twoFactorAuthentication,true);
                            
                                public static final PermissionCode _twoFactorAuthenticationBase =
                                    new PermissionCode(__twoFactorAuthenticationBase,true);
                            
                                public static final PermissionCode _unbilledReceivableRegisters =
                                    new PermissionCode(__unbilledReceivableRegisters,true);
                            
                                public static final PermissionCode _unbuildAssemblies =
                                    new PermissionCode(__unbuildAssemblies,true);
                            
                                public static final PermissionCode _uncategorizedPresentationItems =
                                    new PermissionCode(__uncategorizedPresentationItems,true);
                            
                                public static final PermissionCode _undeliveredEmails =
                                    new PermissionCode(__undeliveredEmails,true);
                            
                                public static final PermissionCode _units =
                                    new PermissionCode(__units,true);
                            
                                public static final PermissionCode _updatePrices =
                                    new PermissionCode(__updatePrices,true);
                            
                                public static final PermissionCode _upsellAssistant =
                                    new PermissionCode(__upsellAssistant,true);
                            
                                public static final PermissionCode _upsellSetup =
                                    new PermissionCode(__upsellSetup,true);
                            
                                public static final PermissionCode _upsellWizard =
                                    new PermissionCode(__upsellWizard,true);
                            
                                public static final PermissionCode _usage =
                                    new PermissionCode(__usage,true);
                            
                                public static final PermissionCode _userAccessTokens =
                                    new PermissionCode(__userAccessTokens,true);
                            
                                public static final PermissionCode _userPreferences =
                                    new PermissionCode(__userPreferences,true);
                            
                                public static final PermissionCode _usersAndPasswords =
                                    new PermissionCode(__usersAndPasswords,true);
                            
                                public static final PermissionCode _vendorBillApproval =
                                    new PermissionCode(__vendorBillApproval,true);
                            
                                public static final PermissionCode _vendorPaymentStatus =
                                    new PermissionCode(__vendorPaymentStatus,true);
                            
                                public static final PermissionCode _vendorRequestForQuote =
                                    new PermissionCode(__vendorRequestForQuote,true);
                            
                                public static final PermissionCode _vendorReturnAuthApproval =
                                    new PermissionCode(__vendorReturnAuthApproval,true);
                            
                                public static final PermissionCode _vendorReturnAuthorization =
                                    new PermissionCode(__vendorReturnAuthorization,true);
                            
                                public static final PermissionCode _vendorReturns =
                                    new PermissionCode(__vendorReturns,true);
                            
                                public static final PermissionCode _vendors =
                                    new PermissionCode(__vendors,true);
                            
                                public static final PermissionCode _viewGatewayAsynchronousNotifications =
                                    new PermissionCode(__viewGatewayAsynchronousNotifications,true);
                            
                                public static final PermissionCode _viewLoginAuditTrail =
                                    new PermissionCode(__viewLoginAuditTrail,true);
                            
                                public static final PermissionCode _viewOnlineBillPayStatus =
                                    new PermissionCode(__viewOnlineBillPayStatus,true);
                            
                                public static final PermissionCode _viewPaymentEvents =
                                    new PermissionCode(__viewPaymentEvents,true);
                            
                                public static final PermissionCode _viewUnencryptedCreditCards =
                                    new PermissionCode(__viewUnencryptedCreditCards,true);
                            
                                public static final PermissionCode _viewWebServicesLogs =
                                    new PermissionCode(__viewWebServicesLogs,true);
                            
                                public static final PermissionCode _webServices =
                                    new PermissionCode(__webServices,true);
                            
                                public static final PermissionCode _webSiteExternalPublisher =
                                    new PermissionCode(__webSiteExternalPublisher,true);
                            
                                public static final PermissionCode _webSiteManagement =
                                    new PermissionCode(__webSiteManagement,true);
                            
                                public static final PermissionCode _webSiteReport =
                                    new PermissionCode(__webSiteReport,true);
                            
                                public static final PermissionCode _webStoreEmailTemplate =
                                    new PermissionCode(__webStoreEmailTemplate,true);
                            
                                public static final PermissionCode _webStoreReport =
                                    new PermissionCode(__webStoreReport,true);
                            
                                public static final PermissionCode _workCalendar =
                                    new PermissionCode(__workCalendar,true);
                            
                                public static final PermissionCode _workflow =
                                    new PermissionCode(__workflow,true);
                            
                                public static final PermissionCode _workOrder =
                                    new PermissionCode(__workOrder,true);
                            
                                public static final PermissionCode _workOrderClose =
                                    new PermissionCode(__workOrderClose,true);
                            
                                public static final PermissionCode _workOrderCompletion =
                                    new PermissionCode(__workOrderCompletion,true);
                            
                                public static final PermissionCode _workOrderIssue =
                                    new PermissionCode(__workOrderIssue,true);
                            
                                public static final PermissionCode _workplaces =
                                    new PermissionCode(__workplaces,true);
                            

                                public java.lang.String getValue() { return localPermissionCode;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localPermissionCode.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.common_2017_2.platform.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":PermissionCode",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "PermissionCode",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localPermissionCode==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("PermissionCode cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localPermissionCode);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.common_2017_2.platform.webservices.netsuite.com")){
                return "ns6";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPermissionCode)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static PermissionCode fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    PermissionCode enumeration = (PermissionCode)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static PermissionCode fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static PermissionCode fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return PermissionCode.Factory.fromString(content,namespaceUri);
                    } else {
                       return PermissionCode.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static PermissionCode parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            PermissionCode object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"PermissionCode" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = PermissionCode.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = PermissionCode.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    