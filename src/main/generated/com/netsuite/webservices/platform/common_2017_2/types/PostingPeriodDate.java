
/**
 * PostingPeriodDate.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2.types;
            

            /**
            *  PostingPeriodDate bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class PostingPeriodDate
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.common_2017_2.platform.webservices.netsuite.com",
                "PostingPeriodDate",
                "ns6");

            

                        /**
                        * field for PostingPeriodDate
                        */

                        
                                    protected java.lang.String localPostingPeriodDate ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected PostingPeriodDate(java.lang.String value, boolean isRegisterValue) {
                                    localPostingPeriodDate = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localPostingPeriodDate, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String __firstFiscalQuarterOfLastFy =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_firstFiscalQuarterOfLastFy");
                                
                                    public static final java.lang.String __firstFiscalQuarterOfThisFy =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_firstFiscalQuarterOfThisFy");
                                
                                    public static final java.lang.String __fiscalQuarterBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_fiscalQuarterBeforeLast");
                                
                                    public static final java.lang.String __fiscalYearBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_fiscalYearBeforeLast");
                                
                                    public static final java.lang.String __fourthFiscalQuarterOfLastFy =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_fourthFiscalQuarterOfLastFy");
                                
                                    public static final java.lang.String __fourthFiscalQuarterOfThisFy =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_fourthFiscalQuarterOfThisFy");
                                
                                    public static final java.lang.String __lastFiscalQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lastFiscalQuarter");
                                
                                    public static final java.lang.String __lastFiscalQuarterOneFiscalYearAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lastFiscalQuarterOneFiscalYearAgo");
                                
                                    public static final java.lang.String __lastFiscalQuarterToPeriod =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lastFiscalQuarterToPeriod");
                                
                                    public static final java.lang.String __lastFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lastFiscalYear");
                                
                                    public static final java.lang.String __lastFiscalYearToPeriod =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lastFiscalYearToPeriod");
                                
                                    public static final java.lang.String __lastPeriod =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lastPeriod");
                                
                                    public static final java.lang.String __lastPeriodOneFiscalQuarterAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lastPeriodOneFiscalQuarterAgo");
                                
                                    public static final java.lang.String __lastPeriodOneFiscalYearAgo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lastPeriodOneFiscalYearAgo");
                                
                                    public static final java.lang.String __lastRolling18periods =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lastRolling18periods");
                                
                                    public static final java.lang.String __lastRolling6fiscalQuarters =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lastRolling6fiscalQuarters");
                                
                                    public static final java.lang.String __periodBeforeLast =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_periodBeforeLast");
                                
                                    public static final java.lang.String __sameFiscalQuarterOfLastFy =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sameFiscalQuarterOfLastFy");
                                
                                    public static final java.lang.String __sameFiscalQuarterOfLastFyToPeriod =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sameFiscalQuarterOfLastFyToPeriod");
                                
                                    public static final java.lang.String __samePeriodOfLastFiscalQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_samePeriodOfLastFiscalQuarter");
                                
                                    public static final java.lang.String __samePeriodOfLastFy =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_samePeriodOfLastFy");
                                
                                    public static final java.lang.String __secondFiscalQuarterOfLastFy =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_secondFiscalQuarterOfLastFy");
                                
                                    public static final java.lang.String __secondFiscalQuarterOfThisFy =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_secondFiscalQuarterOfThisFy");
                                
                                    public static final java.lang.String __thirdFiscalQuarterOfLastFy =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_thirdFiscalQuarterOfLastFy");
                                
                                    public static final java.lang.String __thirdFiscalQuarterOfThisFy =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_thirdFiscalQuarterOfThisFy");
                                
                                    public static final java.lang.String __thisFiscalQuarter =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_thisFiscalQuarter");
                                
                                    public static final java.lang.String __thisFiscalQuarterToPeriod =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_thisFiscalQuarterToPeriod");
                                
                                    public static final java.lang.String __thisFiscalYear =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_thisFiscalYear");
                                
                                    public static final java.lang.String __thisFiscalYearToPeriod =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_thisFiscalYearToPeriod");
                                
                                    public static final java.lang.String __thisPeriod =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_thisPeriod");
                                
                                public static final PostingPeriodDate _firstFiscalQuarterOfLastFy =
                                    new PostingPeriodDate(__firstFiscalQuarterOfLastFy,true);
                            
                                public static final PostingPeriodDate _firstFiscalQuarterOfThisFy =
                                    new PostingPeriodDate(__firstFiscalQuarterOfThisFy,true);
                            
                                public static final PostingPeriodDate _fiscalQuarterBeforeLast =
                                    new PostingPeriodDate(__fiscalQuarterBeforeLast,true);
                            
                                public static final PostingPeriodDate _fiscalYearBeforeLast =
                                    new PostingPeriodDate(__fiscalYearBeforeLast,true);
                            
                                public static final PostingPeriodDate _fourthFiscalQuarterOfLastFy =
                                    new PostingPeriodDate(__fourthFiscalQuarterOfLastFy,true);
                            
                                public static final PostingPeriodDate _fourthFiscalQuarterOfThisFy =
                                    new PostingPeriodDate(__fourthFiscalQuarterOfThisFy,true);
                            
                                public static final PostingPeriodDate _lastFiscalQuarter =
                                    new PostingPeriodDate(__lastFiscalQuarter,true);
                            
                                public static final PostingPeriodDate _lastFiscalQuarterOneFiscalYearAgo =
                                    new PostingPeriodDate(__lastFiscalQuarterOneFiscalYearAgo,true);
                            
                                public static final PostingPeriodDate _lastFiscalQuarterToPeriod =
                                    new PostingPeriodDate(__lastFiscalQuarterToPeriod,true);
                            
                                public static final PostingPeriodDate _lastFiscalYear =
                                    new PostingPeriodDate(__lastFiscalYear,true);
                            
                                public static final PostingPeriodDate _lastFiscalYearToPeriod =
                                    new PostingPeriodDate(__lastFiscalYearToPeriod,true);
                            
                                public static final PostingPeriodDate _lastPeriod =
                                    new PostingPeriodDate(__lastPeriod,true);
                            
                                public static final PostingPeriodDate _lastPeriodOneFiscalQuarterAgo =
                                    new PostingPeriodDate(__lastPeriodOneFiscalQuarterAgo,true);
                            
                                public static final PostingPeriodDate _lastPeriodOneFiscalYearAgo =
                                    new PostingPeriodDate(__lastPeriodOneFiscalYearAgo,true);
                            
                                public static final PostingPeriodDate _lastRolling18periods =
                                    new PostingPeriodDate(__lastRolling18periods,true);
                            
                                public static final PostingPeriodDate _lastRolling6fiscalQuarters =
                                    new PostingPeriodDate(__lastRolling6fiscalQuarters,true);
                            
                                public static final PostingPeriodDate _periodBeforeLast =
                                    new PostingPeriodDate(__periodBeforeLast,true);
                            
                                public static final PostingPeriodDate _sameFiscalQuarterOfLastFy =
                                    new PostingPeriodDate(__sameFiscalQuarterOfLastFy,true);
                            
                                public static final PostingPeriodDate _sameFiscalQuarterOfLastFyToPeriod =
                                    new PostingPeriodDate(__sameFiscalQuarterOfLastFyToPeriod,true);
                            
                                public static final PostingPeriodDate _samePeriodOfLastFiscalQuarter =
                                    new PostingPeriodDate(__samePeriodOfLastFiscalQuarter,true);
                            
                                public static final PostingPeriodDate _samePeriodOfLastFy =
                                    new PostingPeriodDate(__samePeriodOfLastFy,true);
                            
                                public static final PostingPeriodDate _secondFiscalQuarterOfLastFy =
                                    new PostingPeriodDate(__secondFiscalQuarterOfLastFy,true);
                            
                                public static final PostingPeriodDate _secondFiscalQuarterOfThisFy =
                                    new PostingPeriodDate(__secondFiscalQuarterOfThisFy,true);
                            
                                public static final PostingPeriodDate _thirdFiscalQuarterOfLastFy =
                                    new PostingPeriodDate(__thirdFiscalQuarterOfLastFy,true);
                            
                                public static final PostingPeriodDate _thirdFiscalQuarterOfThisFy =
                                    new PostingPeriodDate(__thirdFiscalQuarterOfThisFy,true);
                            
                                public static final PostingPeriodDate _thisFiscalQuarter =
                                    new PostingPeriodDate(__thisFiscalQuarter,true);
                            
                                public static final PostingPeriodDate _thisFiscalQuarterToPeriod =
                                    new PostingPeriodDate(__thisFiscalQuarterToPeriod,true);
                            
                                public static final PostingPeriodDate _thisFiscalYear =
                                    new PostingPeriodDate(__thisFiscalYear,true);
                            
                                public static final PostingPeriodDate _thisFiscalYearToPeriod =
                                    new PostingPeriodDate(__thisFiscalYearToPeriod,true);
                            
                                public static final PostingPeriodDate _thisPeriod =
                                    new PostingPeriodDate(__thisPeriod,true);
                            

                                public java.lang.String getValue() { return localPostingPeriodDate;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localPostingPeriodDate.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.common_2017_2.platform.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":PostingPeriodDate",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "PostingPeriodDate",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localPostingPeriodDate==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("PostingPeriodDate cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localPostingPeriodDate);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.common_2017_2.platform.webservices.netsuite.com")){
                return "ns6";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPostingPeriodDate)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static PostingPeriodDate fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    PostingPeriodDate enumeration = (PostingPeriodDate)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static PostingPeriodDate fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static PostingPeriodDate fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return PostingPeriodDate.Factory.fromString(content,namespaceUri);
                    } else {
                       return PostingPeriodDate.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static PostingPeriodDate parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            PostingPeriodDate object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"PostingPeriodDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = PostingPeriodDate.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = PostingPeriodDate.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    