
/**
 * VendorSearchBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  VendorSearchBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class VendorSearchBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRecordBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = VendorSearchBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for AccountNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAccountNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountNumberTracker = false ;

                           public boolean isAccountNumberSpecified(){
                               return localAccountNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAccountNumber(){
                               return localAccountNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccountNumber
                               */
                               public void setAccountNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAccountNumberTracker = param != null;
                                   
                                            this.localAccountNumber=param;
                                    

                               }
                            

                        /**
                        * field for Address
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddressTracker = false ;

                           public boolean isAddressSpecified(){
                               return localAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAddress(){
                               return localAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Address
                               */
                               public void setAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAddressTracker = param != null;
                                   
                                            this.localAddress=param;
                                    

                               }
                            

                        /**
                        * field for Addressee
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAddressee ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddresseeTracker = false ;

                           public boolean isAddresseeSpecified(){
                               return localAddresseeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAddressee(){
                               return localAddressee;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Addressee
                               */
                               public void setAddressee(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAddresseeTracker = param != null;
                                   
                                            this.localAddressee=param;
                                    

                               }
                            

                        /**
                        * field for AddressLabel
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAddressLabel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddressLabelTracker = false ;

                           public boolean isAddressLabelSpecified(){
                               return localAddressLabelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAddressLabel(){
                               return localAddressLabel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AddressLabel
                               */
                               public void setAddressLabel(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAddressLabelTracker = param != null;
                                   
                                            this.localAddressLabel=param;
                                    

                               }
                            

                        /**
                        * field for AddressPhone
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAddressPhone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddressPhoneTracker = false ;

                           public boolean isAddressPhoneSpecified(){
                               return localAddressPhoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAddressPhone(){
                               return localAddressPhone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AddressPhone
                               */
                               public void setAddressPhone(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAddressPhoneTracker = param != null;
                                   
                                            this.localAddressPhone=param;
                                    

                               }
                            

                        /**
                        * field for Attention
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAttention ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAttentionTracker = false ;

                           public boolean isAttentionSpecified(){
                               return localAttentionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAttention(){
                               return localAttention;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Attention
                               */
                               public void setAttention(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAttentionTracker = param != null;
                                   
                                            this.localAttention=param;
                                    

                               }
                            

                        /**
                        * field for Balance
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localBalance ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBalanceTracker = false ;

                           public boolean isBalanceSpecified(){
                               return localBalanceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getBalance(){
                               return localBalance;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Balance
                               */
                               public void setBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localBalanceTracker = param != null;
                                   
                                            this.localBalance=param;
                                    

                               }
                            

                        /**
                        * field for BillAddress
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localBillAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillAddressTracker = false ;

                           public boolean isBillAddressSpecified(){
                               return localBillAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getBillAddress(){
                               return localBillAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillAddress
                               */
                               public void setBillAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localBillAddressTracker = param != null;
                                   
                                            this.localBillAddress=param;
                                    

                               }
                            

                        /**
                        * field for Category
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCategory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCategoryTracker = false ;

                           public boolean isCategorySpecified(){
                               return localCategoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCategory(){
                               return localCategory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Category
                               */
                               public void setCategory(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCategoryTracker = param != null;
                                   
                                            this.localCategory=param;
                                    

                               }
                            

                        /**
                        * field for City
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localCity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCityTracker = false ;

                           public boolean isCitySpecified(){
                               return localCityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getCity(){
                               return localCity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param City
                               */
                               public void setCity(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCityTracker = param != null;
                                   
                                            this.localCity=param;
                                    

                               }
                            

                        /**
                        * field for Comments
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localComments ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCommentsTracker = false ;

                           public boolean isCommentsSpecified(){
                               return localCommentsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getComments(){
                               return localComments;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Comments
                               */
                               public void setComments(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCommentsTracker = param != null;
                                   
                                            this.localComments=param;
                                    

                               }
                            

                        /**
                        * field for Contact
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localContact ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContactTracker = false ;

                           public boolean isContactSpecified(){
                               return localContactTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getContact(){
                               return localContact;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Contact
                               */
                               public void setContact(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localContactTracker = param != null;
                                   
                                            this.localContact=param;
                                    

                               }
                            

                        /**
                        * field for Country
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localCountry ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCountryTracker = false ;

                           public boolean isCountrySpecified(){
                               return localCountryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getCountry(){
                               return localCountry;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Country
                               */
                               public void setCountry(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localCountryTracker = param != null;
                                   
                                            this.localCountry=param;
                                    

                               }
                            

                        /**
                        * field for County
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localCounty ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCountyTracker = false ;

                           public boolean isCountySpecified(){
                               return localCountyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getCounty(){
                               return localCounty;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param County
                               */
                               public void setCounty(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCountyTracker = param != null;
                                   
                                            this.localCounty=param;
                                    

                               }
                            

                        /**
                        * field for CreditLimit
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localCreditLimit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreditLimitTracker = false ;

                           public boolean isCreditLimitSpecified(){
                               return localCreditLimitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getCreditLimit(){
                               return localCreditLimit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreditLimit
                               */
                               public void setCreditLimit(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localCreditLimitTracker = param != null;
                                   
                                            this.localCreditLimit=param;
                                    

                               }
                            

                        /**
                        * field for Currency
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrencyTracker = false ;

                           public boolean isCurrencySpecified(){
                               return localCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCurrency(){
                               return localCurrency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Currency
                               */
                               public void setCurrency(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCurrencyTracker = param != null;
                                   
                                            this.localCurrency=param;
                                    

                               }
                            

                        /**
                        * field for CurrentExchangeRate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localCurrentExchangeRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrentExchangeRateTracker = false ;

                           public boolean isCurrentExchangeRateSpecified(){
                               return localCurrentExchangeRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getCurrentExchangeRate(){
                               return localCurrentExchangeRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CurrentExchangeRate
                               */
                               public void setCurrentExchangeRate(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localCurrentExchangeRateTracker = param != null;
                                   
                                            this.localCurrentExchangeRate=param;
                                    

                               }
                            

                        /**
                        * field for DateCreated
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localDateCreated ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDateCreatedTracker = false ;

                           public boolean isDateCreatedSpecified(){
                               return localDateCreatedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getDateCreated(){
                               return localDateCreated;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DateCreated
                               */
                               public void setDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localDateCreatedTracker = param != null;
                                   
                                            this.localDateCreated=param;
                                    

                               }
                            

                        /**
                        * field for EligibleForCommission
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localEligibleForCommission ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEligibleForCommissionTracker = false ;

                           public boolean isEligibleForCommissionSpecified(){
                               return localEligibleForCommissionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getEligibleForCommission(){
                               return localEligibleForCommission;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EligibleForCommission
                               */
                               public void setEligibleForCommission(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localEligibleForCommissionTracker = param != null;
                                   
                                            this.localEligibleForCommission=param;
                                    

                               }
                            

                        /**
                        * field for Email
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailTracker = false ;

                           public boolean isEmailSpecified(){
                               return localEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getEmail(){
                               return localEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Email
                               */
                               public void setEmail(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localEmailTracker = param != null;
                                   
                                            this.localEmail=param;
                                    

                               }
                            

                        /**
                        * field for EmailPreference
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localEmailPreference ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailPreferenceTracker = false ;

                           public boolean isEmailPreferenceSpecified(){
                               return localEmailPreferenceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getEmailPreference(){
                               return localEmailPreference;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmailPreference
                               */
                               public void setEmailPreference(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localEmailPreferenceTracker = param != null;
                                   
                                            this.localEmailPreference=param;
                                    

                               }
                            

                        /**
                        * field for EmailTransactions
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localEmailTransactions ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailTransactionsTracker = false ;

                           public boolean isEmailTransactionsSpecified(){
                               return localEmailTransactionsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getEmailTransactions(){
                               return localEmailTransactions;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmailTransactions
                               */
                               public void setEmailTransactions(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localEmailTransactionsTracker = param != null;
                                   
                                            this.localEmailTransactions=param;
                                    

                               }
                            

                        /**
                        * field for EntityId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localEntityId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityIdTracker = false ;

                           public boolean isEntityIdSpecified(){
                               return localEntityIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getEntityId(){
                               return localEntityId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EntityId
                               */
                               public void setEntityId(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localEntityIdTracker = param != null;
                                   
                                            this.localEntityId=param;
                                    

                               }
                            

                        /**
                        * field for ExpenseAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localExpenseAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExpenseAccountTracker = false ;

                           public boolean isExpenseAccountSpecified(){
                               return localExpenseAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getExpenseAccount(){
                               return localExpenseAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExpenseAccount
                               */
                               public void setExpenseAccount(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localExpenseAccountTracker = param != null;
                                   
                                            this.localExpenseAccount=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localExternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdTracker = false ;

                           public boolean isExternalIdSpecified(){
                               return localExternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localExternalIdTracker = param != null;
                                   
                                            this.localExternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalIdString
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localExternalIdString ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdStringTracker = false ;

                           public boolean isExternalIdStringSpecified(){
                               return localExternalIdStringTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getExternalIdString(){
                               return localExternalIdString;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalIdString
                               */
                               public void setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localExternalIdStringTracker = param != null;
                                   
                                            this.localExternalIdString=param;
                                    

                               }
                            

                        /**
                        * field for Fax
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localFax ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFaxTracker = false ;

                           public boolean isFaxSpecified(){
                               return localFaxTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getFax(){
                               return localFax;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Fax
                               */
                               public void setFax(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localFaxTracker = param != null;
                                   
                                            this.localFax=param;
                                    

                               }
                            

                        /**
                        * field for FaxTransactions
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localFaxTransactions ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFaxTransactionsTracker = false ;

                           public boolean isFaxTransactionsSpecified(){
                               return localFaxTransactionsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getFaxTransactions(){
                               return localFaxTransactions;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FaxTransactions
                               */
                               public void setFaxTransactions(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localFaxTransactionsTracker = param != null;
                                   
                                            this.localFaxTransactions=param;
                                    

                               }
                            

                        /**
                        * field for FirstName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localFirstName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFirstNameTracker = false ;

                           public boolean isFirstNameSpecified(){
                               return localFirstNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getFirstName(){
                               return localFirstName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FirstName
                               */
                               public void setFirstName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localFirstNameTracker = param != null;
                                   
                                            this.localFirstName=param;
                                    

                               }
                            

                        /**
                        * field for FxBalance
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localFxBalance ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFxBalanceTracker = false ;

                           public boolean isFxBalanceSpecified(){
                               return localFxBalanceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getFxBalance(){
                               return localFxBalance;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FxBalance
                               */
                               public void setFxBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localFxBalanceTracker = param != null;
                                   
                                            this.localFxBalance=param;
                                    

                               }
                            

                        /**
                        * field for FxUnbilledOrders
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localFxUnbilledOrders ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFxUnbilledOrdersTracker = false ;

                           public boolean isFxUnbilledOrdersSpecified(){
                               return localFxUnbilledOrdersTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getFxUnbilledOrders(){
                               return localFxUnbilledOrders;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FxUnbilledOrders
                               */
                               public void setFxUnbilledOrders(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localFxUnbilledOrdersTracker = param != null;
                                   
                                            this.localFxUnbilledOrders=param;
                                    

                               }
                            

                        /**
                        * field for GiveAccess
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localGiveAccess ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiveAccessTracker = false ;

                           public boolean isGiveAccessSpecified(){
                               return localGiveAccessTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getGiveAccess(){
                               return localGiveAccess;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiveAccess
                               */
                               public void setGiveAccess(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localGiveAccessTracker = param != null;
                                   
                                            this.localGiveAccess=param;
                                    

                               }
                            

                        /**
                        * field for GlobalSubscriptionStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localGlobalSubscriptionStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGlobalSubscriptionStatusTracker = false ;

                           public boolean isGlobalSubscriptionStatusSpecified(){
                               return localGlobalSubscriptionStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getGlobalSubscriptionStatus(){
                               return localGlobalSubscriptionStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GlobalSubscriptionStatus
                               */
                               public void setGlobalSubscriptionStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localGlobalSubscriptionStatusTracker = param != null;
                                   
                                            this.localGlobalSubscriptionStatus=param;
                                    

                               }
                            

                        /**
                        * field for Group
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGroupTracker = false ;

                           public boolean isGroupSpecified(){
                               return localGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getGroup(){
                               return localGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Group
                               */
                               public void setGroup(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localGroupTracker = param != null;
                                   
                                            this.localGroup=param;
                                    

                               }
                            

                        /**
                        * field for HasDuplicates
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localHasDuplicates ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHasDuplicatesTracker = false ;

                           public boolean isHasDuplicatesSpecified(){
                               return localHasDuplicatesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getHasDuplicates(){
                               return localHasDuplicates;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HasDuplicates
                               */
                               public void setHasDuplicates(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localHasDuplicatesTracker = param != null;
                                   
                                            this.localHasDuplicates=param;
                                    

                               }
                            

                        /**
                        * field for Image
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localImage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localImageTracker = false ;

                           public boolean isImageSpecified(){
                               return localImageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getImage(){
                               return localImage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Image
                               */
                               public void setImage(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localImageTracker = param != null;
                                   
                                            this.localImage=param;
                                    

                               }
                            

                        /**
                        * field for Incoterm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localIncoterm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIncotermTracker = false ;

                           public boolean isIncotermSpecified(){
                               return localIncotermTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getIncoterm(){
                               return localIncoterm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Incoterm
                               */
                               public void setIncoterm(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localIncotermTracker = param != null;
                                   
                                            this.localIncoterm=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localInternalIdTracker = param != null;
                                   
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for InternalIdNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localInternalIdNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdNumberTracker = false ;

                           public boolean isInternalIdNumberSpecified(){
                               return localInternalIdNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getInternalIdNumber(){
                               return localInternalIdNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalIdNumber
                               */
                               public void setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localInternalIdNumberTracker = param != null;
                                   
                                            this.localInternalIdNumber=param;
                                    

                               }
                            

                        /**
                        * field for Is1099Eligible
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIs1099Eligible ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIs1099EligibleTracker = false ;

                           public boolean isIs1099EligibleSpecified(){
                               return localIs1099EligibleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIs1099Eligible(){
                               return localIs1099Eligible;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Is1099Eligible
                               */
                               public void setIs1099Eligible(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIs1099EligibleTracker = param != null;
                                   
                                            this.localIs1099Eligible=param;
                                    

                               }
                            

                        /**
                        * field for IsDefaultBilling
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsDefaultBilling ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsDefaultBillingTracker = false ;

                           public boolean isIsDefaultBillingSpecified(){
                               return localIsDefaultBillingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsDefaultBilling(){
                               return localIsDefaultBilling;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsDefaultBilling
                               */
                               public void setIsDefaultBilling(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsDefaultBillingTracker = param != null;
                                   
                                            this.localIsDefaultBilling=param;
                                    

                               }
                            

                        /**
                        * field for IsDefaultShipping
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsDefaultShipping ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsDefaultShippingTracker = false ;

                           public boolean isIsDefaultShippingSpecified(){
                               return localIsDefaultShippingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsDefaultShipping(){
                               return localIsDefaultShipping;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsDefaultShipping
                               */
                               public void setIsDefaultShipping(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsDefaultShippingTracker = param != null;
                                   
                                            this.localIsDefaultShipping=param;
                                    

                               }
                            

                        /**
                        * field for IsInactive
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsInactive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsInactiveTracker = false ;

                           public boolean isIsInactiveSpecified(){
                               return localIsInactiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsInactive(){
                               return localIsInactive;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsInactive
                               */
                               public void setIsInactive(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsInactiveTracker = param != null;
                                   
                                            this.localIsInactive=param;
                                    

                               }
                            

                        /**
                        * field for IsJobResourceVend
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsJobResourceVend ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsJobResourceVendTracker = false ;

                           public boolean isIsJobResourceVendSpecified(){
                               return localIsJobResourceVendTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsJobResourceVend(){
                               return localIsJobResourceVend;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsJobResourceVend
                               */
                               public void setIsJobResourceVend(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsJobResourceVendTracker = param != null;
                                   
                                            this.localIsJobResourceVend=param;
                                    

                               }
                            

                        /**
                        * field for IsPerson
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsPerson ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsPersonTracker = false ;

                           public boolean isIsPersonSpecified(){
                               return localIsPersonTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsPerson(){
                               return localIsPerson;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsPerson
                               */
                               public void setIsPerson(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsPersonTracker = param != null;
                                   
                                            this.localIsPerson=param;
                                    

                               }
                            

                        /**
                        * field for LaborCost
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localLaborCost ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLaborCostTracker = false ;

                           public boolean isLaborCostSpecified(){
                               return localLaborCostTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getLaborCost(){
                               return localLaborCost;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LaborCost
                               */
                               public void setLaborCost(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localLaborCostTracker = param != null;
                                   
                                            this.localLaborCost=param;
                                    

                               }
                            

                        /**
                        * field for Language
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localLanguage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLanguageTracker = false ;

                           public boolean isLanguageSpecified(){
                               return localLanguageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getLanguage(){
                               return localLanguage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Language
                               */
                               public void setLanguage(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localLanguageTracker = param != null;
                                   
                                            this.localLanguage=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for LastName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localLastName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastNameTracker = false ;

                           public boolean isLastNameSpecified(){
                               return localLastNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getLastName(){
                               return localLastName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastName
                               */
                               public void setLastName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localLastNameTracker = param != null;
                                   
                                            this.localLastName=param;
                                    

                               }
                            

                        /**
                        * field for Level
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localLevel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLevelTracker = false ;

                           public boolean isLevelSpecified(){
                               return localLevelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getLevel(){
                               return localLevel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Level
                               */
                               public void setLevel(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localLevelTracker = param != null;
                                   
                                            this.localLevel=param;
                                    

                               }
                            

                        /**
                        * field for MiddleName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localMiddleName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMiddleNameTracker = false ;

                           public boolean isMiddleNameSpecified(){
                               return localMiddleNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getMiddleName(){
                               return localMiddleName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MiddleName
                               */
                               public void setMiddleName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localMiddleNameTracker = param != null;
                                   
                                            this.localMiddleName=param;
                                    

                               }
                            

                        /**
                        * field for OtherRelationships
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localOtherRelationships ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOtherRelationshipsTracker = false ;

                           public boolean isOtherRelationshipsSpecified(){
                               return localOtherRelationshipsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getOtherRelationships(){
                               return localOtherRelationships;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OtherRelationships
                               */
                               public void setOtherRelationships(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localOtherRelationshipsTracker = param != null;
                                   
                                            this.localOtherRelationships=param;
                                    

                               }
                            

                        /**
                        * field for PayablesAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localPayablesAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPayablesAccountTracker = false ;

                           public boolean isPayablesAccountSpecified(){
                               return localPayablesAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getPayablesAccount(){
                               return localPayablesAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PayablesAccount
                               */
                               public void setPayablesAccount(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localPayablesAccountTracker = param != null;
                                   
                                            this.localPayablesAccount=param;
                                    

                               }
                            

                        /**
                        * field for Pec
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localPec ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPecTracker = false ;

                           public boolean isPecSpecified(){
                               return localPecTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getPec(){
                               return localPec;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Pec
                               */
                               public void setPec(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localPecTracker = param != null;
                                   
                                            this.localPec=param;
                                    

                               }
                            

                        /**
                        * field for Permission
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localPermission ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPermissionTracker = false ;

                           public boolean isPermissionSpecified(){
                               return localPermissionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getPermission(){
                               return localPermission;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Permission
                               */
                               public void setPermission(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localPermissionTracker = param != null;
                                   
                                            this.localPermission=param;
                                    

                               }
                            

                        /**
                        * field for Phone
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localPhone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPhoneTracker = false ;

                           public boolean isPhoneSpecified(){
                               return localPhoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getPhone(){
                               return localPhone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Phone
                               */
                               public void setPhone(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localPhoneTracker = param != null;
                                   
                                            this.localPhone=param;
                                    

                               }
                            

                        /**
                        * field for PhoneticName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localPhoneticName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPhoneticNameTracker = false ;

                           public boolean isPhoneticNameSpecified(){
                               return localPhoneticNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getPhoneticName(){
                               return localPhoneticName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PhoneticName
                               */
                               public void setPhoneticName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localPhoneticNameTracker = param != null;
                                   
                                            this.localPhoneticName=param;
                                    

                               }
                            

                        /**
                        * field for PrintTransactions
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localPrintTransactions ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPrintTransactionsTracker = false ;

                           public boolean isPrintTransactionsSpecified(){
                               return localPrintTransactionsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getPrintTransactions(){
                               return localPrintTransactions;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PrintTransactions
                               */
                               public void setPrintTransactions(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localPrintTransactionsTracker = param != null;
                                   
                                            this.localPrintTransactions=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseOrderAmount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localPurchaseOrderAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseOrderAmountTracker = false ;

                           public boolean isPurchaseOrderAmountSpecified(){
                               return localPurchaseOrderAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getPurchaseOrderAmount(){
                               return localPurchaseOrderAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseOrderAmount
                               */
                               public void setPurchaseOrderAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localPurchaseOrderAmountTracker = param != null;
                                   
                                            this.localPurchaseOrderAmount=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseOrderQuantity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localPurchaseOrderQuantity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseOrderQuantityTracker = false ;

                           public boolean isPurchaseOrderQuantitySpecified(){
                               return localPurchaseOrderQuantityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getPurchaseOrderQuantity(){
                               return localPurchaseOrderQuantity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseOrderQuantity
                               */
                               public void setPurchaseOrderQuantity(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localPurchaseOrderQuantityTracker = param != null;
                                   
                                            this.localPurchaseOrderQuantity=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseOrderQuantityDiff
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localPurchaseOrderQuantityDiff ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseOrderQuantityDiffTracker = false ;

                           public boolean isPurchaseOrderQuantityDiffSpecified(){
                               return localPurchaseOrderQuantityDiffTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getPurchaseOrderQuantityDiff(){
                               return localPurchaseOrderQuantityDiff;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseOrderQuantityDiff
                               */
                               public void setPurchaseOrderQuantityDiff(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localPurchaseOrderQuantityDiffTracker = param != null;
                                   
                                            this.localPurchaseOrderQuantityDiff=param;
                                    

                               }
                            

                        /**
                        * field for ReceiptAmount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localReceiptAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReceiptAmountTracker = false ;

                           public boolean isReceiptAmountSpecified(){
                               return localReceiptAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getReceiptAmount(){
                               return localReceiptAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReceiptAmount
                               */
                               public void setReceiptAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localReceiptAmountTracker = param != null;
                                   
                                            this.localReceiptAmount=param;
                                    

                               }
                            

                        /**
                        * field for ReceiptQuantity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localReceiptQuantity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReceiptQuantityTracker = false ;

                           public boolean isReceiptQuantitySpecified(){
                               return localReceiptQuantityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getReceiptQuantity(){
                               return localReceiptQuantity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReceiptQuantity
                               */
                               public void setReceiptQuantity(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localReceiptQuantityTracker = param != null;
                                   
                                            this.localReceiptQuantity=param;
                                    

                               }
                            

                        /**
                        * field for ReceiptQuantityDiff
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localReceiptQuantityDiff ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReceiptQuantityDiffTracker = false ;

                           public boolean isReceiptQuantityDiffSpecified(){
                               return localReceiptQuantityDiffTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getReceiptQuantityDiff(){
                               return localReceiptQuantityDiff;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReceiptQuantityDiff
                               */
                               public void setReceiptQuantityDiff(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localReceiptQuantityDiffTracker = param != null;
                                   
                                            this.localReceiptQuantityDiff=param;
                                    

                               }
                            

                        /**
                        * field for Salutation
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localSalutation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalutationTracker = false ;

                           public boolean isSalutationSpecified(){
                               return localSalutationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getSalutation(){
                               return localSalutation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Salutation
                               */
                               public void setSalutation(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localSalutationTracker = param != null;
                                   
                                            this.localSalutation=param;
                                    

                               }
                            

                        /**
                        * field for ShipAddress
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localShipAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipAddressTracker = false ;

                           public boolean isShipAddressSpecified(){
                               return localShipAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getShipAddress(){
                               return localShipAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipAddress
                               */
                               public void setShipAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localShipAddressTracker = param != null;
                                   
                                            this.localShipAddress=param;
                                    

                               }
                            

                        /**
                        * field for State
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localState ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStateTracker = false ;

                           public boolean isStateSpecified(){
                               return localStateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getState(){
                               return localState;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param State
                               */
                               public void setState(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localStateTracker = param != null;
                                   
                                            this.localState=param;
                                    

                               }
                            

                        /**
                        * field for Subsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Subsidiary
                               */
                               public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSubsidiaryTracker = param != null;
                                   
                                            this.localSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for TaxIdNum
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localTaxIdNum ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxIdNumTracker = false ;

                           public boolean isTaxIdNumSpecified(){
                               return localTaxIdNumTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getTaxIdNum(){
                               return localTaxIdNum;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxIdNum
                               */
                               public void setTaxIdNum(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localTaxIdNumTracker = param != null;
                                   
                                            this.localTaxIdNum=param;
                                    

                               }
                            

                        /**
                        * field for TimeApprover
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localTimeApprover ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeApproverTracker = false ;

                           public boolean isTimeApproverSpecified(){
                               return localTimeApproverTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getTimeApprover(){
                               return localTimeApprover;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimeApprover
                               */
                               public void setTimeApprover(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localTimeApproverTracker = param != null;
                                   
                                            this.localTimeApprover=param;
                                    

                               }
                            

                        /**
                        * field for Title
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTitleTracker = false ;

                           public boolean isTitleSpecified(){
                               return localTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getTitle(){
                               return localTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Title
                               */
                               public void setTitle(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localTitleTracker = param != null;
                                   
                                            this.localTitle=param;
                                    

                               }
                            

                        /**
                        * field for UnbilledOrders
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localUnbilledOrders ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUnbilledOrdersTracker = false ;

                           public boolean isUnbilledOrdersSpecified(){
                               return localUnbilledOrdersTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getUnbilledOrders(){
                               return localUnbilledOrders;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UnbilledOrders
                               */
                               public void setUnbilledOrders(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localUnbilledOrdersTracker = param != null;
                                   
                                            this.localUnbilledOrders=param;
                                    

                               }
                            

                        /**
                        * field for Url
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localUrl ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUrlTracker = false ;

                           public boolean isUrlSpecified(){
                               return localUrlTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getUrl(){
                               return localUrl;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Url
                               */
                               public void setUrl(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localUrlTracker = param != null;
                                   
                                            this.localUrl=param;
                                    

                               }
                            

                        /**
                        * field for VatRegNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localVatRegNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVatRegNumberTracker = false ;

                           public boolean isVatRegNumberSpecified(){
                               return localVatRegNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getVatRegNumber(){
                               return localVatRegNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VatRegNumber
                               */
                               public void setVatRegNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localVatRegNumberTracker = param != null;
                                   
                                            this.localVatRegNumber=param;
                                    

                               }
                            

                        /**
                        * field for WorkCalendar
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localWorkCalendar ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWorkCalendarTracker = false ;

                           public boolean isWorkCalendarSpecified(){
                               return localWorkCalendarTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getWorkCalendar(){
                               return localWorkCalendar;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param WorkCalendar
                               */
                               public void setWorkCalendar(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localWorkCalendarTracker = param != null;
                                   
                                            this.localWorkCalendar=param;
                                    

                               }
                            

                        /**
                        * field for ZipCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localZipCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localZipCodeTracker = false ;

                           public boolean isZipCodeSpecified(){
                               return localZipCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getZipCode(){
                               return localZipCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ZipCode
                               */
                               public void setZipCode(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localZipCodeTracker = param != null;
                                   
                                            this.localZipCode=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":VendorSearchBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "VendorSearchBasic",
                           xmlWriter);
                   }

                if (localAccountNumberTracker){
                                            if (localAccountNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accountNumber cannot be null!!");
                                            }
                                           localAccountNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","accountNumber"),
                                               xmlWriter);
                                        } if (localAddressTracker){
                                            if (localAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("address cannot be null!!");
                                            }
                                           localAddress.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address"),
                                               xmlWriter);
                                        } if (localAddresseeTracker){
                                            if (localAddressee==null){
                                                 throw new org.apache.axis2.databinding.ADBException("addressee cannot be null!!");
                                            }
                                           localAddressee.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressee"),
                                               xmlWriter);
                                        } if (localAddressLabelTracker){
                                            if (localAddressLabel==null){
                                                 throw new org.apache.axis2.databinding.ADBException("addressLabel cannot be null!!");
                                            }
                                           localAddressLabel.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressLabel"),
                                               xmlWriter);
                                        } if (localAddressPhoneTracker){
                                            if (localAddressPhone==null){
                                                 throw new org.apache.axis2.databinding.ADBException("addressPhone cannot be null!!");
                                            }
                                           localAddressPhone.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressPhone"),
                                               xmlWriter);
                                        } if (localAttentionTracker){
                                            if (localAttention==null){
                                                 throw new org.apache.axis2.databinding.ADBException("attention cannot be null!!");
                                            }
                                           localAttention.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","attention"),
                                               xmlWriter);
                                        } if (localBalanceTracker){
                                            if (localBalance==null){
                                                 throw new org.apache.axis2.databinding.ADBException("balance cannot be null!!");
                                            }
                                           localBalance.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","balance"),
                                               xmlWriter);
                                        } if (localBillAddressTracker){
                                            if (localBillAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billAddress cannot be null!!");
                                            }
                                           localBillAddress.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billAddress"),
                                               xmlWriter);
                                        } if (localCategoryTracker){
                                            if (localCategory==null){
                                                 throw new org.apache.axis2.databinding.ADBException("category cannot be null!!");
                                            }
                                           localCategory.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","category"),
                                               xmlWriter);
                                        } if (localCityTracker){
                                            if (localCity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("city cannot be null!!");
                                            }
                                           localCity.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","city"),
                                               xmlWriter);
                                        } if (localCommentsTracker){
                                            if (localComments==null){
                                                 throw new org.apache.axis2.databinding.ADBException("comments cannot be null!!");
                                            }
                                           localComments.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","comments"),
                                               xmlWriter);
                                        } if (localContactTracker){
                                            if (localContact==null){
                                                 throw new org.apache.axis2.databinding.ADBException("contact cannot be null!!");
                                            }
                                           localContact.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","contact"),
                                               xmlWriter);
                                        } if (localCountryTracker){
                                            if (localCountry==null){
                                                 throw new org.apache.axis2.databinding.ADBException("country cannot be null!!");
                                            }
                                           localCountry.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","country"),
                                               xmlWriter);
                                        } if (localCountyTracker){
                                            if (localCounty==null){
                                                 throw new org.apache.axis2.databinding.ADBException("county cannot be null!!");
                                            }
                                           localCounty.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","county"),
                                               xmlWriter);
                                        } if (localCreditLimitTracker){
                                            if (localCreditLimit==null){
                                                 throw new org.apache.axis2.databinding.ADBException("creditLimit cannot be null!!");
                                            }
                                           localCreditLimit.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","creditLimit"),
                                               xmlWriter);
                                        } if (localCurrencyTracker){
                                            if (localCurrency==null){
                                                 throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                            }
                                           localCurrency.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency"),
                                               xmlWriter);
                                        } if (localCurrentExchangeRateTracker){
                                            if (localCurrentExchangeRate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("currentExchangeRate cannot be null!!");
                                            }
                                           localCurrentExchangeRate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currentExchangeRate"),
                                               xmlWriter);
                                        } if (localDateCreatedTracker){
                                            if (localDateCreated==null){
                                                 throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                            }
                                           localDateCreated.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated"),
                                               xmlWriter);
                                        } if (localEligibleForCommissionTracker){
                                            if (localEligibleForCommission==null){
                                                 throw new org.apache.axis2.databinding.ADBException("eligibleForCommission cannot be null!!");
                                            }
                                           localEligibleForCommission.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","eligibleForCommission"),
                                               xmlWriter);
                                        } if (localEmailTracker){
                                            if (localEmail==null){
                                                 throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                            }
                                           localEmail.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","email"),
                                               xmlWriter);
                                        } if (localEmailPreferenceTracker){
                                            if (localEmailPreference==null){
                                                 throw new org.apache.axis2.databinding.ADBException("emailPreference cannot be null!!");
                                            }
                                           localEmailPreference.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","emailPreference"),
                                               xmlWriter);
                                        } if (localEmailTransactionsTracker){
                                            if (localEmailTransactions==null){
                                                 throw new org.apache.axis2.databinding.ADBException("emailTransactions cannot be null!!");
                                            }
                                           localEmailTransactions.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","emailTransactions"),
                                               xmlWriter);
                                        } if (localEntityIdTracker){
                                            if (localEntityId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("entityId cannot be null!!");
                                            }
                                           localEntityId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entityId"),
                                               xmlWriter);
                                        } if (localExpenseAccountTracker){
                                            if (localExpenseAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("expenseAccount cannot be null!!");
                                            }
                                           localExpenseAccount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","expenseAccount"),
                                               xmlWriter);
                                        } if (localExternalIdTracker){
                                            if (localExternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                            }
                                           localExternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId"),
                                               xmlWriter);
                                        } if (localExternalIdStringTracker){
                                            if (localExternalIdString==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                            }
                                           localExternalIdString.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString"),
                                               xmlWriter);
                                        } if (localFaxTracker){
                                            if (localFax==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fax cannot be null!!");
                                            }
                                           localFax.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fax"),
                                               xmlWriter);
                                        } if (localFaxTransactionsTracker){
                                            if (localFaxTransactions==null){
                                                 throw new org.apache.axis2.databinding.ADBException("faxTransactions cannot be null!!");
                                            }
                                           localFaxTransactions.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","faxTransactions"),
                                               xmlWriter);
                                        } if (localFirstNameTracker){
                                            if (localFirstName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("firstName cannot be null!!");
                                            }
                                           localFirstName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","firstName"),
                                               xmlWriter);
                                        } if (localFxBalanceTracker){
                                            if (localFxBalance==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fxBalance cannot be null!!");
                                            }
                                           localFxBalance.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxBalance"),
                                               xmlWriter);
                                        } if (localFxUnbilledOrdersTracker){
                                            if (localFxUnbilledOrders==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fxUnbilledOrders cannot be null!!");
                                            }
                                           localFxUnbilledOrders.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxUnbilledOrders"),
                                               xmlWriter);
                                        } if (localGiveAccessTracker){
                                            if (localGiveAccess==null){
                                                 throw new org.apache.axis2.databinding.ADBException("giveAccess cannot be null!!");
                                            }
                                           localGiveAccess.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","giveAccess"),
                                               xmlWriter);
                                        } if (localGlobalSubscriptionStatusTracker){
                                            if (localGlobalSubscriptionStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("globalSubscriptionStatus cannot be null!!");
                                            }
                                           localGlobalSubscriptionStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","globalSubscriptionStatus"),
                                               xmlWriter);
                                        } if (localGroupTracker){
                                            if (localGroup==null){
                                                 throw new org.apache.axis2.databinding.ADBException("group cannot be null!!");
                                            }
                                           localGroup.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","group"),
                                               xmlWriter);
                                        } if (localHasDuplicatesTracker){
                                            if (localHasDuplicates==null){
                                                 throw new org.apache.axis2.databinding.ADBException("hasDuplicates cannot be null!!");
                                            }
                                           localHasDuplicates.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","hasDuplicates"),
                                               xmlWriter);
                                        } if (localImageTracker){
                                            if (localImage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("image cannot be null!!");
                                            }
                                           localImage.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","image"),
                                               xmlWriter);
                                        } if (localIncotermTracker){
                                            if (localIncoterm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("incoterm cannot be null!!");
                                            }
                                           localIncoterm.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","incoterm"),
                                               xmlWriter);
                                        } if (localInternalIdTracker){
                                            if (localInternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                            }
                                           localInternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                               xmlWriter);
                                        } if (localInternalIdNumberTracker){
                                            if (localInternalIdNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                            }
                                           localInternalIdNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber"),
                                               xmlWriter);
                                        } if (localIs1099EligibleTracker){
                                            if (localIs1099Eligible==null){
                                                 throw new org.apache.axis2.databinding.ADBException("is1099Eligible cannot be null!!");
                                            }
                                           localIs1099Eligible.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","is1099Eligible"),
                                               xmlWriter);
                                        } if (localIsDefaultBillingTracker){
                                            if (localIsDefaultBilling==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isDefaultBilling cannot be null!!");
                                            }
                                           localIsDefaultBilling.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isDefaultBilling"),
                                               xmlWriter);
                                        } if (localIsDefaultShippingTracker){
                                            if (localIsDefaultShipping==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isDefaultShipping cannot be null!!");
                                            }
                                           localIsDefaultShipping.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isDefaultShipping"),
                                               xmlWriter);
                                        } if (localIsInactiveTracker){
                                            if (localIsInactive==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                            }
                                           localIsInactive.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isInactive"),
                                               xmlWriter);
                                        } if (localIsJobResourceVendTracker){
                                            if (localIsJobResourceVend==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isJobResourceVend cannot be null!!");
                                            }
                                           localIsJobResourceVend.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isJobResourceVend"),
                                               xmlWriter);
                                        } if (localIsPersonTracker){
                                            if (localIsPerson==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isPerson cannot be null!!");
                                            }
                                           localIsPerson.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isPerson"),
                                               xmlWriter);
                                        } if (localLaborCostTracker){
                                            if (localLaborCost==null){
                                                 throw new org.apache.axis2.databinding.ADBException("laborCost cannot be null!!");
                                            }
                                           localLaborCost.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","laborCost"),
                                               xmlWriter);
                                        } if (localLanguageTracker){
                                            if (localLanguage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("language cannot be null!!");
                                            }
                                           localLanguage.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","language"),
                                               xmlWriter);
                                        } if (localLastModifiedDateTracker){
                                            if (localLastModifiedDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                            }
                                           localLastModifiedDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate"),
                                               xmlWriter);
                                        } if (localLastNameTracker){
                                            if (localLastName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lastName cannot be null!!");
                                            }
                                           localLastName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastName"),
                                               xmlWriter);
                                        } if (localLevelTracker){
                                            if (localLevel==null){
                                                 throw new org.apache.axis2.databinding.ADBException("level cannot be null!!");
                                            }
                                           localLevel.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","level"),
                                               xmlWriter);
                                        } if (localMiddleNameTracker){
                                            if (localMiddleName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("middleName cannot be null!!");
                                            }
                                           localMiddleName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","middleName"),
                                               xmlWriter);
                                        } if (localOtherRelationshipsTracker){
                                            if (localOtherRelationships==null){
                                                 throw new org.apache.axis2.databinding.ADBException("otherRelationships cannot be null!!");
                                            }
                                           localOtherRelationships.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","otherRelationships"),
                                               xmlWriter);
                                        } if (localPayablesAccountTracker){
                                            if (localPayablesAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("payablesAccount cannot be null!!");
                                            }
                                           localPayablesAccount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","payablesAccount"),
                                               xmlWriter);
                                        } if (localPecTracker){
                                            if (localPec==null){
                                                 throw new org.apache.axis2.databinding.ADBException("pec cannot be null!!");
                                            }
                                           localPec.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","pec"),
                                               xmlWriter);
                                        } if (localPermissionTracker){
                                            if (localPermission==null){
                                                 throw new org.apache.axis2.databinding.ADBException("permission cannot be null!!");
                                            }
                                           localPermission.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","permission"),
                                               xmlWriter);
                                        } if (localPhoneTracker){
                                            if (localPhone==null){
                                                 throw new org.apache.axis2.databinding.ADBException("phone cannot be null!!");
                                            }
                                           localPhone.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phone"),
                                               xmlWriter);
                                        } if (localPhoneticNameTracker){
                                            if (localPhoneticName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("phoneticName cannot be null!!");
                                            }
                                           localPhoneticName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phoneticName"),
                                               xmlWriter);
                                        } if (localPrintTransactionsTracker){
                                            if (localPrintTransactions==null){
                                                 throw new org.apache.axis2.databinding.ADBException("printTransactions cannot be null!!");
                                            }
                                           localPrintTransactions.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","printTransactions"),
                                               xmlWriter);
                                        } if (localPurchaseOrderAmountTracker){
                                            if (localPurchaseOrderAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("purchaseOrderAmount cannot be null!!");
                                            }
                                           localPurchaseOrderAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","purchaseOrderAmount"),
                                               xmlWriter);
                                        } if (localPurchaseOrderQuantityTracker){
                                            if (localPurchaseOrderQuantity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("purchaseOrderQuantity cannot be null!!");
                                            }
                                           localPurchaseOrderQuantity.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","purchaseOrderQuantity"),
                                               xmlWriter);
                                        } if (localPurchaseOrderQuantityDiffTracker){
                                            if (localPurchaseOrderQuantityDiff==null){
                                                 throw new org.apache.axis2.databinding.ADBException("purchaseOrderQuantityDiff cannot be null!!");
                                            }
                                           localPurchaseOrderQuantityDiff.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","purchaseOrderQuantityDiff"),
                                               xmlWriter);
                                        } if (localReceiptAmountTracker){
                                            if (localReceiptAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("receiptAmount cannot be null!!");
                                            }
                                           localReceiptAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","receiptAmount"),
                                               xmlWriter);
                                        } if (localReceiptQuantityTracker){
                                            if (localReceiptQuantity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("receiptQuantity cannot be null!!");
                                            }
                                           localReceiptQuantity.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","receiptQuantity"),
                                               xmlWriter);
                                        } if (localReceiptQuantityDiffTracker){
                                            if (localReceiptQuantityDiff==null){
                                                 throw new org.apache.axis2.databinding.ADBException("receiptQuantityDiff cannot be null!!");
                                            }
                                           localReceiptQuantityDiff.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","receiptQuantityDiff"),
                                               xmlWriter);
                                        } if (localSalutationTracker){
                                            if (localSalutation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salutation cannot be null!!");
                                            }
                                           localSalutation.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salutation"),
                                               xmlWriter);
                                        } if (localShipAddressTracker){
                                            if (localShipAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shipAddress cannot be null!!");
                                            }
                                           localShipAddress.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","shipAddress"),
                                               xmlWriter);
                                        } if (localStateTracker){
                                            if (localState==null){
                                                 throw new org.apache.axis2.databinding.ADBException("state cannot be null!!");
                                            }
                                           localState.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","state"),
                                               xmlWriter);
                                        } if (localSubsidiaryTracker){
                                            if (localSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                            }
                                           localSubsidiary.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary"),
                                               xmlWriter);
                                        } if (localTaxIdNumTracker){
                                            if (localTaxIdNum==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxIdNum cannot be null!!");
                                            }
                                           localTaxIdNum.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","taxIdNum"),
                                               xmlWriter);
                                        } if (localTimeApproverTracker){
                                            if (localTimeApprover==null){
                                                 throw new org.apache.axis2.databinding.ADBException("timeApprover cannot be null!!");
                                            }
                                           localTimeApprover.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","timeApprover"),
                                               xmlWriter);
                                        } if (localTitleTracker){
                                            if (localTitle==null){
                                                 throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                            }
                                           localTitle.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","title"),
                                               xmlWriter);
                                        } if (localUnbilledOrdersTracker){
                                            if (localUnbilledOrders==null){
                                                 throw new org.apache.axis2.databinding.ADBException("unbilledOrders cannot be null!!");
                                            }
                                           localUnbilledOrders.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","unbilledOrders"),
                                               xmlWriter);
                                        } if (localUrlTracker){
                                            if (localUrl==null){
                                                 throw new org.apache.axis2.databinding.ADBException("url cannot be null!!");
                                            }
                                           localUrl.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","url"),
                                               xmlWriter);
                                        } if (localVatRegNumberTracker){
                                            if (localVatRegNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vatRegNumber cannot be null!!");
                                            }
                                           localVatRegNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","vatRegNumber"),
                                               xmlWriter);
                                        } if (localWorkCalendarTracker){
                                            if (localWorkCalendar==null){
                                                 throw new org.apache.axis2.databinding.ADBException("workCalendar cannot be null!!");
                                            }
                                           localWorkCalendar.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","workCalendar"),
                                               xmlWriter);
                                        } if (localZipCodeTracker){
                                            if (localZipCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("zipCode cannot be null!!");
                                            }
                                           localZipCode.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","zipCode"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","VendorSearchBasic"));
                 if (localAccountNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "accountNumber"));
                            
                            
                                    if (localAccountNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("accountNumber cannot be null!!");
                                    }
                                    elementList.add(localAccountNumber);
                                } if (localAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "address"));
                            
                            
                                    if (localAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("address cannot be null!!");
                                    }
                                    elementList.add(localAddress);
                                } if (localAddresseeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "addressee"));
                            
                            
                                    if (localAddressee==null){
                                         throw new org.apache.axis2.databinding.ADBException("addressee cannot be null!!");
                                    }
                                    elementList.add(localAddressee);
                                } if (localAddressLabelTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "addressLabel"));
                            
                            
                                    if (localAddressLabel==null){
                                         throw new org.apache.axis2.databinding.ADBException("addressLabel cannot be null!!");
                                    }
                                    elementList.add(localAddressLabel);
                                } if (localAddressPhoneTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "addressPhone"));
                            
                            
                                    if (localAddressPhone==null){
                                         throw new org.apache.axis2.databinding.ADBException("addressPhone cannot be null!!");
                                    }
                                    elementList.add(localAddressPhone);
                                } if (localAttentionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "attention"));
                            
                            
                                    if (localAttention==null){
                                         throw new org.apache.axis2.databinding.ADBException("attention cannot be null!!");
                                    }
                                    elementList.add(localAttention);
                                } if (localBalanceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "balance"));
                            
                            
                                    if (localBalance==null){
                                         throw new org.apache.axis2.databinding.ADBException("balance cannot be null!!");
                                    }
                                    elementList.add(localBalance);
                                } if (localBillAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "billAddress"));
                            
                            
                                    if (localBillAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("billAddress cannot be null!!");
                                    }
                                    elementList.add(localBillAddress);
                                } if (localCategoryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "category"));
                            
                            
                                    if (localCategory==null){
                                         throw new org.apache.axis2.databinding.ADBException("category cannot be null!!");
                                    }
                                    elementList.add(localCategory);
                                } if (localCityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "city"));
                            
                            
                                    if (localCity==null){
                                         throw new org.apache.axis2.databinding.ADBException("city cannot be null!!");
                                    }
                                    elementList.add(localCity);
                                } if (localCommentsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "comments"));
                            
                            
                                    if (localComments==null){
                                         throw new org.apache.axis2.databinding.ADBException("comments cannot be null!!");
                                    }
                                    elementList.add(localComments);
                                } if (localContactTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "contact"));
                            
                            
                                    if (localContact==null){
                                         throw new org.apache.axis2.databinding.ADBException("contact cannot be null!!");
                                    }
                                    elementList.add(localContact);
                                } if (localCountryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "country"));
                            
                            
                                    if (localCountry==null){
                                         throw new org.apache.axis2.databinding.ADBException("country cannot be null!!");
                                    }
                                    elementList.add(localCountry);
                                } if (localCountyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "county"));
                            
                            
                                    if (localCounty==null){
                                         throw new org.apache.axis2.databinding.ADBException("county cannot be null!!");
                                    }
                                    elementList.add(localCounty);
                                } if (localCreditLimitTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "creditLimit"));
                            
                            
                                    if (localCreditLimit==null){
                                         throw new org.apache.axis2.databinding.ADBException("creditLimit cannot be null!!");
                                    }
                                    elementList.add(localCreditLimit);
                                } if (localCurrencyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "currency"));
                            
                            
                                    if (localCurrency==null){
                                         throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                    }
                                    elementList.add(localCurrency);
                                } if (localCurrentExchangeRateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "currentExchangeRate"));
                            
                            
                                    if (localCurrentExchangeRate==null){
                                         throw new org.apache.axis2.databinding.ADBException("currentExchangeRate cannot be null!!");
                                    }
                                    elementList.add(localCurrentExchangeRate);
                                } if (localDateCreatedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "dateCreated"));
                            
                            
                                    if (localDateCreated==null){
                                         throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                    }
                                    elementList.add(localDateCreated);
                                } if (localEligibleForCommissionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "eligibleForCommission"));
                            
                            
                                    if (localEligibleForCommission==null){
                                         throw new org.apache.axis2.databinding.ADBException("eligibleForCommission cannot be null!!");
                                    }
                                    elementList.add(localEligibleForCommission);
                                } if (localEmailTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "email"));
                            
                            
                                    if (localEmail==null){
                                         throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                    }
                                    elementList.add(localEmail);
                                } if (localEmailPreferenceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "emailPreference"));
                            
                            
                                    if (localEmailPreference==null){
                                         throw new org.apache.axis2.databinding.ADBException("emailPreference cannot be null!!");
                                    }
                                    elementList.add(localEmailPreference);
                                } if (localEmailTransactionsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "emailTransactions"));
                            
                            
                                    if (localEmailTransactions==null){
                                         throw new org.apache.axis2.databinding.ADBException("emailTransactions cannot be null!!");
                                    }
                                    elementList.add(localEmailTransactions);
                                } if (localEntityIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "entityId"));
                            
                            
                                    if (localEntityId==null){
                                         throw new org.apache.axis2.databinding.ADBException("entityId cannot be null!!");
                                    }
                                    elementList.add(localEntityId);
                                } if (localExpenseAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "expenseAccount"));
                            
                            
                                    if (localExpenseAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("expenseAccount cannot be null!!");
                                    }
                                    elementList.add(localExpenseAccount);
                                } if (localExternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalId"));
                            
                            
                                    if (localExternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                    }
                                    elementList.add(localExternalId);
                                } if (localExternalIdStringTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalIdString"));
                            
                            
                                    if (localExternalIdString==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                    }
                                    elementList.add(localExternalIdString);
                                } if (localFaxTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "fax"));
                            
                            
                                    if (localFax==null){
                                         throw new org.apache.axis2.databinding.ADBException("fax cannot be null!!");
                                    }
                                    elementList.add(localFax);
                                } if (localFaxTransactionsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "faxTransactions"));
                            
                            
                                    if (localFaxTransactions==null){
                                         throw new org.apache.axis2.databinding.ADBException("faxTransactions cannot be null!!");
                                    }
                                    elementList.add(localFaxTransactions);
                                } if (localFirstNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "firstName"));
                            
                            
                                    if (localFirstName==null){
                                         throw new org.apache.axis2.databinding.ADBException("firstName cannot be null!!");
                                    }
                                    elementList.add(localFirstName);
                                } if (localFxBalanceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "fxBalance"));
                            
                            
                                    if (localFxBalance==null){
                                         throw new org.apache.axis2.databinding.ADBException("fxBalance cannot be null!!");
                                    }
                                    elementList.add(localFxBalance);
                                } if (localFxUnbilledOrdersTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "fxUnbilledOrders"));
                            
                            
                                    if (localFxUnbilledOrders==null){
                                         throw new org.apache.axis2.databinding.ADBException("fxUnbilledOrders cannot be null!!");
                                    }
                                    elementList.add(localFxUnbilledOrders);
                                } if (localGiveAccessTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "giveAccess"));
                            
                            
                                    if (localGiveAccess==null){
                                         throw new org.apache.axis2.databinding.ADBException("giveAccess cannot be null!!");
                                    }
                                    elementList.add(localGiveAccess);
                                } if (localGlobalSubscriptionStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "globalSubscriptionStatus"));
                            
                            
                                    if (localGlobalSubscriptionStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("globalSubscriptionStatus cannot be null!!");
                                    }
                                    elementList.add(localGlobalSubscriptionStatus);
                                } if (localGroupTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "group"));
                            
                            
                                    if (localGroup==null){
                                         throw new org.apache.axis2.databinding.ADBException("group cannot be null!!");
                                    }
                                    elementList.add(localGroup);
                                } if (localHasDuplicatesTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "hasDuplicates"));
                            
                            
                                    if (localHasDuplicates==null){
                                         throw new org.apache.axis2.databinding.ADBException("hasDuplicates cannot be null!!");
                                    }
                                    elementList.add(localHasDuplicates);
                                } if (localImageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "image"));
                            
                            
                                    if (localImage==null){
                                         throw new org.apache.axis2.databinding.ADBException("image cannot be null!!");
                                    }
                                    elementList.add(localImage);
                                } if (localIncotermTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "incoterm"));
                            
                            
                                    if (localIncoterm==null){
                                         throw new org.apache.axis2.databinding.ADBException("incoterm cannot be null!!");
                                    }
                                    elementList.add(localIncoterm);
                                } if (localInternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalId"));
                            
                            
                                    if (localInternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    }
                                    elementList.add(localInternalId);
                                } if (localInternalIdNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalIdNumber"));
                            
                            
                                    if (localInternalIdNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                    }
                                    elementList.add(localInternalIdNumber);
                                } if (localIs1099EligibleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "is1099Eligible"));
                            
                            
                                    if (localIs1099Eligible==null){
                                         throw new org.apache.axis2.databinding.ADBException("is1099Eligible cannot be null!!");
                                    }
                                    elementList.add(localIs1099Eligible);
                                } if (localIsDefaultBillingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isDefaultBilling"));
                            
                            
                                    if (localIsDefaultBilling==null){
                                         throw new org.apache.axis2.databinding.ADBException("isDefaultBilling cannot be null!!");
                                    }
                                    elementList.add(localIsDefaultBilling);
                                } if (localIsDefaultShippingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isDefaultShipping"));
                            
                            
                                    if (localIsDefaultShipping==null){
                                         throw new org.apache.axis2.databinding.ADBException("isDefaultShipping cannot be null!!");
                                    }
                                    elementList.add(localIsDefaultShipping);
                                } if (localIsInactiveTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isInactive"));
                            
                            
                                    if (localIsInactive==null){
                                         throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                    }
                                    elementList.add(localIsInactive);
                                } if (localIsJobResourceVendTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isJobResourceVend"));
                            
                            
                                    if (localIsJobResourceVend==null){
                                         throw new org.apache.axis2.databinding.ADBException("isJobResourceVend cannot be null!!");
                                    }
                                    elementList.add(localIsJobResourceVend);
                                } if (localIsPersonTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isPerson"));
                            
                            
                                    if (localIsPerson==null){
                                         throw new org.apache.axis2.databinding.ADBException("isPerson cannot be null!!");
                                    }
                                    elementList.add(localIsPerson);
                                } if (localLaborCostTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "laborCost"));
                            
                            
                                    if (localLaborCost==null){
                                         throw new org.apache.axis2.databinding.ADBException("laborCost cannot be null!!");
                                    }
                                    elementList.add(localLaborCost);
                                } if (localLanguageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "language"));
                            
                            
                                    if (localLanguage==null){
                                         throw new org.apache.axis2.databinding.ADBException("language cannot be null!!");
                                    }
                                    elementList.add(localLanguage);
                                } if (localLastModifiedDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                            
                            
                                    if (localLastModifiedDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                    }
                                    elementList.add(localLastModifiedDate);
                                } if (localLastNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lastName"));
                            
                            
                                    if (localLastName==null){
                                         throw new org.apache.axis2.databinding.ADBException("lastName cannot be null!!");
                                    }
                                    elementList.add(localLastName);
                                } if (localLevelTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "level"));
                            
                            
                                    if (localLevel==null){
                                         throw new org.apache.axis2.databinding.ADBException("level cannot be null!!");
                                    }
                                    elementList.add(localLevel);
                                } if (localMiddleNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "middleName"));
                            
                            
                                    if (localMiddleName==null){
                                         throw new org.apache.axis2.databinding.ADBException("middleName cannot be null!!");
                                    }
                                    elementList.add(localMiddleName);
                                } if (localOtherRelationshipsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "otherRelationships"));
                            
                            
                                    if (localOtherRelationships==null){
                                         throw new org.apache.axis2.databinding.ADBException("otherRelationships cannot be null!!");
                                    }
                                    elementList.add(localOtherRelationships);
                                } if (localPayablesAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "payablesAccount"));
                            
                            
                                    if (localPayablesAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("payablesAccount cannot be null!!");
                                    }
                                    elementList.add(localPayablesAccount);
                                } if (localPecTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "pec"));
                            
                            
                                    if (localPec==null){
                                         throw new org.apache.axis2.databinding.ADBException("pec cannot be null!!");
                                    }
                                    elementList.add(localPec);
                                } if (localPermissionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "permission"));
                            
                            
                                    if (localPermission==null){
                                         throw new org.apache.axis2.databinding.ADBException("permission cannot be null!!");
                                    }
                                    elementList.add(localPermission);
                                } if (localPhoneTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "phone"));
                            
                            
                                    if (localPhone==null){
                                         throw new org.apache.axis2.databinding.ADBException("phone cannot be null!!");
                                    }
                                    elementList.add(localPhone);
                                } if (localPhoneticNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "phoneticName"));
                            
                            
                                    if (localPhoneticName==null){
                                         throw new org.apache.axis2.databinding.ADBException("phoneticName cannot be null!!");
                                    }
                                    elementList.add(localPhoneticName);
                                } if (localPrintTransactionsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "printTransactions"));
                            
                            
                                    if (localPrintTransactions==null){
                                         throw new org.apache.axis2.databinding.ADBException("printTransactions cannot be null!!");
                                    }
                                    elementList.add(localPrintTransactions);
                                } if (localPurchaseOrderAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "purchaseOrderAmount"));
                            
                            
                                    if (localPurchaseOrderAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("purchaseOrderAmount cannot be null!!");
                                    }
                                    elementList.add(localPurchaseOrderAmount);
                                } if (localPurchaseOrderQuantityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "purchaseOrderQuantity"));
                            
                            
                                    if (localPurchaseOrderQuantity==null){
                                         throw new org.apache.axis2.databinding.ADBException("purchaseOrderQuantity cannot be null!!");
                                    }
                                    elementList.add(localPurchaseOrderQuantity);
                                } if (localPurchaseOrderQuantityDiffTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "purchaseOrderQuantityDiff"));
                            
                            
                                    if (localPurchaseOrderQuantityDiff==null){
                                         throw new org.apache.axis2.databinding.ADBException("purchaseOrderQuantityDiff cannot be null!!");
                                    }
                                    elementList.add(localPurchaseOrderQuantityDiff);
                                } if (localReceiptAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "receiptAmount"));
                            
                            
                                    if (localReceiptAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("receiptAmount cannot be null!!");
                                    }
                                    elementList.add(localReceiptAmount);
                                } if (localReceiptQuantityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "receiptQuantity"));
                            
                            
                                    if (localReceiptQuantity==null){
                                         throw new org.apache.axis2.databinding.ADBException("receiptQuantity cannot be null!!");
                                    }
                                    elementList.add(localReceiptQuantity);
                                } if (localReceiptQuantityDiffTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "receiptQuantityDiff"));
                            
                            
                                    if (localReceiptQuantityDiff==null){
                                         throw new org.apache.axis2.databinding.ADBException("receiptQuantityDiff cannot be null!!");
                                    }
                                    elementList.add(localReceiptQuantityDiff);
                                } if (localSalutationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "salutation"));
                            
                            
                                    if (localSalutation==null){
                                         throw new org.apache.axis2.databinding.ADBException("salutation cannot be null!!");
                                    }
                                    elementList.add(localSalutation);
                                } if (localShipAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "shipAddress"));
                            
                            
                                    if (localShipAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("shipAddress cannot be null!!");
                                    }
                                    elementList.add(localShipAddress);
                                } if (localStateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "state"));
                            
                            
                                    if (localState==null){
                                         throw new org.apache.axis2.databinding.ADBException("state cannot be null!!");
                                    }
                                    elementList.add(localState);
                                } if (localSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "subsidiary"));
                            
                            
                                    if (localSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    }
                                    elementList.add(localSubsidiary);
                                } if (localTaxIdNumTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "taxIdNum"));
                            
                            
                                    if (localTaxIdNum==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxIdNum cannot be null!!");
                                    }
                                    elementList.add(localTaxIdNum);
                                } if (localTimeApproverTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "timeApprover"));
                            
                            
                                    if (localTimeApprover==null){
                                         throw new org.apache.axis2.databinding.ADBException("timeApprover cannot be null!!");
                                    }
                                    elementList.add(localTimeApprover);
                                } if (localTitleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "title"));
                            
                            
                                    if (localTitle==null){
                                         throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                    }
                                    elementList.add(localTitle);
                                } if (localUnbilledOrdersTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "unbilledOrders"));
                            
                            
                                    if (localUnbilledOrders==null){
                                         throw new org.apache.axis2.databinding.ADBException("unbilledOrders cannot be null!!");
                                    }
                                    elementList.add(localUnbilledOrders);
                                } if (localUrlTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "url"));
                            
                            
                                    if (localUrl==null){
                                         throw new org.apache.axis2.databinding.ADBException("url cannot be null!!");
                                    }
                                    elementList.add(localUrl);
                                } if (localVatRegNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "vatRegNumber"));
                            
                            
                                    if (localVatRegNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("vatRegNumber cannot be null!!");
                                    }
                                    elementList.add(localVatRegNumber);
                                } if (localWorkCalendarTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "workCalendar"));
                            
                            
                                    if (localWorkCalendar==null){
                                         throw new org.apache.axis2.databinding.ADBException("workCalendar cannot be null!!");
                                    }
                                    elementList.add(localWorkCalendar);
                                } if (localZipCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "zipCode"));
                            
                            
                                    if (localZipCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("zipCode cannot be null!!");
                                    }
                                    elementList.add(localZipCode);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static VendorSearchBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            VendorSearchBasic object =
                new VendorSearchBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"VendorSearchBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (VendorSearchBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","accountNumber").equals(reader.getName())){
                                
                                                object.setAccountNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address").equals(reader.getName())){
                                
                                                object.setAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressee").equals(reader.getName())){
                                
                                                object.setAddressee(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressLabel").equals(reader.getName())){
                                
                                                object.setAddressLabel(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressPhone").equals(reader.getName())){
                                
                                                object.setAddressPhone(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","attention").equals(reader.getName())){
                                
                                                object.setAttention(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","balance").equals(reader.getName())){
                                
                                                object.setBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billAddress").equals(reader.getName())){
                                
                                                object.setBillAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","category").equals(reader.getName())){
                                
                                                object.setCategory(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","city").equals(reader.getName())){
                                
                                                object.setCity(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","comments").equals(reader.getName())){
                                
                                                object.setComments(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","contact").equals(reader.getName())){
                                
                                                object.setContact(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","country").equals(reader.getName())){
                                
                                                object.setCountry(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","county").equals(reader.getName())){
                                
                                                object.setCounty(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","creditLimit").equals(reader.getName())){
                                
                                                object.setCreditLimit(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency").equals(reader.getName())){
                                
                                                object.setCurrency(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currentExchangeRate").equals(reader.getName())){
                                
                                                object.setCurrentExchangeRate(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated").equals(reader.getName())){
                                
                                                object.setDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","eligibleForCommission").equals(reader.getName())){
                                
                                                object.setEligibleForCommission(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","email").equals(reader.getName())){
                                
                                                object.setEmail(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","emailPreference").equals(reader.getName())){
                                
                                                object.setEmailPreference(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","emailTransactions").equals(reader.getName())){
                                
                                                object.setEmailTransactions(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entityId").equals(reader.getName())){
                                
                                                object.setEntityId(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","expenseAccount").equals(reader.getName())){
                                
                                                object.setExpenseAccount(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                
                                                object.setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString").equals(reader.getName())){
                                
                                                object.setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fax").equals(reader.getName())){
                                
                                                object.setFax(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","faxTransactions").equals(reader.getName())){
                                
                                                object.setFaxTransactions(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","firstName").equals(reader.getName())){
                                
                                                object.setFirstName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxBalance").equals(reader.getName())){
                                
                                                object.setFxBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxUnbilledOrders").equals(reader.getName())){
                                
                                                object.setFxUnbilledOrders(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","giveAccess").equals(reader.getName())){
                                
                                                object.setGiveAccess(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","globalSubscriptionStatus").equals(reader.getName())){
                                
                                                object.setGlobalSubscriptionStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","group").equals(reader.getName())){
                                
                                                object.setGroup(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","hasDuplicates").equals(reader.getName())){
                                
                                                object.setHasDuplicates(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","image").equals(reader.getName())){
                                
                                                object.setImage(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","incoterm").equals(reader.getName())){
                                
                                                object.setIncoterm(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                                object.setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber").equals(reader.getName())){
                                
                                                object.setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","is1099Eligible").equals(reader.getName())){
                                
                                                object.setIs1099Eligible(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isDefaultBilling").equals(reader.getName())){
                                
                                                object.setIsDefaultBilling(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isDefaultShipping").equals(reader.getName())){
                                
                                                object.setIsDefaultShipping(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isInactive").equals(reader.getName())){
                                
                                                object.setIsInactive(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isJobResourceVend").equals(reader.getName())){
                                
                                                object.setIsJobResourceVend(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isPerson").equals(reader.getName())){
                                
                                                object.setIsPerson(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","laborCost").equals(reader.getName())){
                                
                                                object.setLaborCost(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","language").equals(reader.getName())){
                                
                                                object.setLanguage(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                                object.setLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastName").equals(reader.getName())){
                                
                                                object.setLastName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","level").equals(reader.getName())){
                                
                                                object.setLevel(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","middleName").equals(reader.getName())){
                                
                                                object.setMiddleName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","otherRelationships").equals(reader.getName())){
                                
                                                object.setOtherRelationships(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","payablesAccount").equals(reader.getName())){
                                
                                                object.setPayablesAccount(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","pec").equals(reader.getName())){
                                
                                                object.setPec(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","permission").equals(reader.getName())){
                                
                                                object.setPermission(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phone").equals(reader.getName())){
                                
                                                object.setPhone(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phoneticName").equals(reader.getName())){
                                
                                                object.setPhoneticName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","printTransactions").equals(reader.getName())){
                                
                                                object.setPrintTransactions(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","purchaseOrderAmount").equals(reader.getName())){
                                
                                                object.setPurchaseOrderAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","purchaseOrderQuantity").equals(reader.getName())){
                                
                                                object.setPurchaseOrderQuantity(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","purchaseOrderQuantityDiff").equals(reader.getName())){
                                
                                                object.setPurchaseOrderQuantityDiff(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","receiptAmount").equals(reader.getName())){
                                
                                                object.setReceiptAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","receiptQuantity").equals(reader.getName())){
                                
                                                object.setReceiptQuantity(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","receiptQuantityDiff").equals(reader.getName())){
                                
                                                object.setReceiptQuantityDiff(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salutation").equals(reader.getName())){
                                
                                                object.setSalutation(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","shipAddress").equals(reader.getName())){
                                
                                                object.setShipAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","state").equals(reader.getName())){
                                
                                                object.setState(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                                object.setSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","taxIdNum").equals(reader.getName())){
                                
                                                object.setTaxIdNum(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","timeApprover").equals(reader.getName())){
                                
                                                object.setTimeApprover(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","title").equals(reader.getName())){
                                
                                                object.setTitle(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","unbilledOrders").equals(reader.getName())){
                                
                                                object.setUnbilledOrders(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","url").equals(reader.getName())){
                                
                                                object.setUrl(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","vatRegNumber").equals(reader.getName())){
                                
                                                object.setVatRegNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","workCalendar").equals(reader.getName())){
                                
                                                object.setWorkCalendar(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","zipCode").equals(reader.getName())){
                                
                                                object.setZipCode(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    