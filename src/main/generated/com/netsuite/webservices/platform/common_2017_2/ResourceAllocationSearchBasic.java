
/**
 * ResourceAllocationSearchBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  ResourceAllocationSearchBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ResourceAllocationSearchBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRecordBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ResourceAllocationSearchBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for AllocationType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localAllocationType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAllocationTypeTracker = false ;

                           public boolean isAllocationTypeSpecified(){
                               return localAllocationTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getAllocationType(){
                               return localAllocationType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AllocationType
                               */
                               public void setAllocationType(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localAllocationTypeTracker = param != null;
                                   
                                            this.localAllocationType=param;
                                    

                               }
                            

                        /**
                        * field for AllocationUnit
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localAllocationUnit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAllocationUnitTracker = false ;

                           public boolean isAllocationUnitSpecified(){
                               return localAllocationUnitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getAllocationUnit(){
                               return localAllocationUnit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AllocationUnit
                               */
                               public void setAllocationUnit(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localAllocationUnitTracker = param != null;
                                   
                                            this.localAllocationUnit=param;
                                    

                               }
                            

                        /**
                        * field for ApprovalStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localApprovalStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localApprovalStatusTracker = false ;

                           public boolean isApprovalStatusSpecified(){
                               return localApprovalStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getApprovalStatus(){
                               return localApprovalStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ApprovalStatus
                               */
                               public void setApprovalStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localApprovalStatusTracker = param != null;
                                   
                                            this.localApprovalStatus=param;
                                    

                               }
                            

                        /**
                        * field for Customer
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCustomer ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomerTracker = false ;

                           public boolean isCustomerSpecified(){
                               return localCustomerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCustomer(){
                               return localCustomer;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Customer
                               */
                               public void setCustomer(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCustomerTracker = param != null;
                                   
                                            this.localCustomer=param;
                                    

                               }
                            

                        /**
                        * field for EndDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndDateTracker = false ;

                           public boolean isEndDateSpecified(){
                               return localEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getEndDate(){
                               return localEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndDate
                               */
                               public void setEndDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localEndDateTracker = param != null;
                                   
                                            this.localEndDate=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localExternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdTracker = false ;

                           public boolean isExternalIdSpecified(){
                               return localExternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localExternalIdTracker = param != null;
                                   
                                            this.localExternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalIdString
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localExternalIdString ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdStringTracker = false ;

                           public boolean isExternalIdStringSpecified(){
                               return localExternalIdStringTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getExternalIdString(){
                               return localExternalIdString;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalIdString
                               */
                               public void setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localExternalIdStringTracker = param != null;
                                   
                                            this.localExternalIdString=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localInternalIdTracker = param != null;
                                   
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for InternalIdNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localInternalIdNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdNumberTracker = false ;

                           public boolean isInternalIdNumberSpecified(){
                               return localInternalIdNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getInternalIdNumber(){
                               return localInternalIdNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalIdNumber
                               */
                               public void setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localInternalIdNumberTracker = param != null;
                                   
                                            this.localInternalIdNumber=param;
                                    

                               }
                            

                        /**
                        * field for NextApprover
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localNextApprover ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNextApproverTracker = false ;

                           public boolean isNextApproverSpecified(){
                               return localNextApproverTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getNextApprover(){
                               return localNextApprover;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NextApprover
                               */
                               public void setNextApprover(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localNextApproverTracker = param != null;
                                   
                                            this.localNextApprover=param;
                                    

                               }
                            

                        /**
                        * field for Notes
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localNotes ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNotesTracker = false ;

                           public boolean isNotesSpecified(){
                               return localNotesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getNotes(){
                               return localNotes;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Notes
                               */
                               public void setNotes(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localNotesTracker = param != null;
                                   
                                            this.localNotes=param;
                                    

                               }
                            

                        /**
                        * field for NumberHours
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localNumberHours ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNumberHoursTracker = false ;

                           public boolean isNumberHoursSpecified(){
                               return localNumberHoursTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getNumberHours(){
                               return localNumberHours;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NumberHours
                               */
                               public void setNumberHours(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localNumberHoursTracker = param != null;
                                   
                                            this.localNumberHours=param;
                                    

                               }
                            

                        /**
                        * field for PercentOfTime
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localPercentOfTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPercentOfTimeTracker = false ;

                           public boolean isPercentOfTimeSpecified(){
                               return localPercentOfTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getPercentOfTime(){
                               return localPercentOfTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PercentOfTime
                               */
                               public void setPercentOfTime(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localPercentOfTimeTracker = param != null;
                                   
                                            this.localPercentOfTime=param;
                                    

                               }
                            

                        /**
                        * field for Project
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localProject ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProjectTracker = false ;

                           public boolean isProjectSpecified(){
                               return localProjectTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getProject(){
                               return localProject;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Project
                               */
                               public void setProject(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localProjectTracker = param != null;
                                   
                                            this.localProject=param;
                                    

                               }
                            

                        /**
                        * field for RequestedBy
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localRequestedBy ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRequestedByTracker = false ;

                           public boolean isRequestedBySpecified(){
                               return localRequestedByTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getRequestedBy(){
                               return localRequestedBy;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RequestedBy
                               */
                               public void setRequestedBy(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localRequestedByTracker = param != null;
                                   
                                            this.localRequestedBy=param;
                                    

                               }
                            

                        /**
                        * field for Resource
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localResource ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localResourceTracker = false ;

                           public boolean isResourceSpecified(){
                               return localResourceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getResource(){
                               return localResource;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Resource
                               */
                               public void setResource(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localResourceTracker = param != null;
                                   
                                            this.localResource=param;
                                    

                               }
                            

                        /**
                        * field for StartDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateTracker = false ;

                           public boolean isStartDateSpecified(){
                               return localStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getStartDate(){
                               return localStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDate
                               */
                               public void setStartDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localStartDateTracker = param != null;
                                   
                                            this.localStartDate=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ResourceAllocationSearchBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ResourceAllocationSearchBasic",
                           xmlWriter);
                   }

                if (localAllocationTypeTracker){
                                            if (localAllocationType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("allocationType cannot be null!!");
                                            }
                                           localAllocationType.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","allocationType"),
                                               xmlWriter);
                                        } if (localAllocationUnitTracker){
                                            if (localAllocationUnit==null){
                                                 throw new org.apache.axis2.databinding.ADBException("allocationUnit cannot be null!!");
                                            }
                                           localAllocationUnit.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","allocationUnit"),
                                               xmlWriter);
                                        } if (localApprovalStatusTracker){
                                            if (localApprovalStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("approvalStatus cannot be null!!");
                                            }
                                           localApprovalStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","approvalStatus"),
                                               xmlWriter);
                                        } if (localCustomerTracker){
                                            if (localCustomer==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customer cannot be null!!");
                                            }
                                           localCustomer.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customer"),
                                               xmlWriter);
                                        } if (localEndDateTracker){
                                            if (localEndDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                            }
                                           localEndDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endDate"),
                                               xmlWriter);
                                        } if (localExternalIdTracker){
                                            if (localExternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                            }
                                           localExternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId"),
                                               xmlWriter);
                                        } if (localExternalIdStringTracker){
                                            if (localExternalIdString==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                            }
                                           localExternalIdString.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString"),
                                               xmlWriter);
                                        } if (localInternalIdTracker){
                                            if (localInternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                            }
                                           localInternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                               xmlWriter);
                                        } if (localInternalIdNumberTracker){
                                            if (localInternalIdNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                            }
                                           localInternalIdNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber"),
                                               xmlWriter);
                                        } if (localNextApproverTracker){
                                            if (localNextApprover==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nextApprover cannot be null!!");
                                            }
                                           localNextApprover.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nextApprover"),
                                               xmlWriter);
                                        } if (localNotesTracker){
                                            if (localNotes==null){
                                                 throw new org.apache.axis2.databinding.ADBException("notes cannot be null!!");
                                            }
                                           localNotes.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","notes"),
                                               xmlWriter);
                                        } if (localNumberHoursTracker){
                                            if (localNumberHours==null){
                                                 throw new org.apache.axis2.databinding.ADBException("numberHours cannot be null!!");
                                            }
                                           localNumberHours.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","numberHours"),
                                               xmlWriter);
                                        } if (localPercentOfTimeTracker){
                                            if (localPercentOfTime==null){
                                                 throw new org.apache.axis2.databinding.ADBException("percentOfTime cannot be null!!");
                                            }
                                           localPercentOfTime.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","percentOfTime"),
                                               xmlWriter);
                                        } if (localProjectTracker){
                                            if (localProject==null){
                                                 throw new org.apache.axis2.databinding.ADBException("project cannot be null!!");
                                            }
                                           localProject.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","project"),
                                               xmlWriter);
                                        } if (localRequestedByTracker){
                                            if (localRequestedBy==null){
                                                 throw new org.apache.axis2.databinding.ADBException("requestedBy cannot be null!!");
                                            }
                                           localRequestedBy.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","requestedBy"),
                                               xmlWriter);
                                        } if (localResourceTracker){
                                            if (localResource==null){
                                                 throw new org.apache.axis2.databinding.ADBException("resource cannot be null!!");
                                            }
                                           localResource.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","resource"),
                                               xmlWriter);
                                        } if (localStartDateTracker){
                                            if (localStartDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                            }
                                           localStartDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDate"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ResourceAllocationSearchBasic"));
                 if (localAllocationTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "allocationType"));
                            
                            
                                    if (localAllocationType==null){
                                         throw new org.apache.axis2.databinding.ADBException("allocationType cannot be null!!");
                                    }
                                    elementList.add(localAllocationType);
                                } if (localAllocationUnitTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "allocationUnit"));
                            
                            
                                    if (localAllocationUnit==null){
                                         throw new org.apache.axis2.databinding.ADBException("allocationUnit cannot be null!!");
                                    }
                                    elementList.add(localAllocationUnit);
                                } if (localApprovalStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "approvalStatus"));
                            
                            
                                    if (localApprovalStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("approvalStatus cannot be null!!");
                                    }
                                    elementList.add(localApprovalStatus);
                                } if (localCustomerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customer"));
                            
                            
                                    if (localCustomer==null){
                                         throw new org.apache.axis2.databinding.ADBException("customer cannot be null!!");
                                    }
                                    elementList.add(localCustomer);
                                } if (localEndDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "endDate"));
                            
                            
                                    if (localEndDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                    }
                                    elementList.add(localEndDate);
                                } if (localExternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalId"));
                            
                            
                                    if (localExternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                    }
                                    elementList.add(localExternalId);
                                } if (localExternalIdStringTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalIdString"));
                            
                            
                                    if (localExternalIdString==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                    }
                                    elementList.add(localExternalIdString);
                                } if (localInternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalId"));
                            
                            
                                    if (localInternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    }
                                    elementList.add(localInternalId);
                                } if (localInternalIdNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalIdNumber"));
                            
                            
                                    if (localInternalIdNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                    }
                                    elementList.add(localInternalIdNumber);
                                } if (localNextApproverTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "nextApprover"));
                            
                            
                                    if (localNextApprover==null){
                                         throw new org.apache.axis2.databinding.ADBException("nextApprover cannot be null!!");
                                    }
                                    elementList.add(localNextApprover);
                                } if (localNotesTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "notes"));
                            
                            
                                    if (localNotes==null){
                                         throw new org.apache.axis2.databinding.ADBException("notes cannot be null!!");
                                    }
                                    elementList.add(localNotes);
                                } if (localNumberHoursTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "numberHours"));
                            
                            
                                    if (localNumberHours==null){
                                         throw new org.apache.axis2.databinding.ADBException("numberHours cannot be null!!");
                                    }
                                    elementList.add(localNumberHours);
                                } if (localPercentOfTimeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "percentOfTime"));
                            
                            
                                    if (localPercentOfTime==null){
                                         throw new org.apache.axis2.databinding.ADBException("percentOfTime cannot be null!!");
                                    }
                                    elementList.add(localPercentOfTime);
                                } if (localProjectTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "project"));
                            
                            
                                    if (localProject==null){
                                         throw new org.apache.axis2.databinding.ADBException("project cannot be null!!");
                                    }
                                    elementList.add(localProject);
                                } if (localRequestedByTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "requestedBy"));
                            
                            
                                    if (localRequestedBy==null){
                                         throw new org.apache.axis2.databinding.ADBException("requestedBy cannot be null!!");
                                    }
                                    elementList.add(localRequestedBy);
                                } if (localResourceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "resource"));
                            
                            
                                    if (localResource==null){
                                         throw new org.apache.axis2.databinding.ADBException("resource cannot be null!!");
                                    }
                                    elementList.add(localResource);
                                } if (localStartDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "startDate"));
                            
                            
                                    if (localStartDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                    }
                                    elementList.add(localStartDate);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ResourceAllocationSearchBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ResourceAllocationSearchBasic object =
                new ResourceAllocationSearchBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ResourceAllocationSearchBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ResourceAllocationSearchBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","allocationType").equals(reader.getName())){
                                
                                                object.setAllocationType(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","allocationUnit").equals(reader.getName())){
                                
                                                object.setAllocationUnit(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","approvalStatus").equals(reader.getName())){
                                
                                                object.setApprovalStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customer").equals(reader.getName())){
                                
                                                object.setCustomer(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endDate").equals(reader.getName())){
                                
                                                object.setEndDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                
                                                object.setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString").equals(reader.getName())){
                                
                                                object.setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                                object.setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber").equals(reader.getName())){
                                
                                                object.setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nextApprover").equals(reader.getName())){
                                
                                                object.setNextApprover(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","notes").equals(reader.getName())){
                                
                                                object.setNotes(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","numberHours").equals(reader.getName())){
                                
                                                object.setNumberHours(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","percentOfTime").equals(reader.getName())){
                                
                                                object.setPercentOfTime(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","project").equals(reader.getName())){
                                
                                                object.setProject(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","requestedBy").equals(reader.getName())){
                                
                                                object.setRequestedBy(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","resource").equals(reader.getName())){
                                
                                                object.setResource(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDate").equals(reader.getName())){
                                
                                                object.setStartDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    