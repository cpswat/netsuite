
/**
 * CustomerSearchBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  CustomerSearchBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class CustomerSearchBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRecordBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = CustomerSearchBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for AccountNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAccountNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountNumberTracker = false ;

                           public boolean isAccountNumberSpecified(){
                               return localAccountNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAccountNumber(){
                               return localAccountNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccountNumber
                               */
                               public void setAccountNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAccountNumberTracker = param != null;
                                   
                                            this.localAccountNumber=param;
                                    

                               }
                            

                        /**
                        * field for Address
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddressTracker = false ;

                           public boolean isAddressSpecified(){
                               return localAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAddress(){
                               return localAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Address
                               */
                               public void setAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAddressTracker = param != null;
                                   
                                            this.localAddress=param;
                                    

                               }
                            

                        /**
                        * field for Addressee
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAddressee ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddresseeTracker = false ;

                           public boolean isAddresseeSpecified(){
                               return localAddresseeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAddressee(){
                               return localAddressee;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Addressee
                               */
                               public void setAddressee(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAddresseeTracker = param != null;
                                   
                                            this.localAddressee=param;
                                    

                               }
                            

                        /**
                        * field for AddressLabel
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAddressLabel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddressLabelTracker = false ;

                           public boolean isAddressLabelSpecified(){
                               return localAddressLabelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAddressLabel(){
                               return localAddressLabel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AddressLabel
                               */
                               public void setAddressLabel(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAddressLabelTracker = param != null;
                                   
                                            this.localAddressLabel=param;
                                    

                               }
                            

                        /**
                        * field for AddressPhone
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAddressPhone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddressPhoneTracker = false ;

                           public boolean isAddressPhoneSpecified(){
                               return localAddressPhoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAddressPhone(){
                               return localAddressPhone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AddressPhone
                               */
                               public void setAddressPhone(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAddressPhoneTracker = param != null;
                                   
                                            this.localAddressPhone=param;
                                    

                               }
                            

                        /**
                        * field for Attention
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAttention ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAttentionTracker = false ;

                           public boolean isAttentionSpecified(){
                               return localAttentionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAttention(){
                               return localAttention;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Attention
                               */
                               public void setAttention(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAttentionTracker = param != null;
                                   
                                            this.localAttention=param;
                                    

                               }
                            

                        /**
                        * field for AvailableOffline
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localAvailableOffline ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAvailableOfflineTracker = false ;

                           public boolean isAvailableOfflineSpecified(){
                               return localAvailableOfflineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getAvailableOffline(){
                               return localAvailableOffline;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AvailableOffline
                               */
                               public void setAvailableOffline(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localAvailableOfflineTracker = param != null;
                                   
                                            this.localAvailableOffline=param;
                                    

                               }
                            

                        /**
                        * field for Balance
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localBalance ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBalanceTracker = false ;

                           public boolean isBalanceSpecified(){
                               return localBalanceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getBalance(){
                               return localBalance;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Balance
                               */
                               public void setBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localBalanceTracker = param != null;
                                   
                                            this.localBalance=param;
                                    

                               }
                            

                        /**
                        * field for BillAddress
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localBillAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillAddressTracker = false ;

                           public boolean isBillAddressSpecified(){
                               return localBillAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getBillAddress(){
                               return localBillAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillAddress
                               */
                               public void setBillAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localBillAddressTracker = param != null;
                                   
                                            this.localBillAddress=param;
                                    

                               }
                            

                        /**
                        * field for BoughtAmount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localBoughtAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBoughtAmountTracker = false ;

                           public boolean isBoughtAmountSpecified(){
                               return localBoughtAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getBoughtAmount(){
                               return localBoughtAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BoughtAmount
                               */
                               public void setBoughtAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localBoughtAmountTracker = param != null;
                                   
                                            this.localBoughtAmount=param;
                                    

                               }
                            

                        /**
                        * field for BoughtDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localBoughtDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBoughtDateTracker = false ;

                           public boolean isBoughtDateSpecified(){
                               return localBoughtDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getBoughtDate(){
                               return localBoughtDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BoughtDate
                               */
                               public void setBoughtDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localBoughtDateTracker = param != null;
                                   
                                            this.localBoughtDate=param;
                                    

                               }
                            

                        /**
                        * field for BuyingReason
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localBuyingReason ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBuyingReasonTracker = false ;

                           public boolean isBuyingReasonSpecified(){
                               return localBuyingReasonTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getBuyingReason(){
                               return localBuyingReason;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BuyingReason
                               */
                               public void setBuyingReason(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localBuyingReasonTracker = param != null;
                                   
                                            this.localBuyingReason=param;
                                    

                               }
                            

                        /**
                        * field for BuyingTimeFrame
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localBuyingTimeFrame ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBuyingTimeFrameTracker = false ;

                           public boolean isBuyingTimeFrameSpecified(){
                               return localBuyingTimeFrameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getBuyingTimeFrame(){
                               return localBuyingTimeFrame;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BuyingTimeFrame
                               */
                               public void setBuyingTimeFrame(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localBuyingTimeFrameTracker = param != null;
                                   
                                            this.localBuyingTimeFrame=param;
                                    

                               }
                            

                        /**
                        * field for Category
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCategory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCategoryTracker = false ;

                           public boolean isCategorySpecified(){
                               return localCategoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCategory(){
                               return localCategory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Category
                               */
                               public void setCategory(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCategoryTracker = param != null;
                                   
                                            this.localCategory=param;
                                    

                               }
                            

                        /**
                        * field for CcCustomerCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localCcCustomerCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcCustomerCodeTracker = false ;

                           public boolean isCcCustomerCodeSpecified(){
                               return localCcCustomerCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getCcCustomerCode(){
                               return localCcCustomerCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcCustomerCode
                               */
                               public void setCcCustomerCode(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCcCustomerCodeTracker = param != null;
                                   
                                            this.localCcCustomerCode=param;
                                    

                               }
                            

                        /**
                        * field for CcDefault
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localCcDefault ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcDefaultTracker = false ;

                           public boolean isCcDefaultSpecified(){
                               return localCcDefaultTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getCcDefault(){
                               return localCcDefault;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcDefault
                               */
                               public void setCcDefault(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localCcDefaultTracker = param != null;
                                   
                                            this.localCcDefault=param;
                                    

                               }
                            

                        /**
                        * field for CcExpDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localCcExpDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcExpDateTracker = false ;

                           public boolean isCcExpDateSpecified(){
                               return localCcExpDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getCcExpDate(){
                               return localCcExpDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcExpDate
                               */
                               public void setCcExpDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localCcExpDateTracker = param != null;
                                   
                                            this.localCcExpDate=param;
                                    

                               }
                            

                        /**
                        * field for CcHolderName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localCcHolderName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcHolderNameTracker = false ;

                           public boolean isCcHolderNameSpecified(){
                               return localCcHolderNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getCcHolderName(){
                               return localCcHolderName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcHolderName
                               */
                               public void setCcHolderName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCcHolderNameTracker = param != null;
                                   
                                            this.localCcHolderName=param;
                                    

                               }
                            

                        /**
                        * field for CcNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localCcNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcNumberTracker = false ;

                           public boolean isCcNumberSpecified(){
                               return localCcNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getCcNumber(){
                               return localCcNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcNumber
                               */
                               public void setCcNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCcNumberTracker = param != null;
                                   
                                            this.localCcNumber=param;
                                    

                               }
                            

                        /**
                        * field for CcState
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCcState ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcStateTracker = false ;

                           public boolean isCcStateSpecified(){
                               return localCcStateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCcState(){
                               return localCcState;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcState
                               */
                               public void setCcState(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCcStateTracker = param != null;
                                   
                                            this.localCcState=param;
                                    

                               }
                            

                        /**
                        * field for CcStateFrom
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localCcStateFrom ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcStateFromTracker = false ;

                           public boolean isCcStateFromSpecified(){
                               return localCcStateFromTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getCcStateFrom(){
                               return localCcStateFrom;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcStateFrom
                               */
                               public void setCcStateFrom(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localCcStateFromTracker = param != null;
                                   
                                            this.localCcStateFrom=param;
                                    

                               }
                            

                        /**
                        * field for CcType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCcType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCcTypeTracker = false ;

                           public boolean isCcTypeSpecified(){
                               return localCcTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCcType(){
                               return localCcType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CcType
                               */
                               public void setCcType(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCcTypeTracker = param != null;
                                   
                                            this.localCcType=param;
                                    

                               }
                            

                        /**
                        * field for City
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localCity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCityTracker = false ;

                           public boolean isCitySpecified(){
                               return localCityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getCity(){
                               return localCity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param City
                               */
                               public void setCity(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCityTracker = param != null;
                                   
                                            this.localCity=param;
                                    

                               }
                            

                        /**
                        * field for ClassBought
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localClassBought ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localClassBoughtTracker = false ;

                           public boolean isClassBoughtSpecified(){
                               return localClassBoughtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getClassBought(){
                               return localClassBought;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ClassBought
                               */
                               public void setClassBought(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localClassBoughtTracker = param != null;
                                   
                                            this.localClassBought=param;
                                    

                               }
                            

                        /**
                        * field for Comments
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localComments ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCommentsTracker = false ;

                           public boolean isCommentsSpecified(){
                               return localCommentsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getComments(){
                               return localComments;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Comments
                               */
                               public void setComments(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCommentsTracker = param != null;
                                   
                                            this.localComments=param;
                                    

                               }
                            

                        /**
                        * field for CompanyName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localCompanyName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCompanyNameTracker = false ;

                           public boolean isCompanyNameSpecified(){
                               return localCompanyNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getCompanyName(){
                               return localCompanyName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CompanyName
                               */
                               public void setCompanyName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCompanyNameTracker = param != null;
                                   
                                            this.localCompanyName=param;
                                    

                               }
                            

                        /**
                        * field for ConsolBalance
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localConsolBalance ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localConsolBalanceTracker = false ;

                           public boolean isConsolBalanceSpecified(){
                               return localConsolBalanceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getConsolBalance(){
                               return localConsolBalance;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ConsolBalance
                               */
                               public void setConsolBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localConsolBalanceTracker = param != null;
                                   
                                            this.localConsolBalance=param;
                                    

                               }
                            

                        /**
                        * field for ConsolDaysOverdue
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localConsolDaysOverdue ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localConsolDaysOverdueTracker = false ;

                           public boolean isConsolDaysOverdueSpecified(){
                               return localConsolDaysOverdueTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getConsolDaysOverdue(){
                               return localConsolDaysOverdue;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ConsolDaysOverdue
                               */
                               public void setConsolDaysOverdue(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localConsolDaysOverdueTracker = param != null;
                                   
                                            this.localConsolDaysOverdue=param;
                                    

                               }
                            

                        /**
                        * field for ConsolDepositBalance
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localConsolDepositBalance ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localConsolDepositBalanceTracker = false ;

                           public boolean isConsolDepositBalanceSpecified(){
                               return localConsolDepositBalanceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getConsolDepositBalance(){
                               return localConsolDepositBalance;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ConsolDepositBalance
                               */
                               public void setConsolDepositBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localConsolDepositBalanceTracker = param != null;
                                   
                                            this.localConsolDepositBalance=param;
                                    

                               }
                            

                        /**
                        * field for ConsolOverdueBalance
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localConsolOverdueBalance ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localConsolOverdueBalanceTracker = false ;

                           public boolean isConsolOverdueBalanceSpecified(){
                               return localConsolOverdueBalanceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getConsolOverdueBalance(){
                               return localConsolOverdueBalance;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ConsolOverdueBalance
                               */
                               public void setConsolOverdueBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localConsolOverdueBalanceTracker = param != null;
                                   
                                            this.localConsolOverdueBalance=param;
                                    

                               }
                            

                        /**
                        * field for ConsolUnbilledOrders
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localConsolUnbilledOrders ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localConsolUnbilledOrdersTracker = false ;

                           public boolean isConsolUnbilledOrdersSpecified(){
                               return localConsolUnbilledOrdersTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getConsolUnbilledOrders(){
                               return localConsolUnbilledOrders;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ConsolUnbilledOrders
                               */
                               public void setConsolUnbilledOrders(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localConsolUnbilledOrdersTracker = param != null;
                                   
                                            this.localConsolUnbilledOrders=param;
                                    

                               }
                            

                        /**
                        * field for Contact
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localContact ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContactTracker = false ;

                           public boolean isContactSpecified(){
                               return localContactTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getContact(){
                               return localContact;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Contact
                               */
                               public void setContact(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localContactTracker = param != null;
                                   
                                            this.localContact=param;
                                    

                               }
                            

                        /**
                        * field for Contribution
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localContribution ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContributionTracker = false ;

                           public boolean isContributionSpecified(){
                               return localContributionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getContribution(){
                               return localContribution;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Contribution
                               */
                               public void setContribution(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localContributionTracker = param != null;
                                   
                                            this.localContribution=param;
                                    

                               }
                            

                        /**
                        * field for ConversionDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localConversionDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localConversionDateTracker = false ;

                           public boolean isConversionDateSpecified(){
                               return localConversionDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getConversionDate(){
                               return localConversionDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ConversionDate
                               */
                               public void setConversionDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localConversionDateTracker = param != null;
                                   
                                            this.localConversionDate=param;
                                    

                               }
                            

                        /**
                        * field for Country
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localCountry ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCountryTracker = false ;

                           public boolean isCountrySpecified(){
                               return localCountryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getCountry(){
                               return localCountry;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Country
                               */
                               public void setCountry(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localCountryTracker = param != null;
                                   
                                            this.localCountry=param;
                                    

                               }
                            

                        /**
                        * field for County
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localCounty ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCountyTracker = false ;

                           public boolean isCountySpecified(){
                               return localCountyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getCounty(){
                               return localCounty;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param County
                               */
                               public void setCounty(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCountyTracker = param != null;
                                   
                                            this.localCounty=param;
                                    

                               }
                            

                        /**
                        * field for CreditHold
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localCreditHold ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreditHoldTracker = false ;

                           public boolean isCreditHoldSpecified(){
                               return localCreditHoldTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getCreditHold(){
                               return localCreditHold;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreditHold
                               */
                               public void setCreditHold(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localCreditHoldTracker = param != null;
                                   
                                            this.localCreditHold=param;
                                    

                               }
                            

                        /**
                        * field for CreditHoldOverride
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localCreditHoldOverride ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreditHoldOverrideTracker = false ;

                           public boolean isCreditHoldOverrideSpecified(){
                               return localCreditHoldOverrideTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getCreditHoldOverride(){
                               return localCreditHoldOverride;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreditHoldOverride
                               */
                               public void setCreditHoldOverride(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localCreditHoldOverrideTracker = param != null;
                                   
                                            this.localCreditHoldOverride=param;
                                    

                               }
                            

                        /**
                        * field for CreditLimit
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localCreditLimit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreditLimitTracker = false ;

                           public boolean isCreditLimitSpecified(){
                               return localCreditLimitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getCreditLimit(){
                               return localCreditLimit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreditLimit
                               */
                               public void setCreditLimit(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localCreditLimitTracker = param != null;
                                   
                                            this.localCreditLimit=param;
                                    

                               }
                            

                        /**
                        * field for Currency
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrencyTracker = false ;

                           public boolean isCurrencySpecified(){
                               return localCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCurrency(){
                               return localCurrency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Currency
                               */
                               public void setCurrency(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCurrencyTracker = param != null;
                                   
                                            this.localCurrency=param;
                                    

                               }
                            

                        /**
                        * field for CustStage
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCustStage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustStageTracker = false ;

                           public boolean isCustStageSpecified(){
                               return localCustStageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCustStage(){
                               return localCustStage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustStage
                               */
                               public void setCustStage(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCustStageTracker = param != null;
                                   
                                            this.localCustStage=param;
                                    

                               }
                            

                        /**
                        * field for CustStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCustStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustStatusTracker = false ;

                           public boolean isCustStatusSpecified(){
                               return localCustStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCustStatus(){
                               return localCustStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustStatus
                               */
                               public void setCustStatus(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCustStatusTracker = param != null;
                                   
                                            this.localCustStatus=param;
                                    

                               }
                            

                        /**
                        * field for DateClosed
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localDateClosed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDateClosedTracker = false ;

                           public boolean isDateClosedSpecified(){
                               return localDateClosedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getDateClosed(){
                               return localDateClosed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DateClosed
                               */
                               public void setDateClosed(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localDateClosedTracker = param != null;
                                   
                                            this.localDateClosed=param;
                                    

                               }
                            

                        /**
                        * field for DateCreated
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localDateCreated ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDateCreatedTracker = false ;

                           public boolean isDateCreatedSpecified(){
                               return localDateCreatedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getDateCreated(){
                               return localDateCreated;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DateCreated
                               */
                               public void setDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localDateCreatedTracker = param != null;
                                   
                                            this.localDateCreated=param;
                                    

                               }
                            

                        /**
                        * field for DaysOverdue
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localDaysOverdue ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDaysOverdueTracker = false ;

                           public boolean isDaysOverdueSpecified(){
                               return localDaysOverdueTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getDaysOverdue(){
                               return localDaysOverdue;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DaysOverdue
                               */
                               public void setDaysOverdue(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localDaysOverdueTracker = param != null;
                                   
                                            this.localDaysOverdue=param;
                                    

                               }
                            

                        /**
                        * field for DefaultOrderPriority
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localDefaultOrderPriority ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDefaultOrderPriorityTracker = false ;

                           public boolean isDefaultOrderPrioritySpecified(){
                               return localDefaultOrderPriorityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getDefaultOrderPriority(){
                               return localDefaultOrderPriority;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DefaultOrderPriority
                               */
                               public void setDefaultOrderPriority(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localDefaultOrderPriorityTracker = param != null;
                                   
                                            this.localDefaultOrderPriority=param;
                                    

                               }
                            

                        /**
                        * field for DepositBalance
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localDepositBalance ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepositBalanceTracker = false ;

                           public boolean isDepositBalanceSpecified(){
                               return localDepositBalanceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getDepositBalance(){
                               return localDepositBalance;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DepositBalance
                               */
                               public void setDepositBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localDepositBalanceTracker = param != null;
                                   
                                            this.localDepositBalance=param;
                                    

                               }
                            

                        /**
                        * field for DeptBought
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localDeptBought ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeptBoughtTracker = false ;

                           public boolean isDeptBoughtSpecified(){
                               return localDeptBoughtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getDeptBought(){
                               return localDeptBought;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeptBought
                               */
                               public void setDeptBought(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localDeptBoughtTracker = param != null;
                                   
                                            this.localDeptBought=param;
                                    

                               }
                            

                        /**
                        * field for DrAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localDrAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDrAccountTracker = false ;

                           public boolean isDrAccountSpecified(){
                               return localDrAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getDrAccount(){
                               return localDrAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DrAccount
                               */
                               public void setDrAccount(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localDrAccountTracker = param != null;
                                   
                                            this.localDrAccount=param;
                                    

                               }
                            

                        /**
                        * field for Email
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailTracker = false ;

                           public boolean isEmailSpecified(){
                               return localEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getEmail(){
                               return localEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Email
                               */
                               public void setEmail(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localEmailTracker = param != null;
                                   
                                            this.localEmail=param;
                                    

                               }
                            

                        /**
                        * field for EmailPreference
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localEmailPreference ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailPreferenceTracker = false ;

                           public boolean isEmailPreferenceSpecified(){
                               return localEmailPreferenceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getEmailPreference(){
                               return localEmailPreference;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmailPreference
                               */
                               public void setEmailPreference(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localEmailPreferenceTracker = param != null;
                                   
                                            this.localEmailPreference=param;
                                    

                               }
                            

                        /**
                        * field for EmailTransactions
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localEmailTransactions ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailTransactionsTracker = false ;

                           public boolean isEmailTransactionsSpecified(){
                               return localEmailTransactionsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getEmailTransactions(){
                               return localEmailTransactions;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmailTransactions
                               */
                               public void setEmailTransactions(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localEmailTransactionsTracker = param != null;
                                   
                                            this.localEmailTransactions=param;
                                    

                               }
                            

                        /**
                        * field for EndDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndDateTracker = false ;

                           public boolean isEndDateSpecified(){
                               return localEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getEndDate(){
                               return localEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndDate
                               */
                               public void setEndDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localEndDateTracker = param != null;
                                   
                                            this.localEndDate=param;
                                    

                               }
                            

                        /**
                        * field for EntityId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localEntityId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityIdTracker = false ;

                           public boolean isEntityIdSpecified(){
                               return localEntityIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getEntityId(){
                               return localEntityId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EntityId
                               */
                               public void setEntityId(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localEntityIdTracker = param != null;
                                   
                                            this.localEntityId=param;
                                    

                               }
                            

                        /**
                        * field for EntityStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localEntityStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityStatusTracker = false ;

                           public boolean isEntityStatusSpecified(){
                               return localEntityStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getEntityStatus(){
                               return localEntityStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EntityStatus
                               */
                               public void setEntityStatus(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localEntityStatusTracker = param != null;
                                   
                                            this.localEntityStatus=param;
                                    

                               }
                            

                        /**
                        * field for EstimatedBudget
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localEstimatedBudget ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstimatedBudgetTracker = false ;

                           public boolean isEstimatedBudgetSpecified(){
                               return localEstimatedBudgetTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getEstimatedBudget(){
                               return localEstimatedBudget;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstimatedBudget
                               */
                               public void setEstimatedBudget(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localEstimatedBudgetTracker = param != null;
                                   
                                            this.localEstimatedBudget=param;
                                    

                               }
                            

                        /**
                        * field for ExplicitConversion
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localExplicitConversion ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExplicitConversionTracker = false ;

                           public boolean isExplicitConversionSpecified(){
                               return localExplicitConversionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getExplicitConversion(){
                               return localExplicitConversion;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExplicitConversion
                               */
                               public void setExplicitConversion(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localExplicitConversionTracker = param != null;
                                   
                                            this.localExplicitConversion=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localExternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdTracker = false ;

                           public boolean isExternalIdSpecified(){
                               return localExternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localExternalIdTracker = param != null;
                                   
                                            this.localExternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalIdString
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localExternalIdString ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdStringTracker = false ;

                           public boolean isExternalIdStringSpecified(){
                               return localExternalIdStringTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getExternalIdString(){
                               return localExternalIdString;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalIdString
                               */
                               public void setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localExternalIdStringTracker = param != null;
                                   
                                            this.localExternalIdString=param;
                                    

                               }
                            

                        /**
                        * field for Fax
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localFax ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFaxTracker = false ;

                           public boolean isFaxSpecified(){
                               return localFaxTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getFax(){
                               return localFax;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Fax
                               */
                               public void setFax(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localFaxTracker = param != null;
                                   
                                            this.localFax=param;
                                    

                               }
                            

                        /**
                        * field for FaxTransactions
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localFaxTransactions ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFaxTransactionsTracker = false ;

                           public boolean isFaxTransactionsSpecified(){
                               return localFaxTransactionsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getFaxTransactions(){
                               return localFaxTransactions;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FaxTransactions
                               */
                               public void setFaxTransactions(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localFaxTransactionsTracker = param != null;
                                   
                                            this.localFaxTransactions=param;
                                    

                               }
                            

                        /**
                        * field for FirstName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localFirstName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFirstNameTracker = false ;

                           public boolean isFirstNameSpecified(){
                               return localFirstNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getFirstName(){
                               return localFirstName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FirstName
                               */
                               public void setFirstName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localFirstNameTracker = param != null;
                                   
                                            this.localFirstName=param;
                                    

                               }
                            

                        /**
                        * field for FirstOrderDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localFirstOrderDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFirstOrderDateTracker = false ;

                           public boolean isFirstOrderDateSpecified(){
                               return localFirstOrderDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getFirstOrderDate(){
                               return localFirstOrderDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FirstOrderDate
                               */
                               public void setFirstOrderDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localFirstOrderDateTracker = param != null;
                                   
                                            this.localFirstOrderDate=param;
                                    

                               }
                            

                        /**
                        * field for FirstSaleDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localFirstSaleDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFirstSaleDateTracker = false ;

                           public boolean isFirstSaleDateSpecified(){
                               return localFirstSaleDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getFirstSaleDate(){
                               return localFirstSaleDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FirstSaleDate
                               */
                               public void setFirstSaleDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localFirstSaleDateTracker = param != null;
                                   
                                            this.localFirstSaleDate=param;
                                    

                               }
                            

                        /**
                        * field for FxAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localFxAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFxAccountTracker = false ;

                           public boolean isFxAccountSpecified(){
                               return localFxAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getFxAccount(){
                               return localFxAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FxAccount
                               */
                               public void setFxAccount(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localFxAccountTracker = param != null;
                                   
                                            this.localFxAccount=param;
                                    

                               }
                            

                        /**
                        * field for FxBalance
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localFxBalance ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFxBalanceTracker = false ;

                           public boolean isFxBalanceSpecified(){
                               return localFxBalanceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getFxBalance(){
                               return localFxBalance;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FxBalance
                               */
                               public void setFxBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localFxBalanceTracker = param != null;
                                   
                                            this.localFxBalance=param;
                                    

                               }
                            

                        /**
                        * field for FxConsolBalance
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localFxConsolBalance ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFxConsolBalanceTracker = false ;

                           public boolean isFxConsolBalanceSpecified(){
                               return localFxConsolBalanceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getFxConsolBalance(){
                               return localFxConsolBalance;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FxConsolBalance
                               */
                               public void setFxConsolBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localFxConsolBalanceTracker = param != null;
                                   
                                            this.localFxConsolBalance=param;
                                    

                               }
                            

                        /**
                        * field for FxConsolUnbilledOrders
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localFxConsolUnbilledOrders ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFxConsolUnbilledOrdersTracker = false ;

                           public boolean isFxConsolUnbilledOrdersSpecified(){
                               return localFxConsolUnbilledOrdersTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getFxConsolUnbilledOrders(){
                               return localFxConsolUnbilledOrders;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FxConsolUnbilledOrders
                               */
                               public void setFxConsolUnbilledOrders(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localFxConsolUnbilledOrdersTracker = param != null;
                                   
                                            this.localFxConsolUnbilledOrders=param;
                                    

                               }
                            

                        /**
                        * field for FxUnbilledOrders
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localFxUnbilledOrders ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFxUnbilledOrdersTracker = false ;

                           public boolean isFxUnbilledOrdersSpecified(){
                               return localFxUnbilledOrdersTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getFxUnbilledOrders(){
                               return localFxUnbilledOrders;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FxUnbilledOrders
                               */
                               public void setFxUnbilledOrders(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localFxUnbilledOrdersTracker = param != null;
                                   
                                            this.localFxUnbilledOrders=param;
                                    

                               }
                            

                        /**
                        * field for GiveAccess
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localGiveAccess ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiveAccessTracker = false ;

                           public boolean isGiveAccessSpecified(){
                               return localGiveAccessTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getGiveAccess(){
                               return localGiveAccess;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiveAccess
                               */
                               public void setGiveAccess(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localGiveAccessTracker = param != null;
                                   
                                            this.localGiveAccess=param;
                                    

                               }
                            

                        /**
                        * field for GlobalSubscriptionStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localGlobalSubscriptionStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGlobalSubscriptionStatusTracker = false ;

                           public boolean isGlobalSubscriptionStatusSpecified(){
                               return localGlobalSubscriptionStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getGlobalSubscriptionStatus(){
                               return localGlobalSubscriptionStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GlobalSubscriptionStatus
                               */
                               public void setGlobalSubscriptionStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localGlobalSubscriptionStatusTracker = param != null;
                                   
                                            this.localGlobalSubscriptionStatus=param;
                                    

                               }
                            

                        /**
                        * field for Group
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGroupTracker = false ;

                           public boolean isGroupSpecified(){
                               return localGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getGroup(){
                               return localGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Group
                               */
                               public void setGroup(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localGroupTracker = param != null;
                                   
                                            this.localGroup=param;
                                    

                               }
                            

                        /**
                        * field for GroupPricingLevel
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localGroupPricingLevel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGroupPricingLevelTracker = false ;

                           public boolean isGroupPricingLevelSpecified(){
                               return localGroupPricingLevelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getGroupPricingLevel(){
                               return localGroupPricingLevel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GroupPricingLevel
                               */
                               public void setGroupPricingLevel(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localGroupPricingLevelTracker = param != null;
                                   
                                            this.localGroupPricingLevel=param;
                                    

                               }
                            

                        /**
                        * field for HasDuplicates
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localHasDuplicates ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHasDuplicatesTracker = false ;

                           public boolean isHasDuplicatesSpecified(){
                               return localHasDuplicatesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getHasDuplicates(){
                               return localHasDuplicates;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HasDuplicates
                               */
                               public void setHasDuplicates(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localHasDuplicatesTracker = param != null;
                                   
                                            this.localHasDuplicates=param;
                                    

                               }
                            

                        /**
                        * field for Image
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localImage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localImageTracker = false ;

                           public boolean isImageSpecified(){
                               return localImageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getImage(){
                               return localImage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Image
                               */
                               public void setImage(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localImageTracker = param != null;
                                   
                                            this.localImage=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localInternalIdTracker = param != null;
                                   
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for InternalIdNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localInternalIdNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdNumberTracker = false ;

                           public boolean isInternalIdNumberSpecified(){
                               return localInternalIdNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getInternalIdNumber(){
                               return localInternalIdNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalIdNumber
                               */
                               public void setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localInternalIdNumberTracker = param != null;
                                   
                                            this.localInternalIdNumber=param;
                                    

                               }
                            

                        /**
                        * field for IsBudgetApproved
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsBudgetApproved ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsBudgetApprovedTracker = false ;

                           public boolean isIsBudgetApprovedSpecified(){
                               return localIsBudgetApprovedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsBudgetApproved(){
                               return localIsBudgetApproved;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsBudgetApproved
                               */
                               public void setIsBudgetApproved(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsBudgetApprovedTracker = param != null;
                                   
                                            this.localIsBudgetApproved=param;
                                    

                               }
                            

                        /**
                        * field for IsDefaultBilling
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsDefaultBilling ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsDefaultBillingTracker = false ;

                           public boolean isIsDefaultBillingSpecified(){
                               return localIsDefaultBillingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsDefaultBilling(){
                               return localIsDefaultBilling;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsDefaultBilling
                               */
                               public void setIsDefaultBilling(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsDefaultBillingTracker = param != null;
                                   
                                            this.localIsDefaultBilling=param;
                                    

                               }
                            

                        /**
                        * field for IsDefaultShipping
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsDefaultShipping ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsDefaultShippingTracker = false ;

                           public boolean isIsDefaultShippingSpecified(){
                               return localIsDefaultShippingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsDefaultShipping(){
                               return localIsDefaultShipping;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsDefaultShipping
                               */
                               public void setIsDefaultShipping(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsDefaultShippingTracker = param != null;
                                   
                                            this.localIsDefaultShipping=param;
                                    

                               }
                            

                        /**
                        * field for IsInactive
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsInactive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsInactiveTracker = false ;

                           public boolean isIsInactiveSpecified(){
                               return localIsInactiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsInactive(){
                               return localIsInactive;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsInactive
                               */
                               public void setIsInactive(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsInactiveTracker = param != null;
                                   
                                            this.localIsInactive=param;
                                    

                               }
                            

                        /**
                        * field for IsPerson
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsPerson ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsPersonTracker = false ;

                           public boolean isIsPersonSpecified(){
                               return localIsPersonTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsPerson(){
                               return localIsPerson;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsPerson
                               */
                               public void setIsPerson(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsPersonTracker = param != null;
                                   
                                            this.localIsPerson=param;
                                    

                               }
                            

                        /**
                        * field for IsReportedLead
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsReportedLead ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsReportedLeadTracker = false ;

                           public boolean isIsReportedLeadSpecified(){
                               return localIsReportedLeadTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsReportedLead(){
                               return localIsReportedLead;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsReportedLead
                               */
                               public void setIsReportedLead(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsReportedLeadTracker = param != null;
                                   
                                            this.localIsReportedLead=param;
                                    

                               }
                            

                        /**
                        * field for IsShipAddress
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsShipAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsShipAddressTracker = false ;

                           public boolean isIsShipAddressSpecified(){
                               return localIsShipAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsShipAddress(){
                               return localIsShipAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsShipAddress
                               */
                               public void setIsShipAddress(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsShipAddressTracker = param != null;
                                   
                                            this.localIsShipAddress=param;
                                    

                               }
                            

                        /**
                        * field for ItemPricingLevel
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localItemPricingLevel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemPricingLevelTracker = false ;

                           public boolean isItemPricingLevelSpecified(){
                               return localItemPricingLevelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getItemPricingLevel(){
                               return localItemPricingLevel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemPricingLevel
                               */
                               public void setItemPricingLevel(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localItemPricingLevelTracker = param != null;
                                   
                                            this.localItemPricingLevel=param;
                                    

                               }
                            

                        /**
                        * field for ItemPricingUnitPrice
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localItemPricingUnitPrice ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemPricingUnitPriceTracker = false ;

                           public boolean isItemPricingUnitPriceSpecified(){
                               return localItemPricingUnitPriceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getItemPricingUnitPrice(){
                               return localItemPricingUnitPrice;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemPricingUnitPrice
                               */
                               public void setItemPricingUnitPrice(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localItemPricingUnitPriceTracker = param != null;
                                   
                                            this.localItemPricingUnitPrice=param;
                                    

                               }
                            

                        /**
                        * field for ItemsBought
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localItemsBought ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemsBoughtTracker = false ;

                           public boolean isItemsBoughtSpecified(){
                               return localItemsBoughtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getItemsBought(){
                               return localItemsBought;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemsBought
                               */
                               public void setItemsBought(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localItemsBoughtTracker = param != null;
                                   
                                            this.localItemsBought=param;
                                    

                               }
                            

                        /**
                        * field for ItemsOrdered
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localItemsOrdered ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemsOrderedTracker = false ;

                           public boolean isItemsOrderedSpecified(){
                               return localItemsOrderedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getItemsOrdered(){
                               return localItemsOrdered;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ItemsOrdered
                               */
                               public void setItemsOrdered(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localItemsOrderedTracker = param != null;
                                   
                                            this.localItemsOrdered=param;
                                    

                               }
                            

                        /**
                        * field for Language
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localLanguage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLanguageTracker = false ;

                           public boolean isLanguageSpecified(){
                               return localLanguageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getLanguage(){
                               return localLanguage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Language
                               */
                               public void setLanguage(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localLanguageTracker = param != null;
                                   
                                            this.localLanguage=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for LastName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localLastName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastNameTracker = false ;

                           public boolean isLastNameSpecified(){
                               return localLastNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getLastName(){
                               return localLastName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastName
                               */
                               public void setLastName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localLastNameTracker = param != null;
                                   
                                            this.localLastName=param;
                                    

                               }
                            

                        /**
                        * field for LastOrderDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localLastOrderDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastOrderDateTracker = false ;

                           public boolean isLastOrderDateSpecified(){
                               return localLastOrderDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getLastOrderDate(){
                               return localLastOrderDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastOrderDate
                               */
                               public void setLastOrderDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localLastOrderDateTracker = param != null;
                                   
                                            this.localLastOrderDate=param;
                                    

                               }
                            

                        /**
                        * field for LastSaleDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localLastSaleDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastSaleDateTracker = false ;

                           public boolean isLastSaleDateSpecified(){
                               return localLastSaleDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getLastSaleDate(){
                               return localLastSaleDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastSaleDate
                               */
                               public void setLastSaleDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localLastSaleDateTracker = param != null;
                                   
                                            this.localLastSaleDate=param;
                                    

                               }
                            

                        /**
                        * field for LeadDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localLeadDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLeadDateTracker = false ;

                           public boolean isLeadDateSpecified(){
                               return localLeadDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getLeadDate(){
                               return localLeadDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LeadDate
                               */
                               public void setLeadDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localLeadDateTracker = param != null;
                                   
                                            this.localLeadDate=param;
                                    

                               }
                            

                        /**
                        * field for LeadSource
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localLeadSource ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLeadSourceTracker = false ;

                           public boolean isLeadSourceSpecified(){
                               return localLeadSourceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getLeadSource(){
                               return localLeadSource;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LeadSource
                               */
                               public void setLeadSource(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localLeadSourceTracker = param != null;
                                   
                                            this.localLeadSource=param;
                                    

                               }
                            

                        /**
                        * field for Level
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localLevel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLevelTracker = false ;

                           public boolean isLevelSpecified(){
                               return localLevelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getLevel(){
                               return localLevel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Level
                               */
                               public void setLevel(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localLevelTracker = param != null;
                                   
                                            this.localLevel=param;
                                    

                               }
                            

                        /**
                        * field for LocationBought
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localLocationBought ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationBoughtTracker = false ;

                           public boolean isLocationBoughtSpecified(){
                               return localLocationBoughtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getLocationBought(){
                               return localLocationBought;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationBought
                               */
                               public void setLocationBought(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localLocationBoughtTracker = param != null;
                                   
                                            this.localLocationBought=param;
                                    

                               }
                            

                        /**
                        * field for ManualCreditHold
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localManualCreditHold ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localManualCreditHoldTracker = false ;

                           public boolean isManualCreditHoldSpecified(){
                               return localManualCreditHoldTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getManualCreditHold(){
                               return localManualCreditHold;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ManualCreditHold
                               */
                               public void setManualCreditHold(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localManualCreditHoldTracker = param != null;
                                   
                                            this.localManualCreditHold=param;
                                    

                               }
                            

                        /**
                        * field for MerchantAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localMerchantAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMerchantAccountTracker = false ;

                           public boolean isMerchantAccountSpecified(){
                               return localMerchantAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getMerchantAccount(){
                               return localMerchantAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MerchantAccount
                               */
                               public void setMerchantAccount(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localMerchantAccountTracker = param != null;
                                   
                                            this.localMerchantAccount=param;
                                    

                               }
                            

                        /**
                        * field for MiddleName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localMiddleName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMiddleNameTracker = false ;

                           public boolean isMiddleNameSpecified(){
                               return localMiddleNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getMiddleName(){
                               return localMiddleName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MiddleName
                               */
                               public void setMiddleName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localMiddleNameTracker = param != null;
                                   
                                            this.localMiddleName=param;
                                    

                               }
                            

                        /**
                        * field for MonthlyClosing
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localMonthlyClosing ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMonthlyClosingTracker = false ;

                           public boolean isMonthlyClosingSpecified(){
                               return localMonthlyClosingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getMonthlyClosing(){
                               return localMonthlyClosing;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MonthlyClosing
                               */
                               public void setMonthlyClosing(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localMonthlyClosingTracker = param != null;
                                   
                                            this.localMonthlyClosing=param;
                                    

                               }
                            

                        /**
                        * field for OnCreditHold
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localOnCreditHold ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOnCreditHoldTracker = false ;

                           public boolean isOnCreditHoldSpecified(){
                               return localOnCreditHoldTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getOnCreditHold(){
                               return localOnCreditHold;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OnCreditHold
                               */
                               public void setOnCreditHold(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localOnCreditHoldTracker = param != null;
                                   
                                            this.localOnCreditHold=param;
                                    

                               }
                            

                        /**
                        * field for OrderedAmount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localOrderedAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOrderedAmountTracker = false ;

                           public boolean isOrderedAmountSpecified(){
                               return localOrderedAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getOrderedAmount(){
                               return localOrderedAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OrderedAmount
                               */
                               public void setOrderedAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localOrderedAmountTracker = param != null;
                                   
                                            this.localOrderedAmount=param;
                                    

                               }
                            

                        /**
                        * field for OrderedDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localOrderedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOrderedDateTracker = false ;

                           public boolean isOrderedDateSpecified(){
                               return localOrderedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getOrderedDate(){
                               return localOrderedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OrderedDate
                               */
                               public void setOrderedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localOrderedDateTracker = param != null;
                                   
                                            this.localOrderedDate=param;
                                    

                               }
                            

                        /**
                        * field for OtherRelationships
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localOtherRelationships ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOtherRelationshipsTracker = false ;

                           public boolean isOtherRelationshipsSpecified(){
                               return localOtherRelationshipsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getOtherRelationships(){
                               return localOtherRelationships;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OtherRelationships
                               */
                               public void setOtherRelationships(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localOtherRelationshipsTracker = param != null;
                                   
                                            this.localOtherRelationships=param;
                                    

                               }
                            

                        /**
                        * field for OverdueBalance
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localOverdueBalance ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOverdueBalanceTracker = false ;

                           public boolean isOverdueBalanceSpecified(){
                               return localOverdueBalanceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getOverdueBalance(){
                               return localOverdueBalance;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OverdueBalance
                               */
                               public void setOverdueBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localOverdueBalanceTracker = param != null;
                                   
                                            this.localOverdueBalance=param;
                                    

                               }
                            

                        /**
                        * field for Parent
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localParent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParentTracker = false ;

                           public boolean isParentSpecified(){
                               return localParentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getParent(){
                               return localParent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Parent
                               */
                               public void setParent(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localParentTracker = param != null;
                                   
                                            this.localParent=param;
                                    

                               }
                            

                        /**
                        * field for ParentItemsBought
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localParentItemsBought ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParentItemsBoughtTracker = false ;

                           public boolean isParentItemsBoughtSpecified(){
                               return localParentItemsBoughtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getParentItemsBought(){
                               return localParentItemsBought;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ParentItemsBought
                               */
                               public void setParentItemsBought(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localParentItemsBoughtTracker = param != null;
                                   
                                            this.localParentItemsBought=param;
                                    

                               }
                            

                        /**
                        * field for ParentItemsOrdered
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localParentItemsOrdered ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParentItemsOrderedTracker = false ;

                           public boolean isParentItemsOrderedSpecified(){
                               return localParentItemsOrderedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getParentItemsOrdered(){
                               return localParentItemsOrdered;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ParentItemsOrdered
                               */
                               public void setParentItemsOrdered(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localParentItemsOrderedTracker = param != null;
                                   
                                            this.localParentItemsOrdered=param;
                                    

                               }
                            

                        /**
                        * field for Partner
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localPartner ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerTracker = false ;

                           public boolean isPartnerSpecified(){
                               return localPartnerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getPartner(){
                               return localPartner;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Partner
                               */
                               public void setPartner(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localPartnerTracker = param != null;
                                   
                                            this.localPartner=param;
                                    

                               }
                            

                        /**
                        * field for PartnerContribution
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localPartnerContribution ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerContributionTracker = false ;

                           public boolean isPartnerContributionSpecified(){
                               return localPartnerContributionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getPartnerContribution(){
                               return localPartnerContribution;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PartnerContribution
                               */
                               public void setPartnerContribution(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localPartnerContributionTracker = param != null;
                                   
                                            this.localPartnerContribution=param;
                                    

                               }
                            

                        /**
                        * field for PartnerRole
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localPartnerRole ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerRoleTracker = false ;

                           public boolean isPartnerRoleSpecified(){
                               return localPartnerRoleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getPartnerRole(){
                               return localPartnerRole;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PartnerRole
                               */
                               public void setPartnerRole(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localPartnerRoleTracker = param != null;
                                   
                                            this.localPartnerRole=param;
                                    

                               }
                            

                        /**
                        * field for PartnerTeamMember
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localPartnerTeamMember ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerTeamMemberTracker = false ;

                           public boolean isPartnerTeamMemberSpecified(){
                               return localPartnerTeamMemberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getPartnerTeamMember(){
                               return localPartnerTeamMember;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PartnerTeamMember
                               */
                               public void setPartnerTeamMember(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localPartnerTeamMemberTracker = param != null;
                                   
                                            this.localPartnerTeamMember=param;
                                    

                               }
                            

                        /**
                        * field for Pec
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localPec ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPecTracker = false ;

                           public boolean isPecSpecified(){
                               return localPecTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getPec(){
                               return localPec;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Pec
                               */
                               public void setPec(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localPecTracker = param != null;
                                   
                                            this.localPec=param;
                                    

                               }
                            

                        /**
                        * field for Permission
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localPermission ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPermissionTracker = false ;

                           public boolean isPermissionSpecified(){
                               return localPermissionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getPermission(){
                               return localPermission;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Permission
                               */
                               public void setPermission(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localPermissionTracker = param != null;
                                   
                                            this.localPermission=param;
                                    

                               }
                            

                        /**
                        * field for Phone
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localPhone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPhoneTracker = false ;

                           public boolean isPhoneSpecified(){
                               return localPhoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getPhone(){
                               return localPhone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Phone
                               */
                               public void setPhone(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localPhoneTracker = param != null;
                                   
                                            this.localPhone=param;
                                    

                               }
                            

                        /**
                        * field for PhoneticName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localPhoneticName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPhoneticNameTracker = false ;

                           public boolean isPhoneticNameSpecified(){
                               return localPhoneticNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getPhoneticName(){
                               return localPhoneticName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PhoneticName
                               */
                               public void setPhoneticName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localPhoneticNameTracker = param != null;
                                   
                                            this.localPhoneticName=param;
                                    

                               }
                            

                        /**
                        * field for PriceLevel
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localPriceLevel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPriceLevelTracker = false ;

                           public boolean isPriceLevelSpecified(){
                               return localPriceLevelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getPriceLevel(){
                               return localPriceLevel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PriceLevel
                               */
                               public void setPriceLevel(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localPriceLevelTracker = param != null;
                                   
                                            this.localPriceLevel=param;
                                    

                               }
                            

                        /**
                        * field for PricingGroup
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localPricingGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPricingGroupTracker = false ;

                           public boolean isPricingGroupSpecified(){
                               return localPricingGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getPricingGroup(){
                               return localPricingGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PricingGroup
                               */
                               public void setPricingGroup(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localPricingGroupTracker = param != null;
                                   
                                            this.localPricingGroup=param;
                                    

                               }
                            

                        /**
                        * field for PricingItem
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localPricingItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPricingItemTracker = false ;

                           public boolean isPricingItemSpecified(){
                               return localPricingItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getPricingItem(){
                               return localPricingItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PricingItem
                               */
                               public void setPricingItem(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localPricingItemTracker = param != null;
                                   
                                            this.localPricingItem=param;
                                    

                               }
                            

                        /**
                        * field for PrintTransactions
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localPrintTransactions ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPrintTransactionsTracker = false ;

                           public boolean isPrintTransactionsSpecified(){
                               return localPrintTransactionsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getPrintTransactions(){
                               return localPrintTransactions;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PrintTransactions
                               */
                               public void setPrintTransactions(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localPrintTransactionsTracker = param != null;
                                   
                                            this.localPrintTransactions=param;
                                    

                               }
                            

                        /**
                        * field for ProspectDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localProspectDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProspectDateTracker = false ;

                           public boolean isProspectDateSpecified(){
                               return localProspectDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getProspectDate(){
                               return localProspectDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ProspectDate
                               */
                               public void setProspectDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localProspectDateTracker = param != null;
                                   
                                            this.localProspectDate=param;
                                    

                               }
                            

                        /**
                        * field for PstExempt
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localPstExempt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPstExemptTracker = false ;

                           public boolean isPstExemptSpecified(){
                               return localPstExemptTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getPstExempt(){
                               return localPstExempt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PstExempt
                               */
                               public void setPstExempt(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localPstExemptTracker = param != null;
                                   
                                            this.localPstExempt=param;
                                    

                               }
                            

                        /**
                        * field for ReceivablesAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localReceivablesAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReceivablesAccountTracker = false ;

                           public boolean isReceivablesAccountSpecified(){
                               return localReceivablesAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getReceivablesAccount(){
                               return localReceivablesAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReceivablesAccount
                               */
                               public void setReceivablesAccount(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localReceivablesAccountTracker = param != null;
                                   
                                            this.localReceivablesAccount=param;
                                    

                               }
                            

                        /**
                        * field for ReminderDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localReminderDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReminderDateTracker = false ;

                           public boolean isReminderDateSpecified(){
                               return localReminderDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getReminderDate(){
                               return localReminderDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReminderDate
                               */
                               public void setReminderDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localReminderDateTracker = param != null;
                                   
                                            this.localReminderDate=param;
                                    

                               }
                            

                        /**
                        * field for ResaleNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localResaleNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localResaleNumberTracker = false ;

                           public boolean isResaleNumberSpecified(){
                               return localResaleNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getResaleNumber(){
                               return localResaleNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ResaleNumber
                               */
                               public void setResaleNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localResaleNumberTracker = param != null;
                                   
                                            this.localResaleNumber=param;
                                    

                               }
                            

                        /**
                        * field for Role
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localRole ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRoleTracker = false ;

                           public boolean isRoleSpecified(){
                               return localRoleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getRole(){
                               return localRole;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Role
                               */
                               public void setRole(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localRoleTracker = param != null;
                                   
                                            this.localRole=param;
                                    

                               }
                            

                        /**
                        * field for SalesReadiness
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSalesReadiness ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesReadinessTracker = false ;

                           public boolean isSalesReadinessSpecified(){
                               return localSalesReadinessTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSalesReadiness(){
                               return localSalesReadiness;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesReadiness
                               */
                               public void setSalesReadiness(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSalesReadinessTracker = param != null;
                                   
                                            this.localSalesReadiness=param;
                                    

                               }
                            

                        /**
                        * field for SalesRep
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSalesRep ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesRepTracker = false ;

                           public boolean isSalesRepSpecified(){
                               return localSalesRepTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSalesRep(){
                               return localSalesRep;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesRep
                               */
                               public void setSalesRep(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSalesRepTracker = param != null;
                                   
                                            this.localSalesRep=param;
                                    

                               }
                            

                        /**
                        * field for SalesTeamMember
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSalesTeamMember ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesTeamMemberTracker = false ;

                           public boolean isSalesTeamMemberSpecified(){
                               return localSalesTeamMemberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSalesTeamMember(){
                               return localSalesTeamMember;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesTeamMember
                               */
                               public void setSalesTeamMember(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSalesTeamMemberTracker = param != null;
                                   
                                            this.localSalesTeamMember=param;
                                    

                               }
                            

                        /**
                        * field for SalesTeamRole
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSalesTeamRole ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesTeamRoleTracker = false ;

                           public boolean isSalesTeamRoleSpecified(){
                               return localSalesTeamRoleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSalesTeamRole(){
                               return localSalesTeamRole;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesTeamRole
                               */
                               public void setSalesTeamRole(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSalesTeamRoleTracker = param != null;
                                   
                                            this.localSalesTeamRole=param;
                                    

                               }
                            

                        /**
                        * field for Salutation
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localSalutation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalutationTracker = false ;

                           public boolean isSalutationSpecified(){
                               return localSalutationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getSalutation(){
                               return localSalutation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Salutation
                               */
                               public void setSalutation(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localSalutationTracker = param != null;
                                   
                                            this.localSalutation=param;
                                    

                               }
                            

                        /**
                        * field for ShipAddress
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localShipAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipAddressTracker = false ;

                           public boolean isShipAddressSpecified(){
                               return localShipAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getShipAddress(){
                               return localShipAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipAddress
                               */
                               public void setShipAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localShipAddressTracker = param != null;
                                   
                                            this.localShipAddress=param;
                                    

                               }
                            

                        /**
                        * field for ShipComplete
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localShipComplete ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipCompleteTracker = false ;

                           public boolean isShipCompleteSpecified(){
                               return localShipCompleteTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getShipComplete(){
                               return localShipComplete;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipComplete
                               */
                               public void setShipComplete(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localShipCompleteTracker = param != null;
                                   
                                            this.localShipComplete=param;
                                    

                               }
                            

                        /**
                        * field for ShippingItem
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localShippingItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShippingItemTracker = false ;

                           public boolean isShippingItemSpecified(){
                               return localShippingItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getShippingItem(){
                               return localShippingItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShippingItem
                               */
                               public void setShippingItem(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localShippingItemTracker = param != null;
                                   
                                            this.localShippingItem=param;
                                    

                               }
                            

                        /**
                        * field for Stage
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localStage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStageTracker = false ;

                           public boolean isStageSpecified(){
                               return localStageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getStage(){
                               return localStage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Stage
                               */
                               public void setStage(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localStageTracker = param != null;
                                   
                                            this.localStage=param;
                                    

                               }
                            

                        /**
                        * field for StartDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateTracker = false ;

                           public boolean isStartDateSpecified(){
                               return localStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getStartDate(){
                               return localStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDate
                               */
                               public void setStartDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localStartDateTracker = param != null;
                                   
                                            this.localStartDate=param;
                                    

                               }
                            

                        /**
                        * field for State
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localState ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStateTracker = false ;

                           public boolean isStateSpecified(){
                               return localStateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getState(){
                               return localState;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param State
                               */
                               public void setState(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localStateTracker = param != null;
                                   
                                            this.localState=param;
                                    

                               }
                            

                        /**
                        * field for SubsidBought
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSubsidBought ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidBoughtTracker = false ;

                           public boolean isSubsidBoughtSpecified(){
                               return localSubsidBoughtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSubsidBought(){
                               return localSubsidBought;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SubsidBought
                               */
                               public void setSubsidBought(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSubsidBoughtTracker = param != null;
                                   
                                            this.localSubsidBought=param;
                                    

                               }
                            

                        /**
                        * field for Subsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Subsidiary
                               */
                               public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSubsidiaryTracker = param != null;
                                   
                                            this.localSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for Taxable
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localTaxable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxableTracker = false ;

                           public boolean isTaxableSpecified(){
                               return localTaxableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getTaxable(){
                               return localTaxable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Taxable
                               */
                               public void setTaxable(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localTaxableTracker = param != null;
                                   
                                            this.localTaxable=param;
                                    

                               }
                            

                        /**
                        * field for Terms
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localTerms ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTermsTracker = false ;

                           public boolean isTermsSpecified(){
                               return localTermsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getTerms(){
                               return localTerms;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Terms
                               */
                               public void setTerms(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localTermsTracker = param != null;
                                   
                                            this.localTerms=param;
                                    

                               }
                            

                        /**
                        * field for Territory
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localTerritory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTerritoryTracker = false ;

                           public boolean isTerritorySpecified(){
                               return localTerritoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getTerritory(){
                               return localTerritory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Territory
                               */
                               public void setTerritory(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localTerritoryTracker = param != null;
                                   
                                            this.localTerritory=param;
                                    

                               }
                            

                        /**
                        * field for Title
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTitleTracker = false ;

                           public boolean isTitleSpecified(){
                               return localTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getTitle(){
                               return localTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Title
                               */
                               public void setTitle(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localTitleTracker = param != null;
                                   
                                            this.localTitle=param;
                                    

                               }
                            

                        /**
                        * field for UnbilledOrders
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localUnbilledOrders ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUnbilledOrdersTracker = false ;

                           public boolean isUnbilledOrdersSpecified(){
                               return localUnbilledOrdersTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getUnbilledOrders(){
                               return localUnbilledOrders;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UnbilledOrders
                               */
                               public void setUnbilledOrders(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localUnbilledOrdersTracker = param != null;
                                   
                                            this.localUnbilledOrders=param;
                                    

                               }
                            

                        /**
                        * field for Url
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localUrl ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUrlTracker = false ;

                           public boolean isUrlSpecified(){
                               return localUrlTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getUrl(){
                               return localUrl;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Url
                               */
                               public void setUrl(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localUrlTracker = param != null;
                                   
                                            this.localUrl=param;
                                    

                               }
                            

                        /**
                        * field for VatRegNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localVatRegNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVatRegNumberTracker = false ;

                           public boolean isVatRegNumberSpecified(){
                               return localVatRegNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getVatRegNumber(){
                               return localVatRegNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VatRegNumber
                               */
                               public void setVatRegNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localVatRegNumberTracker = param != null;
                                   
                                            this.localVatRegNumber=param;
                                    

                               }
                            

                        /**
                        * field for WebLead
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localWebLead ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWebLeadTracker = false ;

                           public boolean isWebLeadSpecified(){
                               return localWebLeadTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getWebLead(){
                               return localWebLead;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param WebLead
                               */
                               public void setWebLead(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localWebLeadTracker = param != null;
                                   
                                            this.localWebLead=param;
                                    

                               }
                            

                        /**
                        * field for ZipCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localZipCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localZipCodeTracker = false ;

                           public boolean isZipCodeSpecified(){
                               return localZipCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getZipCode(){
                               return localZipCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ZipCode
                               */
                               public void setZipCode(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localZipCodeTracker = param != null;
                                   
                                            this.localZipCode=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":CustomerSearchBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "CustomerSearchBasic",
                           xmlWriter);
                   }

                if (localAccountNumberTracker){
                                            if (localAccountNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accountNumber cannot be null!!");
                                            }
                                           localAccountNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","accountNumber"),
                                               xmlWriter);
                                        } if (localAddressTracker){
                                            if (localAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("address cannot be null!!");
                                            }
                                           localAddress.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address"),
                                               xmlWriter);
                                        } if (localAddresseeTracker){
                                            if (localAddressee==null){
                                                 throw new org.apache.axis2.databinding.ADBException("addressee cannot be null!!");
                                            }
                                           localAddressee.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressee"),
                                               xmlWriter);
                                        } if (localAddressLabelTracker){
                                            if (localAddressLabel==null){
                                                 throw new org.apache.axis2.databinding.ADBException("addressLabel cannot be null!!");
                                            }
                                           localAddressLabel.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressLabel"),
                                               xmlWriter);
                                        } if (localAddressPhoneTracker){
                                            if (localAddressPhone==null){
                                                 throw new org.apache.axis2.databinding.ADBException("addressPhone cannot be null!!");
                                            }
                                           localAddressPhone.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressPhone"),
                                               xmlWriter);
                                        } if (localAttentionTracker){
                                            if (localAttention==null){
                                                 throw new org.apache.axis2.databinding.ADBException("attention cannot be null!!");
                                            }
                                           localAttention.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","attention"),
                                               xmlWriter);
                                        } if (localAvailableOfflineTracker){
                                            if (localAvailableOffline==null){
                                                 throw new org.apache.axis2.databinding.ADBException("availableOffline cannot be null!!");
                                            }
                                           localAvailableOffline.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","availableOffline"),
                                               xmlWriter);
                                        } if (localBalanceTracker){
                                            if (localBalance==null){
                                                 throw new org.apache.axis2.databinding.ADBException("balance cannot be null!!");
                                            }
                                           localBalance.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","balance"),
                                               xmlWriter);
                                        } if (localBillAddressTracker){
                                            if (localBillAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billAddress cannot be null!!");
                                            }
                                           localBillAddress.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billAddress"),
                                               xmlWriter);
                                        } if (localBoughtAmountTracker){
                                            if (localBoughtAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("boughtAmount cannot be null!!");
                                            }
                                           localBoughtAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","boughtAmount"),
                                               xmlWriter);
                                        } if (localBoughtDateTracker){
                                            if (localBoughtDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("boughtDate cannot be null!!");
                                            }
                                           localBoughtDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","boughtDate"),
                                               xmlWriter);
                                        } if (localBuyingReasonTracker){
                                            if (localBuyingReason==null){
                                                 throw new org.apache.axis2.databinding.ADBException("buyingReason cannot be null!!");
                                            }
                                           localBuyingReason.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buyingReason"),
                                               xmlWriter);
                                        } if (localBuyingTimeFrameTracker){
                                            if (localBuyingTimeFrame==null){
                                                 throw new org.apache.axis2.databinding.ADBException("buyingTimeFrame cannot be null!!");
                                            }
                                           localBuyingTimeFrame.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buyingTimeFrame"),
                                               xmlWriter);
                                        } if (localCategoryTracker){
                                            if (localCategory==null){
                                                 throw new org.apache.axis2.databinding.ADBException("category cannot be null!!");
                                            }
                                           localCategory.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","category"),
                                               xmlWriter);
                                        } if (localCcCustomerCodeTracker){
                                            if (localCcCustomerCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ccCustomerCode cannot be null!!");
                                            }
                                           localCcCustomerCode.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ccCustomerCode"),
                                               xmlWriter);
                                        } if (localCcDefaultTracker){
                                            if (localCcDefault==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ccDefault cannot be null!!");
                                            }
                                           localCcDefault.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ccDefault"),
                                               xmlWriter);
                                        } if (localCcExpDateTracker){
                                            if (localCcExpDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ccExpDate cannot be null!!");
                                            }
                                           localCcExpDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ccExpDate"),
                                               xmlWriter);
                                        } if (localCcHolderNameTracker){
                                            if (localCcHolderName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ccHolderName cannot be null!!");
                                            }
                                           localCcHolderName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ccHolderName"),
                                               xmlWriter);
                                        } if (localCcNumberTracker){
                                            if (localCcNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ccNumber cannot be null!!");
                                            }
                                           localCcNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ccNumber"),
                                               xmlWriter);
                                        } if (localCcStateTracker){
                                            if (localCcState==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ccState cannot be null!!");
                                            }
                                           localCcState.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ccState"),
                                               xmlWriter);
                                        } if (localCcStateFromTracker){
                                            if (localCcStateFrom==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ccStateFrom cannot be null!!");
                                            }
                                           localCcStateFrom.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ccStateFrom"),
                                               xmlWriter);
                                        } if (localCcTypeTracker){
                                            if (localCcType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ccType cannot be null!!");
                                            }
                                           localCcType.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ccType"),
                                               xmlWriter);
                                        } if (localCityTracker){
                                            if (localCity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("city cannot be null!!");
                                            }
                                           localCity.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","city"),
                                               xmlWriter);
                                        } if (localClassBoughtTracker){
                                            if (localClassBought==null){
                                                 throw new org.apache.axis2.databinding.ADBException("classBought cannot be null!!");
                                            }
                                           localClassBought.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","classBought"),
                                               xmlWriter);
                                        } if (localCommentsTracker){
                                            if (localComments==null){
                                                 throw new org.apache.axis2.databinding.ADBException("comments cannot be null!!");
                                            }
                                           localComments.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","comments"),
                                               xmlWriter);
                                        } if (localCompanyNameTracker){
                                            if (localCompanyName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("companyName cannot be null!!");
                                            }
                                           localCompanyName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","companyName"),
                                               xmlWriter);
                                        } if (localConsolBalanceTracker){
                                            if (localConsolBalance==null){
                                                 throw new org.apache.axis2.databinding.ADBException("consolBalance cannot be null!!");
                                            }
                                           localConsolBalance.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","consolBalance"),
                                               xmlWriter);
                                        } if (localConsolDaysOverdueTracker){
                                            if (localConsolDaysOverdue==null){
                                                 throw new org.apache.axis2.databinding.ADBException("consolDaysOverdue cannot be null!!");
                                            }
                                           localConsolDaysOverdue.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","consolDaysOverdue"),
                                               xmlWriter);
                                        } if (localConsolDepositBalanceTracker){
                                            if (localConsolDepositBalance==null){
                                                 throw new org.apache.axis2.databinding.ADBException("consolDepositBalance cannot be null!!");
                                            }
                                           localConsolDepositBalance.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","consolDepositBalance"),
                                               xmlWriter);
                                        } if (localConsolOverdueBalanceTracker){
                                            if (localConsolOverdueBalance==null){
                                                 throw new org.apache.axis2.databinding.ADBException("consolOverdueBalance cannot be null!!");
                                            }
                                           localConsolOverdueBalance.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","consolOverdueBalance"),
                                               xmlWriter);
                                        } if (localConsolUnbilledOrdersTracker){
                                            if (localConsolUnbilledOrders==null){
                                                 throw new org.apache.axis2.databinding.ADBException("consolUnbilledOrders cannot be null!!");
                                            }
                                           localConsolUnbilledOrders.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","consolUnbilledOrders"),
                                               xmlWriter);
                                        } if (localContactTracker){
                                            if (localContact==null){
                                                 throw new org.apache.axis2.databinding.ADBException("contact cannot be null!!");
                                            }
                                           localContact.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","contact"),
                                               xmlWriter);
                                        } if (localContributionTracker){
                                            if (localContribution==null){
                                                 throw new org.apache.axis2.databinding.ADBException("contribution cannot be null!!");
                                            }
                                           localContribution.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","contribution"),
                                               xmlWriter);
                                        } if (localConversionDateTracker){
                                            if (localConversionDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("conversionDate cannot be null!!");
                                            }
                                           localConversionDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","conversionDate"),
                                               xmlWriter);
                                        } if (localCountryTracker){
                                            if (localCountry==null){
                                                 throw new org.apache.axis2.databinding.ADBException("country cannot be null!!");
                                            }
                                           localCountry.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","country"),
                                               xmlWriter);
                                        } if (localCountyTracker){
                                            if (localCounty==null){
                                                 throw new org.apache.axis2.databinding.ADBException("county cannot be null!!");
                                            }
                                           localCounty.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","county"),
                                               xmlWriter);
                                        } if (localCreditHoldTracker){
                                            if (localCreditHold==null){
                                                 throw new org.apache.axis2.databinding.ADBException("creditHold cannot be null!!");
                                            }
                                           localCreditHold.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","creditHold"),
                                               xmlWriter);
                                        } if (localCreditHoldOverrideTracker){
                                            if (localCreditHoldOverride==null){
                                                 throw new org.apache.axis2.databinding.ADBException("creditHoldOverride cannot be null!!");
                                            }
                                           localCreditHoldOverride.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","creditHoldOverride"),
                                               xmlWriter);
                                        } if (localCreditLimitTracker){
                                            if (localCreditLimit==null){
                                                 throw new org.apache.axis2.databinding.ADBException("creditLimit cannot be null!!");
                                            }
                                           localCreditLimit.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","creditLimit"),
                                               xmlWriter);
                                        } if (localCurrencyTracker){
                                            if (localCurrency==null){
                                                 throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                            }
                                           localCurrency.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency"),
                                               xmlWriter);
                                        } if (localCustStageTracker){
                                            if (localCustStage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("custStage cannot be null!!");
                                            }
                                           localCustStage.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","custStage"),
                                               xmlWriter);
                                        } if (localCustStatusTracker){
                                            if (localCustStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("custStatus cannot be null!!");
                                            }
                                           localCustStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","custStatus"),
                                               xmlWriter);
                                        } if (localDateClosedTracker){
                                            if (localDateClosed==null){
                                                 throw new org.apache.axis2.databinding.ADBException("dateClosed cannot be null!!");
                                            }
                                           localDateClosed.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateClosed"),
                                               xmlWriter);
                                        } if (localDateCreatedTracker){
                                            if (localDateCreated==null){
                                                 throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                            }
                                           localDateCreated.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated"),
                                               xmlWriter);
                                        } if (localDaysOverdueTracker){
                                            if (localDaysOverdue==null){
                                                 throw new org.apache.axis2.databinding.ADBException("daysOverdue cannot be null!!");
                                            }
                                           localDaysOverdue.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","daysOverdue"),
                                               xmlWriter);
                                        } if (localDefaultOrderPriorityTracker){
                                            if (localDefaultOrderPriority==null){
                                                 throw new org.apache.axis2.databinding.ADBException("defaultOrderPriority cannot be null!!");
                                            }
                                           localDefaultOrderPriority.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","defaultOrderPriority"),
                                               xmlWriter);
                                        } if (localDepositBalanceTracker){
                                            if (localDepositBalance==null){
                                                 throw new org.apache.axis2.databinding.ADBException("depositBalance cannot be null!!");
                                            }
                                           localDepositBalance.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","depositBalance"),
                                               xmlWriter);
                                        } if (localDeptBoughtTracker){
                                            if (localDeptBought==null){
                                                 throw new org.apache.axis2.databinding.ADBException("deptBought cannot be null!!");
                                            }
                                           localDeptBought.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","deptBought"),
                                               xmlWriter);
                                        } if (localDrAccountTracker){
                                            if (localDrAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("drAccount cannot be null!!");
                                            }
                                           localDrAccount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","drAccount"),
                                               xmlWriter);
                                        } if (localEmailTracker){
                                            if (localEmail==null){
                                                 throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                            }
                                           localEmail.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","email"),
                                               xmlWriter);
                                        } if (localEmailPreferenceTracker){
                                            if (localEmailPreference==null){
                                                 throw new org.apache.axis2.databinding.ADBException("emailPreference cannot be null!!");
                                            }
                                           localEmailPreference.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","emailPreference"),
                                               xmlWriter);
                                        } if (localEmailTransactionsTracker){
                                            if (localEmailTransactions==null){
                                                 throw new org.apache.axis2.databinding.ADBException("emailTransactions cannot be null!!");
                                            }
                                           localEmailTransactions.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","emailTransactions"),
                                               xmlWriter);
                                        } if (localEndDateTracker){
                                            if (localEndDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                            }
                                           localEndDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endDate"),
                                               xmlWriter);
                                        } if (localEntityIdTracker){
                                            if (localEntityId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("entityId cannot be null!!");
                                            }
                                           localEntityId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entityId"),
                                               xmlWriter);
                                        } if (localEntityStatusTracker){
                                            if (localEntityStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("entityStatus cannot be null!!");
                                            }
                                           localEntityStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entityStatus"),
                                               xmlWriter);
                                        } if (localEstimatedBudgetTracker){
                                            if (localEstimatedBudget==null){
                                                 throw new org.apache.axis2.databinding.ADBException("estimatedBudget cannot be null!!");
                                            }
                                           localEstimatedBudget.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedBudget"),
                                               xmlWriter);
                                        } if (localExplicitConversionTracker){
                                            if (localExplicitConversion==null){
                                                 throw new org.apache.axis2.databinding.ADBException("explicitConversion cannot be null!!");
                                            }
                                           localExplicitConversion.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","explicitConversion"),
                                               xmlWriter);
                                        } if (localExternalIdTracker){
                                            if (localExternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                            }
                                           localExternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId"),
                                               xmlWriter);
                                        } if (localExternalIdStringTracker){
                                            if (localExternalIdString==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                            }
                                           localExternalIdString.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString"),
                                               xmlWriter);
                                        } if (localFaxTracker){
                                            if (localFax==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fax cannot be null!!");
                                            }
                                           localFax.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fax"),
                                               xmlWriter);
                                        } if (localFaxTransactionsTracker){
                                            if (localFaxTransactions==null){
                                                 throw new org.apache.axis2.databinding.ADBException("faxTransactions cannot be null!!");
                                            }
                                           localFaxTransactions.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","faxTransactions"),
                                               xmlWriter);
                                        } if (localFirstNameTracker){
                                            if (localFirstName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("firstName cannot be null!!");
                                            }
                                           localFirstName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","firstName"),
                                               xmlWriter);
                                        } if (localFirstOrderDateTracker){
                                            if (localFirstOrderDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("firstOrderDate cannot be null!!");
                                            }
                                           localFirstOrderDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","firstOrderDate"),
                                               xmlWriter);
                                        } if (localFirstSaleDateTracker){
                                            if (localFirstSaleDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("firstSaleDate cannot be null!!");
                                            }
                                           localFirstSaleDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","firstSaleDate"),
                                               xmlWriter);
                                        } if (localFxAccountTracker){
                                            if (localFxAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fxAccount cannot be null!!");
                                            }
                                           localFxAccount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxAccount"),
                                               xmlWriter);
                                        } if (localFxBalanceTracker){
                                            if (localFxBalance==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fxBalance cannot be null!!");
                                            }
                                           localFxBalance.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxBalance"),
                                               xmlWriter);
                                        } if (localFxConsolBalanceTracker){
                                            if (localFxConsolBalance==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fxConsolBalance cannot be null!!");
                                            }
                                           localFxConsolBalance.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxConsolBalance"),
                                               xmlWriter);
                                        } if (localFxConsolUnbilledOrdersTracker){
                                            if (localFxConsolUnbilledOrders==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fxConsolUnbilledOrders cannot be null!!");
                                            }
                                           localFxConsolUnbilledOrders.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxConsolUnbilledOrders"),
                                               xmlWriter);
                                        } if (localFxUnbilledOrdersTracker){
                                            if (localFxUnbilledOrders==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fxUnbilledOrders cannot be null!!");
                                            }
                                           localFxUnbilledOrders.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxUnbilledOrders"),
                                               xmlWriter);
                                        } if (localGiveAccessTracker){
                                            if (localGiveAccess==null){
                                                 throw new org.apache.axis2.databinding.ADBException("giveAccess cannot be null!!");
                                            }
                                           localGiveAccess.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","giveAccess"),
                                               xmlWriter);
                                        } if (localGlobalSubscriptionStatusTracker){
                                            if (localGlobalSubscriptionStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("globalSubscriptionStatus cannot be null!!");
                                            }
                                           localGlobalSubscriptionStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","globalSubscriptionStatus"),
                                               xmlWriter);
                                        } if (localGroupTracker){
                                            if (localGroup==null){
                                                 throw new org.apache.axis2.databinding.ADBException("group cannot be null!!");
                                            }
                                           localGroup.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","group"),
                                               xmlWriter);
                                        } if (localGroupPricingLevelTracker){
                                            if (localGroupPricingLevel==null){
                                                 throw new org.apache.axis2.databinding.ADBException("groupPricingLevel cannot be null!!");
                                            }
                                           localGroupPricingLevel.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","groupPricingLevel"),
                                               xmlWriter);
                                        } if (localHasDuplicatesTracker){
                                            if (localHasDuplicates==null){
                                                 throw new org.apache.axis2.databinding.ADBException("hasDuplicates cannot be null!!");
                                            }
                                           localHasDuplicates.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","hasDuplicates"),
                                               xmlWriter);
                                        } if (localImageTracker){
                                            if (localImage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("image cannot be null!!");
                                            }
                                           localImage.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","image"),
                                               xmlWriter);
                                        } if (localInternalIdTracker){
                                            if (localInternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                            }
                                           localInternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                               xmlWriter);
                                        } if (localInternalIdNumberTracker){
                                            if (localInternalIdNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                            }
                                           localInternalIdNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber"),
                                               xmlWriter);
                                        } if (localIsBudgetApprovedTracker){
                                            if (localIsBudgetApproved==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isBudgetApproved cannot be null!!");
                                            }
                                           localIsBudgetApproved.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isBudgetApproved"),
                                               xmlWriter);
                                        } if (localIsDefaultBillingTracker){
                                            if (localIsDefaultBilling==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isDefaultBilling cannot be null!!");
                                            }
                                           localIsDefaultBilling.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isDefaultBilling"),
                                               xmlWriter);
                                        } if (localIsDefaultShippingTracker){
                                            if (localIsDefaultShipping==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isDefaultShipping cannot be null!!");
                                            }
                                           localIsDefaultShipping.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isDefaultShipping"),
                                               xmlWriter);
                                        } if (localIsInactiveTracker){
                                            if (localIsInactive==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                            }
                                           localIsInactive.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isInactive"),
                                               xmlWriter);
                                        } if (localIsPersonTracker){
                                            if (localIsPerson==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isPerson cannot be null!!");
                                            }
                                           localIsPerson.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isPerson"),
                                               xmlWriter);
                                        } if (localIsReportedLeadTracker){
                                            if (localIsReportedLead==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isReportedLead cannot be null!!");
                                            }
                                           localIsReportedLead.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isReportedLead"),
                                               xmlWriter);
                                        } if (localIsShipAddressTracker){
                                            if (localIsShipAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isShipAddress cannot be null!!");
                                            }
                                           localIsShipAddress.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isShipAddress"),
                                               xmlWriter);
                                        } if (localItemPricingLevelTracker){
                                            if (localItemPricingLevel==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemPricingLevel cannot be null!!");
                                            }
                                           localItemPricingLevel.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","itemPricingLevel"),
                                               xmlWriter);
                                        } if (localItemPricingUnitPriceTracker){
                                            if (localItemPricingUnitPrice==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemPricingUnitPrice cannot be null!!");
                                            }
                                           localItemPricingUnitPrice.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","itemPricingUnitPrice"),
                                               xmlWriter);
                                        } if (localItemsBoughtTracker){
                                            if (localItemsBought==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemsBought cannot be null!!");
                                            }
                                           localItemsBought.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","itemsBought"),
                                               xmlWriter);
                                        } if (localItemsOrderedTracker){
                                            if (localItemsOrdered==null){
                                                 throw new org.apache.axis2.databinding.ADBException("itemsOrdered cannot be null!!");
                                            }
                                           localItemsOrdered.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","itemsOrdered"),
                                               xmlWriter);
                                        } if (localLanguageTracker){
                                            if (localLanguage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("language cannot be null!!");
                                            }
                                           localLanguage.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","language"),
                                               xmlWriter);
                                        } if (localLastModifiedDateTracker){
                                            if (localLastModifiedDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                            }
                                           localLastModifiedDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate"),
                                               xmlWriter);
                                        } if (localLastNameTracker){
                                            if (localLastName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lastName cannot be null!!");
                                            }
                                           localLastName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastName"),
                                               xmlWriter);
                                        } if (localLastOrderDateTracker){
                                            if (localLastOrderDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lastOrderDate cannot be null!!");
                                            }
                                           localLastOrderDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastOrderDate"),
                                               xmlWriter);
                                        } if (localLastSaleDateTracker){
                                            if (localLastSaleDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lastSaleDate cannot be null!!");
                                            }
                                           localLastSaleDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastSaleDate"),
                                               xmlWriter);
                                        } if (localLeadDateTracker){
                                            if (localLeadDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("leadDate cannot be null!!");
                                            }
                                           localLeadDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","leadDate"),
                                               xmlWriter);
                                        } if (localLeadSourceTracker){
                                            if (localLeadSource==null){
                                                 throw new org.apache.axis2.databinding.ADBException("leadSource cannot be null!!");
                                            }
                                           localLeadSource.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","leadSource"),
                                               xmlWriter);
                                        } if (localLevelTracker){
                                            if (localLevel==null){
                                                 throw new org.apache.axis2.databinding.ADBException("level cannot be null!!");
                                            }
                                           localLevel.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","level"),
                                               xmlWriter);
                                        } if (localLocationBoughtTracker){
                                            if (localLocationBought==null){
                                                 throw new org.apache.axis2.databinding.ADBException("locationBought cannot be null!!");
                                            }
                                           localLocationBought.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","locationBought"),
                                               xmlWriter);
                                        } if (localManualCreditHoldTracker){
                                            if (localManualCreditHold==null){
                                                 throw new org.apache.axis2.databinding.ADBException("manualCreditHold cannot be null!!");
                                            }
                                           localManualCreditHold.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","manualCreditHold"),
                                               xmlWriter);
                                        } if (localMerchantAccountTracker){
                                            if (localMerchantAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("merchantAccount cannot be null!!");
                                            }
                                           localMerchantAccount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","merchantAccount"),
                                               xmlWriter);
                                        } if (localMiddleNameTracker){
                                            if (localMiddleName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("middleName cannot be null!!");
                                            }
                                           localMiddleName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","middleName"),
                                               xmlWriter);
                                        } if (localMonthlyClosingTracker){
                                            if (localMonthlyClosing==null){
                                                 throw new org.apache.axis2.databinding.ADBException("monthlyClosing cannot be null!!");
                                            }
                                           localMonthlyClosing.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","monthlyClosing"),
                                               xmlWriter);
                                        } if (localOnCreditHoldTracker){
                                            if (localOnCreditHold==null){
                                                 throw new org.apache.axis2.databinding.ADBException("onCreditHold cannot be null!!");
                                            }
                                           localOnCreditHold.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","onCreditHold"),
                                               xmlWriter);
                                        } if (localOrderedAmountTracker){
                                            if (localOrderedAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("orderedAmount cannot be null!!");
                                            }
                                           localOrderedAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","orderedAmount"),
                                               xmlWriter);
                                        } if (localOrderedDateTracker){
                                            if (localOrderedDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("orderedDate cannot be null!!");
                                            }
                                           localOrderedDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","orderedDate"),
                                               xmlWriter);
                                        } if (localOtherRelationshipsTracker){
                                            if (localOtherRelationships==null){
                                                 throw new org.apache.axis2.databinding.ADBException("otherRelationships cannot be null!!");
                                            }
                                           localOtherRelationships.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","otherRelationships"),
                                               xmlWriter);
                                        } if (localOverdueBalanceTracker){
                                            if (localOverdueBalance==null){
                                                 throw new org.apache.axis2.databinding.ADBException("overdueBalance cannot be null!!");
                                            }
                                           localOverdueBalance.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","overdueBalance"),
                                               xmlWriter);
                                        } if (localParentTracker){
                                            if (localParent==null){
                                                 throw new org.apache.axis2.databinding.ADBException("parent cannot be null!!");
                                            }
                                           localParent.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","parent"),
                                               xmlWriter);
                                        } if (localParentItemsBoughtTracker){
                                            if (localParentItemsBought==null){
                                                 throw new org.apache.axis2.databinding.ADBException("parentItemsBought cannot be null!!");
                                            }
                                           localParentItemsBought.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","parentItemsBought"),
                                               xmlWriter);
                                        } if (localParentItemsOrderedTracker){
                                            if (localParentItemsOrdered==null){
                                                 throw new org.apache.axis2.databinding.ADBException("parentItemsOrdered cannot be null!!");
                                            }
                                           localParentItemsOrdered.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","parentItemsOrdered"),
                                               xmlWriter);
                                        } if (localPartnerTracker){
                                            if (localPartner==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partner cannot be null!!");
                                            }
                                           localPartner.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partner"),
                                               xmlWriter);
                                        } if (localPartnerContributionTracker){
                                            if (localPartnerContribution==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partnerContribution cannot be null!!");
                                            }
                                           localPartnerContribution.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerContribution"),
                                               xmlWriter);
                                        } if (localPartnerRoleTracker){
                                            if (localPartnerRole==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partnerRole cannot be null!!");
                                            }
                                           localPartnerRole.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerRole"),
                                               xmlWriter);
                                        } if (localPartnerTeamMemberTracker){
                                            if (localPartnerTeamMember==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partnerTeamMember cannot be null!!");
                                            }
                                           localPartnerTeamMember.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerTeamMember"),
                                               xmlWriter);
                                        } if (localPecTracker){
                                            if (localPec==null){
                                                 throw new org.apache.axis2.databinding.ADBException("pec cannot be null!!");
                                            }
                                           localPec.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","pec"),
                                               xmlWriter);
                                        } if (localPermissionTracker){
                                            if (localPermission==null){
                                                 throw new org.apache.axis2.databinding.ADBException("permission cannot be null!!");
                                            }
                                           localPermission.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","permission"),
                                               xmlWriter);
                                        } if (localPhoneTracker){
                                            if (localPhone==null){
                                                 throw new org.apache.axis2.databinding.ADBException("phone cannot be null!!");
                                            }
                                           localPhone.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phone"),
                                               xmlWriter);
                                        } if (localPhoneticNameTracker){
                                            if (localPhoneticName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("phoneticName cannot be null!!");
                                            }
                                           localPhoneticName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phoneticName"),
                                               xmlWriter);
                                        } if (localPriceLevelTracker){
                                            if (localPriceLevel==null){
                                                 throw new org.apache.axis2.databinding.ADBException("priceLevel cannot be null!!");
                                            }
                                           localPriceLevel.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","priceLevel"),
                                               xmlWriter);
                                        } if (localPricingGroupTracker){
                                            if (localPricingGroup==null){
                                                 throw new org.apache.axis2.databinding.ADBException("pricingGroup cannot be null!!");
                                            }
                                           localPricingGroup.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","pricingGroup"),
                                               xmlWriter);
                                        } if (localPricingItemTracker){
                                            if (localPricingItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("pricingItem cannot be null!!");
                                            }
                                           localPricingItem.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","pricingItem"),
                                               xmlWriter);
                                        } if (localPrintTransactionsTracker){
                                            if (localPrintTransactions==null){
                                                 throw new org.apache.axis2.databinding.ADBException("printTransactions cannot be null!!");
                                            }
                                           localPrintTransactions.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","printTransactions"),
                                               xmlWriter);
                                        } if (localProspectDateTracker){
                                            if (localProspectDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("prospectDate cannot be null!!");
                                            }
                                           localProspectDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","prospectDate"),
                                               xmlWriter);
                                        } if (localPstExemptTracker){
                                            if (localPstExempt==null){
                                                 throw new org.apache.axis2.databinding.ADBException("pstExempt cannot be null!!");
                                            }
                                           localPstExempt.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","pstExempt"),
                                               xmlWriter);
                                        } if (localReceivablesAccountTracker){
                                            if (localReceivablesAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("receivablesAccount cannot be null!!");
                                            }
                                           localReceivablesAccount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","receivablesAccount"),
                                               xmlWriter);
                                        } if (localReminderDateTracker){
                                            if (localReminderDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("reminderDate cannot be null!!");
                                            }
                                           localReminderDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","reminderDate"),
                                               xmlWriter);
                                        } if (localResaleNumberTracker){
                                            if (localResaleNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("resaleNumber cannot be null!!");
                                            }
                                           localResaleNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","resaleNumber"),
                                               xmlWriter);
                                        } if (localRoleTracker){
                                            if (localRole==null){
                                                 throw new org.apache.axis2.databinding.ADBException("role cannot be null!!");
                                            }
                                           localRole.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","role"),
                                               xmlWriter);
                                        } if (localSalesReadinessTracker){
                                            if (localSalesReadiness==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesReadiness cannot be null!!");
                                            }
                                           localSalesReadiness.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesReadiness"),
                                               xmlWriter);
                                        } if (localSalesRepTracker){
                                            if (localSalesRep==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesRep cannot be null!!");
                                            }
                                           localSalesRep.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesRep"),
                                               xmlWriter);
                                        } if (localSalesTeamMemberTracker){
                                            if (localSalesTeamMember==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesTeamMember cannot be null!!");
                                            }
                                           localSalesTeamMember.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesTeamMember"),
                                               xmlWriter);
                                        } if (localSalesTeamRoleTracker){
                                            if (localSalesTeamRole==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesTeamRole cannot be null!!");
                                            }
                                           localSalesTeamRole.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesTeamRole"),
                                               xmlWriter);
                                        } if (localSalutationTracker){
                                            if (localSalutation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salutation cannot be null!!");
                                            }
                                           localSalutation.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salutation"),
                                               xmlWriter);
                                        } if (localShipAddressTracker){
                                            if (localShipAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shipAddress cannot be null!!");
                                            }
                                           localShipAddress.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","shipAddress"),
                                               xmlWriter);
                                        } if (localShipCompleteTracker){
                                            if (localShipComplete==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shipComplete cannot be null!!");
                                            }
                                           localShipComplete.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","shipComplete"),
                                               xmlWriter);
                                        } if (localShippingItemTracker){
                                            if (localShippingItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shippingItem cannot be null!!");
                                            }
                                           localShippingItem.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","shippingItem"),
                                               xmlWriter);
                                        } if (localStageTracker){
                                            if (localStage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("stage cannot be null!!");
                                            }
                                           localStage.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","stage"),
                                               xmlWriter);
                                        } if (localStartDateTracker){
                                            if (localStartDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                            }
                                           localStartDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDate"),
                                               xmlWriter);
                                        } if (localStateTracker){
                                            if (localState==null){
                                                 throw new org.apache.axis2.databinding.ADBException("state cannot be null!!");
                                            }
                                           localState.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","state"),
                                               xmlWriter);
                                        } if (localSubsidBoughtTracker){
                                            if (localSubsidBought==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidBought cannot be null!!");
                                            }
                                           localSubsidBought.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidBought"),
                                               xmlWriter);
                                        } if (localSubsidiaryTracker){
                                            if (localSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                            }
                                           localSubsidiary.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary"),
                                               xmlWriter);
                                        } if (localTaxableTracker){
                                            if (localTaxable==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxable cannot be null!!");
                                            }
                                           localTaxable.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","taxable"),
                                               xmlWriter);
                                        } if (localTermsTracker){
                                            if (localTerms==null){
                                                 throw new org.apache.axis2.databinding.ADBException("terms cannot be null!!");
                                            }
                                           localTerms.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","terms"),
                                               xmlWriter);
                                        } if (localTerritoryTracker){
                                            if (localTerritory==null){
                                                 throw new org.apache.axis2.databinding.ADBException("territory cannot be null!!");
                                            }
                                           localTerritory.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","territory"),
                                               xmlWriter);
                                        } if (localTitleTracker){
                                            if (localTitle==null){
                                                 throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                            }
                                           localTitle.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","title"),
                                               xmlWriter);
                                        } if (localUnbilledOrdersTracker){
                                            if (localUnbilledOrders==null){
                                                 throw new org.apache.axis2.databinding.ADBException("unbilledOrders cannot be null!!");
                                            }
                                           localUnbilledOrders.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","unbilledOrders"),
                                               xmlWriter);
                                        } if (localUrlTracker){
                                            if (localUrl==null){
                                                 throw new org.apache.axis2.databinding.ADBException("url cannot be null!!");
                                            }
                                           localUrl.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","url"),
                                               xmlWriter);
                                        } if (localVatRegNumberTracker){
                                            if (localVatRegNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vatRegNumber cannot be null!!");
                                            }
                                           localVatRegNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","vatRegNumber"),
                                               xmlWriter);
                                        } if (localWebLeadTracker){
                                            if (localWebLead==null){
                                                 throw new org.apache.axis2.databinding.ADBException("webLead cannot be null!!");
                                            }
                                           localWebLead.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","webLead"),
                                               xmlWriter);
                                        } if (localZipCodeTracker){
                                            if (localZipCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("zipCode cannot be null!!");
                                            }
                                           localZipCode.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","zipCode"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","CustomerSearchBasic"));
                 if (localAccountNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "accountNumber"));
                            
                            
                                    if (localAccountNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("accountNumber cannot be null!!");
                                    }
                                    elementList.add(localAccountNumber);
                                } if (localAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "address"));
                            
                            
                                    if (localAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("address cannot be null!!");
                                    }
                                    elementList.add(localAddress);
                                } if (localAddresseeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "addressee"));
                            
                            
                                    if (localAddressee==null){
                                         throw new org.apache.axis2.databinding.ADBException("addressee cannot be null!!");
                                    }
                                    elementList.add(localAddressee);
                                } if (localAddressLabelTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "addressLabel"));
                            
                            
                                    if (localAddressLabel==null){
                                         throw new org.apache.axis2.databinding.ADBException("addressLabel cannot be null!!");
                                    }
                                    elementList.add(localAddressLabel);
                                } if (localAddressPhoneTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "addressPhone"));
                            
                            
                                    if (localAddressPhone==null){
                                         throw new org.apache.axis2.databinding.ADBException("addressPhone cannot be null!!");
                                    }
                                    elementList.add(localAddressPhone);
                                } if (localAttentionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "attention"));
                            
                            
                                    if (localAttention==null){
                                         throw new org.apache.axis2.databinding.ADBException("attention cannot be null!!");
                                    }
                                    elementList.add(localAttention);
                                } if (localAvailableOfflineTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "availableOffline"));
                            
                            
                                    if (localAvailableOffline==null){
                                         throw new org.apache.axis2.databinding.ADBException("availableOffline cannot be null!!");
                                    }
                                    elementList.add(localAvailableOffline);
                                } if (localBalanceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "balance"));
                            
                            
                                    if (localBalance==null){
                                         throw new org.apache.axis2.databinding.ADBException("balance cannot be null!!");
                                    }
                                    elementList.add(localBalance);
                                } if (localBillAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "billAddress"));
                            
                            
                                    if (localBillAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("billAddress cannot be null!!");
                                    }
                                    elementList.add(localBillAddress);
                                } if (localBoughtAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "boughtAmount"));
                            
                            
                                    if (localBoughtAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("boughtAmount cannot be null!!");
                                    }
                                    elementList.add(localBoughtAmount);
                                } if (localBoughtDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "boughtDate"));
                            
                            
                                    if (localBoughtDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("boughtDate cannot be null!!");
                                    }
                                    elementList.add(localBoughtDate);
                                } if (localBuyingReasonTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "buyingReason"));
                            
                            
                                    if (localBuyingReason==null){
                                         throw new org.apache.axis2.databinding.ADBException("buyingReason cannot be null!!");
                                    }
                                    elementList.add(localBuyingReason);
                                } if (localBuyingTimeFrameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "buyingTimeFrame"));
                            
                            
                                    if (localBuyingTimeFrame==null){
                                         throw new org.apache.axis2.databinding.ADBException("buyingTimeFrame cannot be null!!");
                                    }
                                    elementList.add(localBuyingTimeFrame);
                                } if (localCategoryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "category"));
                            
                            
                                    if (localCategory==null){
                                         throw new org.apache.axis2.databinding.ADBException("category cannot be null!!");
                                    }
                                    elementList.add(localCategory);
                                } if (localCcCustomerCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "ccCustomerCode"));
                            
                            
                                    if (localCcCustomerCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("ccCustomerCode cannot be null!!");
                                    }
                                    elementList.add(localCcCustomerCode);
                                } if (localCcDefaultTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "ccDefault"));
                            
                            
                                    if (localCcDefault==null){
                                         throw new org.apache.axis2.databinding.ADBException("ccDefault cannot be null!!");
                                    }
                                    elementList.add(localCcDefault);
                                } if (localCcExpDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "ccExpDate"));
                            
                            
                                    if (localCcExpDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("ccExpDate cannot be null!!");
                                    }
                                    elementList.add(localCcExpDate);
                                } if (localCcHolderNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "ccHolderName"));
                            
                            
                                    if (localCcHolderName==null){
                                         throw new org.apache.axis2.databinding.ADBException("ccHolderName cannot be null!!");
                                    }
                                    elementList.add(localCcHolderName);
                                } if (localCcNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "ccNumber"));
                            
                            
                                    if (localCcNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("ccNumber cannot be null!!");
                                    }
                                    elementList.add(localCcNumber);
                                } if (localCcStateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "ccState"));
                            
                            
                                    if (localCcState==null){
                                         throw new org.apache.axis2.databinding.ADBException("ccState cannot be null!!");
                                    }
                                    elementList.add(localCcState);
                                } if (localCcStateFromTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "ccStateFrom"));
                            
                            
                                    if (localCcStateFrom==null){
                                         throw new org.apache.axis2.databinding.ADBException("ccStateFrom cannot be null!!");
                                    }
                                    elementList.add(localCcStateFrom);
                                } if (localCcTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "ccType"));
                            
                            
                                    if (localCcType==null){
                                         throw new org.apache.axis2.databinding.ADBException("ccType cannot be null!!");
                                    }
                                    elementList.add(localCcType);
                                } if (localCityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "city"));
                            
                            
                                    if (localCity==null){
                                         throw new org.apache.axis2.databinding.ADBException("city cannot be null!!");
                                    }
                                    elementList.add(localCity);
                                } if (localClassBoughtTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "classBought"));
                            
                            
                                    if (localClassBought==null){
                                         throw new org.apache.axis2.databinding.ADBException("classBought cannot be null!!");
                                    }
                                    elementList.add(localClassBought);
                                } if (localCommentsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "comments"));
                            
                            
                                    if (localComments==null){
                                         throw new org.apache.axis2.databinding.ADBException("comments cannot be null!!");
                                    }
                                    elementList.add(localComments);
                                } if (localCompanyNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "companyName"));
                            
                            
                                    if (localCompanyName==null){
                                         throw new org.apache.axis2.databinding.ADBException("companyName cannot be null!!");
                                    }
                                    elementList.add(localCompanyName);
                                } if (localConsolBalanceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "consolBalance"));
                            
                            
                                    if (localConsolBalance==null){
                                         throw new org.apache.axis2.databinding.ADBException("consolBalance cannot be null!!");
                                    }
                                    elementList.add(localConsolBalance);
                                } if (localConsolDaysOverdueTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "consolDaysOverdue"));
                            
                            
                                    if (localConsolDaysOverdue==null){
                                         throw new org.apache.axis2.databinding.ADBException("consolDaysOverdue cannot be null!!");
                                    }
                                    elementList.add(localConsolDaysOverdue);
                                } if (localConsolDepositBalanceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "consolDepositBalance"));
                            
                            
                                    if (localConsolDepositBalance==null){
                                         throw new org.apache.axis2.databinding.ADBException("consolDepositBalance cannot be null!!");
                                    }
                                    elementList.add(localConsolDepositBalance);
                                } if (localConsolOverdueBalanceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "consolOverdueBalance"));
                            
                            
                                    if (localConsolOverdueBalance==null){
                                         throw new org.apache.axis2.databinding.ADBException("consolOverdueBalance cannot be null!!");
                                    }
                                    elementList.add(localConsolOverdueBalance);
                                } if (localConsolUnbilledOrdersTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "consolUnbilledOrders"));
                            
                            
                                    if (localConsolUnbilledOrders==null){
                                         throw new org.apache.axis2.databinding.ADBException("consolUnbilledOrders cannot be null!!");
                                    }
                                    elementList.add(localConsolUnbilledOrders);
                                } if (localContactTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "contact"));
                            
                            
                                    if (localContact==null){
                                         throw new org.apache.axis2.databinding.ADBException("contact cannot be null!!");
                                    }
                                    elementList.add(localContact);
                                } if (localContributionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "contribution"));
                            
                            
                                    if (localContribution==null){
                                         throw new org.apache.axis2.databinding.ADBException("contribution cannot be null!!");
                                    }
                                    elementList.add(localContribution);
                                } if (localConversionDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "conversionDate"));
                            
                            
                                    if (localConversionDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("conversionDate cannot be null!!");
                                    }
                                    elementList.add(localConversionDate);
                                } if (localCountryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "country"));
                            
                            
                                    if (localCountry==null){
                                         throw new org.apache.axis2.databinding.ADBException("country cannot be null!!");
                                    }
                                    elementList.add(localCountry);
                                } if (localCountyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "county"));
                            
                            
                                    if (localCounty==null){
                                         throw new org.apache.axis2.databinding.ADBException("county cannot be null!!");
                                    }
                                    elementList.add(localCounty);
                                } if (localCreditHoldTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "creditHold"));
                            
                            
                                    if (localCreditHold==null){
                                         throw new org.apache.axis2.databinding.ADBException("creditHold cannot be null!!");
                                    }
                                    elementList.add(localCreditHold);
                                } if (localCreditHoldOverrideTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "creditHoldOverride"));
                            
                            
                                    if (localCreditHoldOverride==null){
                                         throw new org.apache.axis2.databinding.ADBException("creditHoldOverride cannot be null!!");
                                    }
                                    elementList.add(localCreditHoldOverride);
                                } if (localCreditLimitTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "creditLimit"));
                            
                            
                                    if (localCreditLimit==null){
                                         throw new org.apache.axis2.databinding.ADBException("creditLimit cannot be null!!");
                                    }
                                    elementList.add(localCreditLimit);
                                } if (localCurrencyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "currency"));
                            
                            
                                    if (localCurrency==null){
                                         throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                    }
                                    elementList.add(localCurrency);
                                } if (localCustStageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "custStage"));
                            
                            
                                    if (localCustStage==null){
                                         throw new org.apache.axis2.databinding.ADBException("custStage cannot be null!!");
                                    }
                                    elementList.add(localCustStage);
                                } if (localCustStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "custStatus"));
                            
                            
                                    if (localCustStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("custStatus cannot be null!!");
                                    }
                                    elementList.add(localCustStatus);
                                } if (localDateClosedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "dateClosed"));
                            
                            
                                    if (localDateClosed==null){
                                         throw new org.apache.axis2.databinding.ADBException("dateClosed cannot be null!!");
                                    }
                                    elementList.add(localDateClosed);
                                } if (localDateCreatedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "dateCreated"));
                            
                            
                                    if (localDateCreated==null){
                                         throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                    }
                                    elementList.add(localDateCreated);
                                } if (localDaysOverdueTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "daysOverdue"));
                            
                            
                                    if (localDaysOverdue==null){
                                         throw new org.apache.axis2.databinding.ADBException("daysOverdue cannot be null!!");
                                    }
                                    elementList.add(localDaysOverdue);
                                } if (localDefaultOrderPriorityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "defaultOrderPriority"));
                            
                            
                                    if (localDefaultOrderPriority==null){
                                         throw new org.apache.axis2.databinding.ADBException("defaultOrderPriority cannot be null!!");
                                    }
                                    elementList.add(localDefaultOrderPriority);
                                } if (localDepositBalanceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "depositBalance"));
                            
                            
                                    if (localDepositBalance==null){
                                         throw new org.apache.axis2.databinding.ADBException("depositBalance cannot be null!!");
                                    }
                                    elementList.add(localDepositBalance);
                                } if (localDeptBoughtTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "deptBought"));
                            
                            
                                    if (localDeptBought==null){
                                         throw new org.apache.axis2.databinding.ADBException("deptBought cannot be null!!");
                                    }
                                    elementList.add(localDeptBought);
                                } if (localDrAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "drAccount"));
                            
                            
                                    if (localDrAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("drAccount cannot be null!!");
                                    }
                                    elementList.add(localDrAccount);
                                } if (localEmailTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "email"));
                            
                            
                                    if (localEmail==null){
                                         throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                    }
                                    elementList.add(localEmail);
                                } if (localEmailPreferenceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "emailPreference"));
                            
                            
                                    if (localEmailPreference==null){
                                         throw new org.apache.axis2.databinding.ADBException("emailPreference cannot be null!!");
                                    }
                                    elementList.add(localEmailPreference);
                                } if (localEmailTransactionsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "emailTransactions"));
                            
                            
                                    if (localEmailTransactions==null){
                                         throw new org.apache.axis2.databinding.ADBException("emailTransactions cannot be null!!");
                                    }
                                    elementList.add(localEmailTransactions);
                                } if (localEndDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "endDate"));
                            
                            
                                    if (localEndDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                    }
                                    elementList.add(localEndDate);
                                } if (localEntityIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "entityId"));
                            
                            
                                    if (localEntityId==null){
                                         throw new org.apache.axis2.databinding.ADBException("entityId cannot be null!!");
                                    }
                                    elementList.add(localEntityId);
                                } if (localEntityStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "entityStatus"));
                            
                            
                                    if (localEntityStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("entityStatus cannot be null!!");
                                    }
                                    elementList.add(localEntityStatus);
                                } if (localEstimatedBudgetTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "estimatedBudget"));
                            
                            
                                    if (localEstimatedBudget==null){
                                         throw new org.apache.axis2.databinding.ADBException("estimatedBudget cannot be null!!");
                                    }
                                    elementList.add(localEstimatedBudget);
                                } if (localExplicitConversionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "explicitConversion"));
                            
                            
                                    if (localExplicitConversion==null){
                                         throw new org.apache.axis2.databinding.ADBException("explicitConversion cannot be null!!");
                                    }
                                    elementList.add(localExplicitConversion);
                                } if (localExternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalId"));
                            
                            
                                    if (localExternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                    }
                                    elementList.add(localExternalId);
                                } if (localExternalIdStringTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalIdString"));
                            
                            
                                    if (localExternalIdString==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                    }
                                    elementList.add(localExternalIdString);
                                } if (localFaxTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "fax"));
                            
                            
                                    if (localFax==null){
                                         throw new org.apache.axis2.databinding.ADBException("fax cannot be null!!");
                                    }
                                    elementList.add(localFax);
                                } if (localFaxTransactionsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "faxTransactions"));
                            
                            
                                    if (localFaxTransactions==null){
                                         throw new org.apache.axis2.databinding.ADBException("faxTransactions cannot be null!!");
                                    }
                                    elementList.add(localFaxTransactions);
                                } if (localFirstNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "firstName"));
                            
                            
                                    if (localFirstName==null){
                                         throw new org.apache.axis2.databinding.ADBException("firstName cannot be null!!");
                                    }
                                    elementList.add(localFirstName);
                                } if (localFirstOrderDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "firstOrderDate"));
                            
                            
                                    if (localFirstOrderDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("firstOrderDate cannot be null!!");
                                    }
                                    elementList.add(localFirstOrderDate);
                                } if (localFirstSaleDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "firstSaleDate"));
                            
                            
                                    if (localFirstSaleDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("firstSaleDate cannot be null!!");
                                    }
                                    elementList.add(localFirstSaleDate);
                                } if (localFxAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "fxAccount"));
                            
                            
                                    if (localFxAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("fxAccount cannot be null!!");
                                    }
                                    elementList.add(localFxAccount);
                                } if (localFxBalanceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "fxBalance"));
                            
                            
                                    if (localFxBalance==null){
                                         throw new org.apache.axis2.databinding.ADBException("fxBalance cannot be null!!");
                                    }
                                    elementList.add(localFxBalance);
                                } if (localFxConsolBalanceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "fxConsolBalance"));
                            
                            
                                    if (localFxConsolBalance==null){
                                         throw new org.apache.axis2.databinding.ADBException("fxConsolBalance cannot be null!!");
                                    }
                                    elementList.add(localFxConsolBalance);
                                } if (localFxConsolUnbilledOrdersTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "fxConsolUnbilledOrders"));
                            
                            
                                    if (localFxConsolUnbilledOrders==null){
                                         throw new org.apache.axis2.databinding.ADBException("fxConsolUnbilledOrders cannot be null!!");
                                    }
                                    elementList.add(localFxConsolUnbilledOrders);
                                } if (localFxUnbilledOrdersTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "fxUnbilledOrders"));
                            
                            
                                    if (localFxUnbilledOrders==null){
                                         throw new org.apache.axis2.databinding.ADBException("fxUnbilledOrders cannot be null!!");
                                    }
                                    elementList.add(localFxUnbilledOrders);
                                } if (localGiveAccessTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "giveAccess"));
                            
                            
                                    if (localGiveAccess==null){
                                         throw new org.apache.axis2.databinding.ADBException("giveAccess cannot be null!!");
                                    }
                                    elementList.add(localGiveAccess);
                                } if (localGlobalSubscriptionStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "globalSubscriptionStatus"));
                            
                            
                                    if (localGlobalSubscriptionStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("globalSubscriptionStatus cannot be null!!");
                                    }
                                    elementList.add(localGlobalSubscriptionStatus);
                                } if (localGroupTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "group"));
                            
                            
                                    if (localGroup==null){
                                         throw new org.apache.axis2.databinding.ADBException("group cannot be null!!");
                                    }
                                    elementList.add(localGroup);
                                } if (localGroupPricingLevelTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "groupPricingLevel"));
                            
                            
                                    if (localGroupPricingLevel==null){
                                         throw new org.apache.axis2.databinding.ADBException("groupPricingLevel cannot be null!!");
                                    }
                                    elementList.add(localGroupPricingLevel);
                                } if (localHasDuplicatesTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "hasDuplicates"));
                            
                            
                                    if (localHasDuplicates==null){
                                         throw new org.apache.axis2.databinding.ADBException("hasDuplicates cannot be null!!");
                                    }
                                    elementList.add(localHasDuplicates);
                                } if (localImageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "image"));
                            
                            
                                    if (localImage==null){
                                         throw new org.apache.axis2.databinding.ADBException("image cannot be null!!");
                                    }
                                    elementList.add(localImage);
                                } if (localInternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalId"));
                            
                            
                                    if (localInternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    }
                                    elementList.add(localInternalId);
                                } if (localInternalIdNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalIdNumber"));
                            
                            
                                    if (localInternalIdNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                    }
                                    elementList.add(localInternalIdNumber);
                                } if (localIsBudgetApprovedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isBudgetApproved"));
                            
                            
                                    if (localIsBudgetApproved==null){
                                         throw new org.apache.axis2.databinding.ADBException("isBudgetApproved cannot be null!!");
                                    }
                                    elementList.add(localIsBudgetApproved);
                                } if (localIsDefaultBillingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isDefaultBilling"));
                            
                            
                                    if (localIsDefaultBilling==null){
                                         throw new org.apache.axis2.databinding.ADBException("isDefaultBilling cannot be null!!");
                                    }
                                    elementList.add(localIsDefaultBilling);
                                } if (localIsDefaultShippingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isDefaultShipping"));
                            
                            
                                    if (localIsDefaultShipping==null){
                                         throw new org.apache.axis2.databinding.ADBException("isDefaultShipping cannot be null!!");
                                    }
                                    elementList.add(localIsDefaultShipping);
                                } if (localIsInactiveTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isInactive"));
                            
                            
                                    if (localIsInactive==null){
                                         throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                    }
                                    elementList.add(localIsInactive);
                                } if (localIsPersonTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isPerson"));
                            
                            
                                    if (localIsPerson==null){
                                         throw new org.apache.axis2.databinding.ADBException("isPerson cannot be null!!");
                                    }
                                    elementList.add(localIsPerson);
                                } if (localIsReportedLeadTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isReportedLead"));
                            
                            
                                    if (localIsReportedLead==null){
                                         throw new org.apache.axis2.databinding.ADBException("isReportedLead cannot be null!!");
                                    }
                                    elementList.add(localIsReportedLead);
                                } if (localIsShipAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isShipAddress"));
                            
                            
                                    if (localIsShipAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("isShipAddress cannot be null!!");
                                    }
                                    elementList.add(localIsShipAddress);
                                } if (localItemPricingLevelTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "itemPricingLevel"));
                            
                            
                                    if (localItemPricingLevel==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemPricingLevel cannot be null!!");
                                    }
                                    elementList.add(localItemPricingLevel);
                                } if (localItemPricingUnitPriceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "itemPricingUnitPrice"));
                            
                            
                                    if (localItemPricingUnitPrice==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemPricingUnitPrice cannot be null!!");
                                    }
                                    elementList.add(localItemPricingUnitPrice);
                                } if (localItemsBoughtTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "itemsBought"));
                            
                            
                                    if (localItemsBought==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemsBought cannot be null!!");
                                    }
                                    elementList.add(localItemsBought);
                                } if (localItemsOrderedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "itemsOrdered"));
                            
                            
                                    if (localItemsOrdered==null){
                                         throw new org.apache.axis2.databinding.ADBException("itemsOrdered cannot be null!!");
                                    }
                                    elementList.add(localItemsOrdered);
                                } if (localLanguageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "language"));
                            
                            
                                    if (localLanguage==null){
                                         throw new org.apache.axis2.databinding.ADBException("language cannot be null!!");
                                    }
                                    elementList.add(localLanguage);
                                } if (localLastModifiedDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                            
                            
                                    if (localLastModifiedDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                    }
                                    elementList.add(localLastModifiedDate);
                                } if (localLastNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lastName"));
                            
                            
                                    if (localLastName==null){
                                         throw new org.apache.axis2.databinding.ADBException("lastName cannot be null!!");
                                    }
                                    elementList.add(localLastName);
                                } if (localLastOrderDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lastOrderDate"));
                            
                            
                                    if (localLastOrderDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("lastOrderDate cannot be null!!");
                                    }
                                    elementList.add(localLastOrderDate);
                                } if (localLastSaleDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lastSaleDate"));
                            
                            
                                    if (localLastSaleDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("lastSaleDate cannot be null!!");
                                    }
                                    elementList.add(localLastSaleDate);
                                } if (localLeadDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "leadDate"));
                            
                            
                                    if (localLeadDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("leadDate cannot be null!!");
                                    }
                                    elementList.add(localLeadDate);
                                } if (localLeadSourceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "leadSource"));
                            
                            
                                    if (localLeadSource==null){
                                         throw new org.apache.axis2.databinding.ADBException("leadSource cannot be null!!");
                                    }
                                    elementList.add(localLeadSource);
                                } if (localLevelTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "level"));
                            
                            
                                    if (localLevel==null){
                                         throw new org.apache.axis2.databinding.ADBException("level cannot be null!!");
                                    }
                                    elementList.add(localLevel);
                                } if (localLocationBoughtTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "locationBought"));
                            
                            
                                    if (localLocationBought==null){
                                         throw new org.apache.axis2.databinding.ADBException("locationBought cannot be null!!");
                                    }
                                    elementList.add(localLocationBought);
                                } if (localManualCreditHoldTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "manualCreditHold"));
                            
                            
                                    if (localManualCreditHold==null){
                                         throw new org.apache.axis2.databinding.ADBException("manualCreditHold cannot be null!!");
                                    }
                                    elementList.add(localManualCreditHold);
                                } if (localMerchantAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "merchantAccount"));
                            
                            
                                    if (localMerchantAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("merchantAccount cannot be null!!");
                                    }
                                    elementList.add(localMerchantAccount);
                                } if (localMiddleNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "middleName"));
                            
                            
                                    if (localMiddleName==null){
                                         throw new org.apache.axis2.databinding.ADBException("middleName cannot be null!!");
                                    }
                                    elementList.add(localMiddleName);
                                } if (localMonthlyClosingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "monthlyClosing"));
                            
                            
                                    if (localMonthlyClosing==null){
                                         throw new org.apache.axis2.databinding.ADBException("monthlyClosing cannot be null!!");
                                    }
                                    elementList.add(localMonthlyClosing);
                                } if (localOnCreditHoldTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "onCreditHold"));
                            
                            
                                    if (localOnCreditHold==null){
                                         throw new org.apache.axis2.databinding.ADBException("onCreditHold cannot be null!!");
                                    }
                                    elementList.add(localOnCreditHold);
                                } if (localOrderedAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "orderedAmount"));
                            
                            
                                    if (localOrderedAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("orderedAmount cannot be null!!");
                                    }
                                    elementList.add(localOrderedAmount);
                                } if (localOrderedDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "orderedDate"));
                            
                            
                                    if (localOrderedDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("orderedDate cannot be null!!");
                                    }
                                    elementList.add(localOrderedDate);
                                } if (localOtherRelationshipsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "otherRelationships"));
                            
                            
                                    if (localOtherRelationships==null){
                                         throw new org.apache.axis2.databinding.ADBException("otherRelationships cannot be null!!");
                                    }
                                    elementList.add(localOtherRelationships);
                                } if (localOverdueBalanceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "overdueBalance"));
                            
                            
                                    if (localOverdueBalance==null){
                                         throw new org.apache.axis2.databinding.ADBException("overdueBalance cannot be null!!");
                                    }
                                    elementList.add(localOverdueBalance);
                                } if (localParentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "parent"));
                            
                            
                                    if (localParent==null){
                                         throw new org.apache.axis2.databinding.ADBException("parent cannot be null!!");
                                    }
                                    elementList.add(localParent);
                                } if (localParentItemsBoughtTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "parentItemsBought"));
                            
                            
                                    if (localParentItemsBought==null){
                                         throw new org.apache.axis2.databinding.ADBException("parentItemsBought cannot be null!!");
                                    }
                                    elementList.add(localParentItemsBought);
                                } if (localParentItemsOrderedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "parentItemsOrdered"));
                            
                            
                                    if (localParentItemsOrdered==null){
                                         throw new org.apache.axis2.databinding.ADBException("parentItemsOrdered cannot be null!!");
                                    }
                                    elementList.add(localParentItemsOrdered);
                                } if (localPartnerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "partner"));
                            
                            
                                    if (localPartner==null){
                                         throw new org.apache.axis2.databinding.ADBException("partner cannot be null!!");
                                    }
                                    elementList.add(localPartner);
                                } if (localPartnerContributionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "partnerContribution"));
                            
                            
                                    if (localPartnerContribution==null){
                                         throw new org.apache.axis2.databinding.ADBException("partnerContribution cannot be null!!");
                                    }
                                    elementList.add(localPartnerContribution);
                                } if (localPartnerRoleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "partnerRole"));
                            
                            
                                    if (localPartnerRole==null){
                                         throw new org.apache.axis2.databinding.ADBException("partnerRole cannot be null!!");
                                    }
                                    elementList.add(localPartnerRole);
                                } if (localPartnerTeamMemberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "partnerTeamMember"));
                            
                            
                                    if (localPartnerTeamMember==null){
                                         throw new org.apache.axis2.databinding.ADBException("partnerTeamMember cannot be null!!");
                                    }
                                    elementList.add(localPartnerTeamMember);
                                } if (localPecTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "pec"));
                            
                            
                                    if (localPec==null){
                                         throw new org.apache.axis2.databinding.ADBException("pec cannot be null!!");
                                    }
                                    elementList.add(localPec);
                                } if (localPermissionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "permission"));
                            
                            
                                    if (localPermission==null){
                                         throw new org.apache.axis2.databinding.ADBException("permission cannot be null!!");
                                    }
                                    elementList.add(localPermission);
                                } if (localPhoneTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "phone"));
                            
                            
                                    if (localPhone==null){
                                         throw new org.apache.axis2.databinding.ADBException("phone cannot be null!!");
                                    }
                                    elementList.add(localPhone);
                                } if (localPhoneticNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "phoneticName"));
                            
                            
                                    if (localPhoneticName==null){
                                         throw new org.apache.axis2.databinding.ADBException("phoneticName cannot be null!!");
                                    }
                                    elementList.add(localPhoneticName);
                                } if (localPriceLevelTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "priceLevel"));
                            
                            
                                    if (localPriceLevel==null){
                                         throw new org.apache.axis2.databinding.ADBException("priceLevel cannot be null!!");
                                    }
                                    elementList.add(localPriceLevel);
                                } if (localPricingGroupTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "pricingGroup"));
                            
                            
                                    if (localPricingGroup==null){
                                         throw new org.apache.axis2.databinding.ADBException("pricingGroup cannot be null!!");
                                    }
                                    elementList.add(localPricingGroup);
                                } if (localPricingItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "pricingItem"));
                            
                            
                                    if (localPricingItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("pricingItem cannot be null!!");
                                    }
                                    elementList.add(localPricingItem);
                                } if (localPrintTransactionsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "printTransactions"));
                            
                            
                                    if (localPrintTransactions==null){
                                         throw new org.apache.axis2.databinding.ADBException("printTransactions cannot be null!!");
                                    }
                                    elementList.add(localPrintTransactions);
                                } if (localProspectDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "prospectDate"));
                            
                            
                                    if (localProspectDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("prospectDate cannot be null!!");
                                    }
                                    elementList.add(localProspectDate);
                                } if (localPstExemptTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "pstExempt"));
                            
                            
                                    if (localPstExempt==null){
                                         throw new org.apache.axis2.databinding.ADBException("pstExempt cannot be null!!");
                                    }
                                    elementList.add(localPstExempt);
                                } if (localReceivablesAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "receivablesAccount"));
                            
                            
                                    if (localReceivablesAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("receivablesAccount cannot be null!!");
                                    }
                                    elementList.add(localReceivablesAccount);
                                } if (localReminderDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "reminderDate"));
                            
                            
                                    if (localReminderDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("reminderDate cannot be null!!");
                                    }
                                    elementList.add(localReminderDate);
                                } if (localResaleNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "resaleNumber"));
                            
                            
                                    if (localResaleNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("resaleNumber cannot be null!!");
                                    }
                                    elementList.add(localResaleNumber);
                                } if (localRoleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "role"));
                            
                            
                                    if (localRole==null){
                                         throw new org.apache.axis2.databinding.ADBException("role cannot be null!!");
                                    }
                                    elementList.add(localRole);
                                } if (localSalesReadinessTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "salesReadiness"));
                            
                            
                                    if (localSalesReadiness==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesReadiness cannot be null!!");
                                    }
                                    elementList.add(localSalesReadiness);
                                } if (localSalesRepTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "salesRep"));
                            
                            
                                    if (localSalesRep==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesRep cannot be null!!");
                                    }
                                    elementList.add(localSalesRep);
                                } if (localSalesTeamMemberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "salesTeamMember"));
                            
                            
                                    if (localSalesTeamMember==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesTeamMember cannot be null!!");
                                    }
                                    elementList.add(localSalesTeamMember);
                                } if (localSalesTeamRoleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "salesTeamRole"));
                            
                            
                                    if (localSalesTeamRole==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesTeamRole cannot be null!!");
                                    }
                                    elementList.add(localSalesTeamRole);
                                } if (localSalutationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "salutation"));
                            
                            
                                    if (localSalutation==null){
                                         throw new org.apache.axis2.databinding.ADBException("salutation cannot be null!!");
                                    }
                                    elementList.add(localSalutation);
                                } if (localShipAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "shipAddress"));
                            
                            
                                    if (localShipAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("shipAddress cannot be null!!");
                                    }
                                    elementList.add(localShipAddress);
                                } if (localShipCompleteTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "shipComplete"));
                            
                            
                                    if (localShipComplete==null){
                                         throw new org.apache.axis2.databinding.ADBException("shipComplete cannot be null!!");
                                    }
                                    elementList.add(localShipComplete);
                                } if (localShippingItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "shippingItem"));
                            
                            
                                    if (localShippingItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("shippingItem cannot be null!!");
                                    }
                                    elementList.add(localShippingItem);
                                } if (localStageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "stage"));
                            
                            
                                    if (localStage==null){
                                         throw new org.apache.axis2.databinding.ADBException("stage cannot be null!!");
                                    }
                                    elementList.add(localStage);
                                } if (localStartDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "startDate"));
                            
                            
                                    if (localStartDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                    }
                                    elementList.add(localStartDate);
                                } if (localStateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "state"));
                            
                            
                                    if (localState==null){
                                         throw new org.apache.axis2.databinding.ADBException("state cannot be null!!");
                                    }
                                    elementList.add(localState);
                                } if (localSubsidBoughtTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "subsidBought"));
                            
                            
                                    if (localSubsidBought==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidBought cannot be null!!");
                                    }
                                    elementList.add(localSubsidBought);
                                } if (localSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "subsidiary"));
                            
                            
                                    if (localSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    }
                                    elementList.add(localSubsidiary);
                                } if (localTaxableTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "taxable"));
                            
                            
                                    if (localTaxable==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxable cannot be null!!");
                                    }
                                    elementList.add(localTaxable);
                                } if (localTermsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "terms"));
                            
                            
                                    if (localTerms==null){
                                         throw new org.apache.axis2.databinding.ADBException("terms cannot be null!!");
                                    }
                                    elementList.add(localTerms);
                                } if (localTerritoryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "territory"));
                            
                            
                                    if (localTerritory==null){
                                         throw new org.apache.axis2.databinding.ADBException("territory cannot be null!!");
                                    }
                                    elementList.add(localTerritory);
                                } if (localTitleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "title"));
                            
                            
                                    if (localTitle==null){
                                         throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                    }
                                    elementList.add(localTitle);
                                } if (localUnbilledOrdersTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "unbilledOrders"));
                            
                            
                                    if (localUnbilledOrders==null){
                                         throw new org.apache.axis2.databinding.ADBException("unbilledOrders cannot be null!!");
                                    }
                                    elementList.add(localUnbilledOrders);
                                } if (localUrlTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "url"));
                            
                            
                                    if (localUrl==null){
                                         throw new org.apache.axis2.databinding.ADBException("url cannot be null!!");
                                    }
                                    elementList.add(localUrl);
                                } if (localVatRegNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "vatRegNumber"));
                            
                            
                                    if (localVatRegNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("vatRegNumber cannot be null!!");
                                    }
                                    elementList.add(localVatRegNumber);
                                } if (localWebLeadTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "webLead"));
                            
                            
                                    if (localWebLead==null){
                                         throw new org.apache.axis2.databinding.ADBException("webLead cannot be null!!");
                                    }
                                    elementList.add(localWebLead);
                                } if (localZipCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "zipCode"));
                            
                            
                                    if (localZipCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("zipCode cannot be null!!");
                                    }
                                    elementList.add(localZipCode);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static CustomerSearchBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            CustomerSearchBasic object =
                new CustomerSearchBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"CustomerSearchBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (CustomerSearchBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","accountNumber").equals(reader.getName())){
                                
                                                object.setAccountNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address").equals(reader.getName())){
                                
                                                object.setAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressee").equals(reader.getName())){
                                
                                                object.setAddressee(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressLabel").equals(reader.getName())){
                                
                                                object.setAddressLabel(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressPhone").equals(reader.getName())){
                                
                                                object.setAddressPhone(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","attention").equals(reader.getName())){
                                
                                                object.setAttention(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","availableOffline").equals(reader.getName())){
                                
                                                object.setAvailableOffline(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","balance").equals(reader.getName())){
                                
                                                object.setBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billAddress").equals(reader.getName())){
                                
                                                object.setBillAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","boughtAmount").equals(reader.getName())){
                                
                                                object.setBoughtAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","boughtDate").equals(reader.getName())){
                                
                                                object.setBoughtDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buyingReason").equals(reader.getName())){
                                
                                                object.setBuyingReason(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buyingTimeFrame").equals(reader.getName())){
                                
                                                object.setBuyingTimeFrame(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","category").equals(reader.getName())){
                                
                                                object.setCategory(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ccCustomerCode").equals(reader.getName())){
                                
                                                object.setCcCustomerCode(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ccDefault").equals(reader.getName())){
                                
                                                object.setCcDefault(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ccExpDate").equals(reader.getName())){
                                
                                                object.setCcExpDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ccHolderName").equals(reader.getName())){
                                
                                                object.setCcHolderName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ccNumber").equals(reader.getName())){
                                
                                                object.setCcNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ccState").equals(reader.getName())){
                                
                                                object.setCcState(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ccStateFrom").equals(reader.getName())){
                                
                                                object.setCcStateFrom(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ccType").equals(reader.getName())){
                                
                                                object.setCcType(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","city").equals(reader.getName())){
                                
                                                object.setCity(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","classBought").equals(reader.getName())){
                                
                                                object.setClassBought(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","comments").equals(reader.getName())){
                                
                                                object.setComments(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","companyName").equals(reader.getName())){
                                
                                                object.setCompanyName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","consolBalance").equals(reader.getName())){
                                
                                                object.setConsolBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","consolDaysOverdue").equals(reader.getName())){
                                
                                                object.setConsolDaysOverdue(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","consolDepositBalance").equals(reader.getName())){
                                
                                                object.setConsolDepositBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","consolOverdueBalance").equals(reader.getName())){
                                
                                                object.setConsolOverdueBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","consolUnbilledOrders").equals(reader.getName())){
                                
                                                object.setConsolUnbilledOrders(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","contact").equals(reader.getName())){
                                
                                                object.setContact(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","contribution").equals(reader.getName())){
                                
                                                object.setContribution(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","conversionDate").equals(reader.getName())){
                                
                                                object.setConversionDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","country").equals(reader.getName())){
                                
                                                object.setCountry(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","county").equals(reader.getName())){
                                
                                                object.setCounty(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","creditHold").equals(reader.getName())){
                                
                                                object.setCreditHold(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","creditHoldOverride").equals(reader.getName())){
                                
                                                object.setCreditHoldOverride(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","creditLimit").equals(reader.getName())){
                                
                                                object.setCreditLimit(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency").equals(reader.getName())){
                                
                                                object.setCurrency(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","custStage").equals(reader.getName())){
                                
                                                object.setCustStage(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","custStatus").equals(reader.getName())){
                                
                                                object.setCustStatus(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateClosed").equals(reader.getName())){
                                
                                                object.setDateClosed(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated").equals(reader.getName())){
                                
                                                object.setDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","daysOverdue").equals(reader.getName())){
                                
                                                object.setDaysOverdue(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","defaultOrderPriority").equals(reader.getName())){
                                
                                                object.setDefaultOrderPriority(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","depositBalance").equals(reader.getName())){
                                
                                                object.setDepositBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","deptBought").equals(reader.getName())){
                                
                                                object.setDeptBought(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","drAccount").equals(reader.getName())){
                                
                                                object.setDrAccount(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","email").equals(reader.getName())){
                                
                                                object.setEmail(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","emailPreference").equals(reader.getName())){
                                
                                                object.setEmailPreference(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","emailTransactions").equals(reader.getName())){
                                
                                                object.setEmailTransactions(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endDate").equals(reader.getName())){
                                
                                                object.setEndDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entityId").equals(reader.getName())){
                                
                                                object.setEntityId(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entityStatus").equals(reader.getName())){
                                
                                                object.setEntityStatus(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedBudget").equals(reader.getName())){
                                
                                                object.setEstimatedBudget(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","explicitConversion").equals(reader.getName())){
                                
                                                object.setExplicitConversion(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                
                                                object.setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString").equals(reader.getName())){
                                
                                                object.setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fax").equals(reader.getName())){
                                
                                                object.setFax(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","faxTransactions").equals(reader.getName())){
                                
                                                object.setFaxTransactions(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","firstName").equals(reader.getName())){
                                
                                                object.setFirstName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","firstOrderDate").equals(reader.getName())){
                                
                                                object.setFirstOrderDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","firstSaleDate").equals(reader.getName())){
                                
                                                object.setFirstSaleDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxAccount").equals(reader.getName())){
                                
                                                object.setFxAccount(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxBalance").equals(reader.getName())){
                                
                                                object.setFxBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxConsolBalance").equals(reader.getName())){
                                
                                                object.setFxConsolBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxConsolUnbilledOrders").equals(reader.getName())){
                                
                                                object.setFxConsolUnbilledOrders(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxUnbilledOrders").equals(reader.getName())){
                                
                                                object.setFxUnbilledOrders(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","giveAccess").equals(reader.getName())){
                                
                                                object.setGiveAccess(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","globalSubscriptionStatus").equals(reader.getName())){
                                
                                                object.setGlobalSubscriptionStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","group").equals(reader.getName())){
                                
                                                object.setGroup(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","groupPricingLevel").equals(reader.getName())){
                                
                                                object.setGroupPricingLevel(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","hasDuplicates").equals(reader.getName())){
                                
                                                object.setHasDuplicates(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","image").equals(reader.getName())){
                                
                                                object.setImage(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                                object.setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber").equals(reader.getName())){
                                
                                                object.setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isBudgetApproved").equals(reader.getName())){
                                
                                                object.setIsBudgetApproved(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isDefaultBilling").equals(reader.getName())){
                                
                                                object.setIsDefaultBilling(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isDefaultShipping").equals(reader.getName())){
                                
                                                object.setIsDefaultShipping(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isInactive").equals(reader.getName())){
                                
                                                object.setIsInactive(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isPerson").equals(reader.getName())){
                                
                                                object.setIsPerson(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isReportedLead").equals(reader.getName())){
                                
                                                object.setIsReportedLead(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isShipAddress").equals(reader.getName())){
                                
                                                object.setIsShipAddress(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","itemPricingLevel").equals(reader.getName())){
                                
                                                object.setItemPricingLevel(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","itemPricingUnitPrice").equals(reader.getName())){
                                
                                                object.setItemPricingUnitPrice(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","itemsBought").equals(reader.getName())){
                                
                                                object.setItemsBought(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","itemsOrdered").equals(reader.getName())){
                                
                                                object.setItemsOrdered(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","language").equals(reader.getName())){
                                
                                                object.setLanguage(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                                object.setLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastName").equals(reader.getName())){
                                
                                                object.setLastName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastOrderDate").equals(reader.getName())){
                                
                                                object.setLastOrderDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastSaleDate").equals(reader.getName())){
                                
                                                object.setLastSaleDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","leadDate").equals(reader.getName())){
                                
                                                object.setLeadDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","leadSource").equals(reader.getName())){
                                
                                                object.setLeadSource(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","level").equals(reader.getName())){
                                
                                                object.setLevel(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","locationBought").equals(reader.getName())){
                                
                                                object.setLocationBought(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","manualCreditHold").equals(reader.getName())){
                                
                                                object.setManualCreditHold(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","merchantAccount").equals(reader.getName())){
                                
                                                object.setMerchantAccount(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","middleName").equals(reader.getName())){
                                
                                                object.setMiddleName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","monthlyClosing").equals(reader.getName())){
                                
                                                object.setMonthlyClosing(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","onCreditHold").equals(reader.getName())){
                                
                                                object.setOnCreditHold(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","orderedAmount").equals(reader.getName())){
                                
                                                object.setOrderedAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","orderedDate").equals(reader.getName())){
                                
                                                object.setOrderedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","otherRelationships").equals(reader.getName())){
                                
                                                object.setOtherRelationships(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","overdueBalance").equals(reader.getName())){
                                
                                                object.setOverdueBalance(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","parent").equals(reader.getName())){
                                
                                                object.setParent(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","parentItemsBought").equals(reader.getName())){
                                
                                                object.setParentItemsBought(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","parentItemsOrdered").equals(reader.getName())){
                                
                                                object.setParentItemsOrdered(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partner").equals(reader.getName())){
                                
                                                object.setPartner(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerContribution").equals(reader.getName())){
                                
                                                object.setPartnerContribution(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerRole").equals(reader.getName())){
                                
                                                object.setPartnerRole(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerTeamMember").equals(reader.getName())){
                                
                                                object.setPartnerTeamMember(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","pec").equals(reader.getName())){
                                
                                                object.setPec(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","permission").equals(reader.getName())){
                                
                                                object.setPermission(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phone").equals(reader.getName())){
                                
                                                object.setPhone(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phoneticName").equals(reader.getName())){
                                
                                                object.setPhoneticName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","priceLevel").equals(reader.getName())){
                                
                                                object.setPriceLevel(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","pricingGroup").equals(reader.getName())){
                                
                                                object.setPricingGroup(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","pricingItem").equals(reader.getName())){
                                
                                                object.setPricingItem(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","printTransactions").equals(reader.getName())){
                                
                                                object.setPrintTransactions(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","prospectDate").equals(reader.getName())){
                                
                                                object.setProspectDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","pstExempt").equals(reader.getName())){
                                
                                                object.setPstExempt(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","receivablesAccount").equals(reader.getName())){
                                
                                                object.setReceivablesAccount(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","reminderDate").equals(reader.getName())){
                                
                                                object.setReminderDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","resaleNumber").equals(reader.getName())){
                                
                                                object.setResaleNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","role").equals(reader.getName())){
                                
                                                object.setRole(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesReadiness").equals(reader.getName())){
                                
                                                object.setSalesReadiness(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesRep").equals(reader.getName())){
                                
                                                object.setSalesRep(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesTeamMember").equals(reader.getName())){
                                
                                                object.setSalesTeamMember(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesTeamRole").equals(reader.getName())){
                                
                                                object.setSalesTeamRole(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salutation").equals(reader.getName())){
                                
                                                object.setSalutation(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","shipAddress").equals(reader.getName())){
                                
                                                object.setShipAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","shipComplete").equals(reader.getName())){
                                
                                                object.setShipComplete(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","shippingItem").equals(reader.getName())){
                                
                                                object.setShippingItem(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","stage").equals(reader.getName())){
                                
                                                object.setStage(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDate").equals(reader.getName())){
                                
                                                object.setStartDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","state").equals(reader.getName())){
                                
                                                object.setState(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidBought").equals(reader.getName())){
                                
                                                object.setSubsidBought(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                                object.setSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","taxable").equals(reader.getName())){
                                
                                                object.setTaxable(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","terms").equals(reader.getName())){
                                
                                                object.setTerms(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","territory").equals(reader.getName())){
                                
                                                object.setTerritory(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","title").equals(reader.getName())){
                                
                                                object.setTitle(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","unbilledOrders").equals(reader.getName())){
                                
                                                object.setUnbilledOrders(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","url").equals(reader.getName())){
                                
                                                object.setUrl(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","vatRegNumber").equals(reader.getName())){
                                
                                                object.setVatRegNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","webLead").equals(reader.getName())){
                                
                                                object.setWebLead(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","zipCode").equals(reader.getName())){
                                
                                                object.setZipCode(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    