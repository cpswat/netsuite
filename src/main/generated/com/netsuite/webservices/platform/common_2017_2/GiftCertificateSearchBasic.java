
/**
 * GiftCertificateSearchBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  GiftCertificateSearchBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class GiftCertificateSearchBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRecordBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = GiftCertificateSearchBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for AmountAvailableBilled
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localAmountAvailableBilled ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmountAvailableBilledTracker = false ;

                           public boolean isAmountAvailableBilledSpecified(){
                               return localAmountAvailableBilledTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getAmountAvailableBilled(){
                               return localAmountAvailableBilled;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AmountAvailableBilled
                               */
                               public void setAmountAvailableBilled(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localAmountAvailableBilledTracker = param != null;
                                   
                                            this.localAmountAvailableBilled=param;
                                    

                               }
                            

                        /**
                        * field for AmountRemaining
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localAmountRemaining ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmountRemainingTracker = false ;

                           public boolean isAmountRemainingSpecified(){
                               return localAmountRemainingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getAmountRemaining(){
                               return localAmountRemaining;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AmountRemaining
                               */
                               public void setAmountRemaining(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localAmountRemainingTracker = param != null;
                                   
                                            this.localAmountRemaining=param;
                                    

                               }
                            

                        /**
                        * field for CreatedDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localCreatedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedDateTracker = false ;

                           public boolean isCreatedDateSpecified(){
                               return localCreatedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getCreatedDate(){
                               return localCreatedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedDate
                               */
                               public void setCreatedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localCreatedDateTracker = param != null;
                                   
                                            this.localCreatedDate=param;
                                    

                               }
                            

                        /**
                        * field for Email
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailTracker = false ;

                           public boolean isEmailSpecified(){
                               return localEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getEmail(){
                               return localEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Email
                               */
                               public void setEmail(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localEmailTracker = param != null;
                                   
                                            this.localEmail=param;
                                    

                               }
                            

                        /**
                        * field for ExpirationDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localExpirationDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExpirationDateTracker = false ;

                           public boolean isExpirationDateSpecified(){
                               return localExpirationDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getExpirationDate(){
                               return localExpirationDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExpirationDate
                               */
                               public void setExpirationDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localExpirationDateTracker = param != null;
                                   
                                            this.localExpirationDate=param;
                                    

                               }
                            

                        /**
                        * field for GiftCertCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localGiftCertCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiftCertCodeTracker = false ;

                           public boolean isGiftCertCodeSpecified(){
                               return localGiftCertCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getGiftCertCode(){
                               return localGiftCertCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiftCertCode
                               */
                               public void setGiftCertCode(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localGiftCertCodeTracker = param != null;
                                   
                                            this.localGiftCertCode=param;
                                    

                               }
                            

                        /**
                        * field for IncomeAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localIncomeAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIncomeAccountTracker = false ;

                           public boolean isIncomeAccountSpecified(){
                               return localIncomeAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getIncomeAccount(){
                               return localIncomeAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IncomeAccount
                               */
                               public void setIncomeAccount(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localIncomeAccountTracker = param != null;
                                   
                                            this.localIncomeAccount=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localInternalIdTracker = param != null;
                                   
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for InternalIdNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localInternalIdNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdNumberTracker = false ;

                           public boolean isInternalIdNumberSpecified(){
                               return localInternalIdNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getInternalIdNumber(){
                               return localInternalIdNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalIdNumber
                               */
                               public void setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localInternalIdNumberTracker = param != null;
                                   
                                            this.localInternalIdNumber=param;
                                    

                               }
                            

                        /**
                        * field for IsActive
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsActive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsActiveTracker = false ;

                           public boolean isIsActiveSpecified(){
                               return localIsActiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsActive(){
                               return localIsActive;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsActive
                               */
                               public void setIsActive(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsActiveTracker = param != null;
                                   
                                            this.localIsActive=param;
                                    

                               }
                            

                        /**
                        * field for Item
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemTracker = false ;

                           public boolean isItemSpecified(){
                               return localItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getItem(){
                               return localItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Item
                               */
                               public void setItem(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localItemTracker = param != null;
                                   
                                            this.localItem=param;
                                    

                               }
                            

                        /**
                        * field for LiabilityAccount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localLiabilityAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLiabilityAccountTracker = false ;

                           public boolean isLiabilityAccountSpecified(){
                               return localLiabilityAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getLiabilityAccount(){
                               return localLiabilityAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LiabilityAccount
                               */
                               public void setLiabilityAccount(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localLiabilityAccountTracker = param != null;
                                   
                                            this.localLiabilityAccount=param;
                                    

                               }
                            

                        /**
                        * field for Message
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localMessage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMessageTracker = false ;

                           public boolean isMessageSpecified(){
                               return localMessageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getMessage(){
                               return localMessage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Message
                               */
                               public void setMessage(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localMessageTracker = param != null;
                                   
                                            this.localMessage=param;
                                    

                               }
                            

                        /**
                        * field for Name
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNameTracker = false ;

                           public boolean isNameSpecified(){
                               return localNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getName(){
                               return localName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Name
                               */
                               public void setName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localNameTracker = param != null;
                                   
                                            this.localName=param;
                                    

                               }
                            

                        /**
                        * field for OriginalAmount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localOriginalAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOriginalAmountTracker = false ;

                           public boolean isOriginalAmountSpecified(){
                               return localOriginalAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getOriginalAmount(){
                               return localOriginalAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OriginalAmount
                               */
                               public void setOriginalAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localOriginalAmountTracker = param != null;
                                   
                                            this.localOriginalAmount=param;
                                    

                               }
                            

                        /**
                        * field for PurchaseDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localPurchaseDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPurchaseDateTracker = false ;

                           public boolean isPurchaseDateSpecified(){
                               return localPurchaseDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getPurchaseDate(){
                               return localPurchaseDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PurchaseDate
                               */
                               public void setPurchaseDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localPurchaseDateTracker = param != null;
                                   
                                            this.localPurchaseDate=param;
                                    

                               }
                            

                        /**
                        * field for Sender
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localSender ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSenderTracker = false ;

                           public boolean isSenderSpecified(){
                               return localSenderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getSender(){
                               return localSender;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Sender
                               */
                               public void setSender(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localSenderTracker = param != null;
                                   
                                            this.localSender=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":GiftCertificateSearchBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "GiftCertificateSearchBasic",
                           xmlWriter);
                   }

                if (localAmountAvailableBilledTracker){
                                            if (localAmountAvailableBilled==null){
                                                 throw new org.apache.axis2.databinding.ADBException("amountAvailableBilled cannot be null!!");
                                            }
                                           localAmountAvailableBilled.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","amountAvailableBilled"),
                                               xmlWriter);
                                        } if (localAmountRemainingTracker){
                                            if (localAmountRemaining==null){
                                                 throw new org.apache.axis2.databinding.ADBException("amountRemaining cannot be null!!");
                                            }
                                           localAmountRemaining.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","amountRemaining"),
                                               xmlWriter);
                                        } if (localCreatedDateTracker){
                                            if (localCreatedDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                            }
                                           localCreatedDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","createdDate"),
                                               xmlWriter);
                                        } if (localEmailTracker){
                                            if (localEmail==null){
                                                 throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                            }
                                           localEmail.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","email"),
                                               xmlWriter);
                                        } if (localExpirationDateTracker){
                                            if (localExpirationDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("expirationDate cannot be null!!");
                                            }
                                           localExpirationDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","expirationDate"),
                                               xmlWriter);
                                        } if (localGiftCertCodeTracker){
                                            if (localGiftCertCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("giftCertCode cannot be null!!");
                                            }
                                           localGiftCertCode.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","giftCertCode"),
                                               xmlWriter);
                                        } if (localIncomeAccountTracker){
                                            if (localIncomeAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("incomeAccount cannot be null!!");
                                            }
                                           localIncomeAccount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","incomeAccount"),
                                               xmlWriter);
                                        } if (localInternalIdTracker){
                                            if (localInternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                            }
                                           localInternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                               xmlWriter);
                                        } if (localInternalIdNumberTracker){
                                            if (localInternalIdNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                            }
                                           localInternalIdNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber"),
                                               xmlWriter);
                                        } if (localIsActiveTracker){
                                            if (localIsActive==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isActive cannot be null!!");
                                            }
                                           localIsActive.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isActive"),
                                               xmlWriter);
                                        } if (localItemTracker){
                                            if (localItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                            }
                                           localItem.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","item"),
                                               xmlWriter);
                                        } if (localLiabilityAccountTracker){
                                            if (localLiabilityAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("liabilityAccount cannot be null!!");
                                            }
                                           localLiabilityAccount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","liabilityAccount"),
                                               xmlWriter);
                                        } if (localMessageTracker){
                                            if (localMessage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("message cannot be null!!");
                                            }
                                           localMessage.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","message"),
                                               xmlWriter);
                                        } if (localNameTracker){
                                            if (localName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                            }
                                           localName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","name"),
                                               xmlWriter);
                                        } if (localOriginalAmountTracker){
                                            if (localOriginalAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("originalAmount cannot be null!!");
                                            }
                                           localOriginalAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","originalAmount"),
                                               xmlWriter);
                                        } if (localPurchaseDateTracker){
                                            if (localPurchaseDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("purchaseDate cannot be null!!");
                                            }
                                           localPurchaseDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","purchaseDate"),
                                               xmlWriter);
                                        } if (localSenderTracker){
                                            if (localSender==null){
                                                 throw new org.apache.axis2.databinding.ADBException("sender cannot be null!!");
                                            }
                                           localSender.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","sender"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","GiftCertificateSearchBasic"));
                 if (localAmountAvailableBilledTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "amountAvailableBilled"));
                            
                            
                                    if (localAmountAvailableBilled==null){
                                         throw new org.apache.axis2.databinding.ADBException("amountAvailableBilled cannot be null!!");
                                    }
                                    elementList.add(localAmountAvailableBilled);
                                } if (localAmountRemainingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "amountRemaining"));
                            
                            
                                    if (localAmountRemaining==null){
                                         throw new org.apache.axis2.databinding.ADBException("amountRemaining cannot be null!!");
                                    }
                                    elementList.add(localAmountRemaining);
                                } if (localCreatedDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "createdDate"));
                            
                            
                                    if (localCreatedDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                    }
                                    elementList.add(localCreatedDate);
                                } if (localEmailTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "email"));
                            
                            
                                    if (localEmail==null){
                                         throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                    }
                                    elementList.add(localEmail);
                                } if (localExpirationDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "expirationDate"));
                            
                            
                                    if (localExpirationDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("expirationDate cannot be null!!");
                                    }
                                    elementList.add(localExpirationDate);
                                } if (localGiftCertCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "giftCertCode"));
                            
                            
                                    if (localGiftCertCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("giftCertCode cannot be null!!");
                                    }
                                    elementList.add(localGiftCertCode);
                                } if (localIncomeAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "incomeAccount"));
                            
                            
                                    if (localIncomeAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("incomeAccount cannot be null!!");
                                    }
                                    elementList.add(localIncomeAccount);
                                } if (localInternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalId"));
                            
                            
                                    if (localInternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    }
                                    elementList.add(localInternalId);
                                } if (localInternalIdNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalIdNumber"));
                            
                            
                                    if (localInternalIdNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                    }
                                    elementList.add(localInternalIdNumber);
                                } if (localIsActiveTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isActive"));
                            
                            
                                    if (localIsActive==null){
                                         throw new org.apache.axis2.databinding.ADBException("isActive cannot be null!!");
                                    }
                                    elementList.add(localIsActive);
                                } if (localItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "item"));
                            
                            
                                    if (localItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                    }
                                    elementList.add(localItem);
                                } if (localLiabilityAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "liabilityAccount"));
                            
                            
                                    if (localLiabilityAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("liabilityAccount cannot be null!!");
                                    }
                                    elementList.add(localLiabilityAccount);
                                } if (localMessageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "message"));
                            
                            
                                    if (localMessage==null){
                                         throw new org.apache.axis2.databinding.ADBException("message cannot be null!!");
                                    }
                                    elementList.add(localMessage);
                                } if (localNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "name"));
                            
                            
                                    if (localName==null){
                                         throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                    }
                                    elementList.add(localName);
                                } if (localOriginalAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "originalAmount"));
                            
                            
                                    if (localOriginalAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("originalAmount cannot be null!!");
                                    }
                                    elementList.add(localOriginalAmount);
                                } if (localPurchaseDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "purchaseDate"));
                            
                            
                                    if (localPurchaseDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("purchaseDate cannot be null!!");
                                    }
                                    elementList.add(localPurchaseDate);
                                } if (localSenderTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "sender"));
                            
                            
                                    if (localSender==null){
                                         throw new org.apache.axis2.databinding.ADBException("sender cannot be null!!");
                                    }
                                    elementList.add(localSender);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static GiftCertificateSearchBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            GiftCertificateSearchBasic object =
                new GiftCertificateSearchBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"GiftCertificateSearchBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (GiftCertificateSearchBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","amountAvailableBilled").equals(reader.getName())){
                                
                                                object.setAmountAvailableBilled(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","amountRemaining").equals(reader.getName())){
                                
                                                object.setAmountRemaining(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","createdDate").equals(reader.getName())){
                                
                                                object.setCreatedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","email").equals(reader.getName())){
                                
                                                object.setEmail(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","expirationDate").equals(reader.getName())){
                                
                                                object.setExpirationDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","giftCertCode").equals(reader.getName())){
                                
                                                object.setGiftCertCode(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","incomeAccount").equals(reader.getName())){
                                
                                                object.setIncomeAccount(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                                object.setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber").equals(reader.getName())){
                                
                                                object.setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isActive").equals(reader.getName())){
                                
                                                object.setIsActive(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","item").equals(reader.getName())){
                                
                                                object.setItem(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","liabilityAccount").equals(reader.getName())){
                                
                                                object.setLiabilityAccount(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","message").equals(reader.getName())){
                                
                                                object.setMessage(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","name").equals(reader.getName())){
                                
                                                object.setName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","originalAmount").equals(reader.getName())){
                                
                                                object.setOriginalAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","purchaseDate").equals(reader.getName())){
                                
                                                object.setPurchaseDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","sender").equals(reader.getName())){
                                
                                                object.setSender(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    