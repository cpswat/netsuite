
/**
 * OpportunitySearchBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  OpportunitySearchBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class OpportunitySearchBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRecordBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = OpportunitySearchBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for Amount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmountTracker = false ;

                           public boolean isAmountSpecified(){
                               return localAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getAmount(){
                               return localAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Amount
                               */
                               public void setAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localAmountTracker = param != null;
                                   
                                            this.localAmount=param;
                                    

                               }
                            

                        /**
                        * field for AvailableOffline
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localAvailableOffline ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAvailableOfflineTracker = false ;

                           public boolean isAvailableOfflineSpecified(){
                               return localAvailableOfflineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getAvailableOffline(){
                               return localAvailableOffline;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AvailableOffline
                               */
                               public void setAvailableOffline(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localAvailableOfflineTracker = param != null;
                                   
                                            this.localAvailableOffline=param;
                                    

                               }
                            

                        /**
                        * field for BuyingReason
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localBuyingReason ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBuyingReasonTracker = false ;

                           public boolean isBuyingReasonSpecified(){
                               return localBuyingReasonTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getBuyingReason(){
                               return localBuyingReason;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BuyingReason
                               */
                               public void setBuyingReason(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localBuyingReasonTracker = param != null;
                                   
                                            this.localBuyingReason=param;
                                    

                               }
                            

                        /**
                        * field for BuyingTimeFrame
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localBuyingTimeFrame ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBuyingTimeFrameTracker = false ;

                           public boolean isBuyingTimeFrameSpecified(){
                               return localBuyingTimeFrameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getBuyingTimeFrame(){
                               return localBuyingTimeFrame;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BuyingTimeFrame
                               */
                               public void setBuyingTimeFrame(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localBuyingTimeFrameTracker = param != null;
                                   
                                            this.localBuyingTimeFrame=param;
                                    

                               }
                            

                        /**
                        * field for _class
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField get_class(){
                               return local_class;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param _class
                               */
                               public void set_class(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            local_classTracker = param != null;
                                   
                                            this.local_class=param;
                                    

                               }
                            

                        /**
                        * field for CloseDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localCloseDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCloseDateTracker = false ;

                           public boolean isCloseDateSpecified(){
                               return localCloseDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getCloseDate(){
                               return localCloseDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CloseDate
                               */
                               public void setCloseDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localCloseDateTracker = param != null;
                                   
                                            this.localCloseDate=param;
                                    

                               }
                            

                        /**
                        * field for Competitor
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCompetitor ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCompetitorTracker = false ;

                           public boolean isCompetitorSpecified(){
                               return localCompetitorTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCompetitor(){
                               return localCompetitor;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Competitor
                               */
                               public void setCompetitor(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCompetitorTracker = param != null;
                                   
                                            this.localCompetitor=param;
                                    

                               }
                            

                        /**
                        * field for Contribution
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localContribution ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContributionTracker = false ;

                           public boolean isContributionSpecified(){
                               return localContributionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getContribution(){
                               return localContribution;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Contribution
                               */
                               public void setContribution(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localContributionTracker = param != null;
                                   
                                            this.localContribution=param;
                                    

                               }
                            

                        /**
                        * field for Currency
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrencyTracker = false ;

                           public boolean isCurrencySpecified(){
                               return localCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCurrency(){
                               return localCurrency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Currency
                               */
                               public void setCurrency(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCurrencyTracker = param != null;
                                   
                                            this.localCurrency=param;
                                    

                               }
                            

                        /**
                        * field for CustType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCustType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustTypeTracker = false ;

                           public boolean isCustTypeSpecified(){
                               return localCustTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCustType(){
                               return localCustType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustType
                               */
                               public void setCustType(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCustTypeTracker = param != null;
                                   
                                            this.localCustType=param;
                                    

                               }
                            

                        /**
                        * field for DateCreated
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localDateCreated ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDateCreatedTracker = false ;

                           public boolean isDateCreatedSpecified(){
                               return localDateCreatedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getDateCreated(){
                               return localDateCreated;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DateCreated
                               */
                               public void setDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localDateCreatedTracker = param != null;
                                   
                                            this.localDateCreated=param;
                                    

                               }
                            

                        /**
                        * field for DaysOpen
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localDaysOpen ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDaysOpenTracker = false ;

                           public boolean isDaysOpenSpecified(){
                               return localDaysOpenTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getDaysOpen(){
                               return localDaysOpen;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DaysOpen
                               */
                               public void setDaysOpen(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localDaysOpenTracker = param != null;
                                   
                                            this.localDaysOpen=param;
                                    

                               }
                            

                        /**
                        * field for DaysToClose
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localDaysToClose ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDaysToCloseTracker = false ;

                           public boolean isDaysToCloseSpecified(){
                               return localDaysToCloseTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getDaysToClose(){
                               return localDaysToClose;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DaysToClose
                               */
                               public void setDaysToClose(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localDaysToCloseTracker = param != null;
                                   
                                            this.localDaysToClose=param;
                                    

                               }
                            

                        /**
                        * field for Department
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getDepartment(){
                               return localDepartment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Department
                               */
                               public void setDepartment(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localDepartmentTracker = param != null;
                                   
                                            this.localDepartment=param;
                                    

                               }
                            

                        /**
                        * field for Entity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localEntity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityTracker = false ;

                           public boolean isEntitySpecified(){
                               return localEntityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getEntity(){
                               return localEntity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Entity
                               */
                               public void setEntity(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localEntityTracker = param != null;
                                   
                                            this.localEntity=param;
                                    

                               }
                            

                        /**
                        * field for EntityStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localEntityStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityStatusTracker = false ;

                           public boolean isEntityStatusSpecified(){
                               return localEntityStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getEntityStatus(){
                               return localEntityStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EntityStatus
                               */
                               public void setEntityStatus(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localEntityStatusTracker = param != null;
                                   
                                            this.localEntityStatus=param;
                                    

                               }
                            

                        /**
                        * field for EstimatedBudget
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localEstimatedBudget ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstimatedBudgetTracker = false ;

                           public boolean isEstimatedBudgetSpecified(){
                               return localEstimatedBudgetTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getEstimatedBudget(){
                               return localEstimatedBudget;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstimatedBudget
                               */
                               public void setEstimatedBudget(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localEstimatedBudgetTracker = param != null;
                                   
                                            this.localEstimatedBudget=param;
                                    

                               }
                            

                        /**
                        * field for ExpectedCloseDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localExpectedCloseDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExpectedCloseDateTracker = false ;

                           public boolean isExpectedCloseDateSpecified(){
                               return localExpectedCloseDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getExpectedCloseDate(){
                               return localExpectedCloseDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExpectedCloseDate
                               */
                               public void setExpectedCloseDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localExpectedCloseDateTracker = param != null;
                                   
                                            this.localExpectedCloseDate=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localExternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdTracker = false ;

                           public boolean isExternalIdSpecified(){
                               return localExternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localExternalIdTracker = param != null;
                                   
                                            this.localExternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalIdString
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localExternalIdString ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdStringTracker = false ;

                           public boolean isExternalIdStringSpecified(){
                               return localExternalIdStringTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getExternalIdString(){
                               return localExternalIdString;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalIdString
                               */
                               public void setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localExternalIdStringTracker = param != null;
                                   
                                            this.localExternalIdString=param;
                                    

                               }
                            

                        /**
                        * field for ForecastType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localForecastType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localForecastTypeTracker = false ;

                           public boolean isForecastTypeSpecified(){
                               return localForecastTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getForecastType(){
                               return localForecastType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ForecastType
                               */
                               public void setForecastType(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localForecastTypeTracker = param != null;
                                   
                                            this.localForecastType=param;
                                    

                               }
                            

                        /**
                        * field for ForeignProjectedAmount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localForeignProjectedAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localForeignProjectedAmountTracker = false ;

                           public boolean isForeignProjectedAmountSpecified(){
                               return localForeignProjectedAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getForeignProjectedAmount(){
                               return localForeignProjectedAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ForeignProjectedAmount
                               */
                               public void setForeignProjectedAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localForeignProjectedAmountTracker = param != null;
                                   
                                            this.localForeignProjectedAmount=param;
                                    

                               }
                            

                        /**
                        * field for ForeignRangeHigh
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localForeignRangeHigh ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localForeignRangeHighTracker = false ;

                           public boolean isForeignRangeHighSpecified(){
                               return localForeignRangeHighTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getForeignRangeHigh(){
                               return localForeignRangeHigh;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ForeignRangeHigh
                               */
                               public void setForeignRangeHigh(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localForeignRangeHighTracker = param != null;
                                   
                                            this.localForeignRangeHigh=param;
                                    

                               }
                            

                        /**
                        * field for ForeignRangeLow
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localForeignRangeLow ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localForeignRangeLowTracker = false ;

                           public boolean isForeignRangeLowSpecified(){
                               return localForeignRangeLowTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getForeignRangeLow(){
                               return localForeignRangeLow;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ForeignRangeLow
                               */
                               public void setForeignRangeLow(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localForeignRangeLowTracker = param != null;
                                   
                                            this.localForeignRangeLow=param;
                                    

                               }
                            

                        /**
                        * field for FxTranCostEstimate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localFxTranCostEstimate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFxTranCostEstimateTracker = false ;

                           public boolean isFxTranCostEstimateSpecified(){
                               return localFxTranCostEstimateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getFxTranCostEstimate(){
                               return localFxTranCostEstimate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FxTranCostEstimate
                               */
                               public void setFxTranCostEstimate(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localFxTranCostEstimateTracker = param != null;
                                   
                                            this.localFxTranCostEstimate=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localInternalIdTracker = param != null;
                                   
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for InternalIdNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localInternalIdNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdNumberTracker = false ;

                           public boolean isInternalIdNumberSpecified(){
                               return localInternalIdNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getInternalIdNumber(){
                               return localInternalIdNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalIdNumber
                               */
                               public void setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localInternalIdNumberTracker = param != null;
                                   
                                            this.localInternalIdNumber=param;
                                    

                               }
                            

                        /**
                        * field for IsBudgetApproved
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsBudgetApproved ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsBudgetApprovedTracker = false ;

                           public boolean isIsBudgetApprovedSpecified(){
                               return localIsBudgetApprovedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsBudgetApproved(){
                               return localIsBudgetApproved;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsBudgetApproved
                               */
                               public void setIsBudgetApproved(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsBudgetApprovedTracker = param != null;
                                   
                                            this.localIsBudgetApproved=param;
                                    

                               }
                            

                        /**
                        * field for Item
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemTracker = false ;

                           public boolean isItemSpecified(){
                               return localItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getItem(){
                               return localItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Item
                               */
                               public void setItem(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localItemTracker = param != null;
                                   
                                            this.localItem=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for LeadSource
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localLeadSource ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLeadSourceTracker = false ;

                           public boolean isLeadSourceSpecified(){
                               return localLeadSourceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getLeadSource(){
                               return localLeadSource;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LeadSource
                               */
                               public void setLeadSource(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localLeadSourceTracker = param != null;
                                   
                                            this.localLeadSource=param;
                                    

                               }
                            

                        /**
                        * field for Location
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for Memo
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localMemo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMemoTracker = false ;

                           public boolean isMemoSpecified(){
                               return localMemoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getMemo(){
                               return localMemo;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Memo
                               */
                               public void setMemo(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localMemoTracker = param != null;
                                   
                                            this.localMemo=param;
                                    

                               }
                            

                        /**
                        * field for Number
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNumberTracker = false ;

                           public boolean isNumberSpecified(){
                               return localNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getNumber(){
                               return localNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Number
                               */
                               public void setNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localNumberTracker = param != null;
                                   
                                            this.localNumber=param;
                                    

                               }
                            

                        /**
                        * field for Partner
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localPartner ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerTracker = false ;

                           public boolean isPartnerSpecified(){
                               return localPartnerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getPartner(){
                               return localPartner;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Partner
                               */
                               public void setPartner(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localPartnerTracker = param != null;
                                   
                                            this.localPartner=param;
                                    

                               }
                            

                        /**
                        * field for PartnerContribution
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localPartnerContribution ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerContributionTracker = false ;

                           public boolean isPartnerContributionSpecified(){
                               return localPartnerContributionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getPartnerContribution(){
                               return localPartnerContribution;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PartnerContribution
                               */
                               public void setPartnerContribution(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localPartnerContributionTracker = param != null;
                                   
                                            this.localPartnerContribution=param;
                                    

                               }
                            

                        /**
                        * field for PartnerRole
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localPartnerRole ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerRoleTracker = false ;

                           public boolean isPartnerRoleSpecified(){
                               return localPartnerRoleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getPartnerRole(){
                               return localPartnerRole;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PartnerRole
                               */
                               public void setPartnerRole(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localPartnerRoleTracker = param != null;
                                   
                                            this.localPartnerRole=param;
                                    

                               }
                            

                        /**
                        * field for PartnerTeamMember
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localPartnerTeamMember ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerTeamMemberTracker = false ;

                           public boolean isPartnerTeamMemberSpecified(){
                               return localPartnerTeamMemberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getPartnerTeamMember(){
                               return localPartnerTeamMember;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PartnerTeamMember
                               */
                               public void setPartnerTeamMember(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localPartnerTeamMemberTracker = param != null;
                                   
                                            this.localPartnerTeamMember=param;
                                    

                               }
                            

                        /**
                        * field for PostingPeriod
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPostingPeriod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPostingPeriodTracker = false ;

                           public boolean isPostingPeriodSpecified(){
                               return localPostingPeriodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPostingPeriod(){
                               return localPostingPeriod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PostingPeriod
                               */
                               public void setPostingPeriod(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPostingPeriodTracker = param != null;
                                   
                                            this.localPostingPeriod=param;
                                    

                               }
                            

                        /**
                        * field for PostingPeriodRelative
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.PostingPeriodDate localPostingPeriodRelative ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPostingPeriodRelativeTracker = false ;

                           public boolean isPostingPeriodRelativeSpecified(){
                               return localPostingPeriodRelativeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.PostingPeriodDate
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.PostingPeriodDate getPostingPeriodRelative(){
                               return localPostingPeriodRelative;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PostingPeriodRelative
                               */
                               public void setPostingPeriodRelative(com.netsuite.webservices.platform.common_2017_2.types.PostingPeriodDate param){
                            localPostingPeriodRelativeTracker = param != null;
                                   
                                            this.localPostingPeriodRelative=param;
                                    

                               }
                            

                        /**
                        * field for Probability
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localProbability ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProbabilityTracker = false ;

                           public boolean isProbabilitySpecified(){
                               return localProbabilityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getProbability(){
                               return localProbability;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Probability
                               */
                               public void setProbability(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localProbabilityTracker = param != null;
                                   
                                            this.localProbability=param;
                                    

                               }
                            

                        /**
                        * field for ProjAltSalesAmt
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localProjAltSalesAmt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProjAltSalesAmtTracker = false ;

                           public boolean isProjAltSalesAmtSpecified(){
                               return localProjAltSalesAmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getProjAltSalesAmt(){
                               return localProjAltSalesAmt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ProjAltSalesAmt
                               */
                               public void setProjAltSalesAmt(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localProjAltSalesAmtTracker = param != null;
                                   
                                            this.localProjAltSalesAmt=param;
                                    

                               }
                            

                        /**
                        * field for ProjectedTotal
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localProjectedTotal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProjectedTotalTracker = false ;

                           public boolean isProjectedTotalSpecified(){
                               return localProjectedTotalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getProjectedTotal(){
                               return localProjectedTotal;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ProjectedTotal
                               */
                               public void setProjectedTotal(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localProjectedTotalTracker = param != null;
                                   
                                            this.localProjectedTotal=param;
                                    

                               }
                            

                        /**
                        * field for RangeHigh
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localRangeHigh ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRangeHighTracker = false ;

                           public boolean isRangeHighSpecified(){
                               return localRangeHighTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getRangeHigh(){
                               return localRangeHigh;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RangeHigh
                               */
                               public void setRangeHigh(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localRangeHighTracker = param != null;
                                   
                                            this.localRangeHigh=param;
                                    

                               }
                            

                        /**
                        * field for RangeHighAlt
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localRangeHighAlt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRangeHighAltTracker = false ;

                           public boolean isRangeHighAltSpecified(){
                               return localRangeHighAltTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getRangeHighAlt(){
                               return localRangeHighAlt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RangeHighAlt
                               */
                               public void setRangeHighAlt(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localRangeHighAltTracker = param != null;
                                   
                                            this.localRangeHighAlt=param;
                                    

                               }
                            

                        /**
                        * field for RangeLow
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localRangeLow ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRangeLowTracker = false ;

                           public boolean isRangeLowSpecified(){
                               return localRangeLowTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getRangeLow(){
                               return localRangeLow;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RangeLow
                               */
                               public void setRangeLow(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localRangeLowTracker = param != null;
                                   
                                            this.localRangeLow=param;
                                    

                               }
                            

                        /**
                        * field for RangeLowAlt
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localRangeLowAlt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRangeLowAltTracker = false ;

                           public boolean isRangeLowAltSpecified(){
                               return localRangeLowAltTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getRangeLowAlt(){
                               return localRangeLowAlt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RangeLowAlt
                               */
                               public void setRangeLowAlt(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localRangeLowAltTracker = param != null;
                                   
                                            this.localRangeLowAlt=param;
                                    

                               }
                            

                        /**
                        * field for SalesReadiness
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSalesReadiness ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesReadinessTracker = false ;

                           public boolean isSalesReadinessSpecified(){
                               return localSalesReadinessTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSalesReadiness(){
                               return localSalesReadiness;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesReadiness
                               */
                               public void setSalesReadiness(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSalesReadinessTracker = param != null;
                                   
                                            this.localSalesReadiness=param;
                                    

                               }
                            

                        /**
                        * field for SalesRep
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSalesRep ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesRepTracker = false ;

                           public boolean isSalesRepSpecified(){
                               return localSalesRepTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSalesRep(){
                               return localSalesRep;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesRep
                               */
                               public void setSalesRep(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSalesRepTracker = param != null;
                                   
                                            this.localSalesRep=param;
                                    

                               }
                            

                        /**
                        * field for SalesTeamMember
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSalesTeamMember ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesTeamMemberTracker = false ;

                           public boolean isSalesTeamMemberSpecified(){
                               return localSalesTeamMemberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSalesTeamMember(){
                               return localSalesTeamMember;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesTeamMember
                               */
                               public void setSalesTeamMember(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSalesTeamMemberTracker = param != null;
                                   
                                            this.localSalesTeamMember=param;
                                    

                               }
                            

                        /**
                        * field for SalesTeamRole
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSalesTeamRole ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesTeamRoleTracker = false ;

                           public boolean isSalesTeamRoleSpecified(){
                               return localSalesTeamRoleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSalesTeamRole(){
                               return localSalesTeamRole;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SalesTeamRole
                               */
                               public void setSalesTeamRole(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSalesTeamRoleTracker = param != null;
                                   
                                            this.localSalesTeamRole=param;
                                    

                               }
                            

                        /**
                        * field for Status
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStatusTracker = false ;

                           public boolean isStatusSpecified(){
                               return localStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getStatus(){
                               return localStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Status
                               */
                               public void setStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localStatusTracker = param != null;
                                   
                                            this.localStatus=param;
                                    

                               }
                            

                        /**
                        * field for Subsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Subsidiary
                               */
                               public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSubsidiaryTracker = param != null;
                                   
                                            this.localSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for TaxPeriod
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTaxPeriod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxPeriodTracker = false ;

                           public boolean isTaxPeriodSpecified(){
                               return localTaxPeriodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTaxPeriod(){
                               return localTaxPeriod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxPeriod
                               */
                               public void setTaxPeriod(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTaxPeriodTracker = param != null;
                                   
                                            this.localTaxPeriod=param;
                                    

                               }
                            

                        /**
                        * field for TaxPeriodRelative
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.PostingPeriodDate localTaxPeriodRelative ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxPeriodRelativeTracker = false ;

                           public boolean isTaxPeriodRelativeSpecified(){
                               return localTaxPeriodRelativeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.PostingPeriodDate
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.PostingPeriodDate getTaxPeriodRelative(){
                               return localTaxPeriodRelative;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TaxPeriodRelative
                               */
                               public void setTaxPeriodRelative(com.netsuite.webservices.platform.common_2017_2.types.PostingPeriodDate param){
                            localTaxPeriodRelativeTracker = param != null;
                                   
                                            this.localTaxPeriodRelative=param;
                                    

                               }
                            

                        /**
                        * field for Title
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTitleTracker = false ;

                           public boolean isTitleSpecified(){
                               return localTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getTitle(){
                               return localTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Title
                               */
                               public void setTitle(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localTitleTracker = param != null;
                                   
                                            this.localTitle=param;
                                    

                               }
                            

                        /**
                        * field for TranCostEstimate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localTranCostEstimate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranCostEstimateTracker = false ;

                           public boolean isTranCostEstimateSpecified(){
                               return localTranCostEstimateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getTranCostEstimate(){
                               return localTranCostEstimate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranCostEstimate
                               */
                               public void setTranCostEstimate(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localTranCostEstimateTracker = param != null;
                                   
                                            this.localTranCostEstimate=param;
                                    

                               }
                            

                        /**
                        * field for TranDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localTranDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranDateTracker = false ;

                           public boolean isTranDateSpecified(){
                               return localTranDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getTranDate(){
                               return localTranDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranDate
                               */
                               public void setTranDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localTranDateTracker = param != null;
                                   
                                            this.localTranDate=param;
                                    

                               }
                            

                        /**
                        * field for TranEstGrossProfit
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localTranEstGrossProfit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranEstGrossProfitTracker = false ;

                           public boolean isTranEstGrossProfitSpecified(){
                               return localTranEstGrossProfitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getTranEstGrossProfit(){
                               return localTranEstGrossProfit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranEstGrossProfit
                               */
                               public void setTranEstGrossProfit(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localTranEstGrossProfitTracker = param != null;
                                   
                                            this.localTranEstGrossProfit=param;
                                    

                               }
                            

                        /**
                        * field for TranEstGrossProfitPct
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localTranEstGrossProfitPct ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranEstGrossProfitPctTracker = false ;

                           public boolean isTranEstGrossProfitPctSpecified(){
                               return localTranEstGrossProfitPctTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getTranEstGrossProfitPct(){
                               return localTranEstGrossProfitPct;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranEstGrossProfitPct
                               */
                               public void setTranEstGrossProfitPct(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localTranEstGrossProfitPctTracker = param != null;
                                   
                                            this.localTranEstGrossProfitPct=param;
                                    

                               }
                            

                        /**
                        * field for TranFxEstGrossProfit
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localTranFxEstGrossProfit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranFxEstGrossProfitTracker = false ;

                           public boolean isTranFxEstGrossProfitSpecified(){
                               return localTranFxEstGrossProfitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getTranFxEstGrossProfit(){
                               return localTranFxEstGrossProfit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranFxEstGrossProfit
                               */
                               public void setTranFxEstGrossProfit(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localTranFxEstGrossProfitTracker = param != null;
                                   
                                            this.localTranFxEstGrossProfit=param;
                                    

                               }
                            

                        /**
                        * field for TranId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localTranId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranIdTracker = false ;

                           public boolean isTranIdSpecified(){
                               return localTranIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getTranId(){
                               return localTranId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranId
                               */
                               public void setTranId(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localTranIdTracker = param != null;
                                   
                                            this.localTranId=param;
                                    

                               }
                            

                        /**
                        * field for WinLossReason
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localWinLossReason ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWinLossReasonTracker = false ;

                           public boolean isWinLossReasonSpecified(){
                               return localWinLossReasonTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getWinLossReason(){
                               return localWinLossReason;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param WinLossReason
                               */
                               public void setWinLossReason(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localWinLossReasonTracker = param != null;
                                   
                                            this.localWinLossReason=param;
                                    

                               }
                            

                        /**
                        * field for WonBy
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localWonBy ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWonByTracker = false ;

                           public boolean isWonBySpecified(){
                               return localWonByTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getWonBy(){
                               return localWonBy;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param WonBy
                               */
                               public void setWonBy(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localWonByTracker = param != null;
                                   
                                            this.localWonBy=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":OpportunitySearchBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "OpportunitySearchBasic",
                           xmlWriter);
                   }

                if (localAmountTracker){
                                            if (localAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("amount cannot be null!!");
                                            }
                                           localAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","amount"),
                                               xmlWriter);
                                        } if (localAvailableOfflineTracker){
                                            if (localAvailableOffline==null){
                                                 throw new org.apache.axis2.databinding.ADBException("availableOffline cannot be null!!");
                                            }
                                           localAvailableOffline.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","availableOffline"),
                                               xmlWriter);
                                        } if (localBuyingReasonTracker){
                                            if (localBuyingReason==null){
                                                 throw new org.apache.axis2.databinding.ADBException("buyingReason cannot be null!!");
                                            }
                                           localBuyingReason.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buyingReason"),
                                               xmlWriter);
                                        } if (localBuyingTimeFrameTracker){
                                            if (localBuyingTimeFrame==null){
                                                 throw new org.apache.axis2.databinding.ADBException("buyingTimeFrame cannot be null!!");
                                            }
                                           localBuyingTimeFrame.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buyingTimeFrame"),
                                               xmlWriter);
                                        } if (local_classTracker){
                                            if (local_class==null){
                                                 throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                            }
                                           local_class.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","class"),
                                               xmlWriter);
                                        } if (localCloseDateTracker){
                                            if (localCloseDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("closeDate cannot be null!!");
                                            }
                                           localCloseDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","closeDate"),
                                               xmlWriter);
                                        } if (localCompetitorTracker){
                                            if (localCompetitor==null){
                                                 throw new org.apache.axis2.databinding.ADBException("competitor cannot be null!!");
                                            }
                                           localCompetitor.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","competitor"),
                                               xmlWriter);
                                        } if (localContributionTracker){
                                            if (localContribution==null){
                                                 throw new org.apache.axis2.databinding.ADBException("contribution cannot be null!!");
                                            }
                                           localContribution.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","contribution"),
                                               xmlWriter);
                                        } if (localCurrencyTracker){
                                            if (localCurrency==null){
                                                 throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                            }
                                           localCurrency.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency"),
                                               xmlWriter);
                                        } if (localCustTypeTracker){
                                            if (localCustType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("custType cannot be null!!");
                                            }
                                           localCustType.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","custType"),
                                               xmlWriter);
                                        } if (localDateCreatedTracker){
                                            if (localDateCreated==null){
                                                 throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                            }
                                           localDateCreated.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated"),
                                               xmlWriter);
                                        } if (localDaysOpenTracker){
                                            if (localDaysOpen==null){
                                                 throw new org.apache.axis2.databinding.ADBException("daysOpen cannot be null!!");
                                            }
                                           localDaysOpen.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","daysOpen"),
                                               xmlWriter);
                                        } if (localDaysToCloseTracker){
                                            if (localDaysToClose==null){
                                                 throw new org.apache.axis2.databinding.ADBException("daysToClose cannot be null!!");
                                            }
                                           localDaysToClose.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","daysToClose"),
                                               xmlWriter);
                                        } if (localDepartmentTracker){
                                            if (localDepartment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                            }
                                           localDepartment.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","department"),
                                               xmlWriter);
                                        } if (localEntityTracker){
                                            if (localEntity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("entity cannot be null!!");
                                            }
                                           localEntity.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entity"),
                                               xmlWriter);
                                        } if (localEntityStatusTracker){
                                            if (localEntityStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("entityStatus cannot be null!!");
                                            }
                                           localEntityStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entityStatus"),
                                               xmlWriter);
                                        } if (localEstimatedBudgetTracker){
                                            if (localEstimatedBudget==null){
                                                 throw new org.apache.axis2.databinding.ADBException("estimatedBudget cannot be null!!");
                                            }
                                           localEstimatedBudget.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedBudget"),
                                               xmlWriter);
                                        } if (localExpectedCloseDateTracker){
                                            if (localExpectedCloseDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("expectedCloseDate cannot be null!!");
                                            }
                                           localExpectedCloseDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","expectedCloseDate"),
                                               xmlWriter);
                                        } if (localExternalIdTracker){
                                            if (localExternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                            }
                                           localExternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId"),
                                               xmlWriter);
                                        } if (localExternalIdStringTracker){
                                            if (localExternalIdString==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                            }
                                           localExternalIdString.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString"),
                                               xmlWriter);
                                        } if (localForecastTypeTracker){
                                            if (localForecastType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("forecastType cannot be null!!");
                                            }
                                           localForecastType.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","forecastType"),
                                               xmlWriter);
                                        } if (localForeignProjectedAmountTracker){
                                            if (localForeignProjectedAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("foreignProjectedAmount cannot be null!!");
                                            }
                                           localForeignProjectedAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","foreignProjectedAmount"),
                                               xmlWriter);
                                        } if (localForeignRangeHighTracker){
                                            if (localForeignRangeHigh==null){
                                                 throw new org.apache.axis2.databinding.ADBException("foreignRangeHigh cannot be null!!");
                                            }
                                           localForeignRangeHigh.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","foreignRangeHigh"),
                                               xmlWriter);
                                        } if (localForeignRangeLowTracker){
                                            if (localForeignRangeLow==null){
                                                 throw new org.apache.axis2.databinding.ADBException("foreignRangeLow cannot be null!!");
                                            }
                                           localForeignRangeLow.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","foreignRangeLow"),
                                               xmlWriter);
                                        } if (localFxTranCostEstimateTracker){
                                            if (localFxTranCostEstimate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fxTranCostEstimate cannot be null!!");
                                            }
                                           localFxTranCostEstimate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxTranCostEstimate"),
                                               xmlWriter);
                                        } if (localInternalIdTracker){
                                            if (localInternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                            }
                                           localInternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                               xmlWriter);
                                        } if (localInternalIdNumberTracker){
                                            if (localInternalIdNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                            }
                                           localInternalIdNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber"),
                                               xmlWriter);
                                        } if (localIsBudgetApprovedTracker){
                                            if (localIsBudgetApproved==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isBudgetApproved cannot be null!!");
                                            }
                                           localIsBudgetApproved.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isBudgetApproved"),
                                               xmlWriter);
                                        } if (localItemTracker){
                                            if (localItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                            }
                                           localItem.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","item"),
                                               xmlWriter);
                                        } if (localLastModifiedDateTracker){
                                            if (localLastModifiedDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                            }
                                           localLastModifiedDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate"),
                                               xmlWriter);
                                        } if (localLeadSourceTracker){
                                            if (localLeadSource==null){
                                                 throw new org.apache.axis2.databinding.ADBException("leadSource cannot be null!!");
                                            }
                                           localLeadSource.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","leadSource"),
                                               xmlWriter);
                                        } if (localLocationTracker){
                                            if (localLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                            }
                                           localLocation.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","location"),
                                               xmlWriter);
                                        } if (localMemoTracker){
                                            if (localMemo==null){
                                                 throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                            }
                                           localMemo.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","memo"),
                                               xmlWriter);
                                        } if (localNumberTracker){
                                            if (localNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("number cannot be null!!");
                                            }
                                           localNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","number"),
                                               xmlWriter);
                                        } if (localPartnerTracker){
                                            if (localPartner==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partner cannot be null!!");
                                            }
                                           localPartner.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partner"),
                                               xmlWriter);
                                        } if (localPartnerContributionTracker){
                                            if (localPartnerContribution==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partnerContribution cannot be null!!");
                                            }
                                           localPartnerContribution.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerContribution"),
                                               xmlWriter);
                                        } if (localPartnerRoleTracker){
                                            if (localPartnerRole==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partnerRole cannot be null!!");
                                            }
                                           localPartnerRole.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerRole"),
                                               xmlWriter);
                                        } if (localPartnerTeamMemberTracker){
                                            if (localPartnerTeamMember==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partnerTeamMember cannot be null!!");
                                            }
                                           localPartnerTeamMember.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerTeamMember"),
                                               xmlWriter);
                                        } if (localPostingPeriodTracker){
                                            if (localPostingPeriod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("postingPeriod cannot be null!!");
                                            }
                                           localPostingPeriod.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","postingPeriod"),
                                               xmlWriter);
                                        } if (localPostingPeriodRelativeTracker){
                                            if (localPostingPeriodRelative==null){
                                                 throw new org.apache.axis2.databinding.ADBException("postingPeriodRelative cannot be null!!");
                                            }
                                           localPostingPeriodRelative.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","postingPeriodRelative"),
                                               xmlWriter);
                                        } if (localProbabilityTracker){
                                            if (localProbability==null){
                                                 throw new org.apache.axis2.databinding.ADBException("probability cannot be null!!");
                                            }
                                           localProbability.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","probability"),
                                               xmlWriter);
                                        } if (localProjAltSalesAmtTracker){
                                            if (localProjAltSalesAmt==null){
                                                 throw new org.apache.axis2.databinding.ADBException("projAltSalesAmt cannot be null!!");
                                            }
                                           localProjAltSalesAmt.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","projAltSalesAmt"),
                                               xmlWriter);
                                        } if (localProjectedTotalTracker){
                                            if (localProjectedTotal==null){
                                                 throw new org.apache.axis2.databinding.ADBException("projectedTotal cannot be null!!");
                                            }
                                           localProjectedTotal.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","projectedTotal"),
                                               xmlWriter);
                                        } if (localRangeHighTracker){
                                            if (localRangeHigh==null){
                                                 throw new org.apache.axis2.databinding.ADBException("rangeHigh cannot be null!!");
                                            }
                                           localRangeHigh.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeHigh"),
                                               xmlWriter);
                                        } if (localRangeHighAltTracker){
                                            if (localRangeHighAlt==null){
                                                 throw new org.apache.axis2.databinding.ADBException("rangeHighAlt cannot be null!!");
                                            }
                                           localRangeHighAlt.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeHighAlt"),
                                               xmlWriter);
                                        } if (localRangeLowTracker){
                                            if (localRangeLow==null){
                                                 throw new org.apache.axis2.databinding.ADBException("rangeLow cannot be null!!");
                                            }
                                           localRangeLow.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeLow"),
                                               xmlWriter);
                                        } if (localRangeLowAltTracker){
                                            if (localRangeLowAlt==null){
                                                 throw new org.apache.axis2.databinding.ADBException("rangeLowAlt cannot be null!!");
                                            }
                                           localRangeLowAlt.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeLowAlt"),
                                               xmlWriter);
                                        } if (localSalesReadinessTracker){
                                            if (localSalesReadiness==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesReadiness cannot be null!!");
                                            }
                                           localSalesReadiness.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesReadiness"),
                                               xmlWriter);
                                        } if (localSalesRepTracker){
                                            if (localSalesRep==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesRep cannot be null!!");
                                            }
                                           localSalesRep.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesRep"),
                                               xmlWriter);
                                        } if (localSalesTeamMemberTracker){
                                            if (localSalesTeamMember==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesTeamMember cannot be null!!");
                                            }
                                           localSalesTeamMember.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesTeamMember"),
                                               xmlWriter);
                                        } if (localSalesTeamRoleTracker){
                                            if (localSalesTeamRole==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salesTeamRole cannot be null!!");
                                            }
                                           localSalesTeamRole.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesTeamRole"),
                                               xmlWriter);
                                        } if (localStatusTracker){
                                            if (localStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                            }
                                           localStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","status"),
                                               xmlWriter);
                                        } if (localSubsidiaryTracker){
                                            if (localSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                            }
                                           localSubsidiary.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary"),
                                               xmlWriter);
                                        } if (localTaxPeriodTracker){
                                            if (localTaxPeriod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxPeriod cannot be null!!");
                                            }
                                           localTaxPeriod.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","taxPeriod"),
                                               xmlWriter);
                                        } if (localTaxPeriodRelativeTracker){
                                            if (localTaxPeriodRelative==null){
                                                 throw new org.apache.axis2.databinding.ADBException("taxPeriodRelative cannot be null!!");
                                            }
                                           localTaxPeriodRelative.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","taxPeriodRelative"),
                                               xmlWriter);
                                        } if (localTitleTracker){
                                            if (localTitle==null){
                                                 throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                            }
                                           localTitle.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","title"),
                                               xmlWriter);
                                        } if (localTranCostEstimateTracker){
                                            if (localTranCostEstimate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("tranCostEstimate cannot be null!!");
                                            }
                                           localTranCostEstimate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranCostEstimate"),
                                               xmlWriter);
                                        } if (localTranDateTracker){
                                            if (localTranDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("tranDate cannot be null!!");
                                            }
                                           localTranDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranDate"),
                                               xmlWriter);
                                        } if (localTranEstGrossProfitTracker){
                                            if (localTranEstGrossProfit==null){
                                                 throw new org.apache.axis2.databinding.ADBException("tranEstGrossProfit cannot be null!!");
                                            }
                                           localTranEstGrossProfit.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranEstGrossProfit"),
                                               xmlWriter);
                                        } if (localTranEstGrossProfitPctTracker){
                                            if (localTranEstGrossProfitPct==null){
                                                 throw new org.apache.axis2.databinding.ADBException("tranEstGrossProfitPct cannot be null!!");
                                            }
                                           localTranEstGrossProfitPct.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranEstGrossProfitPct"),
                                               xmlWriter);
                                        } if (localTranFxEstGrossProfitTracker){
                                            if (localTranFxEstGrossProfit==null){
                                                 throw new org.apache.axis2.databinding.ADBException("tranFxEstGrossProfit cannot be null!!");
                                            }
                                           localTranFxEstGrossProfit.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranFxEstGrossProfit"),
                                               xmlWriter);
                                        } if (localTranIdTracker){
                                            if (localTranId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("tranId cannot be null!!");
                                            }
                                           localTranId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranId"),
                                               xmlWriter);
                                        } if (localWinLossReasonTracker){
                                            if (localWinLossReason==null){
                                                 throw new org.apache.axis2.databinding.ADBException("winLossReason cannot be null!!");
                                            }
                                           localWinLossReason.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","winLossReason"),
                                               xmlWriter);
                                        } if (localWonByTracker){
                                            if (localWonBy==null){
                                                 throw new org.apache.axis2.databinding.ADBException("wonBy cannot be null!!");
                                            }
                                           localWonBy.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","wonBy"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","OpportunitySearchBasic"));
                 if (localAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "amount"));
                            
                            
                                    if (localAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("amount cannot be null!!");
                                    }
                                    elementList.add(localAmount);
                                } if (localAvailableOfflineTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "availableOffline"));
                            
                            
                                    if (localAvailableOffline==null){
                                         throw new org.apache.axis2.databinding.ADBException("availableOffline cannot be null!!");
                                    }
                                    elementList.add(localAvailableOffline);
                                } if (localBuyingReasonTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "buyingReason"));
                            
                            
                                    if (localBuyingReason==null){
                                         throw new org.apache.axis2.databinding.ADBException("buyingReason cannot be null!!");
                                    }
                                    elementList.add(localBuyingReason);
                                } if (localBuyingTimeFrameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "buyingTimeFrame"));
                            
                            
                                    if (localBuyingTimeFrame==null){
                                         throw new org.apache.axis2.databinding.ADBException("buyingTimeFrame cannot be null!!");
                                    }
                                    elementList.add(localBuyingTimeFrame);
                                } if (local_classTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "class"));
                            
                            
                                    if (local_class==null){
                                         throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    }
                                    elementList.add(local_class);
                                } if (localCloseDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "closeDate"));
                            
                            
                                    if (localCloseDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("closeDate cannot be null!!");
                                    }
                                    elementList.add(localCloseDate);
                                } if (localCompetitorTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "competitor"));
                            
                            
                                    if (localCompetitor==null){
                                         throw new org.apache.axis2.databinding.ADBException("competitor cannot be null!!");
                                    }
                                    elementList.add(localCompetitor);
                                } if (localContributionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "contribution"));
                            
                            
                                    if (localContribution==null){
                                         throw new org.apache.axis2.databinding.ADBException("contribution cannot be null!!");
                                    }
                                    elementList.add(localContribution);
                                } if (localCurrencyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "currency"));
                            
                            
                                    if (localCurrency==null){
                                         throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                    }
                                    elementList.add(localCurrency);
                                } if (localCustTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "custType"));
                            
                            
                                    if (localCustType==null){
                                         throw new org.apache.axis2.databinding.ADBException("custType cannot be null!!");
                                    }
                                    elementList.add(localCustType);
                                } if (localDateCreatedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "dateCreated"));
                            
                            
                                    if (localDateCreated==null){
                                         throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                    }
                                    elementList.add(localDateCreated);
                                } if (localDaysOpenTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "daysOpen"));
                            
                            
                                    if (localDaysOpen==null){
                                         throw new org.apache.axis2.databinding.ADBException("daysOpen cannot be null!!");
                                    }
                                    elementList.add(localDaysOpen);
                                } if (localDaysToCloseTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "daysToClose"));
                            
                            
                                    if (localDaysToClose==null){
                                         throw new org.apache.axis2.databinding.ADBException("daysToClose cannot be null!!");
                                    }
                                    elementList.add(localDaysToClose);
                                } if (localDepartmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "department"));
                            
                            
                                    if (localDepartment==null){
                                         throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    }
                                    elementList.add(localDepartment);
                                } if (localEntityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "entity"));
                            
                            
                                    if (localEntity==null){
                                         throw new org.apache.axis2.databinding.ADBException("entity cannot be null!!");
                                    }
                                    elementList.add(localEntity);
                                } if (localEntityStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "entityStatus"));
                            
                            
                                    if (localEntityStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("entityStatus cannot be null!!");
                                    }
                                    elementList.add(localEntityStatus);
                                } if (localEstimatedBudgetTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "estimatedBudget"));
                            
                            
                                    if (localEstimatedBudget==null){
                                         throw new org.apache.axis2.databinding.ADBException("estimatedBudget cannot be null!!");
                                    }
                                    elementList.add(localEstimatedBudget);
                                } if (localExpectedCloseDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "expectedCloseDate"));
                            
                            
                                    if (localExpectedCloseDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("expectedCloseDate cannot be null!!");
                                    }
                                    elementList.add(localExpectedCloseDate);
                                } if (localExternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalId"));
                            
                            
                                    if (localExternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                    }
                                    elementList.add(localExternalId);
                                } if (localExternalIdStringTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalIdString"));
                            
                            
                                    if (localExternalIdString==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                    }
                                    elementList.add(localExternalIdString);
                                } if (localForecastTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "forecastType"));
                            
                            
                                    if (localForecastType==null){
                                         throw new org.apache.axis2.databinding.ADBException("forecastType cannot be null!!");
                                    }
                                    elementList.add(localForecastType);
                                } if (localForeignProjectedAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "foreignProjectedAmount"));
                            
                            
                                    if (localForeignProjectedAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("foreignProjectedAmount cannot be null!!");
                                    }
                                    elementList.add(localForeignProjectedAmount);
                                } if (localForeignRangeHighTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "foreignRangeHigh"));
                            
                            
                                    if (localForeignRangeHigh==null){
                                         throw new org.apache.axis2.databinding.ADBException("foreignRangeHigh cannot be null!!");
                                    }
                                    elementList.add(localForeignRangeHigh);
                                } if (localForeignRangeLowTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "foreignRangeLow"));
                            
                            
                                    if (localForeignRangeLow==null){
                                         throw new org.apache.axis2.databinding.ADBException("foreignRangeLow cannot be null!!");
                                    }
                                    elementList.add(localForeignRangeLow);
                                } if (localFxTranCostEstimateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "fxTranCostEstimate"));
                            
                            
                                    if (localFxTranCostEstimate==null){
                                         throw new org.apache.axis2.databinding.ADBException("fxTranCostEstimate cannot be null!!");
                                    }
                                    elementList.add(localFxTranCostEstimate);
                                } if (localInternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalId"));
                            
                            
                                    if (localInternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    }
                                    elementList.add(localInternalId);
                                } if (localInternalIdNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalIdNumber"));
                            
                            
                                    if (localInternalIdNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                    }
                                    elementList.add(localInternalIdNumber);
                                } if (localIsBudgetApprovedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isBudgetApproved"));
                            
                            
                                    if (localIsBudgetApproved==null){
                                         throw new org.apache.axis2.databinding.ADBException("isBudgetApproved cannot be null!!");
                                    }
                                    elementList.add(localIsBudgetApproved);
                                } if (localItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "item"));
                            
                            
                                    if (localItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                    }
                                    elementList.add(localItem);
                                } if (localLastModifiedDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                            
                            
                                    if (localLastModifiedDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                    }
                                    elementList.add(localLastModifiedDate);
                                } if (localLeadSourceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "leadSource"));
                            
                            
                                    if (localLeadSource==null){
                                         throw new org.apache.axis2.databinding.ADBException("leadSource cannot be null!!");
                                    }
                                    elementList.add(localLeadSource);
                                } if (localLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "location"));
                            
                            
                                    if (localLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    }
                                    elementList.add(localLocation);
                                } if (localMemoTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "memo"));
                            
                            
                                    if (localMemo==null){
                                         throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                    }
                                    elementList.add(localMemo);
                                } if (localNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "number"));
                            
                            
                                    if (localNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("number cannot be null!!");
                                    }
                                    elementList.add(localNumber);
                                } if (localPartnerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "partner"));
                            
                            
                                    if (localPartner==null){
                                         throw new org.apache.axis2.databinding.ADBException("partner cannot be null!!");
                                    }
                                    elementList.add(localPartner);
                                } if (localPartnerContributionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "partnerContribution"));
                            
                            
                                    if (localPartnerContribution==null){
                                         throw new org.apache.axis2.databinding.ADBException("partnerContribution cannot be null!!");
                                    }
                                    elementList.add(localPartnerContribution);
                                } if (localPartnerRoleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "partnerRole"));
                            
                            
                                    if (localPartnerRole==null){
                                         throw new org.apache.axis2.databinding.ADBException("partnerRole cannot be null!!");
                                    }
                                    elementList.add(localPartnerRole);
                                } if (localPartnerTeamMemberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "partnerTeamMember"));
                            
                            
                                    if (localPartnerTeamMember==null){
                                         throw new org.apache.axis2.databinding.ADBException("partnerTeamMember cannot be null!!");
                                    }
                                    elementList.add(localPartnerTeamMember);
                                } if (localPostingPeriodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "postingPeriod"));
                            
                            
                                    if (localPostingPeriod==null){
                                         throw new org.apache.axis2.databinding.ADBException("postingPeriod cannot be null!!");
                                    }
                                    elementList.add(localPostingPeriod);
                                } if (localPostingPeriodRelativeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "postingPeriodRelative"));
                            
                            
                                    if (localPostingPeriodRelative==null){
                                         throw new org.apache.axis2.databinding.ADBException("postingPeriodRelative cannot be null!!");
                                    }
                                    elementList.add(localPostingPeriodRelative);
                                } if (localProbabilityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "probability"));
                            
                            
                                    if (localProbability==null){
                                         throw new org.apache.axis2.databinding.ADBException("probability cannot be null!!");
                                    }
                                    elementList.add(localProbability);
                                } if (localProjAltSalesAmtTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "projAltSalesAmt"));
                            
                            
                                    if (localProjAltSalesAmt==null){
                                         throw new org.apache.axis2.databinding.ADBException("projAltSalesAmt cannot be null!!");
                                    }
                                    elementList.add(localProjAltSalesAmt);
                                } if (localProjectedTotalTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "projectedTotal"));
                            
                            
                                    if (localProjectedTotal==null){
                                         throw new org.apache.axis2.databinding.ADBException("projectedTotal cannot be null!!");
                                    }
                                    elementList.add(localProjectedTotal);
                                } if (localRangeHighTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "rangeHigh"));
                            
                            
                                    if (localRangeHigh==null){
                                         throw new org.apache.axis2.databinding.ADBException("rangeHigh cannot be null!!");
                                    }
                                    elementList.add(localRangeHigh);
                                } if (localRangeHighAltTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "rangeHighAlt"));
                            
                            
                                    if (localRangeHighAlt==null){
                                         throw new org.apache.axis2.databinding.ADBException("rangeHighAlt cannot be null!!");
                                    }
                                    elementList.add(localRangeHighAlt);
                                } if (localRangeLowTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "rangeLow"));
                            
                            
                                    if (localRangeLow==null){
                                         throw new org.apache.axis2.databinding.ADBException("rangeLow cannot be null!!");
                                    }
                                    elementList.add(localRangeLow);
                                } if (localRangeLowAltTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "rangeLowAlt"));
                            
                            
                                    if (localRangeLowAlt==null){
                                         throw new org.apache.axis2.databinding.ADBException("rangeLowAlt cannot be null!!");
                                    }
                                    elementList.add(localRangeLowAlt);
                                } if (localSalesReadinessTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "salesReadiness"));
                            
                            
                                    if (localSalesReadiness==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesReadiness cannot be null!!");
                                    }
                                    elementList.add(localSalesReadiness);
                                } if (localSalesRepTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "salesRep"));
                            
                            
                                    if (localSalesRep==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesRep cannot be null!!");
                                    }
                                    elementList.add(localSalesRep);
                                } if (localSalesTeamMemberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "salesTeamMember"));
                            
                            
                                    if (localSalesTeamMember==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesTeamMember cannot be null!!");
                                    }
                                    elementList.add(localSalesTeamMember);
                                } if (localSalesTeamRoleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "salesTeamRole"));
                            
                            
                                    if (localSalesTeamRole==null){
                                         throw new org.apache.axis2.databinding.ADBException("salesTeamRole cannot be null!!");
                                    }
                                    elementList.add(localSalesTeamRole);
                                } if (localStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "status"));
                            
                            
                                    if (localStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                    }
                                    elementList.add(localStatus);
                                } if (localSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "subsidiary"));
                            
                            
                                    if (localSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    }
                                    elementList.add(localSubsidiary);
                                } if (localTaxPeriodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "taxPeriod"));
                            
                            
                                    if (localTaxPeriod==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxPeriod cannot be null!!");
                                    }
                                    elementList.add(localTaxPeriod);
                                } if (localTaxPeriodRelativeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "taxPeriodRelative"));
                            
                            
                                    if (localTaxPeriodRelative==null){
                                         throw new org.apache.axis2.databinding.ADBException("taxPeriodRelative cannot be null!!");
                                    }
                                    elementList.add(localTaxPeriodRelative);
                                } if (localTitleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "title"));
                            
                            
                                    if (localTitle==null){
                                         throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                    }
                                    elementList.add(localTitle);
                                } if (localTranCostEstimateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "tranCostEstimate"));
                            
                            
                                    if (localTranCostEstimate==null){
                                         throw new org.apache.axis2.databinding.ADBException("tranCostEstimate cannot be null!!");
                                    }
                                    elementList.add(localTranCostEstimate);
                                } if (localTranDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "tranDate"));
                            
                            
                                    if (localTranDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("tranDate cannot be null!!");
                                    }
                                    elementList.add(localTranDate);
                                } if (localTranEstGrossProfitTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "tranEstGrossProfit"));
                            
                            
                                    if (localTranEstGrossProfit==null){
                                         throw new org.apache.axis2.databinding.ADBException("tranEstGrossProfit cannot be null!!");
                                    }
                                    elementList.add(localTranEstGrossProfit);
                                } if (localTranEstGrossProfitPctTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "tranEstGrossProfitPct"));
                            
                            
                                    if (localTranEstGrossProfitPct==null){
                                         throw new org.apache.axis2.databinding.ADBException("tranEstGrossProfitPct cannot be null!!");
                                    }
                                    elementList.add(localTranEstGrossProfitPct);
                                } if (localTranFxEstGrossProfitTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "tranFxEstGrossProfit"));
                            
                            
                                    if (localTranFxEstGrossProfit==null){
                                         throw new org.apache.axis2.databinding.ADBException("tranFxEstGrossProfit cannot be null!!");
                                    }
                                    elementList.add(localTranFxEstGrossProfit);
                                } if (localTranIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "tranId"));
                            
                            
                                    if (localTranId==null){
                                         throw new org.apache.axis2.databinding.ADBException("tranId cannot be null!!");
                                    }
                                    elementList.add(localTranId);
                                } if (localWinLossReasonTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "winLossReason"));
                            
                            
                                    if (localWinLossReason==null){
                                         throw new org.apache.axis2.databinding.ADBException("winLossReason cannot be null!!");
                                    }
                                    elementList.add(localWinLossReason);
                                } if (localWonByTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "wonBy"));
                            
                            
                                    if (localWonBy==null){
                                         throw new org.apache.axis2.databinding.ADBException("wonBy cannot be null!!");
                                    }
                                    elementList.add(localWonBy);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static OpportunitySearchBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            OpportunitySearchBasic object =
                new OpportunitySearchBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"OpportunitySearchBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (OpportunitySearchBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","amount").equals(reader.getName())){
                                
                                                object.setAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","availableOffline").equals(reader.getName())){
                                
                                                object.setAvailableOffline(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buyingReason").equals(reader.getName())){
                                
                                                object.setBuyingReason(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buyingTimeFrame").equals(reader.getName())){
                                
                                                object.setBuyingTimeFrame(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                                object.set_class(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","closeDate").equals(reader.getName())){
                                
                                                object.setCloseDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","competitor").equals(reader.getName())){
                                
                                                object.setCompetitor(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","contribution").equals(reader.getName())){
                                
                                                object.setContribution(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency").equals(reader.getName())){
                                
                                                object.setCurrency(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","custType").equals(reader.getName())){
                                
                                                object.setCustType(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated").equals(reader.getName())){
                                
                                                object.setDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","daysOpen").equals(reader.getName())){
                                
                                                object.setDaysOpen(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","daysToClose").equals(reader.getName())){
                                
                                                object.setDaysToClose(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                                object.setDepartment(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entity").equals(reader.getName())){
                                
                                                object.setEntity(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entityStatus").equals(reader.getName())){
                                
                                                object.setEntityStatus(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedBudget").equals(reader.getName())){
                                
                                                object.setEstimatedBudget(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","expectedCloseDate").equals(reader.getName())){
                                
                                                object.setExpectedCloseDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                
                                                object.setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString").equals(reader.getName())){
                                
                                                object.setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","forecastType").equals(reader.getName())){
                                
                                                object.setForecastType(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","foreignProjectedAmount").equals(reader.getName())){
                                
                                                object.setForeignProjectedAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","foreignRangeHigh").equals(reader.getName())){
                                
                                                object.setForeignRangeHigh(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","foreignRangeLow").equals(reader.getName())){
                                
                                                object.setForeignRangeLow(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxTranCostEstimate").equals(reader.getName())){
                                
                                                object.setFxTranCostEstimate(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                                object.setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber").equals(reader.getName())){
                                
                                                object.setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isBudgetApproved").equals(reader.getName())){
                                
                                                object.setIsBudgetApproved(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","item").equals(reader.getName())){
                                
                                                object.setItem(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                                object.setLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","leadSource").equals(reader.getName())){
                                
                                                object.setLeadSource(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                                object.setLocation(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","memo").equals(reader.getName())){
                                
                                                object.setMemo(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","number").equals(reader.getName())){
                                
                                                object.setNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partner").equals(reader.getName())){
                                
                                                object.setPartner(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerContribution").equals(reader.getName())){
                                
                                                object.setPartnerContribution(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerRole").equals(reader.getName())){
                                
                                                object.setPartnerRole(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerTeamMember").equals(reader.getName())){
                                
                                                object.setPartnerTeamMember(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","postingPeriod").equals(reader.getName())){
                                
                                                object.setPostingPeriod(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","postingPeriodRelative").equals(reader.getName())){
                                
                                                object.setPostingPeriodRelative(com.netsuite.webservices.platform.common_2017_2.types.PostingPeriodDate.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","probability").equals(reader.getName())){
                                
                                                object.setProbability(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","projAltSalesAmt").equals(reader.getName())){
                                
                                                object.setProjAltSalesAmt(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","projectedTotal").equals(reader.getName())){
                                
                                                object.setProjectedTotal(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeHigh").equals(reader.getName())){
                                
                                                object.setRangeHigh(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeHighAlt").equals(reader.getName())){
                                
                                                object.setRangeHighAlt(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeLow").equals(reader.getName())){
                                
                                                object.setRangeLow(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeLowAlt").equals(reader.getName())){
                                
                                                object.setRangeLowAlt(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesReadiness").equals(reader.getName())){
                                
                                                object.setSalesReadiness(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesRep").equals(reader.getName())){
                                
                                                object.setSalesRep(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesTeamMember").equals(reader.getName())){
                                
                                                object.setSalesTeamMember(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesTeamRole").equals(reader.getName())){
                                
                                                object.setSalesTeamRole(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","status").equals(reader.getName())){
                                
                                                object.setStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                                object.setSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","taxPeriod").equals(reader.getName())){
                                
                                                object.setTaxPeriod(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","taxPeriodRelative").equals(reader.getName())){
                                
                                                object.setTaxPeriodRelative(com.netsuite.webservices.platform.common_2017_2.types.PostingPeriodDate.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","title").equals(reader.getName())){
                                
                                                object.setTitle(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranCostEstimate").equals(reader.getName())){
                                
                                                object.setTranCostEstimate(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranDate").equals(reader.getName())){
                                
                                                object.setTranDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranEstGrossProfit").equals(reader.getName())){
                                
                                                object.setTranEstGrossProfit(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranEstGrossProfitPct").equals(reader.getName())){
                                
                                                object.setTranEstGrossProfitPct(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranFxEstGrossProfit").equals(reader.getName())){
                                
                                                object.setTranFxEstGrossProfit(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranId").equals(reader.getName())){
                                
                                                object.setTranId(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","winLossReason").equals(reader.getName())){
                                
                                                object.setWinLossReason(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","wonBy").equals(reader.getName())){
                                
                                                object.setWonBy(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    