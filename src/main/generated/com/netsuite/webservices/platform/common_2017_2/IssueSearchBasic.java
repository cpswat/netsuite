
/**
 * IssueSearchBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  IssueSearchBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class IssueSearchBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRecordBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = IssueSearchBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for AgeInMonths
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localAgeInMonths ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAgeInMonthsTracker = false ;

                           public boolean isAgeInMonthsSpecified(){
                               return localAgeInMonthsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getAgeInMonths(){
                               return localAgeInMonths;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AgeInMonths
                               */
                               public void setAgeInMonths(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localAgeInMonthsTracker = param != null;
                                   
                                            this.localAgeInMonths=param;
                                    

                               }
                            

                        /**
                        * field for Assigned
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localAssigned ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAssignedTracker = false ;

                           public boolean isAssignedSpecified(){
                               return localAssignedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getAssigned(){
                               return localAssigned;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Assigned
                               */
                               public void setAssigned(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localAssignedTracker = param != null;
                                   
                                            this.localAssigned=param;
                                    

                               }
                            

                        /**
                        * field for BuildBroken
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localBuildBroken ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBuildBrokenTracker = false ;

                           public boolean isBuildBrokenSpecified(){
                               return localBuildBrokenTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getBuildBroken(){
                               return localBuildBroken;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BuildBroken
                               */
                               public void setBuildBroken(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localBuildBrokenTracker = param != null;
                                   
                                            this.localBuildBroken=param;
                                    

                               }
                            

                        /**
                        * field for BuildBrokenName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localBuildBrokenName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBuildBrokenNameTracker = false ;

                           public boolean isBuildBrokenNameSpecified(){
                               return localBuildBrokenNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getBuildBrokenName(){
                               return localBuildBrokenName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BuildBrokenName
                               */
                               public void setBuildBrokenName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localBuildBrokenNameTracker = param != null;
                                   
                                            this.localBuildBrokenName=param;
                                    

                               }
                            

                        /**
                        * field for BuildFixed
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localBuildFixed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBuildFixedTracker = false ;

                           public boolean isBuildFixedSpecified(){
                               return localBuildFixedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getBuildFixed(){
                               return localBuildFixed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BuildFixed
                               */
                               public void setBuildFixed(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localBuildFixedTracker = param != null;
                                   
                                            this.localBuildFixed=param;
                                    

                               }
                            

                        /**
                        * field for BuildFixedName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localBuildFixedName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBuildFixedNameTracker = false ;

                           public boolean isBuildFixedNameSpecified(){
                               return localBuildFixedNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getBuildFixedName(){
                               return localBuildFixedName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BuildFixedName
                               */
                               public void setBuildFixedName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localBuildFixedNameTracker = param != null;
                                   
                                            this.localBuildFixedName=param;
                                    

                               }
                            

                        /**
                        * field for BuildTarget
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localBuildTarget ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBuildTargetTracker = false ;

                           public boolean isBuildTargetSpecified(){
                               return localBuildTargetTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getBuildTarget(){
                               return localBuildTarget;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BuildTarget
                               */
                               public void setBuildTarget(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localBuildTargetTracker = param != null;
                                   
                                            this.localBuildTarget=param;
                                    

                               }
                            

                        /**
                        * field for BuildTargetName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localBuildTargetName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBuildTargetNameTracker = false ;

                           public boolean isBuildTargetNameSpecified(){
                               return localBuildTargetNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getBuildTargetName(){
                               return localBuildTargetName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BuildTargetName
                               */
                               public void setBuildTargetName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localBuildTargetNameTracker = param != null;
                                   
                                            this.localBuildTargetName=param;
                                    

                               }
                            

                        /**
                        * field for CaseCount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localCaseCount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCaseCountTracker = false ;

                           public boolean isCaseCountSpecified(){
                               return localCaseCountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getCaseCount(){
                               return localCaseCount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CaseCount
                               */
                               public void setCaseCount(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localCaseCountTracker = param != null;
                                   
                                            this.localCaseCount=param;
                                    

                               }
                            

                        /**
                        * field for CaseNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localCaseNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCaseNumberTracker = false ;

                           public boolean isCaseNumberSpecified(){
                               return localCaseNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getCaseNumber(){
                               return localCaseNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CaseNumber
                               */
                               public void setCaseNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCaseNumberTracker = param != null;
                                   
                                            this.localCaseNumber=param;
                                    

                               }
                            

                        /**
                        * field for ClosedDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localClosedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localClosedDateTracker = false ;

                           public boolean isClosedDateSpecified(){
                               return localClosedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getClosedDate(){
                               return localClosedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ClosedDate
                               */
                               public void setClosedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localClosedDateTracker = param != null;
                                   
                                            this.localClosedDate=param;
                                    

                               }
                            

                        /**
                        * field for CreatedDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localCreatedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedDateTracker = false ;

                           public boolean isCreatedDateSpecified(){
                               return localCreatedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getCreatedDate(){
                               return localCreatedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedDate
                               */
                               public void setCreatedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localCreatedDateTracker = param != null;
                                   
                                            this.localCreatedDate=param;
                                    

                               }
                            

                        /**
                        * field for DateReleased
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localDateReleased ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDateReleasedTracker = false ;

                           public boolean isDateReleasedSpecified(){
                               return localDateReleasedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getDateReleased(){
                               return localDateReleased;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DateReleased
                               */
                               public void setDateReleased(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localDateReleasedTracker = param != null;
                                   
                                            this.localDateReleased=param;
                                    

                               }
                            

                        /**
                        * field for Details
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localDetails ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDetailsTracker = false ;

                           public boolean isDetailsSpecified(){
                               return localDetailsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getDetails(){
                               return localDetails;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Details
                               */
                               public void setDetails(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localDetailsTracker = param != null;
                                   
                                            this.localDetails=param;
                                    

                               }
                            

                        /**
                        * field for DuplicateOf
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localDuplicateOf ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDuplicateOfTracker = false ;

                           public boolean isDuplicateOfSpecified(){
                               return localDuplicateOfTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getDuplicateOf(){
                               return localDuplicateOf;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DuplicateOf
                               */
                               public void setDuplicateOf(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localDuplicateOfTracker = param != null;
                                   
                                            this.localDuplicateOf=param;
                                    

                               }
                            

                        /**
                        * field for EFix
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localEFix ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEFixTracker = false ;

                           public boolean isEFixSpecified(){
                               return localEFixTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getEFix(){
                               return localEFix;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EFix
                               */
                               public void setEFix(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localEFixTracker = param != null;
                                   
                                            this.localEFix=param;
                                    

                               }
                            

                        /**
                        * field for EmployeeOrTeam
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localEmployeeOrTeam ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmployeeOrTeamTracker = false ;

                           public boolean isEmployeeOrTeamSpecified(){
                               return localEmployeeOrTeamTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getEmployeeOrTeam(){
                               return localEmployeeOrTeam;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmployeeOrTeam
                               */
                               public void setEmployeeOrTeam(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localEmployeeOrTeamTracker = param != null;
                                   
                                            this.localEmployeeOrTeam=param;
                                    

                               }
                            

                        /**
                        * field for EventStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localEventStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEventStatusTracker = false ;

                           public boolean isEventStatusSpecified(){
                               return localEventStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getEventStatus(){
                               return localEventStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EventStatus
                               */
                               public void setEventStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localEventStatusTracker = param != null;
                                   
                                            this.localEventStatus=param;
                                    

                               }
                            

                        /**
                        * field for ExternalAbstract
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localExternalAbstract ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalAbstractTracker = false ;

                           public boolean isExternalAbstractSpecified(){
                               return localExternalAbstractTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getExternalAbstract(){
                               return localExternalAbstract;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalAbstract
                               */
                               public void setExternalAbstract(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localExternalAbstractTracker = param != null;
                                   
                                            this.localExternalAbstract=param;
                                    

                               }
                            

                        /**
                        * field for ExternalDetails
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localExternalDetails ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalDetailsTracker = false ;

                           public boolean isExternalDetailsSpecified(){
                               return localExternalDetailsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getExternalDetails(){
                               return localExternalDetails;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalDetails
                               */
                               public void setExternalDetails(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localExternalDetailsTracker = param != null;
                                   
                                            this.localExternalDetails=param;
                                    

                               }
                            

                        /**
                        * field for ExternalFixedIn
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localExternalFixedIn ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalFixedInTracker = false ;

                           public boolean isExternalFixedInSpecified(){
                               return localExternalFixedInTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getExternalFixedIn(){
                               return localExternalFixedIn;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalFixedIn
                               */
                               public void setExternalFixedIn(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localExternalFixedInTracker = param != null;
                                   
                                            this.localExternalFixedIn=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localExternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdTracker = false ;

                           public boolean isExternalIdSpecified(){
                               return localExternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localExternalIdTracker = param != null;
                                   
                                            this.localExternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalIdString
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localExternalIdString ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdStringTracker = false ;

                           public boolean isExternalIdStringSpecified(){
                               return localExternalIdStringTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getExternalIdString(){
                               return localExternalIdString;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalIdString
                               */
                               public void setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localExternalIdStringTracker = param != null;
                                   
                                            this.localExternalIdString=param;
                                    

                               }
                            

                        /**
                        * field for ExternalStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localExternalStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalStatusTracker = false ;

                           public boolean isExternalStatusSpecified(){
                               return localExternalStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getExternalStatus(){
                               return localExternalStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalStatus
                               */
                               public void setExternalStatus(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localExternalStatusTracker = param != null;
                                   
                                            this.localExternalStatus=param;
                                    

                               }
                            

                        /**
                        * field for Fixed
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localFixed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFixedTracker = false ;

                           public boolean isFixedSpecified(){
                               return localFixedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getFixed(){
                               return localFixed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Fixed
                               */
                               public void setFixed(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localFixedTracker = param != null;
                                   
                                            this.localFixed=param;
                                    

                               }
                            

                        /**
                        * field for FixedBy
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localFixedBy ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFixedByTracker = false ;

                           public boolean isFixedBySpecified(){
                               return localFixedByTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getFixedBy(){
                               return localFixedBy;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FixedBy
                               */
                               public void setFixedBy(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localFixedByTracker = param != null;
                                   
                                            this.localFixedBy=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localInternalIdTracker = param != null;
                                   
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for InternalIdNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localInternalIdNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdNumberTracker = false ;

                           public boolean isInternalIdNumberSpecified(){
                               return localInternalIdNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getInternalIdNumber(){
                               return localInternalIdNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalIdNumber
                               */
                               public void setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localInternalIdNumberTracker = param != null;
                                   
                                            this.localInternalIdNumber=param;
                                    

                               }
                            

                        /**
                        * field for IsOwner
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsOwner ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsOwnerTracker = false ;

                           public boolean isIsOwnerSpecified(){
                               return localIsOwnerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsOwner(){
                               return localIsOwner;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsOwner
                               */
                               public void setIsOwner(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsOwnerTracker = param != null;
                                   
                                            this.localIsOwner=param;
                                    

                               }
                            

                        /**
                        * field for IsReviewed
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsReviewed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsReviewedTracker = false ;

                           public boolean isIsReviewedSpecified(){
                               return localIsReviewedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsReviewed(){
                               return localIsReviewed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsReviewed
                               */
                               public void setIsReviewed(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsReviewedTracker = param != null;
                                   
                                            this.localIsReviewed=param;
                                    

                               }
                            

                        /**
                        * field for IsShowStopper
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsShowStopper ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsShowStopperTracker = false ;

                           public boolean isIsShowStopperSpecified(){
                               return localIsShowStopperTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsShowStopper(){
                               return localIsShowStopper;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsShowStopper
                               */
                               public void setIsShowStopper(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsShowStopperTracker = param != null;
                                   
                                            this.localIsShowStopper=param;
                                    

                               }
                            

                        /**
                        * field for IssueAbstract
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localIssueAbstract ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIssueAbstractTracker = false ;

                           public boolean isIssueAbstractSpecified(){
                               return localIssueAbstractTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getIssueAbstract(){
                               return localIssueAbstract;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IssueAbstract
                               */
                               public void setIssueAbstract(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localIssueAbstractTracker = param != null;
                                   
                                            this.localIssueAbstract=param;
                                    

                               }
                            

                        /**
                        * field for IssueNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localIssueNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIssueNumberTracker = false ;

                           public boolean isIssueNumberSpecified(){
                               return localIssueNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getIssueNumber(){
                               return localIssueNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IssueNumber
                               */
                               public void setIssueNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localIssueNumberTracker = param != null;
                                   
                                            this.localIssueNumber=param;
                                    

                               }
                            

                        /**
                        * field for Item
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemTracker = false ;

                           public boolean isItemSpecified(){
                               return localItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getItem(){
                               return localItem;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Item
                               */
                               public void setItem(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localItemTracker = param != null;
                                   
                                            this.localItem=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for Module
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localModule ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localModuleTracker = false ;

                           public boolean isModuleSpecified(){
                               return localModuleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getModule(){
                               return localModule;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Module
                               */
                               public void setModule(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localModuleTracker = param != null;
                                   
                                            this.localModule=param;
                                    

                               }
                            

                        /**
                        * field for Number
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNumberTracker = false ;

                           public boolean isNumberSpecified(){
                               return localNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getNumber(){
                               return localNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Number
                               */
                               public void setNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localNumberTracker = param != null;
                                   
                                            this.localNumber=param;
                                    

                               }
                            

                        /**
                        * field for OriginalFixedIn
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localOriginalFixedIn ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOriginalFixedInTracker = false ;

                           public boolean isOriginalFixedInSpecified(){
                               return localOriginalFixedInTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getOriginalFixedIn(){
                               return localOriginalFixedIn;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OriginalFixedIn
                               */
                               public void setOriginalFixedIn(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localOriginalFixedInTracker = param != null;
                                   
                                            this.localOriginalFixedIn=param;
                                    

                               }
                            

                        /**
                        * field for Priority
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localPriority ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPriorityTracker = false ;

                           public boolean isPrioritySpecified(){
                               return localPriorityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getPriority(){
                               return localPriority;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Priority
                               */
                               public void setPriority(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localPriorityTracker = param != null;
                                   
                                            this.localPriority=param;
                                    

                               }
                            

                        /**
                        * field for Product
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localProduct ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProductTracker = false ;

                           public boolean isProductSpecified(){
                               return localProductTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getProduct(){
                               return localProduct;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Product
                               */
                               public void setProduct(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localProductTracker = param != null;
                                   
                                            this.localProduct=param;
                                    

                               }
                            

                        /**
                        * field for ProductTeam
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localProductTeam ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProductTeamTracker = false ;

                           public boolean isProductTeamSpecified(){
                               return localProductTeamTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getProductTeam(){
                               return localProductTeam;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ProductTeam
                               */
                               public void setProductTeam(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localProductTeamTracker = param != null;
                                   
                                            this.localProductTeam=param;
                                    

                               }
                            

                        /**
                        * field for RelatedIssue
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localRelatedIssue ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRelatedIssueTracker = false ;

                           public boolean isRelatedIssueSpecified(){
                               return localRelatedIssueTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getRelatedIssue(){
                               return localRelatedIssue;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RelatedIssue
                               */
                               public void setRelatedIssue(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localRelatedIssueTracker = param != null;
                                   
                                            this.localRelatedIssue=param;
                                    

                               }
                            

                        /**
                        * field for Relationship
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localRelationship ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRelationshipTracker = false ;

                           public boolean isRelationshipSpecified(){
                               return localRelationshipTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getRelationship(){
                               return localRelationship;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Relationship
                               */
                               public void setRelationship(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localRelationshipTracker = param != null;
                                   
                                            this.localRelationship=param;
                                    

                               }
                            

                        /**
                        * field for RelationshipComment
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localRelationshipComment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRelationshipCommentTracker = false ;

                           public boolean isRelationshipCommentSpecified(){
                               return localRelationshipCommentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getRelationshipComment(){
                               return localRelationshipComment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RelationshipComment
                               */
                               public void setRelationshipComment(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localRelationshipCommentTracker = param != null;
                                   
                                            this.localRelationshipComment=param;
                                    

                               }
                            

                        /**
                        * field for ReportedBy
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localReportedBy ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReportedByTracker = false ;

                           public boolean isReportedBySpecified(){
                               return localReportedByTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getReportedBy(){
                               return localReportedBy;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReportedBy
                               */
                               public void setReportedBy(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localReportedByTracker = param != null;
                                   
                                            this.localReportedBy=param;
                                    

                               }
                            

                        /**
                        * field for Reproduce
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localReproduce ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReproduceTracker = false ;

                           public boolean isReproduceSpecified(){
                               return localReproduceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getReproduce(){
                               return localReproduce;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Reproduce
                               */
                               public void setReproduce(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localReproduceTracker = param != null;
                                   
                                            this.localReproduce=param;
                                    

                               }
                            

                        /**
                        * field for Resolved
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localResolved ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localResolvedTracker = false ;

                           public boolean isResolvedSpecified(){
                               return localResolvedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getResolved(){
                               return localResolved;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Resolved
                               */
                               public void setResolved(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localResolvedTracker = param != null;
                                   
                                            this.localResolved=param;
                                    

                               }
                            

                        /**
                        * field for ResolvedBy
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localResolvedBy ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localResolvedByTracker = false ;

                           public boolean isResolvedBySpecified(){
                               return localResolvedByTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getResolvedBy(){
                               return localResolvedBy;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ResolvedBy
                               */
                               public void setResolvedBy(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localResolvedByTracker = param != null;
                                   
                                            this.localResolvedBy=param;
                                    

                               }
                            

                        /**
                        * field for Reviewer
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localReviewer ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReviewerTracker = false ;

                           public boolean isReviewerSpecified(){
                               return localReviewerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getReviewer(){
                               return localReviewer;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Reviewer
                               */
                               public void setReviewer(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localReviewerTracker = param != null;
                                   
                                            this.localReviewer=param;
                                    

                               }
                            

                        /**
                        * field for Severity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSeverity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSeverityTracker = false ;

                           public boolean isSeveritySpecified(){
                               return localSeverityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSeverity(){
                               return localSeverity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Severity
                               */
                               public void setSeverity(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSeverityTracker = param != null;
                                   
                                            this.localSeverity=param;
                                    

                               }
                            

                        /**
                        * field for Source
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localSource ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSourceTracker = false ;

                           public boolean isSourceSpecified(){
                               return localSourceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getSource(){
                               return localSource;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Source
                               */
                               public void setSource(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localSourceTracker = param != null;
                                   
                                            this.localSource=param;
                                    

                               }
                            

                        /**
                        * field for Status
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStatusTracker = false ;

                           public boolean isStatusSpecified(){
                               return localStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getStatus(){
                               return localStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Status
                               */
                               public void setStatus(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localStatusTracker = param != null;
                                   
                                            this.localStatus=param;
                                    

                               }
                            

                        /**
                        * field for Tags
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localTags ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTagsTracker = false ;

                           public boolean isTagsSpecified(){
                               return localTagsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getTags(){
                               return localTags;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Tags
                               */
                               public void setTags(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localTagsTracker = param != null;
                                   
                                            this.localTags=param;
                                    

                               }
                            

                        /**
                        * field for Tracking
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localTracking ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTrackingTracker = false ;

                           public boolean isTrackingSpecified(){
                               return localTrackingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getTracking(){
                               return localTracking;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Tracking
                               */
                               public void setTracking(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localTrackingTracker = param != null;
                                   
                                            this.localTracking=param;
                                    

                               }
                            

                        /**
                        * field for Type
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTypeTracker = false ;

                           public boolean isTypeSpecified(){
                               return localTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getType(){
                               return localType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Type
                               */
                               public void setType(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localTypeTracker = param != null;
                                   
                                            this.localType=param;
                                    

                               }
                            

                        /**
                        * field for UserType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localUserType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUserTypeTracker = false ;

                           public boolean isUserTypeSpecified(){
                               return localUserTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getUserType(){
                               return localUserType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserType
                               */
                               public void setUserType(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localUserTypeTracker = param != null;
                                   
                                            this.localUserType=param;
                                    

                               }
                            

                        /**
                        * field for VersionBroken
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localVersionBroken ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVersionBrokenTracker = false ;

                           public boolean isVersionBrokenSpecified(){
                               return localVersionBrokenTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getVersionBroken(){
                               return localVersionBroken;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VersionBroken
                               */
                               public void setVersionBroken(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localVersionBrokenTracker = param != null;
                                   
                                            this.localVersionBroken=param;
                                    

                               }
                            

                        /**
                        * field for VersionFixed
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localVersionFixed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVersionFixedTracker = false ;

                           public boolean isVersionFixedSpecified(){
                               return localVersionFixedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getVersionFixed(){
                               return localVersionFixed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VersionFixed
                               */
                               public void setVersionFixed(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localVersionFixedTracker = param != null;
                                   
                                            this.localVersionFixed=param;
                                    

                               }
                            

                        /**
                        * field for VersionTarget
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localVersionTarget ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVersionTargetTracker = false ;

                           public boolean isVersionTargetSpecified(){
                               return localVersionTargetTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getVersionTarget(){
                               return localVersionTarget;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VersionTarget
                               */
                               public void setVersionTarget(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localVersionTargetTracker = param != null;
                                   
                                            this.localVersionTarget=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":IssueSearchBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "IssueSearchBasic",
                           xmlWriter);
                   }

                if (localAgeInMonthsTracker){
                                            if (localAgeInMonths==null){
                                                 throw new org.apache.axis2.databinding.ADBException("ageInMonths cannot be null!!");
                                            }
                                           localAgeInMonths.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ageInMonths"),
                                               xmlWriter);
                                        } if (localAssignedTracker){
                                            if (localAssigned==null){
                                                 throw new org.apache.axis2.databinding.ADBException("assigned cannot be null!!");
                                            }
                                           localAssigned.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","assigned"),
                                               xmlWriter);
                                        } if (localBuildBrokenTracker){
                                            if (localBuildBroken==null){
                                                 throw new org.apache.axis2.databinding.ADBException("buildBroken cannot be null!!");
                                            }
                                           localBuildBroken.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buildBroken"),
                                               xmlWriter);
                                        } if (localBuildBrokenNameTracker){
                                            if (localBuildBrokenName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("buildBrokenName cannot be null!!");
                                            }
                                           localBuildBrokenName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buildBrokenName"),
                                               xmlWriter);
                                        } if (localBuildFixedTracker){
                                            if (localBuildFixed==null){
                                                 throw new org.apache.axis2.databinding.ADBException("buildFixed cannot be null!!");
                                            }
                                           localBuildFixed.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buildFixed"),
                                               xmlWriter);
                                        } if (localBuildFixedNameTracker){
                                            if (localBuildFixedName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("buildFixedName cannot be null!!");
                                            }
                                           localBuildFixedName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buildFixedName"),
                                               xmlWriter);
                                        } if (localBuildTargetTracker){
                                            if (localBuildTarget==null){
                                                 throw new org.apache.axis2.databinding.ADBException("buildTarget cannot be null!!");
                                            }
                                           localBuildTarget.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buildTarget"),
                                               xmlWriter);
                                        } if (localBuildTargetNameTracker){
                                            if (localBuildTargetName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("buildTargetName cannot be null!!");
                                            }
                                           localBuildTargetName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buildTargetName"),
                                               xmlWriter);
                                        } if (localCaseCountTracker){
                                            if (localCaseCount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("caseCount cannot be null!!");
                                            }
                                           localCaseCount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","caseCount"),
                                               xmlWriter);
                                        } if (localCaseNumberTracker){
                                            if (localCaseNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("caseNumber cannot be null!!");
                                            }
                                           localCaseNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","caseNumber"),
                                               xmlWriter);
                                        } if (localClosedDateTracker){
                                            if (localClosedDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("closedDate cannot be null!!");
                                            }
                                           localClosedDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","closedDate"),
                                               xmlWriter);
                                        } if (localCreatedDateTracker){
                                            if (localCreatedDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                            }
                                           localCreatedDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","createdDate"),
                                               xmlWriter);
                                        } if (localDateReleasedTracker){
                                            if (localDateReleased==null){
                                                 throw new org.apache.axis2.databinding.ADBException("dateReleased cannot be null!!");
                                            }
                                           localDateReleased.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateReleased"),
                                               xmlWriter);
                                        } if (localDetailsTracker){
                                            if (localDetails==null){
                                                 throw new org.apache.axis2.databinding.ADBException("details cannot be null!!");
                                            }
                                           localDetails.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","details"),
                                               xmlWriter);
                                        } if (localDuplicateOfTracker){
                                            if (localDuplicateOf==null){
                                                 throw new org.apache.axis2.databinding.ADBException("duplicateOf cannot be null!!");
                                            }
                                           localDuplicateOf.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","duplicateOf"),
                                               xmlWriter);
                                        } if (localEFixTracker){
                                            if (localEFix==null){
                                                 throw new org.apache.axis2.databinding.ADBException("eFix cannot be null!!");
                                            }
                                           localEFix.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","eFix"),
                                               xmlWriter);
                                        } if (localEmployeeOrTeamTracker){
                                            if (localEmployeeOrTeam==null){
                                                 throw new org.apache.axis2.databinding.ADBException("employeeOrTeam cannot be null!!");
                                            }
                                           localEmployeeOrTeam.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","employeeOrTeam"),
                                               xmlWriter);
                                        } if (localEventStatusTracker){
                                            if (localEventStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("eventStatus cannot be null!!");
                                            }
                                           localEventStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","eventStatus"),
                                               xmlWriter);
                                        } if (localExternalAbstractTracker){
                                            if (localExternalAbstract==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalAbstract cannot be null!!");
                                            }
                                           localExternalAbstract.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalAbstract"),
                                               xmlWriter);
                                        } if (localExternalDetailsTracker){
                                            if (localExternalDetails==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalDetails cannot be null!!");
                                            }
                                           localExternalDetails.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalDetails"),
                                               xmlWriter);
                                        } if (localExternalFixedInTracker){
                                            if (localExternalFixedIn==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalFixedIn cannot be null!!");
                                            }
                                           localExternalFixedIn.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalFixedIn"),
                                               xmlWriter);
                                        } if (localExternalIdTracker){
                                            if (localExternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                            }
                                           localExternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId"),
                                               xmlWriter);
                                        } if (localExternalIdStringTracker){
                                            if (localExternalIdString==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                            }
                                           localExternalIdString.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString"),
                                               xmlWriter);
                                        } if (localExternalStatusTracker){
                                            if (localExternalStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalStatus cannot be null!!");
                                            }
                                           localExternalStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalStatus"),
                                               xmlWriter);
                                        } if (localFixedTracker){
                                            if (localFixed==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fixed cannot be null!!");
                                            }
                                           localFixed.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fixed"),
                                               xmlWriter);
                                        } if (localFixedByTracker){
                                            if (localFixedBy==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fixedBy cannot be null!!");
                                            }
                                           localFixedBy.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fixedBy"),
                                               xmlWriter);
                                        } if (localInternalIdTracker){
                                            if (localInternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                            }
                                           localInternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                               xmlWriter);
                                        } if (localInternalIdNumberTracker){
                                            if (localInternalIdNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                            }
                                           localInternalIdNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber"),
                                               xmlWriter);
                                        } if (localIsOwnerTracker){
                                            if (localIsOwner==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isOwner cannot be null!!");
                                            }
                                           localIsOwner.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isOwner"),
                                               xmlWriter);
                                        } if (localIsReviewedTracker){
                                            if (localIsReviewed==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isReviewed cannot be null!!");
                                            }
                                           localIsReviewed.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isReviewed"),
                                               xmlWriter);
                                        } if (localIsShowStopperTracker){
                                            if (localIsShowStopper==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isShowStopper cannot be null!!");
                                            }
                                           localIsShowStopper.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isShowStopper"),
                                               xmlWriter);
                                        } if (localIssueAbstractTracker){
                                            if (localIssueAbstract==null){
                                                 throw new org.apache.axis2.databinding.ADBException("issueAbstract cannot be null!!");
                                            }
                                           localIssueAbstract.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","issueAbstract"),
                                               xmlWriter);
                                        } if (localIssueNumberTracker){
                                            if (localIssueNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("issueNumber cannot be null!!");
                                            }
                                           localIssueNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","issueNumber"),
                                               xmlWriter);
                                        } if (localItemTracker){
                                            if (localItem==null){
                                                 throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                            }
                                           localItem.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","item"),
                                               xmlWriter);
                                        } if (localLastModifiedDateTracker){
                                            if (localLastModifiedDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                            }
                                           localLastModifiedDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate"),
                                               xmlWriter);
                                        } if (localModuleTracker){
                                            if (localModule==null){
                                                 throw new org.apache.axis2.databinding.ADBException("module cannot be null!!");
                                            }
                                           localModule.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","module"),
                                               xmlWriter);
                                        } if (localNumberTracker){
                                            if (localNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("number cannot be null!!");
                                            }
                                           localNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","number"),
                                               xmlWriter);
                                        } if (localOriginalFixedInTracker){
                                            if (localOriginalFixedIn==null){
                                                 throw new org.apache.axis2.databinding.ADBException("originalFixedIn cannot be null!!");
                                            }
                                           localOriginalFixedIn.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","originalFixedIn"),
                                               xmlWriter);
                                        } if (localPriorityTracker){
                                            if (localPriority==null){
                                                 throw new org.apache.axis2.databinding.ADBException("priority cannot be null!!");
                                            }
                                           localPriority.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","priority"),
                                               xmlWriter);
                                        } if (localProductTracker){
                                            if (localProduct==null){
                                                 throw new org.apache.axis2.databinding.ADBException("product cannot be null!!");
                                            }
                                           localProduct.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","product"),
                                               xmlWriter);
                                        } if (localProductTeamTracker){
                                            if (localProductTeam==null){
                                                 throw new org.apache.axis2.databinding.ADBException("productTeam cannot be null!!");
                                            }
                                           localProductTeam.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","productTeam"),
                                               xmlWriter);
                                        } if (localRelatedIssueTracker){
                                            if (localRelatedIssue==null){
                                                 throw new org.apache.axis2.databinding.ADBException("relatedIssue cannot be null!!");
                                            }
                                           localRelatedIssue.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","relatedIssue"),
                                               xmlWriter);
                                        } if (localRelationshipTracker){
                                            if (localRelationship==null){
                                                 throw new org.apache.axis2.databinding.ADBException("relationship cannot be null!!");
                                            }
                                           localRelationship.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","relationship"),
                                               xmlWriter);
                                        } if (localRelationshipCommentTracker){
                                            if (localRelationshipComment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("relationshipComment cannot be null!!");
                                            }
                                           localRelationshipComment.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","relationshipComment"),
                                               xmlWriter);
                                        } if (localReportedByTracker){
                                            if (localReportedBy==null){
                                                 throw new org.apache.axis2.databinding.ADBException("reportedBy cannot be null!!");
                                            }
                                           localReportedBy.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","reportedBy"),
                                               xmlWriter);
                                        } if (localReproduceTracker){
                                            if (localReproduce==null){
                                                 throw new org.apache.axis2.databinding.ADBException("reproduce cannot be null!!");
                                            }
                                           localReproduce.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","reproduce"),
                                               xmlWriter);
                                        } if (localResolvedTracker){
                                            if (localResolved==null){
                                                 throw new org.apache.axis2.databinding.ADBException("resolved cannot be null!!");
                                            }
                                           localResolved.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","resolved"),
                                               xmlWriter);
                                        } if (localResolvedByTracker){
                                            if (localResolvedBy==null){
                                                 throw new org.apache.axis2.databinding.ADBException("resolvedBy cannot be null!!");
                                            }
                                           localResolvedBy.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","resolvedBy"),
                                               xmlWriter);
                                        } if (localReviewerTracker){
                                            if (localReviewer==null){
                                                 throw new org.apache.axis2.databinding.ADBException("reviewer cannot be null!!");
                                            }
                                           localReviewer.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","reviewer"),
                                               xmlWriter);
                                        } if (localSeverityTracker){
                                            if (localSeverity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("severity cannot be null!!");
                                            }
                                           localSeverity.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","severity"),
                                               xmlWriter);
                                        } if (localSourceTracker){
                                            if (localSource==null){
                                                 throw new org.apache.axis2.databinding.ADBException("source cannot be null!!");
                                            }
                                           localSource.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","source"),
                                               xmlWriter);
                                        } if (localStatusTracker){
                                            if (localStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                            }
                                           localStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","status"),
                                               xmlWriter);
                                        } if (localTagsTracker){
                                            if (localTags==null){
                                                 throw new org.apache.axis2.databinding.ADBException("tags cannot be null!!");
                                            }
                                           localTags.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tags"),
                                               xmlWriter);
                                        } if (localTrackingTracker){
                                            if (localTracking==null){
                                                 throw new org.apache.axis2.databinding.ADBException("tracking cannot be null!!");
                                            }
                                           localTracking.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tracking"),
                                               xmlWriter);
                                        } if (localTypeTracker){
                                            if (localType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("type cannot be null!!");
                                            }
                                           localType.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","type"),
                                               xmlWriter);
                                        } if (localUserTypeTracker){
                                            if (localUserType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("userType cannot be null!!");
                                            }
                                           localUserType.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","userType"),
                                               xmlWriter);
                                        } if (localVersionBrokenTracker){
                                            if (localVersionBroken==null){
                                                 throw new org.apache.axis2.databinding.ADBException("versionBroken cannot be null!!");
                                            }
                                           localVersionBroken.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","versionBroken"),
                                               xmlWriter);
                                        } if (localVersionFixedTracker){
                                            if (localVersionFixed==null){
                                                 throw new org.apache.axis2.databinding.ADBException("versionFixed cannot be null!!");
                                            }
                                           localVersionFixed.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","versionFixed"),
                                               xmlWriter);
                                        } if (localVersionTargetTracker){
                                            if (localVersionTarget==null){
                                                 throw new org.apache.axis2.databinding.ADBException("versionTarget cannot be null!!");
                                            }
                                           localVersionTarget.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","versionTarget"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","IssueSearchBasic"));
                 if (localAgeInMonthsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "ageInMonths"));
                            
                            
                                    if (localAgeInMonths==null){
                                         throw new org.apache.axis2.databinding.ADBException("ageInMonths cannot be null!!");
                                    }
                                    elementList.add(localAgeInMonths);
                                } if (localAssignedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "assigned"));
                            
                            
                                    if (localAssigned==null){
                                         throw new org.apache.axis2.databinding.ADBException("assigned cannot be null!!");
                                    }
                                    elementList.add(localAssigned);
                                } if (localBuildBrokenTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "buildBroken"));
                            
                            
                                    if (localBuildBroken==null){
                                         throw new org.apache.axis2.databinding.ADBException("buildBroken cannot be null!!");
                                    }
                                    elementList.add(localBuildBroken);
                                } if (localBuildBrokenNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "buildBrokenName"));
                            
                            
                                    if (localBuildBrokenName==null){
                                         throw new org.apache.axis2.databinding.ADBException("buildBrokenName cannot be null!!");
                                    }
                                    elementList.add(localBuildBrokenName);
                                } if (localBuildFixedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "buildFixed"));
                            
                            
                                    if (localBuildFixed==null){
                                         throw new org.apache.axis2.databinding.ADBException("buildFixed cannot be null!!");
                                    }
                                    elementList.add(localBuildFixed);
                                } if (localBuildFixedNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "buildFixedName"));
                            
                            
                                    if (localBuildFixedName==null){
                                         throw new org.apache.axis2.databinding.ADBException("buildFixedName cannot be null!!");
                                    }
                                    elementList.add(localBuildFixedName);
                                } if (localBuildTargetTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "buildTarget"));
                            
                            
                                    if (localBuildTarget==null){
                                         throw new org.apache.axis2.databinding.ADBException("buildTarget cannot be null!!");
                                    }
                                    elementList.add(localBuildTarget);
                                } if (localBuildTargetNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "buildTargetName"));
                            
                            
                                    if (localBuildTargetName==null){
                                         throw new org.apache.axis2.databinding.ADBException("buildTargetName cannot be null!!");
                                    }
                                    elementList.add(localBuildTargetName);
                                } if (localCaseCountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "caseCount"));
                            
                            
                                    if (localCaseCount==null){
                                         throw new org.apache.axis2.databinding.ADBException("caseCount cannot be null!!");
                                    }
                                    elementList.add(localCaseCount);
                                } if (localCaseNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "caseNumber"));
                            
                            
                                    if (localCaseNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("caseNumber cannot be null!!");
                                    }
                                    elementList.add(localCaseNumber);
                                } if (localClosedDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "closedDate"));
                            
                            
                                    if (localClosedDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("closedDate cannot be null!!");
                                    }
                                    elementList.add(localClosedDate);
                                } if (localCreatedDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "createdDate"));
                            
                            
                                    if (localCreatedDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                    }
                                    elementList.add(localCreatedDate);
                                } if (localDateReleasedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "dateReleased"));
                            
                            
                                    if (localDateReleased==null){
                                         throw new org.apache.axis2.databinding.ADBException("dateReleased cannot be null!!");
                                    }
                                    elementList.add(localDateReleased);
                                } if (localDetailsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "details"));
                            
                            
                                    if (localDetails==null){
                                         throw new org.apache.axis2.databinding.ADBException("details cannot be null!!");
                                    }
                                    elementList.add(localDetails);
                                } if (localDuplicateOfTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "duplicateOf"));
                            
                            
                                    if (localDuplicateOf==null){
                                         throw new org.apache.axis2.databinding.ADBException("duplicateOf cannot be null!!");
                                    }
                                    elementList.add(localDuplicateOf);
                                } if (localEFixTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "eFix"));
                            
                            
                                    if (localEFix==null){
                                         throw new org.apache.axis2.databinding.ADBException("eFix cannot be null!!");
                                    }
                                    elementList.add(localEFix);
                                } if (localEmployeeOrTeamTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "employeeOrTeam"));
                            
                            
                                    if (localEmployeeOrTeam==null){
                                         throw new org.apache.axis2.databinding.ADBException("employeeOrTeam cannot be null!!");
                                    }
                                    elementList.add(localEmployeeOrTeam);
                                } if (localEventStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "eventStatus"));
                            
                            
                                    if (localEventStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("eventStatus cannot be null!!");
                                    }
                                    elementList.add(localEventStatus);
                                } if (localExternalAbstractTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalAbstract"));
                            
                            
                                    if (localExternalAbstract==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalAbstract cannot be null!!");
                                    }
                                    elementList.add(localExternalAbstract);
                                } if (localExternalDetailsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalDetails"));
                            
                            
                                    if (localExternalDetails==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalDetails cannot be null!!");
                                    }
                                    elementList.add(localExternalDetails);
                                } if (localExternalFixedInTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalFixedIn"));
                            
                            
                                    if (localExternalFixedIn==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalFixedIn cannot be null!!");
                                    }
                                    elementList.add(localExternalFixedIn);
                                } if (localExternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalId"));
                            
                            
                                    if (localExternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                    }
                                    elementList.add(localExternalId);
                                } if (localExternalIdStringTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalIdString"));
                            
                            
                                    if (localExternalIdString==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                    }
                                    elementList.add(localExternalIdString);
                                } if (localExternalStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalStatus"));
                            
                            
                                    if (localExternalStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalStatus cannot be null!!");
                                    }
                                    elementList.add(localExternalStatus);
                                } if (localFixedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "fixed"));
                            
                            
                                    if (localFixed==null){
                                         throw new org.apache.axis2.databinding.ADBException("fixed cannot be null!!");
                                    }
                                    elementList.add(localFixed);
                                } if (localFixedByTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "fixedBy"));
                            
                            
                                    if (localFixedBy==null){
                                         throw new org.apache.axis2.databinding.ADBException("fixedBy cannot be null!!");
                                    }
                                    elementList.add(localFixedBy);
                                } if (localInternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalId"));
                            
                            
                                    if (localInternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    }
                                    elementList.add(localInternalId);
                                } if (localInternalIdNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalIdNumber"));
                            
                            
                                    if (localInternalIdNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                    }
                                    elementList.add(localInternalIdNumber);
                                } if (localIsOwnerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isOwner"));
                            
                            
                                    if (localIsOwner==null){
                                         throw new org.apache.axis2.databinding.ADBException("isOwner cannot be null!!");
                                    }
                                    elementList.add(localIsOwner);
                                } if (localIsReviewedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isReviewed"));
                            
                            
                                    if (localIsReviewed==null){
                                         throw new org.apache.axis2.databinding.ADBException("isReviewed cannot be null!!");
                                    }
                                    elementList.add(localIsReviewed);
                                } if (localIsShowStopperTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isShowStopper"));
                            
                            
                                    if (localIsShowStopper==null){
                                         throw new org.apache.axis2.databinding.ADBException("isShowStopper cannot be null!!");
                                    }
                                    elementList.add(localIsShowStopper);
                                } if (localIssueAbstractTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "issueAbstract"));
                            
                            
                                    if (localIssueAbstract==null){
                                         throw new org.apache.axis2.databinding.ADBException("issueAbstract cannot be null!!");
                                    }
                                    elementList.add(localIssueAbstract);
                                } if (localIssueNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "issueNumber"));
                            
                            
                                    if (localIssueNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("issueNumber cannot be null!!");
                                    }
                                    elementList.add(localIssueNumber);
                                } if (localItemTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "item"));
                            
                            
                                    if (localItem==null){
                                         throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                    }
                                    elementList.add(localItem);
                                } if (localLastModifiedDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                            
                            
                                    if (localLastModifiedDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                    }
                                    elementList.add(localLastModifiedDate);
                                } if (localModuleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "module"));
                            
                            
                                    if (localModule==null){
                                         throw new org.apache.axis2.databinding.ADBException("module cannot be null!!");
                                    }
                                    elementList.add(localModule);
                                } if (localNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "number"));
                            
                            
                                    if (localNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("number cannot be null!!");
                                    }
                                    elementList.add(localNumber);
                                } if (localOriginalFixedInTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "originalFixedIn"));
                            
                            
                                    if (localOriginalFixedIn==null){
                                         throw new org.apache.axis2.databinding.ADBException("originalFixedIn cannot be null!!");
                                    }
                                    elementList.add(localOriginalFixedIn);
                                } if (localPriorityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "priority"));
                            
                            
                                    if (localPriority==null){
                                         throw new org.apache.axis2.databinding.ADBException("priority cannot be null!!");
                                    }
                                    elementList.add(localPriority);
                                } if (localProductTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "product"));
                            
                            
                                    if (localProduct==null){
                                         throw new org.apache.axis2.databinding.ADBException("product cannot be null!!");
                                    }
                                    elementList.add(localProduct);
                                } if (localProductTeamTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "productTeam"));
                            
                            
                                    if (localProductTeam==null){
                                         throw new org.apache.axis2.databinding.ADBException("productTeam cannot be null!!");
                                    }
                                    elementList.add(localProductTeam);
                                } if (localRelatedIssueTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "relatedIssue"));
                            
                            
                                    if (localRelatedIssue==null){
                                         throw new org.apache.axis2.databinding.ADBException("relatedIssue cannot be null!!");
                                    }
                                    elementList.add(localRelatedIssue);
                                } if (localRelationshipTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "relationship"));
                            
                            
                                    if (localRelationship==null){
                                         throw new org.apache.axis2.databinding.ADBException("relationship cannot be null!!");
                                    }
                                    elementList.add(localRelationship);
                                } if (localRelationshipCommentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "relationshipComment"));
                            
                            
                                    if (localRelationshipComment==null){
                                         throw new org.apache.axis2.databinding.ADBException("relationshipComment cannot be null!!");
                                    }
                                    elementList.add(localRelationshipComment);
                                } if (localReportedByTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "reportedBy"));
                            
                            
                                    if (localReportedBy==null){
                                         throw new org.apache.axis2.databinding.ADBException("reportedBy cannot be null!!");
                                    }
                                    elementList.add(localReportedBy);
                                } if (localReproduceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "reproduce"));
                            
                            
                                    if (localReproduce==null){
                                         throw new org.apache.axis2.databinding.ADBException("reproduce cannot be null!!");
                                    }
                                    elementList.add(localReproduce);
                                } if (localResolvedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "resolved"));
                            
                            
                                    if (localResolved==null){
                                         throw new org.apache.axis2.databinding.ADBException("resolved cannot be null!!");
                                    }
                                    elementList.add(localResolved);
                                } if (localResolvedByTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "resolvedBy"));
                            
                            
                                    if (localResolvedBy==null){
                                         throw new org.apache.axis2.databinding.ADBException("resolvedBy cannot be null!!");
                                    }
                                    elementList.add(localResolvedBy);
                                } if (localReviewerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "reviewer"));
                            
                            
                                    if (localReviewer==null){
                                         throw new org.apache.axis2.databinding.ADBException("reviewer cannot be null!!");
                                    }
                                    elementList.add(localReviewer);
                                } if (localSeverityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "severity"));
                            
                            
                                    if (localSeverity==null){
                                         throw new org.apache.axis2.databinding.ADBException("severity cannot be null!!");
                                    }
                                    elementList.add(localSeverity);
                                } if (localSourceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "source"));
                            
                            
                                    if (localSource==null){
                                         throw new org.apache.axis2.databinding.ADBException("source cannot be null!!");
                                    }
                                    elementList.add(localSource);
                                } if (localStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "status"));
                            
                            
                                    if (localStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                    }
                                    elementList.add(localStatus);
                                } if (localTagsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "tags"));
                            
                            
                                    if (localTags==null){
                                         throw new org.apache.axis2.databinding.ADBException("tags cannot be null!!");
                                    }
                                    elementList.add(localTags);
                                } if (localTrackingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "tracking"));
                            
                            
                                    if (localTracking==null){
                                         throw new org.apache.axis2.databinding.ADBException("tracking cannot be null!!");
                                    }
                                    elementList.add(localTracking);
                                } if (localTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "type"));
                            
                            
                                    if (localType==null){
                                         throw new org.apache.axis2.databinding.ADBException("type cannot be null!!");
                                    }
                                    elementList.add(localType);
                                } if (localUserTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "userType"));
                            
                            
                                    if (localUserType==null){
                                         throw new org.apache.axis2.databinding.ADBException("userType cannot be null!!");
                                    }
                                    elementList.add(localUserType);
                                } if (localVersionBrokenTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "versionBroken"));
                            
                            
                                    if (localVersionBroken==null){
                                         throw new org.apache.axis2.databinding.ADBException("versionBroken cannot be null!!");
                                    }
                                    elementList.add(localVersionBroken);
                                } if (localVersionFixedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "versionFixed"));
                            
                            
                                    if (localVersionFixed==null){
                                         throw new org.apache.axis2.databinding.ADBException("versionFixed cannot be null!!");
                                    }
                                    elementList.add(localVersionFixed);
                                } if (localVersionTargetTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "versionTarget"));
                            
                            
                                    if (localVersionTarget==null){
                                         throw new org.apache.axis2.databinding.ADBException("versionTarget cannot be null!!");
                                    }
                                    elementList.add(localVersionTarget);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static IssueSearchBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            IssueSearchBasic object =
                new IssueSearchBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"IssueSearchBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (IssueSearchBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","ageInMonths").equals(reader.getName())){
                                
                                                object.setAgeInMonths(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","assigned").equals(reader.getName())){
                                
                                                object.setAssigned(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buildBroken").equals(reader.getName())){
                                
                                                object.setBuildBroken(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buildBrokenName").equals(reader.getName())){
                                
                                                object.setBuildBrokenName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buildFixed").equals(reader.getName())){
                                
                                                object.setBuildFixed(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buildFixedName").equals(reader.getName())){
                                
                                                object.setBuildFixedName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buildTarget").equals(reader.getName())){
                                
                                                object.setBuildTarget(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buildTargetName").equals(reader.getName())){
                                
                                                object.setBuildTargetName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","caseCount").equals(reader.getName())){
                                
                                                object.setCaseCount(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","caseNumber").equals(reader.getName())){
                                
                                                object.setCaseNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","closedDate").equals(reader.getName())){
                                
                                                object.setClosedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","createdDate").equals(reader.getName())){
                                
                                                object.setCreatedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateReleased").equals(reader.getName())){
                                
                                                object.setDateReleased(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","details").equals(reader.getName())){
                                
                                                object.setDetails(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","duplicateOf").equals(reader.getName())){
                                
                                                object.setDuplicateOf(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","eFix").equals(reader.getName())){
                                
                                                object.setEFix(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","employeeOrTeam").equals(reader.getName())){
                                
                                                object.setEmployeeOrTeam(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","eventStatus").equals(reader.getName())){
                                
                                                object.setEventStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalAbstract").equals(reader.getName())){
                                
                                                object.setExternalAbstract(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalDetails").equals(reader.getName())){
                                
                                                object.setExternalDetails(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalFixedIn").equals(reader.getName())){
                                
                                                object.setExternalFixedIn(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                
                                                object.setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString").equals(reader.getName())){
                                
                                                object.setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalStatus").equals(reader.getName())){
                                
                                                object.setExternalStatus(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fixed").equals(reader.getName())){
                                
                                                object.setFixed(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fixedBy").equals(reader.getName())){
                                
                                                object.setFixedBy(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                                object.setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber").equals(reader.getName())){
                                
                                                object.setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isOwner").equals(reader.getName())){
                                
                                                object.setIsOwner(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isReviewed").equals(reader.getName())){
                                
                                                object.setIsReviewed(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isShowStopper").equals(reader.getName())){
                                
                                                object.setIsShowStopper(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","issueAbstract").equals(reader.getName())){
                                
                                                object.setIssueAbstract(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","issueNumber").equals(reader.getName())){
                                
                                                object.setIssueNumber(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","item").equals(reader.getName())){
                                
                                                object.setItem(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                                object.setLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","module").equals(reader.getName())){
                                
                                                object.setModule(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","number").equals(reader.getName())){
                                
                                                object.setNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","originalFixedIn").equals(reader.getName())){
                                
                                                object.setOriginalFixedIn(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","priority").equals(reader.getName())){
                                
                                                object.setPriority(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","product").equals(reader.getName())){
                                
                                                object.setProduct(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","productTeam").equals(reader.getName())){
                                
                                                object.setProductTeam(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","relatedIssue").equals(reader.getName())){
                                
                                                object.setRelatedIssue(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","relationship").equals(reader.getName())){
                                
                                                object.setRelationship(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","relationshipComment").equals(reader.getName())){
                                
                                                object.setRelationshipComment(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","reportedBy").equals(reader.getName())){
                                
                                                object.setReportedBy(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","reproduce").equals(reader.getName())){
                                
                                                object.setReproduce(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","resolved").equals(reader.getName())){
                                
                                                object.setResolved(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","resolvedBy").equals(reader.getName())){
                                
                                                object.setResolvedBy(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","reviewer").equals(reader.getName())){
                                
                                                object.setReviewer(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","severity").equals(reader.getName())){
                                
                                                object.setSeverity(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","source").equals(reader.getName())){
                                
                                                object.setSource(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","status").equals(reader.getName())){
                                
                                                object.setStatus(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tags").equals(reader.getName())){
                                
                                                object.setTags(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tracking").equals(reader.getName())){
                                
                                                object.setTracking(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","type").equals(reader.getName())){
                                
                                                object.setType(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","userType").equals(reader.getName())){
                                
                                                object.setUserType(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","versionBroken").equals(reader.getName())){
                                
                                                object.setVersionBroken(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","versionFixed").equals(reader.getName())){
                                
                                                object.setVersionFixed(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","versionTarget").equals(reader.getName())){
                                
                                                object.setVersionTarget(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    