
/**
 * AssemblyItemBomSearchBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  AssemblyItemBomSearchBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class AssemblyItemBomSearchBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRecordBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = AssemblyItemBomSearchBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for Assembly
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localAssembly ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAssemblyTracker = false ;

                           public boolean isAssemblySpecified(){
                               return localAssemblyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getAssembly(){
                               return localAssembly;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Assembly
                               */
                               public void setAssembly(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localAssemblyTracker = param != null;
                                   
                                            this.localAssembly=param;
                                    

                               }
                            

                        /**
                        * field for BillOfMaterials
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localBillOfMaterials ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillOfMaterialsTracker = false ;

                           public boolean isBillOfMaterialsSpecified(){
                               return localBillOfMaterialsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getBillOfMaterials(){
                               return localBillOfMaterials;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillOfMaterials
                               */
                               public void setBillOfMaterials(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localBillOfMaterialsTracker = param != null;
                                   
                                            this.localBillOfMaterials=param;
                                    

                               }
                            

                        /**
                        * field for _default
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField local_default ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_defaultTracker = false ;

                           public boolean is_defaultSpecified(){
                               return local_defaultTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField get_default(){
                               return local_default;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param _default
                               */
                               public void set_default(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            local_defaultTracker = param != null;
                                   
                                            this.local_default=param;
                                    

                               }
                            

                        /**
                        * field for Locations
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localLocations ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationsTracker = false ;

                           public boolean isLocationsSpecified(){
                               return localLocationsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getLocations(){
                               return localLocations;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Locations
                               */
                               public void setLocations(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localLocationsTracker = param != null;
                                   
                                            this.localLocations=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":AssemblyItemBomSearchBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "AssemblyItemBomSearchBasic",
                           xmlWriter);
                   }

                if (localAssemblyTracker){
                                            if (localAssembly==null){
                                                 throw new org.apache.axis2.databinding.ADBException("assembly cannot be null!!");
                                            }
                                           localAssembly.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","assembly"),
                                               xmlWriter);
                                        } if (localBillOfMaterialsTracker){
                                            if (localBillOfMaterials==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billOfMaterials cannot be null!!");
                                            }
                                           localBillOfMaterials.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billOfMaterials"),
                                               xmlWriter);
                                        } if (local_defaultTracker){
                                            if (local_default==null){
                                                 throw new org.apache.axis2.databinding.ADBException("default cannot be null!!");
                                            }
                                           local_default.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","default"),
                                               xmlWriter);
                                        } if (localLocationsTracker){
                                            if (localLocations==null){
                                                 throw new org.apache.axis2.databinding.ADBException("locations cannot be null!!");
                                            }
                                           localLocations.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","locations"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","AssemblyItemBomSearchBasic"));
                 if (localAssemblyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "assembly"));
                            
                            
                                    if (localAssembly==null){
                                         throw new org.apache.axis2.databinding.ADBException("assembly cannot be null!!");
                                    }
                                    elementList.add(localAssembly);
                                } if (localBillOfMaterialsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "billOfMaterials"));
                            
                            
                                    if (localBillOfMaterials==null){
                                         throw new org.apache.axis2.databinding.ADBException("billOfMaterials cannot be null!!");
                                    }
                                    elementList.add(localBillOfMaterials);
                                } if (local_defaultTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "default"));
                            
                            
                                    if (local_default==null){
                                         throw new org.apache.axis2.databinding.ADBException("default cannot be null!!");
                                    }
                                    elementList.add(local_default);
                                } if (localLocationsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "locations"));
                            
                            
                                    if (localLocations==null){
                                         throw new org.apache.axis2.databinding.ADBException("locations cannot be null!!");
                                    }
                                    elementList.add(localLocations);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static AssemblyItemBomSearchBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            AssemblyItemBomSearchBasic object =
                new AssemblyItemBomSearchBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"AssemblyItemBomSearchBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (AssemblyItemBomSearchBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","assembly").equals(reader.getName())){
                                
                                                object.setAssembly(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billOfMaterials").equals(reader.getName())){
                                
                                                object.setBillOfMaterials(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","default").equals(reader.getName())){
                                
                                                object.set_default(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","locations").equals(reader.getName())){
                                
                                                object.setLocations(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    