
/**
 * Country.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2.types;
            

            /**
            *  Country bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class Country
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.common_2017_2.platform.webservices.netsuite.com",
                "Country",
                "ns6");

            

                        /**
                        * field for Country
                        */

                        
                                    protected java.lang.String localCountry ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected Country(java.lang.String value, boolean isRegisterValue) {
                                    localCountry = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localCountry, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String __afghanistan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_afghanistan");
                                
                                    public static final java.lang.String __alandIslands =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_alandIslands");
                                
                                    public static final java.lang.String __albania =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_albania");
                                
                                    public static final java.lang.String __algeria =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_algeria");
                                
                                    public static final java.lang.String __americanSamoa =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_americanSamoa");
                                
                                    public static final java.lang.String __andorra =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_andorra");
                                
                                    public static final java.lang.String __angola =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_angola");
                                
                                    public static final java.lang.String __anguilla =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_anguilla");
                                
                                    public static final java.lang.String __antarctica =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_antarctica");
                                
                                    public static final java.lang.String __antiguaAndBarbuda =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_antiguaAndBarbuda");
                                
                                    public static final java.lang.String __argentina =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_argentina");
                                
                                    public static final java.lang.String __armenia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_armenia");
                                
                                    public static final java.lang.String __aruba =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_aruba");
                                
                                    public static final java.lang.String __australia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_australia");
                                
                                    public static final java.lang.String __austria =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_austria");
                                
                                    public static final java.lang.String __azerbaijan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_azerbaijan");
                                
                                    public static final java.lang.String __bahamas =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bahamas");
                                
                                    public static final java.lang.String __bahrain =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bahrain");
                                
                                    public static final java.lang.String __bangladesh =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bangladesh");
                                
                                    public static final java.lang.String __barbados =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_barbados");
                                
                                    public static final java.lang.String __belarus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_belarus");
                                
                                    public static final java.lang.String __belgium =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_belgium");
                                
                                    public static final java.lang.String __belize =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_belize");
                                
                                    public static final java.lang.String __benin =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_benin");
                                
                                    public static final java.lang.String __bermuda =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bermuda");
                                
                                    public static final java.lang.String __bhutan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bhutan");
                                
                                    public static final java.lang.String __bolivia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bolivia");
                                
                                    public static final java.lang.String __bonaireSaintEustatiusAndSaba =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bonaireSaintEustatiusAndSaba");
                                
                                    public static final java.lang.String __bosniaAndHerzegovina =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bosniaAndHerzegovina");
                                
                                    public static final java.lang.String __botswana =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_botswana");
                                
                                    public static final java.lang.String __bouvetIsland =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bouvetIsland");
                                
                                    public static final java.lang.String __brazil =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_brazil");
                                
                                    public static final java.lang.String __britishIndianOceanTerritory =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_britishIndianOceanTerritory");
                                
                                    public static final java.lang.String __bruneiDarussalam =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bruneiDarussalam");
                                
                                    public static final java.lang.String __bulgaria =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bulgaria");
                                
                                    public static final java.lang.String __burkinaFaso =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_burkinaFaso");
                                
                                    public static final java.lang.String __burundi =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_burundi");
                                
                                    public static final java.lang.String __cambodia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cambodia");
                                
                                    public static final java.lang.String __cameroon =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cameroon");
                                
                                    public static final java.lang.String __canada =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_canada");
                                
                                    public static final java.lang.String __canaryIslands =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_canaryIslands");
                                
                                    public static final java.lang.String __capeVerde =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_capeVerde");
                                
                                    public static final java.lang.String __caymanIslands =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_caymanIslands");
                                
                                    public static final java.lang.String __centralAfricanRepublic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_centralAfricanRepublic");
                                
                                    public static final java.lang.String __ceutaAndMelilla =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ceutaAndMelilla");
                                
                                    public static final java.lang.String __chad =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_chad");
                                
                                    public static final java.lang.String __chile =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_chile");
                                
                                    public static final java.lang.String __china =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_china");
                                
                                    public static final java.lang.String __christmasIsland =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_christmasIsland");
                                
                                    public static final java.lang.String __cocosKeelingIslands =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cocosKeelingIslands");
                                
                                    public static final java.lang.String __colombia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_colombia");
                                
                                    public static final java.lang.String __comoros =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_comoros");
                                
                                    public static final java.lang.String __congoDemocraticPeoplesRepublic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_congoDemocraticPeoplesRepublic");
                                
                                    public static final java.lang.String __congoRepublicOf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_congoRepublicOf");
                                
                                    public static final java.lang.String __cookIslands =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cookIslands");
                                
                                    public static final java.lang.String __costaRica =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_costaRica");
                                
                                    public static final java.lang.String __coteDIvoire =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_coteDIvoire");
                                
                                    public static final java.lang.String __croatiaHrvatska =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_croatiaHrvatska");
                                
                                    public static final java.lang.String __cuba =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cuba");
                                
                                    public static final java.lang.String __curacao =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_curacao");
                                
                                    public static final java.lang.String __cyprus =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_cyprus");
                                
                                    public static final java.lang.String __czechRepublic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_czechRepublic");
                                
                                    public static final java.lang.String __denmark =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_denmark");
                                
                                    public static final java.lang.String __djibouti =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_djibouti");
                                
                                    public static final java.lang.String __dominica =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_dominica");
                                
                                    public static final java.lang.String __dominicanRepublic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_dominicanRepublic");
                                
                                    public static final java.lang.String __eastTimor =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_eastTimor");
                                
                                    public static final java.lang.String __ecuador =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ecuador");
                                
                                    public static final java.lang.String __egypt =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_egypt");
                                
                                    public static final java.lang.String __elSalvador =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_elSalvador");
                                
                                    public static final java.lang.String __equatorialGuinea =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_equatorialGuinea");
                                
                                    public static final java.lang.String __eritrea =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_eritrea");
                                
                                    public static final java.lang.String __estonia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_estonia");
                                
                                    public static final java.lang.String __ethiopia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ethiopia");
                                
                                    public static final java.lang.String __falklandIslands =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_falklandIslands");
                                
                                    public static final java.lang.String __faroeIslands =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_faroeIslands");
                                
                                    public static final java.lang.String __fiji =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_fiji");
                                
                                    public static final java.lang.String __finland =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_finland");
                                
                                    public static final java.lang.String __france =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_france");
                                
                                    public static final java.lang.String __frenchGuiana =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_frenchGuiana");
                                
                                    public static final java.lang.String __frenchPolynesia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_frenchPolynesia");
                                
                                    public static final java.lang.String __frenchSouthernTerritories =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_frenchSouthernTerritories");
                                
                                    public static final java.lang.String __gabon =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gabon");
                                
                                    public static final java.lang.String __gambia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gambia");
                                
                                    public static final java.lang.String __georgia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_georgia");
                                
                                    public static final java.lang.String __germany =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_germany");
                                
                                    public static final java.lang.String __ghana =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ghana");
                                
                                    public static final java.lang.String __gibraltar =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gibraltar");
                                
                                    public static final java.lang.String __greece =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_greece");
                                
                                    public static final java.lang.String __greenland =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_greenland");
                                
                                    public static final java.lang.String __grenada =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_grenada");
                                
                                    public static final java.lang.String __guadeloupe =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_guadeloupe");
                                
                                    public static final java.lang.String __guam =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_guam");
                                
                                    public static final java.lang.String __guatemala =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_guatemala");
                                
                                    public static final java.lang.String __guernsey =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_guernsey");
                                
                                    public static final java.lang.String __guinea =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_guinea");
                                
                                    public static final java.lang.String __guineaBissau =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_guineaBissau");
                                
                                    public static final java.lang.String __guyana =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_guyana");
                                
                                    public static final java.lang.String __haiti =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_haiti");
                                
                                    public static final java.lang.String __heardAndMcDonaldIslands =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_heardAndMcDonaldIslands");
                                
                                    public static final java.lang.String __holySeeCityVaticanState =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_holySeeCityVaticanState");
                                
                                    public static final java.lang.String __honduras =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_honduras");
                                
                                    public static final java.lang.String __hongKong =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_hongKong");
                                
                                    public static final java.lang.String __hungary =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_hungary");
                                
                                    public static final java.lang.String __iceland =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_iceland");
                                
                                    public static final java.lang.String __india =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_india");
                                
                                    public static final java.lang.String __indonesia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_indonesia");
                                
                                    public static final java.lang.String __iranIslamicRepublicOf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_iranIslamicRepublicOf");
                                
                                    public static final java.lang.String __iraq =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_iraq");
                                
                                    public static final java.lang.String __ireland =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ireland");
                                
                                    public static final java.lang.String __isleOfMan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_isleOfMan");
                                
                                    public static final java.lang.String __israel =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_israel");
                                
                                    public static final java.lang.String __italy =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_italy");
                                
                                    public static final java.lang.String __jamaica =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_jamaica");
                                
                                    public static final java.lang.String __japan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_japan");
                                
                                    public static final java.lang.String __jersey =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_jersey");
                                
                                    public static final java.lang.String __jordan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_jordan");
                                
                                    public static final java.lang.String __kazakhstan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kazakhstan");
                                
                                    public static final java.lang.String __kenya =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kenya");
                                
                                    public static final java.lang.String __kiribati =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kiribati");
                                
                                    public static final java.lang.String __koreaDemocraticPeoplesRepublic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_koreaDemocraticPeoplesRepublic");
                                
                                    public static final java.lang.String __koreaRepublicOf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_koreaRepublicOf");
                                
                                    public static final java.lang.String __kosovo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kosovo");
                                
                                    public static final java.lang.String __kuwait =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kuwait");
                                
                                    public static final java.lang.String __kyrgyzstan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kyrgyzstan");
                                
                                    public static final java.lang.String __laoPeoplesDemocraticRepublic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_laoPeoplesDemocraticRepublic");
                                
                                    public static final java.lang.String __latvia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_latvia");
                                
                                    public static final java.lang.String __lebanon =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lebanon");
                                
                                    public static final java.lang.String __lesotho =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lesotho");
                                
                                    public static final java.lang.String __liberia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_liberia");
                                
                                    public static final java.lang.String __libya =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_libya");
                                
                                    public static final java.lang.String __liechtenstein =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_liechtenstein");
                                
                                    public static final java.lang.String __lithuania =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lithuania");
                                
                                    public static final java.lang.String __luxembourg =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_luxembourg");
                                
                                    public static final java.lang.String __macau =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_macau");
                                
                                    public static final java.lang.String __macedonia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_macedonia");
                                
                                    public static final java.lang.String __madagascar =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_madagascar");
                                
                                    public static final java.lang.String __malawi =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_malawi");
                                
                                    public static final java.lang.String __malaysia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_malaysia");
                                
                                    public static final java.lang.String __maldives =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_maldives");
                                
                                    public static final java.lang.String __mali =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mali");
                                
                                    public static final java.lang.String __malta =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_malta");
                                
                                    public static final java.lang.String __marshallIslands =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_marshallIslands");
                                
                                    public static final java.lang.String __martinique =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_martinique");
                                
                                    public static final java.lang.String __mauritania =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mauritania");
                                
                                    public static final java.lang.String __mauritius =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mauritius");
                                
                                    public static final java.lang.String __mayotte =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mayotte");
                                
                                    public static final java.lang.String __mexico =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mexico");
                                
                                    public static final java.lang.String __micronesiaFederalStateOf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_micronesiaFederalStateOf");
                                
                                    public static final java.lang.String __moldovaRepublicOf =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_moldovaRepublicOf");
                                
                                    public static final java.lang.String __monaco =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_monaco");
                                
                                    public static final java.lang.String __mongolia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mongolia");
                                
                                    public static final java.lang.String __montenegro =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_montenegro");
                                
                                    public static final java.lang.String __montserrat =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_montserrat");
                                
                                    public static final java.lang.String __morocco =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_morocco");
                                
                                    public static final java.lang.String __mozambique =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_mozambique");
                                
                                    public static final java.lang.String __myanmar =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_myanmar");
                                
                                    public static final java.lang.String __namibia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_namibia");
                                
                                    public static final java.lang.String __nauru =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_nauru");
                                
                                    public static final java.lang.String __nepal =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_nepal");
                                
                                    public static final java.lang.String __netherlands =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_netherlands");
                                
                                    public static final java.lang.String __newCaledonia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_newCaledonia");
                                
                                    public static final java.lang.String __newZealand =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_newZealand");
                                
                                    public static final java.lang.String __nicaragua =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_nicaragua");
                                
                                    public static final java.lang.String __niger =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_niger");
                                
                                    public static final java.lang.String __nigeria =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_nigeria");
                                
                                    public static final java.lang.String __niue =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_niue");
                                
                                    public static final java.lang.String __norfolkIsland =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_norfolkIsland");
                                
                                    public static final java.lang.String __northernMarianaIslands =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_northernMarianaIslands");
                                
                                    public static final java.lang.String __norway =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_norway");
                                
                                    public static final java.lang.String __oman =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_oman");
                                
                                    public static final java.lang.String __pakistan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pakistan");
                                
                                    public static final java.lang.String __palau =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_palau");
                                
                                    public static final java.lang.String __panama =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_panama");
                                
                                    public static final java.lang.String __papuaNewGuinea =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_papuaNewGuinea");
                                
                                    public static final java.lang.String __paraguay =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_paraguay");
                                
                                    public static final java.lang.String __peru =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_peru");
                                
                                    public static final java.lang.String __philippines =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_philippines");
                                
                                    public static final java.lang.String __pitcairnIsland =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_pitcairnIsland");
                                
                                    public static final java.lang.String __poland =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_poland");
                                
                                    public static final java.lang.String __portugal =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_portugal");
                                
                                    public static final java.lang.String __puertoRico =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_puertoRico");
                                
                                    public static final java.lang.String __qatar =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_qatar");
                                
                                    public static final java.lang.String __reunionIsland =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_reunionIsland");
                                
                                    public static final java.lang.String __romania =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_romania");
                                
                                    public static final java.lang.String __russianFederation =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_russianFederation");
                                
                                    public static final java.lang.String __rwanda =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_rwanda");
                                
                                    public static final java.lang.String __saintBarthelemy =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_saintBarthelemy");
                                
                                    public static final java.lang.String __saintHelena =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_saintHelena");
                                
                                    public static final java.lang.String __saintKittsAndNevis =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_saintKittsAndNevis");
                                
                                    public static final java.lang.String __saintLucia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_saintLucia");
                                
                                    public static final java.lang.String __saintMartin =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_saintMartin");
                                
                                    public static final java.lang.String __saintVincentAndTheGrenadines =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_saintVincentAndTheGrenadines");
                                
                                    public static final java.lang.String __samoa =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_samoa");
                                
                                    public static final java.lang.String __sanMarino =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sanMarino");
                                
                                    public static final java.lang.String __saoTomeAndPrincipe =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_saoTomeAndPrincipe");
                                
                                    public static final java.lang.String __saudiArabia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_saudiArabia");
                                
                                    public static final java.lang.String __senegal =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_senegal");
                                
                                    public static final java.lang.String __serbia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_serbia");
                                
                                    public static final java.lang.String __seychelles =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_seychelles");
                                
                                    public static final java.lang.String __sierraLeone =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sierraLeone");
                                
                                    public static final java.lang.String __singapore =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_singapore");
                                
                                    public static final java.lang.String __sintMaarten =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sintMaarten");
                                
                                    public static final java.lang.String __slovakRepublic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_slovakRepublic");
                                
                                    public static final java.lang.String __slovenia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_slovenia");
                                
                                    public static final java.lang.String __solomonIslands =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_solomonIslands");
                                
                                    public static final java.lang.String __somalia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_somalia");
                                
                                    public static final java.lang.String __southAfrica =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_southAfrica");
                                
                                    public static final java.lang.String __southGeorgia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_southGeorgia");
                                
                                    public static final java.lang.String __southSudan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_southSudan");
                                
                                    public static final java.lang.String __spain =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_spain");
                                
                                    public static final java.lang.String __sriLanka =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sriLanka");
                                
                                    public static final java.lang.String __stateOfPalestine =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_stateOfPalestine");
                                
                                    public static final java.lang.String __stPierreAndMiquelon =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_stPierreAndMiquelon");
                                
                                    public static final java.lang.String __sudan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sudan");
                                
                                    public static final java.lang.String __suriname =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_suriname");
                                
                                    public static final java.lang.String __svalbardAndJanMayenIslands =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_svalbardAndJanMayenIslands");
                                
                                    public static final java.lang.String __swaziland =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_swaziland");
                                
                                    public static final java.lang.String __sweden =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_sweden");
                                
                                    public static final java.lang.String __switzerland =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_switzerland");
                                
                                    public static final java.lang.String __syrianArabRepublic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_syrianArabRepublic");
                                
                                    public static final java.lang.String __taiwan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_taiwan");
                                
                                    public static final java.lang.String __tajikistan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tajikistan");
                                
                                    public static final java.lang.String __tanzania =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tanzania");
                                
                                    public static final java.lang.String __thailand =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_thailand");
                                
                                    public static final java.lang.String __togo =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_togo");
                                
                                    public static final java.lang.String __tokelau =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tokelau");
                                
                                    public static final java.lang.String __tonga =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tonga");
                                
                                    public static final java.lang.String __trinidadAndTobago =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_trinidadAndTobago");
                                
                                    public static final java.lang.String __tunisia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tunisia");
                                
                                    public static final java.lang.String __turkey =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_turkey");
                                
                                    public static final java.lang.String __turkmenistan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_turkmenistan");
                                
                                    public static final java.lang.String __turksAndCaicosIslands =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_turksAndCaicosIslands");
                                
                                    public static final java.lang.String __tuvalu =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tuvalu");
                                
                                    public static final java.lang.String __uganda =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_uganda");
                                
                                    public static final java.lang.String __ukraine =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ukraine");
                                
                                    public static final java.lang.String __unitedArabEmirates =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_unitedArabEmirates");
                                
                                    public static final java.lang.String __unitedKingdom =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_unitedKingdom");
                                
                                    public static final java.lang.String __unitedStates =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_unitedStates");
                                
                                    public static final java.lang.String __uruguay =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_uruguay");
                                
                                    public static final java.lang.String __uSMinorOutlyingIslands =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_uSMinorOutlyingIslands");
                                
                                    public static final java.lang.String __uzbekistan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_uzbekistan");
                                
                                    public static final java.lang.String __vanuatu =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vanuatu");
                                
                                    public static final java.lang.String __venezuela =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_venezuela");
                                
                                    public static final java.lang.String __vietnam =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vietnam");
                                
                                    public static final java.lang.String __virginIslandsBritish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_virginIslandsBritish");
                                
                                    public static final java.lang.String __virginIslandsUSA =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_virginIslandsUSA");
                                
                                    public static final java.lang.String __wallisAndFutunaIslands =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_wallisAndFutunaIslands");
                                
                                    public static final java.lang.String __westernSahara =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_westernSahara");
                                
                                    public static final java.lang.String __yemen =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_yemen");
                                
                                    public static final java.lang.String __zambia =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_zambia");
                                
                                    public static final java.lang.String __zimbabwe =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_zimbabwe");
                                
                                public static final Country _afghanistan =
                                    new Country(__afghanistan,true);
                            
                                public static final Country _alandIslands =
                                    new Country(__alandIslands,true);
                            
                                public static final Country _albania =
                                    new Country(__albania,true);
                            
                                public static final Country _algeria =
                                    new Country(__algeria,true);
                            
                                public static final Country _americanSamoa =
                                    new Country(__americanSamoa,true);
                            
                                public static final Country _andorra =
                                    new Country(__andorra,true);
                            
                                public static final Country _angola =
                                    new Country(__angola,true);
                            
                                public static final Country _anguilla =
                                    new Country(__anguilla,true);
                            
                                public static final Country _antarctica =
                                    new Country(__antarctica,true);
                            
                                public static final Country _antiguaAndBarbuda =
                                    new Country(__antiguaAndBarbuda,true);
                            
                                public static final Country _argentina =
                                    new Country(__argentina,true);
                            
                                public static final Country _armenia =
                                    new Country(__armenia,true);
                            
                                public static final Country _aruba =
                                    new Country(__aruba,true);
                            
                                public static final Country _australia =
                                    new Country(__australia,true);
                            
                                public static final Country _austria =
                                    new Country(__austria,true);
                            
                                public static final Country _azerbaijan =
                                    new Country(__azerbaijan,true);
                            
                                public static final Country _bahamas =
                                    new Country(__bahamas,true);
                            
                                public static final Country _bahrain =
                                    new Country(__bahrain,true);
                            
                                public static final Country _bangladesh =
                                    new Country(__bangladesh,true);
                            
                                public static final Country _barbados =
                                    new Country(__barbados,true);
                            
                                public static final Country _belarus =
                                    new Country(__belarus,true);
                            
                                public static final Country _belgium =
                                    new Country(__belgium,true);
                            
                                public static final Country _belize =
                                    new Country(__belize,true);
                            
                                public static final Country _benin =
                                    new Country(__benin,true);
                            
                                public static final Country _bermuda =
                                    new Country(__bermuda,true);
                            
                                public static final Country _bhutan =
                                    new Country(__bhutan,true);
                            
                                public static final Country _bolivia =
                                    new Country(__bolivia,true);
                            
                                public static final Country _bonaireSaintEustatiusAndSaba =
                                    new Country(__bonaireSaintEustatiusAndSaba,true);
                            
                                public static final Country _bosniaAndHerzegovina =
                                    new Country(__bosniaAndHerzegovina,true);
                            
                                public static final Country _botswana =
                                    new Country(__botswana,true);
                            
                                public static final Country _bouvetIsland =
                                    new Country(__bouvetIsland,true);
                            
                                public static final Country _brazil =
                                    new Country(__brazil,true);
                            
                                public static final Country _britishIndianOceanTerritory =
                                    new Country(__britishIndianOceanTerritory,true);
                            
                                public static final Country _bruneiDarussalam =
                                    new Country(__bruneiDarussalam,true);
                            
                                public static final Country _bulgaria =
                                    new Country(__bulgaria,true);
                            
                                public static final Country _burkinaFaso =
                                    new Country(__burkinaFaso,true);
                            
                                public static final Country _burundi =
                                    new Country(__burundi,true);
                            
                                public static final Country _cambodia =
                                    new Country(__cambodia,true);
                            
                                public static final Country _cameroon =
                                    new Country(__cameroon,true);
                            
                                public static final Country _canada =
                                    new Country(__canada,true);
                            
                                public static final Country _canaryIslands =
                                    new Country(__canaryIslands,true);
                            
                                public static final Country _capeVerde =
                                    new Country(__capeVerde,true);
                            
                                public static final Country _caymanIslands =
                                    new Country(__caymanIslands,true);
                            
                                public static final Country _centralAfricanRepublic =
                                    new Country(__centralAfricanRepublic,true);
                            
                                public static final Country _ceutaAndMelilla =
                                    new Country(__ceutaAndMelilla,true);
                            
                                public static final Country _chad =
                                    new Country(__chad,true);
                            
                                public static final Country _chile =
                                    new Country(__chile,true);
                            
                                public static final Country _china =
                                    new Country(__china,true);
                            
                                public static final Country _christmasIsland =
                                    new Country(__christmasIsland,true);
                            
                                public static final Country _cocosKeelingIslands =
                                    new Country(__cocosKeelingIslands,true);
                            
                                public static final Country _colombia =
                                    new Country(__colombia,true);
                            
                                public static final Country _comoros =
                                    new Country(__comoros,true);
                            
                                public static final Country _congoDemocraticPeoplesRepublic =
                                    new Country(__congoDemocraticPeoplesRepublic,true);
                            
                                public static final Country _congoRepublicOf =
                                    new Country(__congoRepublicOf,true);
                            
                                public static final Country _cookIslands =
                                    new Country(__cookIslands,true);
                            
                                public static final Country _costaRica =
                                    new Country(__costaRica,true);
                            
                                public static final Country _coteDIvoire =
                                    new Country(__coteDIvoire,true);
                            
                                public static final Country _croatiaHrvatska =
                                    new Country(__croatiaHrvatska,true);
                            
                                public static final Country _cuba =
                                    new Country(__cuba,true);
                            
                                public static final Country _curacao =
                                    new Country(__curacao,true);
                            
                                public static final Country _cyprus =
                                    new Country(__cyprus,true);
                            
                                public static final Country _czechRepublic =
                                    new Country(__czechRepublic,true);
                            
                                public static final Country _denmark =
                                    new Country(__denmark,true);
                            
                                public static final Country _djibouti =
                                    new Country(__djibouti,true);
                            
                                public static final Country _dominica =
                                    new Country(__dominica,true);
                            
                                public static final Country _dominicanRepublic =
                                    new Country(__dominicanRepublic,true);
                            
                                public static final Country _eastTimor =
                                    new Country(__eastTimor,true);
                            
                                public static final Country _ecuador =
                                    new Country(__ecuador,true);
                            
                                public static final Country _egypt =
                                    new Country(__egypt,true);
                            
                                public static final Country _elSalvador =
                                    new Country(__elSalvador,true);
                            
                                public static final Country _equatorialGuinea =
                                    new Country(__equatorialGuinea,true);
                            
                                public static final Country _eritrea =
                                    new Country(__eritrea,true);
                            
                                public static final Country _estonia =
                                    new Country(__estonia,true);
                            
                                public static final Country _ethiopia =
                                    new Country(__ethiopia,true);
                            
                                public static final Country _falklandIslands =
                                    new Country(__falklandIslands,true);
                            
                                public static final Country _faroeIslands =
                                    new Country(__faroeIslands,true);
                            
                                public static final Country _fiji =
                                    new Country(__fiji,true);
                            
                                public static final Country _finland =
                                    new Country(__finland,true);
                            
                                public static final Country _france =
                                    new Country(__france,true);
                            
                                public static final Country _frenchGuiana =
                                    new Country(__frenchGuiana,true);
                            
                                public static final Country _frenchPolynesia =
                                    new Country(__frenchPolynesia,true);
                            
                                public static final Country _frenchSouthernTerritories =
                                    new Country(__frenchSouthernTerritories,true);
                            
                                public static final Country _gabon =
                                    new Country(__gabon,true);
                            
                                public static final Country _gambia =
                                    new Country(__gambia,true);
                            
                                public static final Country _georgia =
                                    new Country(__georgia,true);
                            
                                public static final Country _germany =
                                    new Country(__germany,true);
                            
                                public static final Country _ghana =
                                    new Country(__ghana,true);
                            
                                public static final Country _gibraltar =
                                    new Country(__gibraltar,true);
                            
                                public static final Country _greece =
                                    new Country(__greece,true);
                            
                                public static final Country _greenland =
                                    new Country(__greenland,true);
                            
                                public static final Country _grenada =
                                    new Country(__grenada,true);
                            
                                public static final Country _guadeloupe =
                                    new Country(__guadeloupe,true);
                            
                                public static final Country _guam =
                                    new Country(__guam,true);
                            
                                public static final Country _guatemala =
                                    new Country(__guatemala,true);
                            
                                public static final Country _guernsey =
                                    new Country(__guernsey,true);
                            
                                public static final Country _guinea =
                                    new Country(__guinea,true);
                            
                                public static final Country _guineaBissau =
                                    new Country(__guineaBissau,true);
                            
                                public static final Country _guyana =
                                    new Country(__guyana,true);
                            
                                public static final Country _haiti =
                                    new Country(__haiti,true);
                            
                                public static final Country _heardAndMcDonaldIslands =
                                    new Country(__heardAndMcDonaldIslands,true);
                            
                                public static final Country _holySeeCityVaticanState =
                                    new Country(__holySeeCityVaticanState,true);
                            
                                public static final Country _honduras =
                                    new Country(__honduras,true);
                            
                                public static final Country _hongKong =
                                    new Country(__hongKong,true);
                            
                                public static final Country _hungary =
                                    new Country(__hungary,true);
                            
                                public static final Country _iceland =
                                    new Country(__iceland,true);
                            
                                public static final Country _india =
                                    new Country(__india,true);
                            
                                public static final Country _indonesia =
                                    new Country(__indonesia,true);
                            
                                public static final Country _iranIslamicRepublicOf =
                                    new Country(__iranIslamicRepublicOf,true);
                            
                                public static final Country _iraq =
                                    new Country(__iraq,true);
                            
                                public static final Country _ireland =
                                    new Country(__ireland,true);
                            
                                public static final Country _isleOfMan =
                                    new Country(__isleOfMan,true);
                            
                                public static final Country _israel =
                                    new Country(__israel,true);
                            
                                public static final Country _italy =
                                    new Country(__italy,true);
                            
                                public static final Country _jamaica =
                                    new Country(__jamaica,true);
                            
                                public static final Country _japan =
                                    new Country(__japan,true);
                            
                                public static final Country _jersey =
                                    new Country(__jersey,true);
                            
                                public static final Country _jordan =
                                    new Country(__jordan,true);
                            
                                public static final Country _kazakhstan =
                                    new Country(__kazakhstan,true);
                            
                                public static final Country _kenya =
                                    new Country(__kenya,true);
                            
                                public static final Country _kiribati =
                                    new Country(__kiribati,true);
                            
                                public static final Country _koreaDemocraticPeoplesRepublic =
                                    new Country(__koreaDemocraticPeoplesRepublic,true);
                            
                                public static final Country _koreaRepublicOf =
                                    new Country(__koreaRepublicOf,true);
                            
                                public static final Country _kosovo =
                                    new Country(__kosovo,true);
                            
                                public static final Country _kuwait =
                                    new Country(__kuwait,true);
                            
                                public static final Country _kyrgyzstan =
                                    new Country(__kyrgyzstan,true);
                            
                                public static final Country _laoPeoplesDemocraticRepublic =
                                    new Country(__laoPeoplesDemocraticRepublic,true);
                            
                                public static final Country _latvia =
                                    new Country(__latvia,true);
                            
                                public static final Country _lebanon =
                                    new Country(__lebanon,true);
                            
                                public static final Country _lesotho =
                                    new Country(__lesotho,true);
                            
                                public static final Country _liberia =
                                    new Country(__liberia,true);
                            
                                public static final Country _libya =
                                    new Country(__libya,true);
                            
                                public static final Country _liechtenstein =
                                    new Country(__liechtenstein,true);
                            
                                public static final Country _lithuania =
                                    new Country(__lithuania,true);
                            
                                public static final Country _luxembourg =
                                    new Country(__luxembourg,true);
                            
                                public static final Country _macau =
                                    new Country(__macau,true);
                            
                                public static final Country _macedonia =
                                    new Country(__macedonia,true);
                            
                                public static final Country _madagascar =
                                    new Country(__madagascar,true);
                            
                                public static final Country _malawi =
                                    new Country(__malawi,true);
                            
                                public static final Country _malaysia =
                                    new Country(__malaysia,true);
                            
                                public static final Country _maldives =
                                    new Country(__maldives,true);
                            
                                public static final Country _mali =
                                    new Country(__mali,true);
                            
                                public static final Country _malta =
                                    new Country(__malta,true);
                            
                                public static final Country _marshallIslands =
                                    new Country(__marshallIslands,true);
                            
                                public static final Country _martinique =
                                    new Country(__martinique,true);
                            
                                public static final Country _mauritania =
                                    new Country(__mauritania,true);
                            
                                public static final Country _mauritius =
                                    new Country(__mauritius,true);
                            
                                public static final Country _mayotte =
                                    new Country(__mayotte,true);
                            
                                public static final Country _mexico =
                                    new Country(__mexico,true);
                            
                                public static final Country _micronesiaFederalStateOf =
                                    new Country(__micronesiaFederalStateOf,true);
                            
                                public static final Country _moldovaRepublicOf =
                                    new Country(__moldovaRepublicOf,true);
                            
                                public static final Country _monaco =
                                    new Country(__monaco,true);
                            
                                public static final Country _mongolia =
                                    new Country(__mongolia,true);
                            
                                public static final Country _montenegro =
                                    new Country(__montenegro,true);
                            
                                public static final Country _montserrat =
                                    new Country(__montserrat,true);
                            
                                public static final Country _morocco =
                                    new Country(__morocco,true);
                            
                                public static final Country _mozambique =
                                    new Country(__mozambique,true);
                            
                                public static final Country _myanmar =
                                    new Country(__myanmar,true);
                            
                                public static final Country _namibia =
                                    new Country(__namibia,true);
                            
                                public static final Country _nauru =
                                    new Country(__nauru,true);
                            
                                public static final Country _nepal =
                                    new Country(__nepal,true);
                            
                                public static final Country _netherlands =
                                    new Country(__netherlands,true);
                            
                                public static final Country _newCaledonia =
                                    new Country(__newCaledonia,true);
                            
                                public static final Country _newZealand =
                                    new Country(__newZealand,true);
                            
                                public static final Country _nicaragua =
                                    new Country(__nicaragua,true);
                            
                                public static final Country _niger =
                                    new Country(__niger,true);
                            
                                public static final Country _nigeria =
                                    new Country(__nigeria,true);
                            
                                public static final Country _niue =
                                    new Country(__niue,true);
                            
                                public static final Country _norfolkIsland =
                                    new Country(__norfolkIsland,true);
                            
                                public static final Country _northernMarianaIslands =
                                    new Country(__northernMarianaIslands,true);
                            
                                public static final Country _norway =
                                    new Country(__norway,true);
                            
                                public static final Country _oman =
                                    new Country(__oman,true);
                            
                                public static final Country _pakistan =
                                    new Country(__pakistan,true);
                            
                                public static final Country _palau =
                                    new Country(__palau,true);
                            
                                public static final Country _panama =
                                    new Country(__panama,true);
                            
                                public static final Country _papuaNewGuinea =
                                    new Country(__papuaNewGuinea,true);
                            
                                public static final Country _paraguay =
                                    new Country(__paraguay,true);
                            
                                public static final Country _peru =
                                    new Country(__peru,true);
                            
                                public static final Country _philippines =
                                    new Country(__philippines,true);
                            
                                public static final Country _pitcairnIsland =
                                    new Country(__pitcairnIsland,true);
                            
                                public static final Country _poland =
                                    new Country(__poland,true);
                            
                                public static final Country _portugal =
                                    new Country(__portugal,true);
                            
                                public static final Country _puertoRico =
                                    new Country(__puertoRico,true);
                            
                                public static final Country _qatar =
                                    new Country(__qatar,true);
                            
                                public static final Country _reunionIsland =
                                    new Country(__reunionIsland,true);
                            
                                public static final Country _romania =
                                    new Country(__romania,true);
                            
                                public static final Country _russianFederation =
                                    new Country(__russianFederation,true);
                            
                                public static final Country _rwanda =
                                    new Country(__rwanda,true);
                            
                                public static final Country _saintBarthelemy =
                                    new Country(__saintBarthelemy,true);
                            
                                public static final Country _saintHelena =
                                    new Country(__saintHelena,true);
                            
                                public static final Country _saintKittsAndNevis =
                                    new Country(__saintKittsAndNevis,true);
                            
                                public static final Country _saintLucia =
                                    new Country(__saintLucia,true);
                            
                                public static final Country _saintMartin =
                                    new Country(__saintMartin,true);
                            
                                public static final Country _saintVincentAndTheGrenadines =
                                    new Country(__saintVincentAndTheGrenadines,true);
                            
                                public static final Country _samoa =
                                    new Country(__samoa,true);
                            
                                public static final Country _sanMarino =
                                    new Country(__sanMarino,true);
                            
                                public static final Country _saoTomeAndPrincipe =
                                    new Country(__saoTomeAndPrincipe,true);
                            
                                public static final Country _saudiArabia =
                                    new Country(__saudiArabia,true);
                            
                                public static final Country _senegal =
                                    new Country(__senegal,true);
                            
                                public static final Country _serbia =
                                    new Country(__serbia,true);
                            
                                public static final Country _seychelles =
                                    new Country(__seychelles,true);
                            
                                public static final Country _sierraLeone =
                                    new Country(__sierraLeone,true);
                            
                                public static final Country _singapore =
                                    new Country(__singapore,true);
                            
                                public static final Country _sintMaarten =
                                    new Country(__sintMaarten,true);
                            
                                public static final Country _slovakRepublic =
                                    new Country(__slovakRepublic,true);
                            
                                public static final Country _slovenia =
                                    new Country(__slovenia,true);
                            
                                public static final Country _solomonIslands =
                                    new Country(__solomonIslands,true);
                            
                                public static final Country _somalia =
                                    new Country(__somalia,true);
                            
                                public static final Country _southAfrica =
                                    new Country(__southAfrica,true);
                            
                                public static final Country _southGeorgia =
                                    new Country(__southGeorgia,true);
                            
                                public static final Country _southSudan =
                                    new Country(__southSudan,true);
                            
                                public static final Country _spain =
                                    new Country(__spain,true);
                            
                                public static final Country _sriLanka =
                                    new Country(__sriLanka,true);
                            
                                public static final Country _stateOfPalestine =
                                    new Country(__stateOfPalestine,true);
                            
                                public static final Country _stPierreAndMiquelon =
                                    new Country(__stPierreAndMiquelon,true);
                            
                                public static final Country _sudan =
                                    new Country(__sudan,true);
                            
                                public static final Country _suriname =
                                    new Country(__suriname,true);
                            
                                public static final Country _svalbardAndJanMayenIslands =
                                    new Country(__svalbardAndJanMayenIslands,true);
                            
                                public static final Country _swaziland =
                                    new Country(__swaziland,true);
                            
                                public static final Country _sweden =
                                    new Country(__sweden,true);
                            
                                public static final Country _switzerland =
                                    new Country(__switzerland,true);
                            
                                public static final Country _syrianArabRepublic =
                                    new Country(__syrianArabRepublic,true);
                            
                                public static final Country _taiwan =
                                    new Country(__taiwan,true);
                            
                                public static final Country _tajikistan =
                                    new Country(__tajikistan,true);
                            
                                public static final Country _tanzania =
                                    new Country(__tanzania,true);
                            
                                public static final Country _thailand =
                                    new Country(__thailand,true);
                            
                                public static final Country _togo =
                                    new Country(__togo,true);
                            
                                public static final Country _tokelau =
                                    new Country(__tokelau,true);
                            
                                public static final Country _tonga =
                                    new Country(__tonga,true);
                            
                                public static final Country _trinidadAndTobago =
                                    new Country(__trinidadAndTobago,true);
                            
                                public static final Country _tunisia =
                                    new Country(__tunisia,true);
                            
                                public static final Country _turkey =
                                    new Country(__turkey,true);
                            
                                public static final Country _turkmenistan =
                                    new Country(__turkmenistan,true);
                            
                                public static final Country _turksAndCaicosIslands =
                                    new Country(__turksAndCaicosIslands,true);
                            
                                public static final Country _tuvalu =
                                    new Country(__tuvalu,true);
                            
                                public static final Country _uganda =
                                    new Country(__uganda,true);
                            
                                public static final Country _ukraine =
                                    new Country(__ukraine,true);
                            
                                public static final Country _unitedArabEmirates =
                                    new Country(__unitedArabEmirates,true);
                            
                                public static final Country _unitedKingdom =
                                    new Country(__unitedKingdom,true);
                            
                                public static final Country _unitedStates =
                                    new Country(__unitedStates,true);
                            
                                public static final Country _uruguay =
                                    new Country(__uruguay,true);
                            
                                public static final Country _uSMinorOutlyingIslands =
                                    new Country(__uSMinorOutlyingIslands,true);
                            
                                public static final Country _uzbekistan =
                                    new Country(__uzbekistan,true);
                            
                                public static final Country _vanuatu =
                                    new Country(__vanuatu,true);
                            
                                public static final Country _venezuela =
                                    new Country(__venezuela,true);
                            
                                public static final Country _vietnam =
                                    new Country(__vietnam,true);
                            
                                public static final Country _virginIslandsBritish =
                                    new Country(__virginIslandsBritish,true);
                            
                                public static final Country _virginIslandsUSA =
                                    new Country(__virginIslandsUSA,true);
                            
                                public static final Country _wallisAndFutunaIslands =
                                    new Country(__wallisAndFutunaIslands,true);
                            
                                public static final Country _westernSahara =
                                    new Country(__westernSahara,true);
                            
                                public static final Country _yemen =
                                    new Country(__yemen,true);
                            
                                public static final Country _zambia =
                                    new Country(__zambia,true);
                            
                                public static final Country _zimbabwe =
                                    new Country(__zimbabwe,true);
                            

                                public java.lang.String getValue() { return localCountry;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localCountry.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.common_2017_2.platform.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":Country",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "Country",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localCountry==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("Country cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localCountry);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.common_2017_2.platform.webservices.netsuite.com")){
                return "ns6";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCountry)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static Country fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    Country enumeration = (Country)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static Country fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static Country fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return Country.Factory.fromString(content,namespaceUri);
                    } else {
                       return Country.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static Country parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            Country object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"Country" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = Country.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = Country.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    