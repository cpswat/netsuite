
/**
 * TimeEntrySearchRowBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  TimeEntrySearchRowBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class TimeEntrySearchRowBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRowBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = TimeEntrySearchRowBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for ApprovalStatus
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localApprovalStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localApprovalStatusTracker = false ;

                           public boolean isApprovalStatusSpecified(){
                               return localApprovalStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getApprovalStatus(){
                               return localApprovalStatus;
                           }

                           
                        


                               
                              /**
                               * validate the array for ApprovalStatus
                               */
                              protected void validateApprovalStatus(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param ApprovalStatus
                              */
                              public void setApprovalStatus(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateApprovalStatus(param);

                               localApprovalStatusTracker = param != null;
                                      
                                      this.localApprovalStatus=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addApprovalStatus(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localApprovalStatus == null){
                                   localApprovalStatus = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localApprovalStatusTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localApprovalStatus);
                               list.add(param);
                               this.localApprovalStatus =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for BillingClass
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localBillingClass ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillingClassTracker = false ;

                           public boolean isBillingClassSpecified(){
                               return localBillingClassTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getBillingClass(){
                               return localBillingClass;
                           }

                           
                        


                               
                              /**
                               * validate the array for BillingClass
                               */
                              protected void validateBillingClass(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param BillingClass
                              */
                              public void setBillingClass(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateBillingClass(param);

                               localBillingClassTracker = param != null;
                                      
                                      this.localBillingClass=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addBillingClass(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localBillingClass == null){
                                   localBillingClass = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localBillingClassTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localBillingClass);
                               list.add(param);
                               this.localBillingClass =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for BillingStatus
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localBillingStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillingStatusTracker = false ;

                           public boolean isBillingStatusSpecified(){
                               return localBillingStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getBillingStatus(){
                               return localBillingStatus;
                           }

                           
                        


                               
                              /**
                               * validate the array for BillingStatus
                               */
                              protected void validateBillingStatus(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param BillingStatus
                              */
                              public void setBillingStatus(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateBillingStatus(param);

                               localBillingStatusTracker = param != null;
                                      
                                      this.localBillingStatus=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addBillingStatus(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localBillingStatus == null){
                                   localBillingStatus = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localBillingStatusTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localBillingStatus);
                               list.add(param);
                               this.localBillingStatus =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for _break
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] local_break ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_breakTracker = false ;

                           public boolean is_breakSpecified(){
                               return local_breakTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] get_break(){
                               return local_break;
                           }

                           
                        


                               
                              /**
                               * validate the array for _break
                               */
                              protected void validate_break(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param _break
                              */
                              public void set_break(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validate_break(param);

                               local_breakTracker = param != null;
                                      
                                      this.local_break=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void add_break(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (local_break == null){
                                   local_break = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                local_breakTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(local_break);
                               list.add(param);
                               this.local_break =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for CaseTaskEvent
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localCaseTaskEvent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCaseTaskEventTracker = false ;

                           public boolean isCaseTaskEventSpecified(){
                               return localCaseTaskEventTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getCaseTaskEvent(){
                               return localCaseTaskEvent;
                           }

                           
                        


                               
                              /**
                               * validate the array for CaseTaskEvent
                               */
                              protected void validateCaseTaskEvent(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param CaseTaskEvent
                              */
                              public void setCaseTaskEvent(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateCaseTaskEvent(param);

                               localCaseTaskEventTracker = param != null;
                                      
                                      this.localCaseTaskEvent=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addCaseTaskEvent(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localCaseTaskEvent == null){
                                   localCaseTaskEvent = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localCaseTaskEventTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCaseTaskEvent);
                               list.add(param);
                               this.localCaseTaskEvent =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for _class
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] get_class(){
                               return local_class;
                           }

                           
                        


                               
                              /**
                               * validate the array for _class
                               */
                              protected void validate_class(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param _class
                              */
                              public void set_class(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validate_class(param);

                               local_classTracker = param != null;
                                      
                                      this.local_class=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void add_class(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (local_class == null){
                                   local_class = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                local_classTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(local_class);
                               list.add(param);
                               this.local_class =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for ClassNoHierarchy
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localClassNoHierarchy ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localClassNoHierarchyTracker = false ;

                           public boolean isClassNoHierarchySpecified(){
                               return localClassNoHierarchyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getClassNoHierarchy(){
                               return localClassNoHierarchy;
                           }

                           
                        


                               
                              /**
                               * validate the array for ClassNoHierarchy
                               */
                              protected void validateClassNoHierarchy(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param ClassNoHierarchy
                              */
                              public void setClassNoHierarchy(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateClassNoHierarchy(param);

                               localClassNoHierarchyTracker = param != null;
                                      
                                      this.localClassNoHierarchy=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addClassNoHierarchy(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localClassNoHierarchy == null){
                                   localClassNoHierarchy = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localClassNoHierarchyTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localClassNoHierarchy);
                               list.add(param);
                               this.localClassNoHierarchy =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for Customer
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localCustomer ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomerTracker = false ;

                           public boolean isCustomerSpecified(){
                               return localCustomerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getCustomer(){
                               return localCustomer;
                           }

                           
                        


                               
                              /**
                               * validate the array for Customer
                               */
                              protected void validateCustomer(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Customer
                              */
                              public void setCustomer(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateCustomer(param);

                               localCustomerTracker = param != null;
                                      
                                      this.localCustomer=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addCustomer(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localCustomer == null){
                                   localCustomer = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localCustomerTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCustomer);
                               list.add(param);
                               this.localCustomer =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for Date
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDateTracker = false ;

                           public boolean isDateSpecified(){
                               return localDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getDate(){
                               return localDate;
                           }

                           
                        


                               
                              /**
                               * validate the array for Date
                               */
                              protected void validateDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Date
                              */
                              public void setDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateDate(param);

                               localDateTracker = param != null;
                                      
                                      this.localDate=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localDate == null){
                                   localDate = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localDateTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localDate);
                               list.add(param);
                               this.localDate =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for DateCreated
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localDateCreated ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDateCreatedTracker = false ;

                           public boolean isDateCreatedSpecified(){
                               return localDateCreatedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getDateCreated(){
                               return localDateCreated;
                           }

                           
                        


                               
                              /**
                               * validate the array for DateCreated
                               */
                              protected void validateDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param DateCreated
                              */
                              public void setDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateDateCreated(param);

                               localDateCreatedTracker = param != null;
                                      
                                      this.localDateCreated=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localDateCreated == null){
                                   localDateCreated = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localDateCreatedTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localDateCreated);
                               list.add(param);
                               this.localDateCreated =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for Department
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getDepartment(){
                               return localDepartment;
                           }

                           
                        


                               
                              /**
                               * validate the array for Department
                               */
                              protected void validateDepartment(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Department
                              */
                              public void setDepartment(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateDepartment(param);

                               localDepartmentTracker = param != null;
                                      
                                      this.localDepartment=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addDepartment(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localDepartment == null){
                                   localDepartment = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localDepartmentTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localDepartment);
                               list.add(param);
                               this.localDepartment =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for DepartmentNoHierarchy
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localDepartmentNoHierarchy ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentNoHierarchyTracker = false ;

                           public boolean isDepartmentNoHierarchySpecified(){
                               return localDepartmentNoHierarchyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getDepartmentNoHierarchy(){
                               return localDepartmentNoHierarchy;
                           }

                           
                        


                               
                              /**
                               * validate the array for DepartmentNoHierarchy
                               */
                              protected void validateDepartmentNoHierarchy(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param DepartmentNoHierarchy
                              */
                              public void setDepartmentNoHierarchy(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateDepartmentNoHierarchy(param);

                               localDepartmentNoHierarchyTracker = param != null;
                                      
                                      this.localDepartmentNoHierarchy=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addDepartmentNoHierarchy(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localDepartmentNoHierarchy == null){
                                   localDepartmentNoHierarchy = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localDepartmentNoHierarchyTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localDepartmentNoHierarchy);
                               list.add(param);
                               this.localDepartmentNoHierarchy =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for DurationDecimal
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localDurationDecimal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDurationDecimalTracker = false ;

                           public boolean isDurationDecimalSpecified(){
                               return localDurationDecimalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getDurationDecimal(){
                               return localDurationDecimal;
                           }

                           
                        


                               
                              /**
                               * validate the array for DurationDecimal
                               */
                              protected void validateDurationDecimal(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param DurationDecimal
                              */
                              public void setDurationDecimal(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateDurationDecimal(param);

                               localDurationDecimalTracker = param != null;
                                      
                                      this.localDurationDecimal=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addDurationDecimal(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localDurationDecimal == null){
                                   localDurationDecimal = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localDurationDecimalTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localDurationDecimal);
                               list.add(param);
                               this.localDurationDecimal =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for Employee
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localEmployee ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmployeeTracker = false ;

                           public boolean isEmployeeSpecified(){
                               return localEmployeeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getEmployee(){
                               return localEmployee;
                           }

                           
                        


                               
                              /**
                               * validate the array for Employee
                               */
                              protected void validateEmployee(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Employee
                              */
                              public void setEmployee(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateEmployee(param);

                               localEmployeeTracker = param != null;
                                      
                                      this.localEmployee=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addEmployee(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localEmployee == null){
                                   localEmployee = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localEmployeeTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localEmployee);
                               list.add(param);
                               this.localEmployee =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for EndTime
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localEndTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndTimeTracker = false ;

                           public boolean isEndTimeSpecified(){
                               return localEndTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getEndTime(){
                               return localEndTime;
                           }

                           
                        


                               
                              /**
                               * validate the array for EndTime
                               */
                              protected void validateEndTime(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param EndTime
                              */
                              public void setEndTime(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateEndTime(param);

                               localEndTimeTracker = param != null;
                                      
                                      this.localEndTime=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addEndTime(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localEndTime == null){
                                   localEndTime = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localEndTimeTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localEndTime);
                               list.add(param);
                               this.localEndTime =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for ExternalId
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localExternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdTracker = false ;

                           public boolean isExternalIdSpecified(){
                               return localExternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getExternalId(){
                               return localExternalId;
                           }

                           
                        


                               
                              /**
                               * validate the array for ExternalId
                               */
                              protected void validateExternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param ExternalId
                              */
                              public void setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateExternalId(param);

                               localExternalIdTracker = param != null;
                                      
                                      this.localExternalId=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addExternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localExternalId == null){
                                   localExternalId = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localExternalIdTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localExternalId);
                               list.add(param);
                               this.localExternalId =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for Hours
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localHours ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHoursTracker = false ;

                           public boolean isHoursSpecified(){
                               return localHoursTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getHours(){
                               return localHours;
                           }

                           
                        


                               
                              /**
                               * validate the array for Hours
                               */
                              protected void validateHours(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Hours
                              */
                              public void setHours(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateHours(param);

                               localHoursTracker = param != null;
                                      
                                      this.localHours=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addHours(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localHours == null){
                                   localHours = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localHoursTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localHours);
                               list.add(param);
                               this.localHours =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for InternalId
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getInternalId(){
                               return localInternalId;
                           }

                           
                        


                               
                              /**
                               * validate the array for InternalId
                               */
                              protected void validateInternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param InternalId
                              */
                              public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateInternalId(param);

                               localInternalIdTracker = param != null;
                                      
                                      this.localInternalId=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addInternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localInternalId == null){
                                   localInternalId = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localInternalIdTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localInternalId);
                               list.add(param);
                               this.localInternalId =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for IsBillable
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localIsBillable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsBillableTracker = false ;

                           public boolean isIsBillableSpecified(){
                               return localIsBillableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getIsBillable(){
                               return localIsBillable;
                           }

                           
                        


                               
                              /**
                               * validate the array for IsBillable
                               */
                              protected void validateIsBillable(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param IsBillable
                              */
                              public void setIsBillable(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateIsBillable(param);

                               localIsBillableTracker = param != null;
                                      
                                      this.localIsBillable=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addIsBillable(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localIsBillable == null){
                                   localIsBillable = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localIsBillableTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localIsBillable);
                               list.add(param);
                               this.localIsBillable =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for IsExempt
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localIsExempt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsExemptTracker = false ;

                           public boolean isIsExemptSpecified(){
                               return localIsExemptTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getIsExempt(){
                               return localIsExempt;
                           }

                           
                        


                               
                              /**
                               * validate the array for IsExempt
                               */
                              protected void validateIsExempt(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param IsExempt
                              */
                              public void setIsExempt(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateIsExempt(param);

                               localIsExemptTracker = param != null;
                                      
                                      this.localIsExempt=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addIsExempt(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localIsExempt == null){
                                   localIsExempt = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localIsExemptTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localIsExempt);
                               list.add(param);
                               this.localIsExempt =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for IsProductive
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localIsProductive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsProductiveTracker = false ;

                           public boolean isIsProductiveSpecified(){
                               return localIsProductiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getIsProductive(){
                               return localIsProductive;
                           }

                           
                        


                               
                              /**
                               * validate the array for IsProductive
                               */
                              protected void validateIsProductive(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param IsProductive
                              */
                              public void setIsProductive(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateIsProductive(param);

                               localIsProductiveTracker = param != null;
                                      
                                      this.localIsProductive=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addIsProductive(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localIsProductive == null){
                                   localIsProductive = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localIsProductiveTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localIsProductive);
                               list.add(param);
                               this.localIsProductive =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for IsUtilized
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localIsUtilized ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsUtilizedTracker = false ;

                           public boolean isIsUtilizedSpecified(){
                               return localIsUtilizedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getIsUtilized(){
                               return localIsUtilized;
                           }

                           
                        


                               
                              /**
                               * validate the array for IsUtilized
                               */
                              protected void validateIsUtilized(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param IsUtilized
                              */
                              public void setIsUtilized(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateIsUtilized(param);

                               localIsUtilizedTracker = param != null;
                                      
                                      this.localIsUtilized=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addIsUtilized(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localIsUtilized == null){
                                   localIsUtilized = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localIsUtilizedTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localIsUtilized);
                               list.add(param);
                               this.localIsUtilized =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for Item
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localItemTracker = false ;

                           public boolean isItemSpecified(){
                               return localItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getItem(){
                               return localItem;
                           }

                           
                        


                               
                              /**
                               * validate the array for Item
                               */
                              protected void validateItem(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Item
                              */
                              public void setItem(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateItem(param);

                               localItemTracker = param != null;
                                      
                                      this.localItem=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addItem(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localItem == null){
                                   localItem = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localItemTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localItem);
                               list.add(param);
                               this.localItem =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for LastModified
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localLastModified ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedTracker = false ;

                           public boolean isLastModifiedSpecified(){
                               return localLastModifiedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getLastModified(){
                               return localLastModified;
                           }

                           
                        


                               
                              /**
                               * validate the array for LastModified
                               */
                              protected void validateLastModified(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param LastModified
                              */
                              public void setLastModified(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateLastModified(param);

                               localLastModifiedTracker = param != null;
                                      
                                      this.localLastModified=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addLastModified(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localLastModified == null){
                                   localLastModified = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localLastModifiedTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localLastModified);
                               list.add(param);
                               this.localLastModified =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for Location
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getLocation(){
                               return localLocation;
                           }

                           
                        


                               
                              /**
                               * validate the array for Location
                               */
                              protected void validateLocation(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Location
                              */
                              public void setLocation(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateLocation(param);

                               localLocationTracker = param != null;
                                      
                                      this.localLocation=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addLocation(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localLocation == null){
                                   localLocation = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localLocationTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localLocation);
                               list.add(param);
                               this.localLocation =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for LocationNoHierarchy
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localLocationNoHierarchy ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationNoHierarchyTracker = false ;

                           public boolean isLocationNoHierarchySpecified(){
                               return localLocationNoHierarchyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getLocationNoHierarchy(){
                               return localLocationNoHierarchy;
                           }

                           
                        


                               
                              /**
                               * validate the array for LocationNoHierarchy
                               */
                              protected void validateLocationNoHierarchy(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param LocationNoHierarchy
                              */
                              public void setLocationNoHierarchy(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateLocationNoHierarchy(param);

                               localLocationNoHierarchyTracker = param != null;
                                      
                                      this.localLocationNoHierarchy=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addLocationNoHierarchy(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localLocationNoHierarchy == null){
                                   localLocationNoHierarchy = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localLocationNoHierarchyTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localLocationNoHierarchy);
                               list.add(param);
                               this.localLocationNoHierarchy =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for Memo
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localMemo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMemoTracker = false ;

                           public boolean isMemoSpecified(){
                               return localMemoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getMemo(){
                               return localMemo;
                           }

                           
                        


                               
                              /**
                               * validate the array for Memo
                               */
                              protected void validateMemo(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Memo
                              */
                              public void setMemo(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateMemo(param);

                               localMemoTracker = param != null;
                                      
                                      this.localMemo=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addMemo(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localMemo == null){
                                   localMemo = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localMemoTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localMemo);
                               list.add(param);
                               this.localMemo =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for NextApprover
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localNextApprover ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNextApproverTracker = false ;

                           public boolean isNextApproverSpecified(){
                               return localNextApproverTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getNextApprover(){
                               return localNextApprover;
                           }

                           
                        


                               
                              /**
                               * validate the array for NextApprover
                               */
                              protected void validateNextApprover(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param NextApprover
                              */
                              public void setNextApprover(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateNextApprover(param);

                               localNextApproverTracker = param != null;
                                      
                                      this.localNextApprover=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addNextApprover(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localNextApprover == null){
                                   localNextApprover = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localNextApproverTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localNextApprover);
                               list.add(param);
                               this.localNextApprover =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for PaidExternally
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localPaidExternally ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPaidExternallyTracker = false ;

                           public boolean isPaidExternallySpecified(){
                               return localPaidExternallyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getPaidExternally(){
                               return localPaidExternally;
                           }

                           
                        


                               
                              /**
                               * validate the array for PaidExternally
                               */
                              protected void validatePaidExternally(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param PaidExternally
                              */
                              public void setPaidExternally(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validatePaidExternally(param);

                               localPaidExternallyTracker = param != null;
                                      
                                      this.localPaidExternally=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addPaidExternally(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localPaidExternally == null){
                                   localPaidExternally = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localPaidExternallyTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localPaidExternally);
                               list.add(param);
                               this.localPaidExternally =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for PayItem
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localPayItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPayItemTracker = false ;

                           public boolean isPayItemSpecified(){
                               return localPayItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getPayItem(){
                               return localPayItem;
                           }

                           
                        


                               
                              /**
                               * validate the array for PayItem
                               */
                              protected void validatePayItem(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param PayItem
                              */
                              public void setPayItem(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validatePayItem(param);

                               localPayItemTracker = param != null;
                                      
                                      this.localPayItem=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addPayItem(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localPayItem == null){
                                   localPayItem = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localPayItemTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localPayItem);
                               list.add(param);
                               this.localPayItem =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for PayrollDate
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localPayrollDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPayrollDateTracker = false ;

                           public boolean isPayrollDateSpecified(){
                               return localPayrollDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getPayrollDate(){
                               return localPayrollDate;
                           }

                           
                        


                               
                              /**
                               * validate the array for PayrollDate
                               */
                              protected void validatePayrollDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param PayrollDate
                              */
                              public void setPayrollDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validatePayrollDate(param);

                               localPayrollDateTracker = param != null;
                                      
                                      this.localPayrollDate=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addPayrollDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localPayrollDate == null){
                                   localPayrollDate = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localPayrollDateTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localPayrollDate);
                               list.add(param);
                               this.localPayrollDate =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for Rate
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRateTracker = false ;

                           public boolean isRateSpecified(){
                               return localRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getRate(){
                               return localRate;
                           }

                           
                        


                               
                              /**
                               * validate the array for Rate
                               */
                              protected void validateRate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Rate
                              */
                              public void setRate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateRate(param);

                               localRateTracker = param != null;
                                      
                                      this.localRate=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addRate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localRate == null){
                                   localRate = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localRateTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localRate);
                               list.add(param);
                               this.localRate =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for StartTime
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localStartTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartTimeTracker = false ;

                           public boolean isStartTimeSpecified(){
                               return localStartTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getStartTime(){
                               return localStartTime;
                           }

                           
                        


                               
                              /**
                               * validate the array for StartTime
                               */
                              protected void validateStartTime(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param StartTime
                              */
                              public void setStartTime(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateStartTime(param);

                               localStartTimeTracker = param != null;
                                      
                                      this.localStartTime=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addStartTime(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localStartTime == null){
                                   localStartTime = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localStartTimeTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localStartTime);
                               list.add(param);
                               this.localStartTime =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for Subsidiary
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        


                               
                              /**
                               * validate the array for Subsidiary
                               */
                              protected void validateSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Subsidiary
                              */
                              public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateSubsidiary(param);

                               localSubsidiaryTracker = param != null;
                                      
                                      this.localSubsidiary=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localSubsidiary == null){
                                   localSubsidiary = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localSubsidiaryTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localSubsidiary);
                               list.add(param);
                               this.localSubsidiary =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for SubsidiaryNoHierarchy
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localSubsidiaryNoHierarchy ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryNoHierarchyTracker = false ;

                           public boolean isSubsidiaryNoHierarchySpecified(){
                               return localSubsidiaryNoHierarchyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getSubsidiaryNoHierarchy(){
                               return localSubsidiaryNoHierarchy;
                           }

                           
                        


                               
                              /**
                               * validate the array for SubsidiaryNoHierarchy
                               */
                              protected void validateSubsidiaryNoHierarchy(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param SubsidiaryNoHierarchy
                              */
                              public void setSubsidiaryNoHierarchy(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateSubsidiaryNoHierarchy(param);

                               localSubsidiaryNoHierarchyTracker = param != null;
                                      
                                      this.localSubsidiaryNoHierarchy=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addSubsidiaryNoHierarchy(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localSubsidiaryNoHierarchy == null){
                                   localSubsidiaryNoHierarchy = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localSubsidiaryNoHierarchyTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localSubsidiaryNoHierarchy);
                               list.add(param);
                               this.localSubsidiaryNoHierarchy =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for TimeSheet
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localTimeSheet ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeSheetTracker = false ;

                           public boolean isTimeSheetSpecified(){
                               return localTimeSheetTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getTimeSheet(){
                               return localTimeSheet;
                           }

                           
                        


                               
                              /**
                               * validate the array for TimeSheet
                               */
                              protected void validateTimeSheet(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param TimeSheet
                              */
                              public void setTimeSheet(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateTimeSheet(param);

                               localTimeSheetTracker = param != null;
                                      
                                      this.localTimeSheet=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addTimeSheet(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localTimeSheet == null){
                                   localTimeSheet = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localTimeSheetTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localTimeSheet);
                               list.add(param);
                               this.localTimeSheet =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for Type
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] localType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTypeTracker = false ;

                           public boolean isTypeSpecified(){
                               return localTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] getType(){
                               return localType;
                           }

                           
                        


                               
                              /**
                               * validate the array for Type
                               */
                              protected void validateType(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Type
                              */
                              public void setType(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                              
                                   validateType(param);

                               localTypeTracker = param != null;
                                      
                                      this.localType=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField
                             */
                             public void addType(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField param){
                                   if (localType == null){
                                   localType = new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localTypeTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localType);
                               list.add(param);
                               this.localType =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[list.size()]);

                             }
                             

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":TimeEntrySearchRowBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "TimeEntrySearchRowBasic",
                           xmlWriter);
                   }

                if (localApprovalStatusTracker){
                                       if (localApprovalStatus!=null){
                                            for (int i = 0;i < localApprovalStatus.length;i++){
                                                if (localApprovalStatus[i] != null){
                                                 localApprovalStatus[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","approvalStatus"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("approvalStatus cannot be null!!");
                                        
                                    }
                                 } if (localBillingClassTracker){
                                       if (localBillingClass!=null){
                                            for (int i = 0;i < localBillingClass.length;i++){
                                                if (localBillingClass[i] != null){
                                                 localBillingClass[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billingClass"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("billingClass cannot be null!!");
                                        
                                    }
                                 } if (localBillingStatusTracker){
                                       if (localBillingStatus!=null){
                                            for (int i = 0;i < localBillingStatus.length;i++){
                                                if (localBillingStatus[i] != null){
                                                 localBillingStatus[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billingStatus"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("billingStatus cannot be null!!");
                                        
                                    }
                                 } if (local_breakTracker){
                                       if (local_break!=null){
                                            for (int i = 0;i < local_break.length;i++){
                                                if (local_break[i] != null){
                                                 local_break[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","break"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("break cannot be null!!");
                                        
                                    }
                                 } if (localCaseTaskEventTracker){
                                       if (localCaseTaskEvent!=null){
                                            for (int i = 0;i < localCaseTaskEvent.length;i++){
                                                if (localCaseTaskEvent[i] != null){
                                                 localCaseTaskEvent[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","caseTaskEvent"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("caseTaskEvent cannot be null!!");
                                        
                                    }
                                 } if (local_classTracker){
                                       if (local_class!=null){
                                            for (int i = 0;i < local_class.length;i++){
                                                if (local_class[i] != null){
                                                 local_class[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","class"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                        
                                    }
                                 } if (localClassNoHierarchyTracker){
                                       if (localClassNoHierarchy!=null){
                                            for (int i = 0;i < localClassNoHierarchy.length;i++){
                                                if (localClassNoHierarchy[i] != null){
                                                 localClassNoHierarchy[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","classNoHierarchy"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("classNoHierarchy cannot be null!!");
                                        
                                    }
                                 } if (localCustomerTracker){
                                       if (localCustomer!=null){
                                            for (int i = 0;i < localCustomer.length;i++){
                                                if (localCustomer[i] != null){
                                                 localCustomer[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customer"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("customer cannot be null!!");
                                        
                                    }
                                 } if (localDateTracker){
                                       if (localDate!=null){
                                            for (int i = 0;i < localDate.length;i++){
                                                if (localDate[i] != null){
                                                 localDate[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","date"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("date cannot be null!!");
                                        
                                    }
                                 } if (localDateCreatedTracker){
                                       if (localDateCreated!=null){
                                            for (int i = 0;i < localDateCreated.length;i++){
                                                if (localDateCreated[i] != null){
                                                 localDateCreated[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                        
                                    }
                                 } if (localDepartmentTracker){
                                       if (localDepartment!=null){
                                            for (int i = 0;i < localDepartment.length;i++){
                                                if (localDepartment[i] != null){
                                                 localDepartment[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","department"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                        
                                    }
                                 } if (localDepartmentNoHierarchyTracker){
                                       if (localDepartmentNoHierarchy!=null){
                                            for (int i = 0;i < localDepartmentNoHierarchy.length;i++){
                                                if (localDepartmentNoHierarchy[i] != null){
                                                 localDepartmentNoHierarchy[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","departmentNoHierarchy"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("departmentNoHierarchy cannot be null!!");
                                        
                                    }
                                 } if (localDurationDecimalTracker){
                                       if (localDurationDecimal!=null){
                                            for (int i = 0;i < localDurationDecimal.length;i++){
                                                if (localDurationDecimal[i] != null){
                                                 localDurationDecimal[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","durationDecimal"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("durationDecimal cannot be null!!");
                                        
                                    }
                                 } if (localEmployeeTracker){
                                       if (localEmployee!=null){
                                            for (int i = 0;i < localEmployee.length;i++){
                                                if (localEmployee[i] != null){
                                                 localEmployee[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","employee"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("employee cannot be null!!");
                                        
                                    }
                                 } if (localEndTimeTracker){
                                       if (localEndTime!=null){
                                            for (int i = 0;i < localEndTime.length;i++){
                                                if (localEndTime[i] != null){
                                                 localEndTime[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endTime"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("endTime cannot be null!!");
                                        
                                    }
                                 } if (localExternalIdTracker){
                                       if (localExternalId!=null){
                                            for (int i = 0;i < localExternalId.length;i++){
                                                if (localExternalId[i] != null){
                                                 localExternalId[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                        
                                    }
                                 } if (localHoursTracker){
                                       if (localHours!=null){
                                            for (int i = 0;i < localHours.length;i++){
                                                if (localHours[i] != null){
                                                 localHours[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","hours"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("hours cannot be null!!");
                                        
                                    }
                                 } if (localInternalIdTracker){
                                       if (localInternalId!=null){
                                            for (int i = 0;i < localInternalId.length;i++){
                                                if (localInternalId[i] != null){
                                                 localInternalId[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                        
                                    }
                                 } if (localIsBillableTracker){
                                       if (localIsBillable!=null){
                                            for (int i = 0;i < localIsBillable.length;i++){
                                                if (localIsBillable[i] != null){
                                                 localIsBillable[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isBillable"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("isBillable cannot be null!!");
                                        
                                    }
                                 } if (localIsExemptTracker){
                                       if (localIsExempt!=null){
                                            for (int i = 0;i < localIsExempt.length;i++){
                                                if (localIsExempt[i] != null){
                                                 localIsExempt[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isExempt"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("isExempt cannot be null!!");
                                        
                                    }
                                 } if (localIsProductiveTracker){
                                       if (localIsProductive!=null){
                                            for (int i = 0;i < localIsProductive.length;i++){
                                                if (localIsProductive[i] != null){
                                                 localIsProductive[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isProductive"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("isProductive cannot be null!!");
                                        
                                    }
                                 } if (localIsUtilizedTracker){
                                       if (localIsUtilized!=null){
                                            for (int i = 0;i < localIsUtilized.length;i++){
                                                if (localIsUtilized[i] != null){
                                                 localIsUtilized[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isUtilized"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("isUtilized cannot be null!!");
                                        
                                    }
                                 } if (localItemTracker){
                                       if (localItem!=null){
                                            for (int i = 0;i < localItem.length;i++){
                                                if (localItem[i] != null){
                                                 localItem[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","item"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                        
                                    }
                                 } if (localLastModifiedTracker){
                                       if (localLastModified!=null){
                                            for (int i = 0;i < localLastModified.length;i++){
                                                if (localLastModified[i] != null){
                                                 localLastModified[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModified"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("lastModified cannot be null!!");
                                        
                                    }
                                 } if (localLocationTracker){
                                       if (localLocation!=null){
                                            for (int i = 0;i < localLocation.length;i++){
                                                if (localLocation[i] != null){
                                                 localLocation[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","location"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                        
                                    }
                                 } if (localLocationNoHierarchyTracker){
                                       if (localLocationNoHierarchy!=null){
                                            for (int i = 0;i < localLocationNoHierarchy.length;i++){
                                                if (localLocationNoHierarchy[i] != null){
                                                 localLocationNoHierarchy[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","locationNoHierarchy"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("locationNoHierarchy cannot be null!!");
                                        
                                    }
                                 } if (localMemoTracker){
                                       if (localMemo!=null){
                                            for (int i = 0;i < localMemo.length;i++){
                                                if (localMemo[i] != null){
                                                 localMemo[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","memo"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                        
                                    }
                                 } if (localNextApproverTracker){
                                       if (localNextApprover!=null){
                                            for (int i = 0;i < localNextApprover.length;i++){
                                                if (localNextApprover[i] != null){
                                                 localNextApprover[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nextApprover"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("nextApprover cannot be null!!");
                                        
                                    }
                                 } if (localPaidExternallyTracker){
                                       if (localPaidExternally!=null){
                                            for (int i = 0;i < localPaidExternally.length;i++){
                                                if (localPaidExternally[i] != null){
                                                 localPaidExternally[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","paidExternally"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("paidExternally cannot be null!!");
                                        
                                    }
                                 } if (localPayItemTracker){
                                       if (localPayItem!=null){
                                            for (int i = 0;i < localPayItem.length;i++){
                                                if (localPayItem[i] != null){
                                                 localPayItem[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","payItem"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("payItem cannot be null!!");
                                        
                                    }
                                 } if (localPayrollDateTracker){
                                       if (localPayrollDate!=null){
                                            for (int i = 0;i < localPayrollDate.length;i++){
                                                if (localPayrollDate[i] != null){
                                                 localPayrollDate[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","payrollDate"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("payrollDate cannot be null!!");
                                        
                                    }
                                 } if (localRateTracker){
                                       if (localRate!=null){
                                            for (int i = 0;i < localRate.length;i++){
                                                if (localRate[i] != null){
                                                 localRate[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rate"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("rate cannot be null!!");
                                        
                                    }
                                 } if (localStartTimeTracker){
                                       if (localStartTime!=null){
                                            for (int i = 0;i < localStartTime.length;i++){
                                                if (localStartTime[i] != null){
                                                 localStartTime[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startTime"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("startTime cannot be null!!");
                                        
                                    }
                                 } if (localSubsidiaryTracker){
                                       if (localSubsidiary!=null){
                                            for (int i = 0;i < localSubsidiary.length;i++){
                                                if (localSubsidiary[i] != null){
                                                 localSubsidiary[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                        
                                    }
                                 } if (localSubsidiaryNoHierarchyTracker){
                                       if (localSubsidiaryNoHierarchy!=null){
                                            for (int i = 0;i < localSubsidiaryNoHierarchy.length;i++){
                                                if (localSubsidiaryNoHierarchy[i] != null){
                                                 localSubsidiaryNoHierarchy[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiaryNoHierarchy"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("subsidiaryNoHierarchy cannot be null!!");
                                        
                                    }
                                 } if (localTimeSheetTracker){
                                       if (localTimeSheet!=null){
                                            for (int i = 0;i < localTimeSheet.length;i++){
                                                if (localTimeSheet[i] != null){
                                                 localTimeSheet[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","timeSheet"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("timeSheet cannot be null!!");
                                        
                                    }
                                 } if (localTypeTracker){
                                       if (localType!=null){
                                            for (int i = 0;i < localType.length;i++){
                                                if (localType[i] != null){
                                                 localType[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","type"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("type cannot be null!!");
                                        
                                    }
                                 } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","TimeEntrySearchRowBasic"));
                 if (localApprovalStatusTracker){
                             if (localApprovalStatus!=null) {
                                 for (int i = 0;i < localApprovalStatus.length;i++){

                                    if (localApprovalStatus[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "approvalStatus"));
                                         elementList.add(localApprovalStatus[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("approvalStatus cannot be null!!");
                                    
                             }

                        } if (localBillingClassTracker){
                             if (localBillingClass!=null) {
                                 for (int i = 0;i < localBillingClass.length;i++){

                                    if (localBillingClass[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "billingClass"));
                                         elementList.add(localBillingClass[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("billingClass cannot be null!!");
                                    
                             }

                        } if (localBillingStatusTracker){
                             if (localBillingStatus!=null) {
                                 for (int i = 0;i < localBillingStatus.length;i++){

                                    if (localBillingStatus[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "billingStatus"));
                                         elementList.add(localBillingStatus[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("billingStatus cannot be null!!");
                                    
                             }

                        } if (local_breakTracker){
                             if (local_break!=null) {
                                 for (int i = 0;i < local_break.length;i++){

                                    if (local_break[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "break"));
                                         elementList.add(local_break[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("break cannot be null!!");
                                    
                             }

                        } if (localCaseTaskEventTracker){
                             if (localCaseTaskEvent!=null) {
                                 for (int i = 0;i < localCaseTaskEvent.length;i++){

                                    if (localCaseTaskEvent[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "caseTaskEvent"));
                                         elementList.add(localCaseTaskEvent[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("caseTaskEvent cannot be null!!");
                                    
                             }

                        } if (local_classTracker){
                             if (local_class!=null) {
                                 for (int i = 0;i < local_class.length;i++){

                                    if (local_class[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "class"));
                                         elementList.add(local_class[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    
                             }

                        } if (localClassNoHierarchyTracker){
                             if (localClassNoHierarchy!=null) {
                                 for (int i = 0;i < localClassNoHierarchy.length;i++){

                                    if (localClassNoHierarchy[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "classNoHierarchy"));
                                         elementList.add(localClassNoHierarchy[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("classNoHierarchy cannot be null!!");
                                    
                             }

                        } if (localCustomerTracker){
                             if (localCustomer!=null) {
                                 for (int i = 0;i < localCustomer.length;i++){

                                    if (localCustomer[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "customer"));
                                         elementList.add(localCustomer[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("customer cannot be null!!");
                                    
                             }

                        } if (localDateTracker){
                             if (localDate!=null) {
                                 for (int i = 0;i < localDate.length;i++){

                                    if (localDate[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "date"));
                                         elementList.add(localDate[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("date cannot be null!!");
                                    
                             }

                        } if (localDateCreatedTracker){
                             if (localDateCreated!=null) {
                                 for (int i = 0;i < localDateCreated.length;i++){

                                    if (localDateCreated[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "dateCreated"));
                                         elementList.add(localDateCreated[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                    
                             }

                        } if (localDepartmentTracker){
                             if (localDepartment!=null) {
                                 for (int i = 0;i < localDepartment.length;i++){

                                    if (localDepartment[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "department"));
                                         elementList.add(localDepartment[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    
                             }

                        } if (localDepartmentNoHierarchyTracker){
                             if (localDepartmentNoHierarchy!=null) {
                                 for (int i = 0;i < localDepartmentNoHierarchy.length;i++){

                                    if (localDepartmentNoHierarchy[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "departmentNoHierarchy"));
                                         elementList.add(localDepartmentNoHierarchy[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("departmentNoHierarchy cannot be null!!");
                                    
                             }

                        } if (localDurationDecimalTracker){
                             if (localDurationDecimal!=null) {
                                 for (int i = 0;i < localDurationDecimal.length;i++){

                                    if (localDurationDecimal[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "durationDecimal"));
                                         elementList.add(localDurationDecimal[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("durationDecimal cannot be null!!");
                                    
                             }

                        } if (localEmployeeTracker){
                             if (localEmployee!=null) {
                                 for (int i = 0;i < localEmployee.length;i++){

                                    if (localEmployee[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "employee"));
                                         elementList.add(localEmployee[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("employee cannot be null!!");
                                    
                             }

                        } if (localEndTimeTracker){
                             if (localEndTime!=null) {
                                 for (int i = 0;i < localEndTime.length;i++){

                                    if (localEndTime[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "endTime"));
                                         elementList.add(localEndTime[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("endTime cannot be null!!");
                                    
                             }

                        } if (localExternalIdTracker){
                             if (localExternalId!=null) {
                                 for (int i = 0;i < localExternalId.length;i++){

                                    if (localExternalId[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "externalId"));
                                         elementList.add(localExternalId[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                    
                             }

                        } if (localHoursTracker){
                             if (localHours!=null) {
                                 for (int i = 0;i < localHours.length;i++){

                                    if (localHours[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "hours"));
                                         elementList.add(localHours[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("hours cannot be null!!");
                                    
                             }

                        } if (localInternalIdTracker){
                             if (localInternalId!=null) {
                                 for (int i = 0;i < localInternalId.length;i++){

                                    if (localInternalId[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "internalId"));
                                         elementList.add(localInternalId[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    
                             }

                        } if (localIsBillableTracker){
                             if (localIsBillable!=null) {
                                 for (int i = 0;i < localIsBillable.length;i++){

                                    if (localIsBillable[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "isBillable"));
                                         elementList.add(localIsBillable[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("isBillable cannot be null!!");
                                    
                             }

                        } if (localIsExemptTracker){
                             if (localIsExempt!=null) {
                                 for (int i = 0;i < localIsExempt.length;i++){

                                    if (localIsExempt[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "isExempt"));
                                         elementList.add(localIsExempt[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("isExempt cannot be null!!");
                                    
                             }

                        } if (localIsProductiveTracker){
                             if (localIsProductive!=null) {
                                 for (int i = 0;i < localIsProductive.length;i++){

                                    if (localIsProductive[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "isProductive"));
                                         elementList.add(localIsProductive[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("isProductive cannot be null!!");
                                    
                             }

                        } if (localIsUtilizedTracker){
                             if (localIsUtilized!=null) {
                                 for (int i = 0;i < localIsUtilized.length;i++){

                                    if (localIsUtilized[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "isUtilized"));
                                         elementList.add(localIsUtilized[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("isUtilized cannot be null!!");
                                    
                             }

                        } if (localItemTracker){
                             if (localItem!=null) {
                                 for (int i = 0;i < localItem.length;i++){

                                    if (localItem[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "item"));
                                         elementList.add(localItem[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("item cannot be null!!");
                                    
                             }

                        } if (localLastModifiedTracker){
                             if (localLastModified!=null) {
                                 for (int i = 0;i < localLastModified.length;i++){

                                    if (localLastModified[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "lastModified"));
                                         elementList.add(localLastModified[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("lastModified cannot be null!!");
                                    
                             }

                        } if (localLocationTracker){
                             if (localLocation!=null) {
                                 for (int i = 0;i < localLocation.length;i++){

                                    if (localLocation[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "location"));
                                         elementList.add(localLocation[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    
                             }

                        } if (localLocationNoHierarchyTracker){
                             if (localLocationNoHierarchy!=null) {
                                 for (int i = 0;i < localLocationNoHierarchy.length;i++){

                                    if (localLocationNoHierarchy[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "locationNoHierarchy"));
                                         elementList.add(localLocationNoHierarchy[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("locationNoHierarchy cannot be null!!");
                                    
                             }

                        } if (localMemoTracker){
                             if (localMemo!=null) {
                                 for (int i = 0;i < localMemo.length;i++){

                                    if (localMemo[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "memo"));
                                         elementList.add(localMemo[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                    
                             }

                        } if (localNextApproverTracker){
                             if (localNextApprover!=null) {
                                 for (int i = 0;i < localNextApprover.length;i++){

                                    if (localNextApprover[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "nextApprover"));
                                         elementList.add(localNextApprover[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("nextApprover cannot be null!!");
                                    
                             }

                        } if (localPaidExternallyTracker){
                             if (localPaidExternally!=null) {
                                 for (int i = 0;i < localPaidExternally.length;i++){

                                    if (localPaidExternally[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "paidExternally"));
                                         elementList.add(localPaidExternally[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("paidExternally cannot be null!!");
                                    
                             }

                        } if (localPayItemTracker){
                             if (localPayItem!=null) {
                                 for (int i = 0;i < localPayItem.length;i++){

                                    if (localPayItem[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "payItem"));
                                         elementList.add(localPayItem[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("payItem cannot be null!!");
                                    
                             }

                        } if (localPayrollDateTracker){
                             if (localPayrollDate!=null) {
                                 for (int i = 0;i < localPayrollDate.length;i++){

                                    if (localPayrollDate[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "payrollDate"));
                                         elementList.add(localPayrollDate[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("payrollDate cannot be null!!");
                                    
                             }

                        } if (localRateTracker){
                             if (localRate!=null) {
                                 for (int i = 0;i < localRate.length;i++){

                                    if (localRate[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "rate"));
                                         elementList.add(localRate[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("rate cannot be null!!");
                                    
                             }

                        } if (localStartTimeTracker){
                             if (localStartTime!=null) {
                                 for (int i = 0;i < localStartTime.length;i++){

                                    if (localStartTime[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "startTime"));
                                         elementList.add(localStartTime[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("startTime cannot be null!!");
                                    
                             }

                        } if (localSubsidiaryTracker){
                             if (localSubsidiary!=null) {
                                 for (int i = 0;i < localSubsidiary.length;i++){

                                    if (localSubsidiary[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "subsidiary"));
                                         elementList.add(localSubsidiary[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    
                             }

                        } if (localSubsidiaryNoHierarchyTracker){
                             if (localSubsidiaryNoHierarchy!=null) {
                                 for (int i = 0;i < localSubsidiaryNoHierarchy.length;i++){

                                    if (localSubsidiaryNoHierarchy[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "subsidiaryNoHierarchy"));
                                         elementList.add(localSubsidiaryNoHierarchy[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("subsidiaryNoHierarchy cannot be null!!");
                                    
                             }

                        } if (localTimeSheetTracker){
                             if (localTimeSheet!=null) {
                                 for (int i = 0;i < localTimeSheet.length;i++){

                                    if (localTimeSheet[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "timeSheet"));
                                         elementList.add(localTimeSheet[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("timeSheet cannot be null!!");
                                    
                             }

                        } if (localTypeTracker){
                             if (localType!=null) {
                                 for (int i = 0;i < localType.length;i++){

                                    if (localType[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "type"));
                                         elementList.add(localType[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("type cannot be null!!");
                                    
                             }

                        } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static TimeEntrySearchRowBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            TimeEntrySearchRowBasic object =
                new TimeEntrySearchRowBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"TimeEntrySearchRowBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (TimeEntrySearchRowBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                        java.util.ArrayList list1 = new java.util.ArrayList();
                    
                        java.util.ArrayList list2 = new java.util.ArrayList();
                    
                        java.util.ArrayList list3 = new java.util.ArrayList();
                    
                        java.util.ArrayList list4 = new java.util.ArrayList();
                    
                        java.util.ArrayList list5 = new java.util.ArrayList();
                    
                        java.util.ArrayList list6 = new java.util.ArrayList();
                    
                        java.util.ArrayList list7 = new java.util.ArrayList();
                    
                        java.util.ArrayList list8 = new java.util.ArrayList();
                    
                        java.util.ArrayList list9 = new java.util.ArrayList();
                    
                        java.util.ArrayList list10 = new java.util.ArrayList();
                    
                        java.util.ArrayList list11 = new java.util.ArrayList();
                    
                        java.util.ArrayList list12 = new java.util.ArrayList();
                    
                        java.util.ArrayList list13 = new java.util.ArrayList();
                    
                        java.util.ArrayList list14 = new java.util.ArrayList();
                    
                        java.util.ArrayList list15 = new java.util.ArrayList();
                    
                        java.util.ArrayList list16 = new java.util.ArrayList();
                    
                        java.util.ArrayList list17 = new java.util.ArrayList();
                    
                        java.util.ArrayList list18 = new java.util.ArrayList();
                    
                        java.util.ArrayList list19 = new java.util.ArrayList();
                    
                        java.util.ArrayList list20 = new java.util.ArrayList();
                    
                        java.util.ArrayList list21 = new java.util.ArrayList();
                    
                        java.util.ArrayList list22 = new java.util.ArrayList();
                    
                        java.util.ArrayList list23 = new java.util.ArrayList();
                    
                        java.util.ArrayList list24 = new java.util.ArrayList();
                    
                        java.util.ArrayList list25 = new java.util.ArrayList();
                    
                        java.util.ArrayList list26 = new java.util.ArrayList();
                    
                        java.util.ArrayList list27 = new java.util.ArrayList();
                    
                        java.util.ArrayList list28 = new java.util.ArrayList();
                    
                        java.util.ArrayList list29 = new java.util.ArrayList();
                    
                        java.util.ArrayList list30 = new java.util.ArrayList();
                    
                        java.util.ArrayList list31 = new java.util.ArrayList();
                    
                        java.util.ArrayList list32 = new java.util.ArrayList();
                    
                        java.util.ArrayList list33 = new java.util.ArrayList();
                    
                        java.util.ArrayList list34 = new java.util.ArrayList();
                    
                        java.util.ArrayList list35 = new java.util.ArrayList();
                    
                        java.util.ArrayList list36 = new java.util.ArrayList();
                    
                        java.util.ArrayList list37 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","approvalStatus").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list1.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone1 = false;
                                                        while(!loopDone1){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone1 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","approvalStatus").equals(reader.getName())){
                                                                    list1.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone1 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setApprovalStatus((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list1));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billingClass").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list2.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone2 = false;
                                                        while(!loopDone2){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone2 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billingClass").equals(reader.getName())){
                                                                    list2.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone2 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setBillingClass((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list2));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billingStatus").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list3.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone3 = false;
                                                        while(!loopDone3){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone3 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billingStatus").equals(reader.getName())){
                                                                    list3.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone3 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setBillingStatus((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list3));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","break").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list4.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone4 = false;
                                                        while(!loopDone4){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone4 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","break").equals(reader.getName())){
                                                                    list4.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone4 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.set_break((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list4));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","caseTaskEvent").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list5.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone5 = false;
                                                        while(!loopDone5){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone5 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","caseTaskEvent").equals(reader.getName())){
                                                                    list5.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone5 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCaseTaskEvent((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list5));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list6.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone6 = false;
                                                        while(!loopDone6){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone6 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","class").equals(reader.getName())){
                                                                    list6.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone6 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.set_class((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list6));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","classNoHierarchy").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list7.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone7 = false;
                                                        while(!loopDone7){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone7 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","classNoHierarchy").equals(reader.getName())){
                                                                    list7.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone7 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setClassNoHierarchy((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list7));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customer").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list8.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone8 = false;
                                                        while(!loopDone8){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone8 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customer").equals(reader.getName())){
                                                                    list8.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone8 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCustomer((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list8));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","date").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list9.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone9 = false;
                                                        while(!loopDone9){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone9 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","date").equals(reader.getName())){
                                                                    list9.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone9 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setDate((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list9));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list10.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone10 = false;
                                                        while(!loopDone10){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone10 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated").equals(reader.getName())){
                                                                    list10.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone10 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setDateCreated((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list10));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list11.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone11 = false;
                                                        while(!loopDone11){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone11 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","department").equals(reader.getName())){
                                                                    list11.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone11 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setDepartment((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list11));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","departmentNoHierarchy").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list12.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone12 = false;
                                                        while(!loopDone12){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone12 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","departmentNoHierarchy").equals(reader.getName())){
                                                                    list12.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone12 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setDepartmentNoHierarchy((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list12));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","durationDecimal").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list13.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone13 = false;
                                                        while(!loopDone13){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone13 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","durationDecimal").equals(reader.getName())){
                                                                    list13.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone13 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setDurationDecimal((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list13));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","employee").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list14.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone14 = false;
                                                        while(!loopDone14){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone14 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","employee").equals(reader.getName())){
                                                                    list14.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone14 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setEmployee((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list14));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endTime").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list15.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone15 = false;
                                                        while(!loopDone15){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone15 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endTime").equals(reader.getName())){
                                                                    list15.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone15 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setEndTime((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list15));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list16.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone16 = false;
                                                        while(!loopDone16){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone16 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                                                    list16.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone16 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setExternalId((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list16));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","hours").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list17.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone17 = false;
                                                        while(!loopDone17){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone17 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","hours").equals(reader.getName())){
                                                                    list17.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone17 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setHours((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list17));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list18.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone18 = false;
                                                        while(!loopDone18){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone18 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                                                    list18.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone18 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setInternalId((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list18));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isBillable").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list19.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone19 = false;
                                                        while(!loopDone19){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone19 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isBillable").equals(reader.getName())){
                                                                    list19.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone19 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setIsBillable((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list19));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isExempt").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list20.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone20 = false;
                                                        while(!loopDone20){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone20 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isExempt").equals(reader.getName())){
                                                                    list20.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone20 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setIsExempt((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list20));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isProductive").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list21.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone21 = false;
                                                        while(!loopDone21){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone21 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isProductive").equals(reader.getName())){
                                                                    list21.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone21 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setIsProductive((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list21));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isUtilized").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list22.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone22 = false;
                                                        while(!loopDone22){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone22 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isUtilized").equals(reader.getName())){
                                                                    list22.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone22 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setIsUtilized((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list22));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","item").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list23.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone23 = false;
                                                        while(!loopDone23){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone23 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","item").equals(reader.getName())){
                                                                    list23.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone23 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setItem((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list23));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModified").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list24.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone24 = false;
                                                        while(!loopDone24){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone24 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModified").equals(reader.getName())){
                                                                    list24.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone24 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setLastModified((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list24));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list25.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone25 = false;
                                                        while(!loopDone25){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone25 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","location").equals(reader.getName())){
                                                                    list25.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone25 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setLocation((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list25));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","locationNoHierarchy").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list26.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone26 = false;
                                                        while(!loopDone26){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone26 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","locationNoHierarchy").equals(reader.getName())){
                                                                    list26.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone26 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setLocationNoHierarchy((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list26));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","memo").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list27.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone27 = false;
                                                        while(!loopDone27){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone27 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","memo").equals(reader.getName())){
                                                                    list27.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone27 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setMemo((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list27));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nextApprover").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list28.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone28 = false;
                                                        while(!loopDone28){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone28 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nextApprover").equals(reader.getName())){
                                                                    list28.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone28 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setNextApprover((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list28));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","paidExternally").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list29.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone29 = false;
                                                        while(!loopDone29){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone29 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","paidExternally").equals(reader.getName())){
                                                                    list29.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone29 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setPaidExternally((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list29));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","payItem").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list30.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone30 = false;
                                                        while(!loopDone30){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone30 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","payItem").equals(reader.getName())){
                                                                    list30.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone30 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setPayItem((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list30));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","payrollDate").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list31.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone31 = false;
                                                        while(!loopDone31){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone31 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","payrollDate").equals(reader.getName())){
                                                                    list31.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone31 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setPayrollDate((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list31));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rate").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list32.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone32 = false;
                                                        while(!loopDone32){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone32 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rate").equals(reader.getName())){
                                                                    list32.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone32 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setRate((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list32));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startTime").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list33.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone33 = false;
                                                        while(!loopDone33){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone33 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startTime").equals(reader.getName())){
                                                                    list33.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone33 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setStartTime((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list33));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list34.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone34 = false;
                                                        while(!loopDone34){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone34 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                                                    list34.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone34 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setSubsidiary((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list34));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiaryNoHierarchy").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list35.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone35 = false;
                                                        while(!loopDone35){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone35 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiaryNoHierarchy").equals(reader.getName())){
                                                                    list35.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone35 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setSubsidiaryNoHierarchy((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list35));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","timeSheet").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list36.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone36 = false;
                                                        while(!loopDone36){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone36 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","timeSheet").equals(reader.getName())){
                                                                    list36.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone36 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setTimeSheet((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list36));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","type").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list37.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone37 = false;
                                                        while(!loopDone37){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone37 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","type").equals(reader.getName())){
                                                                    list37.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone37 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setType((com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.class,
                                                                list37));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    