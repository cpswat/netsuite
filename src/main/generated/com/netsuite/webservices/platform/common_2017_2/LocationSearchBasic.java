
/**
 * LocationSearchBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  LocationSearchBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class LocationSearchBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRecordBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = LocationSearchBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for Address
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddressTracker = false ;

                           public boolean isAddressSpecified(){
                               return localAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAddress(){
                               return localAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Address
                               */
                               public void setAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAddressTracker = param != null;
                                   
                                            this.localAddress=param;
                                    

                               }
                            

                        /**
                        * field for AllowStorePickup
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localAllowStorePickup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAllowStorePickupTracker = false ;

                           public boolean isAllowStorePickupSpecified(){
                               return localAllowStorePickupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getAllowStorePickup(){
                               return localAllowStorePickup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AllowStorePickup
                               */
                               public void setAllowStorePickup(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localAllowStorePickupTracker = param != null;
                                   
                                            this.localAllowStorePickup=param;
                                    

                               }
                            

                        /**
                        * field for AutoAssignmentRegionSetting
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localAutoAssignmentRegionSetting ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAutoAssignmentRegionSettingTracker = false ;

                           public boolean isAutoAssignmentRegionSettingSpecified(){
                               return localAutoAssignmentRegionSettingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getAutoAssignmentRegionSetting(){
                               return localAutoAssignmentRegionSetting;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AutoAssignmentRegionSetting
                               */
                               public void setAutoAssignmentRegionSetting(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localAutoAssignmentRegionSettingTracker = param != null;
                                   
                                            this.localAutoAssignmentRegionSetting=param;
                                    

                               }
                            

                        /**
                        * field for BufferStock
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localBufferStock ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBufferStockTracker = false ;

                           public boolean isBufferStockSpecified(){
                               return localBufferStockTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getBufferStock(){
                               return localBufferStock;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BufferStock
                               */
                               public void setBufferStock(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localBufferStockTracker = param != null;
                                   
                                            this.localBufferStock=param;
                                    

                               }
                            

                        /**
                        * field for City
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localCity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCityTracker = false ;

                           public boolean isCitySpecified(){
                               return localCityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getCity(){
                               return localCity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param City
                               */
                               public void setCity(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCityTracker = param != null;
                                   
                                            this.localCity=param;
                                    

                               }
                            

                        /**
                        * field for Country
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localCountry ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCountryTracker = false ;

                           public boolean isCountrySpecified(){
                               return localCountryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getCountry(){
                               return localCountry;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Country
                               */
                               public void setCountry(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localCountryTracker = param != null;
                                   
                                            this.localCountry=param;
                                    

                               }
                            

                        /**
                        * field for County
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localCounty ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCountyTracker = false ;

                           public boolean isCountySpecified(){
                               return localCountyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getCounty(){
                               return localCounty;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param County
                               */
                               public void setCounty(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCountyTracker = param != null;
                                   
                                            this.localCounty=param;
                                    

                               }
                            

                        /**
                        * field for DailyShippingCapacity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localDailyShippingCapacity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDailyShippingCapacityTracker = false ;

                           public boolean isDailyShippingCapacitySpecified(){
                               return localDailyShippingCapacityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getDailyShippingCapacity(){
                               return localDailyShippingCapacity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DailyShippingCapacity
                               */
                               public void setDailyShippingCapacity(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localDailyShippingCapacityTracker = param != null;
                                   
                                            this.localDailyShippingCapacity=param;
                                    

                               }
                            

                        /**
                        * field for EndTime
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localEndTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndTimeTracker = false ;

                           public boolean isEndTimeSpecified(){
                               return localEndTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getEndTime(){
                               return localEndTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndTime
                               */
                               public void setEndTime(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localEndTimeTracker = param != null;
                                   
                                            this.localEndTime=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localExternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdTracker = false ;

                           public boolean isExternalIdSpecified(){
                               return localExternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localExternalIdTracker = param != null;
                                   
                                            this.localExternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalIdString
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localExternalIdString ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdStringTracker = false ;

                           public boolean isExternalIdStringSpecified(){
                               return localExternalIdStringTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getExternalIdString(){
                               return localExternalIdString;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalIdString
                               */
                               public void setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localExternalIdStringTracker = param != null;
                                   
                                            this.localExternalIdString=param;
                                    

                               }
                            

                        /**
                        * field for GeolocationMethod
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localGeolocationMethod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGeolocationMethodTracker = false ;

                           public boolean isGeolocationMethodSpecified(){
                               return localGeolocationMethodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getGeolocationMethod(){
                               return localGeolocationMethod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GeolocationMethod
                               */
                               public void setGeolocationMethod(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localGeolocationMethodTracker = param != null;
                                   
                                            this.localGeolocationMethod=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localInternalIdTracker = param != null;
                                   
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for InternalIdNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localInternalIdNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdNumberTracker = false ;

                           public boolean isInternalIdNumberSpecified(){
                               return localInternalIdNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getInternalIdNumber(){
                               return localInternalIdNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalIdNumber
                               */
                               public void setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localInternalIdNumberTracker = param != null;
                                   
                                            this.localInternalIdNumber=param;
                                    

                               }
                            

                        /**
                        * field for IsFriday
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsFriday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsFridayTracker = false ;

                           public boolean isIsFridaySpecified(){
                               return localIsFridayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsFriday(){
                               return localIsFriday;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsFriday
                               */
                               public void setIsFriday(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsFridayTracker = param != null;
                                   
                                            this.localIsFriday=param;
                                    

                               }
                            

                        /**
                        * field for IsInactive
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsInactive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsInactiveTracker = false ;

                           public boolean isIsInactiveSpecified(){
                               return localIsInactiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsInactive(){
                               return localIsInactive;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsInactive
                               */
                               public void setIsInactive(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsInactiveTracker = param != null;
                                   
                                            this.localIsInactive=param;
                                    

                               }
                            

                        /**
                        * field for IsMonday
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsMonday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsMondayTracker = false ;

                           public boolean isIsMondaySpecified(){
                               return localIsMondayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsMonday(){
                               return localIsMonday;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsMonday
                               */
                               public void setIsMonday(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsMondayTracker = param != null;
                                   
                                            this.localIsMonday=param;
                                    

                               }
                            

                        /**
                        * field for IsOffice
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsOffice ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsOfficeTracker = false ;

                           public boolean isIsOfficeSpecified(){
                               return localIsOfficeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsOffice(){
                               return localIsOffice;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsOffice
                               */
                               public void setIsOffice(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsOfficeTracker = param != null;
                                   
                                            this.localIsOffice=param;
                                    

                               }
                            

                        /**
                        * field for IsSaturday
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsSaturday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsSaturdayTracker = false ;

                           public boolean isIsSaturdaySpecified(){
                               return localIsSaturdayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsSaturday(){
                               return localIsSaturday;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsSaturday
                               */
                               public void setIsSaturday(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsSaturdayTracker = param != null;
                                   
                                            this.localIsSaturday=param;
                                    

                               }
                            

                        /**
                        * field for IsSunday
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsSunday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsSundayTracker = false ;

                           public boolean isIsSundaySpecified(){
                               return localIsSundayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsSunday(){
                               return localIsSunday;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsSunday
                               */
                               public void setIsSunday(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsSundayTracker = param != null;
                                   
                                            this.localIsSunday=param;
                                    

                               }
                            

                        /**
                        * field for IsThursday
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsThursday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsThursdayTracker = false ;

                           public boolean isIsThursdaySpecified(){
                               return localIsThursdayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsThursday(){
                               return localIsThursday;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsThursday
                               */
                               public void setIsThursday(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsThursdayTracker = param != null;
                                   
                                            this.localIsThursday=param;
                                    

                               }
                            

                        /**
                        * field for IsTuesday
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsTuesday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsTuesdayTracker = false ;

                           public boolean isIsTuesdaySpecified(){
                               return localIsTuesdayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsTuesday(){
                               return localIsTuesday;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsTuesday
                               */
                               public void setIsTuesday(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsTuesdayTracker = param != null;
                                   
                                            this.localIsTuesday=param;
                                    

                               }
                            

                        /**
                        * field for IsWednesday
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsWednesday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsWednesdayTracker = false ;

                           public boolean isIsWednesdaySpecified(){
                               return localIsWednesdayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsWednesday(){
                               return localIsWednesday;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsWednesday
                               */
                               public void setIsWednesday(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsWednesdayTracker = param != null;
                                   
                                            this.localIsWednesday=param;
                                    

                               }
                            

                        /**
                        * field for Latitude
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localLatitude ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLatitudeTracker = false ;

                           public boolean isLatitudeSpecified(){
                               return localLatitudeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getLatitude(){
                               return localLatitude;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Latitude
                               */
                               public void setLatitude(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localLatitudeTracker = param != null;
                                   
                                            this.localLatitude=param;
                                    

                               }
                            

                        /**
                        * field for LocationType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localLocationType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTypeTracker = false ;

                           public boolean isLocationTypeSpecified(){
                               return localLocationTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getLocationType(){
                               return localLocationType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LocationType
                               */
                               public void setLocationType(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localLocationTypeTracker = param != null;
                                   
                                            this.localLocationType=param;
                                    

                               }
                            

                        /**
                        * field for Longitude
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localLongitude ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLongitudeTracker = false ;

                           public boolean isLongitudeSpecified(){
                               return localLongitudeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getLongitude(){
                               return localLongitude;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Longitude
                               */
                               public void setLongitude(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localLongitudeTracker = param != null;
                                   
                                            this.localLongitude=param;
                                    

                               }
                            

                        /**
                        * field for MakeInventoryAvailable
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localMakeInventoryAvailable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMakeInventoryAvailableTracker = false ;

                           public boolean isMakeInventoryAvailableSpecified(){
                               return localMakeInventoryAvailableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getMakeInventoryAvailable(){
                               return localMakeInventoryAvailable;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MakeInventoryAvailable
                               */
                               public void setMakeInventoryAvailable(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localMakeInventoryAvailableTracker = param != null;
                                   
                                            this.localMakeInventoryAvailable=param;
                                    

                               }
                            

                        /**
                        * field for MakeInventoryAvailableStore
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localMakeInventoryAvailableStore ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMakeInventoryAvailableStoreTracker = false ;

                           public boolean isMakeInventoryAvailableStoreSpecified(){
                               return localMakeInventoryAvailableStoreTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getMakeInventoryAvailableStore(){
                               return localMakeInventoryAvailableStore;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MakeInventoryAvailableStore
                               */
                               public void setMakeInventoryAvailableStore(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localMakeInventoryAvailableStoreTracker = param != null;
                                   
                                            this.localMakeInventoryAvailableStore=param;
                                    

                               }
                            

                        /**
                        * field for Name
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNameTracker = false ;

                           public boolean isNameSpecified(){
                               return localNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getName(){
                               return localName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Name
                               */
                               public void setName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localNameTracker = param != null;
                                   
                                            this.localName=param;
                                    

                               }
                            

                        /**
                        * field for NameNoHierarchy
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localNameNoHierarchy ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNameNoHierarchyTracker = false ;

                           public boolean isNameNoHierarchySpecified(){
                               return localNameNoHierarchyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getNameNoHierarchy(){
                               return localNameNoHierarchy;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NameNoHierarchy
                               */
                               public void setNameNoHierarchy(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localNameNoHierarchyTracker = param != null;
                                   
                                            this.localNameNoHierarchy=param;
                                    

                               }
                            

                        /**
                        * field for NextPickupCutOffTime
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localNextPickupCutOffTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNextPickupCutOffTimeTracker = false ;

                           public boolean isNextPickupCutOffTimeSpecified(){
                               return localNextPickupCutOffTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getNextPickupCutOffTime(){
                               return localNextPickupCutOffTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NextPickupCutOffTime
                               */
                               public void setNextPickupCutOffTime(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localNextPickupCutOffTimeTracker = param != null;
                                   
                                            this.localNextPickupCutOffTime=param;
                                    

                               }
                            

                        /**
                        * field for Phone
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localPhone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPhoneTracker = false ;

                           public boolean isPhoneSpecified(){
                               return localPhoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getPhone(){
                               return localPhone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Phone
                               */
                               public void setPhone(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localPhoneTracker = param != null;
                                   
                                            this.localPhone=param;
                                    

                               }
                            

                        /**
                        * field for SameDayPickupCutOffTime
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localSameDayPickupCutOffTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSameDayPickupCutOffTimeTracker = false ;

                           public boolean isSameDayPickupCutOffTimeSpecified(){
                               return localSameDayPickupCutOffTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getSameDayPickupCutOffTime(){
                               return localSameDayPickupCutOffTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SameDayPickupCutOffTime
                               */
                               public void setSameDayPickupCutOffTime(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localSameDayPickupCutOffTimeTracker = param != null;
                                   
                                            this.localSameDayPickupCutOffTime=param;
                                    

                               }
                            

                        /**
                        * field for StartTime
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localStartTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartTimeTracker = false ;

                           public boolean isStartTimeSpecified(){
                               return localStartTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getStartTime(){
                               return localStartTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartTime
                               */
                               public void setStartTime(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localStartTimeTracker = param != null;
                                   
                                            this.localStartTime=param;
                                    

                               }
                            

                        /**
                        * field for State
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localState ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStateTracker = false ;

                           public boolean isStateSpecified(){
                               return localStateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getState(){
                               return localState;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param State
                               */
                               public void setState(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localStateTracker = param != null;
                                   
                                            this.localState=param;
                                    

                               }
                            

                        /**
                        * field for StorePickupBufferStock
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localStorePickupBufferStock ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStorePickupBufferStockTracker = false ;

                           public boolean isStorePickupBufferStockSpecified(){
                               return localStorePickupBufferStockTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getStorePickupBufferStock(){
                               return localStorePickupBufferStock;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StorePickupBufferStock
                               */
                               public void setStorePickupBufferStock(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localStorePickupBufferStockTracker = param != null;
                                   
                                            this.localStorePickupBufferStock=param;
                                    

                               }
                            

                        /**
                        * field for Subsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Subsidiary
                               */
                               public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSubsidiaryTracker = param != null;
                                   
                                            this.localSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for TimeZone
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localTimeZone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeZoneTracker = false ;

                           public boolean isTimeZoneSpecified(){
                               return localTimeZoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getTimeZone(){
                               return localTimeZone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimeZone
                               */
                               public void setTimeZone(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localTimeZoneTracker = param != null;
                                   
                                            this.localTimeZone=param;
                                    

                               }
                            

                        /**
                        * field for TotalShippingCapacity
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localTotalShippingCapacity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTotalShippingCapacityTracker = false ;

                           public boolean isTotalShippingCapacitySpecified(){
                               return localTotalShippingCapacityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getTotalShippingCapacity(){
                               return localTotalShippingCapacity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TotalShippingCapacity
                               */
                               public void setTotalShippingCapacity(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localTotalShippingCapacityTracker = param != null;
                                   
                                            this.localTotalShippingCapacity=param;
                                    

                               }
                            

                        /**
                        * field for Tranprefix
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localTranprefix ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranprefixTracker = false ;

                           public boolean isTranprefixSpecified(){
                               return localTranprefixTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getTranprefix(){
                               return localTranprefix;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Tranprefix
                               */
                               public void setTranprefix(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localTranprefixTracker = param != null;
                                   
                                            this.localTranprefix=param;
                                    

                               }
                            

                        /**
                        * field for UsesBins
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localUsesBins ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUsesBinsTracker = false ;

                           public boolean isUsesBinsSpecified(){
                               return localUsesBinsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getUsesBins(){
                               return localUsesBins;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UsesBins
                               */
                               public void setUsesBins(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localUsesBinsTracker = param != null;
                                   
                                            this.localUsesBins=param;
                                    

                               }
                            

                        /**
                        * field for Zip
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localZip ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localZipTracker = false ;

                           public boolean isZipSpecified(){
                               return localZipTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getZip(){
                               return localZip;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Zip
                               */
                               public void setZip(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localZipTracker = param != null;
                                   
                                            this.localZip=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":LocationSearchBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "LocationSearchBasic",
                           xmlWriter);
                   }

                if (localAddressTracker){
                                            if (localAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("address cannot be null!!");
                                            }
                                           localAddress.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address"),
                                               xmlWriter);
                                        } if (localAllowStorePickupTracker){
                                            if (localAllowStorePickup==null){
                                                 throw new org.apache.axis2.databinding.ADBException("allowStorePickup cannot be null!!");
                                            }
                                           localAllowStorePickup.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","allowStorePickup"),
                                               xmlWriter);
                                        } if (localAutoAssignmentRegionSettingTracker){
                                            if (localAutoAssignmentRegionSetting==null){
                                                 throw new org.apache.axis2.databinding.ADBException("autoAssignmentRegionSetting cannot be null!!");
                                            }
                                           localAutoAssignmentRegionSetting.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","autoAssignmentRegionSetting"),
                                               xmlWriter);
                                        } if (localBufferStockTracker){
                                            if (localBufferStock==null){
                                                 throw new org.apache.axis2.databinding.ADBException("bufferStock cannot be null!!");
                                            }
                                           localBufferStock.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","bufferStock"),
                                               xmlWriter);
                                        } if (localCityTracker){
                                            if (localCity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("city cannot be null!!");
                                            }
                                           localCity.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","city"),
                                               xmlWriter);
                                        } if (localCountryTracker){
                                            if (localCountry==null){
                                                 throw new org.apache.axis2.databinding.ADBException("country cannot be null!!");
                                            }
                                           localCountry.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","country"),
                                               xmlWriter);
                                        } if (localCountyTracker){
                                            if (localCounty==null){
                                                 throw new org.apache.axis2.databinding.ADBException("county cannot be null!!");
                                            }
                                           localCounty.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","county"),
                                               xmlWriter);
                                        } if (localDailyShippingCapacityTracker){
                                            if (localDailyShippingCapacity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("dailyShippingCapacity cannot be null!!");
                                            }
                                           localDailyShippingCapacity.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dailyShippingCapacity"),
                                               xmlWriter);
                                        } if (localEndTimeTracker){
                                            if (localEndTime==null){
                                                 throw new org.apache.axis2.databinding.ADBException("endTime cannot be null!!");
                                            }
                                           localEndTime.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endTime"),
                                               xmlWriter);
                                        } if (localExternalIdTracker){
                                            if (localExternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                            }
                                           localExternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId"),
                                               xmlWriter);
                                        } if (localExternalIdStringTracker){
                                            if (localExternalIdString==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                            }
                                           localExternalIdString.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString"),
                                               xmlWriter);
                                        } if (localGeolocationMethodTracker){
                                            if (localGeolocationMethod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("geolocationMethod cannot be null!!");
                                            }
                                           localGeolocationMethod.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","geolocationMethod"),
                                               xmlWriter);
                                        } if (localInternalIdTracker){
                                            if (localInternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                            }
                                           localInternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                               xmlWriter);
                                        } if (localInternalIdNumberTracker){
                                            if (localInternalIdNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                            }
                                           localInternalIdNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber"),
                                               xmlWriter);
                                        } if (localIsFridayTracker){
                                            if (localIsFriday==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isFriday cannot be null!!");
                                            }
                                           localIsFriday.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isFriday"),
                                               xmlWriter);
                                        } if (localIsInactiveTracker){
                                            if (localIsInactive==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                            }
                                           localIsInactive.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isInactive"),
                                               xmlWriter);
                                        } if (localIsMondayTracker){
                                            if (localIsMonday==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isMonday cannot be null!!");
                                            }
                                           localIsMonday.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isMonday"),
                                               xmlWriter);
                                        } if (localIsOfficeTracker){
                                            if (localIsOffice==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isOffice cannot be null!!");
                                            }
                                           localIsOffice.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isOffice"),
                                               xmlWriter);
                                        } if (localIsSaturdayTracker){
                                            if (localIsSaturday==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isSaturday cannot be null!!");
                                            }
                                           localIsSaturday.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isSaturday"),
                                               xmlWriter);
                                        } if (localIsSundayTracker){
                                            if (localIsSunday==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isSunday cannot be null!!");
                                            }
                                           localIsSunday.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isSunday"),
                                               xmlWriter);
                                        } if (localIsThursdayTracker){
                                            if (localIsThursday==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isThursday cannot be null!!");
                                            }
                                           localIsThursday.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isThursday"),
                                               xmlWriter);
                                        } if (localIsTuesdayTracker){
                                            if (localIsTuesday==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isTuesday cannot be null!!");
                                            }
                                           localIsTuesday.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isTuesday"),
                                               xmlWriter);
                                        } if (localIsWednesdayTracker){
                                            if (localIsWednesday==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isWednesday cannot be null!!");
                                            }
                                           localIsWednesday.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isWednesday"),
                                               xmlWriter);
                                        } if (localLatitudeTracker){
                                            if (localLatitude==null){
                                                 throw new org.apache.axis2.databinding.ADBException("latitude cannot be null!!");
                                            }
                                           localLatitude.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","latitude"),
                                               xmlWriter);
                                        } if (localLocationTypeTracker){
                                            if (localLocationType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("locationType cannot be null!!");
                                            }
                                           localLocationType.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","locationType"),
                                               xmlWriter);
                                        } if (localLongitudeTracker){
                                            if (localLongitude==null){
                                                 throw new org.apache.axis2.databinding.ADBException("longitude cannot be null!!");
                                            }
                                           localLongitude.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","longitude"),
                                               xmlWriter);
                                        } if (localMakeInventoryAvailableTracker){
                                            if (localMakeInventoryAvailable==null){
                                                 throw new org.apache.axis2.databinding.ADBException("makeInventoryAvailable cannot be null!!");
                                            }
                                           localMakeInventoryAvailable.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","makeInventoryAvailable"),
                                               xmlWriter);
                                        } if (localMakeInventoryAvailableStoreTracker){
                                            if (localMakeInventoryAvailableStore==null){
                                                 throw new org.apache.axis2.databinding.ADBException("makeInventoryAvailableStore cannot be null!!");
                                            }
                                           localMakeInventoryAvailableStore.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","makeInventoryAvailableStore"),
                                               xmlWriter);
                                        } if (localNameTracker){
                                            if (localName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                            }
                                           localName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","name"),
                                               xmlWriter);
                                        } if (localNameNoHierarchyTracker){
                                            if (localNameNoHierarchy==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nameNoHierarchy cannot be null!!");
                                            }
                                           localNameNoHierarchy.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nameNoHierarchy"),
                                               xmlWriter);
                                        } if (localNextPickupCutOffTimeTracker){
                                            if (localNextPickupCutOffTime==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nextPickupCutOffTime cannot be null!!");
                                            }
                                           localNextPickupCutOffTime.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nextPickupCutOffTime"),
                                               xmlWriter);
                                        } if (localPhoneTracker){
                                            if (localPhone==null){
                                                 throw new org.apache.axis2.databinding.ADBException("phone cannot be null!!");
                                            }
                                           localPhone.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phone"),
                                               xmlWriter);
                                        } if (localSameDayPickupCutOffTimeTracker){
                                            if (localSameDayPickupCutOffTime==null){
                                                 throw new org.apache.axis2.databinding.ADBException("sameDayPickupCutOffTime cannot be null!!");
                                            }
                                           localSameDayPickupCutOffTime.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","sameDayPickupCutOffTime"),
                                               xmlWriter);
                                        } if (localStartTimeTracker){
                                            if (localStartTime==null){
                                                 throw new org.apache.axis2.databinding.ADBException("startTime cannot be null!!");
                                            }
                                           localStartTime.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startTime"),
                                               xmlWriter);
                                        } if (localStateTracker){
                                            if (localState==null){
                                                 throw new org.apache.axis2.databinding.ADBException("state cannot be null!!");
                                            }
                                           localState.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","state"),
                                               xmlWriter);
                                        } if (localStorePickupBufferStockTracker){
                                            if (localStorePickupBufferStock==null){
                                                 throw new org.apache.axis2.databinding.ADBException("storePickupBufferStock cannot be null!!");
                                            }
                                           localStorePickupBufferStock.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","storePickupBufferStock"),
                                               xmlWriter);
                                        } if (localSubsidiaryTracker){
                                            if (localSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                            }
                                           localSubsidiary.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary"),
                                               xmlWriter);
                                        } if (localTimeZoneTracker){
                                            if (localTimeZone==null){
                                                 throw new org.apache.axis2.databinding.ADBException("timeZone cannot be null!!");
                                            }
                                           localTimeZone.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","timeZone"),
                                               xmlWriter);
                                        } if (localTotalShippingCapacityTracker){
                                            if (localTotalShippingCapacity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("totalShippingCapacity cannot be null!!");
                                            }
                                           localTotalShippingCapacity.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","totalShippingCapacity"),
                                               xmlWriter);
                                        } if (localTranprefixTracker){
                                            if (localTranprefix==null){
                                                 throw new org.apache.axis2.databinding.ADBException("tranprefix cannot be null!!");
                                            }
                                           localTranprefix.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranprefix"),
                                               xmlWriter);
                                        } if (localUsesBinsTracker){
                                            if (localUsesBins==null){
                                                 throw new org.apache.axis2.databinding.ADBException("usesBins cannot be null!!");
                                            }
                                           localUsesBins.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","usesBins"),
                                               xmlWriter);
                                        } if (localZipTracker){
                                            if (localZip==null){
                                                 throw new org.apache.axis2.databinding.ADBException("zip cannot be null!!");
                                            }
                                           localZip.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","zip"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","LocationSearchBasic"));
                 if (localAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "address"));
                            
                            
                                    if (localAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("address cannot be null!!");
                                    }
                                    elementList.add(localAddress);
                                } if (localAllowStorePickupTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "allowStorePickup"));
                            
                            
                                    if (localAllowStorePickup==null){
                                         throw new org.apache.axis2.databinding.ADBException("allowStorePickup cannot be null!!");
                                    }
                                    elementList.add(localAllowStorePickup);
                                } if (localAutoAssignmentRegionSettingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "autoAssignmentRegionSetting"));
                            
                            
                                    if (localAutoAssignmentRegionSetting==null){
                                         throw new org.apache.axis2.databinding.ADBException("autoAssignmentRegionSetting cannot be null!!");
                                    }
                                    elementList.add(localAutoAssignmentRegionSetting);
                                } if (localBufferStockTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "bufferStock"));
                            
                            
                                    if (localBufferStock==null){
                                         throw new org.apache.axis2.databinding.ADBException("bufferStock cannot be null!!");
                                    }
                                    elementList.add(localBufferStock);
                                } if (localCityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "city"));
                            
                            
                                    if (localCity==null){
                                         throw new org.apache.axis2.databinding.ADBException("city cannot be null!!");
                                    }
                                    elementList.add(localCity);
                                } if (localCountryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "country"));
                            
                            
                                    if (localCountry==null){
                                         throw new org.apache.axis2.databinding.ADBException("country cannot be null!!");
                                    }
                                    elementList.add(localCountry);
                                } if (localCountyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "county"));
                            
                            
                                    if (localCounty==null){
                                         throw new org.apache.axis2.databinding.ADBException("county cannot be null!!");
                                    }
                                    elementList.add(localCounty);
                                } if (localDailyShippingCapacityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "dailyShippingCapacity"));
                            
                            
                                    if (localDailyShippingCapacity==null){
                                         throw new org.apache.axis2.databinding.ADBException("dailyShippingCapacity cannot be null!!");
                                    }
                                    elementList.add(localDailyShippingCapacity);
                                } if (localEndTimeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "endTime"));
                            
                            
                                    if (localEndTime==null){
                                         throw new org.apache.axis2.databinding.ADBException("endTime cannot be null!!");
                                    }
                                    elementList.add(localEndTime);
                                } if (localExternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalId"));
                            
                            
                                    if (localExternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                    }
                                    elementList.add(localExternalId);
                                } if (localExternalIdStringTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalIdString"));
                            
                            
                                    if (localExternalIdString==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                    }
                                    elementList.add(localExternalIdString);
                                } if (localGeolocationMethodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "geolocationMethod"));
                            
                            
                                    if (localGeolocationMethod==null){
                                         throw new org.apache.axis2.databinding.ADBException("geolocationMethod cannot be null!!");
                                    }
                                    elementList.add(localGeolocationMethod);
                                } if (localInternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalId"));
                            
                            
                                    if (localInternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    }
                                    elementList.add(localInternalId);
                                } if (localInternalIdNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalIdNumber"));
                            
                            
                                    if (localInternalIdNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                    }
                                    elementList.add(localInternalIdNumber);
                                } if (localIsFridayTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isFriday"));
                            
                            
                                    if (localIsFriday==null){
                                         throw new org.apache.axis2.databinding.ADBException("isFriday cannot be null!!");
                                    }
                                    elementList.add(localIsFriday);
                                } if (localIsInactiveTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isInactive"));
                            
                            
                                    if (localIsInactive==null){
                                         throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                    }
                                    elementList.add(localIsInactive);
                                } if (localIsMondayTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isMonday"));
                            
                            
                                    if (localIsMonday==null){
                                         throw new org.apache.axis2.databinding.ADBException("isMonday cannot be null!!");
                                    }
                                    elementList.add(localIsMonday);
                                } if (localIsOfficeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isOffice"));
                            
                            
                                    if (localIsOffice==null){
                                         throw new org.apache.axis2.databinding.ADBException("isOffice cannot be null!!");
                                    }
                                    elementList.add(localIsOffice);
                                } if (localIsSaturdayTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isSaturday"));
                            
                            
                                    if (localIsSaturday==null){
                                         throw new org.apache.axis2.databinding.ADBException("isSaturday cannot be null!!");
                                    }
                                    elementList.add(localIsSaturday);
                                } if (localIsSundayTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isSunday"));
                            
                            
                                    if (localIsSunday==null){
                                         throw new org.apache.axis2.databinding.ADBException("isSunday cannot be null!!");
                                    }
                                    elementList.add(localIsSunday);
                                } if (localIsThursdayTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isThursday"));
                            
                            
                                    if (localIsThursday==null){
                                         throw new org.apache.axis2.databinding.ADBException("isThursday cannot be null!!");
                                    }
                                    elementList.add(localIsThursday);
                                } if (localIsTuesdayTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isTuesday"));
                            
                            
                                    if (localIsTuesday==null){
                                         throw new org.apache.axis2.databinding.ADBException("isTuesday cannot be null!!");
                                    }
                                    elementList.add(localIsTuesday);
                                } if (localIsWednesdayTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isWednesday"));
                            
                            
                                    if (localIsWednesday==null){
                                         throw new org.apache.axis2.databinding.ADBException("isWednesday cannot be null!!");
                                    }
                                    elementList.add(localIsWednesday);
                                } if (localLatitudeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "latitude"));
                            
                            
                                    if (localLatitude==null){
                                         throw new org.apache.axis2.databinding.ADBException("latitude cannot be null!!");
                                    }
                                    elementList.add(localLatitude);
                                } if (localLocationTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "locationType"));
                            
                            
                                    if (localLocationType==null){
                                         throw new org.apache.axis2.databinding.ADBException("locationType cannot be null!!");
                                    }
                                    elementList.add(localLocationType);
                                } if (localLongitudeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "longitude"));
                            
                            
                                    if (localLongitude==null){
                                         throw new org.apache.axis2.databinding.ADBException("longitude cannot be null!!");
                                    }
                                    elementList.add(localLongitude);
                                } if (localMakeInventoryAvailableTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "makeInventoryAvailable"));
                            
                            
                                    if (localMakeInventoryAvailable==null){
                                         throw new org.apache.axis2.databinding.ADBException("makeInventoryAvailable cannot be null!!");
                                    }
                                    elementList.add(localMakeInventoryAvailable);
                                } if (localMakeInventoryAvailableStoreTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "makeInventoryAvailableStore"));
                            
                            
                                    if (localMakeInventoryAvailableStore==null){
                                         throw new org.apache.axis2.databinding.ADBException("makeInventoryAvailableStore cannot be null!!");
                                    }
                                    elementList.add(localMakeInventoryAvailableStore);
                                } if (localNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "name"));
                            
                            
                                    if (localName==null){
                                         throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                    }
                                    elementList.add(localName);
                                } if (localNameNoHierarchyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "nameNoHierarchy"));
                            
                            
                                    if (localNameNoHierarchy==null){
                                         throw new org.apache.axis2.databinding.ADBException("nameNoHierarchy cannot be null!!");
                                    }
                                    elementList.add(localNameNoHierarchy);
                                } if (localNextPickupCutOffTimeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "nextPickupCutOffTime"));
                            
                            
                                    if (localNextPickupCutOffTime==null){
                                         throw new org.apache.axis2.databinding.ADBException("nextPickupCutOffTime cannot be null!!");
                                    }
                                    elementList.add(localNextPickupCutOffTime);
                                } if (localPhoneTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "phone"));
                            
                            
                                    if (localPhone==null){
                                         throw new org.apache.axis2.databinding.ADBException("phone cannot be null!!");
                                    }
                                    elementList.add(localPhone);
                                } if (localSameDayPickupCutOffTimeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "sameDayPickupCutOffTime"));
                            
                            
                                    if (localSameDayPickupCutOffTime==null){
                                         throw new org.apache.axis2.databinding.ADBException("sameDayPickupCutOffTime cannot be null!!");
                                    }
                                    elementList.add(localSameDayPickupCutOffTime);
                                } if (localStartTimeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "startTime"));
                            
                            
                                    if (localStartTime==null){
                                         throw new org.apache.axis2.databinding.ADBException("startTime cannot be null!!");
                                    }
                                    elementList.add(localStartTime);
                                } if (localStateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "state"));
                            
                            
                                    if (localState==null){
                                         throw new org.apache.axis2.databinding.ADBException("state cannot be null!!");
                                    }
                                    elementList.add(localState);
                                } if (localStorePickupBufferStockTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "storePickupBufferStock"));
                            
                            
                                    if (localStorePickupBufferStock==null){
                                         throw new org.apache.axis2.databinding.ADBException("storePickupBufferStock cannot be null!!");
                                    }
                                    elementList.add(localStorePickupBufferStock);
                                } if (localSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "subsidiary"));
                            
                            
                                    if (localSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    }
                                    elementList.add(localSubsidiary);
                                } if (localTimeZoneTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "timeZone"));
                            
                            
                                    if (localTimeZone==null){
                                         throw new org.apache.axis2.databinding.ADBException("timeZone cannot be null!!");
                                    }
                                    elementList.add(localTimeZone);
                                } if (localTotalShippingCapacityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "totalShippingCapacity"));
                            
                            
                                    if (localTotalShippingCapacity==null){
                                         throw new org.apache.axis2.databinding.ADBException("totalShippingCapacity cannot be null!!");
                                    }
                                    elementList.add(localTotalShippingCapacity);
                                } if (localTranprefixTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "tranprefix"));
                            
                            
                                    if (localTranprefix==null){
                                         throw new org.apache.axis2.databinding.ADBException("tranprefix cannot be null!!");
                                    }
                                    elementList.add(localTranprefix);
                                } if (localUsesBinsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "usesBins"));
                            
                            
                                    if (localUsesBins==null){
                                         throw new org.apache.axis2.databinding.ADBException("usesBins cannot be null!!");
                                    }
                                    elementList.add(localUsesBins);
                                } if (localZipTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "zip"));
                            
                            
                                    if (localZip==null){
                                         throw new org.apache.axis2.databinding.ADBException("zip cannot be null!!");
                                    }
                                    elementList.add(localZip);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static LocationSearchBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            LocationSearchBasic object =
                new LocationSearchBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"LocationSearchBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (LocationSearchBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address").equals(reader.getName())){
                                
                                                object.setAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","allowStorePickup").equals(reader.getName())){
                                
                                                object.setAllowStorePickup(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","autoAssignmentRegionSetting").equals(reader.getName())){
                                
                                                object.setAutoAssignmentRegionSetting(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","bufferStock").equals(reader.getName())){
                                
                                                object.setBufferStock(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","city").equals(reader.getName())){
                                
                                                object.setCity(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","country").equals(reader.getName())){
                                
                                                object.setCountry(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","county").equals(reader.getName())){
                                
                                                object.setCounty(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dailyShippingCapacity").equals(reader.getName())){
                                
                                                object.setDailyShippingCapacity(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endTime").equals(reader.getName())){
                                
                                                object.setEndTime(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                
                                                object.setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString").equals(reader.getName())){
                                
                                                object.setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","geolocationMethod").equals(reader.getName())){
                                
                                                object.setGeolocationMethod(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                                object.setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber").equals(reader.getName())){
                                
                                                object.setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isFriday").equals(reader.getName())){
                                
                                                object.setIsFriday(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isInactive").equals(reader.getName())){
                                
                                                object.setIsInactive(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isMonday").equals(reader.getName())){
                                
                                                object.setIsMonday(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isOffice").equals(reader.getName())){
                                
                                                object.setIsOffice(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isSaturday").equals(reader.getName())){
                                
                                                object.setIsSaturday(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isSunday").equals(reader.getName())){
                                
                                                object.setIsSunday(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isThursday").equals(reader.getName())){
                                
                                                object.setIsThursday(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isTuesday").equals(reader.getName())){
                                
                                                object.setIsTuesday(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isWednesday").equals(reader.getName())){
                                
                                                object.setIsWednesday(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","latitude").equals(reader.getName())){
                                
                                                object.setLatitude(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","locationType").equals(reader.getName())){
                                
                                                object.setLocationType(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","longitude").equals(reader.getName())){
                                
                                                object.setLongitude(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","makeInventoryAvailable").equals(reader.getName())){
                                
                                                object.setMakeInventoryAvailable(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","makeInventoryAvailableStore").equals(reader.getName())){
                                
                                                object.setMakeInventoryAvailableStore(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","name").equals(reader.getName())){
                                
                                                object.setName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nameNoHierarchy").equals(reader.getName())){
                                
                                                object.setNameNoHierarchy(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nextPickupCutOffTime").equals(reader.getName())){
                                
                                                object.setNextPickupCutOffTime(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phone").equals(reader.getName())){
                                
                                                object.setPhone(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","sameDayPickupCutOffTime").equals(reader.getName())){
                                
                                                object.setSameDayPickupCutOffTime(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startTime").equals(reader.getName())){
                                
                                                object.setStartTime(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","state").equals(reader.getName())){
                                
                                                object.setState(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","storePickupBufferStock").equals(reader.getName())){
                                
                                                object.setStorePickupBufferStock(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                                object.setSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","timeZone").equals(reader.getName())){
                                
                                                object.setTimeZone(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","totalShippingCapacity").equals(reader.getName())){
                                
                                                object.setTotalShippingCapacity(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranprefix").equals(reader.getName())){
                                
                                                object.setTranprefix(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","usesBins").equals(reader.getName())){
                                
                                                object.setUsesBins(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","zip").equals(reader.getName())){
                                
                                                object.setZip(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    