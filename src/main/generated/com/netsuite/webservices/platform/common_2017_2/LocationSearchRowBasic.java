
/**
 * LocationSearchRowBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  LocationSearchRowBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class LocationSearchRowBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRowBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = LocationSearchRowBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for Address1
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localAddress1 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddress1Tracker = false ;

                           public boolean isAddress1Specified(){
                               return localAddress1Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getAddress1(){
                               return localAddress1;
                           }

                           
                        


                               
                              /**
                               * validate the array for Address1
                               */
                              protected void validateAddress1(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Address1
                              */
                              public void setAddress1(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateAddress1(param);

                               localAddress1Tracker = param != null;
                                      
                                      this.localAddress1=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addAddress1(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localAddress1 == null){
                                   localAddress1 = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localAddress1Tracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localAddress1);
                               list.add(param);
                               this.localAddress1 =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for Address2
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localAddress2 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddress2Tracker = false ;

                           public boolean isAddress2Specified(){
                               return localAddress2Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getAddress2(){
                               return localAddress2;
                           }

                           
                        


                               
                              /**
                               * validate the array for Address2
                               */
                              protected void validateAddress2(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Address2
                              */
                              public void setAddress2(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateAddress2(param);

                               localAddress2Tracker = param != null;
                                      
                                      this.localAddress2=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addAddress2(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localAddress2 == null){
                                   localAddress2 = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localAddress2Tracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localAddress2);
                               list.add(param);
                               this.localAddress2 =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for Address3
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localAddress3 ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddress3Tracker = false ;

                           public boolean isAddress3Specified(){
                               return localAddress3Tracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getAddress3(){
                               return localAddress3;
                           }

                           
                        


                               
                              /**
                               * validate the array for Address3
                               */
                              protected void validateAddress3(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Address3
                              */
                              public void setAddress3(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateAddress3(param);

                               localAddress3Tracker = param != null;
                                      
                                      this.localAddress3=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addAddress3(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localAddress3 == null){
                                   localAddress3 = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localAddress3Tracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localAddress3);
                               list.add(param);
                               this.localAddress3 =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for AllowStorePickup
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localAllowStorePickup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAllowStorePickupTracker = false ;

                           public boolean isAllowStorePickupSpecified(){
                               return localAllowStorePickupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getAllowStorePickup(){
                               return localAllowStorePickup;
                           }

                           
                        


                               
                              /**
                               * validate the array for AllowStorePickup
                               */
                              protected void validateAllowStorePickup(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param AllowStorePickup
                              */
                              public void setAllowStorePickup(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateAllowStorePickup(param);

                               localAllowStorePickupTracker = param != null;
                                      
                                      this.localAllowStorePickup=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addAllowStorePickup(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localAllowStorePickup == null){
                                   localAllowStorePickup = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localAllowStorePickupTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localAllowStorePickup);
                               list.add(param);
                               this.localAllowStorePickup =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for AutoAssignmentRegionSetting
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] localAutoAssignmentRegionSetting ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAutoAssignmentRegionSettingTracker = false ;

                           public boolean isAutoAssignmentRegionSettingSpecified(){
                               return localAutoAssignmentRegionSettingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] getAutoAssignmentRegionSetting(){
                               return localAutoAssignmentRegionSetting;
                           }

                           
                        


                               
                              /**
                               * validate the array for AutoAssignmentRegionSetting
                               */
                              protected void validateAutoAssignmentRegionSetting(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param AutoAssignmentRegionSetting
                              */
                              public void setAutoAssignmentRegionSetting(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                              
                                   validateAutoAssignmentRegionSetting(param);

                               localAutoAssignmentRegionSettingTracker = param != null;
                                      
                                      this.localAutoAssignmentRegionSetting=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField
                             */
                             public void addAutoAssignmentRegionSetting(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField param){
                                   if (localAutoAssignmentRegionSetting == null){
                                   localAutoAssignmentRegionSetting = new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localAutoAssignmentRegionSettingTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localAutoAssignmentRegionSetting);
                               list.add(param);
                               this.localAutoAssignmentRegionSetting =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[list.size()]);

                             }
                             

                        /**
                        * field for BufferStock
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] localBufferStock ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBufferStockTracker = false ;

                           public boolean isBufferStockSpecified(){
                               return localBufferStockTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] getBufferStock(){
                               return localBufferStock;
                           }

                           
                        


                               
                              /**
                               * validate the array for BufferStock
                               */
                              protected void validateBufferStock(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param BufferStock
                              */
                              public void setBufferStock(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] param){
                              
                                   validateBufferStock(param);

                               localBufferStockTracker = param != null;
                                      
                                      this.localBufferStock=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField
                             */
                             public void addBufferStock(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField param){
                                   if (localBufferStock == null){
                                   localBufferStock = new com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[]{};
                                   }

                            
                                 //update the setting tracker
                                localBufferStockTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localBufferStock);
                               list.add(param);
                               this.localBufferStock =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[list.size()]);

                             }
                             

                        /**
                        * field for City
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localCity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCityTracker = false ;

                           public boolean isCitySpecified(){
                               return localCityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getCity(){
                               return localCity;
                           }

                           
                        


                               
                              /**
                               * validate the array for City
                               */
                              protected void validateCity(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param City
                              */
                              public void setCity(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateCity(param);

                               localCityTracker = param != null;
                                      
                                      this.localCity=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addCity(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localCity == null){
                                   localCity = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localCityTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCity);
                               list.add(param);
                               this.localCity =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for Country
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] localCountry ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCountryTracker = false ;

                           public boolean isCountrySpecified(){
                               return localCountryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] getCountry(){
                               return localCountry;
                           }

                           
                        


                               
                              /**
                               * validate the array for Country
                               */
                              protected void validateCountry(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Country
                              */
                              public void setCountry(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                              
                                   validateCountry(param);

                               localCountryTracker = param != null;
                                      
                                      this.localCountry=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField
                             */
                             public void addCountry(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField param){
                                   if (localCountry == null){
                                   localCountry = new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localCountryTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCountry);
                               list.add(param);
                               this.localCountry =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[list.size()]);

                             }
                             

                        /**
                        * field for DailyShippingCapacity
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] localDailyShippingCapacity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDailyShippingCapacityTracker = false ;

                           public boolean isDailyShippingCapacitySpecified(){
                               return localDailyShippingCapacityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] getDailyShippingCapacity(){
                               return localDailyShippingCapacity;
                           }

                           
                        


                               
                              /**
                               * validate the array for DailyShippingCapacity
                               */
                              protected void validateDailyShippingCapacity(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param DailyShippingCapacity
                              */
                              public void setDailyShippingCapacity(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] param){
                              
                                   validateDailyShippingCapacity(param);

                               localDailyShippingCapacityTracker = param != null;
                                      
                                      this.localDailyShippingCapacity=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField
                             */
                             public void addDailyShippingCapacity(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField param){
                                   if (localDailyShippingCapacity == null){
                                   localDailyShippingCapacity = new com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[]{};
                                   }

                            
                                 //update the setting tracker
                                localDailyShippingCapacityTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localDailyShippingCapacity);
                               list.add(param);
                               this.localDailyShippingCapacity =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[list.size()]);

                             }
                             

                        /**
                        * field for EndTime
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localEndTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndTimeTracker = false ;

                           public boolean isEndTimeSpecified(){
                               return localEndTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getEndTime(){
                               return localEndTime;
                           }

                           
                        


                               
                              /**
                               * validate the array for EndTime
                               */
                              protected void validateEndTime(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param EndTime
                              */
                              public void setEndTime(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateEndTime(param);

                               localEndTimeTracker = param != null;
                                      
                                      this.localEndTime=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addEndTime(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localEndTime == null){
                                   localEndTime = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localEndTimeTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localEndTime);
                               list.add(param);
                               this.localEndTime =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for ExternalId
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localExternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdTracker = false ;

                           public boolean isExternalIdSpecified(){
                               return localExternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getExternalId(){
                               return localExternalId;
                           }

                           
                        


                               
                              /**
                               * validate the array for ExternalId
                               */
                              protected void validateExternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param ExternalId
                              */
                              public void setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateExternalId(param);

                               localExternalIdTracker = param != null;
                                      
                                      this.localExternalId=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addExternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localExternalId == null){
                                   localExternalId = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localExternalIdTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localExternalId);
                               list.add(param);
                               this.localExternalId =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for GeolocationMethod
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] localGeolocationMethod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGeolocationMethodTracker = false ;

                           public boolean isGeolocationMethodSpecified(){
                               return localGeolocationMethodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] getGeolocationMethod(){
                               return localGeolocationMethod;
                           }

                           
                        


                               
                              /**
                               * validate the array for GeolocationMethod
                               */
                              protected void validateGeolocationMethod(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param GeolocationMethod
                              */
                              public void setGeolocationMethod(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                              
                                   validateGeolocationMethod(param);

                               localGeolocationMethodTracker = param != null;
                                      
                                      this.localGeolocationMethod=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField
                             */
                             public void addGeolocationMethod(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField param){
                                   if (localGeolocationMethod == null){
                                   localGeolocationMethod = new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localGeolocationMethodTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localGeolocationMethod);
                               list.add(param);
                               this.localGeolocationMethod =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[list.size()]);

                             }
                             

                        /**
                        * field for InternalId
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getInternalId(){
                               return localInternalId;
                           }

                           
                        


                               
                              /**
                               * validate the array for InternalId
                               */
                              protected void validateInternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param InternalId
                              */
                              public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateInternalId(param);

                               localInternalIdTracker = param != null;
                                      
                                      this.localInternalId=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addInternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localInternalId == null){
                                   localInternalId = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localInternalIdTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localInternalId);
                               list.add(param);
                               this.localInternalId =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for IsFriday
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localIsFriday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsFridayTracker = false ;

                           public boolean isIsFridaySpecified(){
                               return localIsFridayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getIsFriday(){
                               return localIsFriday;
                           }

                           
                        


                               
                              /**
                               * validate the array for IsFriday
                               */
                              protected void validateIsFriday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param IsFriday
                              */
                              public void setIsFriday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateIsFriday(param);

                               localIsFridayTracker = param != null;
                                      
                                      this.localIsFriday=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addIsFriday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localIsFriday == null){
                                   localIsFriday = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localIsFridayTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localIsFriday);
                               list.add(param);
                               this.localIsFriday =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for IsInactive
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localIsInactive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsInactiveTracker = false ;

                           public boolean isIsInactiveSpecified(){
                               return localIsInactiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getIsInactive(){
                               return localIsInactive;
                           }

                           
                        


                               
                              /**
                               * validate the array for IsInactive
                               */
                              protected void validateIsInactive(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param IsInactive
                              */
                              public void setIsInactive(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateIsInactive(param);

                               localIsInactiveTracker = param != null;
                                      
                                      this.localIsInactive=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addIsInactive(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localIsInactive == null){
                                   localIsInactive = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localIsInactiveTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localIsInactive);
                               list.add(param);
                               this.localIsInactive =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for IsMonday
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localIsMonday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsMondayTracker = false ;

                           public boolean isIsMondaySpecified(){
                               return localIsMondayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getIsMonday(){
                               return localIsMonday;
                           }

                           
                        


                               
                              /**
                               * validate the array for IsMonday
                               */
                              protected void validateIsMonday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param IsMonday
                              */
                              public void setIsMonday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateIsMonday(param);

                               localIsMondayTracker = param != null;
                                      
                                      this.localIsMonday=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addIsMonday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localIsMonday == null){
                                   localIsMonday = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localIsMondayTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localIsMonday);
                               list.add(param);
                               this.localIsMonday =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for IsOffice
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localIsOffice ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsOfficeTracker = false ;

                           public boolean isIsOfficeSpecified(){
                               return localIsOfficeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getIsOffice(){
                               return localIsOffice;
                           }

                           
                        


                               
                              /**
                               * validate the array for IsOffice
                               */
                              protected void validateIsOffice(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param IsOffice
                              */
                              public void setIsOffice(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateIsOffice(param);

                               localIsOfficeTracker = param != null;
                                      
                                      this.localIsOffice=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addIsOffice(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localIsOffice == null){
                                   localIsOffice = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localIsOfficeTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localIsOffice);
                               list.add(param);
                               this.localIsOffice =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for IsSaturday
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localIsSaturday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsSaturdayTracker = false ;

                           public boolean isIsSaturdaySpecified(){
                               return localIsSaturdayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getIsSaturday(){
                               return localIsSaturday;
                           }

                           
                        


                               
                              /**
                               * validate the array for IsSaturday
                               */
                              protected void validateIsSaturday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param IsSaturday
                              */
                              public void setIsSaturday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateIsSaturday(param);

                               localIsSaturdayTracker = param != null;
                                      
                                      this.localIsSaturday=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addIsSaturday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localIsSaturday == null){
                                   localIsSaturday = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localIsSaturdayTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localIsSaturday);
                               list.add(param);
                               this.localIsSaturday =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for IsSunday
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localIsSunday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsSundayTracker = false ;

                           public boolean isIsSundaySpecified(){
                               return localIsSundayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getIsSunday(){
                               return localIsSunday;
                           }

                           
                        


                               
                              /**
                               * validate the array for IsSunday
                               */
                              protected void validateIsSunday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param IsSunday
                              */
                              public void setIsSunday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateIsSunday(param);

                               localIsSundayTracker = param != null;
                                      
                                      this.localIsSunday=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addIsSunday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localIsSunday == null){
                                   localIsSunday = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localIsSundayTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localIsSunday);
                               list.add(param);
                               this.localIsSunday =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for IsThursday
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localIsThursday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsThursdayTracker = false ;

                           public boolean isIsThursdaySpecified(){
                               return localIsThursdayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getIsThursday(){
                               return localIsThursday;
                           }

                           
                        


                               
                              /**
                               * validate the array for IsThursday
                               */
                              protected void validateIsThursday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param IsThursday
                              */
                              public void setIsThursday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateIsThursday(param);

                               localIsThursdayTracker = param != null;
                                      
                                      this.localIsThursday=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addIsThursday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localIsThursday == null){
                                   localIsThursday = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localIsThursdayTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localIsThursday);
                               list.add(param);
                               this.localIsThursday =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for IsTuesday
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localIsTuesday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsTuesdayTracker = false ;

                           public boolean isIsTuesdaySpecified(){
                               return localIsTuesdayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getIsTuesday(){
                               return localIsTuesday;
                           }

                           
                        


                               
                              /**
                               * validate the array for IsTuesday
                               */
                              protected void validateIsTuesday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param IsTuesday
                              */
                              public void setIsTuesday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateIsTuesday(param);

                               localIsTuesdayTracker = param != null;
                                      
                                      this.localIsTuesday=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addIsTuesday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localIsTuesday == null){
                                   localIsTuesday = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localIsTuesdayTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localIsTuesday);
                               list.add(param);
                               this.localIsTuesday =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for IsWednesday
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localIsWednesday ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsWednesdayTracker = false ;

                           public boolean isIsWednesdaySpecified(){
                               return localIsWednesdayTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getIsWednesday(){
                               return localIsWednesday;
                           }

                           
                        


                               
                              /**
                               * validate the array for IsWednesday
                               */
                              protected void validateIsWednesday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param IsWednesday
                              */
                              public void setIsWednesday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateIsWednesday(param);

                               localIsWednesdayTracker = param != null;
                                      
                                      this.localIsWednesday=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addIsWednesday(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localIsWednesday == null){
                                   localIsWednesday = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localIsWednesdayTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localIsWednesday);
                               list.add(param);
                               this.localIsWednesday =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for Latitude
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localLatitude ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLatitudeTracker = false ;

                           public boolean isLatitudeSpecified(){
                               return localLatitudeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getLatitude(){
                               return localLatitude;
                           }

                           
                        


                               
                              /**
                               * validate the array for Latitude
                               */
                              protected void validateLatitude(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Latitude
                              */
                              public void setLatitude(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateLatitude(param);

                               localLatitudeTracker = param != null;
                                      
                                      this.localLatitude=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addLatitude(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localLatitude == null){
                                   localLatitude = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localLatitudeTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localLatitude);
                               list.add(param);
                               this.localLatitude =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for LocationType
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] localLocationType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTypeTracker = false ;

                           public boolean isLocationTypeSpecified(){
                               return localLocationTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] getLocationType(){
                               return localLocationType;
                           }

                           
                        


                               
                              /**
                               * validate the array for LocationType
                               */
                              protected void validateLocationType(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param LocationType
                              */
                              public void setLocationType(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                              
                                   validateLocationType(param);

                               localLocationTypeTracker = param != null;
                                      
                                      this.localLocationType=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField
                             */
                             public void addLocationType(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField param){
                                   if (localLocationType == null){
                                   localLocationType = new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localLocationTypeTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localLocationType);
                               list.add(param);
                               this.localLocationType =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[list.size()]);

                             }
                             

                        /**
                        * field for Longitude
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localLongitude ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLongitudeTracker = false ;

                           public boolean isLongitudeSpecified(){
                               return localLongitudeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getLongitude(){
                               return localLongitude;
                           }

                           
                        


                               
                              /**
                               * validate the array for Longitude
                               */
                              protected void validateLongitude(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Longitude
                              */
                              public void setLongitude(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateLongitude(param);

                               localLongitudeTracker = param != null;
                                      
                                      this.localLongitude=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addLongitude(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localLongitude == null){
                                   localLongitude = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localLongitudeTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localLongitude);
                               list.add(param);
                               this.localLongitude =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for MakeInventoryAvailable
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localMakeInventoryAvailable ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMakeInventoryAvailableTracker = false ;

                           public boolean isMakeInventoryAvailableSpecified(){
                               return localMakeInventoryAvailableTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getMakeInventoryAvailable(){
                               return localMakeInventoryAvailable;
                           }

                           
                        


                               
                              /**
                               * validate the array for MakeInventoryAvailable
                               */
                              protected void validateMakeInventoryAvailable(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param MakeInventoryAvailable
                              */
                              public void setMakeInventoryAvailable(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateMakeInventoryAvailable(param);

                               localMakeInventoryAvailableTracker = param != null;
                                      
                                      this.localMakeInventoryAvailable=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addMakeInventoryAvailable(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localMakeInventoryAvailable == null){
                                   localMakeInventoryAvailable = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localMakeInventoryAvailableTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localMakeInventoryAvailable);
                               list.add(param);
                               this.localMakeInventoryAvailable =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for MakeInventoryAvailableStore
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localMakeInventoryAvailableStore ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMakeInventoryAvailableStoreTracker = false ;

                           public boolean isMakeInventoryAvailableStoreSpecified(){
                               return localMakeInventoryAvailableStoreTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getMakeInventoryAvailableStore(){
                               return localMakeInventoryAvailableStore;
                           }

                           
                        


                               
                              /**
                               * validate the array for MakeInventoryAvailableStore
                               */
                              protected void validateMakeInventoryAvailableStore(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param MakeInventoryAvailableStore
                              */
                              public void setMakeInventoryAvailableStore(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateMakeInventoryAvailableStore(param);

                               localMakeInventoryAvailableStoreTracker = param != null;
                                      
                                      this.localMakeInventoryAvailableStore=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addMakeInventoryAvailableStore(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localMakeInventoryAvailableStore == null){
                                   localMakeInventoryAvailableStore = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localMakeInventoryAvailableStoreTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localMakeInventoryAvailableStore);
                               list.add(param);
                               this.localMakeInventoryAvailableStore =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for Name
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNameTracker = false ;

                           public boolean isNameSpecified(){
                               return localNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getName(){
                               return localName;
                           }

                           
                        


                               
                              /**
                               * validate the array for Name
                               */
                              protected void validateName(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Name
                              */
                              public void setName(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateName(param);

                               localNameTracker = param != null;
                                      
                                      this.localName=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addName(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localName == null){
                                   localName = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localNameTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localName);
                               list.add(param);
                               this.localName =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for NameNoHierarchy
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localNameNoHierarchy ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNameNoHierarchyTracker = false ;

                           public boolean isNameNoHierarchySpecified(){
                               return localNameNoHierarchyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getNameNoHierarchy(){
                               return localNameNoHierarchy;
                           }

                           
                        


                               
                              /**
                               * validate the array for NameNoHierarchy
                               */
                              protected void validateNameNoHierarchy(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param NameNoHierarchy
                              */
                              public void setNameNoHierarchy(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateNameNoHierarchy(param);

                               localNameNoHierarchyTracker = param != null;
                                      
                                      this.localNameNoHierarchy=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addNameNoHierarchy(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localNameNoHierarchy == null){
                                   localNameNoHierarchy = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localNameNoHierarchyTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localNameNoHierarchy);
                               list.add(param);
                               this.localNameNoHierarchy =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for NextPickupCutOffTime
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localNextPickupCutOffTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNextPickupCutOffTimeTracker = false ;

                           public boolean isNextPickupCutOffTimeSpecified(){
                               return localNextPickupCutOffTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getNextPickupCutOffTime(){
                               return localNextPickupCutOffTime;
                           }

                           
                        


                               
                              /**
                               * validate the array for NextPickupCutOffTime
                               */
                              protected void validateNextPickupCutOffTime(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param NextPickupCutOffTime
                              */
                              public void setNextPickupCutOffTime(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateNextPickupCutOffTime(param);

                               localNextPickupCutOffTimeTracker = param != null;
                                      
                                      this.localNextPickupCutOffTime=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addNextPickupCutOffTime(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localNextPickupCutOffTime == null){
                                   localNextPickupCutOffTime = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localNextPickupCutOffTimeTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localNextPickupCutOffTime);
                               list.add(param);
                               this.localNextPickupCutOffTime =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for Phone
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localPhone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPhoneTracker = false ;

                           public boolean isPhoneSpecified(){
                               return localPhoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getPhone(){
                               return localPhone;
                           }

                           
                        


                               
                              /**
                               * validate the array for Phone
                               */
                              protected void validatePhone(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Phone
                              */
                              public void setPhone(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validatePhone(param);

                               localPhoneTracker = param != null;
                                      
                                      this.localPhone=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addPhone(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localPhone == null){
                                   localPhone = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localPhoneTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localPhone);
                               list.add(param);
                               this.localPhone =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for SameDayPickupCutOffTime
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localSameDayPickupCutOffTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSameDayPickupCutOffTimeTracker = false ;

                           public boolean isSameDayPickupCutOffTimeSpecified(){
                               return localSameDayPickupCutOffTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getSameDayPickupCutOffTime(){
                               return localSameDayPickupCutOffTime;
                           }

                           
                        


                               
                              /**
                               * validate the array for SameDayPickupCutOffTime
                               */
                              protected void validateSameDayPickupCutOffTime(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param SameDayPickupCutOffTime
                              */
                              public void setSameDayPickupCutOffTime(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateSameDayPickupCutOffTime(param);

                               localSameDayPickupCutOffTimeTracker = param != null;
                                      
                                      this.localSameDayPickupCutOffTime=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addSameDayPickupCutOffTime(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localSameDayPickupCutOffTime == null){
                                   localSameDayPickupCutOffTime = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localSameDayPickupCutOffTimeTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localSameDayPickupCutOffTime);
                               list.add(param);
                               this.localSameDayPickupCutOffTime =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for StartTime
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localStartTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartTimeTracker = false ;

                           public boolean isStartTimeSpecified(){
                               return localStartTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getStartTime(){
                               return localStartTime;
                           }

                           
                        


                               
                              /**
                               * validate the array for StartTime
                               */
                              protected void validateStartTime(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param StartTime
                              */
                              public void setStartTime(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateStartTime(param);

                               localStartTimeTracker = param != null;
                                      
                                      this.localStartTime=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addStartTime(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localStartTime == null){
                                   localStartTime = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localStartTimeTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localStartTime);
                               list.add(param);
                               this.localStartTime =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for State
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localState ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStateTracker = false ;

                           public boolean isStateSpecified(){
                               return localStateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getState(){
                               return localState;
                           }

                           
                        


                               
                              /**
                               * validate the array for State
                               */
                              protected void validateState(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param State
                              */
                              public void setState(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateState(param);

                               localStateTracker = param != null;
                                      
                                      this.localState=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addState(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localState == null){
                                   localState = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localStateTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localState);
                               list.add(param);
                               this.localState =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for StorePickupBufferStock
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localStorePickupBufferStock ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStorePickupBufferStockTracker = false ;

                           public boolean isStorePickupBufferStockSpecified(){
                               return localStorePickupBufferStockTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getStorePickupBufferStock(){
                               return localStorePickupBufferStock;
                           }

                           
                        


                               
                              /**
                               * validate the array for StorePickupBufferStock
                               */
                              protected void validateStorePickupBufferStock(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param StorePickupBufferStock
                              */
                              public void setStorePickupBufferStock(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateStorePickupBufferStock(param);

                               localStorePickupBufferStockTracker = param != null;
                                      
                                      this.localStorePickupBufferStock=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addStorePickupBufferStock(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localStorePickupBufferStock == null){
                                   localStorePickupBufferStock = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localStorePickupBufferStockTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localStorePickupBufferStock);
                               list.add(param);
                               this.localStorePickupBufferStock =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for Subsidiary
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        


                               
                              /**
                               * validate the array for Subsidiary
                               */
                              protected void validateSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Subsidiary
                              */
                              public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateSubsidiary(param);

                               localSubsidiaryTracker = param != null;
                                      
                                      this.localSubsidiary=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localSubsidiary == null){
                                   localSubsidiary = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localSubsidiaryTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localSubsidiary);
                               list.add(param);
                               this.localSubsidiary =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for TimeZone
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] localTimeZone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeZoneTracker = false ;

                           public boolean isTimeZoneSpecified(){
                               return localTimeZoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] getTimeZone(){
                               return localTimeZone;
                           }

                           
                        


                               
                              /**
                               * validate the array for TimeZone
                               */
                              protected void validateTimeZone(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param TimeZone
                              */
                              public void setTimeZone(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                              
                                   validateTimeZone(param);

                               localTimeZoneTracker = param != null;
                                      
                                      this.localTimeZone=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField
                             */
                             public void addTimeZone(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField param){
                                   if (localTimeZone == null){
                                   localTimeZone = new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localTimeZoneTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localTimeZone);
                               list.add(param);
                               this.localTimeZone =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[list.size()]);

                             }
                             

                        /**
                        * field for TotalShippingCapacity
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] localTotalShippingCapacity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTotalShippingCapacityTracker = false ;

                           public boolean isTotalShippingCapacitySpecified(){
                               return localTotalShippingCapacityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] getTotalShippingCapacity(){
                               return localTotalShippingCapacity;
                           }

                           
                        


                               
                              /**
                               * validate the array for TotalShippingCapacity
                               */
                              protected void validateTotalShippingCapacity(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param TotalShippingCapacity
                              */
                              public void setTotalShippingCapacity(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] param){
                              
                                   validateTotalShippingCapacity(param);

                               localTotalShippingCapacityTracker = param != null;
                                      
                                      this.localTotalShippingCapacity=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField
                             */
                             public void addTotalShippingCapacity(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField param){
                                   if (localTotalShippingCapacity == null){
                                   localTotalShippingCapacity = new com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[]{};
                                   }

                            
                                 //update the setting tracker
                                localTotalShippingCapacityTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localTotalShippingCapacity);
                               list.add(param);
                               this.localTotalShippingCapacity =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[list.size()]);

                             }
                             

                        /**
                        * field for TranPrefix
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localTranPrefix ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranPrefixTracker = false ;

                           public boolean isTranPrefixSpecified(){
                               return localTranPrefixTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getTranPrefix(){
                               return localTranPrefix;
                           }

                           
                        


                               
                              /**
                               * validate the array for TranPrefix
                               */
                              protected void validateTranPrefix(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param TranPrefix
                              */
                              public void setTranPrefix(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateTranPrefix(param);

                               localTranPrefixTracker = param != null;
                                      
                                      this.localTranPrefix=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addTranPrefix(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localTranPrefix == null){
                                   localTranPrefix = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localTranPrefixTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localTranPrefix);
                               list.add(param);
                               this.localTranPrefix =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for UsesBins
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localUsesBins ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUsesBinsTracker = false ;

                           public boolean isUsesBinsSpecified(){
                               return localUsesBinsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getUsesBins(){
                               return localUsesBins;
                           }

                           
                        


                               
                              /**
                               * validate the array for UsesBins
                               */
                              protected void validateUsesBins(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param UsesBins
                              */
                              public void setUsesBins(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateUsesBins(param);

                               localUsesBinsTracker = param != null;
                                      
                                      this.localUsesBins=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addUsesBins(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localUsesBins == null){
                                   localUsesBins = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localUsesBinsTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localUsesBins);
                               list.add(param);
                               this.localUsesBins =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for Zip
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localZip ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localZipTracker = false ;

                           public boolean isZipSpecified(){
                               return localZipTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getZip(){
                               return localZip;
                           }

                           
                        


                               
                              /**
                               * validate the array for Zip
                               */
                              protected void validateZip(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Zip
                              */
                              public void setZip(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateZip(param);

                               localZipTracker = param != null;
                                      
                                      this.localZip=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addZip(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localZip == null){
                                   localZip = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localZipTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localZip);
                               list.add(param);
                               this.localZip =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":LocationSearchRowBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "LocationSearchRowBasic",
                           xmlWriter);
                   }

                if (localAddress1Tracker){
                                       if (localAddress1!=null){
                                            for (int i = 0;i < localAddress1.length;i++){
                                                if (localAddress1[i] != null){
                                                 localAddress1[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address1"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("address1 cannot be null!!");
                                        
                                    }
                                 } if (localAddress2Tracker){
                                       if (localAddress2!=null){
                                            for (int i = 0;i < localAddress2.length;i++){
                                                if (localAddress2[i] != null){
                                                 localAddress2[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address2"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("address2 cannot be null!!");
                                        
                                    }
                                 } if (localAddress3Tracker){
                                       if (localAddress3!=null){
                                            for (int i = 0;i < localAddress3.length;i++){
                                                if (localAddress3[i] != null){
                                                 localAddress3[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address3"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("address3 cannot be null!!");
                                        
                                    }
                                 } if (localAllowStorePickupTracker){
                                       if (localAllowStorePickup!=null){
                                            for (int i = 0;i < localAllowStorePickup.length;i++){
                                                if (localAllowStorePickup[i] != null){
                                                 localAllowStorePickup[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","allowStorePickup"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("allowStorePickup cannot be null!!");
                                        
                                    }
                                 } if (localAutoAssignmentRegionSettingTracker){
                                       if (localAutoAssignmentRegionSetting!=null){
                                            for (int i = 0;i < localAutoAssignmentRegionSetting.length;i++){
                                                if (localAutoAssignmentRegionSetting[i] != null){
                                                 localAutoAssignmentRegionSetting[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","autoAssignmentRegionSetting"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("autoAssignmentRegionSetting cannot be null!!");
                                        
                                    }
                                 } if (localBufferStockTracker){
                                       if (localBufferStock!=null){
                                            for (int i = 0;i < localBufferStock.length;i++){
                                                if (localBufferStock[i] != null){
                                                 localBufferStock[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","bufferStock"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("bufferStock cannot be null!!");
                                        
                                    }
                                 } if (localCityTracker){
                                       if (localCity!=null){
                                            for (int i = 0;i < localCity.length;i++){
                                                if (localCity[i] != null){
                                                 localCity[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","city"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("city cannot be null!!");
                                        
                                    }
                                 } if (localCountryTracker){
                                       if (localCountry!=null){
                                            for (int i = 0;i < localCountry.length;i++){
                                                if (localCountry[i] != null){
                                                 localCountry[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","country"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("country cannot be null!!");
                                        
                                    }
                                 } if (localDailyShippingCapacityTracker){
                                       if (localDailyShippingCapacity!=null){
                                            for (int i = 0;i < localDailyShippingCapacity.length;i++){
                                                if (localDailyShippingCapacity[i] != null){
                                                 localDailyShippingCapacity[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dailyShippingCapacity"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("dailyShippingCapacity cannot be null!!");
                                        
                                    }
                                 } if (localEndTimeTracker){
                                       if (localEndTime!=null){
                                            for (int i = 0;i < localEndTime.length;i++){
                                                if (localEndTime[i] != null){
                                                 localEndTime[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endTime"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("endTime cannot be null!!");
                                        
                                    }
                                 } if (localExternalIdTracker){
                                       if (localExternalId!=null){
                                            for (int i = 0;i < localExternalId.length;i++){
                                                if (localExternalId[i] != null){
                                                 localExternalId[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                        
                                    }
                                 } if (localGeolocationMethodTracker){
                                       if (localGeolocationMethod!=null){
                                            for (int i = 0;i < localGeolocationMethod.length;i++){
                                                if (localGeolocationMethod[i] != null){
                                                 localGeolocationMethod[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","geolocationMethod"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("geolocationMethod cannot be null!!");
                                        
                                    }
                                 } if (localInternalIdTracker){
                                       if (localInternalId!=null){
                                            for (int i = 0;i < localInternalId.length;i++){
                                                if (localInternalId[i] != null){
                                                 localInternalId[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                        
                                    }
                                 } if (localIsFridayTracker){
                                       if (localIsFriday!=null){
                                            for (int i = 0;i < localIsFriday.length;i++){
                                                if (localIsFriday[i] != null){
                                                 localIsFriday[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isFriday"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("isFriday cannot be null!!");
                                        
                                    }
                                 } if (localIsInactiveTracker){
                                       if (localIsInactive!=null){
                                            for (int i = 0;i < localIsInactive.length;i++){
                                                if (localIsInactive[i] != null){
                                                 localIsInactive[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isInactive"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                        
                                    }
                                 } if (localIsMondayTracker){
                                       if (localIsMonday!=null){
                                            for (int i = 0;i < localIsMonday.length;i++){
                                                if (localIsMonday[i] != null){
                                                 localIsMonday[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isMonday"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("isMonday cannot be null!!");
                                        
                                    }
                                 } if (localIsOfficeTracker){
                                       if (localIsOffice!=null){
                                            for (int i = 0;i < localIsOffice.length;i++){
                                                if (localIsOffice[i] != null){
                                                 localIsOffice[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isOffice"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("isOffice cannot be null!!");
                                        
                                    }
                                 } if (localIsSaturdayTracker){
                                       if (localIsSaturday!=null){
                                            for (int i = 0;i < localIsSaturday.length;i++){
                                                if (localIsSaturday[i] != null){
                                                 localIsSaturday[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isSaturday"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("isSaturday cannot be null!!");
                                        
                                    }
                                 } if (localIsSundayTracker){
                                       if (localIsSunday!=null){
                                            for (int i = 0;i < localIsSunday.length;i++){
                                                if (localIsSunday[i] != null){
                                                 localIsSunday[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isSunday"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("isSunday cannot be null!!");
                                        
                                    }
                                 } if (localIsThursdayTracker){
                                       if (localIsThursday!=null){
                                            for (int i = 0;i < localIsThursday.length;i++){
                                                if (localIsThursday[i] != null){
                                                 localIsThursday[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isThursday"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("isThursday cannot be null!!");
                                        
                                    }
                                 } if (localIsTuesdayTracker){
                                       if (localIsTuesday!=null){
                                            for (int i = 0;i < localIsTuesday.length;i++){
                                                if (localIsTuesday[i] != null){
                                                 localIsTuesday[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isTuesday"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("isTuesday cannot be null!!");
                                        
                                    }
                                 } if (localIsWednesdayTracker){
                                       if (localIsWednesday!=null){
                                            for (int i = 0;i < localIsWednesday.length;i++){
                                                if (localIsWednesday[i] != null){
                                                 localIsWednesday[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isWednesday"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("isWednesday cannot be null!!");
                                        
                                    }
                                 } if (localLatitudeTracker){
                                       if (localLatitude!=null){
                                            for (int i = 0;i < localLatitude.length;i++){
                                                if (localLatitude[i] != null){
                                                 localLatitude[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","latitude"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("latitude cannot be null!!");
                                        
                                    }
                                 } if (localLocationTypeTracker){
                                       if (localLocationType!=null){
                                            for (int i = 0;i < localLocationType.length;i++){
                                                if (localLocationType[i] != null){
                                                 localLocationType[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","locationType"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("locationType cannot be null!!");
                                        
                                    }
                                 } if (localLongitudeTracker){
                                       if (localLongitude!=null){
                                            for (int i = 0;i < localLongitude.length;i++){
                                                if (localLongitude[i] != null){
                                                 localLongitude[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","longitude"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("longitude cannot be null!!");
                                        
                                    }
                                 } if (localMakeInventoryAvailableTracker){
                                       if (localMakeInventoryAvailable!=null){
                                            for (int i = 0;i < localMakeInventoryAvailable.length;i++){
                                                if (localMakeInventoryAvailable[i] != null){
                                                 localMakeInventoryAvailable[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","makeInventoryAvailable"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("makeInventoryAvailable cannot be null!!");
                                        
                                    }
                                 } if (localMakeInventoryAvailableStoreTracker){
                                       if (localMakeInventoryAvailableStore!=null){
                                            for (int i = 0;i < localMakeInventoryAvailableStore.length;i++){
                                                if (localMakeInventoryAvailableStore[i] != null){
                                                 localMakeInventoryAvailableStore[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","makeInventoryAvailableStore"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("makeInventoryAvailableStore cannot be null!!");
                                        
                                    }
                                 } if (localNameTracker){
                                       if (localName!=null){
                                            for (int i = 0;i < localName.length;i++){
                                                if (localName[i] != null){
                                                 localName[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","name"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                        
                                    }
                                 } if (localNameNoHierarchyTracker){
                                       if (localNameNoHierarchy!=null){
                                            for (int i = 0;i < localNameNoHierarchy.length;i++){
                                                if (localNameNoHierarchy[i] != null){
                                                 localNameNoHierarchy[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nameNoHierarchy"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("nameNoHierarchy cannot be null!!");
                                        
                                    }
                                 } if (localNextPickupCutOffTimeTracker){
                                       if (localNextPickupCutOffTime!=null){
                                            for (int i = 0;i < localNextPickupCutOffTime.length;i++){
                                                if (localNextPickupCutOffTime[i] != null){
                                                 localNextPickupCutOffTime[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nextPickupCutOffTime"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("nextPickupCutOffTime cannot be null!!");
                                        
                                    }
                                 } if (localPhoneTracker){
                                       if (localPhone!=null){
                                            for (int i = 0;i < localPhone.length;i++){
                                                if (localPhone[i] != null){
                                                 localPhone[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phone"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("phone cannot be null!!");
                                        
                                    }
                                 } if (localSameDayPickupCutOffTimeTracker){
                                       if (localSameDayPickupCutOffTime!=null){
                                            for (int i = 0;i < localSameDayPickupCutOffTime.length;i++){
                                                if (localSameDayPickupCutOffTime[i] != null){
                                                 localSameDayPickupCutOffTime[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","sameDayPickupCutOffTime"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("sameDayPickupCutOffTime cannot be null!!");
                                        
                                    }
                                 } if (localStartTimeTracker){
                                       if (localStartTime!=null){
                                            for (int i = 0;i < localStartTime.length;i++){
                                                if (localStartTime[i] != null){
                                                 localStartTime[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startTime"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("startTime cannot be null!!");
                                        
                                    }
                                 } if (localStateTracker){
                                       if (localState!=null){
                                            for (int i = 0;i < localState.length;i++){
                                                if (localState[i] != null){
                                                 localState[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","state"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("state cannot be null!!");
                                        
                                    }
                                 } if (localStorePickupBufferStockTracker){
                                       if (localStorePickupBufferStock!=null){
                                            for (int i = 0;i < localStorePickupBufferStock.length;i++){
                                                if (localStorePickupBufferStock[i] != null){
                                                 localStorePickupBufferStock[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","storePickupBufferStock"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("storePickupBufferStock cannot be null!!");
                                        
                                    }
                                 } if (localSubsidiaryTracker){
                                       if (localSubsidiary!=null){
                                            for (int i = 0;i < localSubsidiary.length;i++){
                                                if (localSubsidiary[i] != null){
                                                 localSubsidiary[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                        
                                    }
                                 } if (localTimeZoneTracker){
                                       if (localTimeZone!=null){
                                            for (int i = 0;i < localTimeZone.length;i++){
                                                if (localTimeZone[i] != null){
                                                 localTimeZone[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","timeZone"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("timeZone cannot be null!!");
                                        
                                    }
                                 } if (localTotalShippingCapacityTracker){
                                       if (localTotalShippingCapacity!=null){
                                            for (int i = 0;i < localTotalShippingCapacity.length;i++){
                                                if (localTotalShippingCapacity[i] != null){
                                                 localTotalShippingCapacity[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","totalShippingCapacity"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("totalShippingCapacity cannot be null!!");
                                        
                                    }
                                 } if (localTranPrefixTracker){
                                       if (localTranPrefix!=null){
                                            for (int i = 0;i < localTranPrefix.length;i++){
                                                if (localTranPrefix[i] != null){
                                                 localTranPrefix[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranPrefix"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("tranPrefix cannot be null!!");
                                        
                                    }
                                 } if (localUsesBinsTracker){
                                       if (localUsesBins!=null){
                                            for (int i = 0;i < localUsesBins.length;i++){
                                                if (localUsesBins[i] != null){
                                                 localUsesBins[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","usesBins"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("usesBins cannot be null!!");
                                        
                                    }
                                 } if (localZipTracker){
                                       if (localZip!=null){
                                            for (int i = 0;i < localZip.length;i++){
                                                if (localZip[i] != null){
                                                 localZip[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","zip"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("zip cannot be null!!");
                                        
                                    }
                                 } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","LocationSearchRowBasic"));
                 if (localAddress1Tracker){
                             if (localAddress1!=null) {
                                 for (int i = 0;i < localAddress1.length;i++){

                                    if (localAddress1[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "address1"));
                                         elementList.add(localAddress1[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("address1 cannot be null!!");
                                    
                             }

                        } if (localAddress2Tracker){
                             if (localAddress2!=null) {
                                 for (int i = 0;i < localAddress2.length;i++){

                                    if (localAddress2[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "address2"));
                                         elementList.add(localAddress2[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("address2 cannot be null!!");
                                    
                             }

                        } if (localAddress3Tracker){
                             if (localAddress3!=null) {
                                 for (int i = 0;i < localAddress3.length;i++){

                                    if (localAddress3[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "address3"));
                                         elementList.add(localAddress3[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("address3 cannot be null!!");
                                    
                             }

                        } if (localAllowStorePickupTracker){
                             if (localAllowStorePickup!=null) {
                                 for (int i = 0;i < localAllowStorePickup.length;i++){

                                    if (localAllowStorePickup[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "allowStorePickup"));
                                         elementList.add(localAllowStorePickup[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("allowStorePickup cannot be null!!");
                                    
                             }

                        } if (localAutoAssignmentRegionSettingTracker){
                             if (localAutoAssignmentRegionSetting!=null) {
                                 for (int i = 0;i < localAutoAssignmentRegionSetting.length;i++){

                                    if (localAutoAssignmentRegionSetting[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "autoAssignmentRegionSetting"));
                                         elementList.add(localAutoAssignmentRegionSetting[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("autoAssignmentRegionSetting cannot be null!!");
                                    
                             }

                        } if (localBufferStockTracker){
                             if (localBufferStock!=null) {
                                 for (int i = 0;i < localBufferStock.length;i++){

                                    if (localBufferStock[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "bufferStock"));
                                         elementList.add(localBufferStock[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("bufferStock cannot be null!!");
                                    
                             }

                        } if (localCityTracker){
                             if (localCity!=null) {
                                 for (int i = 0;i < localCity.length;i++){

                                    if (localCity[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "city"));
                                         elementList.add(localCity[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("city cannot be null!!");
                                    
                             }

                        } if (localCountryTracker){
                             if (localCountry!=null) {
                                 for (int i = 0;i < localCountry.length;i++){

                                    if (localCountry[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "country"));
                                         elementList.add(localCountry[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("country cannot be null!!");
                                    
                             }

                        } if (localDailyShippingCapacityTracker){
                             if (localDailyShippingCapacity!=null) {
                                 for (int i = 0;i < localDailyShippingCapacity.length;i++){

                                    if (localDailyShippingCapacity[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "dailyShippingCapacity"));
                                         elementList.add(localDailyShippingCapacity[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("dailyShippingCapacity cannot be null!!");
                                    
                             }

                        } if (localEndTimeTracker){
                             if (localEndTime!=null) {
                                 for (int i = 0;i < localEndTime.length;i++){

                                    if (localEndTime[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "endTime"));
                                         elementList.add(localEndTime[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("endTime cannot be null!!");
                                    
                             }

                        } if (localExternalIdTracker){
                             if (localExternalId!=null) {
                                 for (int i = 0;i < localExternalId.length;i++){

                                    if (localExternalId[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "externalId"));
                                         elementList.add(localExternalId[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                    
                             }

                        } if (localGeolocationMethodTracker){
                             if (localGeolocationMethod!=null) {
                                 for (int i = 0;i < localGeolocationMethod.length;i++){

                                    if (localGeolocationMethod[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "geolocationMethod"));
                                         elementList.add(localGeolocationMethod[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("geolocationMethod cannot be null!!");
                                    
                             }

                        } if (localInternalIdTracker){
                             if (localInternalId!=null) {
                                 for (int i = 0;i < localInternalId.length;i++){

                                    if (localInternalId[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "internalId"));
                                         elementList.add(localInternalId[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    
                             }

                        } if (localIsFridayTracker){
                             if (localIsFriday!=null) {
                                 for (int i = 0;i < localIsFriday.length;i++){

                                    if (localIsFriday[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "isFriday"));
                                         elementList.add(localIsFriday[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("isFriday cannot be null!!");
                                    
                             }

                        } if (localIsInactiveTracker){
                             if (localIsInactive!=null) {
                                 for (int i = 0;i < localIsInactive.length;i++){

                                    if (localIsInactive[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "isInactive"));
                                         elementList.add(localIsInactive[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                    
                             }

                        } if (localIsMondayTracker){
                             if (localIsMonday!=null) {
                                 for (int i = 0;i < localIsMonday.length;i++){

                                    if (localIsMonday[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "isMonday"));
                                         elementList.add(localIsMonday[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("isMonday cannot be null!!");
                                    
                             }

                        } if (localIsOfficeTracker){
                             if (localIsOffice!=null) {
                                 for (int i = 0;i < localIsOffice.length;i++){

                                    if (localIsOffice[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "isOffice"));
                                         elementList.add(localIsOffice[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("isOffice cannot be null!!");
                                    
                             }

                        } if (localIsSaturdayTracker){
                             if (localIsSaturday!=null) {
                                 for (int i = 0;i < localIsSaturday.length;i++){

                                    if (localIsSaturday[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "isSaturday"));
                                         elementList.add(localIsSaturday[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("isSaturday cannot be null!!");
                                    
                             }

                        } if (localIsSundayTracker){
                             if (localIsSunday!=null) {
                                 for (int i = 0;i < localIsSunday.length;i++){

                                    if (localIsSunday[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "isSunday"));
                                         elementList.add(localIsSunday[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("isSunday cannot be null!!");
                                    
                             }

                        } if (localIsThursdayTracker){
                             if (localIsThursday!=null) {
                                 for (int i = 0;i < localIsThursday.length;i++){

                                    if (localIsThursday[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "isThursday"));
                                         elementList.add(localIsThursday[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("isThursday cannot be null!!");
                                    
                             }

                        } if (localIsTuesdayTracker){
                             if (localIsTuesday!=null) {
                                 for (int i = 0;i < localIsTuesday.length;i++){

                                    if (localIsTuesday[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "isTuesday"));
                                         elementList.add(localIsTuesday[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("isTuesday cannot be null!!");
                                    
                             }

                        } if (localIsWednesdayTracker){
                             if (localIsWednesday!=null) {
                                 for (int i = 0;i < localIsWednesday.length;i++){

                                    if (localIsWednesday[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "isWednesday"));
                                         elementList.add(localIsWednesday[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("isWednesday cannot be null!!");
                                    
                             }

                        } if (localLatitudeTracker){
                             if (localLatitude!=null) {
                                 for (int i = 0;i < localLatitude.length;i++){

                                    if (localLatitude[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "latitude"));
                                         elementList.add(localLatitude[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("latitude cannot be null!!");
                                    
                             }

                        } if (localLocationTypeTracker){
                             if (localLocationType!=null) {
                                 for (int i = 0;i < localLocationType.length;i++){

                                    if (localLocationType[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "locationType"));
                                         elementList.add(localLocationType[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("locationType cannot be null!!");
                                    
                             }

                        } if (localLongitudeTracker){
                             if (localLongitude!=null) {
                                 for (int i = 0;i < localLongitude.length;i++){

                                    if (localLongitude[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "longitude"));
                                         elementList.add(localLongitude[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("longitude cannot be null!!");
                                    
                             }

                        } if (localMakeInventoryAvailableTracker){
                             if (localMakeInventoryAvailable!=null) {
                                 for (int i = 0;i < localMakeInventoryAvailable.length;i++){

                                    if (localMakeInventoryAvailable[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "makeInventoryAvailable"));
                                         elementList.add(localMakeInventoryAvailable[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("makeInventoryAvailable cannot be null!!");
                                    
                             }

                        } if (localMakeInventoryAvailableStoreTracker){
                             if (localMakeInventoryAvailableStore!=null) {
                                 for (int i = 0;i < localMakeInventoryAvailableStore.length;i++){

                                    if (localMakeInventoryAvailableStore[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "makeInventoryAvailableStore"));
                                         elementList.add(localMakeInventoryAvailableStore[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("makeInventoryAvailableStore cannot be null!!");
                                    
                             }

                        } if (localNameTracker){
                             if (localName!=null) {
                                 for (int i = 0;i < localName.length;i++){

                                    if (localName[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "name"));
                                         elementList.add(localName[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                                    
                             }

                        } if (localNameNoHierarchyTracker){
                             if (localNameNoHierarchy!=null) {
                                 for (int i = 0;i < localNameNoHierarchy.length;i++){

                                    if (localNameNoHierarchy[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "nameNoHierarchy"));
                                         elementList.add(localNameNoHierarchy[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("nameNoHierarchy cannot be null!!");
                                    
                             }

                        } if (localNextPickupCutOffTimeTracker){
                             if (localNextPickupCutOffTime!=null) {
                                 for (int i = 0;i < localNextPickupCutOffTime.length;i++){

                                    if (localNextPickupCutOffTime[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "nextPickupCutOffTime"));
                                         elementList.add(localNextPickupCutOffTime[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("nextPickupCutOffTime cannot be null!!");
                                    
                             }

                        } if (localPhoneTracker){
                             if (localPhone!=null) {
                                 for (int i = 0;i < localPhone.length;i++){

                                    if (localPhone[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "phone"));
                                         elementList.add(localPhone[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("phone cannot be null!!");
                                    
                             }

                        } if (localSameDayPickupCutOffTimeTracker){
                             if (localSameDayPickupCutOffTime!=null) {
                                 for (int i = 0;i < localSameDayPickupCutOffTime.length;i++){

                                    if (localSameDayPickupCutOffTime[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "sameDayPickupCutOffTime"));
                                         elementList.add(localSameDayPickupCutOffTime[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("sameDayPickupCutOffTime cannot be null!!");
                                    
                             }

                        } if (localStartTimeTracker){
                             if (localStartTime!=null) {
                                 for (int i = 0;i < localStartTime.length;i++){

                                    if (localStartTime[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "startTime"));
                                         elementList.add(localStartTime[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("startTime cannot be null!!");
                                    
                             }

                        } if (localStateTracker){
                             if (localState!=null) {
                                 for (int i = 0;i < localState.length;i++){

                                    if (localState[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "state"));
                                         elementList.add(localState[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("state cannot be null!!");
                                    
                             }

                        } if (localStorePickupBufferStockTracker){
                             if (localStorePickupBufferStock!=null) {
                                 for (int i = 0;i < localStorePickupBufferStock.length;i++){

                                    if (localStorePickupBufferStock[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "storePickupBufferStock"));
                                         elementList.add(localStorePickupBufferStock[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("storePickupBufferStock cannot be null!!");
                                    
                             }

                        } if (localSubsidiaryTracker){
                             if (localSubsidiary!=null) {
                                 for (int i = 0;i < localSubsidiary.length;i++){

                                    if (localSubsidiary[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "subsidiary"));
                                         elementList.add(localSubsidiary[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    
                             }

                        } if (localTimeZoneTracker){
                             if (localTimeZone!=null) {
                                 for (int i = 0;i < localTimeZone.length;i++){

                                    if (localTimeZone[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "timeZone"));
                                         elementList.add(localTimeZone[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("timeZone cannot be null!!");
                                    
                             }

                        } if (localTotalShippingCapacityTracker){
                             if (localTotalShippingCapacity!=null) {
                                 for (int i = 0;i < localTotalShippingCapacity.length;i++){

                                    if (localTotalShippingCapacity[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "totalShippingCapacity"));
                                         elementList.add(localTotalShippingCapacity[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("totalShippingCapacity cannot be null!!");
                                    
                             }

                        } if (localTranPrefixTracker){
                             if (localTranPrefix!=null) {
                                 for (int i = 0;i < localTranPrefix.length;i++){

                                    if (localTranPrefix[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "tranPrefix"));
                                         elementList.add(localTranPrefix[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("tranPrefix cannot be null!!");
                                    
                             }

                        } if (localUsesBinsTracker){
                             if (localUsesBins!=null) {
                                 for (int i = 0;i < localUsesBins.length;i++){

                                    if (localUsesBins[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "usesBins"));
                                         elementList.add(localUsesBins[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("usesBins cannot be null!!");
                                    
                             }

                        } if (localZipTracker){
                             if (localZip!=null) {
                                 for (int i = 0;i < localZip.length;i++){

                                    if (localZip[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "zip"));
                                         elementList.add(localZip[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("zip cannot be null!!");
                                    
                             }

                        } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static LocationSearchRowBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            LocationSearchRowBasic object =
                new LocationSearchRowBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"LocationSearchRowBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (LocationSearchRowBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                        java.util.ArrayList list1 = new java.util.ArrayList();
                    
                        java.util.ArrayList list2 = new java.util.ArrayList();
                    
                        java.util.ArrayList list3 = new java.util.ArrayList();
                    
                        java.util.ArrayList list4 = new java.util.ArrayList();
                    
                        java.util.ArrayList list5 = new java.util.ArrayList();
                    
                        java.util.ArrayList list6 = new java.util.ArrayList();
                    
                        java.util.ArrayList list7 = new java.util.ArrayList();
                    
                        java.util.ArrayList list8 = new java.util.ArrayList();
                    
                        java.util.ArrayList list9 = new java.util.ArrayList();
                    
                        java.util.ArrayList list10 = new java.util.ArrayList();
                    
                        java.util.ArrayList list11 = new java.util.ArrayList();
                    
                        java.util.ArrayList list12 = new java.util.ArrayList();
                    
                        java.util.ArrayList list13 = new java.util.ArrayList();
                    
                        java.util.ArrayList list14 = new java.util.ArrayList();
                    
                        java.util.ArrayList list15 = new java.util.ArrayList();
                    
                        java.util.ArrayList list16 = new java.util.ArrayList();
                    
                        java.util.ArrayList list17 = new java.util.ArrayList();
                    
                        java.util.ArrayList list18 = new java.util.ArrayList();
                    
                        java.util.ArrayList list19 = new java.util.ArrayList();
                    
                        java.util.ArrayList list20 = new java.util.ArrayList();
                    
                        java.util.ArrayList list21 = new java.util.ArrayList();
                    
                        java.util.ArrayList list22 = new java.util.ArrayList();
                    
                        java.util.ArrayList list23 = new java.util.ArrayList();
                    
                        java.util.ArrayList list24 = new java.util.ArrayList();
                    
                        java.util.ArrayList list25 = new java.util.ArrayList();
                    
                        java.util.ArrayList list26 = new java.util.ArrayList();
                    
                        java.util.ArrayList list27 = new java.util.ArrayList();
                    
                        java.util.ArrayList list28 = new java.util.ArrayList();
                    
                        java.util.ArrayList list29 = new java.util.ArrayList();
                    
                        java.util.ArrayList list30 = new java.util.ArrayList();
                    
                        java.util.ArrayList list31 = new java.util.ArrayList();
                    
                        java.util.ArrayList list32 = new java.util.ArrayList();
                    
                        java.util.ArrayList list33 = new java.util.ArrayList();
                    
                        java.util.ArrayList list34 = new java.util.ArrayList();
                    
                        java.util.ArrayList list35 = new java.util.ArrayList();
                    
                        java.util.ArrayList list36 = new java.util.ArrayList();
                    
                        java.util.ArrayList list37 = new java.util.ArrayList();
                    
                        java.util.ArrayList list38 = new java.util.ArrayList();
                    
                        java.util.ArrayList list39 = new java.util.ArrayList();
                    
                        java.util.ArrayList list40 = new java.util.ArrayList();
                    
                        java.util.ArrayList list41 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address1").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list1.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone1 = false;
                                                        while(!loopDone1){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone1 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address1").equals(reader.getName())){
                                                                    list1.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone1 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setAddress1((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list1));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address2").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list2.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone2 = false;
                                                        while(!loopDone2){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone2 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address2").equals(reader.getName())){
                                                                    list2.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone2 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setAddress2((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list2));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address3").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list3.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone3 = false;
                                                        while(!loopDone3){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone3 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address3").equals(reader.getName())){
                                                                    list3.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone3 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setAddress3((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list3));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","allowStorePickup").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list4.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone4 = false;
                                                        while(!loopDone4){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone4 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","allowStorePickup").equals(reader.getName())){
                                                                    list4.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone4 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setAllowStorePickup((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list4));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","autoAssignmentRegionSetting").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list5.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone5 = false;
                                                        while(!loopDone5){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone5 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","autoAssignmentRegionSetting").equals(reader.getName())){
                                                                    list5.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone5 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setAutoAssignmentRegionSetting((com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.class,
                                                                list5));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","bufferStock").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list6.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone6 = false;
                                                        while(!loopDone6){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone6 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","bufferStock").equals(reader.getName())){
                                                                    list6.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone6 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setBufferStock((com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField.class,
                                                                list6));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","city").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list7.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone7 = false;
                                                        while(!loopDone7){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone7 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","city").equals(reader.getName())){
                                                                    list7.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone7 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCity((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list7));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","country").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list8.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone8 = false;
                                                        while(!loopDone8){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone8 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","country").equals(reader.getName())){
                                                                    list8.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone8 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCountry((com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.class,
                                                                list8));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dailyShippingCapacity").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list9.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone9 = false;
                                                        while(!loopDone9){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone9 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dailyShippingCapacity").equals(reader.getName())){
                                                                    list9.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone9 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setDailyShippingCapacity((com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField.class,
                                                                list9));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endTime").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list10.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone10 = false;
                                                        while(!loopDone10){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone10 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endTime").equals(reader.getName())){
                                                                    list10.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone10 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setEndTime((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list10));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list11.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone11 = false;
                                                        while(!loopDone11){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone11 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                                                    list11.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone11 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setExternalId((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list11));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","geolocationMethod").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list12.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone12 = false;
                                                        while(!loopDone12){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone12 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","geolocationMethod").equals(reader.getName())){
                                                                    list12.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone12 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setGeolocationMethod((com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.class,
                                                                list12));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list13.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone13 = false;
                                                        while(!loopDone13){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone13 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                                                    list13.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone13 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setInternalId((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list13));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isFriday").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list14.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone14 = false;
                                                        while(!loopDone14){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone14 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isFriday").equals(reader.getName())){
                                                                    list14.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone14 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setIsFriday((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list14));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isInactive").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list15.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone15 = false;
                                                        while(!loopDone15){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone15 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isInactive").equals(reader.getName())){
                                                                    list15.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone15 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setIsInactive((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list15));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isMonday").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list16.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone16 = false;
                                                        while(!loopDone16){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone16 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isMonday").equals(reader.getName())){
                                                                    list16.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone16 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setIsMonday((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list16));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isOffice").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list17.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone17 = false;
                                                        while(!loopDone17){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone17 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isOffice").equals(reader.getName())){
                                                                    list17.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone17 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setIsOffice((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list17));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isSaturday").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list18.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone18 = false;
                                                        while(!loopDone18){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone18 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isSaturday").equals(reader.getName())){
                                                                    list18.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone18 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setIsSaturday((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list18));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isSunday").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list19.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone19 = false;
                                                        while(!loopDone19){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone19 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isSunday").equals(reader.getName())){
                                                                    list19.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone19 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setIsSunday((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list19));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isThursday").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list20.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone20 = false;
                                                        while(!loopDone20){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone20 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isThursday").equals(reader.getName())){
                                                                    list20.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone20 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setIsThursday((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list20));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isTuesday").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list21.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone21 = false;
                                                        while(!loopDone21){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone21 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isTuesday").equals(reader.getName())){
                                                                    list21.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone21 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setIsTuesday((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list21));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isWednesday").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list22.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone22 = false;
                                                        while(!loopDone22){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone22 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isWednesday").equals(reader.getName())){
                                                                    list22.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone22 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setIsWednesday((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list22));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","latitude").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list23.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone23 = false;
                                                        while(!loopDone23){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone23 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","latitude").equals(reader.getName())){
                                                                    list23.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone23 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setLatitude((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list23));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","locationType").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list24.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone24 = false;
                                                        while(!loopDone24){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone24 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","locationType").equals(reader.getName())){
                                                                    list24.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone24 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setLocationType((com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.class,
                                                                list24));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","longitude").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list25.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone25 = false;
                                                        while(!loopDone25){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone25 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","longitude").equals(reader.getName())){
                                                                    list25.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone25 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setLongitude((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list25));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","makeInventoryAvailable").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list26.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone26 = false;
                                                        while(!loopDone26){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone26 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","makeInventoryAvailable").equals(reader.getName())){
                                                                    list26.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone26 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setMakeInventoryAvailable((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list26));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","makeInventoryAvailableStore").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list27.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone27 = false;
                                                        while(!loopDone27){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone27 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","makeInventoryAvailableStore").equals(reader.getName())){
                                                                    list27.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone27 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setMakeInventoryAvailableStore((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list27));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","name").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list28.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone28 = false;
                                                        while(!loopDone28){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone28 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","name").equals(reader.getName())){
                                                                    list28.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone28 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setName((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list28));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nameNoHierarchy").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list29.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone29 = false;
                                                        while(!loopDone29){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone29 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nameNoHierarchy").equals(reader.getName())){
                                                                    list29.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone29 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setNameNoHierarchy((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list29));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nextPickupCutOffTime").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list30.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone30 = false;
                                                        while(!loopDone30){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone30 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","nextPickupCutOffTime").equals(reader.getName())){
                                                                    list30.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone30 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setNextPickupCutOffTime((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list30));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phone").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list31.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone31 = false;
                                                        while(!loopDone31){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone31 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phone").equals(reader.getName())){
                                                                    list31.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone31 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setPhone((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list31));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","sameDayPickupCutOffTime").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list32.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone32 = false;
                                                        while(!loopDone32){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone32 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","sameDayPickupCutOffTime").equals(reader.getName())){
                                                                    list32.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone32 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setSameDayPickupCutOffTime((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list32));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startTime").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list33.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone33 = false;
                                                        while(!loopDone33){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone33 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startTime").equals(reader.getName())){
                                                                    list33.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone33 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setStartTime((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list33));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","state").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list34.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone34 = false;
                                                        while(!loopDone34){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone34 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","state").equals(reader.getName())){
                                                                    list34.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone34 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setState((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list34));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","storePickupBufferStock").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list35.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone35 = false;
                                                        while(!loopDone35){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone35 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","storePickupBufferStock").equals(reader.getName())){
                                                                    list35.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone35 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setStorePickupBufferStock((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list35));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list36.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone36 = false;
                                                        while(!loopDone36){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone36 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                                                    list36.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone36 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setSubsidiary((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list36));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","timeZone").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list37.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone37 = false;
                                                        while(!loopDone37){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone37 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","timeZone").equals(reader.getName())){
                                                                    list37.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone37 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setTimeZone((com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.class,
                                                                list37));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","totalShippingCapacity").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list38.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone38 = false;
                                                        while(!loopDone38){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone38 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","totalShippingCapacity").equals(reader.getName())){
                                                                    list38.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone38 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setTotalShippingCapacity((com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField.class,
                                                                list38));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranPrefix").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list39.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone39 = false;
                                                        while(!loopDone39){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone39 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranPrefix").equals(reader.getName())){
                                                                    list39.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone39 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setTranPrefix((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list39));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","usesBins").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list40.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone40 = false;
                                                        while(!loopDone40){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone40 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","usesBins").equals(reader.getName())){
                                                                    list40.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone40 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setUsesBins((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list40));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","zip").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list41.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone41 = false;
                                                        while(!loopDone41){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone41 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","zip").equals(reader.getName())){
                                                                    list41.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone41 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setZip((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list41));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    