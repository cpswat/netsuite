
/**
 * PartnerSearchBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  PartnerSearchBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class PartnerSearchBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRecordBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = PartnerSearchBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for Address
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddressTracker = false ;

                           public boolean isAddressSpecified(){
                               return localAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAddress(){
                               return localAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Address
                               */
                               public void setAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAddressTracker = param != null;
                                   
                                            this.localAddress=param;
                                    

                               }
                            

                        /**
                        * field for Addressee
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAddressee ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddresseeTracker = false ;

                           public boolean isAddresseeSpecified(){
                               return localAddresseeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAddressee(){
                               return localAddressee;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Addressee
                               */
                               public void setAddressee(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAddresseeTracker = param != null;
                                   
                                            this.localAddressee=param;
                                    

                               }
                            

                        /**
                        * field for AddressLabel
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAddressLabel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddressLabelTracker = false ;

                           public boolean isAddressLabelSpecified(){
                               return localAddressLabelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAddressLabel(){
                               return localAddressLabel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AddressLabel
                               */
                               public void setAddressLabel(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAddressLabelTracker = param != null;
                                   
                                            this.localAddressLabel=param;
                                    

                               }
                            

                        /**
                        * field for AddressPhone
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAddressPhone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAddressPhoneTracker = false ;

                           public boolean isAddressPhoneSpecified(){
                               return localAddressPhoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAddressPhone(){
                               return localAddressPhone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AddressPhone
                               */
                               public void setAddressPhone(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAddressPhoneTracker = param != null;
                                   
                                            this.localAddressPhone=param;
                                    

                               }
                            

                        /**
                        * field for AssignTasks
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localAssignTasks ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAssignTasksTracker = false ;

                           public boolean isAssignTasksSpecified(){
                               return localAssignTasksTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getAssignTasks(){
                               return localAssignTasks;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AssignTasks
                               */
                               public void setAssignTasks(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localAssignTasksTracker = param != null;
                                   
                                            this.localAssignTasks=param;
                                    

                               }
                            

                        /**
                        * field for Attention
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localAttention ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAttentionTracker = false ;

                           public boolean isAttentionSpecified(){
                               return localAttentionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getAttention(){
                               return localAttention;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Attention
                               */
                               public void setAttention(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localAttentionTracker = param != null;
                                   
                                            this.localAttention=param;
                                    

                               }
                            

                        /**
                        * field for BillAddress
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localBillAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBillAddressTracker = false ;

                           public boolean isBillAddressSpecified(){
                               return localBillAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getBillAddress(){
                               return localBillAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BillAddress
                               */
                               public void setBillAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localBillAddressTracker = param != null;
                                   
                                            this.localBillAddress=param;
                                    

                               }
                            

                        /**
                        * field for Category
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCategory ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCategoryTracker = false ;

                           public boolean isCategorySpecified(){
                               return localCategoryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCategory(){
                               return localCategory;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Category
                               */
                               public void setCategory(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCategoryTracker = param != null;
                                   
                                            this.localCategory=param;
                                    

                               }
                            

                        /**
                        * field for City
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localCity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCityTracker = false ;

                           public boolean isCitySpecified(){
                               return localCityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getCity(){
                               return localCity;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param City
                               */
                               public void setCity(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCityTracker = param != null;
                                   
                                            this.localCity=param;
                                    

                               }
                            

                        /**
                        * field for _class
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField get_class(){
                               return local_class;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param _class
                               */
                               public void set_class(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            local_classTracker = param != null;
                                   
                                            this.local_class=param;
                                    

                               }
                            

                        /**
                        * field for Comments
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localComments ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCommentsTracker = false ;

                           public boolean isCommentsSpecified(){
                               return localCommentsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getComments(){
                               return localComments;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Comments
                               */
                               public void setComments(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCommentsTracker = param != null;
                                   
                                            this.localComments=param;
                                    

                               }
                            

                        /**
                        * field for CommissionPlan
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCommissionPlan ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCommissionPlanTracker = false ;

                           public boolean isCommissionPlanSpecified(){
                               return localCommissionPlanTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCommissionPlan(){
                               return localCommissionPlan;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CommissionPlan
                               */
                               public void setCommissionPlan(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCommissionPlanTracker = param != null;
                                   
                                            this.localCommissionPlan=param;
                                    

                               }
                            

                        /**
                        * field for Country
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localCountry ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCountryTracker = false ;

                           public boolean isCountrySpecified(){
                               return localCountryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getCountry(){
                               return localCountry;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Country
                               */
                               public void setCountry(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localCountryTracker = param != null;
                                   
                                            this.localCountry=param;
                                    

                               }
                            

                        /**
                        * field for County
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localCounty ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCountyTracker = false ;

                           public boolean isCountySpecified(){
                               return localCountyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getCounty(){
                               return localCounty;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param County
                               */
                               public void setCounty(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localCountyTracker = param != null;
                                   
                                            this.localCounty=param;
                                    

                               }
                            

                        /**
                        * field for DateCreated
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localDateCreated ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDateCreatedTracker = false ;

                           public boolean isDateCreatedSpecified(){
                               return localDateCreatedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getDateCreated(){
                               return localDateCreated;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DateCreated
                               */
                               public void setDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localDateCreatedTracker = param != null;
                                   
                                            this.localDateCreated=param;
                                    

                               }
                            

                        /**
                        * field for Department
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getDepartment(){
                               return localDepartment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Department
                               */
                               public void setDepartment(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localDepartmentTracker = param != null;
                                   
                                            this.localDepartment=param;
                                    

                               }
                            

                        /**
                        * field for EligibleForCommission
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localEligibleForCommission ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEligibleForCommissionTracker = false ;

                           public boolean isEligibleForCommissionSpecified(){
                               return localEligibleForCommissionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getEligibleForCommission(){
                               return localEligibleForCommission;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EligibleForCommission
                               */
                               public void setEligibleForCommission(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localEligibleForCommissionTracker = param != null;
                                   
                                            this.localEligibleForCommission=param;
                                    

                               }
                            

                        /**
                        * field for Email
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailTracker = false ;

                           public boolean isEmailSpecified(){
                               return localEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getEmail(){
                               return localEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Email
                               */
                               public void setEmail(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localEmailTracker = param != null;
                                   
                                            this.localEmail=param;
                                    

                               }
                            

                        /**
                        * field for EmailPreference
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localEmailPreference ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEmailPreferenceTracker = false ;

                           public boolean isEmailPreferenceSpecified(){
                               return localEmailPreferenceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getEmailPreference(){
                               return localEmailPreference;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EmailPreference
                               */
                               public void setEmailPreference(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localEmailPreferenceTracker = param != null;
                                   
                                            this.localEmailPreference=param;
                                    

                               }
                            

                        /**
                        * field for EntityId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localEntityId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityIdTracker = false ;

                           public boolean isEntityIdSpecified(){
                               return localEntityIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getEntityId(){
                               return localEntityId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EntityId
                               */
                               public void setEntityId(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localEntityIdTracker = param != null;
                                   
                                            this.localEntityId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localExternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdTracker = false ;

                           public boolean isExternalIdSpecified(){
                               return localExternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localExternalIdTracker = param != null;
                                   
                                            this.localExternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalIdString
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localExternalIdString ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdStringTracker = false ;

                           public boolean isExternalIdStringSpecified(){
                               return localExternalIdStringTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getExternalIdString(){
                               return localExternalIdString;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalIdString
                               */
                               public void setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localExternalIdStringTracker = param != null;
                                   
                                            this.localExternalIdString=param;
                                    

                               }
                            

                        /**
                        * field for Fax
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localFax ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFaxTracker = false ;

                           public boolean isFaxSpecified(){
                               return localFaxTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getFax(){
                               return localFax;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Fax
                               */
                               public void setFax(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localFaxTracker = param != null;
                                   
                                            this.localFax=param;
                                    

                               }
                            

                        /**
                        * field for FirstName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localFirstName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFirstNameTracker = false ;

                           public boolean isFirstNameSpecified(){
                               return localFirstNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getFirstName(){
                               return localFirstName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FirstName
                               */
                               public void setFirstName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localFirstNameTracker = param != null;
                                   
                                            this.localFirstName=param;
                                    

                               }
                            

                        /**
                        * field for GiveAccess
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localGiveAccess ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGiveAccessTracker = false ;

                           public boolean isGiveAccessSpecified(){
                               return localGiveAccessTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getGiveAccess(){
                               return localGiveAccess;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GiveAccess
                               */
                               public void setGiveAccess(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localGiveAccessTracker = param != null;
                                   
                                            this.localGiveAccess=param;
                                    

                               }
                            

                        /**
                        * field for GlobalSubscriptionStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localGlobalSubscriptionStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGlobalSubscriptionStatusTracker = false ;

                           public boolean isGlobalSubscriptionStatusSpecified(){
                               return localGlobalSubscriptionStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getGlobalSubscriptionStatus(){
                               return localGlobalSubscriptionStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GlobalSubscriptionStatus
                               */
                               public void setGlobalSubscriptionStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localGlobalSubscriptionStatusTracker = param != null;
                                   
                                            this.localGlobalSubscriptionStatus=param;
                                    

                               }
                            

                        /**
                        * field for Group
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localGroup ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGroupTracker = false ;

                           public boolean isGroupSpecified(){
                               return localGroupTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getGroup(){
                               return localGroup;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Group
                               */
                               public void setGroup(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localGroupTracker = param != null;
                                   
                                            this.localGroup=param;
                                    

                               }
                            

                        /**
                        * field for HasDuplicates
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localHasDuplicates ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localHasDuplicatesTracker = false ;

                           public boolean isHasDuplicatesSpecified(){
                               return localHasDuplicatesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getHasDuplicates(){
                               return localHasDuplicates;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param HasDuplicates
                               */
                               public void setHasDuplicates(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localHasDuplicatesTracker = param != null;
                                   
                                            this.localHasDuplicates=param;
                                    

                               }
                            

                        /**
                        * field for Image
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localImage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localImageTracker = false ;

                           public boolean isImageSpecified(){
                               return localImageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getImage(){
                               return localImage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Image
                               */
                               public void setImage(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localImageTracker = param != null;
                                   
                                            this.localImage=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localInternalIdTracker = param != null;
                                   
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for InternalIdNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localInternalIdNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdNumberTracker = false ;

                           public boolean isInternalIdNumberSpecified(){
                               return localInternalIdNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getInternalIdNumber(){
                               return localInternalIdNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalIdNumber
                               */
                               public void setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localInternalIdNumberTracker = param != null;
                                   
                                            this.localInternalIdNumber=param;
                                    

                               }
                            

                        /**
                        * field for IsDefaultBilling
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsDefaultBilling ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsDefaultBillingTracker = false ;

                           public boolean isIsDefaultBillingSpecified(){
                               return localIsDefaultBillingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsDefaultBilling(){
                               return localIsDefaultBilling;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsDefaultBilling
                               */
                               public void setIsDefaultBilling(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsDefaultBillingTracker = param != null;
                                   
                                            this.localIsDefaultBilling=param;
                                    

                               }
                            

                        /**
                        * field for IsDefaultShipping
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsDefaultShipping ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsDefaultShippingTracker = false ;

                           public boolean isIsDefaultShippingSpecified(){
                               return localIsDefaultShippingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsDefaultShipping(){
                               return localIsDefaultShipping;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsDefaultShipping
                               */
                               public void setIsDefaultShipping(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsDefaultShippingTracker = param != null;
                                   
                                            this.localIsDefaultShipping=param;
                                    

                               }
                            

                        /**
                        * field for IsInactive
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsInactive ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsInactiveTracker = false ;

                           public boolean isIsInactiveSpecified(){
                               return localIsInactiveTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsInactive(){
                               return localIsInactive;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsInactive
                               */
                               public void setIsInactive(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsInactiveTracker = param != null;
                                   
                                            this.localIsInactive=param;
                                    

                               }
                            

                        /**
                        * field for IsPerson
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsPerson ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsPersonTracker = false ;

                           public boolean isIsPersonSpecified(){
                               return localIsPersonTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsPerson(){
                               return localIsPerson;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsPerson
                               */
                               public void setIsPerson(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsPersonTracker = param != null;
                                   
                                            this.localIsPerson=param;
                                    

                               }
                            

                        /**
                        * field for Language
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localLanguage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLanguageTracker = false ;

                           public boolean isLanguageSpecified(){
                               return localLanguageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getLanguage(){
                               return localLanguage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Language
                               */
                               public void setLanguage(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localLanguageTracker = param != null;
                                   
                                            this.localLanguage=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for LastName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localLastName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastNameTracker = false ;

                           public boolean isLastNameSpecified(){
                               return localLastNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getLastName(){
                               return localLastName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastName
                               */
                               public void setLastName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localLastNameTracker = param != null;
                                   
                                            this.localLastName=param;
                                    

                               }
                            

                        /**
                        * field for Level
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localLevel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLevelTracker = false ;

                           public boolean isLevelSpecified(){
                               return localLevelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getLevel(){
                               return localLevel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Level
                               */
                               public void setLevel(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localLevelTracker = param != null;
                                   
                                            this.localLevel=param;
                                    

                               }
                            

                        /**
                        * field for Location
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for MiddleName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localMiddleName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMiddleNameTracker = false ;

                           public boolean isMiddleNameSpecified(){
                               return localMiddleNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getMiddleName(){
                               return localMiddleName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MiddleName
                               */
                               public void setMiddleName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localMiddleNameTracker = param != null;
                                   
                                            this.localMiddleName=param;
                                    

                               }
                            

                        /**
                        * field for OtherRelationships
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localOtherRelationships ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOtherRelationshipsTracker = false ;

                           public boolean isOtherRelationshipsSpecified(){
                               return localOtherRelationshipsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getOtherRelationships(){
                               return localOtherRelationships;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param OtherRelationships
                               */
                               public void setOtherRelationships(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localOtherRelationshipsTracker = param != null;
                                   
                                            this.localOtherRelationships=param;
                                    

                               }
                            

                        /**
                        * field for Parent
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localParent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParentTracker = false ;

                           public boolean isParentSpecified(){
                               return localParentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getParent(){
                               return localParent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Parent
                               */
                               public void setParent(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localParentTracker = param != null;
                                   
                                            this.localParent=param;
                                    

                               }
                            

                        /**
                        * field for PartnerCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localPartnerCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerCodeTracker = false ;

                           public boolean isPartnerCodeSpecified(){
                               return localPartnerCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getPartnerCode(){
                               return localPartnerCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PartnerCode
                               */
                               public void setPartnerCode(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localPartnerCodeTracker = param != null;
                                   
                                            this.localPartnerCode=param;
                                    

                               }
                            

                        /**
                        * field for Permission
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localPermission ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPermissionTracker = false ;

                           public boolean isPermissionSpecified(){
                               return localPermissionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getPermission(){
                               return localPermission;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Permission
                               */
                               public void setPermission(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localPermissionTracker = param != null;
                                   
                                            this.localPermission=param;
                                    

                               }
                            

                        /**
                        * field for Phone
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localPhone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPhoneTracker = false ;

                           public boolean isPhoneSpecified(){
                               return localPhoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getPhone(){
                               return localPhone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Phone
                               */
                               public void setPhone(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localPhoneTracker = param != null;
                                   
                                            this.localPhone=param;
                                    

                               }
                            

                        /**
                        * field for PhoneticName
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localPhoneticName ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPhoneticNameTracker = false ;

                           public boolean isPhoneticNameSpecified(){
                               return localPhoneticNameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getPhoneticName(){
                               return localPhoneticName;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PhoneticName
                               */
                               public void setPhoneticName(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localPhoneticNameTracker = param != null;
                                   
                                            this.localPhoneticName=param;
                                    

                               }
                            

                        /**
                        * field for PromoCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localPromoCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPromoCodeTracker = false ;

                           public boolean isPromoCodeSpecified(){
                               return localPromoCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getPromoCode(){
                               return localPromoCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PromoCode
                               */
                               public void setPromoCode(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localPromoCodeTracker = param != null;
                                   
                                            this.localPromoCode=param;
                                    

                               }
                            

                        /**
                        * field for Salutation
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localSalutation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalutationTracker = false ;

                           public boolean isSalutationSpecified(){
                               return localSalutationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getSalutation(){
                               return localSalutation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Salutation
                               */
                               public void setSalutation(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localSalutationTracker = param != null;
                                   
                                            this.localSalutation=param;
                                    

                               }
                            

                        /**
                        * field for ShipAddress
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localShipAddress ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localShipAddressTracker = false ;

                           public boolean isShipAddressSpecified(){
                               return localShipAddressTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getShipAddress(){
                               return localShipAddress;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ShipAddress
                               */
                               public void setShipAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localShipAddressTracker = param != null;
                                   
                                            this.localShipAddress=param;
                                    

                               }
                            

                        /**
                        * field for State
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localState ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStateTracker = false ;

                           public boolean isStateSpecified(){
                               return localStateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getState(){
                               return localState;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param State
                               */
                               public void setState(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localStateTracker = param != null;
                                   
                                            this.localState=param;
                                    

                               }
                            

                        /**
                        * field for Subsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Subsidiary
                               */
                               public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSubsidiaryTracker = param != null;
                                   
                                            this.localSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for Title
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTitleTracker = false ;

                           public boolean isTitleSpecified(){
                               return localTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getTitle(){
                               return localTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Title
                               */
                               public void setTitle(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localTitleTracker = param != null;
                                   
                                            this.localTitle=param;
                                    

                               }
                            

                        /**
                        * field for URL
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localURL ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localURLTracker = false ;

                           public boolean isURLSpecified(){
                               return localURLTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getURL(){
                               return localURL;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param URL
                               */
                               public void setURL(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localURLTracker = param != null;
                                   
                                            this.localURL=param;
                                    

                               }
                            

                        /**
                        * field for ZipCode
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localZipCode ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localZipCodeTracker = false ;

                           public boolean isZipCodeSpecified(){
                               return localZipCodeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getZipCode(){
                               return localZipCode;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ZipCode
                               */
                               public void setZipCode(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localZipCodeTracker = param != null;
                                   
                                            this.localZipCode=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":PartnerSearchBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "PartnerSearchBasic",
                           xmlWriter);
                   }

                if (localAddressTracker){
                                            if (localAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("address cannot be null!!");
                                            }
                                           localAddress.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address"),
                                               xmlWriter);
                                        } if (localAddresseeTracker){
                                            if (localAddressee==null){
                                                 throw new org.apache.axis2.databinding.ADBException("addressee cannot be null!!");
                                            }
                                           localAddressee.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressee"),
                                               xmlWriter);
                                        } if (localAddressLabelTracker){
                                            if (localAddressLabel==null){
                                                 throw new org.apache.axis2.databinding.ADBException("addressLabel cannot be null!!");
                                            }
                                           localAddressLabel.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressLabel"),
                                               xmlWriter);
                                        } if (localAddressPhoneTracker){
                                            if (localAddressPhone==null){
                                                 throw new org.apache.axis2.databinding.ADBException("addressPhone cannot be null!!");
                                            }
                                           localAddressPhone.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressPhone"),
                                               xmlWriter);
                                        } if (localAssignTasksTracker){
                                            if (localAssignTasks==null){
                                                 throw new org.apache.axis2.databinding.ADBException("assignTasks cannot be null!!");
                                            }
                                           localAssignTasks.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","assignTasks"),
                                               xmlWriter);
                                        } if (localAttentionTracker){
                                            if (localAttention==null){
                                                 throw new org.apache.axis2.databinding.ADBException("attention cannot be null!!");
                                            }
                                           localAttention.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","attention"),
                                               xmlWriter);
                                        } if (localBillAddressTracker){
                                            if (localBillAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("billAddress cannot be null!!");
                                            }
                                           localBillAddress.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billAddress"),
                                               xmlWriter);
                                        } if (localCategoryTracker){
                                            if (localCategory==null){
                                                 throw new org.apache.axis2.databinding.ADBException("category cannot be null!!");
                                            }
                                           localCategory.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","category"),
                                               xmlWriter);
                                        } if (localCityTracker){
                                            if (localCity==null){
                                                 throw new org.apache.axis2.databinding.ADBException("city cannot be null!!");
                                            }
                                           localCity.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","city"),
                                               xmlWriter);
                                        } if (local_classTracker){
                                            if (local_class==null){
                                                 throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                            }
                                           local_class.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","class"),
                                               xmlWriter);
                                        } if (localCommentsTracker){
                                            if (localComments==null){
                                                 throw new org.apache.axis2.databinding.ADBException("comments cannot be null!!");
                                            }
                                           localComments.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","comments"),
                                               xmlWriter);
                                        } if (localCommissionPlanTracker){
                                            if (localCommissionPlan==null){
                                                 throw new org.apache.axis2.databinding.ADBException("commissionPlan cannot be null!!");
                                            }
                                           localCommissionPlan.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","commissionPlan"),
                                               xmlWriter);
                                        } if (localCountryTracker){
                                            if (localCountry==null){
                                                 throw new org.apache.axis2.databinding.ADBException("country cannot be null!!");
                                            }
                                           localCountry.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","country"),
                                               xmlWriter);
                                        } if (localCountyTracker){
                                            if (localCounty==null){
                                                 throw new org.apache.axis2.databinding.ADBException("county cannot be null!!");
                                            }
                                           localCounty.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","county"),
                                               xmlWriter);
                                        } if (localDateCreatedTracker){
                                            if (localDateCreated==null){
                                                 throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                            }
                                           localDateCreated.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated"),
                                               xmlWriter);
                                        } if (localDepartmentTracker){
                                            if (localDepartment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                            }
                                           localDepartment.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","department"),
                                               xmlWriter);
                                        } if (localEligibleForCommissionTracker){
                                            if (localEligibleForCommission==null){
                                                 throw new org.apache.axis2.databinding.ADBException("eligibleForCommission cannot be null!!");
                                            }
                                           localEligibleForCommission.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","eligibleForCommission"),
                                               xmlWriter);
                                        } if (localEmailTracker){
                                            if (localEmail==null){
                                                 throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                            }
                                           localEmail.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","email"),
                                               xmlWriter);
                                        } if (localEmailPreferenceTracker){
                                            if (localEmailPreference==null){
                                                 throw new org.apache.axis2.databinding.ADBException("emailPreference cannot be null!!");
                                            }
                                           localEmailPreference.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","emailPreference"),
                                               xmlWriter);
                                        } if (localEntityIdTracker){
                                            if (localEntityId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("entityId cannot be null!!");
                                            }
                                           localEntityId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entityId"),
                                               xmlWriter);
                                        } if (localExternalIdTracker){
                                            if (localExternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                            }
                                           localExternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId"),
                                               xmlWriter);
                                        } if (localExternalIdStringTracker){
                                            if (localExternalIdString==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                            }
                                           localExternalIdString.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString"),
                                               xmlWriter);
                                        } if (localFaxTracker){
                                            if (localFax==null){
                                                 throw new org.apache.axis2.databinding.ADBException("fax cannot be null!!");
                                            }
                                           localFax.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fax"),
                                               xmlWriter);
                                        } if (localFirstNameTracker){
                                            if (localFirstName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("firstName cannot be null!!");
                                            }
                                           localFirstName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","firstName"),
                                               xmlWriter);
                                        } if (localGiveAccessTracker){
                                            if (localGiveAccess==null){
                                                 throw new org.apache.axis2.databinding.ADBException("giveAccess cannot be null!!");
                                            }
                                           localGiveAccess.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","giveAccess"),
                                               xmlWriter);
                                        } if (localGlobalSubscriptionStatusTracker){
                                            if (localGlobalSubscriptionStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("globalSubscriptionStatus cannot be null!!");
                                            }
                                           localGlobalSubscriptionStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","globalSubscriptionStatus"),
                                               xmlWriter);
                                        } if (localGroupTracker){
                                            if (localGroup==null){
                                                 throw new org.apache.axis2.databinding.ADBException("group cannot be null!!");
                                            }
                                           localGroup.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","group"),
                                               xmlWriter);
                                        } if (localHasDuplicatesTracker){
                                            if (localHasDuplicates==null){
                                                 throw new org.apache.axis2.databinding.ADBException("hasDuplicates cannot be null!!");
                                            }
                                           localHasDuplicates.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","hasDuplicates"),
                                               xmlWriter);
                                        } if (localImageTracker){
                                            if (localImage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("image cannot be null!!");
                                            }
                                           localImage.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","image"),
                                               xmlWriter);
                                        } if (localInternalIdTracker){
                                            if (localInternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                            }
                                           localInternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                               xmlWriter);
                                        } if (localInternalIdNumberTracker){
                                            if (localInternalIdNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                            }
                                           localInternalIdNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber"),
                                               xmlWriter);
                                        } if (localIsDefaultBillingTracker){
                                            if (localIsDefaultBilling==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isDefaultBilling cannot be null!!");
                                            }
                                           localIsDefaultBilling.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isDefaultBilling"),
                                               xmlWriter);
                                        } if (localIsDefaultShippingTracker){
                                            if (localIsDefaultShipping==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isDefaultShipping cannot be null!!");
                                            }
                                           localIsDefaultShipping.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isDefaultShipping"),
                                               xmlWriter);
                                        } if (localIsInactiveTracker){
                                            if (localIsInactive==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                            }
                                           localIsInactive.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isInactive"),
                                               xmlWriter);
                                        } if (localIsPersonTracker){
                                            if (localIsPerson==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isPerson cannot be null!!");
                                            }
                                           localIsPerson.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isPerson"),
                                               xmlWriter);
                                        } if (localLanguageTracker){
                                            if (localLanguage==null){
                                                 throw new org.apache.axis2.databinding.ADBException("language cannot be null!!");
                                            }
                                           localLanguage.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","language"),
                                               xmlWriter);
                                        } if (localLastModifiedDateTracker){
                                            if (localLastModifiedDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                            }
                                           localLastModifiedDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate"),
                                               xmlWriter);
                                        } if (localLastNameTracker){
                                            if (localLastName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lastName cannot be null!!");
                                            }
                                           localLastName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastName"),
                                               xmlWriter);
                                        } if (localLevelTracker){
                                            if (localLevel==null){
                                                 throw new org.apache.axis2.databinding.ADBException("level cannot be null!!");
                                            }
                                           localLevel.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","level"),
                                               xmlWriter);
                                        } if (localLocationTracker){
                                            if (localLocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                            }
                                           localLocation.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","location"),
                                               xmlWriter);
                                        } if (localMiddleNameTracker){
                                            if (localMiddleName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("middleName cannot be null!!");
                                            }
                                           localMiddleName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","middleName"),
                                               xmlWriter);
                                        } if (localOtherRelationshipsTracker){
                                            if (localOtherRelationships==null){
                                                 throw new org.apache.axis2.databinding.ADBException("otherRelationships cannot be null!!");
                                            }
                                           localOtherRelationships.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","otherRelationships"),
                                               xmlWriter);
                                        } if (localParentTracker){
                                            if (localParent==null){
                                                 throw new org.apache.axis2.databinding.ADBException("parent cannot be null!!");
                                            }
                                           localParent.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","parent"),
                                               xmlWriter);
                                        } if (localPartnerCodeTracker){
                                            if (localPartnerCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("partnerCode cannot be null!!");
                                            }
                                           localPartnerCode.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerCode"),
                                               xmlWriter);
                                        } if (localPermissionTracker){
                                            if (localPermission==null){
                                                 throw new org.apache.axis2.databinding.ADBException("permission cannot be null!!");
                                            }
                                           localPermission.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","permission"),
                                               xmlWriter);
                                        } if (localPhoneTracker){
                                            if (localPhone==null){
                                                 throw new org.apache.axis2.databinding.ADBException("phone cannot be null!!");
                                            }
                                           localPhone.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phone"),
                                               xmlWriter);
                                        } if (localPhoneticNameTracker){
                                            if (localPhoneticName==null){
                                                 throw new org.apache.axis2.databinding.ADBException("phoneticName cannot be null!!");
                                            }
                                           localPhoneticName.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phoneticName"),
                                               xmlWriter);
                                        } if (localPromoCodeTracker){
                                            if (localPromoCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("promoCode cannot be null!!");
                                            }
                                           localPromoCode.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","promoCode"),
                                               xmlWriter);
                                        } if (localSalutationTracker){
                                            if (localSalutation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("salutation cannot be null!!");
                                            }
                                           localSalutation.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salutation"),
                                               xmlWriter);
                                        } if (localShipAddressTracker){
                                            if (localShipAddress==null){
                                                 throw new org.apache.axis2.databinding.ADBException("shipAddress cannot be null!!");
                                            }
                                           localShipAddress.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","shipAddress"),
                                               xmlWriter);
                                        } if (localStateTracker){
                                            if (localState==null){
                                                 throw new org.apache.axis2.databinding.ADBException("state cannot be null!!");
                                            }
                                           localState.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","state"),
                                               xmlWriter);
                                        } if (localSubsidiaryTracker){
                                            if (localSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                            }
                                           localSubsidiary.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary"),
                                               xmlWriter);
                                        } if (localTitleTracker){
                                            if (localTitle==null){
                                                 throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                            }
                                           localTitle.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","title"),
                                               xmlWriter);
                                        } if (localURLTracker){
                                            if (localURL==null){
                                                 throw new org.apache.axis2.databinding.ADBException("URL cannot be null!!");
                                            }
                                           localURL.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","URL"),
                                               xmlWriter);
                                        } if (localZipCodeTracker){
                                            if (localZipCode==null){
                                                 throw new org.apache.axis2.databinding.ADBException("zipCode cannot be null!!");
                                            }
                                           localZipCode.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","zipCode"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","PartnerSearchBasic"));
                 if (localAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "address"));
                            
                            
                                    if (localAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("address cannot be null!!");
                                    }
                                    elementList.add(localAddress);
                                } if (localAddresseeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "addressee"));
                            
                            
                                    if (localAddressee==null){
                                         throw new org.apache.axis2.databinding.ADBException("addressee cannot be null!!");
                                    }
                                    elementList.add(localAddressee);
                                } if (localAddressLabelTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "addressLabel"));
                            
                            
                                    if (localAddressLabel==null){
                                         throw new org.apache.axis2.databinding.ADBException("addressLabel cannot be null!!");
                                    }
                                    elementList.add(localAddressLabel);
                                } if (localAddressPhoneTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "addressPhone"));
                            
                            
                                    if (localAddressPhone==null){
                                         throw new org.apache.axis2.databinding.ADBException("addressPhone cannot be null!!");
                                    }
                                    elementList.add(localAddressPhone);
                                } if (localAssignTasksTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "assignTasks"));
                            
                            
                                    if (localAssignTasks==null){
                                         throw new org.apache.axis2.databinding.ADBException("assignTasks cannot be null!!");
                                    }
                                    elementList.add(localAssignTasks);
                                } if (localAttentionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "attention"));
                            
                            
                                    if (localAttention==null){
                                         throw new org.apache.axis2.databinding.ADBException("attention cannot be null!!");
                                    }
                                    elementList.add(localAttention);
                                } if (localBillAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "billAddress"));
                            
                            
                                    if (localBillAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("billAddress cannot be null!!");
                                    }
                                    elementList.add(localBillAddress);
                                } if (localCategoryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "category"));
                            
                            
                                    if (localCategory==null){
                                         throw new org.apache.axis2.databinding.ADBException("category cannot be null!!");
                                    }
                                    elementList.add(localCategory);
                                } if (localCityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "city"));
                            
                            
                                    if (localCity==null){
                                         throw new org.apache.axis2.databinding.ADBException("city cannot be null!!");
                                    }
                                    elementList.add(localCity);
                                } if (local_classTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "class"));
                            
                            
                                    if (local_class==null){
                                         throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    }
                                    elementList.add(local_class);
                                } if (localCommentsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "comments"));
                            
                            
                                    if (localComments==null){
                                         throw new org.apache.axis2.databinding.ADBException("comments cannot be null!!");
                                    }
                                    elementList.add(localComments);
                                } if (localCommissionPlanTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "commissionPlan"));
                            
                            
                                    if (localCommissionPlan==null){
                                         throw new org.apache.axis2.databinding.ADBException("commissionPlan cannot be null!!");
                                    }
                                    elementList.add(localCommissionPlan);
                                } if (localCountryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "country"));
                            
                            
                                    if (localCountry==null){
                                         throw new org.apache.axis2.databinding.ADBException("country cannot be null!!");
                                    }
                                    elementList.add(localCountry);
                                } if (localCountyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "county"));
                            
                            
                                    if (localCounty==null){
                                         throw new org.apache.axis2.databinding.ADBException("county cannot be null!!");
                                    }
                                    elementList.add(localCounty);
                                } if (localDateCreatedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "dateCreated"));
                            
                            
                                    if (localDateCreated==null){
                                         throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                    }
                                    elementList.add(localDateCreated);
                                } if (localDepartmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "department"));
                            
                            
                                    if (localDepartment==null){
                                         throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    }
                                    elementList.add(localDepartment);
                                } if (localEligibleForCommissionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "eligibleForCommission"));
                            
                            
                                    if (localEligibleForCommission==null){
                                         throw new org.apache.axis2.databinding.ADBException("eligibleForCommission cannot be null!!");
                                    }
                                    elementList.add(localEligibleForCommission);
                                } if (localEmailTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "email"));
                            
                            
                                    if (localEmail==null){
                                         throw new org.apache.axis2.databinding.ADBException("email cannot be null!!");
                                    }
                                    elementList.add(localEmail);
                                } if (localEmailPreferenceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "emailPreference"));
                            
                            
                                    if (localEmailPreference==null){
                                         throw new org.apache.axis2.databinding.ADBException("emailPreference cannot be null!!");
                                    }
                                    elementList.add(localEmailPreference);
                                } if (localEntityIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "entityId"));
                            
                            
                                    if (localEntityId==null){
                                         throw new org.apache.axis2.databinding.ADBException("entityId cannot be null!!");
                                    }
                                    elementList.add(localEntityId);
                                } if (localExternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalId"));
                            
                            
                                    if (localExternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                    }
                                    elementList.add(localExternalId);
                                } if (localExternalIdStringTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalIdString"));
                            
                            
                                    if (localExternalIdString==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                    }
                                    elementList.add(localExternalIdString);
                                } if (localFaxTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "fax"));
                            
                            
                                    if (localFax==null){
                                         throw new org.apache.axis2.databinding.ADBException("fax cannot be null!!");
                                    }
                                    elementList.add(localFax);
                                } if (localFirstNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "firstName"));
                            
                            
                                    if (localFirstName==null){
                                         throw new org.apache.axis2.databinding.ADBException("firstName cannot be null!!");
                                    }
                                    elementList.add(localFirstName);
                                } if (localGiveAccessTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "giveAccess"));
                            
                            
                                    if (localGiveAccess==null){
                                         throw new org.apache.axis2.databinding.ADBException("giveAccess cannot be null!!");
                                    }
                                    elementList.add(localGiveAccess);
                                } if (localGlobalSubscriptionStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "globalSubscriptionStatus"));
                            
                            
                                    if (localGlobalSubscriptionStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("globalSubscriptionStatus cannot be null!!");
                                    }
                                    elementList.add(localGlobalSubscriptionStatus);
                                } if (localGroupTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "group"));
                            
                            
                                    if (localGroup==null){
                                         throw new org.apache.axis2.databinding.ADBException("group cannot be null!!");
                                    }
                                    elementList.add(localGroup);
                                } if (localHasDuplicatesTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "hasDuplicates"));
                            
                            
                                    if (localHasDuplicates==null){
                                         throw new org.apache.axis2.databinding.ADBException("hasDuplicates cannot be null!!");
                                    }
                                    elementList.add(localHasDuplicates);
                                } if (localImageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "image"));
                            
                            
                                    if (localImage==null){
                                         throw new org.apache.axis2.databinding.ADBException("image cannot be null!!");
                                    }
                                    elementList.add(localImage);
                                } if (localInternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalId"));
                            
                            
                                    if (localInternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    }
                                    elementList.add(localInternalId);
                                } if (localInternalIdNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalIdNumber"));
                            
                            
                                    if (localInternalIdNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                    }
                                    elementList.add(localInternalIdNumber);
                                } if (localIsDefaultBillingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isDefaultBilling"));
                            
                            
                                    if (localIsDefaultBilling==null){
                                         throw new org.apache.axis2.databinding.ADBException("isDefaultBilling cannot be null!!");
                                    }
                                    elementList.add(localIsDefaultBilling);
                                } if (localIsDefaultShippingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isDefaultShipping"));
                            
                            
                                    if (localIsDefaultShipping==null){
                                         throw new org.apache.axis2.databinding.ADBException("isDefaultShipping cannot be null!!");
                                    }
                                    elementList.add(localIsDefaultShipping);
                                } if (localIsInactiveTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isInactive"));
                            
                            
                                    if (localIsInactive==null){
                                         throw new org.apache.axis2.databinding.ADBException("isInactive cannot be null!!");
                                    }
                                    elementList.add(localIsInactive);
                                } if (localIsPersonTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isPerson"));
                            
                            
                                    if (localIsPerson==null){
                                         throw new org.apache.axis2.databinding.ADBException("isPerson cannot be null!!");
                                    }
                                    elementList.add(localIsPerson);
                                } if (localLanguageTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "language"));
                            
                            
                                    if (localLanguage==null){
                                         throw new org.apache.axis2.databinding.ADBException("language cannot be null!!");
                                    }
                                    elementList.add(localLanguage);
                                } if (localLastModifiedDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                            
                            
                                    if (localLastModifiedDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                    }
                                    elementList.add(localLastModifiedDate);
                                } if (localLastNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lastName"));
                            
                            
                                    if (localLastName==null){
                                         throw new org.apache.axis2.databinding.ADBException("lastName cannot be null!!");
                                    }
                                    elementList.add(localLastName);
                                } if (localLevelTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "level"));
                            
                            
                                    if (localLevel==null){
                                         throw new org.apache.axis2.databinding.ADBException("level cannot be null!!");
                                    }
                                    elementList.add(localLevel);
                                } if (localLocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "location"));
                            
                            
                                    if (localLocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    }
                                    elementList.add(localLocation);
                                } if (localMiddleNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "middleName"));
                            
                            
                                    if (localMiddleName==null){
                                         throw new org.apache.axis2.databinding.ADBException("middleName cannot be null!!");
                                    }
                                    elementList.add(localMiddleName);
                                } if (localOtherRelationshipsTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "otherRelationships"));
                            
                            
                                    if (localOtherRelationships==null){
                                         throw new org.apache.axis2.databinding.ADBException("otherRelationships cannot be null!!");
                                    }
                                    elementList.add(localOtherRelationships);
                                } if (localParentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "parent"));
                            
                            
                                    if (localParent==null){
                                         throw new org.apache.axis2.databinding.ADBException("parent cannot be null!!");
                                    }
                                    elementList.add(localParent);
                                } if (localPartnerCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "partnerCode"));
                            
                            
                                    if (localPartnerCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("partnerCode cannot be null!!");
                                    }
                                    elementList.add(localPartnerCode);
                                } if (localPermissionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "permission"));
                            
                            
                                    if (localPermission==null){
                                         throw new org.apache.axis2.databinding.ADBException("permission cannot be null!!");
                                    }
                                    elementList.add(localPermission);
                                } if (localPhoneTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "phone"));
                            
                            
                                    if (localPhone==null){
                                         throw new org.apache.axis2.databinding.ADBException("phone cannot be null!!");
                                    }
                                    elementList.add(localPhone);
                                } if (localPhoneticNameTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "phoneticName"));
                            
                            
                                    if (localPhoneticName==null){
                                         throw new org.apache.axis2.databinding.ADBException("phoneticName cannot be null!!");
                                    }
                                    elementList.add(localPhoneticName);
                                } if (localPromoCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "promoCode"));
                            
                            
                                    if (localPromoCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("promoCode cannot be null!!");
                                    }
                                    elementList.add(localPromoCode);
                                } if (localSalutationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "salutation"));
                            
                            
                                    if (localSalutation==null){
                                         throw new org.apache.axis2.databinding.ADBException("salutation cannot be null!!");
                                    }
                                    elementList.add(localSalutation);
                                } if (localShipAddressTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "shipAddress"));
                            
                            
                                    if (localShipAddress==null){
                                         throw new org.apache.axis2.databinding.ADBException("shipAddress cannot be null!!");
                                    }
                                    elementList.add(localShipAddress);
                                } if (localStateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "state"));
                            
                            
                                    if (localState==null){
                                         throw new org.apache.axis2.databinding.ADBException("state cannot be null!!");
                                    }
                                    elementList.add(localState);
                                } if (localSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "subsidiary"));
                            
                            
                                    if (localSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    }
                                    elementList.add(localSubsidiary);
                                } if (localTitleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "title"));
                            
                            
                                    if (localTitle==null){
                                         throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                    }
                                    elementList.add(localTitle);
                                } if (localURLTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "URL"));
                            
                            
                                    if (localURL==null){
                                         throw new org.apache.axis2.databinding.ADBException("URL cannot be null!!");
                                    }
                                    elementList.add(localURL);
                                } if (localZipCodeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "zipCode"));
                            
                            
                                    if (localZipCode==null){
                                         throw new org.apache.axis2.databinding.ADBException("zipCode cannot be null!!");
                                    }
                                    elementList.add(localZipCode);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static PartnerSearchBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            PartnerSearchBasic object =
                new PartnerSearchBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"PartnerSearchBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (PartnerSearchBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","address").equals(reader.getName())){
                                
                                                object.setAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressee").equals(reader.getName())){
                                
                                                object.setAddressee(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressLabel").equals(reader.getName())){
                                
                                                object.setAddressLabel(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","addressPhone").equals(reader.getName())){
                                
                                                object.setAddressPhone(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","assignTasks").equals(reader.getName())){
                                
                                                object.setAssignTasks(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","attention").equals(reader.getName())){
                                
                                                object.setAttention(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","billAddress").equals(reader.getName())){
                                
                                                object.setBillAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","category").equals(reader.getName())){
                                
                                                object.setCategory(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","city").equals(reader.getName())){
                                
                                                object.setCity(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                                object.set_class(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","comments").equals(reader.getName())){
                                
                                                object.setComments(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","commissionPlan").equals(reader.getName())){
                                
                                                object.setCommissionPlan(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","country").equals(reader.getName())){
                                
                                                object.setCountry(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","county").equals(reader.getName())){
                                
                                                object.setCounty(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated").equals(reader.getName())){
                                
                                                object.setDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                                object.setDepartment(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","eligibleForCommission").equals(reader.getName())){
                                
                                                object.setEligibleForCommission(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","email").equals(reader.getName())){
                                
                                                object.setEmail(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","emailPreference").equals(reader.getName())){
                                
                                                object.setEmailPreference(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entityId").equals(reader.getName())){
                                
                                                object.setEntityId(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                
                                                object.setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString").equals(reader.getName())){
                                
                                                object.setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fax").equals(reader.getName())){
                                
                                                object.setFax(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","firstName").equals(reader.getName())){
                                
                                                object.setFirstName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","giveAccess").equals(reader.getName())){
                                
                                                object.setGiveAccess(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","globalSubscriptionStatus").equals(reader.getName())){
                                
                                                object.setGlobalSubscriptionStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","group").equals(reader.getName())){
                                
                                                object.setGroup(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","hasDuplicates").equals(reader.getName())){
                                
                                                object.setHasDuplicates(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","image").equals(reader.getName())){
                                
                                                object.setImage(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                                object.setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber").equals(reader.getName())){
                                
                                                object.setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isDefaultBilling").equals(reader.getName())){
                                
                                                object.setIsDefaultBilling(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isDefaultShipping").equals(reader.getName())){
                                
                                                object.setIsDefaultShipping(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isInactive").equals(reader.getName())){
                                
                                                object.setIsInactive(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isPerson").equals(reader.getName())){
                                
                                                object.setIsPerson(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","language").equals(reader.getName())){
                                
                                                object.setLanguage(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                                object.setLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastName").equals(reader.getName())){
                                
                                                object.setLastName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","level").equals(reader.getName())){
                                
                                                object.setLevel(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                                object.setLocation(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","middleName").equals(reader.getName())){
                                
                                                object.setMiddleName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","otherRelationships").equals(reader.getName())){
                                
                                                object.setOtherRelationships(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","parent").equals(reader.getName())){
                                
                                                object.setParent(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerCode").equals(reader.getName())){
                                
                                                object.setPartnerCode(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","permission").equals(reader.getName())){
                                
                                                object.setPermission(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phone").equals(reader.getName())){
                                
                                                object.setPhone(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","phoneticName").equals(reader.getName())){
                                
                                                object.setPhoneticName(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","promoCode").equals(reader.getName())){
                                
                                                object.setPromoCode(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salutation").equals(reader.getName())){
                                
                                                object.setSalutation(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","shipAddress").equals(reader.getName())){
                                
                                                object.setShipAddress(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","state").equals(reader.getName())){
                                
                                                object.setState(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                                object.setSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","title").equals(reader.getName())){
                                
                                                object.setTitle(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","URL").equals(reader.getName())){
                                
                                                object.setURL(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","zipCode").equals(reader.getName())){
                                
                                                object.setZipCode(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    