
/**
 * OpportunitySearchRowBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  OpportunitySearchRowBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class OpportunitySearchRowBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRowBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = OpportunitySearchRowBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for ActionItem
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localActionItem ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localActionItemTracker = false ;

                           public boolean isActionItemSpecified(){
                               return localActionItemTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getActionItem(){
                               return localActionItem;
                           }

                           
                        


                               
                              /**
                               * validate the array for ActionItem
                               */
                              protected void validateActionItem(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param ActionItem
                              */
                              public void setActionItem(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateActionItem(param);

                               localActionItemTracker = param != null;
                                      
                                      this.localActionItem=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addActionItem(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localActionItem == null){
                                   localActionItem = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localActionItemTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localActionItem);
                               list.add(param);
                               this.localActionItem =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for AvailableOffline
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localAvailableOffline ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAvailableOfflineTracker = false ;

                           public boolean isAvailableOfflineSpecified(){
                               return localAvailableOfflineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getAvailableOffline(){
                               return localAvailableOffline;
                           }

                           
                        


                               
                              /**
                               * validate the array for AvailableOffline
                               */
                              protected void validateAvailableOffline(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param AvailableOffline
                              */
                              public void setAvailableOffline(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateAvailableOffline(param);

                               localAvailableOfflineTracker = param != null;
                                      
                                      this.localAvailableOffline=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addAvailableOffline(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localAvailableOffline == null){
                                   localAvailableOffline = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localAvailableOfflineTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localAvailableOffline);
                               list.add(param);
                               this.localAvailableOffline =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for BuyingReason
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localBuyingReason ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBuyingReasonTracker = false ;

                           public boolean isBuyingReasonSpecified(){
                               return localBuyingReasonTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getBuyingReason(){
                               return localBuyingReason;
                           }

                           
                        


                               
                              /**
                               * validate the array for BuyingReason
                               */
                              protected void validateBuyingReason(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param BuyingReason
                              */
                              public void setBuyingReason(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateBuyingReason(param);

                               localBuyingReasonTracker = param != null;
                                      
                                      this.localBuyingReason=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addBuyingReason(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localBuyingReason == null){
                                   localBuyingReason = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localBuyingReasonTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localBuyingReason);
                               list.add(param);
                               this.localBuyingReason =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for BuyingTimeFrame
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localBuyingTimeFrame ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBuyingTimeFrameTracker = false ;

                           public boolean isBuyingTimeFrameSpecified(){
                               return localBuyingTimeFrameTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getBuyingTimeFrame(){
                               return localBuyingTimeFrame;
                           }

                           
                        


                               
                              /**
                               * validate the array for BuyingTimeFrame
                               */
                              protected void validateBuyingTimeFrame(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param BuyingTimeFrame
                              */
                              public void setBuyingTimeFrame(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateBuyingTimeFrame(param);

                               localBuyingTimeFrameTracker = param != null;
                                      
                                      this.localBuyingTimeFrame=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addBuyingTimeFrame(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localBuyingTimeFrame == null){
                                   localBuyingTimeFrame = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localBuyingTimeFrameTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localBuyingTimeFrame);
                               list.add(param);
                               this.localBuyingTimeFrame =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for _class
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] local_class ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_classTracker = false ;

                           public boolean is_classSpecified(){
                               return local_classTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] get_class(){
                               return local_class;
                           }

                           
                        


                               
                              /**
                               * validate the array for _class
                               */
                              protected void validate_class(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param _class
                              */
                              public void set_class(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validate_class(param);

                               local_classTracker = param != null;
                                      
                                      this.local_class=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void add_class(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (local_class == null){
                                   local_class = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                local_classTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(local_class);
                               list.add(param);
                               this.local_class =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for CloseDate
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localCloseDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCloseDateTracker = false ;

                           public boolean isCloseDateSpecified(){
                               return localCloseDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getCloseDate(){
                               return localCloseDate;
                           }

                           
                        


                               
                              /**
                               * validate the array for CloseDate
                               */
                              protected void validateCloseDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param CloseDate
                              */
                              public void setCloseDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateCloseDate(param);

                               localCloseDateTracker = param != null;
                                      
                                      this.localCloseDate=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addCloseDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localCloseDate == null){
                                   localCloseDate = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localCloseDateTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCloseDate);
                               list.add(param);
                               this.localCloseDate =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for Competitor
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localCompetitor ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCompetitorTracker = false ;

                           public boolean isCompetitorSpecified(){
                               return localCompetitorTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getCompetitor(){
                               return localCompetitor;
                           }

                           
                        


                               
                              /**
                               * validate the array for Competitor
                               */
                              protected void validateCompetitor(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Competitor
                              */
                              public void setCompetitor(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateCompetitor(param);

                               localCompetitorTracker = param != null;
                                      
                                      this.localCompetitor=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addCompetitor(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localCompetitor == null){
                                   localCompetitor = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localCompetitorTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCompetitor);
                               list.add(param);
                               this.localCompetitor =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for Contribution
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localContribution ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContributionTracker = false ;

                           public boolean isContributionSpecified(){
                               return localContributionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getContribution(){
                               return localContribution;
                           }

                           
                        


                               
                              /**
                               * validate the array for Contribution
                               */
                              protected void validateContribution(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Contribution
                              */
                              public void setContribution(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateContribution(param);

                               localContributionTracker = param != null;
                                      
                                      this.localContribution=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addContribution(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localContribution == null){
                                   localContribution = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localContributionTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localContribution);
                               list.add(param);
                               this.localContribution =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for ContributionPrimary
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localContributionPrimary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContributionPrimaryTracker = false ;

                           public boolean isContributionPrimarySpecified(){
                               return localContributionPrimaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getContributionPrimary(){
                               return localContributionPrimary;
                           }

                           
                        


                               
                              /**
                               * validate the array for ContributionPrimary
                               */
                              protected void validateContributionPrimary(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param ContributionPrimary
                              */
                              public void setContributionPrimary(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateContributionPrimary(param);

                               localContributionPrimaryTracker = param != null;
                                      
                                      this.localContributionPrimary=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addContributionPrimary(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localContributionPrimary == null){
                                   localContributionPrimary = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localContributionPrimaryTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localContributionPrimary);
                               list.add(param);
                               this.localContributionPrimary =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for Currency
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localCurrency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCurrencyTracker = false ;

                           public boolean isCurrencySpecified(){
                               return localCurrencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getCurrency(){
                               return localCurrency;
                           }

                           
                        


                               
                              /**
                               * validate the array for Currency
                               */
                              protected void validateCurrency(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Currency
                              */
                              public void setCurrency(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateCurrency(param);

                               localCurrencyTracker = param != null;
                                      
                                      this.localCurrency=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addCurrency(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localCurrency == null){
                                   localCurrency = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localCurrencyTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCurrency);
                               list.add(param);
                               this.localCurrency =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for CustType
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localCustType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustTypeTracker = false ;

                           public boolean isCustTypeSpecified(){
                               return localCustTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getCustType(){
                               return localCustType;
                           }

                           
                        


                               
                              /**
                               * validate the array for CustType
                               */
                              protected void validateCustType(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param CustType
                              */
                              public void setCustType(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateCustType(param);

                               localCustTypeTracker = param != null;
                                      
                                      this.localCustType=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addCustType(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localCustType == null){
                                   localCustType = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localCustTypeTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCustType);
                               list.add(param);
                               this.localCustType =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for DateCreated
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localDateCreated ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDateCreatedTracker = false ;

                           public boolean isDateCreatedSpecified(){
                               return localDateCreatedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getDateCreated(){
                               return localDateCreated;
                           }

                           
                        


                               
                              /**
                               * validate the array for DateCreated
                               */
                              protected void validateDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param DateCreated
                              */
                              public void setDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateDateCreated(param);

                               localDateCreatedTracker = param != null;
                                      
                                      this.localDateCreated=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addDateCreated(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localDateCreated == null){
                                   localDateCreated = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localDateCreatedTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localDateCreated);
                               list.add(param);
                               this.localDateCreated =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for DaysOpen
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] localDaysOpen ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDaysOpenTracker = false ;

                           public boolean isDaysOpenSpecified(){
                               return localDaysOpenTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] getDaysOpen(){
                               return localDaysOpen;
                           }

                           
                        


                               
                              /**
                               * validate the array for DaysOpen
                               */
                              protected void validateDaysOpen(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param DaysOpen
                              */
                              public void setDaysOpen(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] param){
                              
                                   validateDaysOpen(param);

                               localDaysOpenTracker = param != null;
                                      
                                      this.localDaysOpen=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField
                             */
                             public void addDaysOpen(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField param){
                                   if (localDaysOpen == null){
                                   localDaysOpen = new com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[]{};
                                   }

                            
                                 //update the setting tracker
                                localDaysOpenTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localDaysOpen);
                               list.add(param);
                               this.localDaysOpen =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[list.size()]);

                             }
                             

                        /**
                        * field for DaysToClose
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] localDaysToClose ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDaysToCloseTracker = false ;

                           public boolean isDaysToCloseSpecified(){
                               return localDaysToCloseTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] getDaysToClose(){
                               return localDaysToClose;
                           }

                           
                        


                               
                              /**
                               * validate the array for DaysToClose
                               */
                              protected void validateDaysToClose(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param DaysToClose
                              */
                              public void setDaysToClose(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[] param){
                              
                                   validateDaysToClose(param);

                               localDaysToCloseTracker = param != null;
                                      
                                      this.localDaysToClose=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField
                             */
                             public void addDaysToClose(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField param){
                                   if (localDaysToClose == null){
                                   localDaysToClose = new com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[]{};
                                   }

                            
                                 //update the setting tracker
                                localDaysToCloseTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localDaysToClose);
                               list.add(param);
                               this.localDaysToClose =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[list.size()]);

                             }
                             

                        /**
                        * field for DecisionMaker
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localDecisionMaker ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDecisionMakerTracker = false ;

                           public boolean isDecisionMakerSpecified(){
                               return localDecisionMakerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getDecisionMaker(){
                               return localDecisionMaker;
                           }

                           
                        


                               
                              /**
                               * validate the array for DecisionMaker
                               */
                              protected void validateDecisionMaker(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param DecisionMaker
                              */
                              public void setDecisionMaker(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateDecisionMaker(param);

                               localDecisionMakerTracker = param != null;
                                      
                                      this.localDecisionMaker=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addDecisionMaker(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localDecisionMaker == null){
                                   localDecisionMaker = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localDecisionMakerTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localDecisionMaker);
                               list.add(param);
                               this.localDecisionMaker =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for Department
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localDepartment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDepartmentTracker = false ;

                           public boolean isDepartmentSpecified(){
                               return localDepartmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getDepartment(){
                               return localDepartment;
                           }

                           
                        


                               
                              /**
                               * validate the array for Department
                               */
                              protected void validateDepartment(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Department
                              */
                              public void setDepartment(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateDepartment(param);

                               localDepartmentTracker = param != null;
                                      
                                      this.localDepartment=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addDepartment(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localDepartment == null){
                                   localDepartment = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localDepartmentTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localDepartment);
                               list.add(param);
                               this.localDepartment =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for Status
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] localStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStatusTracker = false ;

                           public boolean isStatusSpecified(){
                               return localStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] getStatus(){
                               return localStatus;
                           }

                           
                        


                               
                              /**
                               * validate the array for Status
                               */
                              protected void validateStatus(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Status
                              */
                              public void setStatus(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                              
                                   validateStatus(param);

                               localStatusTracker = param != null;
                                      
                                      this.localStatus=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField
                             */
                             public void addStatus(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField param){
                                   if (localStatus == null){
                                   localStatus = new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localStatusTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localStatus);
                               list.add(param);
                               this.localStatus =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[list.size()]);

                             }
                             

                        /**
                        * field for Entity
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localEntity ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityTracker = false ;

                           public boolean isEntitySpecified(){
                               return localEntityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getEntity(){
                               return localEntity;
                           }

                           
                        


                               
                              /**
                               * validate the array for Entity
                               */
                              protected void validateEntity(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Entity
                              */
                              public void setEntity(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateEntity(param);

                               localEntityTracker = param != null;
                                      
                                      this.localEntity=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addEntity(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localEntity == null){
                                   localEntity = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localEntityTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localEntity);
                               list.add(param);
                               this.localEntity =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for EntityStatus
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localEntityStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEntityStatusTracker = false ;

                           public boolean isEntityStatusSpecified(){
                               return localEntityStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getEntityStatus(){
                               return localEntityStatus;
                           }

                           
                        


                               
                              /**
                               * validate the array for EntityStatus
                               */
                              protected void validateEntityStatus(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param EntityStatus
                              */
                              public void setEntityStatus(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateEntityStatus(param);

                               localEntityStatusTracker = param != null;
                                      
                                      this.localEntityStatus=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addEntityStatus(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localEntityStatus == null){
                                   localEntityStatus = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localEntityStatusTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localEntityStatus);
                               list.add(param);
                               this.localEntityStatus =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for EstimatedBudget
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localEstimatedBudget ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstimatedBudgetTracker = false ;

                           public boolean isEstimatedBudgetSpecified(){
                               return localEstimatedBudgetTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getEstimatedBudget(){
                               return localEstimatedBudget;
                           }

                           
                        


                               
                              /**
                               * validate the array for EstimatedBudget
                               */
                              protected void validateEstimatedBudget(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param EstimatedBudget
                              */
                              public void setEstimatedBudget(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateEstimatedBudget(param);

                               localEstimatedBudgetTracker = param != null;
                                      
                                      this.localEstimatedBudget=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addEstimatedBudget(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localEstimatedBudget == null){
                                   localEstimatedBudget = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localEstimatedBudgetTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localEstimatedBudget);
                               list.add(param);
                               this.localEstimatedBudget =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for ExpectedCloseDate
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localExpectedCloseDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExpectedCloseDateTracker = false ;

                           public boolean isExpectedCloseDateSpecified(){
                               return localExpectedCloseDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getExpectedCloseDate(){
                               return localExpectedCloseDate;
                           }

                           
                        


                               
                              /**
                               * validate the array for ExpectedCloseDate
                               */
                              protected void validateExpectedCloseDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param ExpectedCloseDate
                              */
                              public void setExpectedCloseDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateExpectedCloseDate(param);

                               localExpectedCloseDateTracker = param != null;
                                      
                                      this.localExpectedCloseDate=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addExpectedCloseDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localExpectedCloseDate == null){
                                   localExpectedCloseDate = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localExpectedCloseDateTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localExpectedCloseDate);
                               list.add(param);
                               this.localExpectedCloseDate =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for ExternalId
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localExternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdTracker = false ;

                           public boolean isExternalIdSpecified(){
                               return localExternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getExternalId(){
                               return localExternalId;
                           }

                           
                        


                               
                              /**
                               * validate the array for ExternalId
                               */
                              protected void validateExternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param ExternalId
                              */
                              public void setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateExternalId(param);

                               localExternalIdTracker = param != null;
                                      
                                      this.localExternalId=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addExternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localExternalId == null){
                                   localExternalId = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localExternalIdTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localExternalId);
                               list.add(param);
                               this.localExternalId =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for ForecastType
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] localForecastType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localForecastTypeTracker = false ;

                           public boolean isForecastTypeSpecified(){
                               return localForecastTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] getForecastType(){
                               return localForecastType;
                           }

                           
                        


                               
                              /**
                               * validate the array for ForecastType
                               */
                              protected void validateForecastType(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param ForecastType
                              */
                              public void setForecastType(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[] param){
                              
                                   validateForecastType(param);

                               localForecastTypeTracker = param != null;
                                      
                                      this.localForecastType=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField
                             */
                             public void addForecastType(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField param){
                                   if (localForecastType == null){
                                   localForecastType = new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localForecastTypeTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localForecastType);
                               list.add(param);
                               this.localForecastType =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[list.size()]);

                             }
                             

                        /**
                        * field for ForeignProjectedAmount
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localForeignProjectedAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localForeignProjectedAmountTracker = false ;

                           public boolean isForeignProjectedAmountSpecified(){
                               return localForeignProjectedAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getForeignProjectedAmount(){
                               return localForeignProjectedAmount;
                           }

                           
                        


                               
                              /**
                               * validate the array for ForeignProjectedAmount
                               */
                              protected void validateForeignProjectedAmount(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param ForeignProjectedAmount
                              */
                              public void setForeignProjectedAmount(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateForeignProjectedAmount(param);

                               localForeignProjectedAmountTracker = param != null;
                                      
                                      this.localForeignProjectedAmount=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addForeignProjectedAmount(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localForeignProjectedAmount == null){
                                   localForeignProjectedAmount = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localForeignProjectedAmountTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localForeignProjectedAmount);
                               list.add(param);
                               this.localForeignProjectedAmount =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for ForeignRangeHigh
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localForeignRangeHigh ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localForeignRangeHighTracker = false ;

                           public boolean isForeignRangeHighSpecified(){
                               return localForeignRangeHighTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getForeignRangeHigh(){
                               return localForeignRangeHigh;
                           }

                           
                        


                               
                              /**
                               * validate the array for ForeignRangeHigh
                               */
                              protected void validateForeignRangeHigh(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param ForeignRangeHigh
                              */
                              public void setForeignRangeHigh(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateForeignRangeHigh(param);

                               localForeignRangeHighTracker = param != null;
                                      
                                      this.localForeignRangeHigh=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addForeignRangeHigh(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localForeignRangeHigh == null){
                                   localForeignRangeHigh = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localForeignRangeHighTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localForeignRangeHigh);
                               list.add(param);
                               this.localForeignRangeHigh =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for ForeignRangeLow
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localForeignRangeLow ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localForeignRangeLowTracker = false ;

                           public boolean isForeignRangeLowSpecified(){
                               return localForeignRangeLowTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getForeignRangeLow(){
                               return localForeignRangeLow;
                           }

                           
                        


                               
                              /**
                               * validate the array for ForeignRangeLow
                               */
                              protected void validateForeignRangeLow(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param ForeignRangeLow
                              */
                              public void setForeignRangeLow(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateForeignRangeLow(param);

                               localForeignRangeLowTracker = param != null;
                                      
                                      this.localForeignRangeLow=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addForeignRangeLow(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localForeignRangeLow == null){
                                   localForeignRangeLow = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localForeignRangeLowTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localForeignRangeLow);
                               list.add(param);
                               this.localForeignRangeLow =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for FxTranCostEstimate
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localFxTranCostEstimate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFxTranCostEstimateTracker = false ;

                           public boolean isFxTranCostEstimateSpecified(){
                               return localFxTranCostEstimateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getFxTranCostEstimate(){
                               return localFxTranCostEstimate;
                           }

                           
                        


                               
                              /**
                               * validate the array for FxTranCostEstimate
                               */
                              protected void validateFxTranCostEstimate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param FxTranCostEstimate
                              */
                              public void setFxTranCostEstimate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateFxTranCostEstimate(param);

                               localFxTranCostEstimateTracker = param != null;
                                      
                                      this.localFxTranCostEstimate=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addFxTranCostEstimate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localFxTranCostEstimate == null){
                                   localFxTranCostEstimate = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localFxTranCostEstimateTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localFxTranCostEstimate);
                               list.add(param);
                               this.localFxTranCostEstimate =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for InternalId
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getInternalId(){
                               return localInternalId;
                           }

                           
                        


                               
                              /**
                               * validate the array for InternalId
                               */
                              protected void validateInternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param InternalId
                              */
                              public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateInternalId(param);

                               localInternalIdTracker = param != null;
                                      
                                      this.localInternalId=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addInternalId(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localInternalId == null){
                                   localInternalId = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localInternalIdTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localInternalId);
                               list.add(param);
                               this.localInternalId =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for IsBudgetApproved
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] localIsBudgetApproved ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsBudgetApprovedTracker = false ;

                           public boolean isIsBudgetApprovedSpecified(){
                               return localIsBudgetApprovedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] getIsBudgetApproved(){
                               return localIsBudgetApproved;
                           }

                           
                        


                               
                              /**
                               * validate the array for IsBudgetApproved
                               */
                              protected void validateIsBudgetApproved(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param IsBudgetApproved
                              */
                              public void setIsBudgetApproved(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[] param){
                              
                                   validateIsBudgetApproved(param);

                               localIsBudgetApprovedTracker = param != null;
                                      
                                      this.localIsBudgetApproved=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField
                             */
                             public void addIsBudgetApproved(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField param){
                                   if (localIsBudgetApproved == null){
                                   localIsBudgetApproved = new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[]{};
                                   }

                            
                                 //update the setting tracker
                                localIsBudgetApprovedTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localIsBudgetApproved);
                               list.add(param);
                               this.localIsBudgetApproved =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[list.size()]);

                             }
                             

                        /**
                        * field for LastModifiedDate
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        


                               
                              /**
                               * validate the array for LastModifiedDate
                               */
                              protected void validateLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param LastModifiedDate
                              */
                              public void setLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateLastModifiedDate(param);

                               localLastModifiedDateTracker = param != null;
                                      
                                      this.localLastModifiedDate=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localLastModifiedDate == null){
                                   localLastModifiedDate = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localLastModifiedDateTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localLastModifiedDate);
                               list.add(param);
                               this.localLastModifiedDate =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for LeadSource
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localLeadSource ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLeadSourceTracker = false ;

                           public boolean isLeadSourceSpecified(){
                               return localLeadSourceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getLeadSource(){
                               return localLeadSource;
                           }

                           
                        


                               
                              /**
                               * validate the array for LeadSource
                               */
                              protected void validateLeadSource(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param LeadSource
                              */
                              public void setLeadSource(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateLeadSource(param);

                               localLeadSourceTracker = param != null;
                                      
                                      this.localLeadSource=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addLeadSource(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localLeadSource == null){
                                   localLeadSource = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localLeadSourceTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localLeadSource);
                               list.add(param);
                               this.localLeadSource =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for Location
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getLocation(){
                               return localLocation;
                           }

                           
                        


                               
                              /**
                               * validate the array for Location
                               */
                              protected void validateLocation(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Location
                              */
                              public void setLocation(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateLocation(param);

                               localLocationTracker = param != null;
                                      
                                      this.localLocation=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addLocation(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localLocation == null){
                                   localLocation = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localLocationTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localLocation);
                               list.add(param);
                               this.localLocation =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for Memo
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localMemo ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMemoTracker = false ;

                           public boolean isMemoSpecified(){
                               return localMemoTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getMemo(){
                               return localMemo;
                           }

                           
                        


                               
                              /**
                               * validate the array for Memo
                               */
                              protected void validateMemo(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Memo
                              */
                              public void setMemo(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateMemo(param);

                               localMemoTracker = param != null;
                                      
                                      this.localMemo=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addMemo(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localMemo == null){
                                   localMemo = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localMemoTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localMemo);
                               list.add(param);
                               this.localMemo =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for Partner
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localPartner ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerTracker = false ;

                           public boolean isPartnerSpecified(){
                               return localPartnerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getPartner(){
                               return localPartner;
                           }

                           
                        


                               
                              /**
                               * validate the array for Partner
                               */
                              protected void validatePartner(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Partner
                              */
                              public void setPartner(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validatePartner(param);

                               localPartnerTracker = param != null;
                                      
                                      this.localPartner=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addPartner(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localPartner == null){
                                   localPartner = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localPartnerTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localPartner);
                               list.add(param);
                               this.localPartner =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for PartnerContribution
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localPartnerContribution ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerContributionTracker = false ;

                           public boolean isPartnerContributionSpecified(){
                               return localPartnerContributionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getPartnerContribution(){
                               return localPartnerContribution;
                           }

                           
                        


                               
                              /**
                               * validate the array for PartnerContribution
                               */
                              protected void validatePartnerContribution(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param PartnerContribution
                              */
                              public void setPartnerContribution(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validatePartnerContribution(param);

                               localPartnerContributionTracker = param != null;
                                      
                                      this.localPartnerContribution=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addPartnerContribution(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localPartnerContribution == null){
                                   localPartnerContribution = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localPartnerContributionTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localPartnerContribution);
                               list.add(param);
                               this.localPartnerContribution =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for PartnerRole
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localPartnerRole ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerRoleTracker = false ;

                           public boolean isPartnerRoleSpecified(){
                               return localPartnerRoleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getPartnerRole(){
                               return localPartnerRole;
                           }

                           
                        


                               
                              /**
                               * validate the array for PartnerRole
                               */
                              protected void validatePartnerRole(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param PartnerRole
                              */
                              public void setPartnerRole(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validatePartnerRole(param);

                               localPartnerRoleTracker = param != null;
                                      
                                      this.localPartnerRole=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addPartnerRole(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localPartnerRole == null){
                                   localPartnerRole = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localPartnerRoleTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localPartnerRole);
                               list.add(param);
                               this.localPartnerRole =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for PartnerTeamMember
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localPartnerTeamMember ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPartnerTeamMemberTracker = false ;

                           public boolean isPartnerTeamMemberSpecified(){
                               return localPartnerTeamMemberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getPartnerTeamMember(){
                               return localPartnerTeamMember;
                           }

                           
                        


                               
                              /**
                               * validate the array for PartnerTeamMember
                               */
                              protected void validatePartnerTeamMember(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param PartnerTeamMember
                              */
                              public void setPartnerTeamMember(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validatePartnerTeamMember(param);

                               localPartnerTeamMemberTracker = param != null;
                                      
                                      this.localPartnerTeamMember=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addPartnerTeamMember(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localPartnerTeamMember == null){
                                   localPartnerTeamMember = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localPartnerTeamMemberTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localPartnerTeamMember);
                               list.add(param);
                               this.localPartnerTeamMember =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for Period
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localPeriod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodTracker = false ;

                           public boolean isPeriodSpecified(){
                               return localPeriodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getPeriod(){
                               return localPeriod;
                           }

                           
                        


                               
                              /**
                               * validate the array for Period
                               */
                              protected void validatePeriod(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Period
                              */
                              public void setPeriod(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validatePeriod(param);

                               localPeriodTracker = param != null;
                                      
                                      this.localPeriod=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addPeriod(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localPeriod == null){
                                   localPeriod = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localPeriodTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localPeriod);
                               list.add(param);
                               this.localPeriod =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for Probability
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localProbability ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProbabilityTracker = false ;

                           public boolean isProbabilitySpecified(){
                               return localProbabilityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getProbability(){
                               return localProbability;
                           }

                           
                        


                               
                              /**
                               * validate the array for Probability
                               */
                              protected void validateProbability(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Probability
                              */
                              public void setProbability(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateProbability(param);

                               localProbabilityTracker = param != null;
                                      
                                      this.localProbability=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addProbability(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localProbability == null){
                                   localProbability = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localProbabilityTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localProbability);
                               list.add(param);
                               this.localProbability =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for ProjAltSalesAmt
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localProjAltSalesAmt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProjAltSalesAmtTracker = false ;

                           public boolean isProjAltSalesAmtSpecified(){
                               return localProjAltSalesAmtTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getProjAltSalesAmt(){
                               return localProjAltSalesAmt;
                           }

                           
                        


                               
                              /**
                               * validate the array for ProjAltSalesAmt
                               */
                              protected void validateProjAltSalesAmt(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param ProjAltSalesAmt
                              */
                              public void setProjAltSalesAmt(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateProjAltSalesAmt(param);

                               localProjAltSalesAmtTracker = param != null;
                                      
                                      this.localProjAltSalesAmt=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addProjAltSalesAmt(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localProjAltSalesAmt == null){
                                   localProjAltSalesAmt = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localProjAltSalesAmtTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localProjAltSalesAmt);
                               list.add(param);
                               this.localProjAltSalesAmt =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for ProjectedTotal
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localProjectedTotal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProjectedTotalTracker = false ;

                           public boolean isProjectedTotalSpecified(){
                               return localProjectedTotalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getProjectedTotal(){
                               return localProjectedTotal;
                           }

                           
                        


                               
                              /**
                               * validate the array for ProjectedTotal
                               */
                              protected void validateProjectedTotal(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param ProjectedTotal
                              */
                              public void setProjectedTotal(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateProjectedTotal(param);

                               localProjectedTotalTracker = param != null;
                                      
                                      this.localProjectedTotal=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addProjectedTotal(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localProjectedTotal == null){
                                   localProjectedTotal = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localProjectedTotalTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localProjectedTotal);
                               list.add(param);
                               this.localProjectedTotal =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for RangeHigh
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localRangeHigh ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRangeHighTracker = false ;

                           public boolean isRangeHighSpecified(){
                               return localRangeHighTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getRangeHigh(){
                               return localRangeHigh;
                           }

                           
                        


                               
                              /**
                               * validate the array for RangeHigh
                               */
                              protected void validateRangeHigh(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param RangeHigh
                              */
                              public void setRangeHigh(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateRangeHigh(param);

                               localRangeHighTracker = param != null;
                                      
                                      this.localRangeHigh=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addRangeHigh(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localRangeHigh == null){
                                   localRangeHigh = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localRangeHighTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localRangeHigh);
                               list.add(param);
                               this.localRangeHigh =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for RangeHighAlt
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localRangeHighAlt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRangeHighAltTracker = false ;

                           public boolean isRangeHighAltSpecified(){
                               return localRangeHighAltTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getRangeHighAlt(){
                               return localRangeHighAlt;
                           }

                           
                        


                               
                              /**
                               * validate the array for RangeHighAlt
                               */
                              protected void validateRangeHighAlt(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param RangeHighAlt
                              */
                              public void setRangeHighAlt(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateRangeHighAlt(param);

                               localRangeHighAltTracker = param != null;
                                      
                                      this.localRangeHighAlt=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addRangeHighAlt(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localRangeHighAlt == null){
                                   localRangeHighAlt = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localRangeHighAltTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localRangeHighAlt);
                               list.add(param);
                               this.localRangeHighAlt =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for RangeLow
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localRangeLow ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRangeLowTracker = false ;

                           public boolean isRangeLowSpecified(){
                               return localRangeLowTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getRangeLow(){
                               return localRangeLow;
                           }

                           
                        


                               
                              /**
                               * validate the array for RangeLow
                               */
                              protected void validateRangeLow(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param RangeLow
                              */
                              public void setRangeLow(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateRangeLow(param);

                               localRangeLowTracker = param != null;
                                      
                                      this.localRangeLow=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addRangeLow(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localRangeLow == null){
                                   localRangeLow = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localRangeLowTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localRangeLow);
                               list.add(param);
                               this.localRangeLow =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for RangeLowAlt
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localRangeLowAlt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRangeLowAltTracker = false ;

                           public boolean isRangeLowAltSpecified(){
                               return localRangeLowAltTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getRangeLowAlt(){
                               return localRangeLowAlt;
                           }

                           
                        


                               
                              /**
                               * validate the array for RangeLowAlt
                               */
                              protected void validateRangeLowAlt(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param RangeLowAlt
                              */
                              public void setRangeLowAlt(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateRangeLowAlt(param);

                               localRangeLowAltTracker = param != null;
                                      
                                      this.localRangeLowAlt=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addRangeLowAlt(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localRangeLowAlt == null){
                                   localRangeLowAlt = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localRangeLowAltTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localRangeLowAlt);
                               list.add(param);
                               this.localRangeLowAlt =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for SalesReadiness
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localSalesReadiness ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesReadinessTracker = false ;

                           public boolean isSalesReadinessSpecified(){
                               return localSalesReadinessTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getSalesReadiness(){
                               return localSalesReadiness;
                           }

                           
                        


                               
                              /**
                               * validate the array for SalesReadiness
                               */
                              protected void validateSalesReadiness(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param SalesReadiness
                              */
                              public void setSalesReadiness(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateSalesReadiness(param);

                               localSalesReadinessTracker = param != null;
                                      
                                      this.localSalesReadiness=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addSalesReadiness(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localSalesReadiness == null){
                                   localSalesReadiness = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localSalesReadinessTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localSalesReadiness);
                               list.add(param);
                               this.localSalesReadiness =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for SalesRep
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localSalesRep ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesRepTracker = false ;

                           public boolean isSalesRepSpecified(){
                               return localSalesRepTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getSalesRep(){
                               return localSalesRep;
                           }

                           
                        


                               
                              /**
                               * validate the array for SalesRep
                               */
                              protected void validateSalesRep(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param SalesRep
                              */
                              public void setSalesRep(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateSalesRep(param);

                               localSalesRepTracker = param != null;
                                      
                                      this.localSalesRep=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addSalesRep(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localSalesRep == null){
                                   localSalesRep = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localSalesRepTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localSalesRep);
                               list.add(param);
                               this.localSalesRep =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for SalesTeamMember
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localSalesTeamMember ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesTeamMemberTracker = false ;

                           public boolean isSalesTeamMemberSpecified(){
                               return localSalesTeamMemberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getSalesTeamMember(){
                               return localSalesTeamMember;
                           }

                           
                        


                               
                              /**
                               * validate the array for SalesTeamMember
                               */
                              protected void validateSalesTeamMember(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param SalesTeamMember
                              */
                              public void setSalesTeamMember(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateSalesTeamMember(param);

                               localSalesTeamMemberTracker = param != null;
                                      
                                      this.localSalesTeamMember=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addSalesTeamMember(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localSalesTeamMember == null){
                                   localSalesTeamMember = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localSalesTeamMemberTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localSalesTeamMember);
                               list.add(param);
                               this.localSalesTeamMember =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for SalesTeamRole
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localSalesTeamRole ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSalesTeamRoleTracker = false ;

                           public boolean isSalesTeamRoleSpecified(){
                               return localSalesTeamRoleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getSalesTeamRole(){
                               return localSalesTeamRole;
                           }

                           
                        


                               
                              /**
                               * validate the array for SalesTeamRole
                               */
                              protected void validateSalesTeamRole(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param SalesTeamRole
                              */
                              public void setSalesTeamRole(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateSalesTeamRole(param);

                               localSalesTeamRoleTracker = param != null;
                                      
                                      this.localSalesTeamRole=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addSalesTeamRole(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localSalesTeamRole == null){
                                   localSalesTeamRole = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localSalesTeamRoleTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localSalesTeamRole);
                               list.add(param);
                               this.localSalesTeamRole =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for Subsidiary
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        


                               
                              /**
                               * validate the array for Subsidiary
                               */
                              protected void validateSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Subsidiary
                              */
                              public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateSubsidiary(param);

                               localSubsidiaryTracker = param != null;
                                      
                                      this.localSubsidiary=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localSubsidiary == null){
                                   localSubsidiary = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localSubsidiaryTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localSubsidiary);
                               list.add(param);
                               this.localSubsidiary =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for TaxPeriod
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localTaxPeriod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTaxPeriodTracker = false ;

                           public boolean isTaxPeriodSpecified(){
                               return localTaxPeriodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getTaxPeriod(){
                               return localTaxPeriod;
                           }

                           
                        


                               
                              /**
                               * validate the array for TaxPeriod
                               */
                              protected void validateTaxPeriod(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param TaxPeriod
                              */
                              public void setTaxPeriod(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateTaxPeriod(param);

                               localTaxPeriodTracker = param != null;
                                      
                                      this.localTaxPeriod=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addTaxPeriod(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localTaxPeriod == null){
                                   localTaxPeriod = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localTaxPeriodTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localTaxPeriod);
                               list.add(param);
                               this.localTaxPeriod =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for Title
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTitleTracker = false ;

                           public boolean isTitleSpecified(){
                               return localTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getTitle(){
                               return localTitle;
                           }

                           
                        


                               
                              /**
                               * validate the array for Title
                               */
                              protected void validateTitle(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Title
                              */
                              public void setTitle(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateTitle(param);

                               localTitleTracker = param != null;
                                      
                                      this.localTitle=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addTitle(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localTitle == null){
                                   localTitle = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localTitleTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localTitle);
                               list.add(param);
                               this.localTitle =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for Total
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localTotal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTotalTracker = false ;

                           public boolean isTotalSpecified(){
                               return localTotalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getTotal(){
                               return localTotal;
                           }

                           
                        


                               
                              /**
                               * validate the array for Total
                               */
                              protected void validateTotal(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param Total
                              */
                              public void setTotal(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateTotal(param);

                               localTotalTracker = param != null;
                                      
                                      this.localTotal=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addTotal(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localTotal == null){
                                   localTotal = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localTotalTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localTotal);
                               list.add(param);
                               this.localTotal =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for TranCostEstimate
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localTranCostEstimate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranCostEstimateTracker = false ;

                           public boolean isTranCostEstimateSpecified(){
                               return localTranCostEstimateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getTranCostEstimate(){
                               return localTranCostEstimate;
                           }

                           
                        


                               
                              /**
                               * validate the array for TranCostEstimate
                               */
                              protected void validateTranCostEstimate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param TranCostEstimate
                              */
                              public void setTranCostEstimate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateTranCostEstimate(param);

                               localTranCostEstimateTracker = param != null;
                                      
                                      this.localTranCostEstimate=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addTranCostEstimate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localTranCostEstimate == null){
                                   localTranCostEstimate = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localTranCostEstimateTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localTranCostEstimate);
                               list.add(param);
                               this.localTranCostEstimate =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for TranDate
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] localTranDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranDateTracker = false ;

                           public boolean isTranDateSpecified(){
                               return localTranDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] getTranDate(){
                               return localTranDate;
                           }

                           
                        


                               
                              /**
                               * validate the array for TranDate
                               */
                              protected void validateTranDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param TranDate
                              */
                              public void setTranDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[] param){
                              
                                   validateTranDate(param);

                               localTranDateTracker = param != null;
                                      
                                      this.localTranDate=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField
                             */
                             public void addTranDate(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField param){
                                   if (localTranDate == null){
                                   localTranDate = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[]{};
                                   }

                            
                                 //update the setting tracker
                                localTranDateTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localTranDate);
                               list.add(param);
                               this.localTranDate =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[list.size()]);

                             }
                             

                        /**
                        * field for TranEstGrossProfit
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localTranEstGrossProfit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranEstGrossProfitTracker = false ;

                           public boolean isTranEstGrossProfitSpecified(){
                               return localTranEstGrossProfitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getTranEstGrossProfit(){
                               return localTranEstGrossProfit;
                           }

                           
                        


                               
                              /**
                               * validate the array for TranEstGrossProfit
                               */
                              protected void validateTranEstGrossProfit(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param TranEstGrossProfit
                              */
                              public void setTranEstGrossProfit(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateTranEstGrossProfit(param);

                               localTranEstGrossProfitTracker = param != null;
                                      
                                      this.localTranEstGrossProfit=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addTranEstGrossProfit(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localTranEstGrossProfit == null){
                                   localTranEstGrossProfit = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localTranEstGrossProfitTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localTranEstGrossProfit);
                               list.add(param);
                               this.localTranEstGrossProfit =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for TranEstGrossProfitPct
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localTranEstGrossProfitPct ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranEstGrossProfitPctTracker = false ;

                           public boolean isTranEstGrossProfitPctSpecified(){
                               return localTranEstGrossProfitPctTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getTranEstGrossProfitPct(){
                               return localTranEstGrossProfitPct;
                           }

                           
                        


                               
                              /**
                               * validate the array for TranEstGrossProfitPct
                               */
                              protected void validateTranEstGrossProfitPct(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param TranEstGrossProfitPct
                              */
                              public void setTranEstGrossProfitPct(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateTranEstGrossProfitPct(param);

                               localTranEstGrossProfitPctTracker = param != null;
                                      
                                      this.localTranEstGrossProfitPct=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addTranEstGrossProfitPct(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localTranEstGrossProfitPct == null){
                                   localTranEstGrossProfitPct = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localTranEstGrossProfitPctTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localTranEstGrossProfitPct);
                               list.add(param);
                               this.localTranEstGrossProfitPct =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for TranFxEstGrossProfit
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localTranFxEstGrossProfit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranFxEstGrossProfitTracker = false ;

                           public boolean isTranFxEstGrossProfitSpecified(){
                               return localTranFxEstGrossProfitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getTranFxEstGrossProfit(){
                               return localTranFxEstGrossProfit;
                           }

                           
                        


                               
                              /**
                               * validate the array for TranFxEstGrossProfit
                               */
                              protected void validateTranFxEstGrossProfit(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param TranFxEstGrossProfit
                              */
                              public void setTranFxEstGrossProfit(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateTranFxEstGrossProfit(param);

                               localTranFxEstGrossProfitTracker = param != null;
                                      
                                      this.localTranFxEstGrossProfit=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addTranFxEstGrossProfit(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localTranFxEstGrossProfit == null){
                                   localTranFxEstGrossProfit = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localTranFxEstGrossProfitTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localTranFxEstGrossProfit);
                               list.add(param);
                               this.localTranFxEstGrossProfit =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for TranId
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] localTranId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranIdTracker = false ;

                           public boolean isTranIdSpecified(){
                               return localTranIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] getTranId(){
                               return localTranId;
                           }

                           
                        


                               
                              /**
                               * validate the array for TranId
                               */
                              protected void validateTranId(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param TranId
                              */
                              public void setTranId(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[] param){
                              
                                   validateTranId(param);

                               localTranIdTracker = param != null;
                                      
                                      this.localTranId=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField
                             */
                             public void addTranId(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField param){
                                   if (localTranId == null){
                                   localTranId = new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[]{};
                                   }

                            
                                 //update the setting tracker
                                localTranIdTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localTranId);
                               list.add(param);
                               this.localTranId =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[list.size()]);

                             }
                             

                        /**
                        * field for WeightedTotal
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] localWeightedTotal ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWeightedTotalTracker = false ;

                           public boolean isWeightedTotalSpecified(){
                               return localWeightedTotalTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] getWeightedTotal(){
                               return localWeightedTotal;
                           }

                           
                        


                               
                              /**
                               * validate the array for WeightedTotal
                               */
                              protected void validateWeightedTotal(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param WeightedTotal
                              */
                              public void setWeightedTotal(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[] param){
                              
                                   validateWeightedTotal(param);

                               localWeightedTotalTracker = param != null;
                                      
                                      this.localWeightedTotal=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField
                             */
                             public void addWeightedTotal(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField param){
                                   if (localWeightedTotal == null){
                                   localWeightedTotal = new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[]{};
                                   }

                            
                                 //update the setting tracker
                                localWeightedTotalTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localWeightedTotal);
                               list.add(param);
                               this.localWeightedTotal =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[list.size()]);

                             }
                             

                        /**
                        * field for WinLossReason
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localWinLossReason ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWinLossReasonTracker = false ;

                           public boolean isWinLossReasonSpecified(){
                               return localWinLossReasonTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getWinLossReason(){
                               return localWinLossReason;
                           }

                           
                        


                               
                              /**
                               * validate the array for WinLossReason
                               */
                              protected void validateWinLossReason(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param WinLossReason
                              */
                              public void setWinLossReason(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateWinLossReason(param);

                               localWinLossReasonTracker = param != null;
                                      
                                      this.localWinLossReason=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addWinLossReason(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localWinLossReason == null){
                                   localWinLossReason = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localWinLossReasonTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localWinLossReason);
                               list.add(param);
                               this.localWinLossReason =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for WonBy
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] localWonBy ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWonByTracker = false ;

                           public boolean isWonBySpecified(){
                               return localWonByTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] getWonBy(){
                               return localWonBy;
                           }

                           
                        


                               
                              /**
                               * validate the array for WonBy
                               */
                              protected void validateWonBy(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param WonBy
                              */
                              public void setWonBy(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[] param){
                              
                                   validateWonBy(param);

                               localWonByTracker = param != null;
                                      
                                      this.localWonBy=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField
                             */
                             public void addWonBy(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField param){
                                   if (localWonBy == null){
                                   localWonBy = new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[]{};
                                   }

                            
                                 //update the setting tracker
                                localWonByTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localWonBy);
                               list.add(param);
                               this.localWonBy =
                             (com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])list.toArray(
                            new com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[list.size()]);

                             }
                             

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":OpportunitySearchRowBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "OpportunitySearchRowBasic",
                           xmlWriter);
                   }

                if (localActionItemTracker){
                                       if (localActionItem!=null){
                                            for (int i = 0;i < localActionItem.length;i++){
                                                if (localActionItem[i] != null){
                                                 localActionItem[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","actionItem"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("actionItem cannot be null!!");
                                        
                                    }
                                 } if (localAvailableOfflineTracker){
                                       if (localAvailableOffline!=null){
                                            for (int i = 0;i < localAvailableOffline.length;i++){
                                                if (localAvailableOffline[i] != null){
                                                 localAvailableOffline[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","availableOffline"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("availableOffline cannot be null!!");
                                        
                                    }
                                 } if (localBuyingReasonTracker){
                                       if (localBuyingReason!=null){
                                            for (int i = 0;i < localBuyingReason.length;i++){
                                                if (localBuyingReason[i] != null){
                                                 localBuyingReason[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buyingReason"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("buyingReason cannot be null!!");
                                        
                                    }
                                 } if (localBuyingTimeFrameTracker){
                                       if (localBuyingTimeFrame!=null){
                                            for (int i = 0;i < localBuyingTimeFrame.length;i++){
                                                if (localBuyingTimeFrame[i] != null){
                                                 localBuyingTimeFrame[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buyingTimeFrame"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("buyingTimeFrame cannot be null!!");
                                        
                                    }
                                 } if (local_classTracker){
                                       if (local_class!=null){
                                            for (int i = 0;i < local_class.length;i++){
                                                if (local_class[i] != null){
                                                 local_class[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","class"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                        
                                    }
                                 } if (localCloseDateTracker){
                                       if (localCloseDate!=null){
                                            for (int i = 0;i < localCloseDate.length;i++){
                                                if (localCloseDate[i] != null){
                                                 localCloseDate[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","closeDate"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("closeDate cannot be null!!");
                                        
                                    }
                                 } if (localCompetitorTracker){
                                       if (localCompetitor!=null){
                                            for (int i = 0;i < localCompetitor.length;i++){
                                                if (localCompetitor[i] != null){
                                                 localCompetitor[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","competitor"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("competitor cannot be null!!");
                                        
                                    }
                                 } if (localContributionTracker){
                                       if (localContribution!=null){
                                            for (int i = 0;i < localContribution.length;i++){
                                                if (localContribution[i] != null){
                                                 localContribution[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","contribution"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("contribution cannot be null!!");
                                        
                                    }
                                 } if (localContributionPrimaryTracker){
                                       if (localContributionPrimary!=null){
                                            for (int i = 0;i < localContributionPrimary.length;i++){
                                                if (localContributionPrimary[i] != null){
                                                 localContributionPrimary[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","contributionPrimary"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("contributionPrimary cannot be null!!");
                                        
                                    }
                                 } if (localCurrencyTracker){
                                       if (localCurrency!=null){
                                            for (int i = 0;i < localCurrency.length;i++){
                                                if (localCurrency[i] != null){
                                                 localCurrency[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                        
                                    }
                                 } if (localCustTypeTracker){
                                       if (localCustType!=null){
                                            for (int i = 0;i < localCustType.length;i++){
                                                if (localCustType[i] != null){
                                                 localCustType[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","custType"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("custType cannot be null!!");
                                        
                                    }
                                 } if (localDateCreatedTracker){
                                       if (localDateCreated!=null){
                                            for (int i = 0;i < localDateCreated.length;i++){
                                                if (localDateCreated[i] != null){
                                                 localDateCreated[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                        
                                    }
                                 } if (localDaysOpenTracker){
                                       if (localDaysOpen!=null){
                                            for (int i = 0;i < localDaysOpen.length;i++){
                                                if (localDaysOpen[i] != null){
                                                 localDaysOpen[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","daysOpen"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("daysOpen cannot be null!!");
                                        
                                    }
                                 } if (localDaysToCloseTracker){
                                       if (localDaysToClose!=null){
                                            for (int i = 0;i < localDaysToClose.length;i++){
                                                if (localDaysToClose[i] != null){
                                                 localDaysToClose[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","daysToClose"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("daysToClose cannot be null!!");
                                        
                                    }
                                 } if (localDecisionMakerTracker){
                                       if (localDecisionMaker!=null){
                                            for (int i = 0;i < localDecisionMaker.length;i++){
                                                if (localDecisionMaker[i] != null){
                                                 localDecisionMaker[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","decisionMaker"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("decisionMaker cannot be null!!");
                                        
                                    }
                                 } if (localDepartmentTracker){
                                       if (localDepartment!=null){
                                            for (int i = 0;i < localDepartment.length;i++){
                                                if (localDepartment[i] != null){
                                                 localDepartment[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","department"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                        
                                    }
                                 } if (localStatusTracker){
                                       if (localStatus!=null){
                                            for (int i = 0;i < localStatus.length;i++){
                                                if (localStatus[i] != null){
                                                 localStatus[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","status"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                        
                                    }
                                 } if (localEntityTracker){
                                       if (localEntity!=null){
                                            for (int i = 0;i < localEntity.length;i++){
                                                if (localEntity[i] != null){
                                                 localEntity[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entity"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("entity cannot be null!!");
                                        
                                    }
                                 } if (localEntityStatusTracker){
                                       if (localEntityStatus!=null){
                                            for (int i = 0;i < localEntityStatus.length;i++){
                                                if (localEntityStatus[i] != null){
                                                 localEntityStatus[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entityStatus"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("entityStatus cannot be null!!");
                                        
                                    }
                                 } if (localEstimatedBudgetTracker){
                                       if (localEstimatedBudget!=null){
                                            for (int i = 0;i < localEstimatedBudget.length;i++){
                                                if (localEstimatedBudget[i] != null){
                                                 localEstimatedBudget[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedBudget"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("estimatedBudget cannot be null!!");
                                        
                                    }
                                 } if (localExpectedCloseDateTracker){
                                       if (localExpectedCloseDate!=null){
                                            for (int i = 0;i < localExpectedCloseDate.length;i++){
                                                if (localExpectedCloseDate[i] != null){
                                                 localExpectedCloseDate[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","expectedCloseDate"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("expectedCloseDate cannot be null!!");
                                        
                                    }
                                 } if (localExternalIdTracker){
                                       if (localExternalId!=null){
                                            for (int i = 0;i < localExternalId.length;i++){
                                                if (localExternalId[i] != null){
                                                 localExternalId[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                        
                                    }
                                 } if (localForecastTypeTracker){
                                       if (localForecastType!=null){
                                            for (int i = 0;i < localForecastType.length;i++){
                                                if (localForecastType[i] != null){
                                                 localForecastType[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","forecastType"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("forecastType cannot be null!!");
                                        
                                    }
                                 } if (localForeignProjectedAmountTracker){
                                       if (localForeignProjectedAmount!=null){
                                            for (int i = 0;i < localForeignProjectedAmount.length;i++){
                                                if (localForeignProjectedAmount[i] != null){
                                                 localForeignProjectedAmount[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","foreignProjectedAmount"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("foreignProjectedAmount cannot be null!!");
                                        
                                    }
                                 } if (localForeignRangeHighTracker){
                                       if (localForeignRangeHigh!=null){
                                            for (int i = 0;i < localForeignRangeHigh.length;i++){
                                                if (localForeignRangeHigh[i] != null){
                                                 localForeignRangeHigh[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","foreignRangeHigh"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("foreignRangeHigh cannot be null!!");
                                        
                                    }
                                 } if (localForeignRangeLowTracker){
                                       if (localForeignRangeLow!=null){
                                            for (int i = 0;i < localForeignRangeLow.length;i++){
                                                if (localForeignRangeLow[i] != null){
                                                 localForeignRangeLow[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","foreignRangeLow"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("foreignRangeLow cannot be null!!");
                                        
                                    }
                                 } if (localFxTranCostEstimateTracker){
                                       if (localFxTranCostEstimate!=null){
                                            for (int i = 0;i < localFxTranCostEstimate.length;i++){
                                                if (localFxTranCostEstimate[i] != null){
                                                 localFxTranCostEstimate[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxTranCostEstimate"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("fxTranCostEstimate cannot be null!!");
                                        
                                    }
                                 } if (localInternalIdTracker){
                                       if (localInternalId!=null){
                                            for (int i = 0;i < localInternalId.length;i++){
                                                if (localInternalId[i] != null){
                                                 localInternalId[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                        
                                    }
                                 } if (localIsBudgetApprovedTracker){
                                       if (localIsBudgetApproved!=null){
                                            for (int i = 0;i < localIsBudgetApproved.length;i++){
                                                if (localIsBudgetApproved[i] != null){
                                                 localIsBudgetApproved[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isBudgetApproved"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("isBudgetApproved cannot be null!!");
                                        
                                    }
                                 } if (localLastModifiedDateTracker){
                                       if (localLastModifiedDate!=null){
                                            for (int i = 0;i < localLastModifiedDate.length;i++){
                                                if (localLastModifiedDate[i] != null){
                                                 localLastModifiedDate[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                        
                                    }
                                 } if (localLeadSourceTracker){
                                       if (localLeadSource!=null){
                                            for (int i = 0;i < localLeadSource.length;i++){
                                                if (localLeadSource[i] != null){
                                                 localLeadSource[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","leadSource"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("leadSource cannot be null!!");
                                        
                                    }
                                 } if (localLocationTracker){
                                       if (localLocation!=null){
                                            for (int i = 0;i < localLocation.length;i++){
                                                if (localLocation[i] != null){
                                                 localLocation[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","location"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                        
                                    }
                                 } if (localMemoTracker){
                                       if (localMemo!=null){
                                            for (int i = 0;i < localMemo.length;i++){
                                                if (localMemo[i] != null){
                                                 localMemo[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","memo"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                        
                                    }
                                 } if (localPartnerTracker){
                                       if (localPartner!=null){
                                            for (int i = 0;i < localPartner.length;i++){
                                                if (localPartner[i] != null){
                                                 localPartner[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partner"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("partner cannot be null!!");
                                        
                                    }
                                 } if (localPartnerContributionTracker){
                                       if (localPartnerContribution!=null){
                                            for (int i = 0;i < localPartnerContribution.length;i++){
                                                if (localPartnerContribution[i] != null){
                                                 localPartnerContribution[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerContribution"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("partnerContribution cannot be null!!");
                                        
                                    }
                                 } if (localPartnerRoleTracker){
                                       if (localPartnerRole!=null){
                                            for (int i = 0;i < localPartnerRole.length;i++){
                                                if (localPartnerRole[i] != null){
                                                 localPartnerRole[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerRole"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("partnerRole cannot be null!!");
                                        
                                    }
                                 } if (localPartnerTeamMemberTracker){
                                       if (localPartnerTeamMember!=null){
                                            for (int i = 0;i < localPartnerTeamMember.length;i++){
                                                if (localPartnerTeamMember[i] != null){
                                                 localPartnerTeamMember[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerTeamMember"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("partnerTeamMember cannot be null!!");
                                        
                                    }
                                 } if (localPeriodTracker){
                                       if (localPeriod!=null){
                                            for (int i = 0;i < localPeriod.length;i++){
                                                if (localPeriod[i] != null){
                                                 localPeriod[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","period"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("period cannot be null!!");
                                        
                                    }
                                 } if (localProbabilityTracker){
                                       if (localProbability!=null){
                                            for (int i = 0;i < localProbability.length;i++){
                                                if (localProbability[i] != null){
                                                 localProbability[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","probability"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("probability cannot be null!!");
                                        
                                    }
                                 } if (localProjAltSalesAmtTracker){
                                       if (localProjAltSalesAmt!=null){
                                            for (int i = 0;i < localProjAltSalesAmt.length;i++){
                                                if (localProjAltSalesAmt[i] != null){
                                                 localProjAltSalesAmt[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","projAltSalesAmt"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("projAltSalesAmt cannot be null!!");
                                        
                                    }
                                 } if (localProjectedTotalTracker){
                                       if (localProjectedTotal!=null){
                                            for (int i = 0;i < localProjectedTotal.length;i++){
                                                if (localProjectedTotal[i] != null){
                                                 localProjectedTotal[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","projectedTotal"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("projectedTotal cannot be null!!");
                                        
                                    }
                                 } if (localRangeHighTracker){
                                       if (localRangeHigh!=null){
                                            for (int i = 0;i < localRangeHigh.length;i++){
                                                if (localRangeHigh[i] != null){
                                                 localRangeHigh[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeHigh"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("rangeHigh cannot be null!!");
                                        
                                    }
                                 } if (localRangeHighAltTracker){
                                       if (localRangeHighAlt!=null){
                                            for (int i = 0;i < localRangeHighAlt.length;i++){
                                                if (localRangeHighAlt[i] != null){
                                                 localRangeHighAlt[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeHighAlt"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("rangeHighAlt cannot be null!!");
                                        
                                    }
                                 } if (localRangeLowTracker){
                                       if (localRangeLow!=null){
                                            for (int i = 0;i < localRangeLow.length;i++){
                                                if (localRangeLow[i] != null){
                                                 localRangeLow[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeLow"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("rangeLow cannot be null!!");
                                        
                                    }
                                 } if (localRangeLowAltTracker){
                                       if (localRangeLowAlt!=null){
                                            for (int i = 0;i < localRangeLowAlt.length;i++){
                                                if (localRangeLowAlt[i] != null){
                                                 localRangeLowAlt[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeLowAlt"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("rangeLowAlt cannot be null!!");
                                        
                                    }
                                 } if (localSalesReadinessTracker){
                                       if (localSalesReadiness!=null){
                                            for (int i = 0;i < localSalesReadiness.length;i++){
                                                if (localSalesReadiness[i] != null){
                                                 localSalesReadiness[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesReadiness"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("salesReadiness cannot be null!!");
                                        
                                    }
                                 } if (localSalesRepTracker){
                                       if (localSalesRep!=null){
                                            for (int i = 0;i < localSalesRep.length;i++){
                                                if (localSalesRep[i] != null){
                                                 localSalesRep[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesRep"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("salesRep cannot be null!!");
                                        
                                    }
                                 } if (localSalesTeamMemberTracker){
                                       if (localSalesTeamMember!=null){
                                            for (int i = 0;i < localSalesTeamMember.length;i++){
                                                if (localSalesTeamMember[i] != null){
                                                 localSalesTeamMember[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesTeamMember"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("salesTeamMember cannot be null!!");
                                        
                                    }
                                 } if (localSalesTeamRoleTracker){
                                       if (localSalesTeamRole!=null){
                                            for (int i = 0;i < localSalesTeamRole.length;i++){
                                                if (localSalesTeamRole[i] != null){
                                                 localSalesTeamRole[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesTeamRole"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("salesTeamRole cannot be null!!");
                                        
                                    }
                                 } if (localSubsidiaryTracker){
                                       if (localSubsidiary!=null){
                                            for (int i = 0;i < localSubsidiary.length;i++){
                                                if (localSubsidiary[i] != null){
                                                 localSubsidiary[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                        
                                    }
                                 } if (localTaxPeriodTracker){
                                       if (localTaxPeriod!=null){
                                            for (int i = 0;i < localTaxPeriod.length;i++){
                                                if (localTaxPeriod[i] != null){
                                                 localTaxPeriod[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","taxPeriod"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("taxPeriod cannot be null!!");
                                        
                                    }
                                 } if (localTitleTracker){
                                       if (localTitle!=null){
                                            for (int i = 0;i < localTitle.length;i++){
                                                if (localTitle[i] != null){
                                                 localTitle[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","title"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                        
                                    }
                                 } if (localTotalTracker){
                                       if (localTotal!=null){
                                            for (int i = 0;i < localTotal.length;i++){
                                                if (localTotal[i] != null){
                                                 localTotal[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","total"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("total cannot be null!!");
                                        
                                    }
                                 } if (localTranCostEstimateTracker){
                                       if (localTranCostEstimate!=null){
                                            for (int i = 0;i < localTranCostEstimate.length;i++){
                                                if (localTranCostEstimate[i] != null){
                                                 localTranCostEstimate[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranCostEstimate"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("tranCostEstimate cannot be null!!");
                                        
                                    }
                                 } if (localTranDateTracker){
                                       if (localTranDate!=null){
                                            for (int i = 0;i < localTranDate.length;i++){
                                                if (localTranDate[i] != null){
                                                 localTranDate[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranDate"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("tranDate cannot be null!!");
                                        
                                    }
                                 } if (localTranEstGrossProfitTracker){
                                       if (localTranEstGrossProfit!=null){
                                            for (int i = 0;i < localTranEstGrossProfit.length;i++){
                                                if (localTranEstGrossProfit[i] != null){
                                                 localTranEstGrossProfit[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranEstGrossProfit"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("tranEstGrossProfit cannot be null!!");
                                        
                                    }
                                 } if (localTranEstGrossProfitPctTracker){
                                       if (localTranEstGrossProfitPct!=null){
                                            for (int i = 0;i < localTranEstGrossProfitPct.length;i++){
                                                if (localTranEstGrossProfitPct[i] != null){
                                                 localTranEstGrossProfitPct[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranEstGrossProfitPct"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("tranEstGrossProfitPct cannot be null!!");
                                        
                                    }
                                 } if (localTranFxEstGrossProfitTracker){
                                       if (localTranFxEstGrossProfit!=null){
                                            for (int i = 0;i < localTranFxEstGrossProfit.length;i++){
                                                if (localTranFxEstGrossProfit[i] != null){
                                                 localTranFxEstGrossProfit[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranFxEstGrossProfit"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("tranFxEstGrossProfit cannot be null!!");
                                        
                                    }
                                 } if (localTranIdTracker){
                                       if (localTranId!=null){
                                            for (int i = 0;i < localTranId.length;i++){
                                                if (localTranId[i] != null){
                                                 localTranId[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranId"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("tranId cannot be null!!");
                                        
                                    }
                                 } if (localWeightedTotalTracker){
                                       if (localWeightedTotal!=null){
                                            for (int i = 0;i < localWeightedTotal.length;i++){
                                                if (localWeightedTotal[i] != null){
                                                 localWeightedTotal[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","weightedTotal"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("weightedTotal cannot be null!!");
                                        
                                    }
                                 } if (localWinLossReasonTracker){
                                       if (localWinLossReason!=null){
                                            for (int i = 0;i < localWinLossReason.length;i++){
                                                if (localWinLossReason[i] != null){
                                                 localWinLossReason[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","winLossReason"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("winLossReason cannot be null!!");
                                        
                                    }
                                 } if (localWonByTracker){
                                       if (localWonBy!=null){
                                            for (int i = 0;i < localWonBy.length;i++){
                                                if (localWonBy[i] != null){
                                                 localWonBy[i].serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","wonBy"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("wonBy cannot be null!!");
                                        
                                    }
                                 } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","OpportunitySearchRowBasic"));
                 if (localActionItemTracker){
                             if (localActionItem!=null) {
                                 for (int i = 0;i < localActionItem.length;i++){

                                    if (localActionItem[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "actionItem"));
                                         elementList.add(localActionItem[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("actionItem cannot be null!!");
                                    
                             }

                        } if (localAvailableOfflineTracker){
                             if (localAvailableOffline!=null) {
                                 for (int i = 0;i < localAvailableOffline.length;i++){

                                    if (localAvailableOffline[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "availableOffline"));
                                         elementList.add(localAvailableOffline[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("availableOffline cannot be null!!");
                                    
                             }

                        } if (localBuyingReasonTracker){
                             if (localBuyingReason!=null) {
                                 for (int i = 0;i < localBuyingReason.length;i++){

                                    if (localBuyingReason[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "buyingReason"));
                                         elementList.add(localBuyingReason[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("buyingReason cannot be null!!");
                                    
                             }

                        } if (localBuyingTimeFrameTracker){
                             if (localBuyingTimeFrame!=null) {
                                 for (int i = 0;i < localBuyingTimeFrame.length;i++){

                                    if (localBuyingTimeFrame[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "buyingTimeFrame"));
                                         elementList.add(localBuyingTimeFrame[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("buyingTimeFrame cannot be null!!");
                                    
                             }

                        } if (local_classTracker){
                             if (local_class!=null) {
                                 for (int i = 0;i < local_class.length;i++){

                                    if (local_class[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "class"));
                                         elementList.add(local_class[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("class cannot be null!!");
                                    
                             }

                        } if (localCloseDateTracker){
                             if (localCloseDate!=null) {
                                 for (int i = 0;i < localCloseDate.length;i++){

                                    if (localCloseDate[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "closeDate"));
                                         elementList.add(localCloseDate[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("closeDate cannot be null!!");
                                    
                             }

                        } if (localCompetitorTracker){
                             if (localCompetitor!=null) {
                                 for (int i = 0;i < localCompetitor.length;i++){

                                    if (localCompetitor[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "competitor"));
                                         elementList.add(localCompetitor[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("competitor cannot be null!!");
                                    
                             }

                        } if (localContributionTracker){
                             if (localContribution!=null) {
                                 for (int i = 0;i < localContribution.length;i++){

                                    if (localContribution[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "contribution"));
                                         elementList.add(localContribution[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("contribution cannot be null!!");
                                    
                             }

                        } if (localContributionPrimaryTracker){
                             if (localContributionPrimary!=null) {
                                 for (int i = 0;i < localContributionPrimary.length;i++){

                                    if (localContributionPrimary[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "contributionPrimary"));
                                         elementList.add(localContributionPrimary[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("contributionPrimary cannot be null!!");
                                    
                             }

                        } if (localCurrencyTracker){
                             if (localCurrency!=null) {
                                 for (int i = 0;i < localCurrency.length;i++){

                                    if (localCurrency[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "currency"));
                                         elementList.add(localCurrency[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("currency cannot be null!!");
                                    
                             }

                        } if (localCustTypeTracker){
                             if (localCustType!=null) {
                                 for (int i = 0;i < localCustType.length;i++){

                                    if (localCustType[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "custType"));
                                         elementList.add(localCustType[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("custType cannot be null!!");
                                    
                             }

                        } if (localDateCreatedTracker){
                             if (localDateCreated!=null) {
                                 for (int i = 0;i < localDateCreated.length;i++){

                                    if (localDateCreated[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "dateCreated"));
                                         elementList.add(localDateCreated[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("dateCreated cannot be null!!");
                                    
                             }

                        } if (localDaysOpenTracker){
                             if (localDaysOpen!=null) {
                                 for (int i = 0;i < localDaysOpen.length;i++){

                                    if (localDaysOpen[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "daysOpen"));
                                         elementList.add(localDaysOpen[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("daysOpen cannot be null!!");
                                    
                             }

                        } if (localDaysToCloseTracker){
                             if (localDaysToClose!=null) {
                                 for (int i = 0;i < localDaysToClose.length;i++){

                                    if (localDaysToClose[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "daysToClose"));
                                         elementList.add(localDaysToClose[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("daysToClose cannot be null!!");
                                    
                             }

                        } if (localDecisionMakerTracker){
                             if (localDecisionMaker!=null) {
                                 for (int i = 0;i < localDecisionMaker.length;i++){

                                    if (localDecisionMaker[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "decisionMaker"));
                                         elementList.add(localDecisionMaker[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("decisionMaker cannot be null!!");
                                    
                             }

                        } if (localDepartmentTracker){
                             if (localDepartment!=null) {
                                 for (int i = 0;i < localDepartment.length;i++){

                                    if (localDepartment[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "department"));
                                         elementList.add(localDepartment[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("department cannot be null!!");
                                    
                             }

                        } if (localStatusTracker){
                             if (localStatus!=null) {
                                 for (int i = 0;i < localStatus.length;i++){

                                    if (localStatus[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "status"));
                                         elementList.add(localStatus[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                    
                             }

                        } if (localEntityTracker){
                             if (localEntity!=null) {
                                 for (int i = 0;i < localEntity.length;i++){

                                    if (localEntity[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "entity"));
                                         elementList.add(localEntity[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("entity cannot be null!!");
                                    
                             }

                        } if (localEntityStatusTracker){
                             if (localEntityStatus!=null) {
                                 for (int i = 0;i < localEntityStatus.length;i++){

                                    if (localEntityStatus[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "entityStatus"));
                                         elementList.add(localEntityStatus[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("entityStatus cannot be null!!");
                                    
                             }

                        } if (localEstimatedBudgetTracker){
                             if (localEstimatedBudget!=null) {
                                 for (int i = 0;i < localEstimatedBudget.length;i++){

                                    if (localEstimatedBudget[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "estimatedBudget"));
                                         elementList.add(localEstimatedBudget[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("estimatedBudget cannot be null!!");
                                    
                             }

                        } if (localExpectedCloseDateTracker){
                             if (localExpectedCloseDate!=null) {
                                 for (int i = 0;i < localExpectedCloseDate.length;i++){

                                    if (localExpectedCloseDate[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "expectedCloseDate"));
                                         elementList.add(localExpectedCloseDate[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("expectedCloseDate cannot be null!!");
                                    
                             }

                        } if (localExternalIdTracker){
                             if (localExternalId!=null) {
                                 for (int i = 0;i < localExternalId.length;i++){

                                    if (localExternalId[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "externalId"));
                                         elementList.add(localExternalId[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                    
                             }

                        } if (localForecastTypeTracker){
                             if (localForecastType!=null) {
                                 for (int i = 0;i < localForecastType.length;i++){

                                    if (localForecastType[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "forecastType"));
                                         elementList.add(localForecastType[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("forecastType cannot be null!!");
                                    
                             }

                        } if (localForeignProjectedAmountTracker){
                             if (localForeignProjectedAmount!=null) {
                                 for (int i = 0;i < localForeignProjectedAmount.length;i++){

                                    if (localForeignProjectedAmount[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "foreignProjectedAmount"));
                                         elementList.add(localForeignProjectedAmount[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("foreignProjectedAmount cannot be null!!");
                                    
                             }

                        } if (localForeignRangeHighTracker){
                             if (localForeignRangeHigh!=null) {
                                 for (int i = 0;i < localForeignRangeHigh.length;i++){

                                    if (localForeignRangeHigh[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "foreignRangeHigh"));
                                         elementList.add(localForeignRangeHigh[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("foreignRangeHigh cannot be null!!");
                                    
                             }

                        } if (localForeignRangeLowTracker){
                             if (localForeignRangeLow!=null) {
                                 for (int i = 0;i < localForeignRangeLow.length;i++){

                                    if (localForeignRangeLow[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "foreignRangeLow"));
                                         elementList.add(localForeignRangeLow[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("foreignRangeLow cannot be null!!");
                                    
                             }

                        } if (localFxTranCostEstimateTracker){
                             if (localFxTranCostEstimate!=null) {
                                 for (int i = 0;i < localFxTranCostEstimate.length;i++){

                                    if (localFxTranCostEstimate[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "fxTranCostEstimate"));
                                         elementList.add(localFxTranCostEstimate[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("fxTranCostEstimate cannot be null!!");
                                    
                             }

                        } if (localInternalIdTracker){
                             if (localInternalId!=null) {
                                 for (int i = 0;i < localInternalId.length;i++){

                                    if (localInternalId[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "internalId"));
                                         elementList.add(localInternalId[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    
                             }

                        } if (localIsBudgetApprovedTracker){
                             if (localIsBudgetApproved!=null) {
                                 for (int i = 0;i < localIsBudgetApproved.length;i++){

                                    if (localIsBudgetApproved[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "isBudgetApproved"));
                                         elementList.add(localIsBudgetApproved[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("isBudgetApproved cannot be null!!");
                                    
                             }

                        } if (localLastModifiedDateTracker){
                             if (localLastModifiedDate!=null) {
                                 for (int i = 0;i < localLastModifiedDate.length;i++){

                                    if (localLastModifiedDate[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "lastModifiedDate"));
                                         elementList.add(localLastModifiedDate[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                    
                             }

                        } if (localLeadSourceTracker){
                             if (localLeadSource!=null) {
                                 for (int i = 0;i < localLeadSource.length;i++){

                                    if (localLeadSource[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "leadSource"));
                                         elementList.add(localLeadSource[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("leadSource cannot be null!!");
                                    
                             }

                        } if (localLocationTracker){
                             if (localLocation!=null) {
                                 for (int i = 0;i < localLocation.length;i++){

                                    if (localLocation[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "location"));
                                         elementList.add(localLocation[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                    
                             }

                        } if (localMemoTracker){
                             if (localMemo!=null) {
                                 for (int i = 0;i < localMemo.length;i++){

                                    if (localMemo[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "memo"));
                                         elementList.add(localMemo[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("memo cannot be null!!");
                                    
                             }

                        } if (localPartnerTracker){
                             if (localPartner!=null) {
                                 for (int i = 0;i < localPartner.length;i++){

                                    if (localPartner[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "partner"));
                                         elementList.add(localPartner[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("partner cannot be null!!");
                                    
                             }

                        } if (localPartnerContributionTracker){
                             if (localPartnerContribution!=null) {
                                 for (int i = 0;i < localPartnerContribution.length;i++){

                                    if (localPartnerContribution[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "partnerContribution"));
                                         elementList.add(localPartnerContribution[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("partnerContribution cannot be null!!");
                                    
                             }

                        } if (localPartnerRoleTracker){
                             if (localPartnerRole!=null) {
                                 for (int i = 0;i < localPartnerRole.length;i++){

                                    if (localPartnerRole[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "partnerRole"));
                                         elementList.add(localPartnerRole[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("partnerRole cannot be null!!");
                                    
                             }

                        } if (localPartnerTeamMemberTracker){
                             if (localPartnerTeamMember!=null) {
                                 for (int i = 0;i < localPartnerTeamMember.length;i++){

                                    if (localPartnerTeamMember[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "partnerTeamMember"));
                                         elementList.add(localPartnerTeamMember[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("partnerTeamMember cannot be null!!");
                                    
                             }

                        } if (localPeriodTracker){
                             if (localPeriod!=null) {
                                 for (int i = 0;i < localPeriod.length;i++){

                                    if (localPeriod[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "period"));
                                         elementList.add(localPeriod[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("period cannot be null!!");
                                    
                             }

                        } if (localProbabilityTracker){
                             if (localProbability!=null) {
                                 for (int i = 0;i < localProbability.length;i++){

                                    if (localProbability[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "probability"));
                                         elementList.add(localProbability[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("probability cannot be null!!");
                                    
                             }

                        } if (localProjAltSalesAmtTracker){
                             if (localProjAltSalesAmt!=null) {
                                 for (int i = 0;i < localProjAltSalesAmt.length;i++){

                                    if (localProjAltSalesAmt[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "projAltSalesAmt"));
                                         elementList.add(localProjAltSalesAmt[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("projAltSalesAmt cannot be null!!");
                                    
                             }

                        } if (localProjectedTotalTracker){
                             if (localProjectedTotal!=null) {
                                 for (int i = 0;i < localProjectedTotal.length;i++){

                                    if (localProjectedTotal[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "projectedTotal"));
                                         elementList.add(localProjectedTotal[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("projectedTotal cannot be null!!");
                                    
                             }

                        } if (localRangeHighTracker){
                             if (localRangeHigh!=null) {
                                 for (int i = 0;i < localRangeHigh.length;i++){

                                    if (localRangeHigh[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "rangeHigh"));
                                         elementList.add(localRangeHigh[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("rangeHigh cannot be null!!");
                                    
                             }

                        } if (localRangeHighAltTracker){
                             if (localRangeHighAlt!=null) {
                                 for (int i = 0;i < localRangeHighAlt.length;i++){

                                    if (localRangeHighAlt[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "rangeHighAlt"));
                                         elementList.add(localRangeHighAlt[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("rangeHighAlt cannot be null!!");
                                    
                             }

                        } if (localRangeLowTracker){
                             if (localRangeLow!=null) {
                                 for (int i = 0;i < localRangeLow.length;i++){

                                    if (localRangeLow[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "rangeLow"));
                                         elementList.add(localRangeLow[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("rangeLow cannot be null!!");
                                    
                             }

                        } if (localRangeLowAltTracker){
                             if (localRangeLowAlt!=null) {
                                 for (int i = 0;i < localRangeLowAlt.length;i++){

                                    if (localRangeLowAlt[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "rangeLowAlt"));
                                         elementList.add(localRangeLowAlt[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("rangeLowAlt cannot be null!!");
                                    
                             }

                        } if (localSalesReadinessTracker){
                             if (localSalesReadiness!=null) {
                                 for (int i = 0;i < localSalesReadiness.length;i++){

                                    if (localSalesReadiness[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "salesReadiness"));
                                         elementList.add(localSalesReadiness[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("salesReadiness cannot be null!!");
                                    
                             }

                        } if (localSalesRepTracker){
                             if (localSalesRep!=null) {
                                 for (int i = 0;i < localSalesRep.length;i++){

                                    if (localSalesRep[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "salesRep"));
                                         elementList.add(localSalesRep[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("salesRep cannot be null!!");
                                    
                             }

                        } if (localSalesTeamMemberTracker){
                             if (localSalesTeamMember!=null) {
                                 for (int i = 0;i < localSalesTeamMember.length;i++){

                                    if (localSalesTeamMember[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "salesTeamMember"));
                                         elementList.add(localSalesTeamMember[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("salesTeamMember cannot be null!!");
                                    
                             }

                        } if (localSalesTeamRoleTracker){
                             if (localSalesTeamRole!=null) {
                                 for (int i = 0;i < localSalesTeamRole.length;i++){

                                    if (localSalesTeamRole[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "salesTeamRole"));
                                         elementList.add(localSalesTeamRole[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("salesTeamRole cannot be null!!");
                                    
                             }

                        } if (localSubsidiaryTracker){
                             if (localSubsidiary!=null) {
                                 for (int i = 0;i < localSubsidiary.length;i++){

                                    if (localSubsidiary[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "subsidiary"));
                                         elementList.add(localSubsidiary[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    
                             }

                        } if (localTaxPeriodTracker){
                             if (localTaxPeriod!=null) {
                                 for (int i = 0;i < localTaxPeriod.length;i++){

                                    if (localTaxPeriod[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "taxPeriod"));
                                         elementList.add(localTaxPeriod[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("taxPeriod cannot be null!!");
                                    
                             }

                        } if (localTitleTracker){
                             if (localTitle!=null) {
                                 for (int i = 0;i < localTitle.length;i++){

                                    if (localTitle[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "title"));
                                         elementList.add(localTitle[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                    
                             }

                        } if (localTotalTracker){
                             if (localTotal!=null) {
                                 for (int i = 0;i < localTotal.length;i++){

                                    if (localTotal[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "total"));
                                         elementList.add(localTotal[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("total cannot be null!!");
                                    
                             }

                        } if (localTranCostEstimateTracker){
                             if (localTranCostEstimate!=null) {
                                 for (int i = 0;i < localTranCostEstimate.length;i++){

                                    if (localTranCostEstimate[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "tranCostEstimate"));
                                         elementList.add(localTranCostEstimate[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("tranCostEstimate cannot be null!!");
                                    
                             }

                        } if (localTranDateTracker){
                             if (localTranDate!=null) {
                                 for (int i = 0;i < localTranDate.length;i++){

                                    if (localTranDate[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "tranDate"));
                                         elementList.add(localTranDate[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("tranDate cannot be null!!");
                                    
                             }

                        } if (localTranEstGrossProfitTracker){
                             if (localTranEstGrossProfit!=null) {
                                 for (int i = 0;i < localTranEstGrossProfit.length;i++){

                                    if (localTranEstGrossProfit[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "tranEstGrossProfit"));
                                         elementList.add(localTranEstGrossProfit[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("tranEstGrossProfit cannot be null!!");
                                    
                             }

                        } if (localTranEstGrossProfitPctTracker){
                             if (localTranEstGrossProfitPct!=null) {
                                 for (int i = 0;i < localTranEstGrossProfitPct.length;i++){

                                    if (localTranEstGrossProfitPct[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "tranEstGrossProfitPct"));
                                         elementList.add(localTranEstGrossProfitPct[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("tranEstGrossProfitPct cannot be null!!");
                                    
                             }

                        } if (localTranFxEstGrossProfitTracker){
                             if (localTranFxEstGrossProfit!=null) {
                                 for (int i = 0;i < localTranFxEstGrossProfit.length;i++){

                                    if (localTranFxEstGrossProfit[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "tranFxEstGrossProfit"));
                                         elementList.add(localTranFxEstGrossProfit[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("tranFxEstGrossProfit cannot be null!!");
                                    
                             }

                        } if (localTranIdTracker){
                             if (localTranId!=null) {
                                 for (int i = 0;i < localTranId.length;i++){

                                    if (localTranId[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "tranId"));
                                         elementList.add(localTranId[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("tranId cannot be null!!");
                                    
                             }

                        } if (localWeightedTotalTracker){
                             if (localWeightedTotal!=null) {
                                 for (int i = 0;i < localWeightedTotal.length;i++){

                                    if (localWeightedTotal[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "weightedTotal"));
                                         elementList.add(localWeightedTotal[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("weightedTotal cannot be null!!");
                                    
                             }

                        } if (localWinLossReasonTracker){
                             if (localWinLossReason!=null) {
                                 for (int i = 0;i < localWinLossReason.length;i++){

                                    if (localWinLossReason[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "winLossReason"));
                                         elementList.add(localWinLossReason[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("winLossReason cannot be null!!");
                                    
                             }

                        } if (localWonByTracker){
                             if (localWonBy!=null) {
                                 for (int i = 0;i < localWonBy.length;i++){

                                    if (localWonBy[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                          "wonBy"));
                                         elementList.add(localWonBy[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("wonBy cannot be null!!");
                                    
                             }

                        } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static OpportunitySearchRowBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            OpportunitySearchRowBasic object =
                new OpportunitySearchRowBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"OpportunitySearchRowBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (OpportunitySearchRowBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                        java.util.ArrayList list1 = new java.util.ArrayList();
                    
                        java.util.ArrayList list2 = new java.util.ArrayList();
                    
                        java.util.ArrayList list3 = new java.util.ArrayList();
                    
                        java.util.ArrayList list4 = new java.util.ArrayList();
                    
                        java.util.ArrayList list5 = new java.util.ArrayList();
                    
                        java.util.ArrayList list6 = new java.util.ArrayList();
                    
                        java.util.ArrayList list7 = new java.util.ArrayList();
                    
                        java.util.ArrayList list8 = new java.util.ArrayList();
                    
                        java.util.ArrayList list9 = new java.util.ArrayList();
                    
                        java.util.ArrayList list10 = new java.util.ArrayList();
                    
                        java.util.ArrayList list11 = new java.util.ArrayList();
                    
                        java.util.ArrayList list12 = new java.util.ArrayList();
                    
                        java.util.ArrayList list13 = new java.util.ArrayList();
                    
                        java.util.ArrayList list14 = new java.util.ArrayList();
                    
                        java.util.ArrayList list15 = new java.util.ArrayList();
                    
                        java.util.ArrayList list16 = new java.util.ArrayList();
                    
                        java.util.ArrayList list17 = new java.util.ArrayList();
                    
                        java.util.ArrayList list18 = new java.util.ArrayList();
                    
                        java.util.ArrayList list19 = new java.util.ArrayList();
                    
                        java.util.ArrayList list20 = new java.util.ArrayList();
                    
                        java.util.ArrayList list21 = new java.util.ArrayList();
                    
                        java.util.ArrayList list22 = new java.util.ArrayList();
                    
                        java.util.ArrayList list23 = new java.util.ArrayList();
                    
                        java.util.ArrayList list24 = new java.util.ArrayList();
                    
                        java.util.ArrayList list25 = new java.util.ArrayList();
                    
                        java.util.ArrayList list26 = new java.util.ArrayList();
                    
                        java.util.ArrayList list27 = new java.util.ArrayList();
                    
                        java.util.ArrayList list28 = new java.util.ArrayList();
                    
                        java.util.ArrayList list29 = new java.util.ArrayList();
                    
                        java.util.ArrayList list30 = new java.util.ArrayList();
                    
                        java.util.ArrayList list31 = new java.util.ArrayList();
                    
                        java.util.ArrayList list32 = new java.util.ArrayList();
                    
                        java.util.ArrayList list33 = new java.util.ArrayList();
                    
                        java.util.ArrayList list34 = new java.util.ArrayList();
                    
                        java.util.ArrayList list35 = new java.util.ArrayList();
                    
                        java.util.ArrayList list36 = new java.util.ArrayList();
                    
                        java.util.ArrayList list37 = new java.util.ArrayList();
                    
                        java.util.ArrayList list38 = new java.util.ArrayList();
                    
                        java.util.ArrayList list39 = new java.util.ArrayList();
                    
                        java.util.ArrayList list40 = new java.util.ArrayList();
                    
                        java.util.ArrayList list41 = new java.util.ArrayList();
                    
                        java.util.ArrayList list42 = new java.util.ArrayList();
                    
                        java.util.ArrayList list43 = new java.util.ArrayList();
                    
                        java.util.ArrayList list44 = new java.util.ArrayList();
                    
                        java.util.ArrayList list45 = new java.util.ArrayList();
                    
                        java.util.ArrayList list46 = new java.util.ArrayList();
                    
                        java.util.ArrayList list47 = new java.util.ArrayList();
                    
                        java.util.ArrayList list48 = new java.util.ArrayList();
                    
                        java.util.ArrayList list49 = new java.util.ArrayList();
                    
                        java.util.ArrayList list50 = new java.util.ArrayList();
                    
                        java.util.ArrayList list51 = new java.util.ArrayList();
                    
                        java.util.ArrayList list52 = new java.util.ArrayList();
                    
                        java.util.ArrayList list53 = new java.util.ArrayList();
                    
                        java.util.ArrayList list54 = new java.util.ArrayList();
                    
                        java.util.ArrayList list55 = new java.util.ArrayList();
                    
                        java.util.ArrayList list56 = new java.util.ArrayList();
                    
                        java.util.ArrayList list57 = new java.util.ArrayList();
                    
                        java.util.ArrayList list58 = new java.util.ArrayList();
                    
                        java.util.ArrayList list59 = new java.util.ArrayList();
                    
                        java.util.ArrayList list60 = new java.util.ArrayList();
                    
                        java.util.ArrayList list61 = new java.util.ArrayList();
                    
                        java.util.ArrayList list62 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","actionItem").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list1.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone1 = false;
                                                        while(!loopDone1){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone1 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","actionItem").equals(reader.getName())){
                                                                    list1.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone1 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setActionItem((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list1));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","availableOffline").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list2.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone2 = false;
                                                        while(!loopDone2){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone2 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","availableOffline").equals(reader.getName())){
                                                                    list2.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone2 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setAvailableOffline((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list2));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buyingReason").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list3.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone3 = false;
                                                        while(!loopDone3){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone3 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buyingReason").equals(reader.getName())){
                                                                    list3.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone3 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setBuyingReason((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list3));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buyingTimeFrame").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list4.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone4 = false;
                                                        while(!loopDone4){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone4 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","buyingTimeFrame").equals(reader.getName())){
                                                                    list4.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone4 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setBuyingTimeFrame((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list4));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","class").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list5.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone5 = false;
                                                        while(!loopDone5){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone5 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","class").equals(reader.getName())){
                                                                    list5.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone5 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.set_class((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list5));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","closeDate").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list6.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone6 = false;
                                                        while(!loopDone6){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone6 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","closeDate").equals(reader.getName())){
                                                                    list6.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone6 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCloseDate((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list6));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","competitor").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list7.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone7 = false;
                                                        while(!loopDone7){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone7 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","competitor").equals(reader.getName())){
                                                                    list7.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone7 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCompetitor((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list7));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","contribution").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list8.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone8 = false;
                                                        while(!loopDone8){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone8 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","contribution").equals(reader.getName())){
                                                                    list8.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone8 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setContribution((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list8));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","contributionPrimary").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list9.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone9 = false;
                                                        while(!loopDone9){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone9 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","contributionPrimary").equals(reader.getName())){
                                                                    list9.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone9 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setContributionPrimary((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list9));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list10.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone10 = false;
                                                        while(!loopDone10){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone10 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","currency").equals(reader.getName())){
                                                                    list10.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone10 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCurrency((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list10));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","custType").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list11.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone11 = false;
                                                        while(!loopDone11){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone11 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","custType").equals(reader.getName())){
                                                                    list11.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone11 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCustType((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list11));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list12.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone12 = false;
                                                        while(!loopDone12){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone12 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","dateCreated").equals(reader.getName())){
                                                                    list12.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone12 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setDateCreated((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list12));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","daysOpen").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list13.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone13 = false;
                                                        while(!loopDone13){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone13 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","daysOpen").equals(reader.getName())){
                                                                    list13.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone13 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setDaysOpen((com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField.class,
                                                                list13));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","daysToClose").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list14.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone14 = false;
                                                        while(!loopDone14){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone14 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","daysToClose").equals(reader.getName())){
                                                                    list14.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone14 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setDaysToClose((com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnLongField.class,
                                                                list14));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","decisionMaker").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list15.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone15 = false;
                                                        while(!loopDone15){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone15 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","decisionMaker").equals(reader.getName())){
                                                                    list15.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone15 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setDecisionMaker((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list15));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","department").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list16.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone16 = false;
                                                        while(!loopDone16){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone16 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","department").equals(reader.getName())){
                                                                    list16.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone16 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setDepartment((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list16));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","status").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list17.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone17 = false;
                                                        while(!loopDone17){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone17 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","status").equals(reader.getName())){
                                                                    list17.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone17 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setStatus((com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.class,
                                                                list17));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entity").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list18.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone18 = false;
                                                        while(!loopDone18){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone18 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entity").equals(reader.getName())){
                                                                    list18.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone18 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setEntity((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list18));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entityStatus").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list19.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone19 = false;
                                                        while(!loopDone19){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone19 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","entityStatus").equals(reader.getName())){
                                                                    list19.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone19 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setEntityStatus((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list19));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedBudget").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list20.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone20 = false;
                                                        while(!loopDone20){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone20 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedBudget").equals(reader.getName())){
                                                                    list20.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone20 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setEstimatedBudget((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list20));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","expectedCloseDate").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list21.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone21 = false;
                                                        while(!loopDone21){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone21 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","expectedCloseDate").equals(reader.getName())){
                                                                    list21.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone21 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setExpectedCloseDate((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list21));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list22.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone22 = false;
                                                        while(!loopDone22){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone22 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                                                    list22.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone22 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setExternalId((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list22));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","forecastType").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list23.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone23 = false;
                                                        while(!loopDone23){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone23 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","forecastType").equals(reader.getName())){
                                                                    list23.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone23 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setForecastType((com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnEnumSelectField.class,
                                                                list23));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","foreignProjectedAmount").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list24.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone24 = false;
                                                        while(!loopDone24){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone24 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","foreignProjectedAmount").equals(reader.getName())){
                                                                    list24.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone24 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setForeignProjectedAmount((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list24));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","foreignRangeHigh").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list25.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone25 = false;
                                                        while(!loopDone25){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone25 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","foreignRangeHigh").equals(reader.getName())){
                                                                    list25.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone25 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setForeignRangeHigh((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list25));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","foreignRangeLow").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list26.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone26 = false;
                                                        while(!loopDone26){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone26 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","foreignRangeLow").equals(reader.getName())){
                                                                    list26.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone26 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setForeignRangeLow((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list26));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxTranCostEstimate").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list27.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone27 = false;
                                                        while(!loopDone27){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone27 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","fxTranCostEstimate").equals(reader.getName())){
                                                                    list27.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone27 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setFxTranCostEstimate((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list27));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list28.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone28 = false;
                                                        while(!loopDone28){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone28 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                                                    list28.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone28 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setInternalId((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list28));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isBudgetApproved").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list29.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone29 = false;
                                                        while(!loopDone29){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone29 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isBudgetApproved").equals(reader.getName())){
                                                                    list29.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone29 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setIsBudgetApproved((com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnBooleanField.class,
                                                                list29));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list30.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone30 = false;
                                                        while(!loopDone30){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone30 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                                                    list30.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone30 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setLastModifiedDate((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list30));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","leadSource").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list31.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone31 = false;
                                                        while(!loopDone31){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone31 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","leadSource").equals(reader.getName())){
                                                                    list31.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone31 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setLeadSource((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list31));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list32.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone32 = false;
                                                        while(!loopDone32){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone32 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","location").equals(reader.getName())){
                                                                    list32.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone32 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setLocation((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list32));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","memo").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list33.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone33 = false;
                                                        while(!loopDone33){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone33 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","memo").equals(reader.getName())){
                                                                    list33.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone33 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setMemo((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list33));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partner").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list34.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone34 = false;
                                                        while(!loopDone34){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone34 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partner").equals(reader.getName())){
                                                                    list34.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone34 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setPartner((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list34));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerContribution").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list35.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone35 = false;
                                                        while(!loopDone35){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone35 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerContribution").equals(reader.getName())){
                                                                    list35.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone35 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setPartnerContribution((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list35));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerRole").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list36.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone36 = false;
                                                        while(!loopDone36){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone36 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerRole").equals(reader.getName())){
                                                                    list36.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone36 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setPartnerRole((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list36));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerTeamMember").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list37.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone37 = false;
                                                        while(!loopDone37){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone37 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","partnerTeamMember").equals(reader.getName())){
                                                                    list37.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone37 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setPartnerTeamMember((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list37));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","period").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list38.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone38 = false;
                                                        while(!loopDone38){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone38 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","period").equals(reader.getName())){
                                                                    list38.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone38 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setPeriod((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list38));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","probability").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list39.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone39 = false;
                                                        while(!loopDone39){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone39 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","probability").equals(reader.getName())){
                                                                    list39.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone39 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setProbability((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list39));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","projAltSalesAmt").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list40.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone40 = false;
                                                        while(!loopDone40){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone40 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","projAltSalesAmt").equals(reader.getName())){
                                                                    list40.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone40 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setProjAltSalesAmt((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list40));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","projectedTotal").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list41.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone41 = false;
                                                        while(!loopDone41){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone41 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","projectedTotal").equals(reader.getName())){
                                                                    list41.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone41 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setProjectedTotal((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list41));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeHigh").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list42.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone42 = false;
                                                        while(!loopDone42){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone42 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeHigh").equals(reader.getName())){
                                                                    list42.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone42 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setRangeHigh((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list42));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeHighAlt").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list43.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone43 = false;
                                                        while(!loopDone43){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone43 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeHighAlt").equals(reader.getName())){
                                                                    list43.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone43 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setRangeHighAlt((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list43));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeLow").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list44.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone44 = false;
                                                        while(!loopDone44){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone44 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeLow").equals(reader.getName())){
                                                                    list44.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone44 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setRangeLow((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list44));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeLowAlt").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list45.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone45 = false;
                                                        while(!loopDone45){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone45 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","rangeLowAlt").equals(reader.getName())){
                                                                    list45.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone45 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setRangeLowAlt((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list45));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesReadiness").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list46.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone46 = false;
                                                        while(!loopDone46){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone46 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesReadiness").equals(reader.getName())){
                                                                    list46.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone46 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setSalesReadiness((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list46));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesRep").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list47.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone47 = false;
                                                        while(!loopDone47){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone47 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesRep").equals(reader.getName())){
                                                                    list47.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone47 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setSalesRep((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list47));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesTeamMember").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list48.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone48 = false;
                                                        while(!loopDone48){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone48 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesTeamMember").equals(reader.getName())){
                                                                    list48.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone48 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setSalesTeamMember((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list48));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesTeamRole").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list49.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone49 = false;
                                                        while(!loopDone49){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone49 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","salesTeamRole").equals(reader.getName())){
                                                                    list49.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone49 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setSalesTeamRole((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list49));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list50.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone50 = false;
                                                        while(!loopDone50){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone50 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                                                    list50.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone50 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setSubsidiary((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list50));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","taxPeriod").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list51.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone51 = false;
                                                        while(!loopDone51){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone51 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","taxPeriod").equals(reader.getName())){
                                                                    list51.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone51 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setTaxPeriod((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list51));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","title").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list52.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone52 = false;
                                                        while(!loopDone52){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone52 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","title").equals(reader.getName())){
                                                                    list52.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone52 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setTitle((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list52));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","total").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list53.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone53 = false;
                                                        while(!loopDone53){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone53 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","total").equals(reader.getName())){
                                                                    list53.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone53 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setTotal((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list53));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranCostEstimate").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list54.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone54 = false;
                                                        while(!loopDone54){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone54 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranCostEstimate").equals(reader.getName())){
                                                                    list54.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone54 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setTranCostEstimate((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list54));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranDate").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list55.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone55 = false;
                                                        while(!loopDone55){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone55 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranDate").equals(reader.getName())){
                                                                    list55.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone55 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setTranDate((com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDateField.class,
                                                                list55));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranEstGrossProfit").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list56.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone56 = false;
                                                        while(!loopDone56){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone56 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranEstGrossProfit").equals(reader.getName())){
                                                                    list56.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone56 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setTranEstGrossProfit((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list56));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranEstGrossProfitPct").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list57.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone57 = false;
                                                        while(!loopDone57){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone57 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranEstGrossProfitPct").equals(reader.getName())){
                                                                    list57.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone57 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setTranEstGrossProfitPct((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list57));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranFxEstGrossProfit").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list58.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone58 = false;
                                                        while(!loopDone58){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone58 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranFxEstGrossProfit").equals(reader.getName())){
                                                                    list58.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone58 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setTranFxEstGrossProfit((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list58));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranId").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list59.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone59 = false;
                                                        while(!loopDone59){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone59 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranId").equals(reader.getName())){
                                                                    list59.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone59 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setTranId((com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnStringField.class,
                                                                list59));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","weightedTotal").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list60.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone60 = false;
                                                        while(!loopDone60){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone60 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","weightedTotal").equals(reader.getName())){
                                                                    list60.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone60 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setWeightedTotal((com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnDoubleField.class,
                                                                list60));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","winLossReason").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list61.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone61 = false;
                                                        while(!loopDone61){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone61 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","winLossReason").equals(reader.getName())){
                                                                    list61.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone61 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setWinLossReason((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list61));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","wonBy").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list62.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone62 = false;
                                                        while(!loopDone62){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone62 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","wonBy").equals(reader.getName())){
                                                                    list62.add(com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone62 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setWonBy((com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.core_2017_2.SearchColumnSelectField.class,
                                                                list62));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchColumnCustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    