
/**
 * AccountingTransactionSearchBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  AccountingTransactionSearchBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class AccountingTransactionSearchBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRecordBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = AccountingTransactionSearchBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for Account
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localAccount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountTracker = false ;

                           public boolean isAccountSpecified(){
                               return localAccountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getAccount(){
                               return localAccount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Account
                               */
                               public void setAccount(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localAccountTracker = param != null;
                                   
                                            this.localAccount=param;
                                    

                               }
                            

                        /**
                        * field for AccountingBook
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localAccountingBook ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountingBookTracker = false ;

                           public boolean isAccountingBookSpecified(){
                               return localAccountingBookTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getAccountingBook(){
                               return localAccountingBook;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccountingBook
                               */
                               public void setAccountingBook(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localAccountingBookTracker = param != null;
                                   
                                            this.localAccountingBook=param;
                                    

                               }
                            

                        /**
                        * field for AccountType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localAccountType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccountTypeTracker = false ;

                           public boolean isAccountTypeSpecified(){
                               return localAccountTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getAccountType(){
                               return localAccountType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccountType
                               */
                               public void setAccountType(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localAccountTypeTracker = param != null;
                                   
                                            this.localAccountType=param;
                                    

                               }
                            

                        /**
                        * field for AltSalesAmount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localAltSalesAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAltSalesAmountTracker = false ;

                           public boolean isAltSalesAmountSpecified(){
                               return localAltSalesAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getAltSalesAmount(){
                               return localAltSalesAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AltSalesAmount
                               */
                               public void setAltSalesAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localAltSalesAmountTracker = param != null;
                                   
                                            this.localAltSalesAmount=param;
                                    

                               }
                            

                        /**
                        * field for AltSalesNetAmount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localAltSalesNetAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAltSalesNetAmountTracker = false ;

                           public boolean isAltSalesNetAmountSpecified(){
                               return localAltSalesNetAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getAltSalesNetAmount(){
                               return localAltSalesNetAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AltSalesNetAmount
                               */
                               public void setAltSalesNetAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localAltSalesNetAmountTracker = param != null;
                                   
                                            this.localAltSalesNetAmount=param;
                                    

                               }
                            

                        /**
                        * field for Amount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAmountTracker = false ;

                           public boolean isAmountSpecified(){
                               return localAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getAmount(){
                               return localAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Amount
                               */
                               public void setAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localAmountTracker = param != null;
                                   
                                            this.localAmount=param;
                                    

                               }
                            

                        /**
                        * field for BookSpecificTransaction
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localBookSpecificTransaction ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBookSpecificTransactionTracker = false ;

                           public boolean isBookSpecificTransactionSpecified(){
                               return localBookSpecificTransactionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getBookSpecificTransaction(){
                               return localBookSpecificTransaction;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param BookSpecificTransaction
                               */
                               public void setBookSpecificTransaction(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localBookSpecificTransactionTracker = param != null;
                                   
                                            this.localBookSpecificTransaction=param;
                                    

                               }
                            

                        /**
                        * field for CatchUpPeriod
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCatchUpPeriod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCatchUpPeriodTracker = false ;

                           public boolean isCatchUpPeriodSpecified(){
                               return localCatchUpPeriodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCatchUpPeriod(){
                               return localCatchUpPeriod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CatchUpPeriod
                               */
                               public void setCatchUpPeriod(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCatchUpPeriodTracker = param != null;
                                   
                                            this.localCatchUpPeriod=param;
                                    

                               }
                            

                        /**
                        * field for CreditAmount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localCreditAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreditAmountTracker = false ;

                           public boolean isCreditAmountSpecified(){
                               return localCreditAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getCreditAmount(){
                               return localCreditAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreditAmount
                               */
                               public void setCreditAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localCreditAmountTracker = param != null;
                                   
                                            this.localCreditAmount=param;
                                    

                               }
                            

                        /**
                        * field for CustomGL
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localCustomGL ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomGLTracker = false ;

                           public boolean isCustomGLSpecified(){
                               return localCustomGLTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getCustomGL(){
                               return localCustomGL;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomGL
                               */
                               public void setCustomGL(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localCustomGLTracker = param != null;
                                   
                                            this.localCustomGL=param;
                                    

                               }
                            

                        /**
                        * field for DebitAmount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localDebitAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDebitAmountTracker = false ;

                           public boolean isDebitAmountSpecified(){
                               return localDebitAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getDebitAmount(){
                               return localDebitAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DebitAmount
                               */
                               public void setDebitAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localDebitAmountTracker = param != null;
                                   
                                            this.localDebitAmount=param;
                                    

                               }
                            

                        /**
                        * field for DeferRevRec
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localDeferRevRec ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDeferRevRecTracker = false ;

                           public boolean isDeferRevRecSpecified(){
                               return localDeferRevRecTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getDeferRevRec(){
                               return localDeferRevRec;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DeferRevRec
                               */
                               public void setDeferRevRec(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localDeferRevRecTracker = param != null;
                                   
                                            this.localDeferRevRec=param;
                                    

                               }
                            

                        /**
                        * field for ExchangeRate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localExchangeRate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExchangeRateTracker = false ;

                           public boolean isExchangeRateSpecified(){
                               return localExchangeRateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getExchangeRate(){
                               return localExchangeRate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExchangeRate
                               */
                               public void setExchangeRate(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localExchangeRateTracker = param != null;
                                   
                                            this.localExchangeRate=param;
                                    

                               }
                            

                        /**
                        * field for GrossAmount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localGrossAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localGrossAmountTracker = false ;

                           public boolean isGrossAmountSpecified(){
                               return localGrossAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getGrossAmount(){
                               return localGrossAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param GrossAmount
                               */
                               public void setGrossAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localGrossAmountTracker = param != null;
                                   
                                            this.localGrossAmount=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localInternalIdTracker = param != null;
                                   
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for InternalIdNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localInternalIdNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdNumberTracker = false ;

                           public boolean isInternalIdNumberSpecified(){
                               return localInternalIdNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getInternalIdNumber(){
                               return localInternalIdNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalIdNumber
                               */
                               public void setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localInternalIdNumberTracker = param != null;
                                   
                                            this.localInternalIdNumber=param;
                                    

                               }
                            

                        /**
                        * field for MultiSubsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localMultiSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMultiSubsidiaryTracker = false ;

                           public boolean isMultiSubsidiarySpecified(){
                               return localMultiSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getMultiSubsidiary(){
                               return localMultiSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MultiSubsidiary
                               */
                               public void setMultiSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localMultiSubsidiaryTracker = param != null;
                                   
                                            this.localMultiSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for NetAmount
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localNetAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNetAmountTracker = false ;

                           public boolean isNetAmountSpecified(){
                               return localNetAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getNetAmount(){
                               return localNetAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NetAmount
                               */
                               public void setNetAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localNetAmountTracker = param != null;
                                   
                                            this.localNetAmount=param;
                                    

                               }
                            

                        /**
                        * field for Posting
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localPosting ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPostingTracker = false ;

                           public boolean isPostingSpecified(){
                               return localPostingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getPosting(){
                               return localPosting;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Posting
                               */
                               public void setPosting(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localPostingTracker = param != null;
                                   
                                            this.localPosting=param;
                                    

                               }
                            

                        /**
                        * field for QuantityRevCommitted
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localQuantityRevCommitted ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localQuantityRevCommittedTracker = false ;

                           public boolean isQuantityRevCommittedSpecified(){
                               return localQuantityRevCommittedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getQuantityRevCommitted(){
                               return localQuantityRevCommitted;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param QuantityRevCommitted
                               */
                               public void setQuantityRevCommitted(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localQuantityRevCommittedTracker = param != null;
                                   
                                            this.localQuantityRevCommitted=param;
                                    

                               }
                            

                        /**
                        * field for RecognizedRevenue
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localRecognizedRevenue ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecognizedRevenueTracker = false ;

                           public boolean isRecognizedRevenueSpecified(){
                               return localRecognizedRevenueTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getRecognizedRevenue(){
                               return localRecognizedRevenue;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecognizedRevenue
                               */
                               public void setRecognizedRevenue(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localRecognizedRevenueTracker = param != null;
                                   
                                            this.localRecognizedRevenue=param;
                                    

                               }
                            

                        /**
                        * field for RevCommitStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localRevCommitStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevCommitStatusTracker = false ;

                           public boolean isRevCommitStatusSpecified(){
                               return localRevCommitStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getRevCommitStatus(){
                               return localRevCommitStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevCommitStatus
                               */
                               public void setRevCommitStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localRevCommitStatusTracker = param != null;
                                   
                                            this.localRevCommitStatus=param;
                                    

                               }
                            

                        /**
                        * field for RevenueStatus
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localRevenueStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevenueStatusTracker = false ;

                           public boolean isRevenueStatusSpecified(){
                               return localRevenueStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getRevenueStatus(){
                               return localRevenueStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevenueStatus
                               */
                               public void setRevenueStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localRevenueStatusTracker = param != null;
                                   
                                            this.localRevenueStatus=param;
                                    

                               }
                            

                        /**
                        * field for RevRecEndDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localRevRecEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecEndDateTracker = false ;

                           public boolean isRevRecEndDateSpecified(){
                               return localRevRecEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getRevRecEndDate(){
                               return localRevRecEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecEndDate
                               */
                               public void setRevRecEndDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localRevRecEndDateTracker = param != null;
                                   
                                            this.localRevRecEndDate=param;
                                    

                               }
                            

                        /**
                        * field for RevRecOnRevCommitment
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localRevRecOnRevCommitment ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecOnRevCommitmentTracker = false ;

                           public boolean isRevRecOnRevCommitmentSpecified(){
                               return localRevRecOnRevCommitmentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getRevRecOnRevCommitment(){
                               return localRevRecOnRevCommitment;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecOnRevCommitment
                               */
                               public void setRevRecOnRevCommitment(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localRevRecOnRevCommitmentTracker = param != null;
                                   
                                            this.localRevRecOnRevCommitment=param;
                                    

                               }
                            

                        /**
                        * field for RevRecStartDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localRevRecStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRevRecStartDateTracker = false ;

                           public boolean isRevRecStartDateSpecified(){
                               return localRevRecStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getRevRecStartDate(){
                               return localRevRecStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RevRecStartDate
                               */
                               public void setRevRecStartDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localRevRecStartDateTracker = param != null;
                                   
                                            this.localRevRecStartDate=param;
                                    

                               }
                            

                        /**
                        * field for Subsidiary
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localSubsidiary ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSubsidiaryTracker = false ;

                           public boolean isSubsidiarySpecified(){
                               return localSubsidiaryTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getSubsidiary(){
                               return localSubsidiary;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Subsidiary
                               */
                               public void setSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localSubsidiaryTracker = param != null;
                                   
                                            this.localSubsidiary=param;
                                    

                               }
                            

                        /**
                        * field for TranIsVsoeBundle
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localTranIsVsoeBundle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTranIsVsoeBundleTracker = false ;

                           public boolean isTranIsVsoeBundleSpecified(){
                               return localTranIsVsoeBundleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getTranIsVsoeBundle(){
                               return localTranIsVsoeBundle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TranIsVsoeBundle
                               */
                               public void setTranIsVsoeBundle(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localTranIsVsoeBundleTracker = param != null;
                                   
                                            this.localTranIsVsoeBundle=param;
                                    

                               }
                            

                        /**
                        * field for Type
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTypeTracker = false ;

                           public boolean isTypeSpecified(){
                               return localTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getType(){
                               return localType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Type
                               */
                               public void setType(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localTypeTracker = param != null;
                                   
                                            this.localType=param;
                                    

                               }
                            

                        /**
                        * field for VsoeAllocation
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localVsoeAllocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localVsoeAllocationTracker = false ;

                           public boolean isVsoeAllocationSpecified(){
                               return localVsoeAllocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getVsoeAllocation(){
                               return localVsoeAllocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param VsoeAllocation
                               */
                               public void setVsoeAllocation(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localVsoeAllocationTracker = param != null;
                                   
                                            this.localVsoeAllocation=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":AccountingTransactionSearchBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "AccountingTransactionSearchBasic",
                           xmlWriter);
                   }

                if (localAccountTracker){
                                            if (localAccount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("account cannot be null!!");
                                            }
                                           localAccount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","account"),
                                               xmlWriter);
                                        } if (localAccountingBookTracker){
                                            if (localAccountingBook==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accountingBook cannot be null!!");
                                            }
                                           localAccountingBook.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","accountingBook"),
                                               xmlWriter);
                                        } if (localAccountTypeTracker){
                                            if (localAccountType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accountType cannot be null!!");
                                            }
                                           localAccountType.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","accountType"),
                                               xmlWriter);
                                        } if (localAltSalesAmountTracker){
                                            if (localAltSalesAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("altSalesAmount cannot be null!!");
                                            }
                                           localAltSalesAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","altSalesAmount"),
                                               xmlWriter);
                                        } if (localAltSalesNetAmountTracker){
                                            if (localAltSalesNetAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("altSalesNetAmount cannot be null!!");
                                            }
                                           localAltSalesNetAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","altSalesNetAmount"),
                                               xmlWriter);
                                        } if (localAmountTracker){
                                            if (localAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("amount cannot be null!!");
                                            }
                                           localAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","amount"),
                                               xmlWriter);
                                        } if (localBookSpecificTransactionTracker){
                                            if (localBookSpecificTransaction==null){
                                                 throw new org.apache.axis2.databinding.ADBException("bookSpecificTransaction cannot be null!!");
                                            }
                                           localBookSpecificTransaction.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","bookSpecificTransaction"),
                                               xmlWriter);
                                        } if (localCatchUpPeriodTracker){
                                            if (localCatchUpPeriod==null){
                                                 throw new org.apache.axis2.databinding.ADBException("catchUpPeriod cannot be null!!");
                                            }
                                           localCatchUpPeriod.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","catchUpPeriod"),
                                               xmlWriter);
                                        } if (localCreditAmountTracker){
                                            if (localCreditAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("creditAmount cannot be null!!");
                                            }
                                           localCreditAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","creditAmount"),
                                               xmlWriter);
                                        } if (localCustomGLTracker){
                                            if (localCustomGL==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customGL cannot be null!!");
                                            }
                                           localCustomGL.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customGL"),
                                               xmlWriter);
                                        } if (localDebitAmountTracker){
                                            if (localDebitAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("debitAmount cannot be null!!");
                                            }
                                           localDebitAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","debitAmount"),
                                               xmlWriter);
                                        } if (localDeferRevRecTracker){
                                            if (localDeferRevRec==null){
                                                 throw new org.apache.axis2.databinding.ADBException("deferRevRec cannot be null!!");
                                            }
                                           localDeferRevRec.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","deferRevRec"),
                                               xmlWriter);
                                        } if (localExchangeRateTracker){
                                            if (localExchangeRate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("exchangeRate cannot be null!!");
                                            }
                                           localExchangeRate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","exchangeRate"),
                                               xmlWriter);
                                        } if (localGrossAmountTracker){
                                            if (localGrossAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("grossAmount cannot be null!!");
                                            }
                                           localGrossAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","grossAmount"),
                                               xmlWriter);
                                        } if (localInternalIdTracker){
                                            if (localInternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                            }
                                           localInternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                               xmlWriter);
                                        } if (localInternalIdNumberTracker){
                                            if (localInternalIdNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                            }
                                           localInternalIdNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber"),
                                               xmlWriter);
                                        } if (localMultiSubsidiaryTracker){
                                            if (localMultiSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("multiSubsidiary cannot be null!!");
                                            }
                                           localMultiSubsidiary.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","multiSubsidiary"),
                                               xmlWriter);
                                        } if (localNetAmountTracker){
                                            if (localNetAmount==null){
                                                 throw new org.apache.axis2.databinding.ADBException("netAmount cannot be null!!");
                                            }
                                           localNetAmount.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","netAmount"),
                                               xmlWriter);
                                        } if (localPostingTracker){
                                            if (localPosting==null){
                                                 throw new org.apache.axis2.databinding.ADBException("posting cannot be null!!");
                                            }
                                           localPosting.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","posting"),
                                               xmlWriter);
                                        } if (localQuantityRevCommittedTracker){
                                            if (localQuantityRevCommitted==null){
                                                 throw new org.apache.axis2.databinding.ADBException("quantityRevCommitted cannot be null!!");
                                            }
                                           localQuantityRevCommitted.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","quantityRevCommitted"),
                                               xmlWriter);
                                        } if (localRecognizedRevenueTracker){
                                            if (localRecognizedRevenue==null){
                                                 throw new org.apache.axis2.databinding.ADBException("recognizedRevenue cannot be null!!");
                                            }
                                           localRecognizedRevenue.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","recognizedRevenue"),
                                               xmlWriter);
                                        } if (localRevCommitStatusTracker){
                                            if (localRevCommitStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revCommitStatus cannot be null!!");
                                            }
                                           localRevCommitStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","revCommitStatus"),
                                               xmlWriter);
                                        } if (localRevenueStatusTracker){
                                            if (localRevenueStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revenueStatus cannot be null!!");
                                            }
                                           localRevenueStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","revenueStatus"),
                                               xmlWriter);
                                        } if (localRevRecEndDateTracker){
                                            if (localRevRecEndDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revRecEndDate cannot be null!!");
                                            }
                                           localRevRecEndDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","revRecEndDate"),
                                               xmlWriter);
                                        } if (localRevRecOnRevCommitmentTracker){
                                            if (localRevRecOnRevCommitment==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revRecOnRevCommitment cannot be null!!");
                                            }
                                           localRevRecOnRevCommitment.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","revRecOnRevCommitment"),
                                               xmlWriter);
                                        } if (localRevRecStartDateTracker){
                                            if (localRevRecStartDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("revRecStartDate cannot be null!!");
                                            }
                                           localRevRecStartDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","revRecStartDate"),
                                               xmlWriter);
                                        } if (localSubsidiaryTracker){
                                            if (localSubsidiary==null){
                                                 throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                            }
                                           localSubsidiary.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary"),
                                               xmlWriter);
                                        } if (localTranIsVsoeBundleTracker){
                                            if (localTranIsVsoeBundle==null){
                                                 throw new org.apache.axis2.databinding.ADBException("tranIsVsoeBundle cannot be null!!");
                                            }
                                           localTranIsVsoeBundle.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranIsVsoeBundle"),
                                               xmlWriter);
                                        } if (localTypeTracker){
                                            if (localType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("type cannot be null!!");
                                            }
                                           localType.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","type"),
                                               xmlWriter);
                                        } if (localVsoeAllocationTracker){
                                            if (localVsoeAllocation==null){
                                                 throw new org.apache.axis2.databinding.ADBException("vsoeAllocation cannot be null!!");
                                            }
                                           localVsoeAllocation.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","vsoeAllocation"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","AccountingTransactionSearchBasic"));
                 if (localAccountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "account"));
                            
                            
                                    if (localAccount==null){
                                         throw new org.apache.axis2.databinding.ADBException("account cannot be null!!");
                                    }
                                    elementList.add(localAccount);
                                } if (localAccountingBookTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "accountingBook"));
                            
                            
                                    if (localAccountingBook==null){
                                         throw new org.apache.axis2.databinding.ADBException("accountingBook cannot be null!!");
                                    }
                                    elementList.add(localAccountingBook);
                                } if (localAccountTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "accountType"));
                            
                            
                                    if (localAccountType==null){
                                         throw new org.apache.axis2.databinding.ADBException("accountType cannot be null!!");
                                    }
                                    elementList.add(localAccountType);
                                } if (localAltSalesAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "altSalesAmount"));
                            
                            
                                    if (localAltSalesAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("altSalesAmount cannot be null!!");
                                    }
                                    elementList.add(localAltSalesAmount);
                                } if (localAltSalesNetAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "altSalesNetAmount"));
                            
                            
                                    if (localAltSalesNetAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("altSalesNetAmount cannot be null!!");
                                    }
                                    elementList.add(localAltSalesNetAmount);
                                } if (localAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "amount"));
                            
                            
                                    if (localAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("amount cannot be null!!");
                                    }
                                    elementList.add(localAmount);
                                } if (localBookSpecificTransactionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "bookSpecificTransaction"));
                            
                            
                                    if (localBookSpecificTransaction==null){
                                         throw new org.apache.axis2.databinding.ADBException("bookSpecificTransaction cannot be null!!");
                                    }
                                    elementList.add(localBookSpecificTransaction);
                                } if (localCatchUpPeriodTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "catchUpPeriod"));
                            
                            
                                    if (localCatchUpPeriod==null){
                                         throw new org.apache.axis2.databinding.ADBException("catchUpPeriod cannot be null!!");
                                    }
                                    elementList.add(localCatchUpPeriod);
                                } if (localCreditAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "creditAmount"));
                            
                            
                                    if (localCreditAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("creditAmount cannot be null!!");
                                    }
                                    elementList.add(localCreditAmount);
                                } if (localCustomGLTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customGL"));
                            
                            
                                    if (localCustomGL==null){
                                         throw new org.apache.axis2.databinding.ADBException("customGL cannot be null!!");
                                    }
                                    elementList.add(localCustomGL);
                                } if (localDebitAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "debitAmount"));
                            
                            
                                    if (localDebitAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("debitAmount cannot be null!!");
                                    }
                                    elementList.add(localDebitAmount);
                                } if (localDeferRevRecTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "deferRevRec"));
                            
                            
                                    if (localDeferRevRec==null){
                                         throw new org.apache.axis2.databinding.ADBException("deferRevRec cannot be null!!");
                                    }
                                    elementList.add(localDeferRevRec);
                                } if (localExchangeRateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "exchangeRate"));
                            
                            
                                    if (localExchangeRate==null){
                                         throw new org.apache.axis2.databinding.ADBException("exchangeRate cannot be null!!");
                                    }
                                    elementList.add(localExchangeRate);
                                } if (localGrossAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "grossAmount"));
                            
                            
                                    if (localGrossAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("grossAmount cannot be null!!");
                                    }
                                    elementList.add(localGrossAmount);
                                } if (localInternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalId"));
                            
                            
                                    if (localInternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    }
                                    elementList.add(localInternalId);
                                } if (localInternalIdNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalIdNumber"));
                            
                            
                                    if (localInternalIdNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                    }
                                    elementList.add(localInternalIdNumber);
                                } if (localMultiSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "multiSubsidiary"));
                            
                            
                                    if (localMultiSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("multiSubsidiary cannot be null!!");
                                    }
                                    elementList.add(localMultiSubsidiary);
                                } if (localNetAmountTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "netAmount"));
                            
                            
                                    if (localNetAmount==null){
                                         throw new org.apache.axis2.databinding.ADBException("netAmount cannot be null!!");
                                    }
                                    elementList.add(localNetAmount);
                                } if (localPostingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "posting"));
                            
                            
                                    if (localPosting==null){
                                         throw new org.apache.axis2.databinding.ADBException("posting cannot be null!!");
                                    }
                                    elementList.add(localPosting);
                                } if (localQuantityRevCommittedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "quantityRevCommitted"));
                            
                            
                                    if (localQuantityRevCommitted==null){
                                         throw new org.apache.axis2.databinding.ADBException("quantityRevCommitted cannot be null!!");
                                    }
                                    elementList.add(localQuantityRevCommitted);
                                } if (localRecognizedRevenueTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "recognizedRevenue"));
                            
                            
                                    if (localRecognizedRevenue==null){
                                         throw new org.apache.axis2.databinding.ADBException("recognizedRevenue cannot be null!!");
                                    }
                                    elementList.add(localRecognizedRevenue);
                                } if (localRevCommitStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "revCommitStatus"));
                            
                            
                                    if (localRevCommitStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("revCommitStatus cannot be null!!");
                                    }
                                    elementList.add(localRevCommitStatus);
                                } if (localRevenueStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "revenueStatus"));
                            
                            
                                    if (localRevenueStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("revenueStatus cannot be null!!");
                                    }
                                    elementList.add(localRevenueStatus);
                                } if (localRevRecEndDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "revRecEndDate"));
                            
                            
                                    if (localRevRecEndDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("revRecEndDate cannot be null!!");
                                    }
                                    elementList.add(localRevRecEndDate);
                                } if (localRevRecOnRevCommitmentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "revRecOnRevCommitment"));
                            
                            
                                    if (localRevRecOnRevCommitment==null){
                                         throw new org.apache.axis2.databinding.ADBException("revRecOnRevCommitment cannot be null!!");
                                    }
                                    elementList.add(localRevRecOnRevCommitment);
                                } if (localRevRecStartDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "revRecStartDate"));
                            
                            
                                    if (localRevRecStartDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("revRecStartDate cannot be null!!");
                                    }
                                    elementList.add(localRevRecStartDate);
                                } if (localSubsidiaryTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "subsidiary"));
                            
                            
                                    if (localSubsidiary==null){
                                         throw new org.apache.axis2.databinding.ADBException("subsidiary cannot be null!!");
                                    }
                                    elementList.add(localSubsidiary);
                                } if (localTranIsVsoeBundleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "tranIsVsoeBundle"));
                            
                            
                                    if (localTranIsVsoeBundle==null){
                                         throw new org.apache.axis2.databinding.ADBException("tranIsVsoeBundle cannot be null!!");
                                    }
                                    elementList.add(localTranIsVsoeBundle);
                                } if (localTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "type"));
                            
                            
                                    if (localType==null){
                                         throw new org.apache.axis2.databinding.ADBException("type cannot be null!!");
                                    }
                                    elementList.add(localType);
                                } if (localVsoeAllocationTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "vsoeAllocation"));
                            
                            
                                    if (localVsoeAllocation==null){
                                         throw new org.apache.axis2.databinding.ADBException("vsoeAllocation cannot be null!!");
                                    }
                                    elementList.add(localVsoeAllocation);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static AccountingTransactionSearchBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            AccountingTransactionSearchBasic object =
                new AccountingTransactionSearchBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"AccountingTransactionSearchBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (AccountingTransactionSearchBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","account").equals(reader.getName())){
                                
                                                object.setAccount(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","accountingBook").equals(reader.getName())){
                                
                                                object.setAccountingBook(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","accountType").equals(reader.getName())){
                                
                                                object.setAccountType(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","altSalesAmount").equals(reader.getName())){
                                
                                                object.setAltSalesAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","altSalesNetAmount").equals(reader.getName())){
                                
                                                object.setAltSalesNetAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","amount").equals(reader.getName())){
                                
                                                object.setAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","bookSpecificTransaction").equals(reader.getName())){
                                
                                                object.setBookSpecificTransaction(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","catchUpPeriod").equals(reader.getName())){
                                
                                                object.setCatchUpPeriod(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","creditAmount").equals(reader.getName())){
                                
                                                object.setCreditAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customGL").equals(reader.getName())){
                                
                                                object.setCustomGL(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","debitAmount").equals(reader.getName())){
                                
                                                object.setDebitAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","deferRevRec").equals(reader.getName())){
                                
                                                object.setDeferRevRec(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","exchangeRate").equals(reader.getName())){
                                
                                                object.setExchangeRate(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","grossAmount").equals(reader.getName())){
                                
                                                object.setGrossAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                                object.setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber").equals(reader.getName())){
                                
                                                object.setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","multiSubsidiary").equals(reader.getName())){
                                
                                                object.setMultiSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","netAmount").equals(reader.getName())){
                                
                                                object.setNetAmount(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","posting").equals(reader.getName())){
                                
                                                object.setPosting(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","quantityRevCommitted").equals(reader.getName())){
                                
                                                object.setQuantityRevCommitted(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","recognizedRevenue").equals(reader.getName())){
                                
                                                object.setRecognizedRevenue(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","revCommitStatus").equals(reader.getName())){
                                
                                                object.setRevCommitStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","revenueStatus").equals(reader.getName())){
                                
                                                object.setRevenueStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","revRecEndDate").equals(reader.getName())){
                                
                                                object.setRevRecEndDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","revRecOnRevCommitment").equals(reader.getName())){
                                
                                                object.setRevRecOnRevCommitment(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","revRecStartDate").equals(reader.getName())){
                                
                                                object.setRevRecStartDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","subsidiary").equals(reader.getName())){
                                
                                                object.setSubsidiary(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","tranIsVsoeBundle").equals(reader.getName())){
                                
                                                object.setTranIsVsoeBundle(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","type").equals(reader.getName())){
                                
                                                object.setType(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","vsoeAllocation").equals(reader.getName())){
                                
                                                object.setVsoeAllocation(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    