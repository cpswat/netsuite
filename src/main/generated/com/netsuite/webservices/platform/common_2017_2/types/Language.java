
/**
 * Language.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2.types;
            

            /**
            *  Language bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class Language
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.common_2017_2.platform.webservices.netsuite.com",
                "Language",
                "ns6");

            

                        /**
                        * field for Language
                        */

                        
                                    protected java.lang.String localLanguage ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected Language(java.lang.String value, boolean isRegisterValue) {
                                    localLanguage = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localLanguage, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String __afrikaans =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_afrikaans");
                                
                                    public static final java.lang.String __albanian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_albanian");
                                
                                    public static final java.lang.String __arabic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_arabic");
                                
                                    public static final java.lang.String __armenian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_armenian");
                                
                                    public static final java.lang.String __bengali =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bengali");
                                
                                    public static final java.lang.String __bosnian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bosnian");
                                
                                    public static final java.lang.String __bulgarian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_bulgarian");
                                
                                    public static final java.lang.String __catalan =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_catalan");
                                
                                    public static final java.lang.String __chineseSimplified =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_chineseSimplified");
                                
                                    public static final java.lang.String __chineseTraditional =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_chineseTraditional");
                                
                                    public static final java.lang.String __croatian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_croatian");
                                
                                    public static final java.lang.String __czech =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_czech");
                                
                                    public static final java.lang.String __danish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_danish");
                                
                                    public static final java.lang.String __dutch =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_dutch");
                                
                                    public static final java.lang.String __englishAu =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_englishAu");
                                
                                    public static final java.lang.String __englishCa =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_englishCa");
                                
                                    public static final java.lang.String __englishInternational =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_englishInternational");
                                
                                    public static final java.lang.String __englishUK =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_englishUK");
                                
                                    public static final java.lang.String __englishUS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_englishUS");
                                
                                    public static final java.lang.String __estonian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_estonian");
                                
                                    public static final java.lang.String __filipino =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_filipino");
                                
                                    public static final java.lang.String __finnish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_finnish");
                                
                                    public static final java.lang.String __frenchCanada =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_frenchCanada");
                                
                                    public static final java.lang.String __frenchFrance =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_frenchFrance");
                                
                                    public static final java.lang.String __german =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_german");
                                
                                    public static final java.lang.String __greek =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_greek");
                                
                                    public static final java.lang.String __gujarati =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_gujarati");
                                
                                    public static final java.lang.String __haitian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_haitian");
                                
                                    public static final java.lang.String __hebrew =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_hebrew");
                                
                                    public static final java.lang.String __hindi =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_hindi");
                                
                                    public static final java.lang.String __hungarian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_hungarian");
                                
                                    public static final java.lang.String __icelandic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_icelandic");
                                
                                    public static final java.lang.String __indonesian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_indonesian");
                                
                                    public static final java.lang.String __italian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_italian");
                                
                                    public static final java.lang.String __japanese =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_japanese");
                                
                                    public static final java.lang.String __kannada =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_kannada");
                                
                                    public static final java.lang.String __korean =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_korean");
                                
                                    public static final java.lang.String __latinAmericanSpanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_latinAmericanSpanish");
                                
                                    public static final java.lang.String __latvian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_latvian");
                                
                                    public static final java.lang.String __lithuanian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_lithuanian");
                                
                                    public static final java.lang.String __luxembourgish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_luxembourgish");
                                
                                    public static final java.lang.String __malay =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_malay");
                                
                                    public static final java.lang.String __marathi =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_marathi");
                                
                                    public static final java.lang.String __norwegian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_norwegian");
                                
                                    public static final java.lang.String __persianIran =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_persianIran");
                                
                                    public static final java.lang.String __polish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_polish");
                                
                                    public static final java.lang.String __portugueseBrazil =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_portugueseBrazil");
                                
                                    public static final java.lang.String __portuguesePortugal =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_portuguesePortugal");
                                
                                    public static final java.lang.String __punjabi =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_punjabi");
                                
                                    public static final java.lang.String __romanian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_romanian");
                                
                                    public static final java.lang.String __russian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_russian");
                                
                                    public static final java.lang.String __serbianCyrillic =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_serbianCyrillic");
                                
                                    public static final java.lang.String __serbianLatin =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_serbianLatin");
                                
                                    public static final java.lang.String __slovak =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_slovak");
                                
                                    public static final java.lang.String __slovenian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_slovenian");
                                
                                    public static final java.lang.String __spanish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_spanish");
                                
                                    public static final java.lang.String __swedish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_swedish");
                                
                                    public static final java.lang.String __tamil =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_tamil");
                                
                                    public static final java.lang.String __telugu =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_telugu");
                                
                                    public static final java.lang.String __thai =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_thai");
                                
                                    public static final java.lang.String __turkish =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_turkish");
                                
                                    public static final java.lang.String __ukrainian =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ukrainian");
                                
                                    public static final java.lang.String __vietnamese =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_vietnamese");
                                
                                public static final Language _afrikaans =
                                    new Language(__afrikaans,true);
                            
                                public static final Language _albanian =
                                    new Language(__albanian,true);
                            
                                public static final Language _arabic =
                                    new Language(__arabic,true);
                            
                                public static final Language _armenian =
                                    new Language(__armenian,true);
                            
                                public static final Language _bengali =
                                    new Language(__bengali,true);
                            
                                public static final Language _bosnian =
                                    new Language(__bosnian,true);
                            
                                public static final Language _bulgarian =
                                    new Language(__bulgarian,true);
                            
                                public static final Language _catalan =
                                    new Language(__catalan,true);
                            
                                public static final Language _chineseSimplified =
                                    new Language(__chineseSimplified,true);
                            
                                public static final Language _chineseTraditional =
                                    new Language(__chineseTraditional,true);
                            
                                public static final Language _croatian =
                                    new Language(__croatian,true);
                            
                                public static final Language _czech =
                                    new Language(__czech,true);
                            
                                public static final Language _danish =
                                    new Language(__danish,true);
                            
                                public static final Language _dutch =
                                    new Language(__dutch,true);
                            
                                public static final Language _englishAu =
                                    new Language(__englishAu,true);
                            
                                public static final Language _englishCa =
                                    new Language(__englishCa,true);
                            
                                public static final Language _englishInternational =
                                    new Language(__englishInternational,true);
                            
                                public static final Language _englishUK =
                                    new Language(__englishUK,true);
                            
                                public static final Language _englishUS =
                                    new Language(__englishUS,true);
                            
                                public static final Language _estonian =
                                    new Language(__estonian,true);
                            
                                public static final Language _filipino =
                                    new Language(__filipino,true);
                            
                                public static final Language _finnish =
                                    new Language(__finnish,true);
                            
                                public static final Language _frenchCanada =
                                    new Language(__frenchCanada,true);
                            
                                public static final Language _frenchFrance =
                                    new Language(__frenchFrance,true);
                            
                                public static final Language _german =
                                    new Language(__german,true);
                            
                                public static final Language _greek =
                                    new Language(__greek,true);
                            
                                public static final Language _gujarati =
                                    new Language(__gujarati,true);
                            
                                public static final Language _haitian =
                                    new Language(__haitian,true);
                            
                                public static final Language _hebrew =
                                    new Language(__hebrew,true);
                            
                                public static final Language _hindi =
                                    new Language(__hindi,true);
                            
                                public static final Language _hungarian =
                                    new Language(__hungarian,true);
                            
                                public static final Language _icelandic =
                                    new Language(__icelandic,true);
                            
                                public static final Language _indonesian =
                                    new Language(__indonesian,true);
                            
                                public static final Language _italian =
                                    new Language(__italian,true);
                            
                                public static final Language _japanese =
                                    new Language(__japanese,true);
                            
                                public static final Language _kannada =
                                    new Language(__kannada,true);
                            
                                public static final Language _korean =
                                    new Language(__korean,true);
                            
                                public static final Language _latinAmericanSpanish =
                                    new Language(__latinAmericanSpanish,true);
                            
                                public static final Language _latvian =
                                    new Language(__latvian,true);
                            
                                public static final Language _lithuanian =
                                    new Language(__lithuanian,true);
                            
                                public static final Language _luxembourgish =
                                    new Language(__luxembourgish,true);
                            
                                public static final Language _malay =
                                    new Language(__malay,true);
                            
                                public static final Language _marathi =
                                    new Language(__marathi,true);
                            
                                public static final Language _norwegian =
                                    new Language(__norwegian,true);
                            
                                public static final Language _persianIran =
                                    new Language(__persianIran,true);
                            
                                public static final Language _polish =
                                    new Language(__polish,true);
                            
                                public static final Language _portugueseBrazil =
                                    new Language(__portugueseBrazil,true);
                            
                                public static final Language _portuguesePortugal =
                                    new Language(__portuguesePortugal,true);
                            
                                public static final Language _punjabi =
                                    new Language(__punjabi,true);
                            
                                public static final Language _romanian =
                                    new Language(__romanian,true);
                            
                                public static final Language _russian =
                                    new Language(__russian,true);
                            
                                public static final Language _serbianCyrillic =
                                    new Language(__serbianCyrillic,true);
                            
                                public static final Language _serbianLatin =
                                    new Language(__serbianLatin,true);
                            
                                public static final Language _slovak =
                                    new Language(__slovak,true);
                            
                                public static final Language _slovenian =
                                    new Language(__slovenian,true);
                            
                                public static final Language _spanish =
                                    new Language(__spanish,true);
                            
                                public static final Language _swedish =
                                    new Language(__swedish,true);
                            
                                public static final Language _tamil =
                                    new Language(__tamil,true);
                            
                                public static final Language _telugu =
                                    new Language(__telugu,true);
                            
                                public static final Language _thai =
                                    new Language(__thai,true);
                            
                                public static final Language _turkish =
                                    new Language(__turkish,true);
                            
                                public static final Language _ukrainian =
                                    new Language(__ukrainian,true);
                            
                                public static final Language _vietnamese =
                                    new Language(__vietnamese,true);
                            

                                public java.lang.String getValue() { return localLanguage;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localLanguage.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.common_2017_2.platform.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":Language",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "Language",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localLanguage==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("Language cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localLanguage);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.common_2017_2.platform.webservices.netsuite.com")){
                return "ns6";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLanguage)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static Language fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    Language enumeration = (Language)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static Language fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static Language fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return Language.Factory.fromString(content,namespaceUri);
                    } else {
                       return Language.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static Language parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            Language object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"Language" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = Language.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = Language.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    