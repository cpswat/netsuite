
/**
 * TaskSearchBasic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.common_2017_2;
            

            /**
            *  TaskSearchBasic bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class TaskSearchBasic extends com.netsuite.webservices.platform.core_2017_2.SearchRecordBasic
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = TaskSearchBasic
                Namespace URI = urn:common_2017_2.platform.webservices.netsuite.com
                Namespace Prefix = ns7
                */
            

                        /**
                        * field for ActualTime
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localActualTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localActualTimeTracker = false ;

                           public boolean isActualTimeSpecified(){
                               return localActualTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getActualTime(){
                               return localActualTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ActualTime
                               */
                               public void setActualTime(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localActualTimeTracker = param != null;
                                   
                                            this.localActualTime=param;
                                    

                               }
                            

                        /**
                        * field for Assigned
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localAssigned ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAssignedTracker = false ;

                           public boolean isAssignedSpecified(){
                               return localAssignedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getAssigned(){
                               return localAssigned;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Assigned
                               */
                               public void setAssigned(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localAssignedTracker = param != null;
                                   
                                            this.localAssigned=param;
                                    

                               }
                            

                        /**
                        * field for Company
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localCompany ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCompanyTracker = false ;

                           public boolean isCompanySpecified(){
                               return localCompanyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getCompany(){
                               return localCompany;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Company
                               */
                               public void setCompany(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localCompanyTracker = param != null;
                                   
                                            this.localCompany=param;
                                    

                               }
                            

                        /**
                        * field for CompletedDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localCompletedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCompletedDateTracker = false ;

                           public boolean isCompletedDateSpecified(){
                               return localCompletedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getCompletedDate(){
                               return localCompletedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CompletedDate
                               */
                               public void setCompletedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localCompletedDateTracker = param != null;
                                   
                                            this.localCompletedDate=param;
                                    

                               }
                            

                        /**
                        * field for Contact
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localContact ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContactTracker = false ;

                           public boolean isContactSpecified(){
                               return localContactTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getContact(){
                               return localContact;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Contact
                               */
                               public void setContact(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localContactTracker = param != null;
                                   
                                            this.localContact=param;
                                    

                               }
                            

                        /**
                        * field for CreatedDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localCreatedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedDateTracker = false ;

                           public boolean isCreatedDateSpecified(){
                               return localCreatedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getCreatedDate(){
                               return localCreatedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedDate
                               */
                               public void setCreatedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localCreatedDateTracker = param != null;
                                   
                                            this.localCreatedDate=param;
                                    

                               }
                            

                        /**
                        * field for EndDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndDateTracker = false ;

                           public boolean isEndDateSpecified(){
                               return localEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getEndDate(){
                               return localEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndDate
                               */
                               public void setEndDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localEndDateTracker = param != null;
                                   
                                            this.localEndDate=param;
                                    

                               }
                            

                        /**
                        * field for EstimatedTime
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localEstimatedTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstimatedTimeTracker = false ;

                           public boolean isEstimatedTimeSpecified(){
                               return localEstimatedTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getEstimatedTime(){
                               return localEstimatedTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstimatedTime
                               */
                               public void setEstimatedTime(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localEstimatedTimeTracker = param != null;
                                   
                                            this.localEstimatedTime=param;
                                    

                               }
                            

                        /**
                        * field for EstimatedTimeOverride
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localEstimatedTimeOverride ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstimatedTimeOverrideTracker = false ;

                           public boolean isEstimatedTimeOverrideSpecified(){
                               return localEstimatedTimeOverrideTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getEstimatedTimeOverride(){
                               return localEstimatedTimeOverride;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstimatedTimeOverride
                               */
                               public void setEstimatedTimeOverride(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localEstimatedTimeOverrideTracker = param != null;
                                   
                                            this.localEstimatedTimeOverride=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localExternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdTracker = false ;

                           public boolean isExternalIdSpecified(){
                               return localExternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localExternalIdTracker = param != null;
                                   
                                            this.localExternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalIdString
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localExternalIdString ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExternalIdStringTracker = false ;

                           public boolean isExternalIdStringSpecified(){
                               return localExternalIdStringTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getExternalIdString(){
                               return localExternalIdString;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalIdString
                               */
                               public void setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localExternalIdStringTracker = param != null;
                                   
                                            this.localExternalIdString=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localInternalId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdTracker = false ;

                           public boolean isInternalIdSpecified(){
                               return localInternalIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localInternalIdTracker = param != null;
                                   
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for InternalIdNumber
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localInternalIdNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInternalIdNumberTracker = false ;

                           public boolean isInternalIdNumberSpecified(){
                               return localInternalIdNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getInternalIdNumber(){
                               return localInternalIdNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalIdNumber
                               */
                               public void setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localInternalIdNumberTracker = param != null;
                                   
                                            this.localInternalIdNumber=param;
                                    

                               }
                            

                        /**
                        * field for IsJobSummaryTask
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsJobSummaryTask ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsJobSummaryTaskTracker = false ;

                           public boolean isIsJobSummaryTaskSpecified(){
                               return localIsJobSummaryTaskTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsJobSummaryTask(){
                               return localIsJobSummaryTask;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsJobSummaryTask
                               */
                               public void setIsJobSummaryTask(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsJobSummaryTaskTracker = param != null;
                                   
                                            this.localIsJobSummaryTask=param;
                                    

                               }
                            

                        /**
                        * field for IsJobTask
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsJobTask ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsJobTaskTracker = false ;

                           public boolean isIsJobTaskSpecified(){
                               return localIsJobTaskTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsJobTask(){
                               return localIsJobTask;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsJobTask
                               */
                               public void setIsJobTask(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsJobTaskTracker = param != null;
                                   
                                            this.localIsJobTask=param;
                                    

                               }
                            

                        /**
                        * field for IsPrivate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchBooleanField localIsPrivate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsPrivateTracker = false ;

                           public boolean isIsPrivateSpecified(){
                               return localIsPrivateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchBooleanField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchBooleanField getIsPrivate(){
                               return localIsPrivate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsPrivate
                               */
                               public void setIsPrivate(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField param){
                            localIsPrivateTracker = param != null;
                                   
                                            this.localIsPrivate=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for Milestone
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localMilestone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMilestoneTracker = false ;

                           public boolean isMilestoneSpecified(){
                               return localMilestoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getMilestone(){
                               return localMilestone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Milestone
                               */
                               public void setMilestone(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localMilestoneTracker = param != null;
                                   
                                            this.localMilestone=param;
                                    

                               }
                            

                        /**
                        * field for Owner
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField localOwner ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOwnerTracker = false ;

                           public boolean isOwnerSpecified(){
                               return localOwnerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField getOwner(){
                               return localOwner;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Owner
                               */
                               public void setOwner(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField param){
                            localOwnerTracker = param != null;
                                   
                                            this.localOwner=param;
                                    

                               }
                            

                        /**
                        * field for PercentComplete
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localPercentComplete ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPercentCompleteTracker = false ;

                           public boolean isPercentCompleteSpecified(){
                               return localPercentCompleteTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getPercentComplete(){
                               return localPercentComplete;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PercentComplete
                               */
                               public void setPercentComplete(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localPercentCompleteTracker = param != null;
                                   
                                            this.localPercentComplete=param;
                                    

                               }
                            

                        /**
                        * field for PercentTimeComplete
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchLongField localPercentTimeComplete ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPercentTimeCompleteTracker = false ;

                           public boolean isPercentTimeCompleteSpecified(){
                               return localPercentTimeCompleteTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchLongField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchLongField getPercentTimeComplete(){
                               return localPercentTimeComplete;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PercentTimeComplete
                               */
                               public void setPercentTimeComplete(com.netsuite.webservices.platform.core_2017_2.SearchLongField param){
                            localPercentTimeCompleteTracker = param != null;
                                   
                                            this.localPercentTimeComplete=param;
                                    

                               }
                            

                        /**
                        * field for Priority
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localPriority ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPriorityTracker = false ;

                           public boolean isPrioritySpecified(){
                               return localPriorityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getPriority(){
                               return localPriority;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Priority
                               */
                               public void setPriority(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localPriorityTracker = param != null;
                                   
                                            this.localPriority=param;
                                    

                               }
                            

                        /**
                        * field for StartDate
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDateField localStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateTracker = false ;

                           public boolean isStartDateSpecified(){
                               return localStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDateField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDateField getStartDate(){
                               return localStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDate
                               */
                               public void setStartDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField param){
                            localStartDateTracker = param != null;
                                   
                                            this.localStartDate=param;
                                    

                               }
                            

                        /**
                        * field for Status
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField localStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStatusTracker = false ;

                           public boolean isStatusSpecified(){
                               return localStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField getStatus(){
                               return localStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Status
                               */
                               public void setStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField param){
                            localStatusTracker = param != null;
                                   
                                            this.localStatus=param;
                                    

                               }
                            

                        /**
                        * field for TimeRemaining
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchDoubleField localTimeRemaining ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeRemainingTracker = false ;

                           public boolean isTimeRemainingSpecified(){
                               return localTimeRemainingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchDoubleField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchDoubleField getTimeRemaining(){
                               return localTimeRemaining;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimeRemaining
                               */
                               public void setTimeRemaining(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField param){
                            localTimeRemainingTracker = param != null;
                                   
                                            this.localTimeRemaining=param;
                                    

                               }
                            

                        /**
                        * field for Title
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchStringField localTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTitleTracker = false ;

                           public boolean isTitleSpecified(){
                               return localTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchStringField
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchStringField getTitle(){
                               return localTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Title
                               */
                               public void setTitle(com.netsuite.webservices.platform.core_2017_2.SearchStringField param){
                            localTitleTracker = param != null;
                                   
                                            this.localTitle=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:common_2017_2.platform.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":TaskSearchBasic",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "TaskSearchBasic",
                           xmlWriter);
                   }

                if (localActualTimeTracker){
                                            if (localActualTime==null){
                                                 throw new org.apache.axis2.databinding.ADBException("actualTime cannot be null!!");
                                            }
                                           localActualTime.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","actualTime"),
                                               xmlWriter);
                                        } if (localAssignedTracker){
                                            if (localAssigned==null){
                                                 throw new org.apache.axis2.databinding.ADBException("assigned cannot be null!!");
                                            }
                                           localAssigned.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","assigned"),
                                               xmlWriter);
                                        } if (localCompanyTracker){
                                            if (localCompany==null){
                                                 throw new org.apache.axis2.databinding.ADBException("company cannot be null!!");
                                            }
                                           localCompany.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","company"),
                                               xmlWriter);
                                        } if (localCompletedDateTracker){
                                            if (localCompletedDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("completedDate cannot be null!!");
                                            }
                                           localCompletedDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","completedDate"),
                                               xmlWriter);
                                        } if (localContactTracker){
                                            if (localContact==null){
                                                 throw new org.apache.axis2.databinding.ADBException("contact cannot be null!!");
                                            }
                                           localContact.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","contact"),
                                               xmlWriter);
                                        } if (localCreatedDateTracker){
                                            if (localCreatedDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                            }
                                           localCreatedDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","createdDate"),
                                               xmlWriter);
                                        } if (localEndDateTracker){
                                            if (localEndDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                            }
                                           localEndDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endDate"),
                                               xmlWriter);
                                        } if (localEstimatedTimeTracker){
                                            if (localEstimatedTime==null){
                                                 throw new org.apache.axis2.databinding.ADBException("estimatedTime cannot be null!!");
                                            }
                                           localEstimatedTime.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedTime"),
                                               xmlWriter);
                                        } if (localEstimatedTimeOverrideTracker){
                                            if (localEstimatedTimeOverride==null){
                                                 throw new org.apache.axis2.databinding.ADBException("estimatedTimeOverride cannot be null!!");
                                            }
                                           localEstimatedTimeOverride.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedTimeOverride"),
                                               xmlWriter);
                                        } if (localExternalIdTracker){
                                            if (localExternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                            }
                                           localExternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId"),
                                               xmlWriter);
                                        } if (localExternalIdStringTracker){
                                            if (localExternalIdString==null){
                                                 throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                            }
                                           localExternalIdString.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString"),
                                               xmlWriter);
                                        } if (localInternalIdTracker){
                                            if (localInternalId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                            }
                                           localInternalId.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId"),
                                               xmlWriter);
                                        } if (localInternalIdNumberTracker){
                                            if (localInternalIdNumber==null){
                                                 throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                            }
                                           localInternalIdNumber.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber"),
                                               xmlWriter);
                                        } if (localIsJobSummaryTaskTracker){
                                            if (localIsJobSummaryTask==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isJobSummaryTask cannot be null!!");
                                            }
                                           localIsJobSummaryTask.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isJobSummaryTask"),
                                               xmlWriter);
                                        } if (localIsJobTaskTracker){
                                            if (localIsJobTask==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isJobTask cannot be null!!");
                                            }
                                           localIsJobTask.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isJobTask"),
                                               xmlWriter);
                                        } if (localIsPrivateTracker){
                                            if (localIsPrivate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("isPrivate cannot be null!!");
                                            }
                                           localIsPrivate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isPrivate"),
                                               xmlWriter);
                                        } if (localLastModifiedDateTracker){
                                            if (localLastModifiedDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                            }
                                           localLastModifiedDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate"),
                                               xmlWriter);
                                        } if (localMilestoneTracker){
                                            if (localMilestone==null){
                                                 throw new org.apache.axis2.databinding.ADBException("milestone cannot be null!!");
                                            }
                                           localMilestone.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","milestone"),
                                               xmlWriter);
                                        } if (localOwnerTracker){
                                            if (localOwner==null){
                                                 throw new org.apache.axis2.databinding.ADBException("owner cannot be null!!");
                                            }
                                           localOwner.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","owner"),
                                               xmlWriter);
                                        } if (localPercentCompleteTracker){
                                            if (localPercentComplete==null){
                                                 throw new org.apache.axis2.databinding.ADBException("percentComplete cannot be null!!");
                                            }
                                           localPercentComplete.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","percentComplete"),
                                               xmlWriter);
                                        } if (localPercentTimeCompleteTracker){
                                            if (localPercentTimeComplete==null){
                                                 throw new org.apache.axis2.databinding.ADBException("percentTimeComplete cannot be null!!");
                                            }
                                           localPercentTimeComplete.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","percentTimeComplete"),
                                               xmlWriter);
                                        } if (localPriorityTracker){
                                            if (localPriority==null){
                                                 throw new org.apache.axis2.databinding.ADBException("priority cannot be null!!");
                                            }
                                           localPriority.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","priority"),
                                               xmlWriter);
                                        } if (localStartDateTracker){
                                            if (localStartDate==null){
                                                 throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                            }
                                           localStartDate.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDate"),
                                               xmlWriter);
                                        } if (localStatusTracker){
                                            if (localStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                            }
                                           localStatus.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","status"),
                                               xmlWriter);
                                        } if (localTimeRemainingTracker){
                                            if (localTimeRemaining==null){
                                                 throw new org.apache.axis2.databinding.ADBException("timeRemaining cannot be null!!");
                                            }
                                           localTimeRemaining.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","timeRemaining"),
                                               xmlWriter);
                                        } if (localTitleTracker){
                                            if (localTitle==null){
                                                 throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                            }
                                           localTitle.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","title"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:common_2017_2.platform.webservices.netsuite.com")){
                return "ns7";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","TaskSearchBasic"));
                 if (localActualTimeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "actualTime"));
                            
                            
                                    if (localActualTime==null){
                                         throw new org.apache.axis2.databinding.ADBException("actualTime cannot be null!!");
                                    }
                                    elementList.add(localActualTime);
                                } if (localAssignedTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "assigned"));
                            
                            
                                    if (localAssigned==null){
                                         throw new org.apache.axis2.databinding.ADBException("assigned cannot be null!!");
                                    }
                                    elementList.add(localAssigned);
                                } if (localCompanyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "company"));
                            
                            
                                    if (localCompany==null){
                                         throw new org.apache.axis2.databinding.ADBException("company cannot be null!!");
                                    }
                                    elementList.add(localCompany);
                                } if (localCompletedDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "completedDate"));
                            
                            
                                    if (localCompletedDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("completedDate cannot be null!!");
                                    }
                                    elementList.add(localCompletedDate);
                                } if (localContactTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "contact"));
                            
                            
                                    if (localContact==null){
                                         throw new org.apache.axis2.databinding.ADBException("contact cannot be null!!");
                                    }
                                    elementList.add(localContact);
                                } if (localCreatedDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "createdDate"));
                            
                            
                                    if (localCreatedDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                    }
                                    elementList.add(localCreatedDate);
                                } if (localEndDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "endDate"));
                            
                            
                                    if (localEndDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                    }
                                    elementList.add(localEndDate);
                                } if (localEstimatedTimeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "estimatedTime"));
                            
                            
                                    if (localEstimatedTime==null){
                                         throw new org.apache.axis2.databinding.ADBException("estimatedTime cannot be null!!");
                                    }
                                    elementList.add(localEstimatedTime);
                                } if (localEstimatedTimeOverrideTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "estimatedTimeOverride"));
                            
                            
                                    if (localEstimatedTimeOverride==null){
                                         throw new org.apache.axis2.databinding.ADBException("estimatedTimeOverride cannot be null!!");
                                    }
                                    elementList.add(localEstimatedTimeOverride);
                                } if (localExternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalId"));
                            
                            
                                    if (localExternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalId cannot be null!!");
                                    }
                                    elementList.add(localExternalId);
                                } if (localExternalIdStringTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "externalIdString"));
                            
                            
                                    if (localExternalIdString==null){
                                         throw new org.apache.axis2.databinding.ADBException("externalIdString cannot be null!!");
                                    }
                                    elementList.add(localExternalIdString);
                                } if (localInternalIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalId"));
                            
                            
                                    if (localInternalId==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalId cannot be null!!");
                                    }
                                    elementList.add(localInternalId);
                                } if (localInternalIdNumberTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "internalIdNumber"));
                            
                            
                                    if (localInternalIdNumber==null){
                                         throw new org.apache.axis2.databinding.ADBException("internalIdNumber cannot be null!!");
                                    }
                                    elementList.add(localInternalIdNumber);
                                } if (localIsJobSummaryTaskTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isJobSummaryTask"));
                            
                            
                                    if (localIsJobSummaryTask==null){
                                         throw new org.apache.axis2.databinding.ADBException("isJobSummaryTask cannot be null!!");
                                    }
                                    elementList.add(localIsJobSummaryTask);
                                } if (localIsJobTaskTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isJobTask"));
                            
                            
                                    if (localIsJobTask==null){
                                         throw new org.apache.axis2.databinding.ADBException("isJobTask cannot be null!!");
                                    }
                                    elementList.add(localIsJobTask);
                                } if (localIsPrivateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "isPrivate"));
                            
                            
                                    if (localIsPrivate==null){
                                         throw new org.apache.axis2.databinding.ADBException("isPrivate cannot be null!!");
                                    }
                                    elementList.add(localIsPrivate);
                                } if (localLastModifiedDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                            
                            
                                    if (localLastModifiedDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                    }
                                    elementList.add(localLastModifiedDate);
                                } if (localMilestoneTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "milestone"));
                            
                            
                                    if (localMilestone==null){
                                         throw new org.apache.axis2.databinding.ADBException("milestone cannot be null!!");
                                    }
                                    elementList.add(localMilestone);
                                } if (localOwnerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "owner"));
                            
                            
                                    if (localOwner==null){
                                         throw new org.apache.axis2.databinding.ADBException("owner cannot be null!!");
                                    }
                                    elementList.add(localOwner);
                                } if (localPercentCompleteTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "percentComplete"));
                            
                            
                                    if (localPercentComplete==null){
                                         throw new org.apache.axis2.databinding.ADBException("percentComplete cannot be null!!");
                                    }
                                    elementList.add(localPercentComplete);
                                } if (localPercentTimeCompleteTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "percentTimeComplete"));
                            
                            
                                    if (localPercentTimeComplete==null){
                                         throw new org.apache.axis2.databinding.ADBException("percentTimeComplete cannot be null!!");
                                    }
                                    elementList.add(localPercentTimeComplete);
                                } if (localPriorityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "priority"));
                            
                            
                                    if (localPriority==null){
                                         throw new org.apache.axis2.databinding.ADBException("priority cannot be null!!");
                                    }
                                    elementList.add(localPriority);
                                } if (localStartDateTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "startDate"));
                            
                            
                                    if (localStartDate==null){
                                         throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                    }
                                    elementList.add(localStartDate);
                                } if (localStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "status"));
                            
                            
                                    if (localStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                    }
                                    elementList.add(localStatus);
                                } if (localTimeRemainingTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "timeRemaining"));
                            
                            
                                    if (localTimeRemaining==null){
                                         throw new org.apache.axis2.databinding.ADBException("timeRemaining cannot be null!!");
                                    }
                                    elementList.add(localTimeRemaining);
                                } if (localTitleTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "title"));
                            
                            
                                    if (localTitle==null){
                                         throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                    }
                                    elementList.add(localTitle);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static TaskSearchBasic parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            TaskSearchBasic object =
                new TaskSearchBasic();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"TaskSearchBasic".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (TaskSearchBasic)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","actualTime").equals(reader.getName())){
                                
                                                object.setActualTime(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","assigned").equals(reader.getName())){
                                
                                                object.setAssigned(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","company").equals(reader.getName())){
                                
                                                object.setCompany(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","completedDate").equals(reader.getName())){
                                
                                                object.setCompletedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","contact").equals(reader.getName())){
                                
                                                object.setContact(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","createdDate").equals(reader.getName())){
                                
                                                object.setCreatedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","endDate").equals(reader.getName())){
                                
                                                object.setEndDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedTime").equals(reader.getName())){
                                
                                                object.setEstimatedTime(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","estimatedTimeOverride").equals(reader.getName())){
                                
                                                object.setEstimatedTimeOverride(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalId").equals(reader.getName())){
                                
                                                object.setExternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","externalIdString").equals(reader.getName())){
                                
                                                object.setExternalIdString(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalId").equals(reader.getName())){
                                
                                                object.setInternalId(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","internalIdNumber").equals(reader.getName())){
                                
                                                object.setInternalIdNumber(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isJobSummaryTask").equals(reader.getName())){
                                
                                                object.setIsJobSummaryTask(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isJobTask").equals(reader.getName())){
                                
                                                object.setIsJobTask(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","isPrivate").equals(reader.getName())){
                                
                                                object.setIsPrivate(com.netsuite.webservices.platform.core_2017_2.SearchBooleanField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                                object.setLastModifiedDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","milestone").equals(reader.getName())){
                                
                                                object.setMilestone(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","owner").equals(reader.getName())){
                                
                                                object.setOwner(com.netsuite.webservices.platform.core_2017_2.SearchMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","percentComplete").equals(reader.getName())){
                                
                                                object.setPercentComplete(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","percentTimeComplete").equals(reader.getName())){
                                
                                                object.setPercentTimeComplete(com.netsuite.webservices.platform.core_2017_2.SearchLongField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","priority").equals(reader.getName())){
                                
                                                object.setPriority(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","startDate").equals(reader.getName())){
                                
                                                object.setStartDate(com.netsuite.webservices.platform.core_2017_2.SearchDateField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","status").equals(reader.getName())){
                                
                                                object.setStatus(com.netsuite.webservices.platform.core_2017_2.SearchEnumMultiSelectField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","timeRemaining").equals(reader.getName())){
                                
                                                object.setTimeRemaining(com.netsuite.webservices.platform.core_2017_2.SearchDoubleField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","title").equals(reader.getName())){
                                
                                                object.setTitle(com.netsuite.webservices.platform.core_2017_2.SearchStringField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:common_2017_2.platform.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.SearchCustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    