
/**
 * StatusDetailCodeType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.faults_2017_2.types;
            

            /**
            *  StatusDetailCodeType bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class StatusDetailCodeType
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.faults_2017_2.platform.webservices.netsuite.com",
                "StatusDetailCodeType",
                "ns2");

            

                        /**
                        * field for StatusDetailCodeType
                        */

                        
                                    protected java.lang.String localStatusDetailCodeType ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected StatusDetailCodeType(java.lang.String value, boolean isRegisterValue) {
                                    localStatusDetailCodeType = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localStatusDetailCodeType, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String _ABORT_SEARCH_EXCEEDED_MAX_TIME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ABORT_SEARCH_EXCEEDED_MAX_TIME");
                                
                                    public static final java.lang.String _ABORT_UPLOAD_VIRUS_DETECTED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ABORT_UPLOAD_VIRUS_DETECTED");
                                
                                    public static final java.lang.String _ACCESS_DENIED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACCESS_DENIED");
                                
                                    public static final java.lang.String _ACCTNG_PRD_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACCTNG_PRD_REQD");
                                
                                    public static final java.lang.String _ACCT_DISABLED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACCT_DISABLED");
                                
                                    public static final java.lang.String _ACCT_MERGE_DUP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACCT_MERGE_DUP");
                                
                                    public static final java.lang.String _ACCT_NAME_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACCT_NAME_REQD");
                                
                                    public static final java.lang.String _ACCT_NEEDS_CAMPAIGN_PROVISION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACCT_NEEDS_CAMPAIGN_PROVISION");
                                
                                    public static final java.lang.String _ACCT_NOT_CREATED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACCT_NOT_CREATED");
                                
                                    public static final java.lang.String _ACCT_NUMS_REQD_OR_DONT_MATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACCT_NUMS_REQD_OR_DONT_MATCH");
                                
                                    public static final java.lang.String _ACCT_NUM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACCT_NUM_REQD");
                                
                                    public static final java.lang.String _ACCT_PERIOD_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACCT_PERIOD_SETUP_REQD");
                                
                                    public static final java.lang.String _ACCT_PRDS_BEING_ADDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACCT_PRDS_BEING_ADDED");
                                
                                    public static final java.lang.String _ACCT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACCT_REQD");
                                
                                    public static final java.lang.String _ACCT_TEMP_DISABLED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACCT_TEMP_DISABLED");
                                
                                    public static final java.lang.String _ACCT_TEMP_UNAVAILABLE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACCT_TEMP_UNAVAILABLE");
                                
                                    public static final java.lang.String _ACH_NOT_AVAILBL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACH_NOT_AVAILBL");
                                
                                    public static final java.lang.String _ACH_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACH_SETUP_REQD");
                                
                                    public static final java.lang.String _ACTIVE_AP_ACCT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACTIVE_AP_ACCT_REQD");
                                
                                    public static final java.lang.String _ACTIVE_ROLE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACTIVE_ROLE_REQD");
                                
                                    public static final java.lang.String _ACTIVE_TRANS_EXIST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACTIVE_TRANS_EXIST");
                                
                                    public static final java.lang.String _ADDITIONAL_AUTHENTICATION_REQUIRED_2FA =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ADDITIONAL_AUTHENTICATION_REQUIRED_2FA");
                                
                                    public static final java.lang.String _ADDITIONAL_AUTHENTICATION_REQUIRED_SQ =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ADDITIONAL_AUTHENTICATION_REQUIRED_SQ");
                                
                                    public static final java.lang.String _ADDRESS_LINE_1_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ADDRESS_LINE_1_REQD");
                                
                                    public static final java.lang.String _ADMIN_ACCESS_REQ =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ADMIN_ACCESS_REQ");
                                
                                    public static final java.lang.String _ADMIN_ACCESS_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ADMIN_ACCESS_REQD");
                                
                                    public static final java.lang.String _ADMIN_ONLY_ACCESS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ADMIN_ONLY_ACCESS");
                                
                                    public static final java.lang.String _ADMIN_USER_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ADMIN_USER_REQD");
                                
                                    public static final java.lang.String _ADMISSIBILITY_PACKG_TYP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ADMISSIBILITY_PACKG_TYP_REQD");
                                
                                    public static final java.lang.String _ALL_DATA_DELETE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ALL_DATA_DELETE_REQD");
                                
                                    public static final java.lang.String _ALL_MTRX_SUBITMES_OPTNS_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ALL_MTRX_SUBITMES_OPTNS_REQD");
                                
                                    public static final java.lang.String _ALREADY_IN_INVT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ALREADY_IN_INVT");
                                
                                    public static final java.lang.String _AMORTZN_INVALID_DATE_RANGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("AMORTZN_INVALID_DATE_RANGE");
                                
                                    public static final java.lang.String _AMORTZN_TMPLT_DATA_MISSING =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("AMORTZN_TMPLT_DATA_MISSING");
                                
                                    public static final java.lang.String _AMT_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("AMT_DISALLWD");
                                
                                    public static final java.lang.String _AMT_EXCEEDS_APPROVAL_LIMIT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("AMT_EXCEEDS_APPROVAL_LIMIT");
                                
                                    public static final java.lang.String _ANSWER_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ANSWER_REQD");
                                
                                    public static final java.lang.String _APPROVAL_PERMS_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("APPROVAL_PERMS_REQD");
                                
                                    public static final java.lang.String _AREA_CODE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("AREA_CODE_REQD");
                                
                                    public static final java.lang.String _ASSIGNEE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ASSIGNEE_REQD");
                                
                                    public static final java.lang.String _ATTACHMNT_CONTAINS_VIRUS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ATTACHMNT_CONTAINS_VIRUS");
                                
                                    public static final java.lang.String _ATTACH_SIZE_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ATTACH_SIZE_EXCEEDED");
                                
                                    public static final java.lang.String _AT_LEAST_ONE_FILE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("AT_LEAST_ONE_FILE_REQD");
                                
                                    public static final java.lang.String _AT_LEAST_ONE_PACKAGE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("AT_LEAST_ONE_PACKAGE_REQD");
                                
                                    public static final java.lang.String _AT_LEAST_ONE_RETURN_FLD_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("AT_LEAST_ONE_RETURN_FLD_REQD");
                                
                                    public static final java.lang.String _AT_LEAST_ONE_SUB_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("AT_LEAST_ONE_SUB_REQD");
                                
                                    public static final java.lang.String _AUDIT_W2_1099 =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("AUDIT_W2_1099");
                                
                                    public static final java.lang.String _AUTO_NUM_UPDATE_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("AUTO_NUM_UPDATE_DISALLWD");
                                
                                    public static final java.lang.String _AVS_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("AVS_ERROR");
                                
                                    public static final java.lang.String _BALANCE_EXCEEDS_CREDIT_LIMIT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("BALANCE_EXCEEDS_CREDIT_LIMIT");
                                
                                    public static final java.lang.String _BANK_ACCT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("BANK_ACCT_REQD");
                                
                                    public static final java.lang.String _BASE_CRNCY_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("BASE_CRNCY_REQD");
                                
                                    public static final java.lang.String _BILLABLES_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("BILLABLES_DISALLWD");
                                
                                    public static final java.lang.String _BILLING_ISSUES =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("BILLING_ISSUES");
                                
                                    public static final java.lang.String _BILLING_SCHDUL_INVALID_RECURR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("BILLING_SCHDUL_INVALID_RECURR");
                                
                                    public static final java.lang.String _BILLPAY_APPROVAL_UNAVAILBL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("BILLPAY_APPROVAL_UNAVAILBL");
                                
                                    public static final java.lang.String _BILLPAY_REGSTRTN_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("BILLPAY_REGSTRTN_REQD");
                                
                                    public static final java.lang.String _BILLPAY_SRVC_UNAVAILBL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("BILLPAY_SRVC_UNAVAILBL");
                                
                                    public static final java.lang.String _BILL_PAY_STATUS_UNAVAILABLE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("BILL_PAY_STATUS_UNAVAILABLE");
                                
                                    public static final java.lang.String _BILL_PMTS_MADE_FROM_ACCT_ONLY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("BILL_PMTS_MADE_FROM_ACCT_ONLY");
                                
                                    public static final java.lang.String _BIN_DSNT_CONTAIN_ENOUGH_ITEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("BIN_DSNT_CONTAIN_ENOUGH_ITEM");
                                
                                    public static final java.lang.String _BIN_ITEM_UNAVAILBL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("BIN_ITEM_UNAVAILBL");
                                
                                    public static final java.lang.String _BIN_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("BIN_SETUP_REQD");
                                
                                    public static final java.lang.String _BIN_UNDEFND =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("BIN_UNDEFND");
                                
                                    public static final java.lang.String _BUNDLE_IS_DEPRECATED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("BUNDLE_IS_DEPRECATED");
                                
                                    public static final java.lang.String _CALENDAR_PREFS_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CALENDAR_PREFS_REQD");
                                
                                    public static final java.lang.String _CAMPAGIN_ALREADY_EXECUTED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CAMPAGIN_ALREADY_EXECUTED");
                                
                                    public static final java.lang.String _CAMPAIGN_IN_USE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CAMPAIGN_IN_USE");
                                
                                    public static final java.lang.String _CAMPAIGN_SET_UP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CAMPAIGN_SET_UP_REQD");
                                
                                    public static final java.lang.String _CANNOT_RESET_PASSWORD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANNOT_RESET_PASSWORD");
                                
                                    public static final java.lang.String _CANT_APPLY_PMT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_APPLY_PMT");
                                
                                    public static final java.lang.String _CANT_AUTO_CREATE_ADJSTMNT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_AUTO_CREATE_ADJSTMNT");
                                
                                    public static final java.lang.String _CANT_CALC_FEDEX_RATES =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CALC_FEDEX_RATES");
                                
                                    public static final java.lang.String _CANT_CANCEL_APPRVD_RETRN_AUTH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CANCEL_APPRVD_RETRN_AUTH");
                                
                                    public static final java.lang.String _CANT_CANCEL_BILL_PMT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CANCEL_BILL_PMT");
                                
                                    public static final java.lang.String _CANT_CHANGE_COMMSSN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CHANGE_COMMSSN");
                                
                                    public static final java.lang.String _CANT_CHANGE_CONTACT_RESTRICTN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CHANGE_CONTACT_RESTRICTN");
                                
                                    public static final java.lang.String _CANT_CHANGE_CRMRECORDTYPELINKS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CHANGE_CRMRECORDTYPELINKS");
                                
                                    public static final java.lang.String _CANT_CHANGE_EVENT_PRIMARY_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CHANGE_EVENT_PRIMARY_TYP");
                                
                                    public static final java.lang.String _CANT_CHANGE_IP_ADDRESS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CHANGE_IP_ADDRESS");
                                
                                    public static final java.lang.String _CANT_CHANGE_LEAD_SOURCE_CAT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CHANGE_LEAD_SOURCE_CAT");
                                
                                    public static final java.lang.String _CANT_CHANGE_PSWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CHANGE_PSWD");
                                
                                    public static final java.lang.String _CANT_CHANGE_REV_REC_TMPLT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CHANGE_REV_REC_TMPLT");
                                
                                    public static final java.lang.String _CANT_CHANGE_SUB =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CHANGE_SUB");
                                
                                    public static final java.lang.String _CANT_CHANGE_TASK_LINK =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CHANGE_TASK_LINK");
                                
                                    public static final java.lang.String _CANT_CHANGE_UNITS_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CHANGE_UNITS_TYP");
                                
                                    public static final java.lang.String _CANT_CHANGE_VSOE_ALLOCTN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CHANGE_VSOE_ALLOCTN");
                                
                                    public static final java.lang.String _CANT_CHG_POSTED_BILL_VRNC =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CHG_POSTED_BILL_VRNC");
                                
                                    public static final java.lang.String _CANT_COMPLETE_FULFILL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_COMPLETE_FULFILL");
                                
                                    public static final java.lang.String _CANT_CONNECT_TO_STORE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CONNECT_TO_STORE");
                                
                                    public static final java.lang.String _CANT_CONVERT_CLASS_DEPT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CONVERT_CLASS_DEPT");
                                
                                    public static final java.lang.String _CANT_CONVERT_CLASS_LOC =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CONVERT_CLASS_LOC");
                                
                                    public static final java.lang.String _CANT_CONVERT_INVT_ITEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CONVERT_INVT_ITEM");
                                
                                    public static final java.lang.String _CANT_CREATE_FILES =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CREATE_FILES");
                                
                                    public static final java.lang.String _CANT_CREATE_NON_UNIQUE_RCRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CREATE_NON_UNIQUE_RCRD");
                                
                                    public static final java.lang.String _CANT_CREATE_PO =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CREATE_PO");
                                
                                    public static final java.lang.String _CANT_CREATE_SHIP_LABEL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CREATE_SHIP_LABEL");
                                
                                    public static final java.lang.String _CANT_CREATE_WORK_ORD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CREATE_WORK_ORD");
                                
                                    public static final java.lang.String _CANT_CREAT_SHIP_LABEL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_CREAT_SHIP_LABEL");
                                
                                    public static final java.lang.String _CANT_DELETE_ACCT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_ACCT");
                                
                                    public static final java.lang.String _CANT_DELETE_ACCT_PRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_ACCT_PRD");
                                
                                    public static final java.lang.String _CANT_DELETE_ALLOCTN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_ALLOCTN");
                                
                                    public static final java.lang.String _CANT_DELETE_BIN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_BIN");
                                
                                    public static final java.lang.String _CANT_DELETE_CATEGORY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_CATEGORY");
                                
                                    public static final java.lang.String _CANT_DELETE_CC_PROCESSOR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_CC_PROCESSOR");
                                
                                    public static final java.lang.String _CANT_DELETE_CELL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_CELL");
                                
                                    public static final java.lang.String _CANT_DELETE_CHILD_RCRDS_EXIST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_CHILD_RCRDS_EXIST");
                                
                                    public static final java.lang.String _CANT_DELETE_CHILD_RCRD_FOUND =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_CHILD_RCRD_FOUND");
                                
                                    public static final java.lang.String _CANT_DELETE_CLASS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_CLASS");
                                
                                    public static final java.lang.String _CANT_DELETE_COLOR_THEME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_COLOR_THEME");
                                
                                    public static final java.lang.String _CANT_DELETE_COMMSSN_SCHDUL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_COMMSSN_SCHDUL");
                                
                                    public static final java.lang.String _CANT_DELETE_COMPANY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_COMPANY");
                                
                                    public static final java.lang.String _CANT_DELETE_COMPANY_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_COMPANY_TYP");
                                
                                    public static final java.lang.String _CANT_DELETE_CONTACT_HAS_CHILD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_CONTACT_HAS_CHILD");
                                
                                    public static final java.lang.String _CANT_DELETE_CSTM_FIELD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_CSTM_FIELD");
                                
                                    public static final java.lang.String _CANT_DELETE_CSTM_FORM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_CSTM_FORM");
                                
                                    public static final java.lang.String _CANT_DELETE_CSTM_ITEM_FIELD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_CSTM_ITEM_FIELD");
                                
                                    public static final java.lang.String _CANT_DELETE_CSTM_LAYOUT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_CSTM_LAYOUT");
                                
                                    public static final java.lang.String _CANT_DELETE_CSTM_LIST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_CSTM_LIST");
                                
                                    public static final java.lang.String _CANT_DELETE_CSTM_RCRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_CSTM_RCRD");
                                
                                    public static final java.lang.String _CANT_DELETE_CSTM_RCRD_ENTRY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_CSTM_RCRD_ENTRY");
                                
                                    public static final java.lang.String _CANT_DELETE_CUST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_CUST");
                                
                                    public static final java.lang.String _CANT_DELETE_CUSTOMER =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_CUSTOMER");
                                
                                    public static final java.lang.String _CANT_DELETE_DEFAULT_FLDR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_DEFAULT_FLDR");
                                
                                    public static final java.lang.String _CANT_DELETE_DEFAULT_PRIORITY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_DEFAULT_PRIORITY");
                                
                                    public static final java.lang.String _CANT_DELETE_DEFAULT_SALES_REP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_DEFAULT_SALES_REP");
                                
                                    public static final java.lang.String _CANT_DELETE_DEFAULT_STATUS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_DEFAULT_STATUS");
                                
                                    public static final java.lang.String _CANT_DELETE_DEFAULT_VALUE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_DEFAULT_VALUE");
                                
                                    public static final java.lang.String _CANT_DELETE_DEFAULT_WEBSITE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_DEFAULT_WEBSITE");
                                
                                    public static final java.lang.String _CANT_DELETE_EMPL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_EMPL");
                                
                                    public static final java.lang.String _CANT_DELETE_ENTITY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_ENTITY");
                                
                                    public static final java.lang.String _CANT_DELETE_FIN_STATMNT_LAYOUT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_FIN_STATMNT_LAYOUT");
                                
                                    public static final java.lang.String _CANT_DELETE_FLDR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_FLDR");
                                
                                    public static final java.lang.String _CANT_DELETE_HAS_CHILD_ITEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_HAS_CHILD_ITEM");
                                
                                    public static final java.lang.String _CANT_DELETE_INFO_ITEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_INFO_ITEM");
                                
                                    public static final java.lang.String _CANT_DELETE_ITEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_ITEM");
                                
                                    public static final java.lang.String _CANT_DELETE_ITEM_LAYOUT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_ITEM_LAYOUT");
                                
                                    public static final java.lang.String _CANT_DELETE_ITEM_TMPLT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_ITEM_TMPLT");
                                
                                    public static final java.lang.String _CANT_DELETE_JOB_RESOURCE_ROLE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_JOB_RESOURCE_ROLE");
                                
                                    public static final java.lang.String _CANT_DELETE_LEGACY_CATEGORY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_LEGACY_CATEGORY");
                                
                                    public static final java.lang.String _CANT_DELETE_LINE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_LINE");
                                
                                    public static final java.lang.String _CANT_DELETE_MEDIA_ITEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_MEDIA_ITEM");
                                
                                    public static final java.lang.String _CANT_DELETE_MEMRZD_TRANS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_MEMRZD_TRANS");
                                
                                    public static final java.lang.String _CANT_DELETE_OR_CHANGE_ACCT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_OR_CHANGE_ACCT");
                                
                                    public static final java.lang.String _CANT_DELETE_PLAN_ASSGNMNT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_PLAN_ASSGNMNT");
                                
                                    public static final java.lang.String _CANT_DELETE_PRESNTN_CAT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_PRESNTN_CAT");
                                
                                    public static final java.lang.String _CANT_DELETE_RCRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_RCRD");
                                
                                    public static final java.lang.String _CANT_DELETE_RCRDS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_RCRDS");
                                
                                    public static final java.lang.String _CANT_DELETE_SITE_TAG =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_SITE_TAG");
                                
                                    public static final java.lang.String _CANT_DELETE_SITE_THEME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_SITE_THEME");
                                
                                    public static final java.lang.String _CANT_DELETE_SOLUTN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_SOLUTN");
                                
                                    public static final java.lang.String _CANT_DELETE_STATUS_TYPE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_STATUS_TYPE");
                                
                                    public static final java.lang.String _CANT_DELETE_SUBTAB =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_SUBTAB");
                                
                                    public static final java.lang.String _CANT_DELETE_SYSTEM_NOTE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_SYSTEM_NOTE");
                                
                                    public static final java.lang.String _CANT_DELETE_TAX_VENDOR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_TAX_VENDOR");
                                
                                    public static final java.lang.String _CANT_DELETE_TMPLT_RCRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_TMPLT_RCRD");
                                
                                    public static final java.lang.String _CANT_DELETE_TRANS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_TRANS");
                                
                                    public static final java.lang.String _CANT_DELETE_TRAN_LINE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_TRAN_LINE");
                                
                                    public static final java.lang.String _CANT_DELETE_TRAN_LINES =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_TRAN_LINES");
                                
                                    public static final java.lang.String _CANT_DELETE_UPDATE_ACCT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_UPDATE_ACCT");
                                
                                    public static final java.lang.String _CANT_DELETE_VENDOR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DELETE_VENDOR");
                                
                                    public static final java.lang.String _CANT_DEL_DEFAULT_CALENDAR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DEL_DEFAULT_CALENDAR");
                                
                                    public static final java.lang.String _CANT_DEL_DEFAULT_SHIP_METHOD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DEL_DEFAULT_SHIP_METHOD");
                                
                                    public static final java.lang.String _CANT_DEL_REALIZED_GAINLOSS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DEL_REALIZED_GAINLOSS");
                                
                                    public static final java.lang.String _CANT_DEL_TRANS_RVRSL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DEL_TRANS_RVRSL");
                                
                                    public static final java.lang.String _CANT_DIVIDE_BY_ZERO =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DIVIDE_BY_ZERO");
                                
                                    public static final java.lang.String _CANT_DOWNLOAD_EXPIRED_FILE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_DOWNLOAD_EXPIRED_FILE");
                                
                                    public static final java.lang.String _CANT_EDIT_CUST_LIST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_EDIT_CUST_LIST");
                                
                                    public static final java.lang.String _CANT_EDIT_CUST_PMT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_EDIT_CUST_PMT");
                                
                                    public static final java.lang.String _CANT_EDIT_DPLYMNT_IN_PROGRESS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_EDIT_DPLYMNT_IN_PROGRESS");
                                
                                    public static final java.lang.String _CANT_EDIT_FOLDER =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_EDIT_FOLDER");
                                
                                    public static final java.lang.String _CANT_EDIT_OLD_CASE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_EDIT_OLD_CASE");
                                
                                    public static final java.lang.String _CANT_EDIT_STANDARD_OBJ =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_EDIT_STANDARD_OBJ");
                                
                                    public static final java.lang.String _CANT_EDIT_TAGATA =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_EDIT_TAGATA");
                                
                                    public static final java.lang.String _CANT_EDIT_TRAN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_EDIT_TRAN");
                                
                                    public static final java.lang.String _CANT_ESTABLISH_LINK =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_ESTABLISH_LINK");
                                
                                    public static final java.lang.String _CANT_FIND_BUG =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_FIND_BUG");
                                
                                    public static final java.lang.String _CANT_FIND_MAIL_MERGE_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_FIND_MAIL_MERGE_ID");
                                
                                    public static final java.lang.String _CANT_FIND_RCRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_FIND_RCRD");
                                
                                    public static final java.lang.String _CANT_FIND_SAVED_IMPORT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_FIND_SAVED_IMPORT");
                                
                                    public static final java.lang.String _CANT_FIND_SOURCE_AMORTZN_ACCT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_FIND_SOURCE_AMORTZN_ACCT");
                                
                                    public static final java.lang.String _CANT_FIND_UPS_REG_FOR_LOC =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_FIND_UPS_REG_FOR_LOC");
                                
                                    public static final java.lang.String _CANT_FULFILL_ITEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_FULFILL_ITEM");
                                
                                    public static final java.lang.String _CANT_INACTIVATE_COMMSSN_PLAN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_INACTIVATE_COMMSSN_PLAN");
                                
                                    public static final java.lang.String _CANT_INACTIVE_DEFAULT_SYNC_CAT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_INACTIVE_DEFAULT_SYNC_CAT");
                                
                                    public static final java.lang.String _CANT_INACTIVE_DEFAULT_TMPLT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_INACTIVE_DEFAULT_TMPLT");
                                
                                    public static final java.lang.String _CANT_LOAD_SAVED_SEARCH_PARAM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_LOAD_SAVED_SEARCH_PARAM");
                                
                                    public static final java.lang.String _CANT_LOGIN_WITH_OAUTH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_LOGIN_WITH_OAUTH");
                                
                                    public static final java.lang.String _CANT_LOOKUP_FLD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_LOOKUP_FLD");
                                
                                    public static final java.lang.String _CANT_MAKE_CONTACT_PRIVATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_MAKE_CONTACT_PRIVATE");
                                
                                    public static final java.lang.String _CANT_MARK_SHIPPED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_MARK_SHIPPED");
                                
                                    public static final java.lang.String _CANT_MERGE_EMPLS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_MERGE_EMPLS");
                                
                                    public static final java.lang.String _CANT_MODIFY_APPRVD_TIME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_MODIFY_APPRVD_TIME");
                                
                                    public static final java.lang.String _CANT_MODIFY_FULFILL_STATUS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_MODIFY_FULFILL_STATUS");
                                
                                    public static final java.lang.String _CANT_MODIFY_ISSUE_STATUS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_MODIFY_ISSUE_STATUS");
                                
                                    public static final java.lang.String _CANT_MODIFY_LOCKED_FLD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_MODIFY_LOCKED_FLD");
                                
                                    public static final java.lang.String _CANT_MODIFY_PARENT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_MODIFY_PARENT");
                                
                                    public static final java.lang.String _CANT_MODIFY_REV_REC =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_MODIFY_REV_REC");
                                
                                    public static final java.lang.String _CANT_MODIFY_SUB =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_MODIFY_SUB");
                                
                                    public static final java.lang.String _CANT_MODIFY_TAGATA =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_MODIFY_TAGATA");
                                
                                    public static final java.lang.String _CANT_MODIFY_TEGATA =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_MODIFY_TEGATA");
                                
                                    public static final java.lang.String _CANT_MODIFY_VOID_TRANS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_MODIFY_VOID_TRANS");
                                
                                    public static final java.lang.String _CANT_MOVE_REALIZED_GAINLOSS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_MOVE_REALIZED_GAINLOSS");
                                
                                    public static final java.lang.String _CANT_PAY_TAGATA =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_PAY_TAGATA");
                                
                                    public static final java.lang.String _CANT_PRINT_EMPTY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_PRINT_EMPTY");
                                
                                    public static final java.lang.String _CANT_PROCESS_IMG =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_PROCESS_IMG");
                                
                                    public static final java.lang.String _CANT_RCEIV_BEFORE_FULFILL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_RCEIV_BEFORE_FULFILL");
                                
                                    public static final java.lang.String _CANT_RCEIV_ITEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_RCEIV_ITEM");
                                
                                    public static final java.lang.String _CANT_RECEIVE_TAGATA =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_RECEIVE_TAGATA");
                                
                                    public static final java.lang.String _CANT_REJECT_ORDER =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_REJECT_ORDER");
                                
                                    public static final java.lang.String _CANT_REMOVE_ACH_PAY_METHOD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_REMOVE_ACH_PAY_METHOD");
                                
                                    public static final java.lang.String _CANT_REMOVE_APPROVAL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_REMOVE_APPROVAL");
                                
                                    public static final java.lang.String _CANT_REMOVE_DOMAIN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_REMOVE_DOMAIN");
                                
                                    public static final java.lang.String _CANT_REMOVE_NEXUS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_REMOVE_NEXUS");
                                
                                    public static final java.lang.String _CANT_REMOVE_SCHDUL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_REMOVE_SCHDUL");
                                
                                    public static final java.lang.String _CANT_REMOVE_SUB =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_REMOVE_SUB");
                                
                                    public static final java.lang.String _CANT_REMOV_ALL_FULFILMNT_LINKS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_REMOV_ALL_FULFILMNT_LINKS");
                                
                                    public static final java.lang.String _CANT_REMOV_ITEM_SUB =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_REMOV_ITEM_SUB");
                                
                                    public static final java.lang.String _CANT_RESUBMIT_FAILED_DPLYMNT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_RESUBMIT_FAILED_DPLYMNT");
                                
                                    public static final java.lang.String _CANT_RETURN_FLD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_RETURN_FLD");
                                
                                    public static final java.lang.String _CANT_RETURN_USED_GIFT_CERT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_RETURN_USED_GIFT_CERT");
                                
                                    public static final java.lang.String _CANT_REVERSE_AUTH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_REVERSE_AUTH");
                                
                                    public static final java.lang.String _CANT_REV_REC_BODY_AND_LINE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_REV_REC_BODY_AND_LINE");
                                
                                    public static final java.lang.String _CANT_SCHDUL_RECUR_EVENT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_SCHDUL_RECUR_EVENT");
                                
                                    public static final java.lang.String _CANT_SEND_EMAIL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_SEND_EMAIL");
                                
                                    public static final java.lang.String _CANT_SET_CLOSE_DATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_SET_CLOSE_DATE");
                                
                                    public static final java.lang.String _CANT_SET_INTERNALID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_SET_INTERNALID");
                                
                                    public static final java.lang.String _CANT_SET_STATUS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_SET_STATUS");
                                
                                    public static final java.lang.String _CANT_SWITCH_ROLES_FROM_LOGIN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_SWITCH_ROLES_FROM_LOGIN");
                                
                                    public static final java.lang.String _CANT_SWITCH_SHIP_METHOD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_SWITCH_SHIP_METHOD");
                                
                                    public static final java.lang.String _CANT_UPDATE_ACCTNG_PRDS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_UPDATE_ACCTNG_PRDS");
                                
                                    public static final java.lang.String _CANT_UPDATE_AMT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_UPDATE_AMT");
                                
                                    public static final java.lang.String _CANT_UPDATE_DYNAMIC_GROUP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_UPDATE_DYNAMIC_GROUP");
                                
                                    public static final java.lang.String _CANT_UPDATE_FLDR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_UPDATE_FLDR");
                                
                                    public static final java.lang.String _CANT_UPDATE_LINKED_TRANS_LINES =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_UPDATE_LINKED_TRANS_LINES");
                                
                                    public static final java.lang.String _CANT_UPDATE_PRODUCT_FEED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_UPDATE_PRODUCT_FEED");
                                
                                    public static final java.lang.String _CANT_UPDATE_RECRD_HAS_CHANGED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_UPDATE_RECRD_HAS_CHANGED");
                                
                                    public static final java.lang.String _CANT_UPDATE_RECUR_EVENT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_UPDATE_RECUR_EVENT");
                                
                                    public static final java.lang.String _CANT_UPDATE_ROOT_CATEGORY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_UPDATE_ROOT_CATEGORY");
                                
                                    public static final java.lang.String _CANT_UPDATE_STATUS_TYPE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_UPDATE_STATUS_TYPE");
                                
                                    public static final java.lang.String _CANT_VERIFY_CARD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_VERIFY_CARD");
                                
                                    public static final java.lang.String _CANT_VOID_TRANS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CANT_VOID_TRANS");
                                
                                    public static final java.lang.String _CARD_EXPIRED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CARD_EXPIRED");
                                
                                    public static final java.lang.String _CARD_ID_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CARD_ID_REQD");
                                
                                    public static final java.lang.String _CASE_ALREADY_ASSIGNED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CASE_ALREADY_ASSIGNED");
                                
                                    public static final java.lang.String _CASE_DSNT_EXIST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CASE_DSNT_EXIST");
                                
                                    public static final java.lang.String _CASE_NOT_GROUP_MEMBER =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CASE_NOT_GROUP_MEMBER");
                                
                                    public static final java.lang.String _CASH_SALE_EDIT_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CASH_SALE_EDIT_DISALLWD");
                                
                                    public static final java.lang.String _CC_ACCT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CC_ACCT_REQD");
                                
                                    public static final java.lang.String _CC_ALREADY_SAVED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CC_ALREADY_SAVED");
                                
                                    public static final java.lang.String _CC_EMAIL_ADDRESS_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CC_EMAIL_ADDRESS_REQD");
                                
                                    public static final java.lang.String _CC_NUM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CC_NUM_REQD");
                                
                                    public static final java.lang.String _CC_PROCESSOR_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CC_PROCESSOR_ERROR");
                                
                                    public static final java.lang.String _CC_PROCESSOR_NOT_FOUND =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CC_PROCESSOR_NOT_FOUND");
                                
                                    public static final java.lang.String _CC_SECURITY_CODE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CC_SECURITY_CODE_REQD");
                                
                                    public static final java.lang.String _CERT_AUTH_EXPD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CERT_AUTH_EXPD");
                                
                                    public static final java.lang.String _CERT_EXPD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CERT_EXPD");
                                
                                    public static final java.lang.String _CERT_UNAVAILABLE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CERT_UNAVAILABLE");
                                
                                    public static final java.lang.String _CHANGE_PMT_DATE_AND_REAPPROVE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CHANGE_PMT_DATE_AND_REAPPROVE");
                                
                                    public static final java.lang.String _CHAR_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CHAR_ERROR");
                                
                                    public static final java.lang.String _CHECKOUT_EMAIL_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CHECKOUT_EMAIL_REQD");
                                
                                    public static final java.lang.String _CITY_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CITY_REQD");
                                
                                    public static final java.lang.String _CLASS_ALREADY_EXISTS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CLASS_ALREADY_EXISTS");
                                
                                    public static final java.lang.String _CLASS_OR_DEPT_OR_CUST_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CLASS_OR_DEPT_OR_CUST_REQD");
                                
                                    public static final java.lang.String _CLEAR_AUTOCALC =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CLEAR_AUTOCALC");
                                
                                    public static final java.lang.String _CLOSED_TRAN_PRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CLOSED_TRAN_PRD");
                                
                                    public static final java.lang.String _CLOSE_PREVIOUSE_PERIOD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CLOSE_PREVIOUSE_PERIOD");
                                
                                    public static final java.lang.String _COGS_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("COGS_ERROR");
                                
                                    public static final java.lang.String _COMMSSN_ALREADY_CALCLTD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("COMMSSN_ALREADY_CALCLTD");
                                
                                    public static final java.lang.String _COMMSSN_FEATURE_DISABLED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("COMMSSN_FEATURE_DISABLED");
                                
                                    public static final java.lang.String _COMMSSN_PAYROLL_ITEM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("COMMSSN_PAYROLL_ITEM_REQD");
                                
                                    public static final java.lang.String _COMPANION_PROP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("COMPANION_PROP_REQD");
                                
                                    public static final java.lang.String _COMPANY_FLD_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("COMPANY_FLD_REQD");
                                
                                    public static final java.lang.String _COMP_DELETED_OR_MERGED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("COMP_DELETED_OR_MERGED");
                                
                                    public static final java.lang.String _CONCUR_BILLPAY_JOB_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CONCUR_BILLPAY_JOB_DISALLWD");
                                
                                    public static final java.lang.String _CONCUR_BULK_JOB_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CONCUR_BULK_JOB_DISALLWD");
                                
                                    public static final java.lang.String _CONCUR_MASS_UPDATE_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CONCUR_MASS_UPDATE_DISALLWD");
                                
                                    public static final java.lang.String _CONCUR_SEARCH_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CONCUR_SEARCH_DISALLWD");
                                
                                    public static final java.lang.String _CONSLD_PRNT_AND_CHILD_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CONSLD_PRNT_AND_CHILD_DISALLWD");
                                
                                    public static final java.lang.String _CONTACT_ALREADY_EXISTS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CONTACT_ALREADY_EXISTS");
                                
                                    public static final java.lang.String _CONTACT_NOT_GROUP_MEMBR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CONTACT_NOT_GROUP_MEMBR");
                                
                                    public static final java.lang.String _COOKIES_DISABLED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("COOKIES_DISABLED");
                                
                                    public static final java.lang.String _COUNTRY_STATE_MISMATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("COUNTRY_STATE_MISMATCH");
                                
                                    public static final java.lang.String _CREATEDFROM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CREATEDFROM_REQD");
                                
                                    public static final java.lang.String _CREDITS_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CREDITS_DISALLWD");
                                
                                    public static final java.lang.String _CRNCY_MISMATCH_BASE_CRNCY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CRNCY_MISMATCH_BASE_CRNCY");
                                
                                    public static final java.lang.String _CRNCY_NOT_UPDATED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CRNCY_NOT_UPDATED");
                                
                                    public static final java.lang.String _CRNCY_RCRD_DELETED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CRNCY_RCRD_DELETED");
                                
                                    public static final java.lang.String _CRNCY_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CRNCY_REQD");
                                
                                    public static final java.lang.String _CSC_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CSC_SETUP_REQD");
                                
                                    public static final java.lang.String _CSTM_FIELD_KEY_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CSTM_FIELD_KEY_REQD");
                                
                                    public static final java.lang.String _CSTM_FIELD_VALUE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CSTM_FIELD_VALUE_REQD");
                                
                                    public static final java.lang.String _CUST_ARLEADY_HAS_ACCT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CUST_ARLEADY_HAS_ACCT");
                                
                                    public static final java.lang.String _CUST_CNTR_USER_ACCESS_ONLY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CUST_CNTR_USER_ACCESS_ONLY");
                                
                                    public static final java.lang.String _CUST_LEAD_NOT_GROUP_MEMBR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CUST_LEAD_NOT_GROUP_MEMBR");
                                
                                    public static final java.lang.String _CYBERSOURCE_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CYBERSOURCE_ERROR");
                                
                                    public static final java.lang.String _CYCLE_IN_PROJECT_PLAN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("CYCLE_IN_PROJECT_PLAN");
                                
                                    public static final java.lang.String _DASHBOARD_LOCKED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DASHBOARD_LOCKED");
                                
                                    public static final java.lang.String _DATA_MUST_BE_UNIQUE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DATA_MUST_BE_UNIQUE");
                                
                                    public static final java.lang.String _DATA_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DATA_REQD");
                                
                                    public static final java.lang.String _DATE_EXPECTED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DATE_EXPECTED");
                                
                                    public static final java.lang.String _DATE_PARAM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DATE_PARAM_REQD");
                                
                                    public static final java.lang.String _DATE_PRD_MISMATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DATE_PRD_MISMATCH");
                                
                                    public static final java.lang.String _DEFAULT_CUR_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DEFAULT_CUR_REQD");
                                
                                    public static final java.lang.String _DEFAULT_EXPENSE_ACCT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DEFAULT_EXPENSE_ACCT_REQD");
                                
                                    public static final java.lang.String _DEFAULT_ISSUE_OWNER_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DEFAULT_ISSUE_OWNER_REQD");
                                
                                    public static final java.lang.String _DEFAULT_LIAB_ACCT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DEFAULT_LIAB_ACCT_REQD");
                                
                                    public static final java.lang.String _DEFAULT_ROLE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DEFAULT_ROLE_REQD");
                                
                                    public static final java.lang.String _DEFAULT_TYPE_DELETE_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DEFAULT_TYPE_DELETE_DISALLWD");
                                
                                    public static final java.lang.String _DEFERRAL_ACCT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DEFERRAL_ACCT_REQD");
                                
                                    public static final java.lang.String _DEFERRED_REV_REC_ACCT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DEFERRED_REV_REC_ACCT_REQD");
                                
                                    public static final java.lang.String _DEPT_IN_USE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DEPT_IN_USE");
                                
                                    public static final java.lang.String _DFRNT_SWAP_PRICE_LEVELS_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DFRNT_SWAP_PRICE_LEVELS_REQD");
                                
                                    public static final java.lang.String _DISALLWD_IP_ADDRESS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DISALLWD_IP_ADDRESS");
                                
                                    public static final java.lang.String _DISCOUNT_ACCT_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DISCOUNT_ACCT_SETUP_REQD");
                                
                                    public static final java.lang.String _DISCOUNT_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DISCOUNT_DISALLWD");
                                
                                    public static final java.lang.String _DISCOUNT_DISALLWD_VSOE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DISCOUNT_DISALLWD_VSOE");
                                
                                    public static final java.lang.String _DISCOUNT_EXCEED_TOTAL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DISCOUNT_EXCEED_TOTAL");
                                
                                    public static final java.lang.String _DISTRIB_REQD_ONE_DAY_BFORE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DISTRIB_REQD_ONE_DAY_BFORE");
                                
                                    public static final java.lang.String _DOMAIN_IN_USE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DOMAIN_IN_USE");
                                
                                    public static final java.lang.String _DOMAIN_WEBSITE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DOMAIN_WEBSITE_REQD");
                                
                                    public static final java.lang.String _DROP_SHIP_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DROP_SHIP_ERROR");
                                
                                    public static final java.lang.String _DROP_SHIP_OR_SPECIAL_ORD_ALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DROP_SHIP_OR_SPECIAL_ORD_ALLWD");
                                
                                    public static final java.lang.String _DUE_DATE_BFORE_START_DATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUE_DATE_BFORE_START_DATE");
                                
                                    public static final java.lang.String _DUE_DATE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUE_DATE_REQD");
                                
                                    public static final java.lang.String _DUPLICATE_INVENTORY_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUPLICATE_INVENTORY_NUM");
                                
                                    public static final java.lang.String _DUPLICATE_KEYS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUPLICATE_KEYS");
                                
                                    public static final java.lang.String _DUPLICATE_METHOD_NAME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUPLICATE_METHOD_NAME");
                                
                                    public static final java.lang.String _DUPLICATE_NAME_FOR_PRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUPLICATE_NAME_FOR_PRD");
                                
                                    public static final java.lang.String _DUPLICATE_NAME_FOR_ROLE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUPLICATE_NAME_FOR_ROLE");
                                
                                    public static final java.lang.String _DUPLICATE_RELATIONSHIP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUPLICATE_RELATIONSHIP");
                                
                                    public static final java.lang.String _DUP_ACCT_NAME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_ACCT_NAME");
                                
                                    public static final java.lang.String _DUP_ACCT_NOT_ALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_ACCT_NOT_ALLWD");
                                
                                    public static final java.lang.String _DUP_ACCT_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_ACCT_NUM");
                                
                                    public static final java.lang.String _DUP_ACCT_ON_TRANS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_ACCT_ON_TRANS");
                                
                                    public static final java.lang.String _DUP_BIN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_BIN");
                                
                                    public static final java.lang.String _DUP_BUNDLE_IN_ACCT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_BUNDLE_IN_ACCT");
                                
                                    public static final java.lang.String _DUP_CATEGORY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_CATEGORY");
                                
                                    public static final java.lang.String _DUP_CATEGORY_NAME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_CATEGORY_NAME");
                                
                                    public static final java.lang.String _DUP_COLOR_THEME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_COLOR_THEME");
                                
                                    public static final java.lang.String _DUP_CSTM_FIELD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_CSTM_FIELD");
                                
                                    public static final java.lang.String _DUP_CSTM_LAYOUT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_CSTM_LAYOUT");
                                
                                    public static final java.lang.String _DUP_CSTM_LIST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_CSTM_LIST");
                                
                                    public static final java.lang.String _DUP_CSTM_RCRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_CSTM_RCRD");
                                
                                    public static final java.lang.String _DUP_CSTM_RCRD_ENTRY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_CSTM_RCRD_ENTRY");
                                
                                    public static final java.lang.String _DUP_CSTM_TAB =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_CSTM_TAB");
                                
                                    public static final java.lang.String _DUP_EMPL_EMAIL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_EMPL_EMAIL");
                                
                                    public static final java.lang.String _DUP_EMPL_ENTITY_NAME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_EMPL_ENTITY_NAME");
                                
                                    public static final java.lang.String _DUP_EMPL_TMPLT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_EMPL_TMPLT");
                                
                                    public static final java.lang.String _DUP_ENTITY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_ENTITY");
                                
                                    public static final java.lang.String _DUP_ENTITY_EMAIL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_ENTITY_EMAIL");
                                
                                    public static final java.lang.String _DUP_ENTITY_NAME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_ENTITY_NAME");
                                
                                    public static final java.lang.String _DUP_FEDEX_ACCT_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_FEDEX_ACCT_NUM");
                                
                                    public static final java.lang.String _DUP_FINANCL_STATMNT_LAYOUT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_FINANCL_STATMNT_LAYOUT");
                                
                                    public static final java.lang.String _DUP_INFO_ITEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_INFO_ITEM");
                                
                                    public static final java.lang.String _DUP_ISSUE_NAME_OR_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_ISSUE_NAME_OR_NUM");
                                
                                    public static final java.lang.String _DUP_ITEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_ITEM");
                                
                                    public static final java.lang.String _DUP_ITEM_LAYOUT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_ITEM_LAYOUT");
                                
                                    public static final java.lang.String _DUP_ITEM_NAME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_ITEM_NAME");
                                
                                    public static final java.lang.String _DUP_ITEM_OPTION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_ITEM_OPTION");
                                
                                    public static final java.lang.String _DUP_ITEM_TMPLT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_ITEM_TMPLT");
                                
                                    public static final java.lang.String _DUP_MATRIX_OPTN_ABBRV =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_MATRIX_OPTN_ABBRV");
                                
                                    public static final java.lang.String _DUP_MEMRZD_TRANS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_MEMRZD_TRANS");
                                
                                    public static final java.lang.String _DUP_NAME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_NAME");
                                
                                    public static final java.lang.String _DUP_PAYROLL_ITEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_PAYROLL_ITEM");
                                
                                    public static final java.lang.String _DUP_PRESNTN_CAT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_PRESNTN_CAT");
                                
                                    public static final java.lang.String _DUP_RCRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_RCRD");
                                
                                    public static final java.lang.String _DUP_RCRD_LINK =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_RCRD_LINK");
                                
                                    public static final java.lang.String _DUP_SALES_TAX_ITEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_SALES_TAX_ITEM");
                                
                                    public static final java.lang.String _DUP_SHIPPING_ITEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_SHIPPING_ITEM");
                                
                                    public static final java.lang.String _DUP_SHORT_NAME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_SHORT_NAME");
                                
                                    public static final java.lang.String _DUP_SITE_THEME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_SITE_THEME");
                                
                                    public static final java.lang.String _DUP_SOURCE_ACCT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_SOURCE_ACCT");
                                
                                    public static final java.lang.String _DUP_TAX_CODE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_TAX_CODE");
                                
                                    public static final java.lang.String _DUP_TRACKING_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_TRACKING_NUM");
                                
                                    public static final java.lang.String _DUP_TRANS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_TRANS");
                                
                                    public static final java.lang.String _DUP_UPS_ACCT_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_UPS_ACCT_NUM");
                                
                                    public static final java.lang.String _DUP_VENDOR_EMAIL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_VENDOR_EMAIL");
                                
                                    public static final java.lang.String _DUP_VENDOR_NAME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DUP_VENDOR_NAME");
                                
                                    public static final java.lang.String _EARNING_ITEM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EARNING_ITEM_REQD");
                                
                                    public static final java.lang.String _EBAY_FEE_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EBAY_FEE_ERROR");
                                
                                    public static final java.lang.String _EBAY_TMPLT_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EBAY_TMPLT_ERROR");
                                
                                    public static final java.lang.String _EDITION_DSNT_SUPRT_WORLDPAY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EDITION_DSNT_SUPRT_WORLDPAY");
                                
                                    public static final java.lang.String _EIN_OR_TIN_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EIN_OR_TIN_REQD");
                                
                                    public static final java.lang.String _EMAIL_ADDRS_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EMAIL_ADDRS_REQD");
                                
                                    public static final java.lang.String _EMAIL_ADDRS_REQD_TO_NOTIFY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EMAIL_ADDRS_REQD_TO_NOTIFY");
                                
                                    public static final java.lang.String _EMAIL_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EMAIL_REQD");
                                
                                    public static final java.lang.String _EMAIL_REQD_ACCT_PROVISION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EMAIL_REQD_ACCT_PROVISION");
                                
                                    public static final java.lang.String _EMAIL_REQ_HANDLER_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EMAIL_REQ_HANDLER_ERROR");
                                
                                    public static final java.lang.String _EMPL_IN_USE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EMPL_IN_USE");
                                
                                    public static final java.lang.String _ERROR_DELETE_CARD_DATA =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ERROR_DELETE_CARD_DATA");
                                
                                    public static final java.lang.String _ERROR_IN_TERRITORY_ASSGNMNT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ERROR_IN_TERRITORY_ASSGNMNT");
                                
                                    public static final java.lang.String _ERROR_PRCSSNG_TRANS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ERROR_PRCSSNG_TRANS");
                                
                                    public static final java.lang.String _ERROR_REFUND_TRANS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ERROR_REFUND_TRANS");
                                
                                    public static final java.lang.String _ERROR_REVERSE_AUTH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ERROR_REVERSE_AUTH");
                                
                                    public static final java.lang.String _ERROR_SENDING_TRAN_EMAIL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ERROR_SENDING_TRAN_EMAIL");
                                
                                    public static final java.lang.String _ERROR_VOID_TRANS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ERROR_VOID_TRANS");
                                
                                    public static final java.lang.String _EVENT_ID_NOT_FOUND =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EVENT_ID_NOT_FOUND");
                                
                                    public static final java.lang.String _EXCEEDED_MAX_ALLWD_LOC =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_MAX_ALLWD_LOC");
                                
                                    public static final java.lang.String _EXCEEDED_MAX_CONCUR_RQST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_MAX_CONCUR_RQST");
                                
                                    public static final java.lang.String _EXCEEDED_MAX_EMAILS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_MAX_EMAILS");
                                
                                    public static final java.lang.String _EXCEEDED_MAX_FEATURED_ITEMS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_MAX_FEATURED_ITEMS");
                                
                                    public static final java.lang.String _EXCEEDED_MAX_FIELD_LENGTH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_MAX_FIELD_LENGTH");
                                
                                    public static final java.lang.String _EXCEEDED_MAX_MATRIX_OPTNS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_MAX_MATRIX_OPTNS");
                                
                                    public static final java.lang.String _EXCEEDED_MAX_PDF_ELEMENTS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_MAX_PDF_ELEMENTS");
                                
                                    public static final java.lang.String _EXCEEDED_MAX_PDF_EXPORT_COL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_MAX_PDF_EXPORT_COL");
                                
                                    public static final java.lang.String _EXCEEDED_MAX_PIN_RETRIES =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_MAX_PIN_RETRIES");
                                
                                    public static final java.lang.String _EXCEEDED_MAX_RCRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_MAX_RCRD");
                                
                                    public static final java.lang.String _EXCEEDED_MAX_REPORT_COL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_MAX_REPORT_COL");
                                
                                    public static final java.lang.String _EXCEEDED_MAX_REPORT_ROWS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_MAX_REPORT_ROWS");
                                
                                    public static final java.lang.String _EXCEEDED_MAX_REPORT_SIZE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_MAX_REPORT_SIZE");
                                
                                    public static final java.lang.String _EXCEEDED_MAX_SESSIONS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_MAX_SESSIONS");
                                
                                    public static final java.lang.String _EXCEEDED_MAX_SHIP_PACKAGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_MAX_SHIP_PACKAGE");
                                
                                    public static final java.lang.String _EXCEEDED_MAX_TIME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_MAX_TIME");
                                
                                    public static final java.lang.String _EXCEEDED_MAX_TRANS_LINES =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_MAX_TRANS_LINES");
                                
                                    public static final java.lang.String _EXCEEDED_MAX_USERS_ALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_MAX_USERS_ALLWD");
                                
                                    public static final java.lang.String _EXCEEDED_PER_TRANS_MAX =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_PER_TRANS_MAX");
                                
                                    public static final java.lang.String _EXCEEDED_RQST_SIZE_LIMIT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDED_RQST_SIZE_LIMIT");
                                
                                    public static final java.lang.String _EXCEEDS_ALLWD_LICENSES =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXCEEDS_ALLWD_LICENSES");
                                
                                    public static final java.lang.String _EXPENSE_ENTRY_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXPENSE_ENTRY_DISALLWD");
                                
                                    public static final java.lang.String _EXPIRED_SEARCH_CRITERIA =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXPIRED_SEARCH_CRITERIA");
                                
                                    public static final java.lang.String _EXTERNALID_NOT_SUPPORTED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXTERNALID_NOT_SUPPORTED");
                                
                                    public static final java.lang.String _EXTERNALID_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXTERNALID_REQD");
                                
                                    public static final java.lang.String _EXT_CAT_LINK_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EXT_CAT_LINK_SETUP_REQD");
                                
                                    public static final java.lang.String _FAILED_FEDEX_LABEL_VOID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FAILED_FEDEX_LABEL_VOID");
                                
                                    public static final java.lang.String _FAILED_FORM_VALIDATION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FAILED_FORM_VALIDATION");
                                
                                    public static final java.lang.String _FAILED_UPS_LABEL_VOID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FAILED_UPS_LABEL_VOID");
                                
                                    public static final java.lang.String _FAX_NUM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FAX_NUM_REQD");
                                
                                    public static final java.lang.String _FAX_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FAX_SETUP_REQD");
                                
                                    public static final java.lang.String _FEATURE_DISABLED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEATURE_DISABLED");
                                
                                    public static final java.lang.String _FEATURE_UNAVAILABLE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEATURE_UNAVAILABLE");
                                
                                    public static final java.lang.String _FEDEX_ACCT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEDEX_ACCT_REQD");
                                
                                    public static final java.lang.String _FEDEX_CANT_INTEGRATE_FULFILL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEDEX_CANT_INTEGRATE_FULFILL");
                                
                                    public static final java.lang.String _FEDEX_DROPOFF_TYP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEDEX_DROPOFF_TYP_REQD");
                                
                                    public static final java.lang.String _FEDEX_INVALID_ACCT_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEDEX_INVALID_ACCT_NUM");
                                
                                    public static final java.lang.String _FEDEX_ITEM_CONTENTS_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEDEX_ITEM_CONTENTS_REQD");
                                
                                    public static final java.lang.String _FEDEX_METER_NOT_RETRIEVED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEDEX_METER_NOT_RETRIEVED");
                                
                                    public static final java.lang.String _FEDEX_METER_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEDEX_METER_REQD");
                                
                                    public static final java.lang.String _FEDEX_ONE_PACKG_ALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEDEX_ONE_PACKG_ALLWD");
                                
                                    public static final java.lang.String _FEDEX_ORIGIN_COUNTRY_US_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEDEX_ORIGIN_COUNTRY_US_REQD");
                                
                                    public static final java.lang.String _FEDEX_RATING_SRVC_UNAVAILBL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEDEX_RATING_SRVC_UNAVAILBL");
                                
                                    public static final java.lang.String _FEDEX_REG_NOT_FOUND =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEDEX_REG_NOT_FOUND");
                                
                                    public static final java.lang.String _FEDEX_SHIP_SRVC_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEDEX_SHIP_SRVC_REQD");
                                
                                    public static final java.lang.String _FEDEX_SHIP_SRVC_UNAVAILBL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEDEX_SHIP_SRVC_UNAVAILBL");
                                
                                    public static final java.lang.String _FEDEX_UNSUPRTD_ORIGIN_COUNTRY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEDEX_UNSUPRTD_ORIGIN_COUNTRY");
                                
                                    public static final java.lang.String _FEDEX_USD_EXCHANGE_RATE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEDEX_USD_EXCHANGE_RATE_REQD");
                                
                                    public static final java.lang.String _FEDEX_USER_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEDEX_USER_ERROR");
                                
                                    public static final java.lang.String _FEDEX_VOID_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FEDEX_VOID_ERROR");
                                
                                    public static final java.lang.String _FED_ID_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FED_ID_REQD");
                                
                                    public static final java.lang.String _FED_WITHHOLDING_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FED_WITHHOLDING_REQD");
                                
                                    public static final java.lang.String _FIELD_CALL_DATE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FIELD_CALL_DATE_REQD");
                                
                                    public static final java.lang.String _FIELD_DEFN_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FIELD_DEFN_REQD");
                                
                                    public static final java.lang.String _FIELD_NOT_SETTABLE_ON_ADD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FIELD_NOT_SETTABLE_ON_ADD");
                                
                                    public static final java.lang.String _FIELD_PARAM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FIELD_PARAM_REQD");
                                
                                    public static final java.lang.String _FIELD_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FIELD_REQD");
                                
                                    public static final java.lang.String _FILE_ALREADY_EXISTS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FILE_ALREADY_EXISTS");
                                
                                    public static final java.lang.String _FILE_DISALLWD_IN_ROOT_FLDR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FILE_DISALLWD_IN_ROOT_FLDR");
                                
                                    public static final java.lang.String _FILE_MISSING =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FILE_MISSING");
                                
                                    public static final java.lang.String _FILE_NOT_DOWNLOADABLE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FILE_NOT_DOWNLOADABLE");
                                
                                    public static final java.lang.String _FILE_NOT_FOUND =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FILE_NOT_FOUND");
                                
                                    public static final java.lang.String _FILE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FILE_REQD");
                                
                                    public static final java.lang.String _FILE_UPLOAD_IN_PROGRESS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FILE_UPLOAD_IN_PROGRESS");
                                
                                    public static final java.lang.String _FILTER_BY_AMT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FILTER_BY_AMT_REQD");
                                
                                    public static final java.lang.String _FINANCE_CHARGE_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FINANCE_CHARGE_SETUP_REQD");
                                
                                    public static final java.lang.String _FINANCE_CHARGE_SET_PREFS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FINANCE_CHARGE_SET_PREFS");
                                
                                    public static final java.lang.String _FIRST_LAST_NAMES_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FIRST_LAST_NAMES_REQD");
                                
                                    public static final java.lang.String _FIRST_QTY_BUCKET_MUST_BE_ZERO =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FIRST_QTY_BUCKET_MUST_BE_ZERO");
                                
                                    public static final java.lang.String _FLD_VALUE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FLD_VALUE_REQD");
                                
                                    public static final java.lang.String _FLD_VALUE_TOO_LARGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FLD_VALUE_TOO_LARGE");
                                
                                    public static final java.lang.String _FOLDER_ALREADY_EXISTS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FOLDER_ALREADY_EXISTS");
                                
                                    public static final java.lang.String _FORMULA_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FORMULA_ERROR");
                                
                                    public static final java.lang.String _FORM_RESUBMISSION_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FORM_RESUBMISSION_REQD");
                                
                                    public static final java.lang.String _FORM_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FORM_SETUP_REQD");
                                
                                    public static final java.lang.String _FORM_UNAVAILBL_ONLINE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FORM_UNAVAILBL_ONLINE");
                                
                                    public static final java.lang.String _FRIENDLY_NAME_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FRIENDLY_NAME_REQD");
                                
                                    public static final java.lang.String _FULFILL_REQD_FIELDS_MISSING =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FULFILL_REQD_FIELDS_MISSING");
                                
                                    public static final java.lang.String _FULFILL_REQD_PARAMS_MISSING =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FULFILL_REQD_PARAMS_MISSING");
                                
                                    public static final java.lang.String _FULL_DISTRIB_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FULL_DISTRIB_REQD");
                                
                                    public static final java.lang.String _FULL_USERS_REQD_TO_INTEGRATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FULL_USERS_REQD_TO_INTEGRATE");
                                
                                    public static final java.lang.String _FX_MALFORMED_RESPONSE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FX_MALFORMED_RESPONSE");
                                
                                    public static final java.lang.String _FX_RATE_REQD_FEDEX_RATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FX_RATE_REQD_FEDEX_RATE");
                                
                                    public static final java.lang.String _FX_TRANS_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("FX_TRANS_DISALLWD");
                                
                                    public static final java.lang.String _GETALL_RCRD_TYPE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("GETALL_RCRD_TYPE_REQD");
                                
                                    public static final java.lang.String _GIFT_CERT_AMT_EXCEED_AVAILBL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("GIFT_CERT_AMT_EXCEED_AVAILBL");
                                
                                    public static final java.lang.String _GIFT_CERT_AUTH_ALREADY_EXISTS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("GIFT_CERT_AUTH_ALREADY_EXISTS");
                                
                                    public static final java.lang.String _GIFT_CERT_CAN_BE_USED_ONCE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("GIFT_CERT_CAN_BE_USED_ONCE");
                                
                                    public static final java.lang.String _GIFT_CERT_CODE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("GIFT_CERT_CODE_REQD");
                                
                                    public static final java.lang.String _GIFT_CERT_INVALID_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("GIFT_CERT_INVALID_NUM");
                                
                                    public static final java.lang.String _GIFT_CERT_IN_USE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("GIFT_CERT_IN_USE");
                                
                                    public static final java.lang.String _GROUP_DSNT_EXIST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("GROUP_DSNT_EXIST");
                                
                                    public static final java.lang.String _GROUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("GROUP_REQD");
                                
                                    public static final java.lang.String _GROUP_TYPE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("GROUP_TYPE_REQD");
                                
                                    public static final java.lang.String _GRTR_QTY_PRICE_LEVEL_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("GRTR_QTY_PRICE_LEVEL_REQD");
                                
                                    public static final java.lang.String _ILLEGAL_PERIOD_STRUCTURE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ILLEGAL_PERIOD_STRUCTURE");
                                
                                    public static final java.lang.String _INACTIVE_CC_PROFILE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INACTIVE_CC_PROFILE");
                                
                                    public static final java.lang.String _INACTIVE_RCRD_FOR_ROLE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INACTIVE_RCRD_FOR_ROLE");
                                
                                    public static final java.lang.String _INAVLID_FILE_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INAVLID_FILE_TYP");
                                
                                    public static final java.lang.String _INAVLID_ITEM_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INAVLID_ITEM_TYP");
                                
                                    public static final java.lang.String _INAVLID_PRICING_MTRX =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INAVLID_PRICING_MTRX");
                                
                                    public static final java.lang.String _INCOMPATIBLE_ACCT_CHANGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INCOMPATIBLE_ACCT_CHANGE");
                                
                                    public static final java.lang.String _INCOMPLETE_BILLING_ADDR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INCOMPLETE_BILLING_ADDR");
                                
                                    public static final java.lang.String _INCOMPLETE_FILE_UPLOAD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INCOMPLETE_FILE_UPLOAD");
                                
                                    public static final java.lang.String _INCRCT_ORD_INFO =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INCRCT_ORD_INFO");
                                
                                    public static final java.lang.String _INFINITE_LOOP_DETECTED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INFINITE_LOOP_DETECTED");
                                
                                    public static final java.lang.String _INITIALIZE_ARG_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INITIALIZE_ARG_REQD");
                                
                                    public static final java.lang.String _INITIALIZE_AUXREF_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INITIALIZE_AUXREF_REQD");
                                
                                    public static final java.lang.String _INSTALL_SCRIPT_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INSTALL_SCRIPT_ERROR");
                                
                                    public static final java.lang.String _INSUFCNT_NUM_PRDS_FOR_REV_REC =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INSUFCNT_NUM_PRDS_FOR_REV_REC");
                                
                                    public static final java.lang.String _INSUFCNT_OPEN_PRDS_FOR_REV_REC =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INSUFCNT_OPEN_PRDS_FOR_REV_REC");
                                
                                    public static final java.lang.String _INSUFFICIENT_CHARS_IN_SEARCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INSUFFICIENT_CHARS_IN_SEARCH");
                                
                                    public static final java.lang.String _INSUFFICIENT_FLD_PERMISSION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INSUFFICIENT_FLD_PERMISSION");
                                
                                    public static final java.lang.String _INSUFFICIENT_FUND =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INSUFFICIENT_FUND");
                                
                                    public static final java.lang.String _INSUFFICIENT_PERMISSION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INSUFFICIENT_PERMISSION");
                                
                                    public static final java.lang.String _INTEGER_REQD_FOR_QTY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INTEGER_REQD_FOR_QTY");
                                
                                    public static final java.lang.String _INTL_FEDEX_ONE_PACKG_ALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INTL_FEDEX_ONE_PACKG_ALLWD");
                                
                                    public static final java.lang.String _INTL_SHIP_EXCEED_MAX_ITEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INTL_SHIP_EXCEED_MAX_ITEM");
                                
                                    public static final java.lang.String _INVALID_ABN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ABN");
                                
                                    public static final java.lang.String _INVALID_ACCT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ACCT");
                                
                                    public static final java.lang.String _INVALID_ACCT_NUM_CSTM_FIELD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ACCT_NUM_CSTM_FIELD");
                                
                                    public static final java.lang.String _INVALID_ACCT_PRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ACCT_PRD");
                                
                                    public static final java.lang.String _INVALID_ACCT_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ACCT_TYP");
                                
                                    public static final java.lang.String _INVALID_ACTION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ACTION");
                                
                                    public static final java.lang.String _INVALID_ADDRESS_OR_SHIPPER_NO =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ADDRESS_OR_SHIPPER_NO");
                                
                                    public static final java.lang.String _INVALID_ADJUSTMENT_ACCT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ADJUSTMENT_ACCT");
                                
                                    public static final java.lang.String _INVALID_AES_FTSR_EXEMPTN_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_AES_FTSR_EXEMPTN_NUM");
                                
                                    public static final java.lang.String _INVALID_ALLOCTN_METHOD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ALLOCTN_METHOD");
                                
                                    public static final java.lang.String _INVALID_AMORTZN_ACCT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_AMORTZN_ACCT");
                                
                                    public static final java.lang.String _INVALID_AMT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_AMT");
                                
                                    public static final java.lang.String _INVALID_APP_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_APP_ID");
                                
                                    public static final java.lang.String _INVALID_ASSIGN_STATUS_COMBO =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ASSIGN_STATUS_COMBO");
                                
                                    public static final java.lang.String _INVALID_AUTH_CODE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_AUTH_CODE");
                                
                                    public static final java.lang.String _INVALID_AUTOAPPLY_VALUE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_AUTOAPPLY_VALUE");
                                
                                    public static final java.lang.String _INVALID_AVS_ADDR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_AVS_ADDR");
                                
                                    public static final java.lang.String _INVALID_AVS_ZIP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_AVS_ZIP");
                                
                                    public static final java.lang.String _INVALID_BALANCE_RANGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_BALANCE_RANGE");
                                
                                    public static final java.lang.String _INVALID_BILLING_SCHDUL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_BILLING_SCHDUL");
                                
                                    public static final java.lang.String _INVALID_BILLING_SCHDUL_DATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_BILLING_SCHDUL_DATE");
                                
                                    public static final java.lang.String _INVALID_BILLING_SCHDUL_ENTRY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_BILLING_SCHDUL_ENTRY");
                                
                                    public static final java.lang.String _INVALID_BIN_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_BIN_NUM");
                                
                                    public static final java.lang.String _INVALID_BOM_QTY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_BOM_QTY");
                                
                                    public static final java.lang.String _INVALID_BOOLEAN_VALUE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_BOOLEAN_VALUE");
                                
                                    public static final java.lang.String _INVALID_BUG_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_BUG_NUM");
                                
                                    public static final java.lang.String _INVALID_CAMPAIGN_CHANNEL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CAMPAIGN_CHANNEL");
                                
                                    public static final java.lang.String _INVALID_CAMPAIGN_GROUP_SIZE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CAMPAIGN_GROUP_SIZE");
                                
                                    public static final java.lang.String _INVALID_CAMPAIGN_STATUS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CAMPAIGN_STATUS");
                                
                                    public static final java.lang.String _INVALID_CARD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CARD");
                                
                                    public static final java.lang.String _INVALID_CARD_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CARD_ID");
                                
                                    public static final java.lang.String _INVALID_CARD_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CARD_NUM");
                                
                                    public static final java.lang.String _INVALID_CARD_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CARD_TYP");
                                
                                    public static final java.lang.String _INVALID_CASE_FORM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CASE_FORM");
                                
                                    public static final java.lang.String _INVALID_CATGRY_TAX_AGENCY_REQ =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CATGRY_TAX_AGENCY_REQ");
                                
                                    public static final java.lang.String _INVALID_CC_EMAIL_ADDRESS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CC_EMAIL_ADDRESS");
                                
                                    public static final java.lang.String _INVALID_CC_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CC_NUM");
                                
                                    public static final java.lang.String _INVALID_CERT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CERT");
                                
                                    public static final java.lang.String _INVALID_CERT_AUTH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CERT_AUTH");
                                
                                    public static final java.lang.String _INVALID_CHANGE_LIST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CHANGE_LIST");
                                
                                    public static final java.lang.String _INVALID_CHARS_IN_EMAIL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CHARS_IN_EMAIL");
                                
                                    public static final java.lang.String _INVALID_CHARS_IN_NAME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CHARS_IN_NAME");
                                
                                    public static final java.lang.String _INVALID_CHARS_IN_PARAM_FIELD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CHARS_IN_PARAM_FIELD");
                                
                                    public static final java.lang.String _INVALID_CHARS_IN_URL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CHARS_IN_URL");
                                
                                    public static final java.lang.String _INVALID_CHECKOUT_EMAIL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CHECKOUT_EMAIL");
                                
                                    public static final java.lang.String _INVALID_CITY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CITY");
                                
                                    public static final java.lang.String _INVALID_COLUMN_NAME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_COLUMN_NAME");
                                
                                    public static final java.lang.String _INVALID_COLUMN_VALUE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_COLUMN_VALUE");
                                
                                    public static final java.lang.String _INVALID_CONTENT_TYPE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CONTENT_TYPE");
                                
                                    public static final java.lang.String _INVALID_COSTING_METHOD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_COSTING_METHOD");
                                
                                    public static final java.lang.String _INVALID_CRNCY_EXCH_RATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CRNCY_EXCH_RATE");
                                
                                    public static final java.lang.String _INVALID_CRYPT_KEY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CRYPT_KEY");
                                
                                    public static final java.lang.String _INVALID_CSTM_FIELD_DATA_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CSTM_FIELD_DATA_TYP");
                                
                                    public static final java.lang.String _INVALID_CSTM_FIELD_RCRD_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CSTM_FIELD_RCRD_TYP");
                                
                                    public static final java.lang.String _INVALID_CSTM_FIELD_REF =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CSTM_FIELD_REF");
                                
                                    public static final java.lang.String _INVALID_CSTM_FORM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CSTM_FORM");
                                
                                    public static final java.lang.String _INVALID_CSTM_RCRD_KEY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CSTM_RCRD_KEY");
                                
                                    public static final java.lang.String _INVALID_CSTM_RCRD_QUERY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CSTM_RCRD_QUERY");
                                
                                    public static final java.lang.String _INVALID_CSTM_RCRD_TYPE_KEY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CSTM_RCRD_TYPE_KEY");
                                
                                    public static final java.lang.String _INVALID_CSTM_RCRD_TYP_KEY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CSTM_RCRD_TYP_KEY");
                                
                                    public static final java.lang.String _INVALID_CUR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CUR");
                                
                                    public static final java.lang.String _INVALID_CURRENCY_CODE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CURRENCY_CODE");
                                
                                    public static final java.lang.String _INVALID_CURRENCY_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CURRENCY_TYP");
                                
                                    public static final java.lang.String _INVALID_CURR_CODE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CURR_CODE");
                                
                                    public static final java.lang.String _INVALID_CUSTOMER_RCRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_CUSTOMER_RCRD");
                                
                                    public static final java.lang.String _INVALID_DATA =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_DATA");
                                
                                    public static final java.lang.String _INVALID_DATA_FORMAT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_DATA_FORMAT");
                                
                                    public static final java.lang.String _INVALID_DATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_DATE");
                                
                                    public static final java.lang.String _INVALID_DATE_FORMAT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_DATE_FORMAT");
                                
                                    public static final java.lang.String _INVALID_DATE_RANGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_DATE_RANGE");
                                
                                    public static final java.lang.String _INVALID_DATE_TIME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_DATE_TIME");
                                
                                    public static final java.lang.String _INVALID_DEAL_RANGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_DEAL_RANGE");
                                
                                    public static final java.lang.String _INVALID_DELETE_REF =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_DELETE_REF");
                                
                                    public static final java.lang.String _INVALID_DESTINATION_FLDR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_DESTINATION_FLDR");
                                
                                    public static final java.lang.String _INVALID_DESTNTN_COUNTRY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_DESTNTN_COUNTRY");
                                
                                    public static final java.lang.String _INVALID_DESTNTN_POST_CODE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_DESTNTN_POST_CODE");
                                
                                    public static final java.lang.String _INVALID_DESTNTN_STATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_DESTNTN_STATE");
                                
                                    public static final java.lang.String _INVALID_DETACH_RECORD_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_DETACH_RECORD_TYP");
                                
                                    public static final java.lang.String _INVALID_DISCOUNT_MARKUP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_DISCOUNT_MARKUP");
                                
                                    public static final java.lang.String _INVALID_DOMAIN_KEY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_DOMAIN_KEY");
                                
                                    public static final java.lang.String _INVALID_DOMAIN_NAME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_DOMAIN_NAME");
                                
                                    public static final java.lang.String _INVALID_DUP_ISSUE_REF =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_DUP_ISSUE_REF");
                                
                                    public static final java.lang.String _INVALID_EMAIL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_EMAIL");
                                
                                    public static final java.lang.String _INVALID_EMAIL_ADDR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_EMAIL_ADDR");
                                
                                    public static final java.lang.String _INVALID_END_DATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_END_DATE");
                                
                                    public static final java.lang.String _INVALID_END_TIME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_END_TIME");
                                
                                    public static final java.lang.String _INVALID_ENTITY_INTERNALID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ENTITY_INTERNALID");
                                
                                    public static final java.lang.String _INVALID_ENTITY_STATUS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ENTITY_STATUS");
                                
                                    public static final java.lang.String _INVALID_EVENT_TIME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_EVENT_TIME");
                                
                                    public static final java.lang.String _INVALID_EXPNS_ACCT_SUB =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_EXPNS_ACCT_SUB");
                                
                                    public static final java.lang.String _INVALID_EXPRESSION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_EXPRESSION");
                                
                                    public static final java.lang.String _INVALID_EXP_DATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_EXP_DATE");
                                
                                    public static final java.lang.String _INVALID_FAX_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FAX_NUM");
                                
                                    public static final java.lang.String _INVALID_FAX_PHONE_FORMAT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FAX_PHONE_FORMAT");
                                
                                    public static final java.lang.String _INVALID_FIELD_FOR_RCRD_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FIELD_FOR_RCRD_TYP");
                                
                                    public static final java.lang.String _INVALID_FIELD_NAME_FOR_NULL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FIELD_NAME_FOR_NULL");
                                
                                    public static final java.lang.String _INVALID_FILE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FILE");
                                
                                    public static final java.lang.String _INVALID_FILE_ENCODING =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FILE_ENCODING");
                                
                                    public static final java.lang.String _INVALID_FILE_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FILE_TYP");
                                
                                    public static final java.lang.String _INVALID_FLD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FLD");
                                
                                    public static final java.lang.String _INVALID_FLDR_SIZE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FLDR_SIZE");
                                
                                    public static final java.lang.String _INVALID_FLD_RANGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FLD_RANGE");
                                
                                    public static final java.lang.String _INVALID_FLD_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FLD_TYP");
                                
                                    public static final java.lang.String _INVALID_FLD_VALUE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FLD_VALUE");
                                
                                    public static final java.lang.String _INVALID_FORMAT_IN_PARAM_FIELD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FORMAT_IN_PARAM_FIELD");
                                
                                    public static final java.lang.String _INVALID_FORMULA =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FORMULA");
                                
                                    public static final java.lang.String _INVALID_FORMULA_FIELD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FORMULA_FIELD");
                                
                                    public static final java.lang.String _INVALID_FROM_DATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FROM_DATE");
                                
                                    public static final java.lang.String _INVALID_FROM_TIME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FROM_TIME");
                                
                                    public static final java.lang.String _INVALID_FULFILMNT_ITEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FULFILMNT_ITEM");
                                
                                    public static final java.lang.String _INVALID_FX_BASE_CURRENCY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FX_BASE_CURRENCY");
                                
                                    public static final java.lang.String _INVALID_FX_RATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_FX_RATE");
                                
                                    public static final java.lang.String _INVALID_GET_REF =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_GET_REF");
                                
                                    public static final java.lang.String _INVALID_GIFT_CERT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_GIFT_CERT");
                                
                                    public static final java.lang.String _INVALID_GIFT_CERT_AMT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_GIFT_CERT_AMT");
                                
                                    public static final java.lang.String _INVALID_GIFT_CERT_CODE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_GIFT_CERT_CODE");
                                
                                    public static final java.lang.String _INVALID_GROUP_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_GROUP_TYP");
                                
                                    public static final java.lang.String _INVALID_GROUP_TYPE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_GROUP_TYPE");
                                
                                    public static final java.lang.String _INVALID_GRP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_GRP");
                                
                                    public static final java.lang.String _INVALID_GST_PST_AGENCIES =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_GST_PST_AGENCIES");
                                
                                    public static final java.lang.String _INVALID_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ID");
                                
                                    public static final java.lang.String _INVALID_INITIALIZE_ARG =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_INITIALIZE_ARG");
                                
                                    public static final java.lang.String _INVALID_INITIALIZE_AUXREF =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_INITIALIZE_AUXREF");
                                
                                    public static final java.lang.String _INVALID_INITIALIZE_REF =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_INITIALIZE_REF");
                                
                                    public static final java.lang.String _INVALID_INSURED_VALUE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_INSURED_VALUE");
                                
                                    public static final java.lang.String _INVALID_INTERNALID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_INTERNALID");
                                
                                    public static final java.lang.String _INVALID_INVENTORY_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_INVENTORY_NUM");
                                
                                    public static final java.lang.String _INVALID_INV_DATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_INV_DATE");
                                
                                    public static final java.lang.String _INVALID_IP_ADDRESS_RULE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_IP_ADDRESS_RULE");
                                
                                    public static final java.lang.String _INVALID_ISSUE_BUILD_VERSION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ISSUE_BUILD_VERSION");
                                
                                    public static final java.lang.String _INVALID_ISSUE_PRIORITY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ISSUE_PRIORITY");
                                
                                    public static final java.lang.String _INVALID_ISSUE_PRODUCT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ISSUE_PRODUCT");
                                
                                    public static final java.lang.String _INVALID_ISSUE_STATUS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ISSUE_STATUS");
                                
                                    public static final java.lang.String _INVALID_ITEM_OPTION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ITEM_OPTION");
                                
                                    public static final java.lang.String _INVALID_ITEM_OPTIONS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ITEM_OPTIONS");
                                
                                    public static final java.lang.String _INVALID_ITEM_SUBTYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ITEM_SUBTYP");
                                
                                    public static final java.lang.String _INVALID_ITEM_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ITEM_TYP");
                                
                                    public static final java.lang.String _INVALID_ITEM_WEIGHT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ITEM_WEIGHT");
                                
                                    public static final java.lang.String _INVALID_JOB_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_JOB_ID");
                                
                                    public static final java.lang.String _INVALID_KEY_OR_REF =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_KEY_OR_REF");
                                
                                    public static final java.lang.String _INVALID_KEY_PASSWORD_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_KEY_PASSWORD_REQD");
                                
                                    public static final java.lang.String _INVALID_LINE_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_LINE_ID");
                                
                                    public static final java.lang.String _INVALID_LINK_SUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_LINK_SUM");
                                
                                    public static final java.lang.String _INVALID_LIST_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_LIST_ID");
                                
                                    public static final java.lang.String _INVALID_LIST_KEY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_LIST_KEY");
                                
                                    public static final java.lang.String _INVALID_LOC =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_LOC");
                                
                                    public static final java.lang.String _INVALID_LOC_SUB_RESTRICTN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_LOC_SUB_RESTRICTN");
                                
                                    public static final java.lang.String _INVALID_LOGIN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_LOGIN");
                                
                                    public static final java.lang.String _INVALID_LOGIN_ATTEMPT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_LOGIN_ATTEMPT");
                                
                                    public static final java.lang.String _INVALID_LOGIN_CREDENTIALS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_LOGIN_CREDENTIALS");
                                
                                    public static final java.lang.String _INVALID_LOGIN_IP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_LOGIN_IP");
                                
                                    public static final java.lang.String _INVALID_LOT_NUM_FORMAT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_LOT_NUM_FORMAT");
                                
                                    public static final java.lang.String _INVALID_MACRO_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_MACRO_ID");
                                
                                    public static final java.lang.String _INVALID_MARKUP_DISCOUNT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_MARKUP_DISCOUNT");
                                
                                    public static final java.lang.String _INVALID_MCC =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_MCC");
                                
                                    public static final java.lang.String _INVALID_MEMBER_HIERARCHY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_MEMBER_HIERARCHY");
                                
                                    public static final java.lang.String _INVALID_MEMRZD_TRANS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_MEMRZD_TRANS");
                                
                                    public static final java.lang.String _INVALID_MERCHANT_KEY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_MERCHANT_KEY");
                                
                                    public static final java.lang.String _INVALID_MERCHANT_NAME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_MERCHANT_NAME");
                                
                                    public static final java.lang.String _INVALID_NAME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_NAME");
                                
                                    public static final java.lang.String _INVALID_NEXUS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_NEXUS");
                                
                                    public static final java.lang.String _INVALID_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_NUM");
                                
                                    public static final java.lang.String _INVALID_NUMBER =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_NUMBER");
                                
                                    public static final java.lang.String _INVALID_OBJ =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_OBJ");
                                
                                    public static final java.lang.String _INVALID_ONLINE_FORM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ONLINE_FORM");
                                
                                    public static final java.lang.String _INVALID_ONLINE_FORM_URL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ONLINE_FORM_URL");
                                
                                    public static final java.lang.String _INVALID_OPENID_DOMAIN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_OPENID_DOMAIN");
                                
                                    public static final java.lang.String _INVALID_OPERATION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_OPERATION");
                                
                                    public static final java.lang.String _INVALID_ORD_STATUS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ORD_STATUS");
                                
                                    public static final java.lang.String _INVALID_ORIGIN_COUNTRY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ORIGIN_COUNTRY");
                                
                                    public static final java.lang.String _INVALID_ORIGIN_POSTCODE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ORIGIN_POSTCODE");
                                
                                    public static final java.lang.String _INVALID_ORIGIN_STATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ORIGIN_STATE");
                                
                                    public static final java.lang.String _INVALID_PAGER_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PAGER_NUM");
                                
                                    public static final java.lang.String _INVALID_PAGE_INDEX =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PAGE_INDEX");
                                
                                    public static final java.lang.String _INVALID_PAGE_PARAM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PAGE_PARAM");
                                
                                    public static final java.lang.String _INVALID_PARAM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PARAM");
                                
                                    public static final java.lang.String _INVALID_PARENT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PARENT");
                                
                                    public static final java.lang.String _INVALID_PARTNER_CODE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PARTNER_CODE");
                                
                                    public static final java.lang.String _INVALID_PARTNER_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PARTNER_ID");
                                
                                    public static final java.lang.String _INVALID_PASSWORD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PASSWORD");
                                
                                    public static final java.lang.String _INVALID_PAYCHECK_DATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PAYCHECK_DATE");
                                
                                    public static final java.lang.String _INVALID_PERIOD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PERIOD");
                                
                                    public static final java.lang.String _INVALID_PHONE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PHONE");
                                
                                    public static final java.lang.String _INVALID_PHONE_FAX_PAGER_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PHONE_FAX_PAGER_NUM");
                                
                                    public static final java.lang.String _INVALID_PHONE_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PHONE_NUM");
                                
                                    public static final java.lang.String _INVALID_PHONE_NUMBER =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PHONE_NUMBER");
                                
                                    public static final java.lang.String _INVALID_PICKUP_POSTAL_CODE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PICKUP_POSTAL_CODE");
                                
                                    public static final java.lang.String _INVALID_PIN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PIN");
                                
                                    public static final java.lang.String _INVALID_PIN_DEBIT_TRANS_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PIN_DEBIT_TRANS_TYP");
                                
                                    public static final java.lang.String _INVALID_PORTLET_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PORTLET_TYP");
                                
                                    public static final java.lang.String _INVALID_POST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_POST");
                                
                                    public static final java.lang.String _INVALID_PRESENTATION_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PRESENTATION_TYP");
                                
                                    public static final java.lang.String _INVALID_PROBABILITY_RANGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PROBABILITY_RANGE");
                                
                                    public static final java.lang.String _INVALID_PROFILE_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PROFILE_ID");
                                
                                    public static final java.lang.String _INVALID_PROJ_BILLING_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PROJ_BILLING_TYP");
                                
                                    public static final java.lang.String _INVALID_PST_TAX_VALUE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PST_TAX_VALUE");
                                
                                    public static final java.lang.String _INVALID_PSWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PSWD");
                                
                                    public static final java.lang.String _INVALID_PSWD_HINT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PSWD_HINT");
                                
                                    public static final java.lang.String _INVALID_PSWD_ILLEGAL_CHAR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PSWD_ILLEGAL_CHAR");
                                
                                    public static final java.lang.String _INVALID_PURCHASE_TAX_CODE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PURCHASE_TAX_CODE");
                                
                                    public static final java.lang.String _INVALID_QTY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_QTY");
                                
                                    public static final java.lang.String _INVALID_QUANTITY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_QUANTITY");
                                
                                    public static final java.lang.String _INVALID_QUESTION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_QUESTION");
                                
                                    public static final java.lang.String _INVALID_RCRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RCRD");
                                
                                    public static final java.lang.String _INVALID_RCRD_CONVERSION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RCRD_CONVERSION");
                                
                                    public static final java.lang.String _INVALID_RCRD_HEADER_ =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RCRD_HEADER_");
                                
                                    public static final java.lang.String _INVALID_RCRD_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RCRD_ID");
                                
                                    public static final java.lang.String _INVALID_RCRD_INITIALIZE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RCRD_INITIALIZE");
                                
                                    public static final java.lang.String _INVALID_RCRD_OBJ =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RCRD_OBJ");
                                
                                    public static final java.lang.String _INVALID_RCRD_REF =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RCRD_REF");
                                
                                    public static final java.lang.String _INVALID_RCRD_TRANSFRM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RCRD_TRANSFRM");
                                
                                    public static final java.lang.String _INVALID_RCRD_TYPE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RCRD_TYPE");
                                
                                    public static final java.lang.String _INVALID_RECIPIENT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RECIPIENT");
                                
                                    public static final java.lang.String _INVALID_RECR_REF =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RECR_REF");
                                
                                    public static final java.lang.String _INVALID_RECUR_DATE_RANGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RECUR_DATE_RANGE");
                                
                                    public static final java.lang.String _INVALID_RECUR_DESC_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RECUR_DESC_REQD");
                                
                                    public static final java.lang.String _INVALID_RECUR_DOW =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RECUR_DOW");
                                
                                    public static final java.lang.String _INVALID_RECUR_DOWIM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RECUR_DOWIM");
                                
                                    public static final java.lang.String _INVALID_RECUR_DOWMASK =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RECUR_DOWMASK");
                                
                                    public static final java.lang.String _INVALID_RECUR_FREQUENCY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RECUR_FREQUENCY");
                                
                                    public static final java.lang.String _INVALID_RECUR_PATTERN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RECUR_PATTERN");
                                
                                    public static final java.lang.String _INVALID_RECUR_PERIOD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RECUR_PERIOD");
                                
                                    public static final java.lang.String _INVALID_RECUR_TIME_ZONE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RECUR_TIME_ZONE_REQD");
                                
                                    public static final java.lang.String _INVALID_REFFERER_EMAIL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_REFFERER_EMAIL");
                                
                                    public static final java.lang.String _INVALID_REFUND_AMT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_REFUND_AMT");
                                
                                    public static final java.lang.String _INVALID_REF_CANT_INITIALIZE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_REF_CANT_INITIALIZE");
                                
                                    public static final java.lang.String _INVALID_REF_KEY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_REF_KEY");
                                
                                    public static final java.lang.String _INVALID_REPORT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_REPORT");
                                
                                    public static final java.lang.String _INVALID_REPORT_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_REPORT_ID");
                                
                                    public static final java.lang.String _INVALID_REPORT_ROW =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_REPORT_ROW");
                                
                                    public static final java.lang.String _INVALID_REQUEST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_REQUEST");
                                
                                    public static final java.lang.String _INVALID_RESOURCE_TIME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RESOURCE_TIME");
                                
                                    public static final java.lang.String _INVALID_RESULT_SUMMARY_FUNC =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RESULT_SUMMARY_FUNC");
                                
                                    public static final java.lang.String _INVALID_RETURN_DATA_OBJECT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RETURN_DATA_OBJECT");
                                
                                    public static final java.lang.String _INVALID_REV_REC_DATE_RANGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_REV_REC_DATE_RANGE");
                                
                                    public static final java.lang.String _INVALID_ROLE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ROLE");
                                
                                    public static final java.lang.String _INVALID_ROLE_FOR_EVENT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ROLE_FOR_EVENT");
                                
                                    public static final java.lang.String _INVALID_RQST_CONTACTS_EXIST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RQST_CONTACTS_EXIST");
                                
                                    public static final java.lang.String _INVALID_RQST_PARENT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RQST_PARENT_REQD");
                                
                                    public static final java.lang.String _INVALID_RQST_SBCUST_JOBS_EXIST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_RQST_SBCUST_JOBS_EXIST");
                                
                                    public static final java.lang.String _INVALID_SAVEDSEARCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SAVEDSEARCH");
                                
                                    public static final java.lang.String _INVALID_SAVED_SRCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SAVED_SRCH");
                                
                                    public static final java.lang.String _INVALID_SCHDUL_AMT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SCHDUL_AMT");
                                
                                    public static final java.lang.String _INVALID_SCHDUL_FORMAT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SCHDUL_FORMAT");
                                
                                    public static final java.lang.String _INVALID_SCRIPT_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SCRIPT_ID");
                                
                                    public static final java.lang.String _INVALID_SEARCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SEARCH");
                                
                                    public static final java.lang.String _INVALID_SEARCH_CRITERIA =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SEARCH_CRITERIA");
                                
                                    public static final java.lang.String _INVALID_SEARCH_FIELD_KEY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SEARCH_FIELD_KEY");
                                
                                    public static final java.lang.String _INVALID_SEARCH_FIELD_NAME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SEARCH_FIELD_NAME");
                                
                                    public static final java.lang.String _INVALID_SEARCH_FIELD_OBJ =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SEARCH_FIELD_OBJ");
                                
                                    public static final java.lang.String _INVALID_SEARCH_JOIN_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SEARCH_JOIN_ID");
                                
                                    public static final java.lang.String _INVALID_SEARCH_MORE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SEARCH_MORE");
                                
                                    public static final java.lang.String _INVALID_SEARCH_OPERATOR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SEARCH_OPERATOR");
                                
                                    public static final java.lang.String _INVALID_SEARCH_PAGE_INDEX =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SEARCH_PAGE_INDEX");
                                
                                    public static final java.lang.String _INVALID_SEARCH_PAGE_SIZE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SEARCH_PAGE_SIZE");
                                
                                    public static final java.lang.String _INVALID_SEARCH_PREF =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SEARCH_PREF");
                                
                                    public static final java.lang.String _INVALID_SEARCH_SELECT_OBJ =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SEARCH_SELECT_OBJ");
                                
                                    public static final java.lang.String _INVALID_SEARCH_TYPE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SEARCH_TYPE");
                                
                                    public static final java.lang.String _INVALID_SEARCH_VALUE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SEARCH_VALUE");
                                
                                    public static final java.lang.String _INVALID_SECONDARY_EMAIL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SECONDARY_EMAIL");
                                
                                    public static final java.lang.String _INVALID_SECPAY_CREDENTIALS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SECPAY_CREDENTIALS");
                                
                                    public static final java.lang.String _INVALID_SERIAL_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SERIAL_NUM");
                                
                                    public static final java.lang.String _INVALID_SERIAL_OR_LOT_NUMBER =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SERIAL_OR_LOT_NUMBER");
                                
                                    public static final java.lang.String _INVALID_SERVICE_CODE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SERVICE_CODE");
                                
                                    public static final java.lang.String _INVALID_SESSION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SESSION");
                                
                                    public static final java.lang.String _INVALID_SHIPPER_STATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SHIPPER_STATE");
                                
                                    public static final java.lang.String _INVALID_SHIP_DATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SHIP_DATE");
                                
                                    public static final java.lang.String _INVALID_SHIP_FROM_STATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SHIP_FROM_STATE");
                                
                                    public static final java.lang.String _INVALID_SHIP_GRP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SHIP_GRP");
                                
                                    public static final java.lang.String _INVALID_SHIP_SRVC =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SHIP_SRVC");
                                
                                    public static final java.lang.String _INVALID_SHIP_TO_SATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SHIP_TO_SATE");
                                
                                    public static final java.lang.String _INVALID_SITE_CSTM_FILE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SITE_CSTM_FILE");
                                
                                    public static final java.lang.String _INVALID_SOAP_HEADER =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SOAP_HEADER");
                                
                                    public static final java.lang.String _INVALID_SRCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SRCH");
                                
                                    public static final java.lang.String _INVALID_SRCH_CRITERIA =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SRCH_CRITERIA");
                                
                                    public static final java.lang.String _INVALID_SRCH_CSTM_FLD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SRCH_CSTM_FLD");
                                
                                    public static final java.lang.String _INVALID_SRCH_FUNCTN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SRCH_FUNCTN");
                                
                                    public static final java.lang.String _INVALID_SRCH_SORT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SRCH_SORT");
                                
                                    public static final java.lang.String _INVALID_SRCH_SUMMARY_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SRCH_SUMMARY_TYP");
                                
                                    public static final java.lang.String _INVALID_SRCH_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SRCH_TYP");
                                
                                    public static final java.lang.String _INVALID_SRVC_ITEM_SUB =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SRVC_ITEM_SUB");
                                
                                    public static final java.lang.String _INVALID_SSO =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SSO");
                                
                                    public static final java.lang.String _INVALID_SSS_DEBUG_SESSION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SSS_DEBUG_SESSION");
                                
                                    public static final java.lang.String _INVALID_STATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_STATE");
                                
                                    public static final java.lang.String _INVALID_STATUS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_STATUS");
                                
                                    public static final java.lang.String _INVALID_SUB =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SUB");
                                
                                    public static final java.lang.String _INVALID_SUBLIST_DESC =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SUBLIST_DESC");
                                
                                    public static final java.lang.String _INVALID_SUBSCRIPTION_STATUS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SUBSCRIPTION_STATUS");
                                
                                    public static final java.lang.String _INVALID_SUMMARY_SRCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SUMMARY_SRCH");
                                
                                    public static final java.lang.String _INVALID_SUPERVISOR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_SUPERVISOR");
                                
                                    public static final java.lang.String _INVALID_TASK_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TASK_ID");
                                
                                    public static final java.lang.String _INVALID_TAX_AMT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TAX_AMT");
                                
                                    public static final java.lang.String _INVALID_TAX_CODE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TAX_CODE");
                                
                                    public static final java.lang.String _INVALID_TAX_CODES =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TAX_CODES");
                                
                                    public static final java.lang.String _INVALID_TAX_CODE_FOR_SUB =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TAX_CODE_FOR_SUB");
                                
                                    public static final java.lang.String _INVALID_TAX_PMT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TAX_PMT");
                                
                                    public static final java.lang.String _INVALID_TAX_VALUE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TAX_VALUE");
                                
                                    public static final java.lang.String _INVALID_TIME_FORMAT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TIME_FORMAT");
                                
                                    public static final java.lang.String _INVALID_TO_DATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TO_DATE");
                                
                                    public static final java.lang.String _INVALID_TRACKING_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TRACKING_NUM");
                                
                                    public static final java.lang.String _INVALID_TRANS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TRANS");
                                
                                    public static final java.lang.String _INVALID_TRANSACTION_DATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TRANSACTION_DATE");
                                
                                    public static final java.lang.String _INVALID_TRANSACTIO_DATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TRANSACTIO_DATE");
                                
                                    public static final java.lang.String _INVALID_TRANS_AMT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TRANS_AMT");
                                
                                    public static final java.lang.String _INVALID_TRANS_COMPNT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TRANS_COMPNT");
                                
                                    public static final java.lang.String _INVALID_TRANS_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TRANS_ID");
                                
                                    public static final java.lang.String _INVALID_TRANS_SUB_ACCT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TRANS_SUB_ACCT");
                                
                                    public static final java.lang.String _INVALID_TRANS_SUB_CLASS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TRANS_SUB_CLASS");
                                
                                    public static final java.lang.String _INVALID_TRANS_SUB_DEPT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TRANS_SUB_DEPT");
                                
                                    public static final java.lang.String _INVALID_TRANS_SUB_ENTITY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TRANS_SUB_ENTITY");
                                
                                    public static final java.lang.String _INVALID_TRANS_SUB_ITEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TRANS_SUB_ITEM");
                                
                                    public static final java.lang.String _INVALID_TRANS_SUB_LOC =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TRANS_SUB_LOC");
                                
                                    public static final java.lang.String _INVALID_TRANS_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TRANS_TYP");
                                
                                    public static final java.lang.String _INVALID_TRAN_ITEM_LINE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TRAN_ITEM_LINE");
                                
                                    public static final java.lang.String _INVALID_TRIAL_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TRIAL_TYP");
                                
                                    public static final java.lang.String _INVALID_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_TYP");
                                
                                    public static final java.lang.String _INVALID_UNIT_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_UNIT_TYP");
                                
                                    public static final java.lang.String _INVALID_UNSUPRTD_RCRD_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_UNSUPRTD_RCRD_TYP");
                                
                                    public static final java.lang.String _INVALID_UPS_ACCT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_UPS_ACCT");
                                
                                    public static final java.lang.String _INVALID_UPS_PACKG_WEIGHT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_UPS_PACKG_WEIGHT");
                                
                                    public static final java.lang.String _INVALID_UPS_VALUES =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_UPS_VALUES");
                                
                                    public static final java.lang.String _INVALID_URL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_URL");
                                
                                    public static final java.lang.String _INVALID_URL_PARAM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_URL_PARAM");
                                
                                    public static final java.lang.String _INVALID_VALUE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_VALUE");
                                
                                    public static final java.lang.String _INVALID_VAT_AMOUNT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_VAT_AMOUNT");
                                
                                    public static final java.lang.String _INVALID_VAT_REGSTRTN_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_VAT_REGSTRTN_NUM");
                                
                                    public static final java.lang.String _INVALID_VSOE_ALLOCTN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_VSOE_ALLOCTN");
                                
                                    public static final java.lang.String _INVALID_WEBSITE_SECTION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_WEBSITE_SECTION");
                                
                                    public static final java.lang.String _INVALID_WO =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_WO");
                                
                                    public static final java.lang.String _INVALID_WORLDPAY_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_WORLDPAY_ID");
                                
                                    public static final java.lang.String _INVALID_WO_ITEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_WO_ITEM");
                                
                                    public static final java.lang.String _INVALID_WS_VERSION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_WS_VERSION");
                                
                                    public static final java.lang.String _INVALID_YEAR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_YEAR");
                                
                                    public static final java.lang.String _INVALID_YEAR_FORMAT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_YEAR_FORMAT");
                                
                                    public static final java.lang.String _INVALID_ZIP_CODE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ZIP_CODE");
                                
                                    public static final java.lang.String _INVALID_ZIP_FILE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ZIP_FILE");
                                
                                    public static final java.lang.String _INVALID_ZIP_POST_CODE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ZIP_POST_CODE");
                                
                                    public static final java.lang.String _INVENTORY_NUM_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVENTORY_NUM_DISALLWD");
                                
                                    public static final java.lang.String _INVLAID_BOOLEAN_VALUE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVLAID_BOOLEAN_VALUE");
                                
                                    public static final java.lang.String _DATA_RETRIEVAL_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("DATA_RETRIEVAL_ERROR");
                                
                                    public static final java.lang.String _IP_REQUEST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("IP_REQUEST");
                                
                                    public static final java.lang.String _ISSUE_ASSIGNEE_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ISSUE_ASSIGNEE_DISALLWD");
                                
                                    public static final java.lang.String _ISSUE_PRODUCT_VERSION_MISMATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ISSUE_PRODUCT_VERSION_MISMATCH");
                                
                                    public static final java.lang.String _ISSUE_VERSION_BUILD_MISMATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ISSUE_VERSION_BUILD_MISMATCH");
                                
                                    public static final java.lang.String _ITEM_ACCT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ITEM_ACCT_REQD");
                                
                                    public static final java.lang.String _ITEM_COUNT_MISMATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ITEM_COUNT_MISMATCH");
                                
                                    public static final java.lang.String _ITEM_IS_UNAVAILABLE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ITEM_IS_UNAVAILABLE");
                                
                                    public static final java.lang.String _ITEM_NAME_MUST_BE_UNIQUE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ITEM_NAME_MUST_BE_UNIQUE");
                                
                                    public static final java.lang.String _ITEM_NOT_UNIQUE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ITEM_NOT_UNIQUE");
                                
                                    public static final java.lang.String _ITEM_PARAM_REQD_IN_URL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ITEM_PARAM_REQD_IN_URL");
                                
                                    public static final java.lang.String _ITEM_QTY_AMT_MISMATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ITEM_QTY_AMT_MISMATCH");
                                
                                    public static final java.lang.String _ITEM_TYP_REQS_UNIT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ITEM_TYP_REQS_UNIT");
                                
                                    public static final java.lang.String _JE_AMOUNTS_MUST_BALANCE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("JE_AMOUNTS_MUST_BALANCE");
                                
                                    public static final java.lang.String _JE_LINE_MISSING_REQD_DATA =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("JE_LINE_MISSING_REQD_DATA");
                                
                                    public static final java.lang.String _JE_MAX_ONE_LINE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("JE_MAX_ONE_LINE");
                                
                                    public static final java.lang.String _JE_REV_REC_IN_PROGRESS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("JE_REV_REC_IN_PROGRESS");
                                
                                    public static final java.lang.String _JE_UNEXPECTED_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("JE_UNEXPECTED_ERROR");
                                
                                    public static final java.lang.String _JOB_NOT_COMPLETE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("JOB_NOT_COMPLETE");
                                
                                    public static final java.lang.String _JS_EXCEPTION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("JS_EXCEPTION");
                                
                                    public static final java.lang.String _KEY_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("KEY_REQD");
                                
                                    public static final java.lang.String _KPI_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("KPI_SETUP_REQD");
                                
                                    public static final java.lang.String _LABEL_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LABEL_REQD");
                                
                                    public static final java.lang.String _LANGUAGE_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LANGUAGE_SETUP_REQD");
                                
                                    public static final java.lang.String _LINKED_ACCT_DONT_MATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LINKED_ACCT_DONT_MATCH");
                                
                                    public static final java.lang.String _LINKED_ENTITIES_DONT_MATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LINKED_ENTITIES_DONT_MATCH");
                                
                                    public static final java.lang.String _LINKED_ITEMS_DONT_MATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LINKED_ITEMS_DONT_MATCH");
                                
                                    public static final java.lang.String _LINK_LINES_TO_ONE_ORD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LINK_LINES_TO_ONE_ORD");
                                
                                    public static final java.lang.String _LIST_ID_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LIST_ID_REQD");
                                
                                    public static final java.lang.String _LIST_KEY_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LIST_KEY_REQD");
                                
                                    public static final java.lang.String _LOCATIONS_IN_USE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LOCATIONS_IN_USE");
                                
                                    public static final java.lang.String _LOCATIONS_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LOCATIONS_SETUP_REQD");
                                
                                    public static final java.lang.String _LOCATION_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LOCATION_REQD");
                                
                                    public static final java.lang.String _LOCKED_DASHBOARD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LOCKED_DASHBOARD");
                                
                                    public static final java.lang.String _LOGIN_DISABLED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LOGIN_DISABLED");
                                
                                    public static final java.lang.String _LOGIN_DISABLED_PARTNER_CTR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LOGIN_DISABLED_PARTNER_CTR");
                                
                                    public static final java.lang.String _LOGIN_EMAIL_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LOGIN_EMAIL_REQD");
                                
                                    public static final java.lang.String _LOGIN_NAME_AND_PSWD_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LOGIN_NAME_AND_PSWD_REQD");
                                
                                    public static final java.lang.String _LOGIN_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LOGIN_REQD");
                                
                                    public static final java.lang.String _LOST_UPSELL_CRITERIA =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LOST_UPSELL_CRITERIA");
                                
                                    public static final java.lang.String _MACHN_LIST_KEY_NAMES_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MACHN_LIST_KEY_NAMES_REQD");
                                
                                    public static final java.lang.String _MANDATORY_PRD_TYPE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MANDATORY_PRD_TYPE_REQD");
                                
                                    public static final java.lang.String _MASS_UPDATE_CRITERIA_LOST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MASS_UPDATE_CRITERIA_LOST");
                                
                                    public static final java.lang.String _MATCHING_CUR_SUB_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MATCHING_CUR_SUB_REQD");
                                
                                    public static final java.lang.String _MATCHING_SERIAL_NUM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MATCHING_SERIAL_NUM_REQD");
                                
                                    public static final java.lang.String _MATRIX_INFO_TEMP_LOST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MATRIX_INFO_TEMP_LOST");
                                
                                    public static final java.lang.String _MATRIX_SUBITEM_NAME_TOO_LONG =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MATRIX_SUBITEM_NAME_TOO_LONG");
                                
                                    public static final java.lang.String _MAX_16_LINES_ALLWD_PER_BILLPAY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MAX_16_LINES_ALLWD_PER_BILLPAY");
                                
                                    public static final java.lang.String _MAX_200_LINES_ALLWD_ON_TRANS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MAX_200_LINES_ALLWD_ON_TRANS");
                                
                                    public static final java.lang.String _MAX_BARCODE_PRINT_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MAX_BARCODE_PRINT_EXCEEDED");
                                
                                    public static final java.lang.String _MAX_BULK_MERGE_RCRDS_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MAX_BULK_MERGE_RCRDS_EXCEEDED");
                                
                                    public static final java.lang.String _MAX_EMAILS_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MAX_EMAILS_EXCEEDED");
                                
                                    public static final java.lang.String _MAX_RCRDS_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MAX_RCRDS_EXCEEDED");
                                
                                    public static final java.lang.String _MAX_VALUES_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MAX_VALUES_EXCEEDED");
                                
                                    public static final java.lang.String _MEDIA_FILE_INVALID_JSCRIPT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MEDIA_FILE_INVALID_JSCRIPT");
                                
                                    public static final java.lang.String _MEDIA_NOT_FOUND =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MEDIA_NOT_FOUND");
                                
                                    public static final java.lang.String _MEDIA_NOT_INITIALIZED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MEDIA_NOT_INITIALIZED");
                                
                                    public static final java.lang.String _MEMORIZED_TRANS_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MEMORIZED_TRANS_ERROR");
                                
                                    public static final java.lang.String _MERGE_OPERATION_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MERGE_OPERATION_DISALLWD");
                                
                                    public static final java.lang.String _MERGE_RCRD_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MERGE_RCRD_REQD");
                                
                                    public static final java.lang.String _METAVANTE_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("METAVANTE_ERROR");
                                
                                    public static final java.lang.String _METAVANTE_SECRET_ANSWER_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("METAVANTE_SECRET_ANSWER_REQD");
                                
                                    public static final java.lang.String _METAVANTE_SECRET_QESTION_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("METAVANTE_SECRET_QESTION_REQD");
                                
                                    public static final java.lang.String _METAVANTE_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("METAVANTE_SETUP_REQD");
                                
                                    public static final java.lang.String _METAVANTE_TEMP_UNAVAILBL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("METAVANTE_TEMP_UNAVAILBL");
                                
                                    public static final java.lang.String _METHOD_NAME_CANNOT_BE_EMPTY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("METHOD_NAME_CANNOT_BE_EMPTY");
                                
                                    public static final java.lang.String _MISMATCHED_CURRENCY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MISMATCHED_CURRENCY");
                                
                                    public static final java.lang.String _MISMATCHED_QTY_PRICING =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MISMATCHED_QTY_PRICING");
                                
                                    public static final java.lang.String _MISMATCHED_SEARCH_PARENTHESIS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MISMATCHED_SEARCH_PARENTHESIS");
                                
                                    public static final java.lang.String _MISMATCH_EVENT_ISSUE_STATUS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MISMATCH_EVENT_ISSUE_STATUS");
                                
                                    public static final java.lang.String _MISMATCH_ISSUE_PRODUCT_VERSION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MISMATCH_ISSUE_PRODUCT_VERSION");
                                
                                    public static final java.lang.String _MISMATCH_SALES_CONTRIBUTION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MISMATCH_SALES_CONTRIBUTION");
                                
                                    public static final java.lang.String _MISSING_ACCT_PRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MISSING_ACCT_PRD");
                                
                                    public static final java.lang.String _MISSING_CRNCY_EXCH_RATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MISSING_CRNCY_EXCH_RATE");
                                
                                    public static final java.lang.String _MISSING_ENUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MISSING_ENUM");
                                
                                    public static final java.lang.String _MISSING_REQD_FLD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MISSING_REQD_FLD");
                                
                                    public static final java.lang.String _MISSNG_ACCT_PRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MISSNG_ACCT_PRD");
                                
                                    public static final java.lang.String _MISSNG_REV_REC_RCRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MISSNG_REV_REC_RCRD");
                                
                                    public static final java.lang.String _MISSNG_SO_REV_REC_PARAMS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MISSNG_SO_REV_REC_PARAMS");
                                
                                    public static final java.lang.String _MISSNG_SO_START_END_DATES =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MISSNG_SO_START_END_DATES");
                                
                                    public static final java.lang.String _MLI_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MLI_REQD");
                                
                                    public static final java.lang.String _MLTPLE_TAX_LINES_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MLTPLE_TAX_LINES_DISALLWD");
                                
                                    public static final java.lang.String _MSNG_FIELD_OWRTE_MUST_BE_TRUE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MSNG_FIELD_OWRTE_MUST_BE_TRUE");
                                
                                    public static final java.lang.String _MST_UPDATE_ITEMS_THEN_RATES =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MST_UPDATE_ITEMS_THEN_RATES");
                                
                                    public static final java.lang.String _MULTISELECT_TYPE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MULTISELECT_TYPE_REQD");
                                
                                    public static final java.lang.String _MULTI_ACCT_CANT_CHANGE_PSWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MULTI_ACCT_CANT_CHANGE_PSWD");
                                
                                    public static final java.lang.String _MULTI_LOC_INVT_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MULTI_LOC_INVT_ERROR");
                                
                                    public static final java.lang.String _MULTI_PRIMARY_PARTNER_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MULTI_PRIMARY_PARTNER_DISALLWD");
                                
                                    public static final java.lang.String _MULTI_SHIP_ROUTES_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MULTI_SHIP_ROUTES_REQD");
                                
                                    public static final java.lang.String _MUST_DEFINE_BASE_UNIT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MUST_DEFINE_BASE_UNIT");
                                
                                    public static final java.lang.String _MUST_RESUBMIT_RCRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MUST_RESUBMIT_RCRD");
                                
                                    public static final java.lang.String _NAME_ALREADY_IN_USE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NAME_ALREADY_IN_USE");
                                
                                    public static final java.lang.String _NAME_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NAME_REQD");
                                
                                    public static final java.lang.String _NAME_TYPE_FLDR_FIELDS_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NAME_TYPE_FLDR_FIELDS_REQD");
                                
                                    public static final java.lang.String _NARROW_KEYWORD_SEARCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NARROW_KEYWORD_SEARCH");
                                
                                    public static final java.lang.String _NEED_BILL_VARIANCE_ACCT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NEED_BILL_VARIANCE_ACCT");
                                
                                    public static final java.lang.String _NEGATIVE_PAYMENT_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NEGATIVE_PAYMENT_DISALLWD");
                                
                                    public static final java.lang.String _NEGATIVE_TAX_RATE_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NEGATIVE_TAX_RATE_DISALLWD");
                                
                                    public static final java.lang.String _NEITHER_ARGUMENT_DEFINED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NEITHER_ARGUMENT_DEFINED");
                                
                                    public static final java.lang.String _NEW_CONNECTION_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NEW_CONNECTION_DISALLWD");
                                
                                    public static final java.lang.String _NEXUS_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NEXUS_REQD");
                                
                                    public static final java.lang.String _NONMATCHING_EMAILS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NONMATCHING_EMAILS");
                                
                                    public static final java.lang.String _NONUNIQUE_INDEX_VALUE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NONUNIQUE_INDEX_VALUE");
                                
                                    public static final java.lang.String _NONZERO_AMT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NONZERO_AMT_REQD");
                                
                                    public static final java.lang.String _NONZERO_QTY_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NONZERO_QTY_REQD");
                                
                                    public static final java.lang.String _NONZERO_WEIGHT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NONZERO_WEIGHT_REQD");
                                
                                    public static final java.lang.String _NON_ADMIN_CANNOT_INITIATE_LINK =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NON_ADMIN_CANNOT_INITIATE_LINK");
                                
                                    public static final java.lang.String _NOT_AN_INVITEE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NOT_AN_INVITEE");
                                
                                    public static final java.lang.String _NOT_IN_INVT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NOT_IN_INVT");
                                
                                    public static final java.lang.String _NO_DATA_FOUND =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NO_DATA_FOUND");
                                
                                    public static final java.lang.String _NO_EXPENSES_FOR_PRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NO_EXPENSES_FOR_PRD");
                                
                                    public static final java.lang.String _NO_ITEMS_TO_PRINT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NO_ITEMS_TO_PRINT");
                                
                                    public static final java.lang.String _NO_MASS_UPDATES_RUNNING =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NO_MASS_UPDATES_RUNNING");
                                
                                    public static final java.lang.String _NO_MTRX_ITEMS_TO_UPDATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NO_MTRX_ITEMS_TO_UPDATE");
                                
                                    public static final java.lang.String _NO_ORD_SHPMNT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NO_ORD_SHPMNT");
                                
                                    public static final java.lang.String _NO_RCRDS_MATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NO_RCRDS_MATCH");
                                
                                    public static final java.lang.String _NO_RCRD_FOR_USER =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NO_RCRD_FOR_USER");
                                
                                    public static final java.lang.String _NO_SCHDUL_APPLIED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NO_SCHDUL_APPLIED");
                                
                                    public static final java.lang.String _NO_UPSERT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NO_UPSERT");
                                
                                    public static final java.lang.String _NULL_CHECK_NUMBER =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NULL_CHECK_NUMBER");
                                
                                    public static final java.lang.String _NUMERIC_CHECK_NUM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NUMERIC_CHECK_NUM_REQD");
                                
                                    public static final java.lang.String _NUMERIC_REF_NUM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NUMERIC_REF_NUM_REQD");
                                
                                    public static final java.lang.String _NUM_ITEMS_GRTR_THAN_QTY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NUM_ITEMS_GRTR_THAN_QTY");
                                
                                    public static final java.lang.String _NUM_ITEMS_NOT_EQUAL_TO_QTY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NUM_ITEMS_NOT_EQUAL_TO_QTY");
                                
                                    public static final java.lang.String _NUM_REQD_FOR_FIRST_LABEL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("NUM_REQD_FOR_FIRST_LABEL");
                                
                                    public static final java.lang.String _OI_FEATURE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("OI_FEATURE_REQD");
                                
                                    public static final java.lang.String _OI_PERMISSION_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("OI_PERMISSION_REQD");
                                
                                    public static final java.lang.String _ONE_ADMIN_REQD_PER_ACCT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONE_ADMIN_REQD_PER_ACCT");
                                
                                    public static final java.lang.String _ONE_DIRECT_DEPOSIT_ACT_ALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONE_DIRECT_DEPOSIT_ACT_ALLWD");
                                
                                    public static final java.lang.String _ONE_EMPL_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONE_EMPL_REQD");
                                
                                    public static final java.lang.String _ONE_PAY_ITEM_PER_EMPL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONE_PAY_ITEM_PER_EMPL");
                                
                                    public static final java.lang.String _ONE_POSITIVE_VALUE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONE_POSITIVE_VALUE_REQD");
                                
                                    public static final java.lang.String _ONE_RCRD_REQD_FOR_MASS_UPDATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONE_RCRD_REQD_FOR_MASS_UPDATE");
                                
                                    public static final java.lang.String _ONE_ROLE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONE_ROLE_REQD");
                                
                                    public static final java.lang.String _ONLINE_BANK_FILE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONLINE_BANK_FILE_REQD");
                                
                                    public static final java.lang.String _ONLINE_BILL_PAY_DUP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONLINE_BILL_PAY_DUP");
                                
                                    public static final java.lang.String _ONLINE_BILL_PAY_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONLINE_BILL_PAY_SETUP_REQD");
                                
                                    public static final java.lang.String _ONLINE_FORM_DSNT_EXIST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONLINE_FORM_DSNT_EXIST");
                                
                                    public static final java.lang.String _ONLINE_FORM_EMPTY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONLINE_FORM_EMPTY");
                                
                                    public static final java.lang.String _ONLINE_FORM_ID_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONLINE_FORM_ID_REQD");
                                
                                    public static final java.lang.String _ONLINE_FORM_USER_ACCESS_ONLY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONLINE_FORM_USER_ACCESS_ONLY");
                                
                                    public static final java.lang.String _ONLINE_ORD_FEATURE_DISABLED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONLINE_ORD_FEATURE_DISABLED");
                                
                                    public static final java.lang.String _ONLY_ONE_CONTRIB_ITEM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONLY_ONE_CONTRIB_ITEM_REQD");
                                
                                    public static final java.lang.String _ONLY_ONE_DEDCT_ITEM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONLY_ONE_DEDCT_ITEM_REQD");
                                
                                    public static final java.lang.String _ONLY_ONE_DISTRIB_ALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONLY_ONE_DISTRIB_ALLWD");
                                
                                    public static final java.lang.String _ONLY_ONE_EARNING_ITEM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONLY_ONE_EARNING_ITEM_REQD");
                                
                                    public static final java.lang.String _ONLY_ONE_LOT_NUM_ALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONLY_ONE_LOT_NUM_ALLWD");
                                
                                    public static final java.lang.String _ONLY_ONE_PREF_BIN_ALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONLY_ONE_PREF_BIN_ALLWD");
                                
                                    public static final java.lang.String _ONLY_ONE_UNIT_AS_BASE_UNIT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONLY_ONE_UNIT_AS_BASE_UNIT");
                                
                                    public static final java.lang.String _ONLY_ONE_UPLOAD_ALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONLY_ONE_UPLOAD_ALLWD");
                                
                                    public static final java.lang.String _ONLY_ONE_WITHLD_ITEM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ONLY_ONE_WITHLD_ITEM_REQD");
                                
                                    public static final java.lang.String _OPENID_DOMAIN_IN_USE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("OPENID_DOMAIN_IN_USE");
                                
                                    public static final java.lang.String _OPENID_NOT_ENABLED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("OPENID_NOT_ENABLED");
                                
                                    public static final java.lang.String _OPERATOR_ARITY_MISMATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("OPERATOR_ARITY_MISMATCH");
                                
                                    public static final java.lang.String _OPRTN_UNAVAILBL_TO_GATEWAY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("OPRTN_UNAVAILBL_TO_GATEWAY");
                                
                                    public static final java.lang.String _ORDER_DSNT_EXIST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ORDER_DSNT_EXIST");
                                
                                    public static final java.lang.String _ORD_ALREADY_APPRVD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ORD_ALREADY_APPRVD");
                                
                                    public static final java.lang.String _OTHER_PMT_AUTH_IN_PROGRESS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("OTHER_PMT_AUTH_IN_PROGRESS");
                                
                                    public static final java.lang.String _OVERAGE_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("OVERAGE_DISALLWD");
                                
                                    public static final java.lang.String _OVERLAPPING_PRDS_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("OVERLAPPING_PRDS_DISALLWD");
                                
                                    public static final java.lang.String _OVER_FULFILL_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("OVER_FULFILL_DISALLWD");
                                
                                    public static final java.lang.String _OVER_FULFILL_RECEIV_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("OVER_FULFILL_RECEIV_DISALLWD");
                                
                                    public static final java.lang.String _OWNER_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("OWNER_REQD");
                                
                                    public static final java.lang.String _PACKAGE_WEIGHT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PACKAGE_WEIGHT_REQD");
                                
                                    public static final java.lang.String _PACKG_LEVEL_REF_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PACKG_LEVEL_REF_DISALLWD");
                                
                                    public static final java.lang.String _PACKG_VALUE_TOO_LARGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PACKG_VALUE_TOO_LARGE");
                                
                                    public static final java.lang.String _PARENT_CANT_ITSELF_BE_MEMBER =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PARENT_CANT_ITSELF_BE_MEMBER");
                                
                                    public static final java.lang.String _PARENT_MUST_BE_MATRIX_ITEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PARENT_MUST_BE_MATRIX_ITEM");
                                
                                    public static final java.lang.String _PARENT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PARENT_REQD");
                                
                                    public static final java.lang.String _PARSING_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PARSING_ERROR");
                                
                                    public static final java.lang.String _PARTIAL_FULFILL_RCEIV_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PARTIAL_FULFILL_RCEIV_DISALLWD");
                                
                                    public static final java.lang.String _PARTNER_ACCESS_DENIED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PARTNER_ACCESS_DENIED");
                                
                                    public static final java.lang.String _PARTNER_ACCT_NOT_LINKED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PARTNER_ACCT_NOT_LINKED");
                                
                                    public static final java.lang.String _PARTNER_CODE_ALREADY_USED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PARTNER_CODE_ALREADY_USED");
                                
                                    public static final java.lang.String _PAYCHECK_ALREADY_PAID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAYCHECK_ALREADY_PAID");
                                
                                    public static final java.lang.String _PAYCHECK_IN_USE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAYCHECK_IN_USE");
                                
                                    public static final java.lang.String _PAYEE_REQD_FOR_PMT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAYEE_REQD_FOR_PMT");
                                
                                    public static final java.lang.String _PAYPAL_FUND_SOURCE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAYPAL_FUND_SOURCE_REQD");
                                
                                    public static final java.lang.String _PAYPAL_INVALID_PMT_METHOD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAYPAL_INVALID_PMT_METHOD");
                                
                                    public static final java.lang.String _PAYPAL_PMT_NOTIFICATION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAYPAL_PMT_NOTIFICATION");
                                
                                    public static final java.lang.String _PAYPAL_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAYPAL_SETUP_REQD");
                                
                                    public static final java.lang.String _PAYROLL_COMMITTED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAYROLL_COMMITTED");
                                
                                    public static final java.lang.String _PAYROLL_EXPENSE_ACCT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAYROLL_EXPENSE_ACCT_REQD");
                                
                                    public static final java.lang.String _PAYROLL_FEATURE_DISABLED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAYROLL_FEATURE_DISABLED");
                                
                                    public static final java.lang.String _PAYROLL_FEATURE_UNAVAILABLE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAYROLL_FEATURE_UNAVAILABLE");
                                
                                    public static final java.lang.String _PAYROLL_FUND_ACCT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAYROLL_FUND_ACCT_REQD");
                                
                                    public static final java.lang.String _PAYROLL_IN_PROCESS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAYROLL_IN_PROCESS");
                                
                                    public static final java.lang.String _PAYROLL_ITEM_DELETE_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAYROLL_ITEM_DELETE_DISALLWD");
                                
                                    public static final java.lang.String _PAYROLL_LIABILITY_ACCT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAYROLL_LIABILITY_ACCT_REQD");
                                
                                    public static final java.lang.String _PAYROLL_MAINTENANCE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAYROLL_MAINTENANCE");
                                
                                    public static final java.lang.String _PAYROLL_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAYROLL_SETUP_REQD");
                                
                                    public static final java.lang.String _PAYROLL_UPDATE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAYROLL_UPDATE_REQD");
                                
                                    public static final java.lang.String _PAY_HOLD_ON_SO =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PAY_HOLD_ON_SO");
                                
                                    public static final java.lang.String _PERMISSION_VIOLATION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PERMISSION_VIOLATION");
                                
                                    public static final java.lang.String _PHONE_NUM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PHONE_NUM_REQD");
                                
                                    public static final java.lang.String _PIN_DEBIT_TRANS_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PIN_DEBIT_TRANS_DISALLWD");
                                
                                    public static final java.lang.String _PLAN_IN_USE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PLAN_IN_USE");
                                
                                    public static final java.lang.String _PLAN_OVERLAP_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PLAN_OVERLAP_DISALLWD");
                                
                                    public static final java.lang.String _PMT_ALREADY_APPRVD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PMT_ALREADY_APPRVD");
                                
                                    public static final java.lang.String _PMT_ALREADY_EXISTS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PMT_ALREADY_EXISTS");
                                
                                    public static final java.lang.String _PMT_ALREADY_SBMTD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PMT_ALREADY_SBMTD");
                                
                                    public static final java.lang.String _PMT_EDIT_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PMT_EDIT_DISALLWD");
                                
                                    public static final java.lang.String _POSITIVE_BIN_QTY_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("POSITIVE_BIN_QTY_REQD");
                                
                                    public static final java.lang.String _POSITIVE_QTY_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("POSITIVE_QTY_REQD");
                                
                                    public static final java.lang.String _POSITIVE_UNITCOST_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("POSITIVE_UNITCOST_REQD");
                                
                                    public static final java.lang.String _POSTING_DISCOUNT_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("POSTING_DISCOUNT_DISALLWD");
                                
                                    public static final java.lang.String _POSTING_PRD_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("POSTING_PRD_SETUP_REQD");
                                
                                    public static final java.lang.String _PRDS_DISALLWD_NAMES_NOT_UNIQUE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PRDS_DISALLWD_NAMES_NOT_UNIQUE");
                                
                                    public static final java.lang.String _PRD_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PRD_SETUP_REQD");
                                
                                    public static final java.lang.String _PREFERRED_TAX_AGENCY_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PREFERRED_TAX_AGENCY_REQD");
                                
                                    public static final java.lang.String _PREF_VENDOR_COST_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PREF_VENDOR_COST_REQD");
                                
                                    public static final java.lang.String _PREF_VENDOR_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PREF_VENDOR_REQD");
                                
                                    public static final java.lang.String _PRIVATE_RCRD_ACCESS_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PRIVATE_RCRD_ACCESS_DISALLWD");
                                
                                    public static final java.lang.String _PRIVATE_STATUS_CHNG_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PRIVATE_STATUS_CHNG_DISALLWD");
                                
                                    public static final java.lang.String _PRODUCT_MODULE_MISMATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PRODUCT_MODULE_MISMATCH");
                                
                                    public static final java.lang.String _PSWD_EXPIRED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PSWD_EXPIRED");
                                
                                    public static final java.lang.String _PSWD_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PSWD_REQD");
                                
                                    public static final java.lang.String _PWSDS_DONT_MATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PWSDS_DONT_MATCH");
                                
                                    public static final java.lang.String _QTY_EXCEEDED_QTY_BUCKETS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("QTY_EXCEEDED_QTY_BUCKETS");
                                
                                    public static final java.lang.String _QTY_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("QTY_REQD");
                                
                                    public static final java.lang.String _RATE_REQUEST_SHPMNT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("RATE_REQUEST_SHPMNT_REQD");
                                
                                    public static final java.lang.String _RATE_SRVC_UNAVAILBL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("RATE_SRVC_UNAVAILBL");
                                
                                    public static final java.lang.String _RCRD_DELETED_SINCE_RETRIEVED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("RCRD_DELETED_SINCE_RETRIEVED");
                                
                                    public static final java.lang.String _RCRD_DSNT_EXIST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("RCRD_DSNT_EXIST");
                                
                                    public static final java.lang.String _RCRD_EDITED_SINCE_RETRIEVED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("RCRD_EDITED_SINCE_RETRIEVED");
                                
                                    public static final java.lang.String _RCRD_HAS_BEEN_CHANGED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("RCRD_HAS_BEEN_CHANGED");
                                
                                    public static final java.lang.String _RCRD_ID_NOT_INT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("RCRD_ID_NOT_INT");
                                
                                    public static final java.lang.String _RCRD_LOCKED_BY_WF =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("RCRD_LOCKED_BY_WF");
                                
                                    public static final java.lang.String _RCRD_NOT_FOUND =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("RCRD_NOT_FOUND");
                                
                                    public static final java.lang.String _RCRD_PREVSLY_DELETED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("RCRD_PREVSLY_DELETED");
                                
                                    public static final java.lang.String _RCRD_REF_RCRD_TYP_MISMATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("RCRD_REF_RCRD_TYP_MISMATCH");
                                
                                    public static final java.lang.String _RCRD_SUB_MISMATCH_WITH_CLASS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("RCRD_SUB_MISMATCH_WITH_CLASS");
                                
                                    public static final java.lang.String _RCRD_TYPE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("RCRD_TYPE_REQD");
                                
                                    public static final java.lang.String _RCRD_UNEDITABLE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("RCRD_UNEDITABLE");
                                
                                    public static final java.lang.String _REACHED_LIST_END =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("REACHED_LIST_END");
                                
                                    public static final java.lang.String _REACHED_LIST_START =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("REACHED_LIST_START");
                                
                                    public static final java.lang.String _RECALCING_PLAN_SCHDUL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("RECALCING_PLAN_SCHDUL");
                                
                                    public static final java.lang.String _RECURSV_REF_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("RECURSV_REF_DISALLWD");
                                
                                    public static final java.lang.String _RECUR_EVENT_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("RECUR_EVENT_DISALLWD");
                                
                                    public static final java.lang.String _REC_TYP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("REC_TYP_REQD");
                                
                                    public static final java.lang.String _REPORT_EXPIRED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("REPORT_EXPIRED");
                                
                                    public static final java.lang.String _REQD_FORM_TAG_MISSING =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("REQD_FORM_TAG_MISSING");
                                
                                    public static final java.lang.String _REQD_LOC_FIELDS_MISSING =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("REQD_LOC_FIELDS_MISSING");
                                
                                    public static final java.lang.String _REQD_SUB_FIELDS_MISSING =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("REQD_SUB_FIELDS_MISSING");
                                
                                    public static final java.lang.String _REQUEST_PARAM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("REQUEST_PARAM_REQD");
                                
                                    public static final java.lang.String _REVERSAL_DATE_WARNING =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("REVERSAL_DATE_WARNING");
                                
                                    public static final java.lang.String _REV_REC_DATE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("REV_REC_DATE_REQD");
                                
                                    public static final java.lang.String _REV_REC_TMPLT_DATA_MISSING =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("REV_REC_TMPLT_DATA_MISSING");
                                
                                    public static final java.lang.String _REV_REC_UPDATE_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("REV_REC_UPDATE_DISALLWD");
                                
                                    public static final java.lang.String _ROLE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ROLE_REQD");
                                
                                    public static final java.lang.String _ROLE_REQUIRED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ROLE_REQUIRED");
                                
                                    public static final java.lang.String _ROUNDING_DIFF_TOO_BIG =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ROUNDING_DIFF_TOO_BIG");
                                
                                    public static final java.lang.String _ROUNDING_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ROUNDING_ERROR");
                                
                                    public static final java.lang.String _ROUTING_NUM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ROUTING_NUM_REQD");
                                
                                    public static final java.lang.String _SALES_DISCOUNT_ACCT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SALES_DISCOUNT_ACCT_REQD");
                                
                                    public static final java.lang.String _SAME_ACCT_TYP_REQD_FOR_PARENT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SAME_ACCT_TYP_REQD_FOR_PARENT");
                                
                                    public static final java.lang.String _SAVED_SRCH_EMAIL_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SAVED_SRCH_EMAIL_ERROR");
                                
                                    public static final java.lang.String _SCHDUL_EDIT_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SCHDUL_EDIT_DISALLWD");
                                
                                    public static final java.lang.String _SCHEDULED_REPORT_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SCHEDULED_REPORT_ERROR");
                                
                                    public static final java.lang.String _SEARCH_DATE_FILTER_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SEARCH_DATE_FILTER_REQD");
                                
                                    public static final java.lang.String _SEARCH_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SEARCH_ERROR");
                                
                                    public static final java.lang.String _SEARCH_INTEGER_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SEARCH_INTEGER_REQD");
                                
                                    public static final java.lang.String _SEARCH_TIMED_OUT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SEARCH_TIMED_OUT");
                                
                                    public static final java.lang.String _SECURE_TRANS_REQD_ON_CHECKOUT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SECURE_TRANS_REQD_ON_CHECKOUT");
                                
                                    public static final java.lang.String _SELECT_OPTION_ALREADY_PRESENT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SELECT_OPTION_ALREADY_PRESENT");
                                
                                    public static final java.lang.String _SELECT_OPTION_NOT_FOUND =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SELECT_OPTION_NOT_FOUND");
                                
                                    public static final java.lang.String _SERIAL_NUM_MATCH_MULTI_ITEMS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SERIAL_NUM_MATCH_MULTI_ITEMS");
                                
                                    public static final java.lang.String _SESSION_TERMD_2ND_LOGIN_DECTD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SESSION_TERMD_2ND_LOGIN_DECTD");
                                
                                    public static final java.lang.String _SESSION_TIMED_OUT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SESSION_TIMED_OUT");
                                
                                    public static final java.lang.String _SETUP_METER_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SETUP_METER_REQD");
                                
                                    public static final java.lang.String _SET_SHIPPING_PICKUP_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SET_SHIPPING_PICKUP_TYP");
                                
                                    public static final java.lang.String _SHIPFROM_ADDRESS_NOT_SET =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SHIPFROM_ADDRESS_NOT_SET");
                                
                                    public static final java.lang.String _SHIPMNT_INSURANCE_NOT_AVAILABLE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SHIPMNT_INSURANCE_NOT_AVAILABLE");
                                
                                    public static final java.lang.String _SHIP_ADDR_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SHIP_ADDR_REQD");
                                
                                    public static final java.lang.String _SHIP_MANIFEST_ALREADY_PRCSSD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SHIP_MANIFEST_ALREADY_PRCSSD");
                                
                                    public static final java.lang.String _SHIP_MANIFEST_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SHIP_MANIFEST_ERROR");
                                
                                    public static final java.lang.String _SHIP_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SHIP_SETUP_REQD");
                                
                                    public static final java.lang.String _SHIP_TALBE_UNBALNCD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SHIP_TALBE_UNBALNCD");
                                
                                    public static final java.lang.String _SHIP_USER_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SHIP_USER_ERROR");
                                
                                    public static final java.lang.String _SINGLE_VALUE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SINGLE_VALUE_REQD");
                                
                                    public static final java.lang.String _SITEMAP_GEN_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SITEMAP_GEN_ERROR");
                                
                                    public static final java.lang.String _SITE_DOMAIN_NAME_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SITE_DOMAIN_NAME_REQD");
                                
                                    public static final java.lang.String _SITE_TAG_ALREADY_EXISTS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SITE_TAG_ALREADY_EXISTS");
                                
                                    public static final java.lang.String _SO_HAS_CHILD_TRANS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SO_HAS_CHILD_TRANS");
                                
                                    public static final java.lang.String _SO_LINE_HAS_PO =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SO_LINE_HAS_PO");
                                
                                    public static final java.lang.String _SRVC_UNAVAILBL_FOR_LOC =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SRVC_UNAVAILBL_FOR_LOC");
                                
                                    public static final java.lang.String _SSS_AUTHOR_MUST_BE_EMPLOYEE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_AUTHOR_MUST_BE_EMPLOYEE");
                                
                                    public static final java.lang.String _SSS_CONNECTION_TIME_OUT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_CONNECTION_TIME_OUT");
                                
                                    public static final java.lang.String _SSS_DEBUG_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_DEBUG_DISALLWD");
                                
                                    public static final java.lang.String _SSS_DRIP_EMAIL_RAN_OUT_OF_COUPON_CODES =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_DRIP_EMAIL_RAN_OUT_OF_COUPON_CODES");
                                
                                    public static final java.lang.String _SSS_DUP_DRIP_EMAIL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_DUP_DRIP_EMAIL");
                                
                                    public static final java.lang.String _SSS_FILE_SIZE_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_FILE_SIZE_EXCEEDED");
                                
                                    public static final java.lang.String _SSS_INSTRUCTION_COUNT_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INSTRUCTION_COUNT_EXCEEDED");
                                
                                    public static final java.lang.String _SSS_INVALID_ATTACH_RECORD_TYPE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_ATTACH_RECORD_TYPE");
                                
                                    public static final java.lang.String _SSS_INVALID_BCC_EMAIL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_BCC_EMAIL");
                                
                                    public static final java.lang.String _SSS_INVALID_CC_EMAIL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_CC_EMAIL");
                                
                                    public static final java.lang.String _SSS_INVALID_CMPGN_EVENT_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_CMPGN_EVENT_ID");
                                
                                    public static final java.lang.String _SSS_INVALID_EMAIL_TEMPLATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_EMAIL_TEMPLATE");
                                
                                    public static final java.lang.String _SSS_INVALID_FORM_ELEMENT_NAME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_FORM_ELEMENT_NAME");
                                
                                    public static final java.lang.String _SSS_INVALID_GSO_FLTR_OPRTOR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_GSO_FLTR_OPRTOR");
                                
                                    public static final java.lang.String _SSS_INVALID_HEADER =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_HEADER");
                                
                                    public static final java.lang.String _SSS_INVALID_HOST_CERT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_HOST_CERT");
                                
                                    public static final java.lang.String _SSS_INVALID_LIST_COLUMN_NAME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_LIST_COLUMN_NAME");
                                
                                    public static final java.lang.String _SSS_INVALID_LOCK_WAIT_TIME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_LOCK_WAIT_TIME");
                                
                                    public static final java.lang.String _SSS_INVALID_LOG_TYPE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_LOG_TYPE");
                                
                                    public static final java.lang.String _SSS_INVALID_PORTLET_INTERVAL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_PORTLET_INTERVAL");
                                
                                    public static final java.lang.String _SSS_INVALID_SCRIPTLET_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_SCRIPTLET_ID");
                                
                                    public static final java.lang.String _SSS_INVALID_SRCH_COL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_SRCH_COL");
                                
                                    public static final java.lang.String _SSS_INVALID_SRCH_COLUMN_JOIN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_SRCH_COLUMN_JOIN");
                                
                                    public static final java.lang.String _SSS_INVALID_SRCH_COLUMN_SUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_SRCH_COLUMN_SUM");
                                
                                    public static final java.lang.String _SSS_INVALID_SRCH_FILTER =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_SRCH_FILTER");
                                
                                    public static final java.lang.String _SSS_INVALID_SRCH_FILTER_JOIN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_SRCH_FILTER_JOIN");
                                
                                    public static final java.lang.String _SSS_INVALID_SRCH_OPERATOR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_SRCH_OPERATOR");
                                
                                    public static final java.lang.String _SSS_INVALID_SUBLIST_OPERATION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_SUBLIST_OPERATION");
                                
                                    public static final java.lang.String _SSS_INVALID_SUBMIT_OPTION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_SUBMIT_OPTION");
                                
                                    public static final java.lang.String _SSS_INVALID_TYPE_ARG =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_TYPE_ARG");
                                
                                    public static final java.lang.String _SSS_INVALID_UI_OBJECT_TYPE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_UI_OBJECT_TYPE");
                                
                                    public static final java.lang.String _SSS_INVALID_URL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_URL");
                                
                                    public static final java.lang.String _SSS_INVALID_URL_CATEGORY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_URL_CATEGORY");
                                
                                    public static final java.lang.String _SSS_INVALID_WF_RCRD_TYPE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_WF_RCRD_TYPE");
                                
                                    public static final java.lang.String _SSS_INVALID_XML_SCHEMA_OR_DEPENDENCY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_INVALID_XML_SCHEMA_OR_DEPENDENCY");
                                
                                    public static final java.lang.String _SSS_MEMORY_USAGE_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_MEMORY_USAGE_EXCEEDED");
                                
                                    public static final java.lang.String _SSS_METHOD_NOT_IMPLEMENTED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_METHOD_NOT_IMPLEMENTED");
                                
                                    public static final java.lang.String _SSS_MISSING_REQD_ARGUMENT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_MISSING_REQD_ARGUMENT");
                                
                                    public static final java.lang.String _SSS_QUEUE_LIMIT_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_QUEUE_LIMIT_EXCEEDED");
                                
                                    public static final java.lang.String _SSS_RECORD_TYPE_MISMATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_RECORD_TYPE_MISMATCH");
                                
                                    public static final java.lang.String _SSS_REQUEST_LIMIT_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_REQUEST_LIMIT_EXCEEDED");
                                
                                    public static final java.lang.String _SSS_REQUEST_TIME_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_REQUEST_TIME_EXCEEDED");
                                
                                    public static final java.lang.String _SSS_SCRIPT_DESERIALIZATION_FAILURE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_SCRIPT_DESERIALIZATION_FAILURE");
                                
                                    public static final java.lang.String _SSS_SCRIPT_SECURITY_VIOLATION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_SCRIPT_SECURITY_VIOLATION");
                                
                                    public static final java.lang.String _SSS_SSO_CONFIG_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_SSO_CONFIG_REQD");
                                
                                    public static final java.lang.String _SSS_STACK_FRAME_DEPTH_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_STACK_FRAME_DEPTH_EXCEEDED");
                                
                                    public static final java.lang.String _SSS_TIME_LIMIT_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_TIME_LIMIT_EXCEEDED");
                                
                                    public static final java.lang.String _SSS_TRANSACTION_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_TRANSACTION_REQD");
                                
                                    public static final java.lang.String _SSS_TRANS_IN_PROGRESS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_TRANS_IN_PROGRESS");
                                
                                    public static final java.lang.String _SSS_UNKNOWN_HOST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_UNKNOWN_HOST");
                                
                                    public static final java.lang.String _SSS_USAGE_LIMIT_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_USAGE_LIMIT_EXCEEDED");
                                
                                    public static final java.lang.String _SSS_XML_DOES_NOT_CONFORM_TO_SCHEMA =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_XML_DOES_NOT_CONFORM_TO_SCHEMA");
                                
                                    public static final java.lang.String _SSS_XML_DOM_EXCEPTION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SSS_XML_DOM_EXCEPTION");
                                
                                    public static final java.lang.String _START_DATE_AFTER_END_DATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("START_DATE_AFTER_END_DATE");
                                
                                    public static final java.lang.String _START_DATE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("START_DATE_REQD");
                                
                                    public static final java.lang.String _STATE_ALREADY_EXISTS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("STATE_ALREADY_EXISTS");
                                
                                    public static final java.lang.String _STATE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("STATE_REQD");
                                
                                    public static final java.lang.String _STATUS_ASSIGNEE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("STATUS_ASSIGNEE_REQD");
                                
                                    public static final java.lang.String _STORAGE_LIMIT_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("STORAGE_LIMIT_EXCEEDED");
                                
                                    public static final java.lang.String _STORE_ALIAS_UNAVAILABLE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("STORE_ALIAS_UNAVAILABLE");
                                
                                    public static final java.lang.String _STORE_DOMAIN_UNAVAILABLE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("STORE_DOMAIN_UNAVAILABLE");
                                
                                    public static final java.lang.String _SUBITEM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SUBITEM_REQD");
                                
                                    public static final java.lang.String _SUBSIDIARY_MISMATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SUBSIDIARY_MISMATCH");
                                
                                    public static final java.lang.String _SUB_MISMATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SUB_MISMATCH");
                                
                                    public static final java.lang.String _SUB_RESTRICT_VIEW_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SUB_RESTRICT_VIEW_REQD");
                                
                                    public static final java.lang.String _SUCCESS_TRANS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SUCCESS_TRANS");
                                
                                    public static final java.lang.String _SUPRT_CNTR_LOGIN_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SUPRT_CNTR_LOGIN_ERROR");
                                
                                    public static final java.lang.String _TAGATA_ALREADY_ENDORSED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TAGATA_ALREADY_ENDORSED");
                                
                                    public static final java.lang.String _TAG_ALREADY_EXISTS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TAG_ALREADY_EXISTS");
                                
                                    public static final java.lang.String _TAG_SUBSTITUTN_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TAG_SUBSTITUTN_ERROR");
                                
                                    public static final java.lang.String _TAX_ACCT_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TAX_ACCT_SETUP_REQD");
                                
                                    public static final java.lang.String _TAX_CODES_SETUP_PROBLEM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TAX_CODES_SETUP_PROBLEM");
                                
                                    public static final java.lang.String _TAX_CODES_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TAX_CODES_SETUP_REQD");
                                
                                    public static final java.lang.String _TAX_CODE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TAX_CODE_REQD");
                                
                                    public static final java.lang.String _TAX_GROUP_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TAX_GROUP_SETUP_REQD");
                                
                                    public static final java.lang.String _TAX_PRD_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TAX_PRD_REQD");
                                
                                    public static final java.lang.String _TAX_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TAX_SETUP_REQD");
                                
                                    public static final java.lang.String _TEMPLATE_NOT_FOUND =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TEMPLATE_NOT_FOUND");
                                
                                    public static final java.lang.String _THIRD_PARTY_BILLING_ACCT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("THIRD_PARTY_BILLING_ACCT_REQD");
                                
                                    public static final java.lang.String _TICKET_NOT_LOCATED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TICKET_NOT_LOCATED");
                                
                                    public static final java.lang.String _TIMEOUT_THE_RECORD_DOESNT_EXIST_ANYMORE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TIMEOUT_THE_RECORD_DOESNT_EXIST_ANYMORE");
                                
                                    public static final java.lang.String _TIME_ENTRY_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TIME_ENTRY_DISALLWD");
                                
                                    public static final java.lang.String _TOPIC_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TOPIC_REQD");
                                
                                    public static final java.lang.String _TRANSACTION_DELETED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANSACTION_DELETED");
                                
                                    public static final java.lang.String _TRANSORD_SHIP_REC_MISMATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANSORD_SHIP_REC_MISMATCH");
                                
                                    public static final java.lang.String _TRANS_ALREADY_REFUNDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_ALREADY_REFUNDED");
                                
                                    public static final java.lang.String _TRANS_ALREADY_SETTLED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_ALREADY_SETTLED");
                                
                                    public static final java.lang.String _TRANS_ALREADY_VOIDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_ALREADY_VOIDED");
                                
                                    public static final java.lang.String _TRANS_AMTS_UNBALNCD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_AMTS_UNBALNCD");
                                
                                    public static final java.lang.String _TRANS_APPLIED_AMTS_UNBALNCD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_APPLIED_AMTS_UNBALNCD");
                                
                                    public static final java.lang.String _TRANS_CLASS_UNBALNCD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_CLASS_UNBALNCD");
                                
                                    public static final java.lang.String _TRANS_DEPT_UNBALNCD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_DEPT_UNBALNCD");
                                
                                    public static final java.lang.String _TRANS_DOES_NOT_EXIST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_DOES_NOT_EXIST");
                                
                                    public static final java.lang.String _TRANS_DSNT_EXIST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_DSNT_EXIST");
                                
                                    public static final java.lang.String _TRANS_EDIT_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_EDIT_DISALLWD");
                                
                                    public static final java.lang.String _TRANS_FORGN_CRNCY_MISMATCH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_FORGN_CRNCY_MISMATCH");
                                
                                    public static final java.lang.String _TRANS_FORGN_CUR_UNBALNCD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_FORGN_CUR_UNBALNCD");
                                
                                    public static final java.lang.String _TRANS_IN_USE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_IN_USE");
                                
                                    public static final java.lang.String _TRANS_LINES_UNBALNCD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_LINES_UNBALNCD");
                                
                                    public static final java.lang.String _TRANS_LINE_AND_PMT_UNBALNCD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_LINE_AND_PMT_UNBALNCD");
                                
                                    public static final java.lang.String _TRANS_LOC_UNBALNCD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_LOC_UNBALNCD");
                                
                                    public static final java.lang.String _TRANS_NOT_CLEANED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_NOT_CLEANED");
                                
                                    public static final java.lang.String _TRANS_NOT_COMPLETED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_NOT_COMPLETED");
                                
                                    public static final java.lang.String _TRANS_PRCSSNG_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_PRCSSNG_ERROR");
                                
                                    public static final java.lang.String _TRANS_UNBALNCD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRANS_UNBALNCD");
                                
                                    public static final java.lang.String _TRAN_DATE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRAN_DATE_REQD");
                                
                                    public static final java.lang.String _TRAN_LINE_FX_AMT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRAN_LINE_FX_AMT_REQD");
                                
                                    public static final java.lang.String _TRAN_LINK_FX_AMT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRAN_LINK_FX_AMT_REQD");
                                
                                    public static final java.lang.String _TRAN_PERIOD_CLOSED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRAN_PERIOD_CLOSED");
                                
                                    public static final java.lang.String _TRAN_PRD_CLOSED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TRAN_PRD_CLOSED");
                                
                                    public static final java.lang.String _TWO_FA_AUTH_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TWO_FA_AUTH_REQD");
                                
                                    public static final java.lang.String _TWO_FA_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("TWO_FA_REQD");
                                
                                    public static final java.lang.String _UNABLE_TO_PRINT_CHECKS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNABLE_TO_PRINT_CHECKS");
                                
                                    public static final java.lang.String _UNABLE_TO_PRINT_DEPOSITS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNABLE_TO_PRINT_DEPOSITS");
                                
                                    public static final java.lang.String _UNAUTH_CAMPAIGN_RSPNS_RQST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNAUTH_CAMPAIGN_RSPNS_RQST");
                                
                                    public static final java.lang.String _UNAUTH_UNSUBSCRIBE_RQST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNAUTH_UNSUBSCRIBE_RQST");
                                
                                    public static final java.lang.String _UNDEFINED_ACCTNG_PRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNDEFINED_ACCTNG_PRD");
                                
                                    public static final java.lang.String _UNDEFINED_CSTM_FIELD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNDEFINED_CSTM_FIELD");
                                
                                    public static final java.lang.String _UNDEFINED_TAX_PRD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNDEFINED_TAX_PRD");
                                
                                    public static final java.lang.String _UNEXPECTED_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNEXPECTED_ERROR");
                                
                                    public static final java.lang.String _UNIQUE_CONTACT_NAME_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNIQUE_CONTACT_NAME_REQD");
                                
                                    public static final java.lang.String _UNIQUE_CUST_EMAIL_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNIQUE_CUST_EMAIL_REQD");
                                
                                    public static final java.lang.String _UNIQUE_CUST_ID_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNIQUE_CUST_ID_REQD");
                                
                                    public static final java.lang.String _UNIQUE_ENTITY_NAME_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNIQUE_ENTITY_NAME_REQD");
                                
                                    public static final java.lang.String _UNIQUE_GROUPID_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNIQUE_GROUPID_REQD");
                                
                                    public static final java.lang.String _UNIQUE_PARTNER_CODE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNIQUE_PARTNER_CODE_REQD");
                                
                                    public static final java.lang.String _UNIQUE_QTY_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNIQUE_QTY_REQD");
                                
                                    public static final java.lang.String _UNIQUE_RCRD_ID_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNIQUE_RCRD_ID_REQD");
                                
                                    public static final java.lang.String _UNIQUE_SOLUTION_CODE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNIQUE_SOLUTION_CODE_REQD");
                                
                                    public static final java.lang.String _UNITS_TYP_IN_USE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNITS_TYP_IN_USE");
                                
                                    public static final java.lang.String _UNKNOWN_CARRIER =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNKNOWN_CARRIER");
                                
                                    public static final java.lang.String _UNKNOWN_RCRD_TYPE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNKNOWN_RCRD_TYPE");
                                
                                    public static final java.lang.String _UNKNOWN_SCRIPT_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNKNOWN_SCRIPT_TYP");
                                
                                    public static final java.lang.String _UNKNWN_ALLOCTN_SCHDUL_FREQ_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNKNWN_ALLOCTN_SCHDUL_FREQ_TYP");
                                
                                    public static final java.lang.String _UNKNWN_EMAIL_AUTHOR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNKNWN_EMAIL_AUTHOR");
                                
                                    public static final java.lang.String _UNKNWN_EXCHANGE_RATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNKNWN_EXCHANGE_RATE");
                                
                                    public static final java.lang.String _UNRECOGNIZED_METHOD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNRECOGNIZED_METHOD");
                                
                                    public static final java.lang.String _UNSUBSCRIBE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNSUBSCRIBE_REQD");
                                
                                    public static final java.lang.String _UNSUPPORTED_METHOD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNSUPPORTED_METHOD");
                                
                                    public static final java.lang.String _UNSUPPORTED_WS_VERSION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNSUPPORTED_WS_VERSION");
                                
                                    public static final java.lang.String _UNSUPRTD_DOC_TYP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNSUPRTD_DOC_TYP");
                                
                                    public static final java.lang.String _UPDATE_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UPDATE_DISALLWD");
                                
                                    public static final java.lang.String _UPDATE_PRICE_AMT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UPDATE_PRICE_AMT_REQD");
                                
                                    public static final java.lang.String _UPGRADE_WS_VERSION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UPGRADE_WS_VERSION");
                                
                                    public static final java.lang.String _UPS_CANT_INTEGRATE_FULFILL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UPS_CANT_INTEGRATE_FULFILL");
                                
                                    public static final java.lang.String _UPS_CONFIG_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UPS_CONFIG_ERROR");
                                
                                    public static final java.lang.String _UPS_LICENSE_AGREEMNT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UPS_LICENSE_AGREEMNT_REQD");
                                
                                    public static final java.lang.String _UPS_ONLINE_RATE_UNAVAILBL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UPS_ONLINE_RATE_UNAVAILBL");
                                
                                    public static final java.lang.String _UPS_ONLINE_SHIP_UNAVAILBL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UPS_ONLINE_SHIP_UNAVAILBL");
                                
                                    public static final java.lang.String _UPS_REG_NUM_IN_USE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UPS_REG_NUM_IN_USE");
                                
                                    public static final java.lang.String _UPS_SETUP_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UPS_SETUP_REQD");
                                
                                    public static final java.lang.String _UPS_USER_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UPS_USER_ERROR");
                                
                                    public static final java.lang.String _UPS_VOID_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UPS_VOID_ERROR");
                                
                                    public static final java.lang.String _UPS_XML_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UPS_XML_ERROR");
                                
                                    public static final java.lang.String _URL_ID_PARAM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("URL_ID_PARAM_REQD");
                                
                                    public static final java.lang.String _URL_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("URL_REQD");
                                
                                    public static final java.lang.String _USER_CONTEXT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("USER_CONTEXT_REQD");
                                
                                    public static final java.lang.String _USER_DISABLED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("USER_DISABLED");
                                
                                    public static final java.lang.String _USER_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("USER_ERROR");
                                
                                    public static final java.lang.String _USPS_ACCT_NUM_ALREADY_EXISTS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("USPS_ACCT_NUM_ALREADY_EXISTS");
                                
                                    public static final java.lang.String _USPS_INVALID_INSURED_VALUE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("USPS_INVALID_INSURED_VALUE");
                                
                                    public static final java.lang.String _USPS_INVALID_PACKAGING =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("USPS_INVALID_PACKAGING");
                                
                                    public static final java.lang.String _USPS_INVALID_PSWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("USPS_INVALID_PSWD");
                                
                                    public static final java.lang.String _USPS_LABEL_VOIDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("USPS_LABEL_VOIDED");
                                
                                    public static final java.lang.String _USPS_MAX_ITEM_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("USPS_MAX_ITEM_EXCEEDED");
                                
                                    public static final java.lang.String _USPS_ONE_PACKAGE_ALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("USPS_ONE_PACKAGE_ALLWD");
                                
                                    public static final java.lang.String _USPS_PASS_PHRASE_NOT_UPDATED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("USPS_PASS_PHRASE_NOT_UPDATED");
                                
                                    public static final java.lang.String _USPS_REFUND_FAILED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("USPS_REFUND_FAILED");
                                
                                    public static final java.lang.String _USPS_RETRY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("USPS_RETRY");
                                
                                    public static final java.lang.String _USPS_VALIDATE_ADDR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("USPS_VALIDATE_ADDR");
                                
                                    public static final java.lang.String _USPS_VERIFY_TRACKING_NUM =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("USPS_VERIFY_TRACKING_NUM");
                                
                                    public static final java.lang.String _USPS_VOID_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("USPS_VOID_ERROR");
                                
                                    public static final java.lang.String _VALID_EMAIL_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VALID_EMAIL_REQD");
                                
                                    public static final java.lang.String _VALID_EMAIL_REQD_FOR_LOGIN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VALID_EMAIL_REQD_FOR_LOGIN");
                                
                                    public static final java.lang.String _VALID_FIRST_NAME_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VALID_FIRST_NAME_REQD");
                                
                                    public static final java.lang.String _VALID_LAST_NAME_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VALID_LAST_NAME_REQD");
                                
                                    public static final java.lang.String _VALID_LINE_ITEM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VALID_LINE_ITEM_REQD");
                                
                                    public static final java.lang.String _VALID_PHONE_NUM_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VALID_PHONE_NUM_REQD");
                                
                                    public static final java.lang.String _VALID_PRD_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VALID_PRD_REQD");
                                
                                    public static final java.lang.String _VALID_URL_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VALID_URL_REQD");
                                
                                    public static final java.lang.String _VALID_VERSION_REQD_IN_URL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VALID_VERSION_REQD_IN_URL");
                                
                                    public static final java.lang.String _VALID_WORK_PHONE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VALID_WORK_PHONE_REQD");
                                
                                    public static final java.lang.String _VALID_ZIPCODE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VALID_ZIPCODE_REQD");
                                
                                    public static final java.lang.String _VENDOR_TYPE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VENDOR_TYPE_REQD");
                                
                                    public static final java.lang.String _VERIFY_DESTNTN_ZIP_CODE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VERIFY_DESTNTN_ZIP_CODE");
                                
                                    public static final java.lang.String _VERIFY_PAYROLL_FUND_ACCT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VERIFY_PAYROLL_FUND_ACCT");
                                
                                    public static final java.lang.String _VERIFY_ZIP_CODE_SETUP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VERIFY_ZIP_CODE_SETUP");
                                
                                    public static final java.lang.String _VISA_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VISA_ERROR");
                                
                                    public static final java.lang.String _VOIDING_REVERSAL_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VOIDING_REVERSAL_DISALLWD");
                                
                                    public static final java.lang.String _VOID_FAILED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VOID_FAILED");
                                
                                    public static final java.lang.String _VSOE_CANT_ADD_ITEM_GROUP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VSOE_CANT_ADD_ITEM_GROUP");
                                
                                    public static final java.lang.String _VSOE_REV_REC_TMPLT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VSOE_REV_REC_TMPLT_REQD");
                                
                                    public static final java.lang.String _VSOE_TOTAL_ALLOCATION_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VSOE_TOTAL_ALLOCATION_ERROR");
                                
                                    public static final java.lang.String _VSOE_TRAN_VSOE_BUNDLE_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("VSOE_TRAN_VSOE_BUNDLE_ERROR");
                                
                                    public static final java.lang.String _WARNING =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("WARNING");
                                
                                    public static final java.lang.String _WF_EXEC_USAGE_LIMIT_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("WF_EXEC_USAGE_LIMIT_EXCEEDED");
                                
                                    public static final java.lang.String _WORK_DAYS_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("WORK_DAYS_REQD");
                                
                                    public static final java.lang.String _WORLDPAY_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("WORLDPAY_ERROR");
                                
                                    public static final java.lang.String _WRITE_OFF_ACCT_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("WRITE_OFF_ACCT_REQD");
                                
                                    public static final java.lang.String _WS_CONCUR_SESSION_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("WS_CONCUR_SESSION_DISALLWD");
                                
                                    public static final java.lang.String _WS_EXCEEDED_CONCUR_USERS_ALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("WS_EXCEEDED_CONCUR_USERS_ALLWD");
                                
                                    public static final java.lang.String _WS_EXCEEDED_MAX_CONCUR_RQST =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("WS_EXCEEDED_MAX_CONCUR_RQST");
                                
                                    public static final java.lang.String _WS_FEATURE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("WS_FEATURE_REQD");
                                
                                    public static final java.lang.String _WS_INVALID_SEARCH_OPERATN =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("WS_INVALID_SEARCH_OPERATN");
                                
                                    public static final java.lang.String _WS_LOG_IN_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("WS_LOG_IN_REQD");
                                
                                    public static final java.lang.String _WS_PERMISSION_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("WS_PERMISSION_REQD");
                                
                                    public static final java.lang.String _WS_REQUEST_BLOCKED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("WS_REQUEST_BLOCKED");
                                
                                    public static final java.lang.String _ZIP_FILE_CONTAINS_VIRUS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ZIP_FILE_CONTAINS_VIRUS");
                                
                                public static final StatusDetailCodeType ABORT_SEARCH_EXCEEDED_MAX_TIME =
                                    new StatusDetailCodeType(_ABORT_SEARCH_EXCEEDED_MAX_TIME,true);
                            
                                public static final StatusDetailCodeType ABORT_UPLOAD_VIRUS_DETECTED =
                                    new StatusDetailCodeType(_ABORT_UPLOAD_VIRUS_DETECTED,true);
                            
                                public static final StatusDetailCodeType ACCESS_DENIED =
                                    new StatusDetailCodeType(_ACCESS_DENIED,true);
                            
                                public static final StatusDetailCodeType ACCTNG_PRD_REQD =
                                    new StatusDetailCodeType(_ACCTNG_PRD_REQD,true);
                            
                                public static final StatusDetailCodeType ACCT_DISABLED =
                                    new StatusDetailCodeType(_ACCT_DISABLED,true);
                            
                                public static final StatusDetailCodeType ACCT_MERGE_DUP =
                                    new StatusDetailCodeType(_ACCT_MERGE_DUP,true);
                            
                                public static final StatusDetailCodeType ACCT_NAME_REQD =
                                    new StatusDetailCodeType(_ACCT_NAME_REQD,true);
                            
                                public static final StatusDetailCodeType ACCT_NEEDS_CAMPAIGN_PROVISION =
                                    new StatusDetailCodeType(_ACCT_NEEDS_CAMPAIGN_PROVISION,true);
                            
                                public static final StatusDetailCodeType ACCT_NOT_CREATED =
                                    new StatusDetailCodeType(_ACCT_NOT_CREATED,true);
                            
                                public static final StatusDetailCodeType ACCT_NUMS_REQD_OR_DONT_MATCH =
                                    new StatusDetailCodeType(_ACCT_NUMS_REQD_OR_DONT_MATCH,true);
                            
                                public static final StatusDetailCodeType ACCT_NUM_REQD =
                                    new StatusDetailCodeType(_ACCT_NUM_REQD,true);
                            
                                public static final StatusDetailCodeType ACCT_PERIOD_SETUP_REQD =
                                    new StatusDetailCodeType(_ACCT_PERIOD_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType ACCT_PRDS_BEING_ADDED =
                                    new StatusDetailCodeType(_ACCT_PRDS_BEING_ADDED,true);
                            
                                public static final StatusDetailCodeType ACCT_REQD =
                                    new StatusDetailCodeType(_ACCT_REQD,true);
                            
                                public static final StatusDetailCodeType ACCT_TEMP_DISABLED =
                                    new StatusDetailCodeType(_ACCT_TEMP_DISABLED,true);
                            
                                public static final StatusDetailCodeType ACCT_TEMP_UNAVAILABLE =
                                    new StatusDetailCodeType(_ACCT_TEMP_UNAVAILABLE,true);
                            
                                public static final StatusDetailCodeType ACH_NOT_AVAILBL =
                                    new StatusDetailCodeType(_ACH_NOT_AVAILBL,true);
                            
                                public static final StatusDetailCodeType ACH_SETUP_REQD =
                                    new StatusDetailCodeType(_ACH_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType ACTIVE_AP_ACCT_REQD =
                                    new StatusDetailCodeType(_ACTIVE_AP_ACCT_REQD,true);
                            
                                public static final StatusDetailCodeType ACTIVE_ROLE_REQD =
                                    new StatusDetailCodeType(_ACTIVE_ROLE_REQD,true);
                            
                                public static final StatusDetailCodeType ACTIVE_TRANS_EXIST =
                                    new StatusDetailCodeType(_ACTIVE_TRANS_EXIST,true);
                            
                                public static final StatusDetailCodeType ADDITIONAL_AUTHENTICATION_REQUIRED_2FA =
                                    new StatusDetailCodeType(_ADDITIONAL_AUTHENTICATION_REQUIRED_2FA,true);
                            
                                public static final StatusDetailCodeType ADDITIONAL_AUTHENTICATION_REQUIRED_SQ =
                                    new StatusDetailCodeType(_ADDITIONAL_AUTHENTICATION_REQUIRED_SQ,true);
                            
                                public static final StatusDetailCodeType ADDRESS_LINE_1_REQD =
                                    new StatusDetailCodeType(_ADDRESS_LINE_1_REQD,true);
                            
                                public static final StatusDetailCodeType ADMIN_ACCESS_REQ =
                                    new StatusDetailCodeType(_ADMIN_ACCESS_REQ,true);
                            
                                public static final StatusDetailCodeType ADMIN_ACCESS_REQD =
                                    new StatusDetailCodeType(_ADMIN_ACCESS_REQD,true);
                            
                                public static final StatusDetailCodeType ADMIN_ONLY_ACCESS =
                                    new StatusDetailCodeType(_ADMIN_ONLY_ACCESS,true);
                            
                                public static final StatusDetailCodeType ADMIN_USER_REQD =
                                    new StatusDetailCodeType(_ADMIN_USER_REQD,true);
                            
                                public static final StatusDetailCodeType ADMISSIBILITY_PACKG_TYP_REQD =
                                    new StatusDetailCodeType(_ADMISSIBILITY_PACKG_TYP_REQD,true);
                            
                                public static final StatusDetailCodeType ALL_DATA_DELETE_REQD =
                                    new StatusDetailCodeType(_ALL_DATA_DELETE_REQD,true);
                            
                                public static final StatusDetailCodeType ALL_MTRX_SUBITMES_OPTNS_REQD =
                                    new StatusDetailCodeType(_ALL_MTRX_SUBITMES_OPTNS_REQD,true);
                            
                                public static final StatusDetailCodeType ALREADY_IN_INVT =
                                    new StatusDetailCodeType(_ALREADY_IN_INVT,true);
                            
                                public static final StatusDetailCodeType AMORTZN_INVALID_DATE_RANGE =
                                    new StatusDetailCodeType(_AMORTZN_INVALID_DATE_RANGE,true);
                            
                                public static final StatusDetailCodeType AMORTZN_TMPLT_DATA_MISSING =
                                    new StatusDetailCodeType(_AMORTZN_TMPLT_DATA_MISSING,true);
                            
                                public static final StatusDetailCodeType AMT_DISALLWD =
                                    new StatusDetailCodeType(_AMT_DISALLWD,true);
                            
                                public static final StatusDetailCodeType AMT_EXCEEDS_APPROVAL_LIMIT =
                                    new StatusDetailCodeType(_AMT_EXCEEDS_APPROVAL_LIMIT,true);
                            
                                public static final StatusDetailCodeType ANSWER_REQD =
                                    new StatusDetailCodeType(_ANSWER_REQD,true);
                            
                                public static final StatusDetailCodeType APPROVAL_PERMS_REQD =
                                    new StatusDetailCodeType(_APPROVAL_PERMS_REQD,true);
                            
                                public static final StatusDetailCodeType AREA_CODE_REQD =
                                    new StatusDetailCodeType(_AREA_CODE_REQD,true);
                            
                                public static final StatusDetailCodeType ASSIGNEE_REQD =
                                    new StatusDetailCodeType(_ASSIGNEE_REQD,true);
                            
                                public static final StatusDetailCodeType ATTACHMNT_CONTAINS_VIRUS =
                                    new StatusDetailCodeType(_ATTACHMNT_CONTAINS_VIRUS,true);
                            
                                public static final StatusDetailCodeType ATTACH_SIZE_EXCEEDED =
                                    new StatusDetailCodeType(_ATTACH_SIZE_EXCEEDED,true);
                            
                                public static final StatusDetailCodeType AT_LEAST_ONE_FILE_REQD =
                                    new StatusDetailCodeType(_AT_LEAST_ONE_FILE_REQD,true);
                            
                                public static final StatusDetailCodeType AT_LEAST_ONE_PACKAGE_REQD =
                                    new StatusDetailCodeType(_AT_LEAST_ONE_PACKAGE_REQD,true);
                            
                                public static final StatusDetailCodeType AT_LEAST_ONE_RETURN_FLD_REQD =
                                    new StatusDetailCodeType(_AT_LEAST_ONE_RETURN_FLD_REQD,true);
                            
                                public static final StatusDetailCodeType AT_LEAST_ONE_SUB_REQD =
                                    new StatusDetailCodeType(_AT_LEAST_ONE_SUB_REQD,true);
                            
                                public static final StatusDetailCodeType AUDIT_W2_1099 =
                                    new StatusDetailCodeType(_AUDIT_W2_1099,true);
                            
                                public static final StatusDetailCodeType AUTO_NUM_UPDATE_DISALLWD =
                                    new StatusDetailCodeType(_AUTO_NUM_UPDATE_DISALLWD,true);
                            
                                public static final StatusDetailCodeType AVS_ERROR =
                                    new StatusDetailCodeType(_AVS_ERROR,true);
                            
                                public static final StatusDetailCodeType BALANCE_EXCEEDS_CREDIT_LIMIT =
                                    new StatusDetailCodeType(_BALANCE_EXCEEDS_CREDIT_LIMIT,true);
                            
                                public static final StatusDetailCodeType BANK_ACCT_REQD =
                                    new StatusDetailCodeType(_BANK_ACCT_REQD,true);
                            
                                public static final StatusDetailCodeType BASE_CRNCY_REQD =
                                    new StatusDetailCodeType(_BASE_CRNCY_REQD,true);
                            
                                public static final StatusDetailCodeType BILLABLES_DISALLWD =
                                    new StatusDetailCodeType(_BILLABLES_DISALLWD,true);
                            
                                public static final StatusDetailCodeType BILLING_ISSUES =
                                    new StatusDetailCodeType(_BILLING_ISSUES,true);
                            
                                public static final StatusDetailCodeType BILLING_SCHDUL_INVALID_RECURR =
                                    new StatusDetailCodeType(_BILLING_SCHDUL_INVALID_RECURR,true);
                            
                                public static final StatusDetailCodeType BILLPAY_APPROVAL_UNAVAILBL =
                                    new StatusDetailCodeType(_BILLPAY_APPROVAL_UNAVAILBL,true);
                            
                                public static final StatusDetailCodeType BILLPAY_REGSTRTN_REQD =
                                    new StatusDetailCodeType(_BILLPAY_REGSTRTN_REQD,true);
                            
                                public static final StatusDetailCodeType BILLPAY_SRVC_UNAVAILBL =
                                    new StatusDetailCodeType(_BILLPAY_SRVC_UNAVAILBL,true);
                            
                                public static final StatusDetailCodeType BILL_PAY_STATUS_UNAVAILABLE =
                                    new StatusDetailCodeType(_BILL_PAY_STATUS_UNAVAILABLE,true);
                            
                                public static final StatusDetailCodeType BILL_PMTS_MADE_FROM_ACCT_ONLY =
                                    new StatusDetailCodeType(_BILL_PMTS_MADE_FROM_ACCT_ONLY,true);
                            
                                public static final StatusDetailCodeType BIN_DSNT_CONTAIN_ENOUGH_ITEM =
                                    new StatusDetailCodeType(_BIN_DSNT_CONTAIN_ENOUGH_ITEM,true);
                            
                                public static final StatusDetailCodeType BIN_ITEM_UNAVAILBL =
                                    new StatusDetailCodeType(_BIN_ITEM_UNAVAILBL,true);
                            
                                public static final StatusDetailCodeType BIN_SETUP_REQD =
                                    new StatusDetailCodeType(_BIN_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType BIN_UNDEFND =
                                    new StatusDetailCodeType(_BIN_UNDEFND,true);
                            
                                public static final StatusDetailCodeType BUNDLE_IS_DEPRECATED =
                                    new StatusDetailCodeType(_BUNDLE_IS_DEPRECATED,true);
                            
                                public static final StatusDetailCodeType CALENDAR_PREFS_REQD =
                                    new StatusDetailCodeType(_CALENDAR_PREFS_REQD,true);
                            
                                public static final StatusDetailCodeType CAMPAGIN_ALREADY_EXECUTED =
                                    new StatusDetailCodeType(_CAMPAGIN_ALREADY_EXECUTED,true);
                            
                                public static final StatusDetailCodeType CAMPAIGN_IN_USE =
                                    new StatusDetailCodeType(_CAMPAIGN_IN_USE,true);
                            
                                public static final StatusDetailCodeType CAMPAIGN_SET_UP_REQD =
                                    new StatusDetailCodeType(_CAMPAIGN_SET_UP_REQD,true);
                            
                                public static final StatusDetailCodeType CANNOT_RESET_PASSWORD =
                                    new StatusDetailCodeType(_CANNOT_RESET_PASSWORD,true);
                            
                                public static final StatusDetailCodeType CANT_APPLY_PMT =
                                    new StatusDetailCodeType(_CANT_APPLY_PMT,true);
                            
                                public static final StatusDetailCodeType CANT_AUTO_CREATE_ADJSTMNT =
                                    new StatusDetailCodeType(_CANT_AUTO_CREATE_ADJSTMNT,true);
                            
                                public static final StatusDetailCodeType CANT_CALC_FEDEX_RATES =
                                    new StatusDetailCodeType(_CANT_CALC_FEDEX_RATES,true);
                            
                                public static final StatusDetailCodeType CANT_CANCEL_APPRVD_RETRN_AUTH =
                                    new StatusDetailCodeType(_CANT_CANCEL_APPRVD_RETRN_AUTH,true);
                            
                                public static final StatusDetailCodeType CANT_CANCEL_BILL_PMT =
                                    new StatusDetailCodeType(_CANT_CANCEL_BILL_PMT,true);
                            
                                public static final StatusDetailCodeType CANT_CHANGE_COMMSSN =
                                    new StatusDetailCodeType(_CANT_CHANGE_COMMSSN,true);
                            
                                public static final StatusDetailCodeType CANT_CHANGE_CONTACT_RESTRICTN =
                                    new StatusDetailCodeType(_CANT_CHANGE_CONTACT_RESTRICTN,true);
                            
                                public static final StatusDetailCodeType CANT_CHANGE_CRMRECORDTYPELINKS =
                                    new StatusDetailCodeType(_CANT_CHANGE_CRMRECORDTYPELINKS,true);
                            
                                public static final StatusDetailCodeType CANT_CHANGE_EVENT_PRIMARY_TYP =
                                    new StatusDetailCodeType(_CANT_CHANGE_EVENT_PRIMARY_TYP,true);
                            
                                public static final StatusDetailCodeType CANT_CHANGE_IP_ADDRESS =
                                    new StatusDetailCodeType(_CANT_CHANGE_IP_ADDRESS,true);
                            
                                public static final StatusDetailCodeType CANT_CHANGE_LEAD_SOURCE_CAT =
                                    new StatusDetailCodeType(_CANT_CHANGE_LEAD_SOURCE_CAT,true);
                            
                                public static final StatusDetailCodeType CANT_CHANGE_PSWD =
                                    new StatusDetailCodeType(_CANT_CHANGE_PSWD,true);
                            
                                public static final StatusDetailCodeType CANT_CHANGE_REV_REC_TMPLT =
                                    new StatusDetailCodeType(_CANT_CHANGE_REV_REC_TMPLT,true);
                            
                                public static final StatusDetailCodeType CANT_CHANGE_SUB =
                                    new StatusDetailCodeType(_CANT_CHANGE_SUB,true);
                            
                                public static final StatusDetailCodeType CANT_CHANGE_TASK_LINK =
                                    new StatusDetailCodeType(_CANT_CHANGE_TASK_LINK,true);
                            
                                public static final StatusDetailCodeType CANT_CHANGE_UNITS_TYP =
                                    new StatusDetailCodeType(_CANT_CHANGE_UNITS_TYP,true);
                            
                                public static final StatusDetailCodeType CANT_CHANGE_VSOE_ALLOCTN =
                                    new StatusDetailCodeType(_CANT_CHANGE_VSOE_ALLOCTN,true);
                            
                                public static final StatusDetailCodeType CANT_CHG_POSTED_BILL_VRNC =
                                    new StatusDetailCodeType(_CANT_CHG_POSTED_BILL_VRNC,true);
                            
                                public static final StatusDetailCodeType CANT_COMPLETE_FULFILL =
                                    new StatusDetailCodeType(_CANT_COMPLETE_FULFILL,true);
                            
                                public static final StatusDetailCodeType CANT_CONNECT_TO_STORE =
                                    new StatusDetailCodeType(_CANT_CONNECT_TO_STORE,true);
                            
                                public static final StatusDetailCodeType CANT_CONVERT_CLASS_DEPT =
                                    new StatusDetailCodeType(_CANT_CONVERT_CLASS_DEPT,true);
                            
                                public static final StatusDetailCodeType CANT_CONVERT_CLASS_LOC =
                                    new StatusDetailCodeType(_CANT_CONVERT_CLASS_LOC,true);
                            
                                public static final StatusDetailCodeType CANT_CONVERT_INVT_ITEM =
                                    new StatusDetailCodeType(_CANT_CONVERT_INVT_ITEM,true);
                            
                                public static final StatusDetailCodeType CANT_CREATE_FILES =
                                    new StatusDetailCodeType(_CANT_CREATE_FILES,true);
                            
                                public static final StatusDetailCodeType CANT_CREATE_NON_UNIQUE_RCRD =
                                    new StatusDetailCodeType(_CANT_CREATE_NON_UNIQUE_RCRD,true);
                            
                                public static final StatusDetailCodeType CANT_CREATE_PO =
                                    new StatusDetailCodeType(_CANT_CREATE_PO,true);
                            
                                public static final StatusDetailCodeType CANT_CREATE_SHIP_LABEL =
                                    new StatusDetailCodeType(_CANT_CREATE_SHIP_LABEL,true);
                            
                                public static final StatusDetailCodeType CANT_CREATE_WORK_ORD =
                                    new StatusDetailCodeType(_CANT_CREATE_WORK_ORD,true);
                            
                                public static final StatusDetailCodeType CANT_CREAT_SHIP_LABEL =
                                    new StatusDetailCodeType(_CANT_CREAT_SHIP_LABEL,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_ACCT =
                                    new StatusDetailCodeType(_CANT_DELETE_ACCT,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_ACCT_PRD =
                                    new StatusDetailCodeType(_CANT_DELETE_ACCT_PRD,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_ALLOCTN =
                                    new StatusDetailCodeType(_CANT_DELETE_ALLOCTN,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_BIN =
                                    new StatusDetailCodeType(_CANT_DELETE_BIN,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_CATEGORY =
                                    new StatusDetailCodeType(_CANT_DELETE_CATEGORY,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_CC_PROCESSOR =
                                    new StatusDetailCodeType(_CANT_DELETE_CC_PROCESSOR,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_CELL =
                                    new StatusDetailCodeType(_CANT_DELETE_CELL,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_CHILD_RCRDS_EXIST =
                                    new StatusDetailCodeType(_CANT_DELETE_CHILD_RCRDS_EXIST,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_CHILD_RCRD_FOUND =
                                    new StatusDetailCodeType(_CANT_DELETE_CHILD_RCRD_FOUND,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_CLASS =
                                    new StatusDetailCodeType(_CANT_DELETE_CLASS,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_COLOR_THEME =
                                    new StatusDetailCodeType(_CANT_DELETE_COLOR_THEME,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_COMMSSN_SCHDUL =
                                    new StatusDetailCodeType(_CANT_DELETE_COMMSSN_SCHDUL,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_COMPANY =
                                    new StatusDetailCodeType(_CANT_DELETE_COMPANY,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_COMPANY_TYP =
                                    new StatusDetailCodeType(_CANT_DELETE_COMPANY_TYP,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_CONTACT_HAS_CHILD =
                                    new StatusDetailCodeType(_CANT_DELETE_CONTACT_HAS_CHILD,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_CSTM_FIELD =
                                    new StatusDetailCodeType(_CANT_DELETE_CSTM_FIELD,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_CSTM_FORM =
                                    new StatusDetailCodeType(_CANT_DELETE_CSTM_FORM,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_CSTM_ITEM_FIELD =
                                    new StatusDetailCodeType(_CANT_DELETE_CSTM_ITEM_FIELD,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_CSTM_LAYOUT =
                                    new StatusDetailCodeType(_CANT_DELETE_CSTM_LAYOUT,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_CSTM_LIST =
                                    new StatusDetailCodeType(_CANT_DELETE_CSTM_LIST,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_CSTM_RCRD =
                                    new StatusDetailCodeType(_CANT_DELETE_CSTM_RCRD,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_CSTM_RCRD_ENTRY =
                                    new StatusDetailCodeType(_CANT_DELETE_CSTM_RCRD_ENTRY,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_CUST =
                                    new StatusDetailCodeType(_CANT_DELETE_CUST,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_CUSTOMER =
                                    new StatusDetailCodeType(_CANT_DELETE_CUSTOMER,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_DEFAULT_FLDR =
                                    new StatusDetailCodeType(_CANT_DELETE_DEFAULT_FLDR,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_DEFAULT_PRIORITY =
                                    new StatusDetailCodeType(_CANT_DELETE_DEFAULT_PRIORITY,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_DEFAULT_SALES_REP =
                                    new StatusDetailCodeType(_CANT_DELETE_DEFAULT_SALES_REP,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_DEFAULT_STATUS =
                                    new StatusDetailCodeType(_CANT_DELETE_DEFAULT_STATUS,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_DEFAULT_VALUE =
                                    new StatusDetailCodeType(_CANT_DELETE_DEFAULT_VALUE,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_DEFAULT_WEBSITE =
                                    new StatusDetailCodeType(_CANT_DELETE_DEFAULT_WEBSITE,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_EMPL =
                                    new StatusDetailCodeType(_CANT_DELETE_EMPL,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_ENTITY =
                                    new StatusDetailCodeType(_CANT_DELETE_ENTITY,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_FIN_STATMNT_LAYOUT =
                                    new StatusDetailCodeType(_CANT_DELETE_FIN_STATMNT_LAYOUT,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_FLDR =
                                    new StatusDetailCodeType(_CANT_DELETE_FLDR,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_HAS_CHILD_ITEM =
                                    new StatusDetailCodeType(_CANT_DELETE_HAS_CHILD_ITEM,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_INFO_ITEM =
                                    new StatusDetailCodeType(_CANT_DELETE_INFO_ITEM,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_ITEM =
                                    new StatusDetailCodeType(_CANT_DELETE_ITEM,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_ITEM_LAYOUT =
                                    new StatusDetailCodeType(_CANT_DELETE_ITEM_LAYOUT,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_ITEM_TMPLT =
                                    new StatusDetailCodeType(_CANT_DELETE_ITEM_TMPLT,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_JOB_RESOURCE_ROLE =
                                    new StatusDetailCodeType(_CANT_DELETE_JOB_RESOURCE_ROLE,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_LEGACY_CATEGORY =
                                    new StatusDetailCodeType(_CANT_DELETE_LEGACY_CATEGORY,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_LINE =
                                    new StatusDetailCodeType(_CANT_DELETE_LINE,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_MEDIA_ITEM =
                                    new StatusDetailCodeType(_CANT_DELETE_MEDIA_ITEM,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_MEMRZD_TRANS =
                                    new StatusDetailCodeType(_CANT_DELETE_MEMRZD_TRANS,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_OR_CHANGE_ACCT =
                                    new StatusDetailCodeType(_CANT_DELETE_OR_CHANGE_ACCT,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_PLAN_ASSGNMNT =
                                    new StatusDetailCodeType(_CANT_DELETE_PLAN_ASSGNMNT,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_PRESNTN_CAT =
                                    new StatusDetailCodeType(_CANT_DELETE_PRESNTN_CAT,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_RCRD =
                                    new StatusDetailCodeType(_CANT_DELETE_RCRD,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_RCRDS =
                                    new StatusDetailCodeType(_CANT_DELETE_RCRDS,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_SITE_TAG =
                                    new StatusDetailCodeType(_CANT_DELETE_SITE_TAG,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_SITE_THEME =
                                    new StatusDetailCodeType(_CANT_DELETE_SITE_THEME,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_SOLUTN =
                                    new StatusDetailCodeType(_CANT_DELETE_SOLUTN,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_STATUS_TYPE =
                                    new StatusDetailCodeType(_CANT_DELETE_STATUS_TYPE,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_SUBTAB =
                                    new StatusDetailCodeType(_CANT_DELETE_SUBTAB,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_SYSTEM_NOTE =
                                    new StatusDetailCodeType(_CANT_DELETE_SYSTEM_NOTE,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_TAX_VENDOR =
                                    new StatusDetailCodeType(_CANT_DELETE_TAX_VENDOR,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_TMPLT_RCRD =
                                    new StatusDetailCodeType(_CANT_DELETE_TMPLT_RCRD,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_TRANS =
                                    new StatusDetailCodeType(_CANT_DELETE_TRANS,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_TRAN_LINE =
                                    new StatusDetailCodeType(_CANT_DELETE_TRAN_LINE,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_TRAN_LINES =
                                    new StatusDetailCodeType(_CANT_DELETE_TRAN_LINES,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_UPDATE_ACCT =
                                    new StatusDetailCodeType(_CANT_DELETE_UPDATE_ACCT,true);
                            
                                public static final StatusDetailCodeType CANT_DELETE_VENDOR =
                                    new StatusDetailCodeType(_CANT_DELETE_VENDOR,true);
                            
                                public static final StatusDetailCodeType CANT_DEL_DEFAULT_CALENDAR =
                                    new StatusDetailCodeType(_CANT_DEL_DEFAULT_CALENDAR,true);
                            
                                public static final StatusDetailCodeType CANT_DEL_DEFAULT_SHIP_METHOD =
                                    new StatusDetailCodeType(_CANT_DEL_DEFAULT_SHIP_METHOD,true);
                            
                                public static final StatusDetailCodeType CANT_DEL_REALIZED_GAINLOSS =
                                    new StatusDetailCodeType(_CANT_DEL_REALIZED_GAINLOSS,true);
                            
                                public static final StatusDetailCodeType CANT_DEL_TRANS_RVRSL =
                                    new StatusDetailCodeType(_CANT_DEL_TRANS_RVRSL,true);
                            
                                public static final StatusDetailCodeType CANT_DIVIDE_BY_ZERO =
                                    new StatusDetailCodeType(_CANT_DIVIDE_BY_ZERO,true);
                            
                                public static final StatusDetailCodeType CANT_DOWNLOAD_EXPIRED_FILE =
                                    new StatusDetailCodeType(_CANT_DOWNLOAD_EXPIRED_FILE,true);
                            
                                public static final StatusDetailCodeType CANT_EDIT_CUST_LIST =
                                    new StatusDetailCodeType(_CANT_EDIT_CUST_LIST,true);
                            
                                public static final StatusDetailCodeType CANT_EDIT_CUST_PMT =
                                    new StatusDetailCodeType(_CANT_EDIT_CUST_PMT,true);
                            
                                public static final StatusDetailCodeType CANT_EDIT_DPLYMNT_IN_PROGRESS =
                                    new StatusDetailCodeType(_CANT_EDIT_DPLYMNT_IN_PROGRESS,true);
                            
                                public static final StatusDetailCodeType CANT_EDIT_FOLDER =
                                    new StatusDetailCodeType(_CANT_EDIT_FOLDER,true);
                            
                                public static final StatusDetailCodeType CANT_EDIT_OLD_CASE =
                                    new StatusDetailCodeType(_CANT_EDIT_OLD_CASE,true);
                            
                                public static final StatusDetailCodeType CANT_EDIT_STANDARD_OBJ =
                                    new StatusDetailCodeType(_CANT_EDIT_STANDARD_OBJ,true);
                            
                                public static final StatusDetailCodeType CANT_EDIT_TAGATA =
                                    new StatusDetailCodeType(_CANT_EDIT_TAGATA,true);
                            
                                public static final StatusDetailCodeType CANT_EDIT_TRAN =
                                    new StatusDetailCodeType(_CANT_EDIT_TRAN,true);
                            
                                public static final StatusDetailCodeType CANT_ESTABLISH_LINK =
                                    new StatusDetailCodeType(_CANT_ESTABLISH_LINK,true);
                            
                                public static final StatusDetailCodeType CANT_FIND_BUG =
                                    new StatusDetailCodeType(_CANT_FIND_BUG,true);
                            
                                public static final StatusDetailCodeType CANT_FIND_MAIL_MERGE_ID =
                                    new StatusDetailCodeType(_CANT_FIND_MAIL_MERGE_ID,true);
                            
                                public static final StatusDetailCodeType CANT_FIND_RCRD =
                                    new StatusDetailCodeType(_CANT_FIND_RCRD,true);
                            
                                public static final StatusDetailCodeType CANT_FIND_SAVED_IMPORT =
                                    new StatusDetailCodeType(_CANT_FIND_SAVED_IMPORT,true);
                            
                                public static final StatusDetailCodeType CANT_FIND_SOURCE_AMORTZN_ACCT =
                                    new StatusDetailCodeType(_CANT_FIND_SOURCE_AMORTZN_ACCT,true);
                            
                                public static final StatusDetailCodeType CANT_FIND_UPS_REG_FOR_LOC =
                                    new StatusDetailCodeType(_CANT_FIND_UPS_REG_FOR_LOC,true);
                            
                                public static final StatusDetailCodeType CANT_FULFILL_ITEM =
                                    new StatusDetailCodeType(_CANT_FULFILL_ITEM,true);
                            
                                public static final StatusDetailCodeType CANT_INACTIVATE_COMMSSN_PLAN =
                                    new StatusDetailCodeType(_CANT_INACTIVATE_COMMSSN_PLAN,true);
                            
                                public static final StatusDetailCodeType CANT_INACTIVE_DEFAULT_SYNC_CAT =
                                    new StatusDetailCodeType(_CANT_INACTIVE_DEFAULT_SYNC_CAT,true);
                            
                                public static final StatusDetailCodeType CANT_INACTIVE_DEFAULT_TMPLT =
                                    new StatusDetailCodeType(_CANT_INACTIVE_DEFAULT_TMPLT,true);
                            
                                public static final StatusDetailCodeType CANT_LOAD_SAVED_SEARCH_PARAM =
                                    new StatusDetailCodeType(_CANT_LOAD_SAVED_SEARCH_PARAM,true);
                            
                                public static final StatusDetailCodeType CANT_LOGIN_WITH_OAUTH =
                                    new StatusDetailCodeType(_CANT_LOGIN_WITH_OAUTH,true);
                            
                                public static final StatusDetailCodeType CANT_LOOKUP_FLD =
                                    new StatusDetailCodeType(_CANT_LOOKUP_FLD,true);
                            
                                public static final StatusDetailCodeType CANT_MAKE_CONTACT_PRIVATE =
                                    new StatusDetailCodeType(_CANT_MAKE_CONTACT_PRIVATE,true);
                            
                                public static final StatusDetailCodeType CANT_MARK_SHIPPED =
                                    new StatusDetailCodeType(_CANT_MARK_SHIPPED,true);
                            
                                public static final StatusDetailCodeType CANT_MERGE_EMPLS =
                                    new StatusDetailCodeType(_CANT_MERGE_EMPLS,true);
                            
                                public static final StatusDetailCodeType CANT_MODIFY_APPRVD_TIME =
                                    new StatusDetailCodeType(_CANT_MODIFY_APPRVD_TIME,true);
                            
                                public static final StatusDetailCodeType CANT_MODIFY_FULFILL_STATUS =
                                    new StatusDetailCodeType(_CANT_MODIFY_FULFILL_STATUS,true);
                            
                                public static final StatusDetailCodeType CANT_MODIFY_ISSUE_STATUS =
                                    new StatusDetailCodeType(_CANT_MODIFY_ISSUE_STATUS,true);
                            
                                public static final StatusDetailCodeType CANT_MODIFY_LOCKED_FLD =
                                    new StatusDetailCodeType(_CANT_MODIFY_LOCKED_FLD,true);
                            
                                public static final StatusDetailCodeType CANT_MODIFY_PARENT =
                                    new StatusDetailCodeType(_CANT_MODIFY_PARENT,true);
                            
                                public static final StatusDetailCodeType CANT_MODIFY_REV_REC =
                                    new StatusDetailCodeType(_CANT_MODIFY_REV_REC,true);
                            
                                public static final StatusDetailCodeType CANT_MODIFY_SUB =
                                    new StatusDetailCodeType(_CANT_MODIFY_SUB,true);
                            
                                public static final StatusDetailCodeType CANT_MODIFY_TAGATA =
                                    new StatusDetailCodeType(_CANT_MODIFY_TAGATA,true);
                            
                                public static final StatusDetailCodeType CANT_MODIFY_TEGATA =
                                    new StatusDetailCodeType(_CANT_MODIFY_TEGATA,true);
                            
                                public static final StatusDetailCodeType CANT_MODIFY_VOID_TRANS =
                                    new StatusDetailCodeType(_CANT_MODIFY_VOID_TRANS,true);
                            
                                public static final StatusDetailCodeType CANT_MOVE_REALIZED_GAINLOSS =
                                    new StatusDetailCodeType(_CANT_MOVE_REALIZED_GAINLOSS,true);
                            
                                public static final StatusDetailCodeType CANT_PAY_TAGATA =
                                    new StatusDetailCodeType(_CANT_PAY_TAGATA,true);
                            
                                public static final StatusDetailCodeType CANT_PRINT_EMPTY =
                                    new StatusDetailCodeType(_CANT_PRINT_EMPTY,true);
                            
                                public static final StatusDetailCodeType CANT_PROCESS_IMG =
                                    new StatusDetailCodeType(_CANT_PROCESS_IMG,true);
                            
                                public static final StatusDetailCodeType CANT_RCEIV_BEFORE_FULFILL =
                                    new StatusDetailCodeType(_CANT_RCEIV_BEFORE_FULFILL,true);
                            
                                public static final StatusDetailCodeType CANT_RCEIV_ITEM =
                                    new StatusDetailCodeType(_CANT_RCEIV_ITEM,true);
                            
                                public static final StatusDetailCodeType CANT_RECEIVE_TAGATA =
                                    new StatusDetailCodeType(_CANT_RECEIVE_TAGATA,true);
                            
                                public static final StatusDetailCodeType CANT_REJECT_ORDER =
                                    new StatusDetailCodeType(_CANT_REJECT_ORDER,true);
                            
                                public static final StatusDetailCodeType CANT_REMOVE_ACH_PAY_METHOD =
                                    new StatusDetailCodeType(_CANT_REMOVE_ACH_PAY_METHOD,true);
                            
                                public static final StatusDetailCodeType CANT_REMOVE_APPROVAL =
                                    new StatusDetailCodeType(_CANT_REMOVE_APPROVAL,true);
                            
                                public static final StatusDetailCodeType CANT_REMOVE_DOMAIN =
                                    new StatusDetailCodeType(_CANT_REMOVE_DOMAIN,true);
                            
                                public static final StatusDetailCodeType CANT_REMOVE_NEXUS =
                                    new StatusDetailCodeType(_CANT_REMOVE_NEXUS,true);
                            
                                public static final StatusDetailCodeType CANT_REMOVE_SCHDUL =
                                    new StatusDetailCodeType(_CANT_REMOVE_SCHDUL,true);
                            
                                public static final StatusDetailCodeType CANT_REMOVE_SUB =
                                    new StatusDetailCodeType(_CANT_REMOVE_SUB,true);
                            
                                public static final StatusDetailCodeType CANT_REMOV_ALL_FULFILMNT_LINKS =
                                    new StatusDetailCodeType(_CANT_REMOV_ALL_FULFILMNT_LINKS,true);
                            
                                public static final StatusDetailCodeType CANT_REMOV_ITEM_SUB =
                                    new StatusDetailCodeType(_CANT_REMOV_ITEM_SUB,true);
                            
                                public static final StatusDetailCodeType CANT_RESUBMIT_FAILED_DPLYMNT =
                                    new StatusDetailCodeType(_CANT_RESUBMIT_FAILED_DPLYMNT,true);
                            
                                public static final StatusDetailCodeType CANT_RETURN_FLD =
                                    new StatusDetailCodeType(_CANT_RETURN_FLD,true);
                            
                                public static final StatusDetailCodeType CANT_RETURN_USED_GIFT_CERT =
                                    new StatusDetailCodeType(_CANT_RETURN_USED_GIFT_CERT,true);
                            
                                public static final StatusDetailCodeType CANT_REVERSE_AUTH =
                                    new StatusDetailCodeType(_CANT_REVERSE_AUTH,true);
                            
                                public static final StatusDetailCodeType CANT_REV_REC_BODY_AND_LINE =
                                    new StatusDetailCodeType(_CANT_REV_REC_BODY_AND_LINE,true);
                            
                                public static final StatusDetailCodeType CANT_SCHDUL_RECUR_EVENT =
                                    new StatusDetailCodeType(_CANT_SCHDUL_RECUR_EVENT,true);
                            
                                public static final StatusDetailCodeType CANT_SEND_EMAIL =
                                    new StatusDetailCodeType(_CANT_SEND_EMAIL,true);
                            
                                public static final StatusDetailCodeType CANT_SET_CLOSE_DATE =
                                    new StatusDetailCodeType(_CANT_SET_CLOSE_DATE,true);
                            
                                public static final StatusDetailCodeType CANT_SET_INTERNALID =
                                    new StatusDetailCodeType(_CANT_SET_INTERNALID,true);
                            
                                public static final StatusDetailCodeType CANT_SET_STATUS =
                                    new StatusDetailCodeType(_CANT_SET_STATUS,true);
                            
                                public static final StatusDetailCodeType CANT_SWITCH_ROLES_FROM_LOGIN =
                                    new StatusDetailCodeType(_CANT_SWITCH_ROLES_FROM_LOGIN,true);
                            
                                public static final StatusDetailCodeType CANT_SWITCH_SHIP_METHOD =
                                    new StatusDetailCodeType(_CANT_SWITCH_SHIP_METHOD,true);
                            
                                public static final StatusDetailCodeType CANT_UPDATE_ACCTNG_PRDS =
                                    new StatusDetailCodeType(_CANT_UPDATE_ACCTNG_PRDS,true);
                            
                                public static final StatusDetailCodeType CANT_UPDATE_AMT =
                                    new StatusDetailCodeType(_CANT_UPDATE_AMT,true);
                            
                                public static final StatusDetailCodeType CANT_UPDATE_DYNAMIC_GROUP =
                                    new StatusDetailCodeType(_CANT_UPDATE_DYNAMIC_GROUP,true);
                            
                                public static final StatusDetailCodeType CANT_UPDATE_FLDR =
                                    new StatusDetailCodeType(_CANT_UPDATE_FLDR,true);
                            
                                public static final StatusDetailCodeType CANT_UPDATE_LINKED_TRANS_LINES =
                                    new StatusDetailCodeType(_CANT_UPDATE_LINKED_TRANS_LINES,true);
                            
                                public static final StatusDetailCodeType CANT_UPDATE_PRODUCT_FEED =
                                    new StatusDetailCodeType(_CANT_UPDATE_PRODUCT_FEED,true);
                            
                                public static final StatusDetailCodeType CANT_UPDATE_RECRD_HAS_CHANGED =
                                    new StatusDetailCodeType(_CANT_UPDATE_RECRD_HAS_CHANGED,true);
                            
                                public static final StatusDetailCodeType CANT_UPDATE_RECUR_EVENT =
                                    new StatusDetailCodeType(_CANT_UPDATE_RECUR_EVENT,true);
                            
                                public static final StatusDetailCodeType CANT_UPDATE_ROOT_CATEGORY =
                                    new StatusDetailCodeType(_CANT_UPDATE_ROOT_CATEGORY,true);
                            
                                public static final StatusDetailCodeType CANT_UPDATE_STATUS_TYPE =
                                    new StatusDetailCodeType(_CANT_UPDATE_STATUS_TYPE,true);
                            
                                public static final StatusDetailCodeType CANT_VERIFY_CARD =
                                    new StatusDetailCodeType(_CANT_VERIFY_CARD,true);
                            
                                public static final StatusDetailCodeType CANT_VOID_TRANS =
                                    new StatusDetailCodeType(_CANT_VOID_TRANS,true);
                            
                                public static final StatusDetailCodeType CARD_EXPIRED =
                                    new StatusDetailCodeType(_CARD_EXPIRED,true);
                            
                                public static final StatusDetailCodeType CARD_ID_REQD =
                                    new StatusDetailCodeType(_CARD_ID_REQD,true);
                            
                                public static final StatusDetailCodeType CASE_ALREADY_ASSIGNED =
                                    new StatusDetailCodeType(_CASE_ALREADY_ASSIGNED,true);
                            
                                public static final StatusDetailCodeType CASE_DSNT_EXIST =
                                    new StatusDetailCodeType(_CASE_DSNT_EXIST,true);
                            
                                public static final StatusDetailCodeType CASE_NOT_GROUP_MEMBER =
                                    new StatusDetailCodeType(_CASE_NOT_GROUP_MEMBER,true);
                            
                                public static final StatusDetailCodeType CASH_SALE_EDIT_DISALLWD =
                                    new StatusDetailCodeType(_CASH_SALE_EDIT_DISALLWD,true);
                            
                                public static final StatusDetailCodeType CC_ACCT_REQD =
                                    new StatusDetailCodeType(_CC_ACCT_REQD,true);
                            
                                public static final StatusDetailCodeType CC_ALREADY_SAVED =
                                    new StatusDetailCodeType(_CC_ALREADY_SAVED,true);
                            
                                public static final StatusDetailCodeType CC_EMAIL_ADDRESS_REQD =
                                    new StatusDetailCodeType(_CC_EMAIL_ADDRESS_REQD,true);
                            
                                public static final StatusDetailCodeType CC_NUM_REQD =
                                    new StatusDetailCodeType(_CC_NUM_REQD,true);
                            
                                public static final StatusDetailCodeType CC_PROCESSOR_ERROR =
                                    new StatusDetailCodeType(_CC_PROCESSOR_ERROR,true);
                            
                                public static final StatusDetailCodeType CC_PROCESSOR_NOT_FOUND =
                                    new StatusDetailCodeType(_CC_PROCESSOR_NOT_FOUND,true);
                            
                                public static final StatusDetailCodeType CC_SECURITY_CODE_REQD =
                                    new StatusDetailCodeType(_CC_SECURITY_CODE_REQD,true);
                            
                                public static final StatusDetailCodeType CERT_AUTH_EXPD =
                                    new StatusDetailCodeType(_CERT_AUTH_EXPD,true);
                            
                                public static final StatusDetailCodeType CERT_EXPD =
                                    new StatusDetailCodeType(_CERT_EXPD,true);
                            
                                public static final StatusDetailCodeType CERT_UNAVAILABLE =
                                    new StatusDetailCodeType(_CERT_UNAVAILABLE,true);
                            
                                public static final StatusDetailCodeType CHANGE_PMT_DATE_AND_REAPPROVE =
                                    new StatusDetailCodeType(_CHANGE_PMT_DATE_AND_REAPPROVE,true);
                            
                                public static final StatusDetailCodeType CHAR_ERROR =
                                    new StatusDetailCodeType(_CHAR_ERROR,true);
                            
                                public static final StatusDetailCodeType CHECKOUT_EMAIL_REQD =
                                    new StatusDetailCodeType(_CHECKOUT_EMAIL_REQD,true);
                            
                                public static final StatusDetailCodeType CITY_REQD =
                                    new StatusDetailCodeType(_CITY_REQD,true);
                            
                                public static final StatusDetailCodeType CLASS_ALREADY_EXISTS =
                                    new StatusDetailCodeType(_CLASS_ALREADY_EXISTS,true);
                            
                                public static final StatusDetailCodeType CLASS_OR_DEPT_OR_CUST_REQD =
                                    new StatusDetailCodeType(_CLASS_OR_DEPT_OR_CUST_REQD,true);
                            
                                public static final StatusDetailCodeType CLEAR_AUTOCALC =
                                    new StatusDetailCodeType(_CLEAR_AUTOCALC,true);
                            
                                public static final StatusDetailCodeType CLOSED_TRAN_PRD =
                                    new StatusDetailCodeType(_CLOSED_TRAN_PRD,true);
                            
                                public static final StatusDetailCodeType CLOSE_PREVIOUSE_PERIOD =
                                    new StatusDetailCodeType(_CLOSE_PREVIOUSE_PERIOD,true);
                            
                                public static final StatusDetailCodeType COGS_ERROR =
                                    new StatusDetailCodeType(_COGS_ERROR,true);
                            
                                public static final StatusDetailCodeType COMMSSN_ALREADY_CALCLTD =
                                    new StatusDetailCodeType(_COMMSSN_ALREADY_CALCLTD,true);
                            
                                public static final StatusDetailCodeType COMMSSN_FEATURE_DISABLED =
                                    new StatusDetailCodeType(_COMMSSN_FEATURE_DISABLED,true);
                            
                                public static final StatusDetailCodeType COMMSSN_PAYROLL_ITEM_REQD =
                                    new StatusDetailCodeType(_COMMSSN_PAYROLL_ITEM_REQD,true);
                            
                                public static final StatusDetailCodeType COMPANION_PROP_REQD =
                                    new StatusDetailCodeType(_COMPANION_PROP_REQD,true);
                            
                                public static final StatusDetailCodeType COMPANY_FLD_REQD =
                                    new StatusDetailCodeType(_COMPANY_FLD_REQD,true);
                            
                                public static final StatusDetailCodeType COMP_DELETED_OR_MERGED =
                                    new StatusDetailCodeType(_COMP_DELETED_OR_MERGED,true);
                            
                                public static final StatusDetailCodeType CONCUR_BILLPAY_JOB_DISALLWD =
                                    new StatusDetailCodeType(_CONCUR_BILLPAY_JOB_DISALLWD,true);
                            
                                public static final StatusDetailCodeType CONCUR_BULK_JOB_DISALLWD =
                                    new StatusDetailCodeType(_CONCUR_BULK_JOB_DISALLWD,true);
                            
                                public static final StatusDetailCodeType CONCUR_MASS_UPDATE_DISALLWD =
                                    new StatusDetailCodeType(_CONCUR_MASS_UPDATE_DISALLWD,true);
                            
                                public static final StatusDetailCodeType CONCUR_SEARCH_DISALLWD =
                                    new StatusDetailCodeType(_CONCUR_SEARCH_DISALLWD,true);
                            
                                public static final StatusDetailCodeType CONSLD_PRNT_AND_CHILD_DISALLWD =
                                    new StatusDetailCodeType(_CONSLD_PRNT_AND_CHILD_DISALLWD,true);
                            
                                public static final StatusDetailCodeType CONTACT_ALREADY_EXISTS =
                                    new StatusDetailCodeType(_CONTACT_ALREADY_EXISTS,true);
                            
                                public static final StatusDetailCodeType CONTACT_NOT_GROUP_MEMBR =
                                    new StatusDetailCodeType(_CONTACT_NOT_GROUP_MEMBR,true);
                            
                                public static final StatusDetailCodeType COOKIES_DISABLED =
                                    new StatusDetailCodeType(_COOKIES_DISABLED,true);
                            
                                public static final StatusDetailCodeType COUNTRY_STATE_MISMATCH =
                                    new StatusDetailCodeType(_COUNTRY_STATE_MISMATCH,true);
                            
                                public static final StatusDetailCodeType CREATEDFROM_REQD =
                                    new StatusDetailCodeType(_CREATEDFROM_REQD,true);
                            
                                public static final StatusDetailCodeType CREDITS_DISALLWD =
                                    new StatusDetailCodeType(_CREDITS_DISALLWD,true);
                            
                                public static final StatusDetailCodeType CRNCY_MISMATCH_BASE_CRNCY =
                                    new StatusDetailCodeType(_CRNCY_MISMATCH_BASE_CRNCY,true);
                            
                                public static final StatusDetailCodeType CRNCY_NOT_UPDATED =
                                    new StatusDetailCodeType(_CRNCY_NOT_UPDATED,true);
                            
                                public static final StatusDetailCodeType CRNCY_RCRD_DELETED =
                                    new StatusDetailCodeType(_CRNCY_RCRD_DELETED,true);
                            
                                public static final StatusDetailCodeType CRNCY_REQD =
                                    new StatusDetailCodeType(_CRNCY_REQD,true);
                            
                                public static final StatusDetailCodeType CSC_SETUP_REQD =
                                    new StatusDetailCodeType(_CSC_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType CSTM_FIELD_KEY_REQD =
                                    new StatusDetailCodeType(_CSTM_FIELD_KEY_REQD,true);
                            
                                public static final StatusDetailCodeType CSTM_FIELD_VALUE_REQD =
                                    new StatusDetailCodeType(_CSTM_FIELD_VALUE_REQD,true);
                            
                                public static final StatusDetailCodeType CUST_ARLEADY_HAS_ACCT =
                                    new StatusDetailCodeType(_CUST_ARLEADY_HAS_ACCT,true);
                            
                                public static final StatusDetailCodeType CUST_CNTR_USER_ACCESS_ONLY =
                                    new StatusDetailCodeType(_CUST_CNTR_USER_ACCESS_ONLY,true);
                            
                                public static final StatusDetailCodeType CUST_LEAD_NOT_GROUP_MEMBR =
                                    new StatusDetailCodeType(_CUST_LEAD_NOT_GROUP_MEMBR,true);
                            
                                public static final StatusDetailCodeType CYBERSOURCE_ERROR =
                                    new StatusDetailCodeType(_CYBERSOURCE_ERROR,true);
                            
                                public static final StatusDetailCodeType CYCLE_IN_PROJECT_PLAN =
                                    new StatusDetailCodeType(_CYCLE_IN_PROJECT_PLAN,true);
                            
                                public static final StatusDetailCodeType DASHBOARD_LOCKED =
                                    new StatusDetailCodeType(_DASHBOARD_LOCKED,true);
                            
                                public static final StatusDetailCodeType DATA_MUST_BE_UNIQUE =
                                    new StatusDetailCodeType(_DATA_MUST_BE_UNIQUE,true);
                            
                                public static final StatusDetailCodeType DATA_REQD =
                                    new StatusDetailCodeType(_DATA_REQD,true);
                            
                                public static final StatusDetailCodeType DATE_EXPECTED =
                                    new StatusDetailCodeType(_DATE_EXPECTED,true);
                            
                                public static final StatusDetailCodeType DATE_PARAM_REQD =
                                    new StatusDetailCodeType(_DATE_PARAM_REQD,true);
                            
                                public static final StatusDetailCodeType DATE_PRD_MISMATCH =
                                    new StatusDetailCodeType(_DATE_PRD_MISMATCH,true);
                            
                                public static final StatusDetailCodeType DEFAULT_CUR_REQD =
                                    new StatusDetailCodeType(_DEFAULT_CUR_REQD,true);
                            
                                public static final StatusDetailCodeType DEFAULT_EXPENSE_ACCT_REQD =
                                    new StatusDetailCodeType(_DEFAULT_EXPENSE_ACCT_REQD,true);
                            
                                public static final StatusDetailCodeType DEFAULT_ISSUE_OWNER_REQD =
                                    new StatusDetailCodeType(_DEFAULT_ISSUE_OWNER_REQD,true);
                            
                                public static final StatusDetailCodeType DEFAULT_LIAB_ACCT_REQD =
                                    new StatusDetailCodeType(_DEFAULT_LIAB_ACCT_REQD,true);
                            
                                public static final StatusDetailCodeType DEFAULT_ROLE_REQD =
                                    new StatusDetailCodeType(_DEFAULT_ROLE_REQD,true);
                            
                                public static final StatusDetailCodeType DEFAULT_TYPE_DELETE_DISALLWD =
                                    new StatusDetailCodeType(_DEFAULT_TYPE_DELETE_DISALLWD,true);
                            
                                public static final StatusDetailCodeType DEFERRAL_ACCT_REQD =
                                    new StatusDetailCodeType(_DEFERRAL_ACCT_REQD,true);
                            
                                public static final StatusDetailCodeType DEFERRED_REV_REC_ACCT_REQD =
                                    new StatusDetailCodeType(_DEFERRED_REV_REC_ACCT_REQD,true);
                            
                                public static final StatusDetailCodeType DEPT_IN_USE =
                                    new StatusDetailCodeType(_DEPT_IN_USE,true);
                            
                                public static final StatusDetailCodeType DFRNT_SWAP_PRICE_LEVELS_REQD =
                                    new StatusDetailCodeType(_DFRNT_SWAP_PRICE_LEVELS_REQD,true);
                            
                                public static final StatusDetailCodeType DISALLWD_IP_ADDRESS =
                                    new StatusDetailCodeType(_DISALLWD_IP_ADDRESS,true);
                            
                                public static final StatusDetailCodeType DISCOUNT_ACCT_SETUP_REQD =
                                    new StatusDetailCodeType(_DISCOUNT_ACCT_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType DISCOUNT_DISALLWD =
                                    new StatusDetailCodeType(_DISCOUNT_DISALLWD,true);
                            
                                public static final StatusDetailCodeType DISCOUNT_DISALLWD_VSOE =
                                    new StatusDetailCodeType(_DISCOUNT_DISALLWD_VSOE,true);
                            
                                public static final StatusDetailCodeType DISCOUNT_EXCEED_TOTAL =
                                    new StatusDetailCodeType(_DISCOUNT_EXCEED_TOTAL,true);
                            
                                public static final StatusDetailCodeType DISTRIB_REQD_ONE_DAY_BFORE =
                                    new StatusDetailCodeType(_DISTRIB_REQD_ONE_DAY_BFORE,true);
                            
                                public static final StatusDetailCodeType DOMAIN_IN_USE =
                                    new StatusDetailCodeType(_DOMAIN_IN_USE,true);
                            
                                public static final StatusDetailCodeType DOMAIN_WEBSITE_REQD =
                                    new StatusDetailCodeType(_DOMAIN_WEBSITE_REQD,true);
                            
                                public static final StatusDetailCodeType DROP_SHIP_ERROR =
                                    new StatusDetailCodeType(_DROP_SHIP_ERROR,true);
                            
                                public static final StatusDetailCodeType DROP_SHIP_OR_SPECIAL_ORD_ALLWD =
                                    new StatusDetailCodeType(_DROP_SHIP_OR_SPECIAL_ORD_ALLWD,true);
                            
                                public static final StatusDetailCodeType DUE_DATE_BFORE_START_DATE =
                                    new StatusDetailCodeType(_DUE_DATE_BFORE_START_DATE,true);
                            
                                public static final StatusDetailCodeType DUE_DATE_REQD =
                                    new StatusDetailCodeType(_DUE_DATE_REQD,true);
                            
                                public static final StatusDetailCodeType DUPLICATE_INVENTORY_NUM =
                                    new StatusDetailCodeType(_DUPLICATE_INVENTORY_NUM,true);
                            
                                public static final StatusDetailCodeType DUPLICATE_KEYS =
                                    new StatusDetailCodeType(_DUPLICATE_KEYS,true);
                            
                                public static final StatusDetailCodeType DUPLICATE_METHOD_NAME =
                                    new StatusDetailCodeType(_DUPLICATE_METHOD_NAME,true);
                            
                                public static final StatusDetailCodeType DUPLICATE_NAME_FOR_PRD =
                                    new StatusDetailCodeType(_DUPLICATE_NAME_FOR_PRD,true);
                            
                                public static final StatusDetailCodeType DUPLICATE_NAME_FOR_ROLE =
                                    new StatusDetailCodeType(_DUPLICATE_NAME_FOR_ROLE,true);
                            
                                public static final StatusDetailCodeType DUPLICATE_RELATIONSHIP =
                                    new StatusDetailCodeType(_DUPLICATE_RELATIONSHIP,true);
                            
                                public static final StatusDetailCodeType DUP_ACCT_NAME =
                                    new StatusDetailCodeType(_DUP_ACCT_NAME,true);
                            
                                public static final StatusDetailCodeType DUP_ACCT_NOT_ALLWD =
                                    new StatusDetailCodeType(_DUP_ACCT_NOT_ALLWD,true);
                            
                                public static final StatusDetailCodeType DUP_ACCT_NUM =
                                    new StatusDetailCodeType(_DUP_ACCT_NUM,true);
                            
                                public static final StatusDetailCodeType DUP_ACCT_ON_TRANS =
                                    new StatusDetailCodeType(_DUP_ACCT_ON_TRANS,true);
                            
                                public static final StatusDetailCodeType DUP_BIN =
                                    new StatusDetailCodeType(_DUP_BIN,true);
                            
                                public static final StatusDetailCodeType DUP_BUNDLE_IN_ACCT =
                                    new StatusDetailCodeType(_DUP_BUNDLE_IN_ACCT,true);
                            
                                public static final StatusDetailCodeType DUP_CATEGORY =
                                    new StatusDetailCodeType(_DUP_CATEGORY,true);
                            
                                public static final StatusDetailCodeType DUP_CATEGORY_NAME =
                                    new StatusDetailCodeType(_DUP_CATEGORY_NAME,true);
                            
                                public static final StatusDetailCodeType DUP_COLOR_THEME =
                                    new StatusDetailCodeType(_DUP_COLOR_THEME,true);
                            
                                public static final StatusDetailCodeType DUP_CSTM_FIELD =
                                    new StatusDetailCodeType(_DUP_CSTM_FIELD,true);
                            
                                public static final StatusDetailCodeType DUP_CSTM_LAYOUT =
                                    new StatusDetailCodeType(_DUP_CSTM_LAYOUT,true);
                            
                                public static final StatusDetailCodeType DUP_CSTM_LIST =
                                    new StatusDetailCodeType(_DUP_CSTM_LIST,true);
                            
                                public static final StatusDetailCodeType DUP_CSTM_RCRD =
                                    new StatusDetailCodeType(_DUP_CSTM_RCRD,true);
                            
                                public static final StatusDetailCodeType DUP_CSTM_RCRD_ENTRY =
                                    new StatusDetailCodeType(_DUP_CSTM_RCRD_ENTRY,true);
                            
                                public static final StatusDetailCodeType DUP_CSTM_TAB =
                                    new StatusDetailCodeType(_DUP_CSTM_TAB,true);
                            
                                public static final StatusDetailCodeType DUP_EMPL_EMAIL =
                                    new StatusDetailCodeType(_DUP_EMPL_EMAIL,true);
                            
                                public static final StatusDetailCodeType DUP_EMPL_ENTITY_NAME =
                                    new StatusDetailCodeType(_DUP_EMPL_ENTITY_NAME,true);
                            
                                public static final StatusDetailCodeType DUP_EMPL_TMPLT =
                                    new StatusDetailCodeType(_DUP_EMPL_TMPLT,true);
                            
                                public static final StatusDetailCodeType DUP_ENTITY =
                                    new StatusDetailCodeType(_DUP_ENTITY,true);
                            
                                public static final StatusDetailCodeType DUP_ENTITY_EMAIL =
                                    new StatusDetailCodeType(_DUP_ENTITY_EMAIL,true);
                            
                                public static final StatusDetailCodeType DUP_ENTITY_NAME =
                                    new StatusDetailCodeType(_DUP_ENTITY_NAME,true);
                            
                                public static final StatusDetailCodeType DUP_FEDEX_ACCT_NUM =
                                    new StatusDetailCodeType(_DUP_FEDEX_ACCT_NUM,true);
                            
                                public static final StatusDetailCodeType DUP_FINANCL_STATMNT_LAYOUT =
                                    new StatusDetailCodeType(_DUP_FINANCL_STATMNT_LAYOUT,true);
                            
                                public static final StatusDetailCodeType DUP_INFO_ITEM =
                                    new StatusDetailCodeType(_DUP_INFO_ITEM,true);
                            
                                public static final StatusDetailCodeType DUP_ISSUE_NAME_OR_NUM =
                                    new StatusDetailCodeType(_DUP_ISSUE_NAME_OR_NUM,true);
                            
                                public static final StatusDetailCodeType DUP_ITEM =
                                    new StatusDetailCodeType(_DUP_ITEM,true);
                            
                                public static final StatusDetailCodeType DUP_ITEM_LAYOUT =
                                    new StatusDetailCodeType(_DUP_ITEM_LAYOUT,true);
                            
                                public static final StatusDetailCodeType DUP_ITEM_NAME =
                                    new StatusDetailCodeType(_DUP_ITEM_NAME,true);
                            
                                public static final StatusDetailCodeType DUP_ITEM_OPTION =
                                    new StatusDetailCodeType(_DUP_ITEM_OPTION,true);
                            
                                public static final StatusDetailCodeType DUP_ITEM_TMPLT =
                                    new StatusDetailCodeType(_DUP_ITEM_TMPLT,true);
                            
                                public static final StatusDetailCodeType DUP_MATRIX_OPTN_ABBRV =
                                    new StatusDetailCodeType(_DUP_MATRIX_OPTN_ABBRV,true);
                            
                                public static final StatusDetailCodeType DUP_MEMRZD_TRANS =
                                    new StatusDetailCodeType(_DUP_MEMRZD_TRANS,true);
                            
                                public static final StatusDetailCodeType DUP_NAME =
                                    new StatusDetailCodeType(_DUP_NAME,true);
                            
                                public static final StatusDetailCodeType DUP_PAYROLL_ITEM =
                                    new StatusDetailCodeType(_DUP_PAYROLL_ITEM,true);
                            
                                public static final StatusDetailCodeType DUP_PRESNTN_CAT =
                                    new StatusDetailCodeType(_DUP_PRESNTN_CAT,true);
                            
                                public static final StatusDetailCodeType DUP_RCRD =
                                    new StatusDetailCodeType(_DUP_RCRD,true);
                            
                                public static final StatusDetailCodeType DUP_RCRD_LINK =
                                    new StatusDetailCodeType(_DUP_RCRD_LINK,true);
                            
                                public static final StatusDetailCodeType DUP_SALES_TAX_ITEM =
                                    new StatusDetailCodeType(_DUP_SALES_TAX_ITEM,true);
                            
                                public static final StatusDetailCodeType DUP_SHIPPING_ITEM =
                                    new StatusDetailCodeType(_DUP_SHIPPING_ITEM,true);
                            
                                public static final StatusDetailCodeType DUP_SHORT_NAME =
                                    new StatusDetailCodeType(_DUP_SHORT_NAME,true);
                            
                                public static final StatusDetailCodeType DUP_SITE_THEME =
                                    new StatusDetailCodeType(_DUP_SITE_THEME,true);
                            
                                public static final StatusDetailCodeType DUP_SOURCE_ACCT =
                                    new StatusDetailCodeType(_DUP_SOURCE_ACCT,true);
                            
                                public static final StatusDetailCodeType DUP_TAX_CODE =
                                    new StatusDetailCodeType(_DUP_TAX_CODE,true);
                            
                                public static final StatusDetailCodeType DUP_TRACKING_NUM =
                                    new StatusDetailCodeType(_DUP_TRACKING_NUM,true);
                            
                                public static final StatusDetailCodeType DUP_TRANS =
                                    new StatusDetailCodeType(_DUP_TRANS,true);
                            
                                public static final StatusDetailCodeType DUP_UPS_ACCT_NUM =
                                    new StatusDetailCodeType(_DUP_UPS_ACCT_NUM,true);
                            
                                public static final StatusDetailCodeType DUP_VENDOR_EMAIL =
                                    new StatusDetailCodeType(_DUP_VENDOR_EMAIL,true);
                            
                                public static final StatusDetailCodeType DUP_VENDOR_NAME =
                                    new StatusDetailCodeType(_DUP_VENDOR_NAME,true);
                            
                                public static final StatusDetailCodeType EARNING_ITEM_REQD =
                                    new StatusDetailCodeType(_EARNING_ITEM_REQD,true);
                            
                                public static final StatusDetailCodeType EBAY_FEE_ERROR =
                                    new StatusDetailCodeType(_EBAY_FEE_ERROR,true);
                            
                                public static final StatusDetailCodeType EBAY_TMPLT_ERROR =
                                    new StatusDetailCodeType(_EBAY_TMPLT_ERROR,true);
                            
                                public static final StatusDetailCodeType EDITION_DSNT_SUPRT_WORLDPAY =
                                    new StatusDetailCodeType(_EDITION_DSNT_SUPRT_WORLDPAY,true);
                            
                                public static final StatusDetailCodeType EIN_OR_TIN_REQD =
                                    new StatusDetailCodeType(_EIN_OR_TIN_REQD,true);
                            
                                public static final StatusDetailCodeType EMAIL_ADDRS_REQD =
                                    new StatusDetailCodeType(_EMAIL_ADDRS_REQD,true);
                            
                                public static final StatusDetailCodeType EMAIL_ADDRS_REQD_TO_NOTIFY =
                                    new StatusDetailCodeType(_EMAIL_ADDRS_REQD_TO_NOTIFY,true);
                            
                                public static final StatusDetailCodeType EMAIL_REQD =
                                    new StatusDetailCodeType(_EMAIL_REQD,true);
                            
                                public static final StatusDetailCodeType EMAIL_REQD_ACCT_PROVISION =
                                    new StatusDetailCodeType(_EMAIL_REQD_ACCT_PROVISION,true);
                            
                                public static final StatusDetailCodeType EMAIL_REQ_HANDLER_ERROR =
                                    new StatusDetailCodeType(_EMAIL_REQ_HANDLER_ERROR,true);
                            
                                public static final StatusDetailCodeType EMPL_IN_USE =
                                    new StatusDetailCodeType(_EMPL_IN_USE,true);
                            
                                public static final StatusDetailCodeType ERROR_DELETE_CARD_DATA =
                                    new StatusDetailCodeType(_ERROR_DELETE_CARD_DATA,true);
                            
                                public static final StatusDetailCodeType ERROR_IN_TERRITORY_ASSGNMNT =
                                    new StatusDetailCodeType(_ERROR_IN_TERRITORY_ASSGNMNT,true);
                            
                                public static final StatusDetailCodeType ERROR_PRCSSNG_TRANS =
                                    new StatusDetailCodeType(_ERROR_PRCSSNG_TRANS,true);
                            
                                public static final StatusDetailCodeType ERROR_REFUND_TRANS =
                                    new StatusDetailCodeType(_ERROR_REFUND_TRANS,true);
                            
                                public static final StatusDetailCodeType ERROR_REVERSE_AUTH =
                                    new StatusDetailCodeType(_ERROR_REVERSE_AUTH,true);
                            
                                public static final StatusDetailCodeType ERROR_SENDING_TRAN_EMAIL =
                                    new StatusDetailCodeType(_ERROR_SENDING_TRAN_EMAIL,true);
                            
                                public static final StatusDetailCodeType ERROR_VOID_TRANS =
                                    new StatusDetailCodeType(_ERROR_VOID_TRANS,true);
                            
                                public static final StatusDetailCodeType EVENT_ID_NOT_FOUND =
                                    new StatusDetailCodeType(_EVENT_ID_NOT_FOUND,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_MAX_ALLWD_LOC =
                                    new StatusDetailCodeType(_EXCEEDED_MAX_ALLWD_LOC,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_MAX_CONCUR_RQST =
                                    new StatusDetailCodeType(_EXCEEDED_MAX_CONCUR_RQST,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_MAX_EMAILS =
                                    new StatusDetailCodeType(_EXCEEDED_MAX_EMAILS,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_MAX_FEATURED_ITEMS =
                                    new StatusDetailCodeType(_EXCEEDED_MAX_FEATURED_ITEMS,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_MAX_FIELD_LENGTH =
                                    new StatusDetailCodeType(_EXCEEDED_MAX_FIELD_LENGTH,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_MAX_MATRIX_OPTNS =
                                    new StatusDetailCodeType(_EXCEEDED_MAX_MATRIX_OPTNS,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_MAX_PDF_ELEMENTS =
                                    new StatusDetailCodeType(_EXCEEDED_MAX_PDF_ELEMENTS,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_MAX_PDF_EXPORT_COL =
                                    new StatusDetailCodeType(_EXCEEDED_MAX_PDF_EXPORT_COL,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_MAX_PIN_RETRIES =
                                    new StatusDetailCodeType(_EXCEEDED_MAX_PIN_RETRIES,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_MAX_RCRD =
                                    new StatusDetailCodeType(_EXCEEDED_MAX_RCRD,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_MAX_REPORT_COL =
                                    new StatusDetailCodeType(_EXCEEDED_MAX_REPORT_COL,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_MAX_REPORT_ROWS =
                                    new StatusDetailCodeType(_EXCEEDED_MAX_REPORT_ROWS,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_MAX_REPORT_SIZE =
                                    new StatusDetailCodeType(_EXCEEDED_MAX_REPORT_SIZE,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_MAX_SESSIONS =
                                    new StatusDetailCodeType(_EXCEEDED_MAX_SESSIONS,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_MAX_SHIP_PACKAGE =
                                    new StatusDetailCodeType(_EXCEEDED_MAX_SHIP_PACKAGE,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_MAX_TIME =
                                    new StatusDetailCodeType(_EXCEEDED_MAX_TIME,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_MAX_TRANS_LINES =
                                    new StatusDetailCodeType(_EXCEEDED_MAX_TRANS_LINES,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_MAX_USERS_ALLWD =
                                    new StatusDetailCodeType(_EXCEEDED_MAX_USERS_ALLWD,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_PER_TRANS_MAX =
                                    new StatusDetailCodeType(_EXCEEDED_PER_TRANS_MAX,true);
                            
                                public static final StatusDetailCodeType EXCEEDED_RQST_SIZE_LIMIT =
                                    new StatusDetailCodeType(_EXCEEDED_RQST_SIZE_LIMIT,true);
                            
                                public static final StatusDetailCodeType EXCEEDS_ALLWD_LICENSES =
                                    new StatusDetailCodeType(_EXCEEDS_ALLWD_LICENSES,true);
                            
                                public static final StatusDetailCodeType EXPENSE_ENTRY_DISALLWD =
                                    new StatusDetailCodeType(_EXPENSE_ENTRY_DISALLWD,true);
                            
                                public static final StatusDetailCodeType EXPIRED_SEARCH_CRITERIA =
                                    new StatusDetailCodeType(_EXPIRED_SEARCH_CRITERIA,true);
                            
                                public static final StatusDetailCodeType EXTERNALID_NOT_SUPPORTED =
                                    new StatusDetailCodeType(_EXTERNALID_NOT_SUPPORTED,true);
                            
                                public static final StatusDetailCodeType EXTERNALID_REQD =
                                    new StatusDetailCodeType(_EXTERNALID_REQD,true);
                            
                                public static final StatusDetailCodeType EXT_CAT_LINK_SETUP_REQD =
                                    new StatusDetailCodeType(_EXT_CAT_LINK_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType FAILED_FEDEX_LABEL_VOID =
                                    new StatusDetailCodeType(_FAILED_FEDEX_LABEL_VOID,true);
                            
                                public static final StatusDetailCodeType FAILED_FORM_VALIDATION =
                                    new StatusDetailCodeType(_FAILED_FORM_VALIDATION,true);
                            
                                public static final StatusDetailCodeType FAILED_UPS_LABEL_VOID =
                                    new StatusDetailCodeType(_FAILED_UPS_LABEL_VOID,true);
                            
                                public static final StatusDetailCodeType FAX_NUM_REQD =
                                    new StatusDetailCodeType(_FAX_NUM_REQD,true);
                            
                                public static final StatusDetailCodeType FAX_SETUP_REQD =
                                    new StatusDetailCodeType(_FAX_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType FEATURE_DISABLED =
                                    new StatusDetailCodeType(_FEATURE_DISABLED,true);
                            
                                public static final StatusDetailCodeType FEATURE_UNAVAILABLE =
                                    new StatusDetailCodeType(_FEATURE_UNAVAILABLE,true);
                            
                                public static final StatusDetailCodeType FEDEX_ACCT_REQD =
                                    new StatusDetailCodeType(_FEDEX_ACCT_REQD,true);
                            
                                public static final StatusDetailCodeType FEDEX_CANT_INTEGRATE_FULFILL =
                                    new StatusDetailCodeType(_FEDEX_CANT_INTEGRATE_FULFILL,true);
                            
                                public static final StatusDetailCodeType FEDEX_DROPOFF_TYP_REQD =
                                    new StatusDetailCodeType(_FEDEX_DROPOFF_TYP_REQD,true);
                            
                                public static final StatusDetailCodeType FEDEX_INVALID_ACCT_NUM =
                                    new StatusDetailCodeType(_FEDEX_INVALID_ACCT_NUM,true);
                            
                                public static final StatusDetailCodeType FEDEX_ITEM_CONTENTS_REQD =
                                    new StatusDetailCodeType(_FEDEX_ITEM_CONTENTS_REQD,true);
                            
                                public static final StatusDetailCodeType FEDEX_METER_NOT_RETRIEVED =
                                    new StatusDetailCodeType(_FEDEX_METER_NOT_RETRIEVED,true);
                            
                                public static final StatusDetailCodeType FEDEX_METER_REQD =
                                    new StatusDetailCodeType(_FEDEX_METER_REQD,true);
                            
                                public static final StatusDetailCodeType FEDEX_ONE_PACKG_ALLWD =
                                    new StatusDetailCodeType(_FEDEX_ONE_PACKG_ALLWD,true);
                            
                                public static final StatusDetailCodeType FEDEX_ORIGIN_COUNTRY_US_REQD =
                                    new StatusDetailCodeType(_FEDEX_ORIGIN_COUNTRY_US_REQD,true);
                            
                                public static final StatusDetailCodeType FEDEX_RATING_SRVC_UNAVAILBL =
                                    new StatusDetailCodeType(_FEDEX_RATING_SRVC_UNAVAILBL,true);
                            
                                public static final StatusDetailCodeType FEDEX_REG_NOT_FOUND =
                                    new StatusDetailCodeType(_FEDEX_REG_NOT_FOUND,true);
                            
                                public static final StatusDetailCodeType FEDEX_SHIP_SRVC_REQD =
                                    new StatusDetailCodeType(_FEDEX_SHIP_SRVC_REQD,true);
                            
                                public static final StatusDetailCodeType FEDEX_SHIP_SRVC_UNAVAILBL =
                                    new StatusDetailCodeType(_FEDEX_SHIP_SRVC_UNAVAILBL,true);
                            
                                public static final StatusDetailCodeType FEDEX_UNSUPRTD_ORIGIN_COUNTRY =
                                    new StatusDetailCodeType(_FEDEX_UNSUPRTD_ORIGIN_COUNTRY,true);
                            
                                public static final StatusDetailCodeType FEDEX_USD_EXCHANGE_RATE_REQD =
                                    new StatusDetailCodeType(_FEDEX_USD_EXCHANGE_RATE_REQD,true);
                            
                                public static final StatusDetailCodeType FEDEX_USER_ERROR =
                                    new StatusDetailCodeType(_FEDEX_USER_ERROR,true);
                            
                                public static final StatusDetailCodeType FEDEX_VOID_ERROR =
                                    new StatusDetailCodeType(_FEDEX_VOID_ERROR,true);
                            
                                public static final StatusDetailCodeType FED_ID_REQD =
                                    new StatusDetailCodeType(_FED_ID_REQD,true);
                            
                                public static final StatusDetailCodeType FED_WITHHOLDING_REQD =
                                    new StatusDetailCodeType(_FED_WITHHOLDING_REQD,true);
                            
                                public static final StatusDetailCodeType FIELD_CALL_DATE_REQD =
                                    new StatusDetailCodeType(_FIELD_CALL_DATE_REQD,true);
                            
                                public static final StatusDetailCodeType FIELD_DEFN_REQD =
                                    new StatusDetailCodeType(_FIELD_DEFN_REQD,true);
                            
                                public static final StatusDetailCodeType FIELD_NOT_SETTABLE_ON_ADD =
                                    new StatusDetailCodeType(_FIELD_NOT_SETTABLE_ON_ADD,true);
                            
                                public static final StatusDetailCodeType FIELD_PARAM_REQD =
                                    new StatusDetailCodeType(_FIELD_PARAM_REQD,true);
                            
                                public static final StatusDetailCodeType FIELD_REQD =
                                    new StatusDetailCodeType(_FIELD_REQD,true);
                            
                                public static final StatusDetailCodeType FILE_ALREADY_EXISTS =
                                    new StatusDetailCodeType(_FILE_ALREADY_EXISTS,true);
                            
                                public static final StatusDetailCodeType FILE_DISALLWD_IN_ROOT_FLDR =
                                    new StatusDetailCodeType(_FILE_DISALLWD_IN_ROOT_FLDR,true);
                            
                                public static final StatusDetailCodeType FILE_MISSING =
                                    new StatusDetailCodeType(_FILE_MISSING,true);
                            
                                public static final StatusDetailCodeType FILE_NOT_DOWNLOADABLE =
                                    new StatusDetailCodeType(_FILE_NOT_DOWNLOADABLE,true);
                            
                                public static final StatusDetailCodeType FILE_NOT_FOUND =
                                    new StatusDetailCodeType(_FILE_NOT_FOUND,true);
                            
                                public static final StatusDetailCodeType FILE_REQD =
                                    new StatusDetailCodeType(_FILE_REQD,true);
                            
                                public static final StatusDetailCodeType FILE_UPLOAD_IN_PROGRESS =
                                    new StatusDetailCodeType(_FILE_UPLOAD_IN_PROGRESS,true);
                            
                                public static final StatusDetailCodeType FILTER_BY_AMT_REQD =
                                    new StatusDetailCodeType(_FILTER_BY_AMT_REQD,true);
                            
                                public static final StatusDetailCodeType FINANCE_CHARGE_SETUP_REQD =
                                    new StatusDetailCodeType(_FINANCE_CHARGE_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType FINANCE_CHARGE_SET_PREFS =
                                    new StatusDetailCodeType(_FINANCE_CHARGE_SET_PREFS,true);
                            
                                public static final StatusDetailCodeType FIRST_LAST_NAMES_REQD =
                                    new StatusDetailCodeType(_FIRST_LAST_NAMES_REQD,true);
                            
                                public static final StatusDetailCodeType FIRST_QTY_BUCKET_MUST_BE_ZERO =
                                    new StatusDetailCodeType(_FIRST_QTY_BUCKET_MUST_BE_ZERO,true);
                            
                                public static final StatusDetailCodeType FLD_VALUE_REQD =
                                    new StatusDetailCodeType(_FLD_VALUE_REQD,true);
                            
                                public static final StatusDetailCodeType FLD_VALUE_TOO_LARGE =
                                    new StatusDetailCodeType(_FLD_VALUE_TOO_LARGE,true);
                            
                                public static final StatusDetailCodeType FOLDER_ALREADY_EXISTS =
                                    new StatusDetailCodeType(_FOLDER_ALREADY_EXISTS,true);
                            
                                public static final StatusDetailCodeType FORMULA_ERROR =
                                    new StatusDetailCodeType(_FORMULA_ERROR,true);
                            
                                public static final StatusDetailCodeType FORM_RESUBMISSION_REQD =
                                    new StatusDetailCodeType(_FORM_RESUBMISSION_REQD,true);
                            
                                public static final StatusDetailCodeType FORM_SETUP_REQD =
                                    new StatusDetailCodeType(_FORM_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType FORM_UNAVAILBL_ONLINE =
                                    new StatusDetailCodeType(_FORM_UNAVAILBL_ONLINE,true);
                            
                                public static final StatusDetailCodeType FRIENDLY_NAME_REQD =
                                    new StatusDetailCodeType(_FRIENDLY_NAME_REQD,true);
                            
                                public static final StatusDetailCodeType FULFILL_REQD_FIELDS_MISSING =
                                    new StatusDetailCodeType(_FULFILL_REQD_FIELDS_MISSING,true);
                            
                                public static final StatusDetailCodeType FULFILL_REQD_PARAMS_MISSING =
                                    new StatusDetailCodeType(_FULFILL_REQD_PARAMS_MISSING,true);
                            
                                public static final StatusDetailCodeType FULL_DISTRIB_REQD =
                                    new StatusDetailCodeType(_FULL_DISTRIB_REQD,true);
                            
                                public static final StatusDetailCodeType FULL_USERS_REQD_TO_INTEGRATE =
                                    new StatusDetailCodeType(_FULL_USERS_REQD_TO_INTEGRATE,true);
                            
                                public static final StatusDetailCodeType FX_MALFORMED_RESPONSE =
                                    new StatusDetailCodeType(_FX_MALFORMED_RESPONSE,true);
                            
                                public static final StatusDetailCodeType FX_RATE_REQD_FEDEX_RATE =
                                    new StatusDetailCodeType(_FX_RATE_REQD_FEDEX_RATE,true);
                            
                                public static final StatusDetailCodeType FX_TRANS_DISALLWD =
                                    new StatusDetailCodeType(_FX_TRANS_DISALLWD,true);
                            
                                public static final StatusDetailCodeType GETALL_RCRD_TYPE_REQD =
                                    new StatusDetailCodeType(_GETALL_RCRD_TYPE_REQD,true);
                            
                                public static final StatusDetailCodeType GIFT_CERT_AMT_EXCEED_AVAILBL =
                                    new StatusDetailCodeType(_GIFT_CERT_AMT_EXCEED_AVAILBL,true);
                            
                                public static final StatusDetailCodeType GIFT_CERT_AUTH_ALREADY_EXISTS =
                                    new StatusDetailCodeType(_GIFT_CERT_AUTH_ALREADY_EXISTS,true);
                            
                                public static final StatusDetailCodeType GIFT_CERT_CAN_BE_USED_ONCE =
                                    new StatusDetailCodeType(_GIFT_CERT_CAN_BE_USED_ONCE,true);
                            
                                public static final StatusDetailCodeType GIFT_CERT_CODE_REQD =
                                    new StatusDetailCodeType(_GIFT_CERT_CODE_REQD,true);
                            
                                public static final StatusDetailCodeType GIFT_CERT_INVALID_NUM =
                                    new StatusDetailCodeType(_GIFT_CERT_INVALID_NUM,true);
                            
                                public static final StatusDetailCodeType GIFT_CERT_IN_USE =
                                    new StatusDetailCodeType(_GIFT_CERT_IN_USE,true);
                            
                                public static final StatusDetailCodeType GROUP_DSNT_EXIST =
                                    new StatusDetailCodeType(_GROUP_DSNT_EXIST,true);
                            
                                public static final StatusDetailCodeType GROUP_REQD =
                                    new StatusDetailCodeType(_GROUP_REQD,true);
                            
                                public static final StatusDetailCodeType GROUP_TYPE_REQD =
                                    new StatusDetailCodeType(_GROUP_TYPE_REQD,true);
                            
                                public static final StatusDetailCodeType GRTR_QTY_PRICE_LEVEL_REQD =
                                    new StatusDetailCodeType(_GRTR_QTY_PRICE_LEVEL_REQD,true);
                            
                                public static final StatusDetailCodeType ILLEGAL_PERIOD_STRUCTURE =
                                    new StatusDetailCodeType(_ILLEGAL_PERIOD_STRUCTURE,true);
                            
                                public static final StatusDetailCodeType INACTIVE_CC_PROFILE =
                                    new StatusDetailCodeType(_INACTIVE_CC_PROFILE,true);
                            
                                public static final StatusDetailCodeType INACTIVE_RCRD_FOR_ROLE =
                                    new StatusDetailCodeType(_INACTIVE_RCRD_FOR_ROLE,true);
                            
                                public static final StatusDetailCodeType INAVLID_FILE_TYP =
                                    new StatusDetailCodeType(_INAVLID_FILE_TYP,true);
                            
                                public static final StatusDetailCodeType INAVLID_ITEM_TYP =
                                    new StatusDetailCodeType(_INAVLID_ITEM_TYP,true);
                            
                                public static final StatusDetailCodeType INAVLID_PRICING_MTRX =
                                    new StatusDetailCodeType(_INAVLID_PRICING_MTRX,true);
                            
                                public static final StatusDetailCodeType INCOMPATIBLE_ACCT_CHANGE =
                                    new StatusDetailCodeType(_INCOMPATIBLE_ACCT_CHANGE,true);
                            
                                public static final StatusDetailCodeType INCOMPLETE_BILLING_ADDR =
                                    new StatusDetailCodeType(_INCOMPLETE_BILLING_ADDR,true);
                            
                                public static final StatusDetailCodeType INCOMPLETE_FILE_UPLOAD =
                                    new StatusDetailCodeType(_INCOMPLETE_FILE_UPLOAD,true);
                            
                                public static final StatusDetailCodeType INCRCT_ORD_INFO =
                                    new StatusDetailCodeType(_INCRCT_ORD_INFO,true);
                            
                                public static final StatusDetailCodeType INFINITE_LOOP_DETECTED =
                                    new StatusDetailCodeType(_INFINITE_LOOP_DETECTED,true);
                            
                                public static final StatusDetailCodeType INITIALIZE_ARG_REQD =
                                    new StatusDetailCodeType(_INITIALIZE_ARG_REQD,true);
                            
                                public static final StatusDetailCodeType INITIALIZE_AUXREF_REQD =
                                    new StatusDetailCodeType(_INITIALIZE_AUXREF_REQD,true);
                            
                                public static final StatusDetailCodeType INSTALL_SCRIPT_ERROR =
                                    new StatusDetailCodeType(_INSTALL_SCRIPT_ERROR,true);
                            
                                public static final StatusDetailCodeType INSUFCNT_NUM_PRDS_FOR_REV_REC =
                                    new StatusDetailCodeType(_INSUFCNT_NUM_PRDS_FOR_REV_REC,true);
                            
                                public static final StatusDetailCodeType INSUFCNT_OPEN_PRDS_FOR_REV_REC =
                                    new StatusDetailCodeType(_INSUFCNT_OPEN_PRDS_FOR_REV_REC,true);
                            
                                public static final StatusDetailCodeType INSUFFICIENT_CHARS_IN_SEARCH =
                                    new StatusDetailCodeType(_INSUFFICIENT_CHARS_IN_SEARCH,true);
                            
                                public static final StatusDetailCodeType INSUFFICIENT_FLD_PERMISSION =
                                    new StatusDetailCodeType(_INSUFFICIENT_FLD_PERMISSION,true);
                            
                                public static final StatusDetailCodeType INSUFFICIENT_FUND =
                                    new StatusDetailCodeType(_INSUFFICIENT_FUND,true);
                            
                                public static final StatusDetailCodeType INSUFFICIENT_PERMISSION =
                                    new StatusDetailCodeType(_INSUFFICIENT_PERMISSION,true);
                            
                                public static final StatusDetailCodeType INTEGER_REQD_FOR_QTY =
                                    new StatusDetailCodeType(_INTEGER_REQD_FOR_QTY,true);
                            
                                public static final StatusDetailCodeType INTL_FEDEX_ONE_PACKG_ALLWD =
                                    new StatusDetailCodeType(_INTL_FEDEX_ONE_PACKG_ALLWD,true);
                            
                                public static final StatusDetailCodeType INTL_SHIP_EXCEED_MAX_ITEM =
                                    new StatusDetailCodeType(_INTL_SHIP_EXCEED_MAX_ITEM,true);
                            
                                public static final StatusDetailCodeType INVALID_ABN =
                                    new StatusDetailCodeType(_INVALID_ABN,true);
                            
                                public static final StatusDetailCodeType INVALID_ACCT =
                                    new StatusDetailCodeType(_INVALID_ACCT,true);
                            
                                public static final StatusDetailCodeType INVALID_ACCT_NUM_CSTM_FIELD =
                                    new StatusDetailCodeType(_INVALID_ACCT_NUM_CSTM_FIELD,true);
                            
                                public static final StatusDetailCodeType INVALID_ACCT_PRD =
                                    new StatusDetailCodeType(_INVALID_ACCT_PRD,true);
                            
                                public static final StatusDetailCodeType INVALID_ACCT_TYP =
                                    new StatusDetailCodeType(_INVALID_ACCT_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_ACTION =
                                    new StatusDetailCodeType(_INVALID_ACTION,true);
                            
                                public static final StatusDetailCodeType INVALID_ADDRESS_OR_SHIPPER_NO =
                                    new StatusDetailCodeType(_INVALID_ADDRESS_OR_SHIPPER_NO,true);
                            
                                public static final StatusDetailCodeType INVALID_ADJUSTMENT_ACCT =
                                    new StatusDetailCodeType(_INVALID_ADJUSTMENT_ACCT,true);
                            
                                public static final StatusDetailCodeType INVALID_AES_FTSR_EXEMPTN_NUM =
                                    new StatusDetailCodeType(_INVALID_AES_FTSR_EXEMPTN_NUM,true);
                            
                                public static final StatusDetailCodeType INVALID_ALLOCTN_METHOD =
                                    new StatusDetailCodeType(_INVALID_ALLOCTN_METHOD,true);
                            
                                public static final StatusDetailCodeType INVALID_AMORTZN_ACCT =
                                    new StatusDetailCodeType(_INVALID_AMORTZN_ACCT,true);
                            
                                public static final StatusDetailCodeType INVALID_AMT =
                                    new StatusDetailCodeType(_INVALID_AMT,true);
                            
                                public static final StatusDetailCodeType INVALID_APP_ID =
                                    new StatusDetailCodeType(_INVALID_APP_ID,true);
                            
                                public static final StatusDetailCodeType INVALID_ASSIGN_STATUS_COMBO =
                                    new StatusDetailCodeType(_INVALID_ASSIGN_STATUS_COMBO,true);
                            
                                public static final StatusDetailCodeType INVALID_AUTH_CODE =
                                    new StatusDetailCodeType(_INVALID_AUTH_CODE,true);
                            
                                public static final StatusDetailCodeType INVALID_AUTOAPPLY_VALUE =
                                    new StatusDetailCodeType(_INVALID_AUTOAPPLY_VALUE,true);
                            
                                public static final StatusDetailCodeType INVALID_AVS_ADDR =
                                    new StatusDetailCodeType(_INVALID_AVS_ADDR,true);
                            
                                public static final StatusDetailCodeType INVALID_AVS_ZIP =
                                    new StatusDetailCodeType(_INVALID_AVS_ZIP,true);
                            
                                public static final StatusDetailCodeType INVALID_BALANCE_RANGE =
                                    new StatusDetailCodeType(_INVALID_BALANCE_RANGE,true);
                            
                                public static final StatusDetailCodeType INVALID_BILLING_SCHDUL =
                                    new StatusDetailCodeType(_INVALID_BILLING_SCHDUL,true);
                            
                                public static final StatusDetailCodeType INVALID_BILLING_SCHDUL_DATE =
                                    new StatusDetailCodeType(_INVALID_BILLING_SCHDUL_DATE,true);
                            
                                public static final StatusDetailCodeType INVALID_BILLING_SCHDUL_ENTRY =
                                    new StatusDetailCodeType(_INVALID_BILLING_SCHDUL_ENTRY,true);
                            
                                public static final StatusDetailCodeType INVALID_BIN_NUM =
                                    new StatusDetailCodeType(_INVALID_BIN_NUM,true);
                            
                                public static final StatusDetailCodeType INVALID_BOM_QTY =
                                    new StatusDetailCodeType(_INVALID_BOM_QTY,true);
                            
                                public static final StatusDetailCodeType INVALID_BOOLEAN_VALUE =
                                    new StatusDetailCodeType(_INVALID_BOOLEAN_VALUE,true);
                            
                                public static final StatusDetailCodeType INVALID_BUG_NUM =
                                    new StatusDetailCodeType(_INVALID_BUG_NUM,true);
                            
                                public static final StatusDetailCodeType INVALID_CAMPAIGN_CHANNEL =
                                    new StatusDetailCodeType(_INVALID_CAMPAIGN_CHANNEL,true);
                            
                                public static final StatusDetailCodeType INVALID_CAMPAIGN_GROUP_SIZE =
                                    new StatusDetailCodeType(_INVALID_CAMPAIGN_GROUP_SIZE,true);
                            
                                public static final StatusDetailCodeType INVALID_CAMPAIGN_STATUS =
                                    new StatusDetailCodeType(_INVALID_CAMPAIGN_STATUS,true);
                            
                                public static final StatusDetailCodeType INVALID_CARD =
                                    new StatusDetailCodeType(_INVALID_CARD,true);
                            
                                public static final StatusDetailCodeType INVALID_CARD_ID =
                                    new StatusDetailCodeType(_INVALID_CARD_ID,true);
                            
                                public static final StatusDetailCodeType INVALID_CARD_NUM =
                                    new StatusDetailCodeType(_INVALID_CARD_NUM,true);
                            
                                public static final StatusDetailCodeType INVALID_CARD_TYP =
                                    new StatusDetailCodeType(_INVALID_CARD_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_CASE_FORM =
                                    new StatusDetailCodeType(_INVALID_CASE_FORM,true);
                            
                                public static final StatusDetailCodeType INVALID_CATGRY_TAX_AGENCY_REQ =
                                    new StatusDetailCodeType(_INVALID_CATGRY_TAX_AGENCY_REQ,true);
                            
                                public static final StatusDetailCodeType INVALID_CC_EMAIL_ADDRESS =
                                    new StatusDetailCodeType(_INVALID_CC_EMAIL_ADDRESS,true);
                            
                                public static final StatusDetailCodeType INVALID_CC_NUM =
                                    new StatusDetailCodeType(_INVALID_CC_NUM,true);
                            
                                public static final StatusDetailCodeType INVALID_CERT =
                                    new StatusDetailCodeType(_INVALID_CERT,true);
                            
                                public static final StatusDetailCodeType INVALID_CERT_AUTH =
                                    new StatusDetailCodeType(_INVALID_CERT_AUTH,true);
                            
                                public static final StatusDetailCodeType INVALID_CHANGE_LIST =
                                    new StatusDetailCodeType(_INVALID_CHANGE_LIST,true);
                            
                                public static final StatusDetailCodeType INVALID_CHARS_IN_EMAIL =
                                    new StatusDetailCodeType(_INVALID_CHARS_IN_EMAIL,true);
                            
                                public static final StatusDetailCodeType INVALID_CHARS_IN_NAME =
                                    new StatusDetailCodeType(_INVALID_CHARS_IN_NAME,true);
                            
                                public static final StatusDetailCodeType INVALID_CHARS_IN_PARAM_FIELD =
                                    new StatusDetailCodeType(_INVALID_CHARS_IN_PARAM_FIELD,true);
                            
                                public static final StatusDetailCodeType INVALID_CHARS_IN_URL =
                                    new StatusDetailCodeType(_INVALID_CHARS_IN_URL,true);
                            
                                public static final StatusDetailCodeType INVALID_CHECKOUT_EMAIL =
                                    new StatusDetailCodeType(_INVALID_CHECKOUT_EMAIL,true);
                            
                                public static final StatusDetailCodeType INVALID_CITY =
                                    new StatusDetailCodeType(_INVALID_CITY,true);
                            
                                public static final StatusDetailCodeType INVALID_COLUMN_NAME =
                                    new StatusDetailCodeType(_INVALID_COLUMN_NAME,true);
                            
                                public static final StatusDetailCodeType INVALID_COLUMN_VALUE =
                                    new StatusDetailCodeType(_INVALID_COLUMN_VALUE,true);
                            
                                public static final StatusDetailCodeType INVALID_CONTENT_TYPE =
                                    new StatusDetailCodeType(_INVALID_CONTENT_TYPE,true);
                            
                                public static final StatusDetailCodeType INVALID_COSTING_METHOD =
                                    new StatusDetailCodeType(_INVALID_COSTING_METHOD,true);
                            
                                public static final StatusDetailCodeType INVALID_CRNCY_EXCH_RATE =
                                    new StatusDetailCodeType(_INVALID_CRNCY_EXCH_RATE,true);
                            
                                public static final StatusDetailCodeType INVALID_CRYPT_KEY =
                                    new StatusDetailCodeType(_INVALID_CRYPT_KEY,true);
                            
                                public static final StatusDetailCodeType INVALID_CSTM_FIELD_DATA_TYP =
                                    new StatusDetailCodeType(_INVALID_CSTM_FIELD_DATA_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_CSTM_FIELD_RCRD_TYP =
                                    new StatusDetailCodeType(_INVALID_CSTM_FIELD_RCRD_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_CSTM_FIELD_REF =
                                    new StatusDetailCodeType(_INVALID_CSTM_FIELD_REF,true);
                            
                                public static final StatusDetailCodeType INVALID_CSTM_FORM =
                                    new StatusDetailCodeType(_INVALID_CSTM_FORM,true);
                            
                                public static final StatusDetailCodeType INVALID_CSTM_RCRD_KEY =
                                    new StatusDetailCodeType(_INVALID_CSTM_RCRD_KEY,true);
                            
                                public static final StatusDetailCodeType INVALID_CSTM_RCRD_QUERY =
                                    new StatusDetailCodeType(_INVALID_CSTM_RCRD_QUERY,true);
                            
                                public static final StatusDetailCodeType INVALID_CSTM_RCRD_TYPE_KEY =
                                    new StatusDetailCodeType(_INVALID_CSTM_RCRD_TYPE_KEY,true);
                            
                                public static final StatusDetailCodeType INVALID_CSTM_RCRD_TYP_KEY =
                                    new StatusDetailCodeType(_INVALID_CSTM_RCRD_TYP_KEY,true);
                            
                                public static final StatusDetailCodeType INVALID_CUR =
                                    new StatusDetailCodeType(_INVALID_CUR,true);
                            
                                public static final StatusDetailCodeType INVALID_CURRENCY_CODE =
                                    new StatusDetailCodeType(_INVALID_CURRENCY_CODE,true);
                            
                                public static final StatusDetailCodeType INVALID_CURRENCY_TYP =
                                    new StatusDetailCodeType(_INVALID_CURRENCY_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_CURR_CODE =
                                    new StatusDetailCodeType(_INVALID_CURR_CODE,true);
                            
                                public static final StatusDetailCodeType INVALID_CUSTOMER_RCRD =
                                    new StatusDetailCodeType(_INVALID_CUSTOMER_RCRD,true);
                            
                                public static final StatusDetailCodeType INVALID_DATA =
                                    new StatusDetailCodeType(_INVALID_DATA,true);
                            
                                public static final StatusDetailCodeType INVALID_DATA_FORMAT =
                                    new StatusDetailCodeType(_INVALID_DATA_FORMAT,true);
                            
                                public static final StatusDetailCodeType INVALID_DATE =
                                    new StatusDetailCodeType(_INVALID_DATE,true);
                            
                                public static final StatusDetailCodeType INVALID_DATE_FORMAT =
                                    new StatusDetailCodeType(_INVALID_DATE_FORMAT,true);
                            
                                public static final StatusDetailCodeType INVALID_DATE_RANGE =
                                    new StatusDetailCodeType(_INVALID_DATE_RANGE,true);
                            
                                public static final StatusDetailCodeType INVALID_DATE_TIME =
                                    new StatusDetailCodeType(_INVALID_DATE_TIME,true);
                            
                                public static final StatusDetailCodeType INVALID_DEAL_RANGE =
                                    new StatusDetailCodeType(_INVALID_DEAL_RANGE,true);
                            
                                public static final StatusDetailCodeType INVALID_DELETE_REF =
                                    new StatusDetailCodeType(_INVALID_DELETE_REF,true);
                            
                                public static final StatusDetailCodeType INVALID_DESTINATION_FLDR =
                                    new StatusDetailCodeType(_INVALID_DESTINATION_FLDR,true);
                            
                                public static final StatusDetailCodeType INVALID_DESTNTN_COUNTRY =
                                    new StatusDetailCodeType(_INVALID_DESTNTN_COUNTRY,true);
                            
                                public static final StatusDetailCodeType INVALID_DESTNTN_POST_CODE =
                                    new StatusDetailCodeType(_INVALID_DESTNTN_POST_CODE,true);
                            
                                public static final StatusDetailCodeType INVALID_DESTNTN_STATE =
                                    new StatusDetailCodeType(_INVALID_DESTNTN_STATE,true);
                            
                                public static final StatusDetailCodeType INVALID_DETACH_RECORD_TYP =
                                    new StatusDetailCodeType(_INVALID_DETACH_RECORD_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_DISCOUNT_MARKUP =
                                    new StatusDetailCodeType(_INVALID_DISCOUNT_MARKUP,true);
                            
                                public static final StatusDetailCodeType INVALID_DOMAIN_KEY =
                                    new StatusDetailCodeType(_INVALID_DOMAIN_KEY,true);
                            
                                public static final StatusDetailCodeType INVALID_DOMAIN_NAME =
                                    new StatusDetailCodeType(_INVALID_DOMAIN_NAME,true);
                            
                                public static final StatusDetailCodeType INVALID_DUP_ISSUE_REF =
                                    new StatusDetailCodeType(_INVALID_DUP_ISSUE_REF,true);
                            
                                public static final StatusDetailCodeType INVALID_EMAIL =
                                    new StatusDetailCodeType(_INVALID_EMAIL,true);
                            
                                public static final StatusDetailCodeType INVALID_EMAIL_ADDR =
                                    new StatusDetailCodeType(_INVALID_EMAIL_ADDR,true);
                            
                                public static final StatusDetailCodeType INVALID_END_DATE =
                                    new StatusDetailCodeType(_INVALID_END_DATE,true);
                            
                                public static final StatusDetailCodeType INVALID_END_TIME =
                                    new StatusDetailCodeType(_INVALID_END_TIME,true);
                            
                                public static final StatusDetailCodeType INVALID_ENTITY_INTERNALID =
                                    new StatusDetailCodeType(_INVALID_ENTITY_INTERNALID,true);
                            
                                public static final StatusDetailCodeType INVALID_ENTITY_STATUS =
                                    new StatusDetailCodeType(_INVALID_ENTITY_STATUS,true);
                            
                                public static final StatusDetailCodeType INVALID_EVENT_TIME =
                                    new StatusDetailCodeType(_INVALID_EVENT_TIME,true);
                            
                                public static final StatusDetailCodeType INVALID_EXPNS_ACCT_SUB =
                                    new StatusDetailCodeType(_INVALID_EXPNS_ACCT_SUB,true);
                            
                                public static final StatusDetailCodeType INVALID_EXPRESSION =
                                    new StatusDetailCodeType(_INVALID_EXPRESSION,true);
                            
                                public static final StatusDetailCodeType INVALID_EXP_DATE =
                                    new StatusDetailCodeType(_INVALID_EXP_DATE,true);
                            
                                public static final StatusDetailCodeType INVALID_FAX_NUM =
                                    new StatusDetailCodeType(_INVALID_FAX_NUM,true);
                            
                                public static final StatusDetailCodeType INVALID_FAX_PHONE_FORMAT =
                                    new StatusDetailCodeType(_INVALID_FAX_PHONE_FORMAT,true);
                            
                                public static final StatusDetailCodeType INVALID_FIELD_FOR_RCRD_TYP =
                                    new StatusDetailCodeType(_INVALID_FIELD_FOR_RCRD_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_FIELD_NAME_FOR_NULL =
                                    new StatusDetailCodeType(_INVALID_FIELD_NAME_FOR_NULL,true);
                            
                                public static final StatusDetailCodeType INVALID_FILE =
                                    new StatusDetailCodeType(_INVALID_FILE,true);
                            
                                public static final StatusDetailCodeType INVALID_FILE_ENCODING =
                                    new StatusDetailCodeType(_INVALID_FILE_ENCODING,true);
                            
                                public static final StatusDetailCodeType INVALID_FILE_TYP =
                                    new StatusDetailCodeType(_INVALID_FILE_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_FLD =
                                    new StatusDetailCodeType(_INVALID_FLD,true);
                            
                                public static final StatusDetailCodeType INVALID_FLDR_SIZE =
                                    new StatusDetailCodeType(_INVALID_FLDR_SIZE,true);
                            
                                public static final StatusDetailCodeType INVALID_FLD_RANGE =
                                    new StatusDetailCodeType(_INVALID_FLD_RANGE,true);
                            
                                public static final StatusDetailCodeType INVALID_FLD_TYP =
                                    new StatusDetailCodeType(_INVALID_FLD_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_FLD_VALUE =
                                    new StatusDetailCodeType(_INVALID_FLD_VALUE,true);
                            
                                public static final StatusDetailCodeType INVALID_FORMAT_IN_PARAM_FIELD =
                                    new StatusDetailCodeType(_INVALID_FORMAT_IN_PARAM_FIELD,true);
                            
                                public static final StatusDetailCodeType INVALID_FORMULA =
                                    new StatusDetailCodeType(_INVALID_FORMULA,true);
                            
                                public static final StatusDetailCodeType INVALID_FORMULA_FIELD =
                                    new StatusDetailCodeType(_INVALID_FORMULA_FIELD,true);
                            
                                public static final StatusDetailCodeType INVALID_FROM_DATE =
                                    new StatusDetailCodeType(_INVALID_FROM_DATE,true);
                            
                                public static final StatusDetailCodeType INVALID_FROM_TIME =
                                    new StatusDetailCodeType(_INVALID_FROM_TIME,true);
                            
                                public static final StatusDetailCodeType INVALID_FULFILMNT_ITEM =
                                    new StatusDetailCodeType(_INVALID_FULFILMNT_ITEM,true);
                            
                                public static final StatusDetailCodeType INVALID_FX_BASE_CURRENCY =
                                    new StatusDetailCodeType(_INVALID_FX_BASE_CURRENCY,true);
                            
                                public static final StatusDetailCodeType INVALID_FX_RATE =
                                    new StatusDetailCodeType(_INVALID_FX_RATE,true);
                            
                                public static final StatusDetailCodeType INVALID_GET_REF =
                                    new StatusDetailCodeType(_INVALID_GET_REF,true);
                            
                                public static final StatusDetailCodeType INVALID_GIFT_CERT =
                                    new StatusDetailCodeType(_INVALID_GIFT_CERT,true);
                            
                                public static final StatusDetailCodeType INVALID_GIFT_CERT_AMT =
                                    new StatusDetailCodeType(_INVALID_GIFT_CERT_AMT,true);
                            
                                public static final StatusDetailCodeType INVALID_GIFT_CERT_CODE =
                                    new StatusDetailCodeType(_INVALID_GIFT_CERT_CODE,true);
                            
                                public static final StatusDetailCodeType INVALID_GROUP_TYP =
                                    new StatusDetailCodeType(_INVALID_GROUP_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_GROUP_TYPE =
                                    new StatusDetailCodeType(_INVALID_GROUP_TYPE,true);
                            
                                public static final StatusDetailCodeType INVALID_GRP =
                                    new StatusDetailCodeType(_INVALID_GRP,true);
                            
                                public static final StatusDetailCodeType INVALID_GST_PST_AGENCIES =
                                    new StatusDetailCodeType(_INVALID_GST_PST_AGENCIES,true);
                            
                                public static final StatusDetailCodeType INVALID_ID =
                                    new StatusDetailCodeType(_INVALID_ID,true);
                            
                                public static final StatusDetailCodeType INVALID_INITIALIZE_ARG =
                                    new StatusDetailCodeType(_INVALID_INITIALIZE_ARG,true);
                            
                                public static final StatusDetailCodeType INVALID_INITIALIZE_AUXREF =
                                    new StatusDetailCodeType(_INVALID_INITIALIZE_AUXREF,true);
                            
                                public static final StatusDetailCodeType INVALID_INITIALIZE_REF =
                                    new StatusDetailCodeType(_INVALID_INITIALIZE_REF,true);
                            
                                public static final StatusDetailCodeType INVALID_INSURED_VALUE =
                                    new StatusDetailCodeType(_INVALID_INSURED_VALUE,true);
                            
                                public static final StatusDetailCodeType INVALID_INTERNALID =
                                    new StatusDetailCodeType(_INVALID_INTERNALID,true);
                            
                                public static final StatusDetailCodeType INVALID_INVENTORY_NUM =
                                    new StatusDetailCodeType(_INVALID_INVENTORY_NUM,true);
                            
                                public static final StatusDetailCodeType INVALID_INV_DATE =
                                    new StatusDetailCodeType(_INVALID_INV_DATE,true);
                            
                                public static final StatusDetailCodeType INVALID_IP_ADDRESS_RULE =
                                    new StatusDetailCodeType(_INVALID_IP_ADDRESS_RULE,true);
                            
                                public static final StatusDetailCodeType INVALID_ISSUE_BUILD_VERSION =
                                    new StatusDetailCodeType(_INVALID_ISSUE_BUILD_VERSION,true);
                            
                                public static final StatusDetailCodeType INVALID_ISSUE_PRIORITY =
                                    new StatusDetailCodeType(_INVALID_ISSUE_PRIORITY,true);
                            
                                public static final StatusDetailCodeType INVALID_ISSUE_PRODUCT =
                                    new StatusDetailCodeType(_INVALID_ISSUE_PRODUCT,true);
                            
                                public static final StatusDetailCodeType INVALID_ISSUE_STATUS =
                                    new StatusDetailCodeType(_INVALID_ISSUE_STATUS,true);
                            
                                public static final StatusDetailCodeType INVALID_ITEM_OPTION =
                                    new StatusDetailCodeType(_INVALID_ITEM_OPTION,true);
                            
                                public static final StatusDetailCodeType INVALID_ITEM_OPTIONS =
                                    new StatusDetailCodeType(_INVALID_ITEM_OPTIONS,true);
                            
                                public static final StatusDetailCodeType INVALID_ITEM_SUBTYP =
                                    new StatusDetailCodeType(_INVALID_ITEM_SUBTYP,true);
                            
                                public static final StatusDetailCodeType INVALID_ITEM_TYP =
                                    new StatusDetailCodeType(_INVALID_ITEM_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_ITEM_WEIGHT =
                                    new StatusDetailCodeType(_INVALID_ITEM_WEIGHT,true);
                            
                                public static final StatusDetailCodeType INVALID_JOB_ID =
                                    new StatusDetailCodeType(_INVALID_JOB_ID,true);
                            
                                public static final StatusDetailCodeType INVALID_KEY_OR_REF =
                                    new StatusDetailCodeType(_INVALID_KEY_OR_REF,true);
                            
                                public static final StatusDetailCodeType INVALID_KEY_PASSWORD_REQD =
                                    new StatusDetailCodeType(_INVALID_KEY_PASSWORD_REQD,true);
                            
                                public static final StatusDetailCodeType INVALID_LINE_ID =
                                    new StatusDetailCodeType(_INVALID_LINE_ID,true);
                            
                                public static final StatusDetailCodeType INVALID_LINK_SUM =
                                    new StatusDetailCodeType(_INVALID_LINK_SUM,true);
                            
                                public static final StatusDetailCodeType INVALID_LIST_ID =
                                    new StatusDetailCodeType(_INVALID_LIST_ID,true);
                            
                                public static final StatusDetailCodeType INVALID_LIST_KEY =
                                    new StatusDetailCodeType(_INVALID_LIST_KEY,true);
                            
                                public static final StatusDetailCodeType INVALID_LOC =
                                    new StatusDetailCodeType(_INVALID_LOC,true);
                            
                                public static final StatusDetailCodeType INVALID_LOC_SUB_RESTRICTN =
                                    new StatusDetailCodeType(_INVALID_LOC_SUB_RESTRICTN,true);
                            
                                public static final StatusDetailCodeType INVALID_LOGIN =
                                    new StatusDetailCodeType(_INVALID_LOGIN,true);
                            
                                public static final StatusDetailCodeType INVALID_LOGIN_ATTEMPT =
                                    new StatusDetailCodeType(_INVALID_LOGIN_ATTEMPT,true);
                            
                                public static final StatusDetailCodeType INVALID_LOGIN_CREDENTIALS =
                                    new StatusDetailCodeType(_INVALID_LOGIN_CREDENTIALS,true);
                            
                                public static final StatusDetailCodeType INVALID_LOGIN_IP =
                                    new StatusDetailCodeType(_INVALID_LOGIN_IP,true);
                            
                                public static final StatusDetailCodeType INVALID_LOT_NUM_FORMAT =
                                    new StatusDetailCodeType(_INVALID_LOT_NUM_FORMAT,true);
                            
                                public static final StatusDetailCodeType INVALID_MACRO_ID =
                                    new StatusDetailCodeType(_INVALID_MACRO_ID,true);
                            
                                public static final StatusDetailCodeType INVALID_MARKUP_DISCOUNT =
                                    new StatusDetailCodeType(_INVALID_MARKUP_DISCOUNT,true);
                            
                                public static final StatusDetailCodeType INVALID_MCC =
                                    new StatusDetailCodeType(_INVALID_MCC,true);
                            
                                public static final StatusDetailCodeType INVALID_MEMBER_HIERARCHY =
                                    new StatusDetailCodeType(_INVALID_MEMBER_HIERARCHY,true);
                            
                                public static final StatusDetailCodeType INVALID_MEMRZD_TRANS =
                                    new StatusDetailCodeType(_INVALID_MEMRZD_TRANS,true);
                            
                                public static final StatusDetailCodeType INVALID_MERCHANT_KEY =
                                    new StatusDetailCodeType(_INVALID_MERCHANT_KEY,true);
                            
                                public static final StatusDetailCodeType INVALID_MERCHANT_NAME =
                                    new StatusDetailCodeType(_INVALID_MERCHANT_NAME,true);
                            
                                public static final StatusDetailCodeType INVALID_NAME =
                                    new StatusDetailCodeType(_INVALID_NAME,true);
                            
                                public static final StatusDetailCodeType INVALID_NEXUS =
                                    new StatusDetailCodeType(_INVALID_NEXUS,true);
                            
                                public static final StatusDetailCodeType INVALID_NUM =
                                    new StatusDetailCodeType(_INVALID_NUM,true);
                            
                                public static final StatusDetailCodeType INVALID_NUMBER =
                                    new StatusDetailCodeType(_INVALID_NUMBER,true);
                            
                                public static final StatusDetailCodeType INVALID_OBJ =
                                    new StatusDetailCodeType(_INVALID_OBJ,true);
                            
                                public static final StatusDetailCodeType INVALID_ONLINE_FORM =
                                    new StatusDetailCodeType(_INVALID_ONLINE_FORM,true);
                            
                                public static final StatusDetailCodeType INVALID_ONLINE_FORM_URL =
                                    new StatusDetailCodeType(_INVALID_ONLINE_FORM_URL,true);
                            
                                public static final StatusDetailCodeType INVALID_OPENID_DOMAIN =
                                    new StatusDetailCodeType(_INVALID_OPENID_DOMAIN,true);
                            
                                public static final StatusDetailCodeType INVALID_OPERATION =
                                    new StatusDetailCodeType(_INVALID_OPERATION,true);
                            
                                public static final StatusDetailCodeType INVALID_ORD_STATUS =
                                    new StatusDetailCodeType(_INVALID_ORD_STATUS,true);
                            
                                public static final StatusDetailCodeType INVALID_ORIGIN_COUNTRY =
                                    new StatusDetailCodeType(_INVALID_ORIGIN_COUNTRY,true);
                            
                                public static final StatusDetailCodeType INVALID_ORIGIN_POSTCODE =
                                    new StatusDetailCodeType(_INVALID_ORIGIN_POSTCODE,true);
                            
                                public static final StatusDetailCodeType INVALID_ORIGIN_STATE =
                                    new StatusDetailCodeType(_INVALID_ORIGIN_STATE,true);
                            
                                public static final StatusDetailCodeType INVALID_PAGER_NUM =
                                    new StatusDetailCodeType(_INVALID_PAGER_NUM,true);
                            
                                public static final StatusDetailCodeType INVALID_PAGE_INDEX =
                                    new StatusDetailCodeType(_INVALID_PAGE_INDEX,true);
                            
                                public static final StatusDetailCodeType INVALID_PAGE_PARAM =
                                    new StatusDetailCodeType(_INVALID_PAGE_PARAM,true);
                            
                                public static final StatusDetailCodeType INVALID_PARAM =
                                    new StatusDetailCodeType(_INVALID_PARAM,true);
                            
                                public static final StatusDetailCodeType INVALID_PARENT =
                                    new StatusDetailCodeType(_INVALID_PARENT,true);
                            
                                public static final StatusDetailCodeType INVALID_PARTNER_CODE =
                                    new StatusDetailCodeType(_INVALID_PARTNER_CODE,true);
                            
                                public static final StatusDetailCodeType INVALID_PARTNER_ID =
                                    new StatusDetailCodeType(_INVALID_PARTNER_ID,true);
                            
                                public static final StatusDetailCodeType INVALID_PASSWORD =
                                    new StatusDetailCodeType(_INVALID_PASSWORD,true);
                            
                                public static final StatusDetailCodeType INVALID_PAYCHECK_DATE =
                                    new StatusDetailCodeType(_INVALID_PAYCHECK_DATE,true);
                            
                                public static final StatusDetailCodeType INVALID_PERIOD =
                                    new StatusDetailCodeType(_INVALID_PERIOD,true);
                            
                                public static final StatusDetailCodeType INVALID_PHONE =
                                    new StatusDetailCodeType(_INVALID_PHONE,true);
                            
                                public static final StatusDetailCodeType INVALID_PHONE_FAX_PAGER_NUM =
                                    new StatusDetailCodeType(_INVALID_PHONE_FAX_PAGER_NUM,true);
                            
                                public static final StatusDetailCodeType INVALID_PHONE_NUM =
                                    new StatusDetailCodeType(_INVALID_PHONE_NUM,true);
                            
                                public static final StatusDetailCodeType INVALID_PHONE_NUMBER =
                                    new StatusDetailCodeType(_INVALID_PHONE_NUMBER,true);
                            
                                public static final StatusDetailCodeType INVALID_PICKUP_POSTAL_CODE =
                                    new StatusDetailCodeType(_INVALID_PICKUP_POSTAL_CODE,true);
                            
                                public static final StatusDetailCodeType INVALID_PIN =
                                    new StatusDetailCodeType(_INVALID_PIN,true);
                            
                                public static final StatusDetailCodeType INVALID_PIN_DEBIT_TRANS_TYP =
                                    new StatusDetailCodeType(_INVALID_PIN_DEBIT_TRANS_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_PORTLET_TYP =
                                    new StatusDetailCodeType(_INVALID_PORTLET_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_POST =
                                    new StatusDetailCodeType(_INVALID_POST,true);
                            
                                public static final StatusDetailCodeType INVALID_PRESENTATION_TYP =
                                    new StatusDetailCodeType(_INVALID_PRESENTATION_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_PROBABILITY_RANGE =
                                    new StatusDetailCodeType(_INVALID_PROBABILITY_RANGE,true);
                            
                                public static final StatusDetailCodeType INVALID_PROFILE_ID =
                                    new StatusDetailCodeType(_INVALID_PROFILE_ID,true);
                            
                                public static final StatusDetailCodeType INVALID_PROJ_BILLING_TYP =
                                    new StatusDetailCodeType(_INVALID_PROJ_BILLING_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_PST_TAX_VALUE =
                                    new StatusDetailCodeType(_INVALID_PST_TAX_VALUE,true);
                            
                                public static final StatusDetailCodeType INVALID_PSWD =
                                    new StatusDetailCodeType(_INVALID_PSWD,true);
                            
                                public static final StatusDetailCodeType INVALID_PSWD_HINT =
                                    new StatusDetailCodeType(_INVALID_PSWD_HINT,true);
                            
                                public static final StatusDetailCodeType INVALID_PSWD_ILLEGAL_CHAR =
                                    new StatusDetailCodeType(_INVALID_PSWD_ILLEGAL_CHAR,true);
                            
                                public static final StatusDetailCodeType INVALID_PURCHASE_TAX_CODE =
                                    new StatusDetailCodeType(_INVALID_PURCHASE_TAX_CODE,true);
                            
                                public static final StatusDetailCodeType INVALID_QTY =
                                    new StatusDetailCodeType(_INVALID_QTY,true);
                            
                                public static final StatusDetailCodeType INVALID_QUANTITY =
                                    new StatusDetailCodeType(_INVALID_QUANTITY,true);
                            
                                public static final StatusDetailCodeType INVALID_QUESTION =
                                    new StatusDetailCodeType(_INVALID_QUESTION,true);
                            
                                public static final StatusDetailCodeType INVALID_RCRD =
                                    new StatusDetailCodeType(_INVALID_RCRD,true);
                            
                                public static final StatusDetailCodeType INVALID_RCRD_CONVERSION =
                                    new StatusDetailCodeType(_INVALID_RCRD_CONVERSION,true);
                            
                                public static final StatusDetailCodeType INVALID_RCRD_HEADER_ =
                                    new StatusDetailCodeType(_INVALID_RCRD_HEADER_,true);
                            
                                public static final StatusDetailCodeType INVALID_RCRD_ID =
                                    new StatusDetailCodeType(_INVALID_RCRD_ID,true);
                            
                                public static final StatusDetailCodeType INVALID_RCRD_INITIALIZE =
                                    new StatusDetailCodeType(_INVALID_RCRD_INITIALIZE,true);
                            
                                public static final StatusDetailCodeType INVALID_RCRD_OBJ =
                                    new StatusDetailCodeType(_INVALID_RCRD_OBJ,true);
                            
                                public static final StatusDetailCodeType INVALID_RCRD_REF =
                                    new StatusDetailCodeType(_INVALID_RCRD_REF,true);
                            
                                public static final StatusDetailCodeType INVALID_RCRD_TRANSFRM =
                                    new StatusDetailCodeType(_INVALID_RCRD_TRANSFRM,true);
                            
                                public static final StatusDetailCodeType INVALID_RCRD_TYPE =
                                    new StatusDetailCodeType(_INVALID_RCRD_TYPE,true);
                            
                                public static final StatusDetailCodeType INVALID_RECIPIENT =
                                    new StatusDetailCodeType(_INVALID_RECIPIENT,true);
                            
                                public static final StatusDetailCodeType INVALID_RECR_REF =
                                    new StatusDetailCodeType(_INVALID_RECR_REF,true);
                            
                                public static final StatusDetailCodeType INVALID_RECUR_DATE_RANGE =
                                    new StatusDetailCodeType(_INVALID_RECUR_DATE_RANGE,true);
                            
                                public static final StatusDetailCodeType INVALID_RECUR_DESC_REQD =
                                    new StatusDetailCodeType(_INVALID_RECUR_DESC_REQD,true);
                            
                                public static final StatusDetailCodeType INVALID_RECUR_DOW =
                                    new StatusDetailCodeType(_INVALID_RECUR_DOW,true);
                            
                                public static final StatusDetailCodeType INVALID_RECUR_DOWIM =
                                    new StatusDetailCodeType(_INVALID_RECUR_DOWIM,true);
                            
                                public static final StatusDetailCodeType INVALID_RECUR_DOWMASK =
                                    new StatusDetailCodeType(_INVALID_RECUR_DOWMASK,true);
                            
                                public static final StatusDetailCodeType INVALID_RECUR_FREQUENCY =
                                    new StatusDetailCodeType(_INVALID_RECUR_FREQUENCY,true);
                            
                                public static final StatusDetailCodeType INVALID_RECUR_PATTERN =
                                    new StatusDetailCodeType(_INVALID_RECUR_PATTERN,true);
                            
                                public static final StatusDetailCodeType INVALID_RECUR_PERIOD =
                                    new StatusDetailCodeType(_INVALID_RECUR_PERIOD,true);
                            
                                public static final StatusDetailCodeType INVALID_RECUR_TIME_ZONE_REQD =
                                    new StatusDetailCodeType(_INVALID_RECUR_TIME_ZONE_REQD,true);
                            
                                public static final StatusDetailCodeType INVALID_REFFERER_EMAIL =
                                    new StatusDetailCodeType(_INVALID_REFFERER_EMAIL,true);
                            
                                public static final StatusDetailCodeType INVALID_REFUND_AMT =
                                    new StatusDetailCodeType(_INVALID_REFUND_AMT,true);
                            
                                public static final StatusDetailCodeType INVALID_REF_CANT_INITIALIZE =
                                    new StatusDetailCodeType(_INVALID_REF_CANT_INITIALIZE,true);
                            
                                public static final StatusDetailCodeType INVALID_REF_KEY =
                                    new StatusDetailCodeType(_INVALID_REF_KEY,true);
                            
                                public static final StatusDetailCodeType INVALID_REPORT =
                                    new StatusDetailCodeType(_INVALID_REPORT,true);
                            
                                public static final StatusDetailCodeType INVALID_REPORT_ID =
                                    new StatusDetailCodeType(_INVALID_REPORT_ID,true);
                            
                                public static final StatusDetailCodeType INVALID_REPORT_ROW =
                                    new StatusDetailCodeType(_INVALID_REPORT_ROW,true);
                            
                                public static final StatusDetailCodeType INVALID_REQUEST =
                                    new StatusDetailCodeType(_INVALID_REQUEST,true);
                            
                                public static final StatusDetailCodeType INVALID_RESOURCE_TIME =
                                    new StatusDetailCodeType(_INVALID_RESOURCE_TIME,true);
                            
                                public static final StatusDetailCodeType INVALID_RESULT_SUMMARY_FUNC =
                                    new StatusDetailCodeType(_INVALID_RESULT_SUMMARY_FUNC,true);
                            
                                public static final StatusDetailCodeType INVALID_RETURN_DATA_OBJECT =
                                    new StatusDetailCodeType(_INVALID_RETURN_DATA_OBJECT,true);
                            
                                public static final StatusDetailCodeType INVALID_REV_REC_DATE_RANGE =
                                    new StatusDetailCodeType(_INVALID_REV_REC_DATE_RANGE,true);
                            
                                public static final StatusDetailCodeType INVALID_ROLE =
                                    new StatusDetailCodeType(_INVALID_ROLE,true);
                            
                                public static final StatusDetailCodeType INVALID_ROLE_FOR_EVENT =
                                    new StatusDetailCodeType(_INVALID_ROLE_FOR_EVENT,true);
                            
                                public static final StatusDetailCodeType INVALID_RQST_CONTACTS_EXIST =
                                    new StatusDetailCodeType(_INVALID_RQST_CONTACTS_EXIST,true);
                            
                                public static final StatusDetailCodeType INVALID_RQST_PARENT_REQD =
                                    new StatusDetailCodeType(_INVALID_RQST_PARENT_REQD,true);
                            
                                public static final StatusDetailCodeType INVALID_RQST_SBCUST_JOBS_EXIST =
                                    new StatusDetailCodeType(_INVALID_RQST_SBCUST_JOBS_EXIST,true);
                            
                                public static final StatusDetailCodeType INVALID_SAVEDSEARCH =
                                    new StatusDetailCodeType(_INVALID_SAVEDSEARCH,true);
                            
                                public static final StatusDetailCodeType INVALID_SAVED_SRCH =
                                    new StatusDetailCodeType(_INVALID_SAVED_SRCH,true);
                            
                                public static final StatusDetailCodeType INVALID_SCHDUL_AMT =
                                    new StatusDetailCodeType(_INVALID_SCHDUL_AMT,true);
                            
                                public static final StatusDetailCodeType INVALID_SCHDUL_FORMAT =
                                    new StatusDetailCodeType(_INVALID_SCHDUL_FORMAT,true);
                            
                                public static final StatusDetailCodeType INVALID_SCRIPT_ID =
                                    new StatusDetailCodeType(_INVALID_SCRIPT_ID,true);
                            
                                public static final StatusDetailCodeType INVALID_SEARCH =
                                    new StatusDetailCodeType(_INVALID_SEARCH,true);
                            
                                public static final StatusDetailCodeType INVALID_SEARCH_CRITERIA =
                                    new StatusDetailCodeType(_INVALID_SEARCH_CRITERIA,true);
                            
                                public static final StatusDetailCodeType INVALID_SEARCH_FIELD_KEY =
                                    new StatusDetailCodeType(_INVALID_SEARCH_FIELD_KEY,true);
                            
                                public static final StatusDetailCodeType INVALID_SEARCH_FIELD_NAME =
                                    new StatusDetailCodeType(_INVALID_SEARCH_FIELD_NAME,true);
                            
                                public static final StatusDetailCodeType INVALID_SEARCH_FIELD_OBJ =
                                    new StatusDetailCodeType(_INVALID_SEARCH_FIELD_OBJ,true);
                            
                                public static final StatusDetailCodeType INVALID_SEARCH_JOIN_ID =
                                    new StatusDetailCodeType(_INVALID_SEARCH_JOIN_ID,true);
                            
                                public static final StatusDetailCodeType INVALID_SEARCH_MORE =
                                    new StatusDetailCodeType(_INVALID_SEARCH_MORE,true);
                            
                                public static final StatusDetailCodeType INVALID_SEARCH_OPERATOR =
                                    new StatusDetailCodeType(_INVALID_SEARCH_OPERATOR,true);
                            
                                public static final StatusDetailCodeType INVALID_SEARCH_PAGE_INDEX =
                                    new StatusDetailCodeType(_INVALID_SEARCH_PAGE_INDEX,true);
                            
                                public static final StatusDetailCodeType INVALID_SEARCH_PAGE_SIZE =
                                    new StatusDetailCodeType(_INVALID_SEARCH_PAGE_SIZE,true);
                            
                                public static final StatusDetailCodeType INVALID_SEARCH_PREF =
                                    new StatusDetailCodeType(_INVALID_SEARCH_PREF,true);
                            
                                public static final StatusDetailCodeType INVALID_SEARCH_SELECT_OBJ =
                                    new StatusDetailCodeType(_INVALID_SEARCH_SELECT_OBJ,true);
                            
                                public static final StatusDetailCodeType INVALID_SEARCH_TYPE =
                                    new StatusDetailCodeType(_INVALID_SEARCH_TYPE,true);
                            
                                public static final StatusDetailCodeType INVALID_SEARCH_VALUE =
                                    new StatusDetailCodeType(_INVALID_SEARCH_VALUE,true);
                            
                                public static final StatusDetailCodeType INVALID_SECONDARY_EMAIL =
                                    new StatusDetailCodeType(_INVALID_SECONDARY_EMAIL,true);
                            
                                public static final StatusDetailCodeType INVALID_SECPAY_CREDENTIALS =
                                    new StatusDetailCodeType(_INVALID_SECPAY_CREDENTIALS,true);
                            
                                public static final StatusDetailCodeType INVALID_SERIAL_NUM =
                                    new StatusDetailCodeType(_INVALID_SERIAL_NUM,true);
                            
                                public static final StatusDetailCodeType INVALID_SERIAL_OR_LOT_NUMBER =
                                    new StatusDetailCodeType(_INVALID_SERIAL_OR_LOT_NUMBER,true);
                            
                                public static final StatusDetailCodeType INVALID_SERVICE_CODE =
                                    new StatusDetailCodeType(_INVALID_SERVICE_CODE,true);
                            
                                public static final StatusDetailCodeType INVALID_SESSION =
                                    new StatusDetailCodeType(_INVALID_SESSION,true);
                            
                                public static final StatusDetailCodeType INVALID_SHIPPER_STATE =
                                    new StatusDetailCodeType(_INVALID_SHIPPER_STATE,true);
                            
                                public static final StatusDetailCodeType INVALID_SHIP_DATE =
                                    new StatusDetailCodeType(_INVALID_SHIP_DATE,true);
                            
                                public static final StatusDetailCodeType INVALID_SHIP_FROM_STATE =
                                    new StatusDetailCodeType(_INVALID_SHIP_FROM_STATE,true);
                            
                                public static final StatusDetailCodeType INVALID_SHIP_GRP =
                                    new StatusDetailCodeType(_INVALID_SHIP_GRP,true);
                            
                                public static final StatusDetailCodeType INVALID_SHIP_SRVC =
                                    new StatusDetailCodeType(_INVALID_SHIP_SRVC,true);
                            
                                public static final StatusDetailCodeType INVALID_SHIP_TO_SATE =
                                    new StatusDetailCodeType(_INVALID_SHIP_TO_SATE,true);
                            
                                public static final StatusDetailCodeType INVALID_SITE_CSTM_FILE =
                                    new StatusDetailCodeType(_INVALID_SITE_CSTM_FILE,true);
                            
                                public static final StatusDetailCodeType INVALID_SOAP_HEADER =
                                    new StatusDetailCodeType(_INVALID_SOAP_HEADER,true);
                            
                                public static final StatusDetailCodeType INVALID_SRCH =
                                    new StatusDetailCodeType(_INVALID_SRCH,true);
                            
                                public static final StatusDetailCodeType INVALID_SRCH_CRITERIA =
                                    new StatusDetailCodeType(_INVALID_SRCH_CRITERIA,true);
                            
                                public static final StatusDetailCodeType INVALID_SRCH_CSTM_FLD =
                                    new StatusDetailCodeType(_INVALID_SRCH_CSTM_FLD,true);
                            
                                public static final StatusDetailCodeType INVALID_SRCH_FUNCTN =
                                    new StatusDetailCodeType(_INVALID_SRCH_FUNCTN,true);
                            
                                public static final StatusDetailCodeType INVALID_SRCH_SORT =
                                    new StatusDetailCodeType(_INVALID_SRCH_SORT,true);
                            
                                public static final StatusDetailCodeType INVALID_SRCH_SUMMARY_TYP =
                                    new StatusDetailCodeType(_INVALID_SRCH_SUMMARY_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_SRCH_TYP =
                                    new StatusDetailCodeType(_INVALID_SRCH_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_SRVC_ITEM_SUB =
                                    new StatusDetailCodeType(_INVALID_SRVC_ITEM_SUB,true);
                            
                                public static final StatusDetailCodeType INVALID_SSO =
                                    new StatusDetailCodeType(_INVALID_SSO,true);
                            
                                public static final StatusDetailCodeType INVALID_SSS_DEBUG_SESSION =
                                    new StatusDetailCodeType(_INVALID_SSS_DEBUG_SESSION,true);
                            
                                public static final StatusDetailCodeType INVALID_STATE =
                                    new StatusDetailCodeType(_INVALID_STATE,true);
                            
                                public static final StatusDetailCodeType INVALID_STATUS =
                                    new StatusDetailCodeType(_INVALID_STATUS,true);
                            
                                public static final StatusDetailCodeType INVALID_SUB =
                                    new StatusDetailCodeType(_INVALID_SUB,true);
                            
                                public static final StatusDetailCodeType INVALID_SUBLIST_DESC =
                                    new StatusDetailCodeType(_INVALID_SUBLIST_DESC,true);
                            
                                public static final StatusDetailCodeType INVALID_SUBSCRIPTION_STATUS =
                                    new StatusDetailCodeType(_INVALID_SUBSCRIPTION_STATUS,true);
                            
                                public static final StatusDetailCodeType INVALID_SUMMARY_SRCH =
                                    new StatusDetailCodeType(_INVALID_SUMMARY_SRCH,true);
                            
                                public static final StatusDetailCodeType INVALID_SUPERVISOR =
                                    new StatusDetailCodeType(_INVALID_SUPERVISOR,true);
                            
                                public static final StatusDetailCodeType INVALID_TASK_ID =
                                    new StatusDetailCodeType(_INVALID_TASK_ID,true);
                            
                                public static final StatusDetailCodeType INVALID_TAX_AMT =
                                    new StatusDetailCodeType(_INVALID_TAX_AMT,true);
                            
                                public static final StatusDetailCodeType INVALID_TAX_CODE =
                                    new StatusDetailCodeType(_INVALID_TAX_CODE,true);
                            
                                public static final StatusDetailCodeType INVALID_TAX_CODES =
                                    new StatusDetailCodeType(_INVALID_TAX_CODES,true);
                            
                                public static final StatusDetailCodeType INVALID_TAX_CODE_FOR_SUB =
                                    new StatusDetailCodeType(_INVALID_TAX_CODE_FOR_SUB,true);
                            
                                public static final StatusDetailCodeType INVALID_TAX_PMT =
                                    new StatusDetailCodeType(_INVALID_TAX_PMT,true);
                            
                                public static final StatusDetailCodeType INVALID_TAX_VALUE =
                                    new StatusDetailCodeType(_INVALID_TAX_VALUE,true);
                            
                                public static final StatusDetailCodeType INVALID_TIME_FORMAT =
                                    new StatusDetailCodeType(_INVALID_TIME_FORMAT,true);
                            
                                public static final StatusDetailCodeType INVALID_TO_DATE =
                                    new StatusDetailCodeType(_INVALID_TO_DATE,true);
                            
                                public static final StatusDetailCodeType INVALID_TRACKING_NUM =
                                    new StatusDetailCodeType(_INVALID_TRACKING_NUM,true);
                            
                                public static final StatusDetailCodeType INVALID_TRANS =
                                    new StatusDetailCodeType(_INVALID_TRANS,true);
                            
                                public static final StatusDetailCodeType INVALID_TRANSACTION_DATE =
                                    new StatusDetailCodeType(_INVALID_TRANSACTION_DATE,true);
                            
                                public static final StatusDetailCodeType INVALID_TRANSACTIO_DATE =
                                    new StatusDetailCodeType(_INVALID_TRANSACTIO_DATE,true);
                            
                                public static final StatusDetailCodeType INVALID_TRANS_AMT =
                                    new StatusDetailCodeType(_INVALID_TRANS_AMT,true);
                            
                                public static final StatusDetailCodeType INVALID_TRANS_COMPNT =
                                    new StatusDetailCodeType(_INVALID_TRANS_COMPNT,true);
                            
                                public static final StatusDetailCodeType INVALID_TRANS_ID =
                                    new StatusDetailCodeType(_INVALID_TRANS_ID,true);
                            
                                public static final StatusDetailCodeType INVALID_TRANS_SUB_ACCT =
                                    new StatusDetailCodeType(_INVALID_TRANS_SUB_ACCT,true);
                            
                                public static final StatusDetailCodeType INVALID_TRANS_SUB_CLASS =
                                    new StatusDetailCodeType(_INVALID_TRANS_SUB_CLASS,true);
                            
                                public static final StatusDetailCodeType INVALID_TRANS_SUB_DEPT =
                                    new StatusDetailCodeType(_INVALID_TRANS_SUB_DEPT,true);
                            
                                public static final StatusDetailCodeType INVALID_TRANS_SUB_ENTITY =
                                    new StatusDetailCodeType(_INVALID_TRANS_SUB_ENTITY,true);
                            
                                public static final StatusDetailCodeType INVALID_TRANS_SUB_ITEM =
                                    new StatusDetailCodeType(_INVALID_TRANS_SUB_ITEM,true);
                            
                                public static final StatusDetailCodeType INVALID_TRANS_SUB_LOC =
                                    new StatusDetailCodeType(_INVALID_TRANS_SUB_LOC,true);
                            
                                public static final StatusDetailCodeType INVALID_TRANS_TYP =
                                    new StatusDetailCodeType(_INVALID_TRANS_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_TRAN_ITEM_LINE =
                                    new StatusDetailCodeType(_INVALID_TRAN_ITEM_LINE,true);
                            
                                public static final StatusDetailCodeType INVALID_TRIAL_TYP =
                                    new StatusDetailCodeType(_INVALID_TRIAL_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_TYP =
                                    new StatusDetailCodeType(_INVALID_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_UNIT_TYP =
                                    new StatusDetailCodeType(_INVALID_UNIT_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_UNSUPRTD_RCRD_TYP =
                                    new StatusDetailCodeType(_INVALID_UNSUPRTD_RCRD_TYP,true);
                            
                                public static final StatusDetailCodeType INVALID_UPS_ACCT =
                                    new StatusDetailCodeType(_INVALID_UPS_ACCT,true);
                            
                                public static final StatusDetailCodeType INVALID_UPS_PACKG_WEIGHT =
                                    new StatusDetailCodeType(_INVALID_UPS_PACKG_WEIGHT,true);
                            
                                public static final StatusDetailCodeType INVALID_UPS_VALUES =
                                    new StatusDetailCodeType(_INVALID_UPS_VALUES,true);
                            
                                public static final StatusDetailCodeType INVALID_URL =
                                    new StatusDetailCodeType(_INVALID_URL,true);
                            
                                public static final StatusDetailCodeType INVALID_URL_PARAM =
                                    new StatusDetailCodeType(_INVALID_URL_PARAM,true);
                            
                                public static final StatusDetailCodeType INVALID_VALUE =
                                    new StatusDetailCodeType(_INVALID_VALUE,true);
                            
                                public static final StatusDetailCodeType INVALID_VAT_AMOUNT =
                                    new StatusDetailCodeType(_INVALID_VAT_AMOUNT,true);
                            
                                public static final StatusDetailCodeType INVALID_VAT_REGSTRTN_NUM =
                                    new StatusDetailCodeType(_INVALID_VAT_REGSTRTN_NUM,true);
                            
                                public static final StatusDetailCodeType INVALID_VSOE_ALLOCTN =
                                    new StatusDetailCodeType(_INVALID_VSOE_ALLOCTN,true);
                            
                                public static final StatusDetailCodeType INVALID_WEBSITE_SECTION =
                                    new StatusDetailCodeType(_INVALID_WEBSITE_SECTION,true);
                            
                                public static final StatusDetailCodeType INVALID_WO =
                                    new StatusDetailCodeType(_INVALID_WO,true);
                            
                                public static final StatusDetailCodeType INVALID_WORLDPAY_ID =
                                    new StatusDetailCodeType(_INVALID_WORLDPAY_ID,true);
                            
                                public static final StatusDetailCodeType INVALID_WO_ITEM =
                                    new StatusDetailCodeType(_INVALID_WO_ITEM,true);
                            
                                public static final StatusDetailCodeType INVALID_WS_VERSION =
                                    new StatusDetailCodeType(_INVALID_WS_VERSION,true);
                            
                                public static final StatusDetailCodeType INVALID_YEAR =
                                    new StatusDetailCodeType(_INVALID_YEAR,true);
                            
                                public static final StatusDetailCodeType INVALID_YEAR_FORMAT =
                                    new StatusDetailCodeType(_INVALID_YEAR_FORMAT,true);
                            
                                public static final StatusDetailCodeType INVALID_ZIP_CODE =
                                    new StatusDetailCodeType(_INVALID_ZIP_CODE,true);
                            
                                public static final StatusDetailCodeType INVALID_ZIP_FILE =
                                    new StatusDetailCodeType(_INVALID_ZIP_FILE,true);
                            
                                public static final StatusDetailCodeType INVALID_ZIP_POST_CODE =
                                    new StatusDetailCodeType(_INVALID_ZIP_POST_CODE,true);
                            
                                public static final StatusDetailCodeType INVENTORY_NUM_DISALLWD =
                                    new StatusDetailCodeType(_INVENTORY_NUM_DISALLWD,true);
                            
                                public static final StatusDetailCodeType INVLAID_BOOLEAN_VALUE =
                                    new StatusDetailCodeType(_INVLAID_BOOLEAN_VALUE,true);
                            
                                public static final StatusDetailCodeType DATA_RETRIEVAL_ERROR =
                                    new StatusDetailCodeType(_DATA_RETRIEVAL_ERROR,true);
                            
                                public static final StatusDetailCodeType IP_REQUEST =
                                    new StatusDetailCodeType(_IP_REQUEST,true);
                            
                                public static final StatusDetailCodeType ISSUE_ASSIGNEE_DISALLWD =
                                    new StatusDetailCodeType(_ISSUE_ASSIGNEE_DISALLWD,true);
                            
                                public static final StatusDetailCodeType ISSUE_PRODUCT_VERSION_MISMATCH =
                                    new StatusDetailCodeType(_ISSUE_PRODUCT_VERSION_MISMATCH,true);
                            
                                public static final StatusDetailCodeType ISSUE_VERSION_BUILD_MISMATCH =
                                    new StatusDetailCodeType(_ISSUE_VERSION_BUILD_MISMATCH,true);
                            
                                public static final StatusDetailCodeType ITEM_ACCT_REQD =
                                    new StatusDetailCodeType(_ITEM_ACCT_REQD,true);
                            
                                public static final StatusDetailCodeType ITEM_COUNT_MISMATCH =
                                    new StatusDetailCodeType(_ITEM_COUNT_MISMATCH,true);
                            
                                public static final StatusDetailCodeType ITEM_IS_UNAVAILABLE =
                                    new StatusDetailCodeType(_ITEM_IS_UNAVAILABLE,true);
                            
                                public static final StatusDetailCodeType ITEM_NAME_MUST_BE_UNIQUE =
                                    new StatusDetailCodeType(_ITEM_NAME_MUST_BE_UNIQUE,true);
                            
                                public static final StatusDetailCodeType ITEM_NOT_UNIQUE =
                                    new StatusDetailCodeType(_ITEM_NOT_UNIQUE,true);
                            
                                public static final StatusDetailCodeType ITEM_PARAM_REQD_IN_URL =
                                    new StatusDetailCodeType(_ITEM_PARAM_REQD_IN_URL,true);
                            
                                public static final StatusDetailCodeType ITEM_QTY_AMT_MISMATCH =
                                    new StatusDetailCodeType(_ITEM_QTY_AMT_MISMATCH,true);
                            
                                public static final StatusDetailCodeType ITEM_TYP_REQS_UNIT =
                                    new StatusDetailCodeType(_ITEM_TYP_REQS_UNIT,true);
                            
                                public static final StatusDetailCodeType JE_AMOUNTS_MUST_BALANCE =
                                    new StatusDetailCodeType(_JE_AMOUNTS_MUST_BALANCE,true);
                            
                                public static final StatusDetailCodeType JE_LINE_MISSING_REQD_DATA =
                                    new StatusDetailCodeType(_JE_LINE_MISSING_REQD_DATA,true);
                            
                                public static final StatusDetailCodeType JE_MAX_ONE_LINE =
                                    new StatusDetailCodeType(_JE_MAX_ONE_LINE,true);
                            
                                public static final StatusDetailCodeType JE_REV_REC_IN_PROGRESS =
                                    new StatusDetailCodeType(_JE_REV_REC_IN_PROGRESS,true);
                            
                                public static final StatusDetailCodeType JE_UNEXPECTED_ERROR =
                                    new StatusDetailCodeType(_JE_UNEXPECTED_ERROR,true);
                            
                                public static final StatusDetailCodeType JOB_NOT_COMPLETE =
                                    new StatusDetailCodeType(_JOB_NOT_COMPLETE,true);
                            
                                public static final StatusDetailCodeType JS_EXCEPTION =
                                    new StatusDetailCodeType(_JS_EXCEPTION,true);
                            
                                public static final StatusDetailCodeType KEY_REQD =
                                    new StatusDetailCodeType(_KEY_REQD,true);
                            
                                public static final StatusDetailCodeType KPI_SETUP_REQD =
                                    new StatusDetailCodeType(_KPI_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType LABEL_REQD =
                                    new StatusDetailCodeType(_LABEL_REQD,true);
                            
                                public static final StatusDetailCodeType LANGUAGE_SETUP_REQD =
                                    new StatusDetailCodeType(_LANGUAGE_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType LINKED_ACCT_DONT_MATCH =
                                    new StatusDetailCodeType(_LINKED_ACCT_DONT_MATCH,true);
                            
                                public static final StatusDetailCodeType LINKED_ENTITIES_DONT_MATCH =
                                    new StatusDetailCodeType(_LINKED_ENTITIES_DONT_MATCH,true);
                            
                                public static final StatusDetailCodeType LINKED_ITEMS_DONT_MATCH =
                                    new StatusDetailCodeType(_LINKED_ITEMS_DONT_MATCH,true);
                            
                                public static final StatusDetailCodeType LINK_LINES_TO_ONE_ORD =
                                    new StatusDetailCodeType(_LINK_LINES_TO_ONE_ORD,true);
                            
                                public static final StatusDetailCodeType LIST_ID_REQD =
                                    new StatusDetailCodeType(_LIST_ID_REQD,true);
                            
                                public static final StatusDetailCodeType LIST_KEY_REQD =
                                    new StatusDetailCodeType(_LIST_KEY_REQD,true);
                            
                                public static final StatusDetailCodeType LOCATIONS_IN_USE =
                                    new StatusDetailCodeType(_LOCATIONS_IN_USE,true);
                            
                                public static final StatusDetailCodeType LOCATIONS_SETUP_REQD =
                                    new StatusDetailCodeType(_LOCATIONS_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType LOCATION_REQD =
                                    new StatusDetailCodeType(_LOCATION_REQD,true);
                            
                                public static final StatusDetailCodeType LOCKED_DASHBOARD =
                                    new StatusDetailCodeType(_LOCKED_DASHBOARD,true);
                            
                                public static final StatusDetailCodeType LOGIN_DISABLED =
                                    new StatusDetailCodeType(_LOGIN_DISABLED,true);
                            
                                public static final StatusDetailCodeType LOGIN_DISABLED_PARTNER_CTR =
                                    new StatusDetailCodeType(_LOGIN_DISABLED_PARTNER_CTR,true);
                            
                                public static final StatusDetailCodeType LOGIN_EMAIL_REQD =
                                    new StatusDetailCodeType(_LOGIN_EMAIL_REQD,true);
                            
                                public static final StatusDetailCodeType LOGIN_NAME_AND_PSWD_REQD =
                                    new StatusDetailCodeType(_LOGIN_NAME_AND_PSWD_REQD,true);
                            
                                public static final StatusDetailCodeType LOGIN_REQD =
                                    new StatusDetailCodeType(_LOGIN_REQD,true);
                            
                                public static final StatusDetailCodeType LOST_UPSELL_CRITERIA =
                                    new StatusDetailCodeType(_LOST_UPSELL_CRITERIA,true);
                            
                                public static final StatusDetailCodeType MACHN_LIST_KEY_NAMES_REQD =
                                    new StatusDetailCodeType(_MACHN_LIST_KEY_NAMES_REQD,true);
                            
                                public static final StatusDetailCodeType MANDATORY_PRD_TYPE_REQD =
                                    new StatusDetailCodeType(_MANDATORY_PRD_TYPE_REQD,true);
                            
                                public static final StatusDetailCodeType MASS_UPDATE_CRITERIA_LOST =
                                    new StatusDetailCodeType(_MASS_UPDATE_CRITERIA_LOST,true);
                            
                                public static final StatusDetailCodeType MATCHING_CUR_SUB_REQD =
                                    new StatusDetailCodeType(_MATCHING_CUR_SUB_REQD,true);
                            
                                public static final StatusDetailCodeType MATCHING_SERIAL_NUM_REQD =
                                    new StatusDetailCodeType(_MATCHING_SERIAL_NUM_REQD,true);
                            
                                public static final StatusDetailCodeType MATRIX_INFO_TEMP_LOST =
                                    new StatusDetailCodeType(_MATRIX_INFO_TEMP_LOST,true);
                            
                                public static final StatusDetailCodeType MATRIX_SUBITEM_NAME_TOO_LONG =
                                    new StatusDetailCodeType(_MATRIX_SUBITEM_NAME_TOO_LONG,true);
                            
                                public static final StatusDetailCodeType MAX_16_LINES_ALLWD_PER_BILLPAY =
                                    new StatusDetailCodeType(_MAX_16_LINES_ALLWD_PER_BILLPAY,true);
                            
                                public static final StatusDetailCodeType MAX_200_LINES_ALLWD_ON_TRANS =
                                    new StatusDetailCodeType(_MAX_200_LINES_ALLWD_ON_TRANS,true);
                            
                                public static final StatusDetailCodeType MAX_BARCODE_PRINT_EXCEEDED =
                                    new StatusDetailCodeType(_MAX_BARCODE_PRINT_EXCEEDED,true);
                            
                                public static final StatusDetailCodeType MAX_BULK_MERGE_RCRDS_EXCEEDED =
                                    new StatusDetailCodeType(_MAX_BULK_MERGE_RCRDS_EXCEEDED,true);
                            
                                public static final StatusDetailCodeType MAX_EMAILS_EXCEEDED =
                                    new StatusDetailCodeType(_MAX_EMAILS_EXCEEDED,true);
                            
                                public static final StatusDetailCodeType MAX_RCRDS_EXCEEDED =
                                    new StatusDetailCodeType(_MAX_RCRDS_EXCEEDED,true);
                            
                                public static final StatusDetailCodeType MAX_VALUES_EXCEEDED =
                                    new StatusDetailCodeType(_MAX_VALUES_EXCEEDED,true);
                            
                                public static final StatusDetailCodeType MEDIA_FILE_INVALID_JSCRIPT =
                                    new StatusDetailCodeType(_MEDIA_FILE_INVALID_JSCRIPT,true);
                            
                                public static final StatusDetailCodeType MEDIA_NOT_FOUND =
                                    new StatusDetailCodeType(_MEDIA_NOT_FOUND,true);
                            
                                public static final StatusDetailCodeType MEDIA_NOT_INITIALIZED =
                                    new StatusDetailCodeType(_MEDIA_NOT_INITIALIZED,true);
                            
                                public static final StatusDetailCodeType MEMORIZED_TRANS_ERROR =
                                    new StatusDetailCodeType(_MEMORIZED_TRANS_ERROR,true);
                            
                                public static final StatusDetailCodeType MERGE_OPERATION_DISALLWD =
                                    new StatusDetailCodeType(_MERGE_OPERATION_DISALLWD,true);
                            
                                public static final StatusDetailCodeType MERGE_RCRD_REQD =
                                    new StatusDetailCodeType(_MERGE_RCRD_REQD,true);
                            
                                public static final StatusDetailCodeType METAVANTE_ERROR =
                                    new StatusDetailCodeType(_METAVANTE_ERROR,true);
                            
                                public static final StatusDetailCodeType METAVANTE_SECRET_ANSWER_REQD =
                                    new StatusDetailCodeType(_METAVANTE_SECRET_ANSWER_REQD,true);
                            
                                public static final StatusDetailCodeType METAVANTE_SECRET_QESTION_REQD =
                                    new StatusDetailCodeType(_METAVANTE_SECRET_QESTION_REQD,true);
                            
                                public static final StatusDetailCodeType METAVANTE_SETUP_REQD =
                                    new StatusDetailCodeType(_METAVANTE_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType METAVANTE_TEMP_UNAVAILBL =
                                    new StatusDetailCodeType(_METAVANTE_TEMP_UNAVAILBL,true);
                            
                                public static final StatusDetailCodeType METHOD_NAME_CANNOT_BE_EMPTY =
                                    new StatusDetailCodeType(_METHOD_NAME_CANNOT_BE_EMPTY,true);
                            
                                public static final StatusDetailCodeType MISMATCHED_CURRENCY =
                                    new StatusDetailCodeType(_MISMATCHED_CURRENCY,true);
                            
                                public static final StatusDetailCodeType MISMATCHED_QTY_PRICING =
                                    new StatusDetailCodeType(_MISMATCHED_QTY_PRICING,true);
                            
                                public static final StatusDetailCodeType MISMATCHED_SEARCH_PARENTHESIS =
                                    new StatusDetailCodeType(_MISMATCHED_SEARCH_PARENTHESIS,true);
                            
                                public static final StatusDetailCodeType MISMATCH_EVENT_ISSUE_STATUS =
                                    new StatusDetailCodeType(_MISMATCH_EVENT_ISSUE_STATUS,true);
                            
                                public static final StatusDetailCodeType MISMATCH_ISSUE_PRODUCT_VERSION =
                                    new StatusDetailCodeType(_MISMATCH_ISSUE_PRODUCT_VERSION,true);
                            
                                public static final StatusDetailCodeType MISMATCH_SALES_CONTRIBUTION =
                                    new StatusDetailCodeType(_MISMATCH_SALES_CONTRIBUTION,true);
                            
                                public static final StatusDetailCodeType MISSING_ACCT_PRD =
                                    new StatusDetailCodeType(_MISSING_ACCT_PRD,true);
                            
                                public static final StatusDetailCodeType MISSING_CRNCY_EXCH_RATE =
                                    new StatusDetailCodeType(_MISSING_CRNCY_EXCH_RATE,true);
                            
                                public static final StatusDetailCodeType MISSING_ENUM =
                                    new StatusDetailCodeType(_MISSING_ENUM,true);
                            
                                public static final StatusDetailCodeType MISSING_REQD_FLD =
                                    new StatusDetailCodeType(_MISSING_REQD_FLD,true);
                            
                                public static final StatusDetailCodeType MISSNG_ACCT_PRD =
                                    new StatusDetailCodeType(_MISSNG_ACCT_PRD,true);
                            
                                public static final StatusDetailCodeType MISSNG_REV_REC_RCRD =
                                    new StatusDetailCodeType(_MISSNG_REV_REC_RCRD,true);
                            
                                public static final StatusDetailCodeType MISSNG_SO_REV_REC_PARAMS =
                                    new StatusDetailCodeType(_MISSNG_SO_REV_REC_PARAMS,true);
                            
                                public static final StatusDetailCodeType MISSNG_SO_START_END_DATES =
                                    new StatusDetailCodeType(_MISSNG_SO_START_END_DATES,true);
                            
                                public static final StatusDetailCodeType MLI_REQD =
                                    new StatusDetailCodeType(_MLI_REQD,true);
                            
                                public static final StatusDetailCodeType MLTPLE_TAX_LINES_DISALLWD =
                                    new StatusDetailCodeType(_MLTPLE_TAX_LINES_DISALLWD,true);
                            
                                public static final StatusDetailCodeType MSNG_FIELD_OWRTE_MUST_BE_TRUE =
                                    new StatusDetailCodeType(_MSNG_FIELD_OWRTE_MUST_BE_TRUE,true);
                            
                                public static final StatusDetailCodeType MST_UPDATE_ITEMS_THEN_RATES =
                                    new StatusDetailCodeType(_MST_UPDATE_ITEMS_THEN_RATES,true);
                            
                                public static final StatusDetailCodeType MULTISELECT_TYPE_REQD =
                                    new StatusDetailCodeType(_MULTISELECT_TYPE_REQD,true);
                            
                                public static final StatusDetailCodeType MULTI_ACCT_CANT_CHANGE_PSWD =
                                    new StatusDetailCodeType(_MULTI_ACCT_CANT_CHANGE_PSWD,true);
                            
                                public static final StatusDetailCodeType MULTI_LOC_INVT_ERROR =
                                    new StatusDetailCodeType(_MULTI_LOC_INVT_ERROR,true);
                            
                                public static final StatusDetailCodeType MULTI_PRIMARY_PARTNER_DISALLWD =
                                    new StatusDetailCodeType(_MULTI_PRIMARY_PARTNER_DISALLWD,true);
                            
                                public static final StatusDetailCodeType MULTI_SHIP_ROUTES_REQD =
                                    new StatusDetailCodeType(_MULTI_SHIP_ROUTES_REQD,true);
                            
                                public static final StatusDetailCodeType MUST_DEFINE_BASE_UNIT =
                                    new StatusDetailCodeType(_MUST_DEFINE_BASE_UNIT,true);
                            
                                public static final StatusDetailCodeType MUST_RESUBMIT_RCRD =
                                    new StatusDetailCodeType(_MUST_RESUBMIT_RCRD,true);
                            
                                public static final StatusDetailCodeType NAME_ALREADY_IN_USE =
                                    new StatusDetailCodeType(_NAME_ALREADY_IN_USE,true);
                            
                                public static final StatusDetailCodeType NAME_REQD =
                                    new StatusDetailCodeType(_NAME_REQD,true);
                            
                                public static final StatusDetailCodeType NAME_TYPE_FLDR_FIELDS_REQD =
                                    new StatusDetailCodeType(_NAME_TYPE_FLDR_FIELDS_REQD,true);
                            
                                public static final StatusDetailCodeType NARROW_KEYWORD_SEARCH =
                                    new StatusDetailCodeType(_NARROW_KEYWORD_SEARCH,true);
                            
                                public static final StatusDetailCodeType NEED_BILL_VARIANCE_ACCT =
                                    new StatusDetailCodeType(_NEED_BILL_VARIANCE_ACCT,true);
                            
                                public static final StatusDetailCodeType NEGATIVE_PAYMENT_DISALLWD =
                                    new StatusDetailCodeType(_NEGATIVE_PAYMENT_DISALLWD,true);
                            
                                public static final StatusDetailCodeType NEGATIVE_TAX_RATE_DISALLWD =
                                    new StatusDetailCodeType(_NEGATIVE_TAX_RATE_DISALLWD,true);
                            
                                public static final StatusDetailCodeType NEITHER_ARGUMENT_DEFINED =
                                    new StatusDetailCodeType(_NEITHER_ARGUMENT_DEFINED,true);
                            
                                public static final StatusDetailCodeType NEW_CONNECTION_DISALLWD =
                                    new StatusDetailCodeType(_NEW_CONNECTION_DISALLWD,true);
                            
                                public static final StatusDetailCodeType NEXUS_REQD =
                                    new StatusDetailCodeType(_NEXUS_REQD,true);
                            
                                public static final StatusDetailCodeType NONMATCHING_EMAILS =
                                    new StatusDetailCodeType(_NONMATCHING_EMAILS,true);
                            
                                public static final StatusDetailCodeType NONUNIQUE_INDEX_VALUE =
                                    new StatusDetailCodeType(_NONUNIQUE_INDEX_VALUE,true);
                            
                                public static final StatusDetailCodeType NONZERO_AMT_REQD =
                                    new StatusDetailCodeType(_NONZERO_AMT_REQD,true);
                            
                                public static final StatusDetailCodeType NONZERO_QTY_REQD =
                                    new StatusDetailCodeType(_NONZERO_QTY_REQD,true);
                            
                                public static final StatusDetailCodeType NONZERO_WEIGHT_REQD =
                                    new StatusDetailCodeType(_NONZERO_WEIGHT_REQD,true);
                            
                                public static final StatusDetailCodeType NON_ADMIN_CANNOT_INITIATE_LINK =
                                    new StatusDetailCodeType(_NON_ADMIN_CANNOT_INITIATE_LINK,true);
                            
                                public static final StatusDetailCodeType NOT_AN_INVITEE =
                                    new StatusDetailCodeType(_NOT_AN_INVITEE,true);
                            
                                public static final StatusDetailCodeType NOT_IN_INVT =
                                    new StatusDetailCodeType(_NOT_IN_INVT,true);
                            
                                public static final StatusDetailCodeType NO_DATA_FOUND =
                                    new StatusDetailCodeType(_NO_DATA_FOUND,true);
                            
                                public static final StatusDetailCodeType NO_EXPENSES_FOR_PRD =
                                    new StatusDetailCodeType(_NO_EXPENSES_FOR_PRD,true);
                            
                                public static final StatusDetailCodeType NO_ITEMS_TO_PRINT =
                                    new StatusDetailCodeType(_NO_ITEMS_TO_PRINT,true);
                            
                                public static final StatusDetailCodeType NO_MASS_UPDATES_RUNNING =
                                    new StatusDetailCodeType(_NO_MASS_UPDATES_RUNNING,true);
                            
                                public static final StatusDetailCodeType NO_MTRX_ITEMS_TO_UPDATE =
                                    new StatusDetailCodeType(_NO_MTRX_ITEMS_TO_UPDATE,true);
                            
                                public static final StatusDetailCodeType NO_ORD_SHPMNT =
                                    new StatusDetailCodeType(_NO_ORD_SHPMNT,true);
                            
                                public static final StatusDetailCodeType NO_RCRDS_MATCH =
                                    new StatusDetailCodeType(_NO_RCRDS_MATCH,true);
                            
                                public static final StatusDetailCodeType NO_RCRD_FOR_USER =
                                    new StatusDetailCodeType(_NO_RCRD_FOR_USER,true);
                            
                                public static final StatusDetailCodeType NO_SCHDUL_APPLIED =
                                    new StatusDetailCodeType(_NO_SCHDUL_APPLIED,true);
                            
                                public static final StatusDetailCodeType NO_UPSERT =
                                    new StatusDetailCodeType(_NO_UPSERT,true);
                            
                                public static final StatusDetailCodeType NULL_CHECK_NUMBER =
                                    new StatusDetailCodeType(_NULL_CHECK_NUMBER,true);
                            
                                public static final StatusDetailCodeType NUMERIC_CHECK_NUM_REQD =
                                    new StatusDetailCodeType(_NUMERIC_CHECK_NUM_REQD,true);
                            
                                public static final StatusDetailCodeType NUMERIC_REF_NUM_REQD =
                                    new StatusDetailCodeType(_NUMERIC_REF_NUM_REQD,true);
                            
                                public static final StatusDetailCodeType NUM_ITEMS_GRTR_THAN_QTY =
                                    new StatusDetailCodeType(_NUM_ITEMS_GRTR_THAN_QTY,true);
                            
                                public static final StatusDetailCodeType NUM_ITEMS_NOT_EQUAL_TO_QTY =
                                    new StatusDetailCodeType(_NUM_ITEMS_NOT_EQUAL_TO_QTY,true);
                            
                                public static final StatusDetailCodeType NUM_REQD_FOR_FIRST_LABEL =
                                    new StatusDetailCodeType(_NUM_REQD_FOR_FIRST_LABEL,true);
                            
                                public static final StatusDetailCodeType OI_FEATURE_REQD =
                                    new StatusDetailCodeType(_OI_FEATURE_REQD,true);
                            
                                public static final StatusDetailCodeType OI_PERMISSION_REQD =
                                    new StatusDetailCodeType(_OI_PERMISSION_REQD,true);
                            
                                public static final StatusDetailCodeType ONE_ADMIN_REQD_PER_ACCT =
                                    new StatusDetailCodeType(_ONE_ADMIN_REQD_PER_ACCT,true);
                            
                                public static final StatusDetailCodeType ONE_DIRECT_DEPOSIT_ACT_ALLWD =
                                    new StatusDetailCodeType(_ONE_DIRECT_DEPOSIT_ACT_ALLWD,true);
                            
                                public static final StatusDetailCodeType ONE_EMPL_REQD =
                                    new StatusDetailCodeType(_ONE_EMPL_REQD,true);
                            
                                public static final StatusDetailCodeType ONE_PAY_ITEM_PER_EMPL =
                                    new StatusDetailCodeType(_ONE_PAY_ITEM_PER_EMPL,true);
                            
                                public static final StatusDetailCodeType ONE_POSITIVE_VALUE_REQD =
                                    new StatusDetailCodeType(_ONE_POSITIVE_VALUE_REQD,true);
                            
                                public static final StatusDetailCodeType ONE_RCRD_REQD_FOR_MASS_UPDATE =
                                    new StatusDetailCodeType(_ONE_RCRD_REQD_FOR_MASS_UPDATE,true);
                            
                                public static final StatusDetailCodeType ONE_ROLE_REQD =
                                    new StatusDetailCodeType(_ONE_ROLE_REQD,true);
                            
                                public static final StatusDetailCodeType ONLINE_BANK_FILE_REQD =
                                    new StatusDetailCodeType(_ONLINE_BANK_FILE_REQD,true);
                            
                                public static final StatusDetailCodeType ONLINE_BILL_PAY_DUP =
                                    new StatusDetailCodeType(_ONLINE_BILL_PAY_DUP,true);
                            
                                public static final StatusDetailCodeType ONLINE_BILL_PAY_SETUP_REQD =
                                    new StatusDetailCodeType(_ONLINE_BILL_PAY_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType ONLINE_FORM_DSNT_EXIST =
                                    new StatusDetailCodeType(_ONLINE_FORM_DSNT_EXIST,true);
                            
                                public static final StatusDetailCodeType ONLINE_FORM_EMPTY =
                                    new StatusDetailCodeType(_ONLINE_FORM_EMPTY,true);
                            
                                public static final StatusDetailCodeType ONLINE_FORM_ID_REQD =
                                    new StatusDetailCodeType(_ONLINE_FORM_ID_REQD,true);
                            
                                public static final StatusDetailCodeType ONLINE_FORM_USER_ACCESS_ONLY =
                                    new StatusDetailCodeType(_ONLINE_FORM_USER_ACCESS_ONLY,true);
                            
                                public static final StatusDetailCodeType ONLINE_ORD_FEATURE_DISABLED =
                                    new StatusDetailCodeType(_ONLINE_ORD_FEATURE_DISABLED,true);
                            
                                public static final StatusDetailCodeType ONLY_ONE_CONTRIB_ITEM_REQD =
                                    new StatusDetailCodeType(_ONLY_ONE_CONTRIB_ITEM_REQD,true);
                            
                                public static final StatusDetailCodeType ONLY_ONE_DEDCT_ITEM_REQD =
                                    new StatusDetailCodeType(_ONLY_ONE_DEDCT_ITEM_REQD,true);
                            
                                public static final StatusDetailCodeType ONLY_ONE_DISTRIB_ALLWD =
                                    new StatusDetailCodeType(_ONLY_ONE_DISTRIB_ALLWD,true);
                            
                                public static final StatusDetailCodeType ONLY_ONE_EARNING_ITEM_REQD =
                                    new StatusDetailCodeType(_ONLY_ONE_EARNING_ITEM_REQD,true);
                            
                                public static final StatusDetailCodeType ONLY_ONE_LOT_NUM_ALLWD =
                                    new StatusDetailCodeType(_ONLY_ONE_LOT_NUM_ALLWD,true);
                            
                                public static final StatusDetailCodeType ONLY_ONE_PREF_BIN_ALLWD =
                                    new StatusDetailCodeType(_ONLY_ONE_PREF_BIN_ALLWD,true);
                            
                                public static final StatusDetailCodeType ONLY_ONE_UNIT_AS_BASE_UNIT =
                                    new StatusDetailCodeType(_ONLY_ONE_UNIT_AS_BASE_UNIT,true);
                            
                                public static final StatusDetailCodeType ONLY_ONE_UPLOAD_ALLWD =
                                    new StatusDetailCodeType(_ONLY_ONE_UPLOAD_ALLWD,true);
                            
                                public static final StatusDetailCodeType ONLY_ONE_WITHLD_ITEM_REQD =
                                    new StatusDetailCodeType(_ONLY_ONE_WITHLD_ITEM_REQD,true);
                            
                                public static final StatusDetailCodeType OPENID_DOMAIN_IN_USE =
                                    new StatusDetailCodeType(_OPENID_DOMAIN_IN_USE,true);
                            
                                public static final StatusDetailCodeType OPENID_NOT_ENABLED =
                                    new StatusDetailCodeType(_OPENID_NOT_ENABLED,true);
                            
                                public static final StatusDetailCodeType OPERATOR_ARITY_MISMATCH =
                                    new StatusDetailCodeType(_OPERATOR_ARITY_MISMATCH,true);
                            
                                public static final StatusDetailCodeType OPRTN_UNAVAILBL_TO_GATEWAY =
                                    new StatusDetailCodeType(_OPRTN_UNAVAILBL_TO_GATEWAY,true);
                            
                                public static final StatusDetailCodeType ORDER_DSNT_EXIST =
                                    new StatusDetailCodeType(_ORDER_DSNT_EXIST,true);
                            
                                public static final StatusDetailCodeType ORD_ALREADY_APPRVD =
                                    new StatusDetailCodeType(_ORD_ALREADY_APPRVD,true);
                            
                                public static final StatusDetailCodeType OTHER_PMT_AUTH_IN_PROGRESS =
                                    new StatusDetailCodeType(_OTHER_PMT_AUTH_IN_PROGRESS,true);
                            
                                public static final StatusDetailCodeType OVERAGE_DISALLWD =
                                    new StatusDetailCodeType(_OVERAGE_DISALLWD,true);
                            
                                public static final StatusDetailCodeType OVERLAPPING_PRDS_DISALLWD =
                                    new StatusDetailCodeType(_OVERLAPPING_PRDS_DISALLWD,true);
                            
                                public static final StatusDetailCodeType OVER_FULFILL_DISALLWD =
                                    new StatusDetailCodeType(_OVER_FULFILL_DISALLWD,true);
                            
                                public static final StatusDetailCodeType OVER_FULFILL_RECEIV_DISALLWD =
                                    new StatusDetailCodeType(_OVER_FULFILL_RECEIV_DISALLWD,true);
                            
                                public static final StatusDetailCodeType OWNER_REQD =
                                    new StatusDetailCodeType(_OWNER_REQD,true);
                            
                                public static final StatusDetailCodeType PACKAGE_WEIGHT_REQD =
                                    new StatusDetailCodeType(_PACKAGE_WEIGHT_REQD,true);
                            
                                public static final StatusDetailCodeType PACKG_LEVEL_REF_DISALLWD =
                                    new StatusDetailCodeType(_PACKG_LEVEL_REF_DISALLWD,true);
                            
                                public static final StatusDetailCodeType PACKG_VALUE_TOO_LARGE =
                                    new StatusDetailCodeType(_PACKG_VALUE_TOO_LARGE,true);
                            
                                public static final StatusDetailCodeType PARENT_CANT_ITSELF_BE_MEMBER =
                                    new StatusDetailCodeType(_PARENT_CANT_ITSELF_BE_MEMBER,true);
                            
                                public static final StatusDetailCodeType PARENT_MUST_BE_MATRIX_ITEM =
                                    new StatusDetailCodeType(_PARENT_MUST_BE_MATRIX_ITEM,true);
                            
                                public static final StatusDetailCodeType PARENT_REQD =
                                    new StatusDetailCodeType(_PARENT_REQD,true);
                            
                                public static final StatusDetailCodeType PARSING_ERROR =
                                    new StatusDetailCodeType(_PARSING_ERROR,true);
                            
                                public static final StatusDetailCodeType PARTIAL_FULFILL_RCEIV_DISALLWD =
                                    new StatusDetailCodeType(_PARTIAL_FULFILL_RCEIV_DISALLWD,true);
                            
                                public static final StatusDetailCodeType PARTNER_ACCESS_DENIED =
                                    new StatusDetailCodeType(_PARTNER_ACCESS_DENIED,true);
                            
                                public static final StatusDetailCodeType PARTNER_ACCT_NOT_LINKED =
                                    new StatusDetailCodeType(_PARTNER_ACCT_NOT_LINKED,true);
                            
                                public static final StatusDetailCodeType PARTNER_CODE_ALREADY_USED =
                                    new StatusDetailCodeType(_PARTNER_CODE_ALREADY_USED,true);
                            
                                public static final StatusDetailCodeType PAYCHECK_ALREADY_PAID =
                                    new StatusDetailCodeType(_PAYCHECK_ALREADY_PAID,true);
                            
                                public static final StatusDetailCodeType PAYCHECK_IN_USE =
                                    new StatusDetailCodeType(_PAYCHECK_IN_USE,true);
                            
                                public static final StatusDetailCodeType PAYEE_REQD_FOR_PMT =
                                    new StatusDetailCodeType(_PAYEE_REQD_FOR_PMT,true);
                            
                                public static final StatusDetailCodeType PAYPAL_FUND_SOURCE_REQD =
                                    new StatusDetailCodeType(_PAYPAL_FUND_SOURCE_REQD,true);
                            
                                public static final StatusDetailCodeType PAYPAL_INVALID_PMT_METHOD =
                                    new StatusDetailCodeType(_PAYPAL_INVALID_PMT_METHOD,true);
                            
                                public static final StatusDetailCodeType PAYPAL_PMT_NOTIFICATION =
                                    new StatusDetailCodeType(_PAYPAL_PMT_NOTIFICATION,true);
                            
                                public static final StatusDetailCodeType PAYPAL_SETUP_REQD =
                                    new StatusDetailCodeType(_PAYPAL_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType PAYROLL_COMMITTED =
                                    new StatusDetailCodeType(_PAYROLL_COMMITTED,true);
                            
                                public static final StatusDetailCodeType PAYROLL_EXPENSE_ACCT_REQD =
                                    new StatusDetailCodeType(_PAYROLL_EXPENSE_ACCT_REQD,true);
                            
                                public static final StatusDetailCodeType PAYROLL_FEATURE_DISABLED =
                                    new StatusDetailCodeType(_PAYROLL_FEATURE_DISABLED,true);
                            
                                public static final StatusDetailCodeType PAYROLL_FEATURE_UNAVAILABLE =
                                    new StatusDetailCodeType(_PAYROLL_FEATURE_UNAVAILABLE,true);
                            
                                public static final StatusDetailCodeType PAYROLL_FUND_ACCT_REQD =
                                    new StatusDetailCodeType(_PAYROLL_FUND_ACCT_REQD,true);
                            
                                public static final StatusDetailCodeType PAYROLL_IN_PROCESS =
                                    new StatusDetailCodeType(_PAYROLL_IN_PROCESS,true);
                            
                                public static final StatusDetailCodeType PAYROLL_ITEM_DELETE_DISALLWD =
                                    new StatusDetailCodeType(_PAYROLL_ITEM_DELETE_DISALLWD,true);
                            
                                public static final StatusDetailCodeType PAYROLL_LIABILITY_ACCT_REQD =
                                    new StatusDetailCodeType(_PAYROLL_LIABILITY_ACCT_REQD,true);
                            
                                public static final StatusDetailCodeType PAYROLL_MAINTENANCE =
                                    new StatusDetailCodeType(_PAYROLL_MAINTENANCE,true);
                            
                                public static final StatusDetailCodeType PAYROLL_SETUP_REQD =
                                    new StatusDetailCodeType(_PAYROLL_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType PAYROLL_UPDATE_REQD =
                                    new StatusDetailCodeType(_PAYROLL_UPDATE_REQD,true);
                            
                                public static final StatusDetailCodeType PAY_HOLD_ON_SO =
                                    new StatusDetailCodeType(_PAY_HOLD_ON_SO,true);
                            
                                public static final StatusDetailCodeType PERMISSION_VIOLATION =
                                    new StatusDetailCodeType(_PERMISSION_VIOLATION,true);
                            
                                public static final StatusDetailCodeType PHONE_NUM_REQD =
                                    new StatusDetailCodeType(_PHONE_NUM_REQD,true);
                            
                                public static final StatusDetailCodeType PIN_DEBIT_TRANS_DISALLWD =
                                    new StatusDetailCodeType(_PIN_DEBIT_TRANS_DISALLWD,true);
                            
                                public static final StatusDetailCodeType PLAN_IN_USE =
                                    new StatusDetailCodeType(_PLAN_IN_USE,true);
                            
                                public static final StatusDetailCodeType PLAN_OVERLAP_DISALLWD =
                                    new StatusDetailCodeType(_PLAN_OVERLAP_DISALLWD,true);
                            
                                public static final StatusDetailCodeType PMT_ALREADY_APPRVD =
                                    new StatusDetailCodeType(_PMT_ALREADY_APPRVD,true);
                            
                                public static final StatusDetailCodeType PMT_ALREADY_EXISTS =
                                    new StatusDetailCodeType(_PMT_ALREADY_EXISTS,true);
                            
                                public static final StatusDetailCodeType PMT_ALREADY_SBMTD =
                                    new StatusDetailCodeType(_PMT_ALREADY_SBMTD,true);
                            
                                public static final StatusDetailCodeType PMT_EDIT_DISALLWD =
                                    new StatusDetailCodeType(_PMT_EDIT_DISALLWD,true);
                            
                                public static final StatusDetailCodeType POSITIVE_BIN_QTY_REQD =
                                    new StatusDetailCodeType(_POSITIVE_BIN_QTY_REQD,true);
                            
                                public static final StatusDetailCodeType POSITIVE_QTY_REQD =
                                    new StatusDetailCodeType(_POSITIVE_QTY_REQD,true);
                            
                                public static final StatusDetailCodeType POSITIVE_UNITCOST_REQD =
                                    new StatusDetailCodeType(_POSITIVE_UNITCOST_REQD,true);
                            
                                public static final StatusDetailCodeType POSTING_DISCOUNT_DISALLWD =
                                    new StatusDetailCodeType(_POSTING_DISCOUNT_DISALLWD,true);
                            
                                public static final StatusDetailCodeType POSTING_PRD_SETUP_REQD =
                                    new StatusDetailCodeType(_POSTING_PRD_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType PRDS_DISALLWD_NAMES_NOT_UNIQUE =
                                    new StatusDetailCodeType(_PRDS_DISALLWD_NAMES_NOT_UNIQUE,true);
                            
                                public static final StatusDetailCodeType PRD_SETUP_REQD =
                                    new StatusDetailCodeType(_PRD_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType PREFERRED_TAX_AGENCY_REQD =
                                    new StatusDetailCodeType(_PREFERRED_TAX_AGENCY_REQD,true);
                            
                                public static final StatusDetailCodeType PREF_VENDOR_COST_REQD =
                                    new StatusDetailCodeType(_PREF_VENDOR_COST_REQD,true);
                            
                                public static final StatusDetailCodeType PREF_VENDOR_REQD =
                                    new StatusDetailCodeType(_PREF_VENDOR_REQD,true);
                            
                                public static final StatusDetailCodeType PRIVATE_RCRD_ACCESS_DISALLWD =
                                    new StatusDetailCodeType(_PRIVATE_RCRD_ACCESS_DISALLWD,true);
                            
                                public static final StatusDetailCodeType PRIVATE_STATUS_CHNG_DISALLWD =
                                    new StatusDetailCodeType(_PRIVATE_STATUS_CHNG_DISALLWD,true);
                            
                                public static final StatusDetailCodeType PRODUCT_MODULE_MISMATCH =
                                    new StatusDetailCodeType(_PRODUCT_MODULE_MISMATCH,true);
                            
                                public static final StatusDetailCodeType PSWD_EXPIRED =
                                    new StatusDetailCodeType(_PSWD_EXPIRED,true);
                            
                                public static final StatusDetailCodeType PSWD_REQD =
                                    new StatusDetailCodeType(_PSWD_REQD,true);
                            
                                public static final StatusDetailCodeType PWSDS_DONT_MATCH =
                                    new StatusDetailCodeType(_PWSDS_DONT_MATCH,true);
                            
                                public static final StatusDetailCodeType QTY_EXCEEDED_QTY_BUCKETS =
                                    new StatusDetailCodeType(_QTY_EXCEEDED_QTY_BUCKETS,true);
                            
                                public static final StatusDetailCodeType QTY_REQD =
                                    new StatusDetailCodeType(_QTY_REQD,true);
                            
                                public static final StatusDetailCodeType RATE_REQUEST_SHPMNT_REQD =
                                    new StatusDetailCodeType(_RATE_REQUEST_SHPMNT_REQD,true);
                            
                                public static final StatusDetailCodeType RATE_SRVC_UNAVAILBL =
                                    new StatusDetailCodeType(_RATE_SRVC_UNAVAILBL,true);
                            
                                public static final StatusDetailCodeType RCRD_DELETED_SINCE_RETRIEVED =
                                    new StatusDetailCodeType(_RCRD_DELETED_SINCE_RETRIEVED,true);
                            
                                public static final StatusDetailCodeType RCRD_DSNT_EXIST =
                                    new StatusDetailCodeType(_RCRD_DSNT_EXIST,true);
                            
                                public static final StatusDetailCodeType RCRD_EDITED_SINCE_RETRIEVED =
                                    new StatusDetailCodeType(_RCRD_EDITED_SINCE_RETRIEVED,true);
                            
                                public static final StatusDetailCodeType RCRD_HAS_BEEN_CHANGED =
                                    new StatusDetailCodeType(_RCRD_HAS_BEEN_CHANGED,true);
                            
                                public static final StatusDetailCodeType RCRD_ID_NOT_INT =
                                    new StatusDetailCodeType(_RCRD_ID_NOT_INT,true);
                            
                                public static final StatusDetailCodeType RCRD_LOCKED_BY_WF =
                                    new StatusDetailCodeType(_RCRD_LOCKED_BY_WF,true);
                            
                                public static final StatusDetailCodeType RCRD_NOT_FOUND =
                                    new StatusDetailCodeType(_RCRD_NOT_FOUND,true);
                            
                                public static final StatusDetailCodeType RCRD_PREVSLY_DELETED =
                                    new StatusDetailCodeType(_RCRD_PREVSLY_DELETED,true);
                            
                                public static final StatusDetailCodeType RCRD_REF_RCRD_TYP_MISMATCH =
                                    new StatusDetailCodeType(_RCRD_REF_RCRD_TYP_MISMATCH,true);
                            
                                public static final StatusDetailCodeType RCRD_SUB_MISMATCH_WITH_CLASS =
                                    new StatusDetailCodeType(_RCRD_SUB_MISMATCH_WITH_CLASS,true);
                            
                                public static final StatusDetailCodeType RCRD_TYPE_REQD =
                                    new StatusDetailCodeType(_RCRD_TYPE_REQD,true);
                            
                                public static final StatusDetailCodeType RCRD_UNEDITABLE =
                                    new StatusDetailCodeType(_RCRD_UNEDITABLE,true);
                            
                                public static final StatusDetailCodeType REACHED_LIST_END =
                                    new StatusDetailCodeType(_REACHED_LIST_END,true);
                            
                                public static final StatusDetailCodeType REACHED_LIST_START =
                                    new StatusDetailCodeType(_REACHED_LIST_START,true);
                            
                                public static final StatusDetailCodeType RECALCING_PLAN_SCHDUL =
                                    new StatusDetailCodeType(_RECALCING_PLAN_SCHDUL,true);
                            
                                public static final StatusDetailCodeType RECURSV_REF_DISALLWD =
                                    new StatusDetailCodeType(_RECURSV_REF_DISALLWD,true);
                            
                                public static final StatusDetailCodeType RECUR_EVENT_DISALLWD =
                                    new StatusDetailCodeType(_RECUR_EVENT_DISALLWD,true);
                            
                                public static final StatusDetailCodeType REC_TYP_REQD =
                                    new StatusDetailCodeType(_REC_TYP_REQD,true);
                            
                                public static final StatusDetailCodeType REPORT_EXPIRED =
                                    new StatusDetailCodeType(_REPORT_EXPIRED,true);
                            
                                public static final StatusDetailCodeType REQD_FORM_TAG_MISSING =
                                    new StatusDetailCodeType(_REQD_FORM_TAG_MISSING,true);
                            
                                public static final StatusDetailCodeType REQD_LOC_FIELDS_MISSING =
                                    new StatusDetailCodeType(_REQD_LOC_FIELDS_MISSING,true);
                            
                                public static final StatusDetailCodeType REQD_SUB_FIELDS_MISSING =
                                    new StatusDetailCodeType(_REQD_SUB_FIELDS_MISSING,true);
                            
                                public static final StatusDetailCodeType REQUEST_PARAM_REQD =
                                    new StatusDetailCodeType(_REQUEST_PARAM_REQD,true);
                            
                                public static final StatusDetailCodeType REVERSAL_DATE_WARNING =
                                    new StatusDetailCodeType(_REVERSAL_DATE_WARNING,true);
                            
                                public static final StatusDetailCodeType REV_REC_DATE_REQD =
                                    new StatusDetailCodeType(_REV_REC_DATE_REQD,true);
                            
                                public static final StatusDetailCodeType REV_REC_TMPLT_DATA_MISSING =
                                    new StatusDetailCodeType(_REV_REC_TMPLT_DATA_MISSING,true);
                            
                                public static final StatusDetailCodeType REV_REC_UPDATE_DISALLWD =
                                    new StatusDetailCodeType(_REV_REC_UPDATE_DISALLWD,true);
                            
                                public static final StatusDetailCodeType ROLE_REQD =
                                    new StatusDetailCodeType(_ROLE_REQD,true);
                            
                                public static final StatusDetailCodeType ROLE_REQUIRED =
                                    new StatusDetailCodeType(_ROLE_REQUIRED,true);
                            
                                public static final StatusDetailCodeType ROUNDING_DIFF_TOO_BIG =
                                    new StatusDetailCodeType(_ROUNDING_DIFF_TOO_BIG,true);
                            
                                public static final StatusDetailCodeType ROUNDING_ERROR =
                                    new StatusDetailCodeType(_ROUNDING_ERROR,true);
                            
                                public static final StatusDetailCodeType ROUTING_NUM_REQD =
                                    new StatusDetailCodeType(_ROUTING_NUM_REQD,true);
                            
                                public static final StatusDetailCodeType SALES_DISCOUNT_ACCT_REQD =
                                    new StatusDetailCodeType(_SALES_DISCOUNT_ACCT_REQD,true);
                            
                                public static final StatusDetailCodeType SAME_ACCT_TYP_REQD_FOR_PARENT =
                                    new StatusDetailCodeType(_SAME_ACCT_TYP_REQD_FOR_PARENT,true);
                            
                                public static final StatusDetailCodeType SAVED_SRCH_EMAIL_ERROR =
                                    new StatusDetailCodeType(_SAVED_SRCH_EMAIL_ERROR,true);
                            
                                public static final StatusDetailCodeType SCHDUL_EDIT_DISALLWD =
                                    new StatusDetailCodeType(_SCHDUL_EDIT_DISALLWD,true);
                            
                                public static final StatusDetailCodeType SCHEDULED_REPORT_ERROR =
                                    new StatusDetailCodeType(_SCHEDULED_REPORT_ERROR,true);
                            
                                public static final StatusDetailCodeType SEARCH_DATE_FILTER_REQD =
                                    new StatusDetailCodeType(_SEARCH_DATE_FILTER_REQD,true);
                            
                                public static final StatusDetailCodeType SEARCH_ERROR =
                                    new StatusDetailCodeType(_SEARCH_ERROR,true);
                            
                                public static final StatusDetailCodeType SEARCH_INTEGER_REQD =
                                    new StatusDetailCodeType(_SEARCH_INTEGER_REQD,true);
                            
                                public static final StatusDetailCodeType SEARCH_TIMED_OUT =
                                    new StatusDetailCodeType(_SEARCH_TIMED_OUT,true);
                            
                                public static final StatusDetailCodeType SECURE_TRANS_REQD_ON_CHECKOUT =
                                    new StatusDetailCodeType(_SECURE_TRANS_REQD_ON_CHECKOUT,true);
                            
                                public static final StatusDetailCodeType SELECT_OPTION_ALREADY_PRESENT =
                                    new StatusDetailCodeType(_SELECT_OPTION_ALREADY_PRESENT,true);
                            
                                public static final StatusDetailCodeType SELECT_OPTION_NOT_FOUND =
                                    new StatusDetailCodeType(_SELECT_OPTION_NOT_FOUND,true);
                            
                                public static final StatusDetailCodeType SERIAL_NUM_MATCH_MULTI_ITEMS =
                                    new StatusDetailCodeType(_SERIAL_NUM_MATCH_MULTI_ITEMS,true);
                            
                                public static final StatusDetailCodeType SESSION_TERMD_2ND_LOGIN_DECTD =
                                    new StatusDetailCodeType(_SESSION_TERMD_2ND_LOGIN_DECTD,true);
                            
                                public static final StatusDetailCodeType SESSION_TIMED_OUT =
                                    new StatusDetailCodeType(_SESSION_TIMED_OUT,true);
                            
                                public static final StatusDetailCodeType SETUP_METER_REQD =
                                    new StatusDetailCodeType(_SETUP_METER_REQD,true);
                            
                                public static final StatusDetailCodeType SET_SHIPPING_PICKUP_TYP =
                                    new StatusDetailCodeType(_SET_SHIPPING_PICKUP_TYP,true);
                            
                                public static final StatusDetailCodeType SHIPFROM_ADDRESS_NOT_SET =
                                    new StatusDetailCodeType(_SHIPFROM_ADDRESS_NOT_SET,true);
                            
                                public static final StatusDetailCodeType SHIPMNT_INSURANCE_NOT_AVAILABLE =
                                    new StatusDetailCodeType(_SHIPMNT_INSURANCE_NOT_AVAILABLE,true);
                            
                                public static final StatusDetailCodeType SHIP_ADDR_REQD =
                                    new StatusDetailCodeType(_SHIP_ADDR_REQD,true);
                            
                                public static final StatusDetailCodeType SHIP_MANIFEST_ALREADY_PRCSSD =
                                    new StatusDetailCodeType(_SHIP_MANIFEST_ALREADY_PRCSSD,true);
                            
                                public static final StatusDetailCodeType SHIP_MANIFEST_ERROR =
                                    new StatusDetailCodeType(_SHIP_MANIFEST_ERROR,true);
                            
                                public static final StatusDetailCodeType SHIP_SETUP_REQD =
                                    new StatusDetailCodeType(_SHIP_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType SHIP_TALBE_UNBALNCD =
                                    new StatusDetailCodeType(_SHIP_TALBE_UNBALNCD,true);
                            
                                public static final StatusDetailCodeType SHIP_USER_ERROR =
                                    new StatusDetailCodeType(_SHIP_USER_ERROR,true);
                            
                                public static final StatusDetailCodeType SINGLE_VALUE_REQD =
                                    new StatusDetailCodeType(_SINGLE_VALUE_REQD,true);
                            
                                public static final StatusDetailCodeType SITEMAP_GEN_ERROR =
                                    new StatusDetailCodeType(_SITEMAP_GEN_ERROR,true);
                            
                                public static final StatusDetailCodeType SITE_DOMAIN_NAME_REQD =
                                    new StatusDetailCodeType(_SITE_DOMAIN_NAME_REQD,true);
                            
                                public static final StatusDetailCodeType SITE_TAG_ALREADY_EXISTS =
                                    new StatusDetailCodeType(_SITE_TAG_ALREADY_EXISTS,true);
                            
                                public static final StatusDetailCodeType SO_HAS_CHILD_TRANS =
                                    new StatusDetailCodeType(_SO_HAS_CHILD_TRANS,true);
                            
                                public static final StatusDetailCodeType SO_LINE_HAS_PO =
                                    new StatusDetailCodeType(_SO_LINE_HAS_PO,true);
                            
                                public static final StatusDetailCodeType SRVC_UNAVAILBL_FOR_LOC =
                                    new StatusDetailCodeType(_SRVC_UNAVAILBL_FOR_LOC,true);
                            
                                public static final StatusDetailCodeType SSS_AUTHOR_MUST_BE_EMPLOYEE =
                                    new StatusDetailCodeType(_SSS_AUTHOR_MUST_BE_EMPLOYEE,true);
                            
                                public static final StatusDetailCodeType SSS_CONNECTION_TIME_OUT =
                                    new StatusDetailCodeType(_SSS_CONNECTION_TIME_OUT,true);
                            
                                public static final StatusDetailCodeType SSS_DEBUG_DISALLWD =
                                    new StatusDetailCodeType(_SSS_DEBUG_DISALLWD,true);
                            
                                public static final StatusDetailCodeType SSS_DRIP_EMAIL_RAN_OUT_OF_COUPON_CODES =
                                    new StatusDetailCodeType(_SSS_DRIP_EMAIL_RAN_OUT_OF_COUPON_CODES,true);
                            
                                public static final StatusDetailCodeType SSS_DUP_DRIP_EMAIL =
                                    new StatusDetailCodeType(_SSS_DUP_DRIP_EMAIL,true);
                            
                                public static final StatusDetailCodeType SSS_FILE_SIZE_EXCEEDED =
                                    new StatusDetailCodeType(_SSS_FILE_SIZE_EXCEEDED,true);
                            
                                public static final StatusDetailCodeType SSS_INSTRUCTION_COUNT_EXCEEDED =
                                    new StatusDetailCodeType(_SSS_INSTRUCTION_COUNT_EXCEEDED,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_ATTACH_RECORD_TYPE =
                                    new StatusDetailCodeType(_SSS_INVALID_ATTACH_RECORD_TYPE,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_BCC_EMAIL =
                                    new StatusDetailCodeType(_SSS_INVALID_BCC_EMAIL,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_CC_EMAIL =
                                    new StatusDetailCodeType(_SSS_INVALID_CC_EMAIL,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_CMPGN_EVENT_ID =
                                    new StatusDetailCodeType(_SSS_INVALID_CMPGN_EVENT_ID,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_EMAIL_TEMPLATE =
                                    new StatusDetailCodeType(_SSS_INVALID_EMAIL_TEMPLATE,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_FORM_ELEMENT_NAME =
                                    new StatusDetailCodeType(_SSS_INVALID_FORM_ELEMENT_NAME,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_GSO_FLTR_OPRTOR =
                                    new StatusDetailCodeType(_SSS_INVALID_GSO_FLTR_OPRTOR,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_HEADER =
                                    new StatusDetailCodeType(_SSS_INVALID_HEADER,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_HOST_CERT =
                                    new StatusDetailCodeType(_SSS_INVALID_HOST_CERT,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_LIST_COLUMN_NAME =
                                    new StatusDetailCodeType(_SSS_INVALID_LIST_COLUMN_NAME,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_LOCK_WAIT_TIME =
                                    new StatusDetailCodeType(_SSS_INVALID_LOCK_WAIT_TIME,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_LOG_TYPE =
                                    new StatusDetailCodeType(_SSS_INVALID_LOG_TYPE,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_PORTLET_INTERVAL =
                                    new StatusDetailCodeType(_SSS_INVALID_PORTLET_INTERVAL,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_SCRIPTLET_ID =
                                    new StatusDetailCodeType(_SSS_INVALID_SCRIPTLET_ID,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_SRCH_COL =
                                    new StatusDetailCodeType(_SSS_INVALID_SRCH_COL,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_SRCH_COLUMN_JOIN =
                                    new StatusDetailCodeType(_SSS_INVALID_SRCH_COLUMN_JOIN,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_SRCH_COLUMN_SUM =
                                    new StatusDetailCodeType(_SSS_INVALID_SRCH_COLUMN_SUM,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_SRCH_FILTER =
                                    new StatusDetailCodeType(_SSS_INVALID_SRCH_FILTER,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_SRCH_FILTER_JOIN =
                                    new StatusDetailCodeType(_SSS_INVALID_SRCH_FILTER_JOIN,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_SRCH_OPERATOR =
                                    new StatusDetailCodeType(_SSS_INVALID_SRCH_OPERATOR,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_SUBLIST_OPERATION =
                                    new StatusDetailCodeType(_SSS_INVALID_SUBLIST_OPERATION,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_SUBMIT_OPTION =
                                    new StatusDetailCodeType(_SSS_INVALID_SUBMIT_OPTION,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_TYPE_ARG =
                                    new StatusDetailCodeType(_SSS_INVALID_TYPE_ARG,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_UI_OBJECT_TYPE =
                                    new StatusDetailCodeType(_SSS_INVALID_UI_OBJECT_TYPE,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_URL =
                                    new StatusDetailCodeType(_SSS_INVALID_URL,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_URL_CATEGORY =
                                    new StatusDetailCodeType(_SSS_INVALID_URL_CATEGORY,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_WF_RCRD_TYPE =
                                    new StatusDetailCodeType(_SSS_INVALID_WF_RCRD_TYPE,true);
                            
                                public static final StatusDetailCodeType SSS_INVALID_XML_SCHEMA_OR_DEPENDENCY =
                                    new StatusDetailCodeType(_SSS_INVALID_XML_SCHEMA_OR_DEPENDENCY,true);
                            
                                public static final StatusDetailCodeType SSS_MEMORY_USAGE_EXCEEDED =
                                    new StatusDetailCodeType(_SSS_MEMORY_USAGE_EXCEEDED,true);
                            
                                public static final StatusDetailCodeType SSS_METHOD_NOT_IMPLEMENTED =
                                    new StatusDetailCodeType(_SSS_METHOD_NOT_IMPLEMENTED,true);
                            
                                public static final StatusDetailCodeType SSS_MISSING_REQD_ARGUMENT =
                                    new StatusDetailCodeType(_SSS_MISSING_REQD_ARGUMENT,true);
                            
                                public static final StatusDetailCodeType SSS_QUEUE_LIMIT_EXCEEDED =
                                    new StatusDetailCodeType(_SSS_QUEUE_LIMIT_EXCEEDED,true);
                            
                                public static final StatusDetailCodeType SSS_RECORD_TYPE_MISMATCH =
                                    new StatusDetailCodeType(_SSS_RECORD_TYPE_MISMATCH,true);
                            
                                public static final StatusDetailCodeType SSS_REQUEST_LIMIT_EXCEEDED =
                                    new StatusDetailCodeType(_SSS_REQUEST_LIMIT_EXCEEDED,true);
                            
                                public static final StatusDetailCodeType SSS_REQUEST_TIME_EXCEEDED =
                                    new StatusDetailCodeType(_SSS_REQUEST_TIME_EXCEEDED,true);
                            
                                public static final StatusDetailCodeType SSS_SCRIPT_DESERIALIZATION_FAILURE =
                                    new StatusDetailCodeType(_SSS_SCRIPT_DESERIALIZATION_FAILURE,true);
                            
                                public static final StatusDetailCodeType SSS_SCRIPT_SECURITY_VIOLATION =
                                    new StatusDetailCodeType(_SSS_SCRIPT_SECURITY_VIOLATION,true);
                            
                                public static final StatusDetailCodeType SSS_SSO_CONFIG_REQD =
                                    new StatusDetailCodeType(_SSS_SSO_CONFIG_REQD,true);
                            
                                public static final StatusDetailCodeType SSS_STACK_FRAME_DEPTH_EXCEEDED =
                                    new StatusDetailCodeType(_SSS_STACK_FRAME_DEPTH_EXCEEDED,true);
                            
                                public static final StatusDetailCodeType SSS_TIME_LIMIT_EXCEEDED =
                                    new StatusDetailCodeType(_SSS_TIME_LIMIT_EXCEEDED,true);
                            
                                public static final StatusDetailCodeType SSS_TRANSACTION_REQD =
                                    new StatusDetailCodeType(_SSS_TRANSACTION_REQD,true);
                            
                                public static final StatusDetailCodeType SSS_TRANS_IN_PROGRESS =
                                    new StatusDetailCodeType(_SSS_TRANS_IN_PROGRESS,true);
                            
                                public static final StatusDetailCodeType SSS_UNKNOWN_HOST =
                                    new StatusDetailCodeType(_SSS_UNKNOWN_HOST,true);
                            
                                public static final StatusDetailCodeType SSS_USAGE_LIMIT_EXCEEDED =
                                    new StatusDetailCodeType(_SSS_USAGE_LIMIT_EXCEEDED,true);
                            
                                public static final StatusDetailCodeType SSS_XML_DOES_NOT_CONFORM_TO_SCHEMA =
                                    new StatusDetailCodeType(_SSS_XML_DOES_NOT_CONFORM_TO_SCHEMA,true);
                            
                                public static final StatusDetailCodeType SSS_XML_DOM_EXCEPTION =
                                    new StatusDetailCodeType(_SSS_XML_DOM_EXCEPTION,true);
                            
                                public static final StatusDetailCodeType START_DATE_AFTER_END_DATE =
                                    new StatusDetailCodeType(_START_DATE_AFTER_END_DATE,true);
                            
                                public static final StatusDetailCodeType START_DATE_REQD =
                                    new StatusDetailCodeType(_START_DATE_REQD,true);
                            
                                public static final StatusDetailCodeType STATE_ALREADY_EXISTS =
                                    new StatusDetailCodeType(_STATE_ALREADY_EXISTS,true);
                            
                                public static final StatusDetailCodeType STATE_REQD =
                                    new StatusDetailCodeType(_STATE_REQD,true);
                            
                                public static final StatusDetailCodeType STATUS_ASSIGNEE_REQD =
                                    new StatusDetailCodeType(_STATUS_ASSIGNEE_REQD,true);
                            
                                public static final StatusDetailCodeType STORAGE_LIMIT_EXCEEDED =
                                    new StatusDetailCodeType(_STORAGE_LIMIT_EXCEEDED,true);
                            
                                public static final StatusDetailCodeType STORE_ALIAS_UNAVAILABLE =
                                    new StatusDetailCodeType(_STORE_ALIAS_UNAVAILABLE,true);
                            
                                public static final StatusDetailCodeType STORE_DOMAIN_UNAVAILABLE =
                                    new StatusDetailCodeType(_STORE_DOMAIN_UNAVAILABLE,true);
                            
                                public static final StatusDetailCodeType SUBITEM_REQD =
                                    new StatusDetailCodeType(_SUBITEM_REQD,true);
                            
                                public static final StatusDetailCodeType SUBSIDIARY_MISMATCH =
                                    new StatusDetailCodeType(_SUBSIDIARY_MISMATCH,true);
                            
                                public static final StatusDetailCodeType SUB_MISMATCH =
                                    new StatusDetailCodeType(_SUB_MISMATCH,true);
                            
                                public static final StatusDetailCodeType SUB_RESTRICT_VIEW_REQD =
                                    new StatusDetailCodeType(_SUB_RESTRICT_VIEW_REQD,true);
                            
                                public static final StatusDetailCodeType SUCCESS_TRANS =
                                    new StatusDetailCodeType(_SUCCESS_TRANS,true);
                            
                                public static final StatusDetailCodeType SUPRT_CNTR_LOGIN_ERROR =
                                    new StatusDetailCodeType(_SUPRT_CNTR_LOGIN_ERROR,true);
                            
                                public static final StatusDetailCodeType TAGATA_ALREADY_ENDORSED =
                                    new StatusDetailCodeType(_TAGATA_ALREADY_ENDORSED,true);
                            
                                public static final StatusDetailCodeType TAG_ALREADY_EXISTS =
                                    new StatusDetailCodeType(_TAG_ALREADY_EXISTS,true);
                            
                                public static final StatusDetailCodeType TAG_SUBSTITUTN_ERROR =
                                    new StatusDetailCodeType(_TAG_SUBSTITUTN_ERROR,true);
                            
                                public static final StatusDetailCodeType TAX_ACCT_SETUP_REQD =
                                    new StatusDetailCodeType(_TAX_ACCT_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType TAX_CODES_SETUP_PROBLEM =
                                    new StatusDetailCodeType(_TAX_CODES_SETUP_PROBLEM,true);
                            
                                public static final StatusDetailCodeType TAX_CODES_SETUP_REQD =
                                    new StatusDetailCodeType(_TAX_CODES_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType TAX_CODE_REQD =
                                    new StatusDetailCodeType(_TAX_CODE_REQD,true);
                            
                                public static final StatusDetailCodeType TAX_GROUP_SETUP_REQD =
                                    new StatusDetailCodeType(_TAX_GROUP_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType TAX_PRD_REQD =
                                    new StatusDetailCodeType(_TAX_PRD_REQD,true);
                            
                                public static final StatusDetailCodeType TAX_SETUP_REQD =
                                    new StatusDetailCodeType(_TAX_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType TEMPLATE_NOT_FOUND =
                                    new StatusDetailCodeType(_TEMPLATE_NOT_FOUND,true);
                            
                                public static final StatusDetailCodeType THIRD_PARTY_BILLING_ACCT_REQD =
                                    new StatusDetailCodeType(_THIRD_PARTY_BILLING_ACCT_REQD,true);
                            
                                public static final StatusDetailCodeType TICKET_NOT_LOCATED =
                                    new StatusDetailCodeType(_TICKET_NOT_LOCATED,true);
                            
                                public static final StatusDetailCodeType TIMEOUT_THE_RECORD_DOESNT_EXIST_ANYMORE =
                                    new StatusDetailCodeType(_TIMEOUT_THE_RECORD_DOESNT_EXIST_ANYMORE,true);
                            
                                public static final StatusDetailCodeType TIME_ENTRY_DISALLWD =
                                    new StatusDetailCodeType(_TIME_ENTRY_DISALLWD,true);
                            
                                public static final StatusDetailCodeType TOPIC_REQD =
                                    new StatusDetailCodeType(_TOPIC_REQD,true);
                            
                                public static final StatusDetailCodeType TRANSACTION_DELETED =
                                    new StatusDetailCodeType(_TRANSACTION_DELETED,true);
                            
                                public static final StatusDetailCodeType TRANSORD_SHIP_REC_MISMATCH =
                                    new StatusDetailCodeType(_TRANSORD_SHIP_REC_MISMATCH,true);
                            
                                public static final StatusDetailCodeType TRANS_ALREADY_REFUNDED =
                                    new StatusDetailCodeType(_TRANS_ALREADY_REFUNDED,true);
                            
                                public static final StatusDetailCodeType TRANS_ALREADY_SETTLED =
                                    new StatusDetailCodeType(_TRANS_ALREADY_SETTLED,true);
                            
                                public static final StatusDetailCodeType TRANS_ALREADY_VOIDED =
                                    new StatusDetailCodeType(_TRANS_ALREADY_VOIDED,true);
                            
                                public static final StatusDetailCodeType TRANS_AMTS_UNBALNCD =
                                    new StatusDetailCodeType(_TRANS_AMTS_UNBALNCD,true);
                            
                                public static final StatusDetailCodeType TRANS_APPLIED_AMTS_UNBALNCD =
                                    new StatusDetailCodeType(_TRANS_APPLIED_AMTS_UNBALNCD,true);
                            
                                public static final StatusDetailCodeType TRANS_CLASS_UNBALNCD =
                                    new StatusDetailCodeType(_TRANS_CLASS_UNBALNCD,true);
                            
                                public static final StatusDetailCodeType TRANS_DEPT_UNBALNCD =
                                    new StatusDetailCodeType(_TRANS_DEPT_UNBALNCD,true);
                            
                                public static final StatusDetailCodeType TRANS_DOES_NOT_EXIST =
                                    new StatusDetailCodeType(_TRANS_DOES_NOT_EXIST,true);
                            
                                public static final StatusDetailCodeType TRANS_DSNT_EXIST =
                                    new StatusDetailCodeType(_TRANS_DSNT_EXIST,true);
                            
                                public static final StatusDetailCodeType TRANS_EDIT_DISALLWD =
                                    new StatusDetailCodeType(_TRANS_EDIT_DISALLWD,true);
                            
                                public static final StatusDetailCodeType TRANS_FORGN_CRNCY_MISMATCH =
                                    new StatusDetailCodeType(_TRANS_FORGN_CRNCY_MISMATCH,true);
                            
                                public static final StatusDetailCodeType TRANS_FORGN_CUR_UNBALNCD =
                                    new StatusDetailCodeType(_TRANS_FORGN_CUR_UNBALNCD,true);
                            
                                public static final StatusDetailCodeType TRANS_IN_USE =
                                    new StatusDetailCodeType(_TRANS_IN_USE,true);
                            
                                public static final StatusDetailCodeType TRANS_LINES_UNBALNCD =
                                    new StatusDetailCodeType(_TRANS_LINES_UNBALNCD,true);
                            
                                public static final StatusDetailCodeType TRANS_LINE_AND_PMT_UNBALNCD =
                                    new StatusDetailCodeType(_TRANS_LINE_AND_PMT_UNBALNCD,true);
                            
                                public static final StatusDetailCodeType TRANS_LOC_UNBALNCD =
                                    new StatusDetailCodeType(_TRANS_LOC_UNBALNCD,true);
                            
                                public static final StatusDetailCodeType TRANS_NOT_CLEANED =
                                    new StatusDetailCodeType(_TRANS_NOT_CLEANED,true);
                            
                                public static final StatusDetailCodeType TRANS_NOT_COMPLETED =
                                    new StatusDetailCodeType(_TRANS_NOT_COMPLETED,true);
                            
                                public static final StatusDetailCodeType TRANS_PRCSSNG_ERROR =
                                    new StatusDetailCodeType(_TRANS_PRCSSNG_ERROR,true);
                            
                                public static final StatusDetailCodeType TRANS_UNBALNCD =
                                    new StatusDetailCodeType(_TRANS_UNBALNCD,true);
                            
                                public static final StatusDetailCodeType TRAN_DATE_REQD =
                                    new StatusDetailCodeType(_TRAN_DATE_REQD,true);
                            
                                public static final StatusDetailCodeType TRAN_LINE_FX_AMT_REQD =
                                    new StatusDetailCodeType(_TRAN_LINE_FX_AMT_REQD,true);
                            
                                public static final StatusDetailCodeType TRAN_LINK_FX_AMT_REQD =
                                    new StatusDetailCodeType(_TRAN_LINK_FX_AMT_REQD,true);
                            
                                public static final StatusDetailCodeType TRAN_PERIOD_CLOSED =
                                    new StatusDetailCodeType(_TRAN_PERIOD_CLOSED,true);
                            
                                public static final StatusDetailCodeType TRAN_PRD_CLOSED =
                                    new StatusDetailCodeType(_TRAN_PRD_CLOSED,true);
                            
                                public static final StatusDetailCodeType TWO_FA_AUTH_REQD =
                                    new StatusDetailCodeType(_TWO_FA_AUTH_REQD,true);
                            
                                public static final StatusDetailCodeType TWO_FA_REQD =
                                    new StatusDetailCodeType(_TWO_FA_REQD,true);
                            
                                public static final StatusDetailCodeType UNABLE_TO_PRINT_CHECKS =
                                    new StatusDetailCodeType(_UNABLE_TO_PRINT_CHECKS,true);
                            
                                public static final StatusDetailCodeType UNABLE_TO_PRINT_DEPOSITS =
                                    new StatusDetailCodeType(_UNABLE_TO_PRINT_DEPOSITS,true);
                            
                                public static final StatusDetailCodeType UNAUTH_CAMPAIGN_RSPNS_RQST =
                                    new StatusDetailCodeType(_UNAUTH_CAMPAIGN_RSPNS_RQST,true);
                            
                                public static final StatusDetailCodeType UNAUTH_UNSUBSCRIBE_RQST =
                                    new StatusDetailCodeType(_UNAUTH_UNSUBSCRIBE_RQST,true);
                            
                                public static final StatusDetailCodeType UNDEFINED_ACCTNG_PRD =
                                    new StatusDetailCodeType(_UNDEFINED_ACCTNG_PRD,true);
                            
                                public static final StatusDetailCodeType UNDEFINED_CSTM_FIELD =
                                    new StatusDetailCodeType(_UNDEFINED_CSTM_FIELD,true);
                            
                                public static final StatusDetailCodeType UNDEFINED_TAX_PRD =
                                    new StatusDetailCodeType(_UNDEFINED_TAX_PRD,true);
                            
                                public static final StatusDetailCodeType UNEXPECTED_ERROR =
                                    new StatusDetailCodeType(_UNEXPECTED_ERROR,true);
                            
                                public static final StatusDetailCodeType UNIQUE_CONTACT_NAME_REQD =
                                    new StatusDetailCodeType(_UNIQUE_CONTACT_NAME_REQD,true);
                            
                                public static final StatusDetailCodeType UNIQUE_CUST_EMAIL_REQD =
                                    new StatusDetailCodeType(_UNIQUE_CUST_EMAIL_REQD,true);
                            
                                public static final StatusDetailCodeType UNIQUE_CUST_ID_REQD =
                                    new StatusDetailCodeType(_UNIQUE_CUST_ID_REQD,true);
                            
                                public static final StatusDetailCodeType UNIQUE_ENTITY_NAME_REQD =
                                    new StatusDetailCodeType(_UNIQUE_ENTITY_NAME_REQD,true);
                            
                                public static final StatusDetailCodeType UNIQUE_GROUPID_REQD =
                                    new StatusDetailCodeType(_UNIQUE_GROUPID_REQD,true);
                            
                                public static final StatusDetailCodeType UNIQUE_PARTNER_CODE_REQD =
                                    new StatusDetailCodeType(_UNIQUE_PARTNER_CODE_REQD,true);
                            
                                public static final StatusDetailCodeType UNIQUE_QTY_REQD =
                                    new StatusDetailCodeType(_UNIQUE_QTY_REQD,true);
                            
                                public static final StatusDetailCodeType UNIQUE_RCRD_ID_REQD =
                                    new StatusDetailCodeType(_UNIQUE_RCRD_ID_REQD,true);
                            
                                public static final StatusDetailCodeType UNIQUE_SOLUTION_CODE_REQD =
                                    new StatusDetailCodeType(_UNIQUE_SOLUTION_CODE_REQD,true);
                            
                                public static final StatusDetailCodeType UNITS_TYP_IN_USE =
                                    new StatusDetailCodeType(_UNITS_TYP_IN_USE,true);
                            
                                public static final StatusDetailCodeType UNKNOWN_CARRIER =
                                    new StatusDetailCodeType(_UNKNOWN_CARRIER,true);
                            
                                public static final StatusDetailCodeType UNKNOWN_RCRD_TYPE =
                                    new StatusDetailCodeType(_UNKNOWN_RCRD_TYPE,true);
                            
                                public static final StatusDetailCodeType UNKNOWN_SCRIPT_TYP =
                                    new StatusDetailCodeType(_UNKNOWN_SCRIPT_TYP,true);
                            
                                public static final StatusDetailCodeType UNKNWN_ALLOCTN_SCHDUL_FREQ_TYP =
                                    new StatusDetailCodeType(_UNKNWN_ALLOCTN_SCHDUL_FREQ_TYP,true);
                            
                                public static final StatusDetailCodeType UNKNWN_EMAIL_AUTHOR =
                                    new StatusDetailCodeType(_UNKNWN_EMAIL_AUTHOR,true);
                            
                                public static final StatusDetailCodeType UNKNWN_EXCHANGE_RATE =
                                    new StatusDetailCodeType(_UNKNWN_EXCHANGE_RATE,true);
                            
                                public static final StatusDetailCodeType UNRECOGNIZED_METHOD =
                                    new StatusDetailCodeType(_UNRECOGNIZED_METHOD,true);
                            
                                public static final StatusDetailCodeType UNSUBSCRIBE_REQD =
                                    new StatusDetailCodeType(_UNSUBSCRIBE_REQD,true);
                            
                                public static final StatusDetailCodeType UNSUPPORTED_METHOD =
                                    new StatusDetailCodeType(_UNSUPPORTED_METHOD,true);
                            
                                public static final StatusDetailCodeType UNSUPPORTED_WS_VERSION =
                                    new StatusDetailCodeType(_UNSUPPORTED_WS_VERSION,true);
                            
                                public static final StatusDetailCodeType UNSUPRTD_DOC_TYP =
                                    new StatusDetailCodeType(_UNSUPRTD_DOC_TYP,true);
                            
                                public static final StatusDetailCodeType UPDATE_DISALLWD =
                                    new StatusDetailCodeType(_UPDATE_DISALLWD,true);
                            
                                public static final StatusDetailCodeType UPDATE_PRICE_AMT_REQD =
                                    new StatusDetailCodeType(_UPDATE_PRICE_AMT_REQD,true);
                            
                                public static final StatusDetailCodeType UPGRADE_WS_VERSION =
                                    new StatusDetailCodeType(_UPGRADE_WS_VERSION,true);
                            
                                public static final StatusDetailCodeType UPS_CANT_INTEGRATE_FULFILL =
                                    new StatusDetailCodeType(_UPS_CANT_INTEGRATE_FULFILL,true);
                            
                                public static final StatusDetailCodeType UPS_CONFIG_ERROR =
                                    new StatusDetailCodeType(_UPS_CONFIG_ERROR,true);
                            
                                public static final StatusDetailCodeType UPS_LICENSE_AGREEMNT_REQD =
                                    new StatusDetailCodeType(_UPS_LICENSE_AGREEMNT_REQD,true);
                            
                                public static final StatusDetailCodeType UPS_ONLINE_RATE_UNAVAILBL =
                                    new StatusDetailCodeType(_UPS_ONLINE_RATE_UNAVAILBL,true);
                            
                                public static final StatusDetailCodeType UPS_ONLINE_SHIP_UNAVAILBL =
                                    new StatusDetailCodeType(_UPS_ONLINE_SHIP_UNAVAILBL,true);
                            
                                public static final StatusDetailCodeType UPS_REG_NUM_IN_USE =
                                    new StatusDetailCodeType(_UPS_REG_NUM_IN_USE,true);
                            
                                public static final StatusDetailCodeType UPS_SETUP_REQD =
                                    new StatusDetailCodeType(_UPS_SETUP_REQD,true);
                            
                                public static final StatusDetailCodeType UPS_USER_ERROR =
                                    new StatusDetailCodeType(_UPS_USER_ERROR,true);
                            
                                public static final StatusDetailCodeType UPS_VOID_ERROR =
                                    new StatusDetailCodeType(_UPS_VOID_ERROR,true);
                            
                                public static final StatusDetailCodeType UPS_XML_ERROR =
                                    new StatusDetailCodeType(_UPS_XML_ERROR,true);
                            
                                public static final StatusDetailCodeType URL_ID_PARAM_REQD =
                                    new StatusDetailCodeType(_URL_ID_PARAM_REQD,true);
                            
                                public static final StatusDetailCodeType URL_REQD =
                                    new StatusDetailCodeType(_URL_REQD,true);
                            
                                public static final StatusDetailCodeType USER_CONTEXT_REQD =
                                    new StatusDetailCodeType(_USER_CONTEXT_REQD,true);
                            
                                public static final StatusDetailCodeType USER_DISABLED =
                                    new StatusDetailCodeType(_USER_DISABLED,true);
                            
                                public static final StatusDetailCodeType USER_ERROR =
                                    new StatusDetailCodeType(_USER_ERROR,true);
                            
                                public static final StatusDetailCodeType USPS_ACCT_NUM_ALREADY_EXISTS =
                                    new StatusDetailCodeType(_USPS_ACCT_NUM_ALREADY_EXISTS,true);
                            
                                public static final StatusDetailCodeType USPS_INVALID_INSURED_VALUE =
                                    new StatusDetailCodeType(_USPS_INVALID_INSURED_VALUE,true);
                            
                                public static final StatusDetailCodeType USPS_INVALID_PACKAGING =
                                    new StatusDetailCodeType(_USPS_INVALID_PACKAGING,true);
                            
                                public static final StatusDetailCodeType USPS_INVALID_PSWD =
                                    new StatusDetailCodeType(_USPS_INVALID_PSWD,true);
                            
                                public static final StatusDetailCodeType USPS_LABEL_VOIDED =
                                    new StatusDetailCodeType(_USPS_LABEL_VOIDED,true);
                            
                                public static final StatusDetailCodeType USPS_MAX_ITEM_EXCEEDED =
                                    new StatusDetailCodeType(_USPS_MAX_ITEM_EXCEEDED,true);
                            
                                public static final StatusDetailCodeType USPS_ONE_PACKAGE_ALLWD =
                                    new StatusDetailCodeType(_USPS_ONE_PACKAGE_ALLWD,true);
                            
                                public static final StatusDetailCodeType USPS_PASS_PHRASE_NOT_UPDATED =
                                    new StatusDetailCodeType(_USPS_PASS_PHRASE_NOT_UPDATED,true);
                            
                                public static final StatusDetailCodeType USPS_REFUND_FAILED =
                                    new StatusDetailCodeType(_USPS_REFUND_FAILED,true);
                            
                                public static final StatusDetailCodeType USPS_RETRY =
                                    new StatusDetailCodeType(_USPS_RETRY,true);
                            
                                public static final StatusDetailCodeType USPS_VALIDATE_ADDR =
                                    new StatusDetailCodeType(_USPS_VALIDATE_ADDR,true);
                            
                                public static final StatusDetailCodeType USPS_VERIFY_TRACKING_NUM =
                                    new StatusDetailCodeType(_USPS_VERIFY_TRACKING_NUM,true);
                            
                                public static final StatusDetailCodeType USPS_VOID_ERROR =
                                    new StatusDetailCodeType(_USPS_VOID_ERROR,true);
                            
                                public static final StatusDetailCodeType VALID_EMAIL_REQD =
                                    new StatusDetailCodeType(_VALID_EMAIL_REQD,true);
                            
                                public static final StatusDetailCodeType VALID_EMAIL_REQD_FOR_LOGIN =
                                    new StatusDetailCodeType(_VALID_EMAIL_REQD_FOR_LOGIN,true);
                            
                                public static final StatusDetailCodeType VALID_FIRST_NAME_REQD =
                                    new StatusDetailCodeType(_VALID_FIRST_NAME_REQD,true);
                            
                                public static final StatusDetailCodeType VALID_LAST_NAME_REQD =
                                    new StatusDetailCodeType(_VALID_LAST_NAME_REQD,true);
                            
                                public static final StatusDetailCodeType VALID_LINE_ITEM_REQD =
                                    new StatusDetailCodeType(_VALID_LINE_ITEM_REQD,true);
                            
                                public static final StatusDetailCodeType VALID_PHONE_NUM_REQD =
                                    new StatusDetailCodeType(_VALID_PHONE_NUM_REQD,true);
                            
                                public static final StatusDetailCodeType VALID_PRD_REQD =
                                    new StatusDetailCodeType(_VALID_PRD_REQD,true);
                            
                                public static final StatusDetailCodeType VALID_URL_REQD =
                                    new StatusDetailCodeType(_VALID_URL_REQD,true);
                            
                                public static final StatusDetailCodeType VALID_VERSION_REQD_IN_URL =
                                    new StatusDetailCodeType(_VALID_VERSION_REQD_IN_URL,true);
                            
                                public static final StatusDetailCodeType VALID_WORK_PHONE_REQD =
                                    new StatusDetailCodeType(_VALID_WORK_PHONE_REQD,true);
                            
                                public static final StatusDetailCodeType VALID_ZIPCODE_REQD =
                                    new StatusDetailCodeType(_VALID_ZIPCODE_REQD,true);
                            
                                public static final StatusDetailCodeType VENDOR_TYPE_REQD =
                                    new StatusDetailCodeType(_VENDOR_TYPE_REQD,true);
                            
                                public static final StatusDetailCodeType VERIFY_DESTNTN_ZIP_CODE =
                                    new StatusDetailCodeType(_VERIFY_DESTNTN_ZIP_CODE,true);
                            
                                public static final StatusDetailCodeType VERIFY_PAYROLL_FUND_ACCT =
                                    new StatusDetailCodeType(_VERIFY_PAYROLL_FUND_ACCT,true);
                            
                                public static final StatusDetailCodeType VERIFY_ZIP_CODE_SETUP =
                                    new StatusDetailCodeType(_VERIFY_ZIP_CODE_SETUP,true);
                            
                                public static final StatusDetailCodeType VISA_ERROR =
                                    new StatusDetailCodeType(_VISA_ERROR,true);
                            
                                public static final StatusDetailCodeType VOIDING_REVERSAL_DISALLWD =
                                    new StatusDetailCodeType(_VOIDING_REVERSAL_DISALLWD,true);
                            
                                public static final StatusDetailCodeType VOID_FAILED =
                                    new StatusDetailCodeType(_VOID_FAILED,true);
                            
                                public static final StatusDetailCodeType VSOE_CANT_ADD_ITEM_GROUP =
                                    new StatusDetailCodeType(_VSOE_CANT_ADD_ITEM_GROUP,true);
                            
                                public static final StatusDetailCodeType VSOE_REV_REC_TMPLT_REQD =
                                    new StatusDetailCodeType(_VSOE_REV_REC_TMPLT_REQD,true);
                            
                                public static final StatusDetailCodeType VSOE_TOTAL_ALLOCATION_ERROR =
                                    new StatusDetailCodeType(_VSOE_TOTAL_ALLOCATION_ERROR,true);
                            
                                public static final StatusDetailCodeType VSOE_TRAN_VSOE_BUNDLE_ERROR =
                                    new StatusDetailCodeType(_VSOE_TRAN_VSOE_BUNDLE_ERROR,true);
                            
                                public static final StatusDetailCodeType WARNING =
                                    new StatusDetailCodeType(_WARNING,true);
                            
                                public static final StatusDetailCodeType WF_EXEC_USAGE_LIMIT_EXCEEDED =
                                    new StatusDetailCodeType(_WF_EXEC_USAGE_LIMIT_EXCEEDED,true);
                            
                                public static final StatusDetailCodeType WORK_DAYS_REQD =
                                    new StatusDetailCodeType(_WORK_DAYS_REQD,true);
                            
                                public static final StatusDetailCodeType WORLDPAY_ERROR =
                                    new StatusDetailCodeType(_WORLDPAY_ERROR,true);
                            
                                public static final StatusDetailCodeType WRITE_OFF_ACCT_REQD =
                                    new StatusDetailCodeType(_WRITE_OFF_ACCT_REQD,true);
                            
                                public static final StatusDetailCodeType WS_CONCUR_SESSION_DISALLWD =
                                    new StatusDetailCodeType(_WS_CONCUR_SESSION_DISALLWD,true);
                            
                                public static final StatusDetailCodeType WS_EXCEEDED_CONCUR_USERS_ALLWD =
                                    new StatusDetailCodeType(_WS_EXCEEDED_CONCUR_USERS_ALLWD,true);
                            
                                public static final StatusDetailCodeType WS_EXCEEDED_MAX_CONCUR_RQST =
                                    new StatusDetailCodeType(_WS_EXCEEDED_MAX_CONCUR_RQST,true);
                            
                                public static final StatusDetailCodeType WS_FEATURE_REQD =
                                    new StatusDetailCodeType(_WS_FEATURE_REQD,true);
                            
                                public static final StatusDetailCodeType WS_INVALID_SEARCH_OPERATN =
                                    new StatusDetailCodeType(_WS_INVALID_SEARCH_OPERATN,true);
                            
                                public static final StatusDetailCodeType WS_LOG_IN_REQD =
                                    new StatusDetailCodeType(_WS_LOG_IN_REQD,true);
                            
                                public static final StatusDetailCodeType WS_PERMISSION_REQD =
                                    new StatusDetailCodeType(_WS_PERMISSION_REQD,true);
                            
                                public static final StatusDetailCodeType WS_REQUEST_BLOCKED =
                                    new StatusDetailCodeType(_WS_REQUEST_BLOCKED,true);
                            
                                public static final StatusDetailCodeType ZIP_FILE_CONTAINS_VIRUS =
                                    new StatusDetailCodeType(_ZIP_FILE_CONTAINS_VIRUS,true);
                            

                                public java.lang.String getValue() { return localStatusDetailCodeType;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localStatusDetailCodeType.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.faults_2017_2.platform.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":StatusDetailCodeType",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "StatusDetailCodeType",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localStatusDetailCodeType==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("StatusDetailCodeType cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localStatusDetailCodeType);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.faults_2017_2.platform.webservices.netsuite.com")){
                return "ns2";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStatusDetailCodeType)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static StatusDetailCodeType fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    StatusDetailCodeType enumeration = (StatusDetailCodeType)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static StatusDetailCodeType fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static StatusDetailCodeType fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return StatusDetailCodeType.Factory.fromString(content,namespaceUri);
                    } else {
                       return StatusDetailCodeType.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static StatusDetailCodeType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            StatusDetailCodeType object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"StatusDetailCodeType" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = StatusDetailCodeType.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = StatusDetailCodeType.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    