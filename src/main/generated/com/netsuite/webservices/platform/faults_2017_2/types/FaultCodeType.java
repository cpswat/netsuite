
/**
 * FaultCodeType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.platform.faults_2017_2.types;
            

            /**
            *  FaultCodeType bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class FaultCodeType
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.faults_2017_2.platform.webservices.netsuite.com",
                "FaultCodeType",
                "ns2");

            

                        /**
                        * field for FaultCodeType
                        */

                        
                                    protected java.lang.String localFaultCodeType ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected FaultCodeType(java.lang.String value, boolean isRegisterValue) {
                                    localFaultCodeType = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localFaultCodeType, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String _ACCT_TEMP_UNAVAILABLE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ACCT_TEMP_UNAVAILABLE");
                                
                                    public static final java.lang.String _EMAIL_ADDRS_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("EMAIL_ADDRS_REQD");
                                
                                    public static final java.lang.String _INSUFFICIENT_PERMISSION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INSUFFICIENT_PERMISSION");
                                
                                    public static final java.lang.String _INVALID_ACCT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ACCT");
                                
                                    public static final java.lang.String _INVALID_JOB_ID =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_JOB_ID");
                                
                                    public static final java.lang.String _INVALID_LOGIN_CREDENTIALS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_LOGIN_CREDENTIALS");
                                
                                    public static final java.lang.String _INVALID_PAGE_INDEX =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_PAGE_INDEX");
                                
                                    public static final java.lang.String _INVALID_ROLE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_ROLE");
                                
                                    public static final java.lang.String _INVALID_WS_VERSION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("INVALID_WS_VERSION");
                                
                                    public static final java.lang.String _JOB_NOT_COMPLETE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("JOB_NOT_COMPLETE");
                                
                                    public static final java.lang.String _LOGIN_DISABLED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("LOGIN_DISABLED");
                                
                                    public static final java.lang.String _MAX_RCRDS_EXCEEDED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("MAX_RCRDS_EXCEEDED");
                                
                                    public static final java.lang.String _OI_FEATURE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("OI_FEATURE_REQD");
                                
                                    public static final java.lang.String _OI_PERMISSION_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("OI_PERMISSION_REQD");
                                
                                    public static final java.lang.String _PSWD_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("PSWD_REQD");
                                
                                    public static final java.lang.String _ROLE_REQUIRED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("ROLE_REQUIRED");
                                
                                    public static final java.lang.String _SESSION_TIMED_OUT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("SESSION_TIMED_OUT");
                                
                                    public static final java.lang.String _UNEXPECTED_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNEXPECTED_ERROR");
                                
                                    public static final java.lang.String _UNSUPPORTED_WS_VERSION =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("UNSUPPORTED_WS_VERSION");
                                
                                    public static final java.lang.String _USER_ERROR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("USER_ERROR");
                                
                                    public static final java.lang.String _WS_CONCUR_SESSION_DISALLWD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("WS_CONCUR_SESSION_DISALLWD");
                                
                                    public static final java.lang.String _WS_FEATURE_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("WS_FEATURE_REQD");
                                
                                    public static final java.lang.String _WS_PERMISSION_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("WS_PERMISSION_REQD");
                                
                                    public static final java.lang.String _WS_LOG_IN_REQD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("WS_LOG_IN_REQD");
                                
                                    public static final java.lang.String _WS_REQUEST_BLOCKED =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("WS_REQUEST_BLOCKED");
                                
                                public static final FaultCodeType ACCT_TEMP_UNAVAILABLE =
                                    new FaultCodeType(_ACCT_TEMP_UNAVAILABLE,true);
                            
                                public static final FaultCodeType EMAIL_ADDRS_REQD =
                                    new FaultCodeType(_EMAIL_ADDRS_REQD,true);
                            
                                public static final FaultCodeType INSUFFICIENT_PERMISSION =
                                    new FaultCodeType(_INSUFFICIENT_PERMISSION,true);
                            
                                public static final FaultCodeType INVALID_ACCT =
                                    new FaultCodeType(_INVALID_ACCT,true);
                            
                                public static final FaultCodeType INVALID_JOB_ID =
                                    new FaultCodeType(_INVALID_JOB_ID,true);
                            
                                public static final FaultCodeType INVALID_LOGIN_CREDENTIALS =
                                    new FaultCodeType(_INVALID_LOGIN_CREDENTIALS,true);
                            
                                public static final FaultCodeType INVALID_PAGE_INDEX =
                                    new FaultCodeType(_INVALID_PAGE_INDEX,true);
                            
                                public static final FaultCodeType INVALID_ROLE =
                                    new FaultCodeType(_INVALID_ROLE,true);
                            
                                public static final FaultCodeType INVALID_WS_VERSION =
                                    new FaultCodeType(_INVALID_WS_VERSION,true);
                            
                                public static final FaultCodeType JOB_NOT_COMPLETE =
                                    new FaultCodeType(_JOB_NOT_COMPLETE,true);
                            
                                public static final FaultCodeType LOGIN_DISABLED =
                                    new FaultCodeType(_LOGIN_DISABLED,true);
                            
                                public static final FaultCodeType MAX_RCRDS_EXCEEDED =
                                    new FaultCodeType(_MAX_RCRDS_EXCEEDED,true);
                            
                                public static final FaultCodeType OI_FEATURE_REQD =
                                    new FaultCodeType(_OI_FEATURE_REQD,true);
                            
                                public static final FaultCodeType OI_PERMISSION_REQD =
                                    new FaultCodeType(_OI_PERMISSION_REQD,true);
                            
                                public static final FaultCodeType PSWD_REQD =
                                    new FaultCodeType(_PSWD_REQD,true);
                            
                                public static final FaultCodeType ROLE_REQUIRED =
                                    new FaultCodeType(_ROLE_REQUIRED,true);
                            
                                public static final FaultCodeType SESSION_TIMED_OUT =
                                    new FaultCodeType(_SESSION_TIMED_OUT,true);
                            
                                public static final FaultCodeType UNEXPECTED_ERROR =
                                    new FaultCodeType(_UNEXPECTED_ERROR,true);
                            
                                public static final FaultCodeType UNSUPPORTED_WS_VERSION =
                                    new FaultCodeType(_UNSUPPORTED_WS_VERSION,true);
                            
                                public static final FaultCodeType USER_ERROR =
                                    new FaultCodeType(_USER_ERROR,true);
                            
                                public static final FaultCodeType WS_CONCUR_SESSION_DISALLWD =
                                    new FaultCodeType(_WS_CONCUR_SESSION_DISALLWD,true);
                            
                                public static final FaultCodeType WS_FEATURE_REQD =
                                    new FaultCodeType(_WS_FEATURE_REQD,true);
                            
                                public static final FaultCodeType WS_PERMISSION_REQD =
                                    new FaultCodeType(_WS_PERMISSION_REQD,true);
                            
                                public static final FaultCodeType WS_LOG_IN_REQD =
                                    new FaultCodeType(_WS_LOG_IN_REQD,true);
                            
                                public static final FaultCodeType WS_REQUEST_BLOCKED =
                                    new FaultCodeType(_WS_REQUEST_BLOCKED,true);
                            

                                public java.lang.String getValue() { return localFaultCodeType;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localFaultCodeType.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.faults_2017_2.platform.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":FaultCodeType",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "FaultCodeType",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localFaultCodeType==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("FaultCodeType cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localFaultCodeType);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.faults_2017_2.platform.webservices.netsuite.com")){
                return "ns2";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFaultCodeType)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static FaultCodeType fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    FaultCodeType enumeration = (FaultCodeType)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static FaultCodeType fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static FaultCodeType fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return FaultCodeType.Factory.fromString(content,namespaceUri);
                    } else {
                       return FaultCodeType.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static FaultCodeType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            FaultCodeType object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"FaultCodeType" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = FaultCodeType.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = FaultCodeType.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    