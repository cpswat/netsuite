
/**
 * MediaType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.documents.filecabinet_2017_2.types;
            

            /**
            *  MediaType bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class MediaType
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.filecabinet_2017_2.documents.webservices.netsuite.com",
                "MediaType",
                "ns11");

            

                        /**
                        * field for MediaType
                        */

                        
                                    protected java.lang.String localMediaType ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected MediaType(java.lang.String value, boolean isRegisterValue) {
                                    localMediaType = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localMediaType, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String __APPCACHE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_APPCACHE");
                                
                                    public static final java.lang.String __AUTOCAD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_AUTOCAD");
                                
                                    public static final java.lang.String __BMPIMAGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_BMPIMAGE");
                                
                                    public static final java.lang.String __CERTIFICATE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_CERTIFICATE");
                                
                                    public static final java.lang.String __CFF =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_CFF");
                                
                                    public static final java.lang.String __CONFIG =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_CONFIG");
                                
                                    public static final java.lang.String __CSV =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_CSV");
                                
                                    public static final java.lang.String __EOT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_EOT");
                                
                                    public static final java.lang.String __EXCEL =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_EXCEL");
                                
                                    public static final java.lang.String __FLASH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_FLASH");
                                
                                    public static final java.lang.String __FREEMARKER =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_FREEMARKER");
                                
                                    public static final java.lang.String __GIFIMAGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_GIFIMAGE");
                                
                                    public static final java.lang.String __GZIP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_GZIP");
                                
                                    public static final java.lang.String __HTMLDOC =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_HTMLDOC");
                                
                                    public static final java.lang.String __ICON =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ICON");
                                
                                    public static final java.lang.String __IMAGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_IMAGE");
                                
                                    public static final java.lang.String __JAVASCRIPT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_JAVASCRIPT");
                                
                                    public static final java.lang.String __JPGIMAGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_JPGIMAGE");
                                
                                    public static final java.lang.String __JSON =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_JSON");
                                
                                    public static final java.lang.String __LZH =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_LZH");
                                
                                    public static final java.lang.String __MESSAGERFC =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_MESSAGERFC");
                                
                                    public static final java.lang.String __MISCBINARY =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_MISCBINARY");
                                
                                    public static final java.lang.String __MISCTEXT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_MISCTEXT");
                                
                                    public static final java.lang.String __MP3 =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_MP3");
                                
                                    public static final java.lang.String __MPEGMOVIE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_MPEGMOVIE");
                                
                                    public static final java.lang.String __MSPROJECT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_MSPROJECT");
                                
                                    public static final java.lang.String __OTF =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_OTF");
                                
                                    public static final java.lang.String __PDF =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_PDF");
                                
                                    public static final java.lang.String __PJPGIMAGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_PJPGIMAGE");
                                
                                    public static final java.lang.String __PLAINTEXT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_PLAINTEXT");
                                
                                    public static final java.lang.String __PNGIMAGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_PNGIMAGE");
                                
                                    public static final java.lang.String __POSTSCRIPT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_POSTSCRIPT");
                                
                                    public static final java.lang.String __POWERPOINT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_POWERPOINT");
                                
                                    public static final java.lang.String __QUICKTIME =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_QUICKTIME");
                                
                                    public static final java.lang.String __RTF =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_RTF");
                                
                                    public static final java.lang.String __SMS =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_SMS");
                                
                                    public static final java.lang.String __STYLESHEET =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_STYLESHEET");
                                
                                    public static final java.lang.String __SVG =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_SVG");
                                
                                    public static final java.lang.String __TAR =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_TAR");
                                
                                    public static final java.lang.String __TARCOMP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_TARCOMP");
                                
                                    public static final java.lang.String __TIFFIMAGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_TIFFIMAGE");
                                
                                    public static final java.lang.String __TTF =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_TTF");
                                
                                    public static final java.lang.String __VISIO =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_VISIO");
                                
                                    public static final java.lang.String __WEBAPPPAGE =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_WEBAPPPAGE");
                                
                                    public static final java.lang.String __WEBAPPSCRIPT =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_WEBAPPSCRIPT");
                                
                                    public static final java.lang.String __WOFF =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_WOFF");
                                
                                    public static final java.lang.String __WOFF2 =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_WOFF2");
                                
                                    public static final java.lang.String __WORD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_WORD");
                                
                                    public static final java.lang.String __XMLDOC =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_XMLDOC");
                                
                                    public static final java.lang.String __XSD =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_XSD");
                                
                                    public static final java.lang.String __ZIP =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_ZIP");
                                
                                public static final MediaType _APPCACHE =
                                    new MediaType(__APPCACHE,true);
                            
                                public static final MediaType _AUTOCAD =
                                    new MediaType(__AUTOCAD,true);
                            
                                public static final MediaType _BMPIMAGE =
                                    new MediaType(__BMPIMAGE,true);
                            
                                public static final MediaType _CERTIFICATE =
                                    new MediaType(__CERTIFICATE,true);
                            
                                public static final MediaType _CFF =
                                    new MediaType(__CFF,true);
                            
                                public static final MediaType _CONFIG =
                                    new MediaType(__CONFIG,true);
                            
                                public static final MediaType _CSV =
                                    new MediaType(__CSV,true);
                            
                                public static final MediaType _EOT =
                                    new MediaType(__EOT,true);
                            
                                public static final MediaType _EXCEL =
                                    new MediaType(__EXCEL,true);
                            
                                public static final MediaType _FLASH =
                                    new MediaType(__FLASH,true);
                            
                                public static final MediaType _FREEMARKER =
                                    new MediaType(__FREEMARKER,true);
                            
                                public static final MediaType _GIFIMAGE =
                                    new MediaType(__GIFIMAGE,true);
                            
                                public static final MediaType _GZIP =
                                    new MediaType(__GZIP,true);
                            
                                public static final MediaType _HTMLDOC =
                                    new MediaType(__HTMLDOC,true);
                            
                                public static final MediaType _ICON =
                                    new MediaType(__ICON,true);
                            
                                public static final MediaType _IMAGE =
                                    new MediaType(__IMAGE,true);
                            
                                public static final MediaType _JAVASCRIPT =
                                    new MediaType(__JAVASCRIPT,true);
                            
                                public static final MediaType _JPGIMAGE =
                                    new MediaType(__JPGIMAGE,true);
                            
                                public static final MediaType _JSON =
                                    new MediaType(__JSON,true);
                            
                                public static final MediaType _LZH =
                                    new MediaType(__LZH,true);
                            
                                public static final MediaType _MESSAGERFC =
                                    new MediaType(__MESSAGERFC,true);
                            
                                public static final MediaType _MISCBINARY =
                                    new MediaType(__MISCBINARY,true);
                            
                                public static final MediaType _MISCTEXT =
                                    new MediaType(__MISCTEXT,true);
                            
                                public static final MediaType _MP3 =
                                    new MediaType(__MP3,true);
                            
                                public static final MediaType _MPEGMOVIE =
                                    new MediaType(__MPEGMOVIE,true);
                            
                                public static final MediaType _MSPROJECT =
                                    new MediaType(__MSPROJECT,true);
                            
                                public static final MediaType _OTF =
                                    new MediaType(__OTF,true);
                            
                                public static final MediaType _PDF =
                                    new MediaType(__PDF,true);
                            
                                public static final MediaType _PJPGIMAGE =
                                    new MediaType(__PJPGIMAGE,true);
                            
                                public static final MediaType _PLAINTEXT =
                                    new MediaType(__PLAINTEXT,true);
                            
                                public static final MediaType _PNGIMAGE =
                                    new MediaType(__PNGIMAGE,true);
                            
                                public static final MediaType _POSTSCRIPT =
                                    new MediaType(__POSTSCRIPT,true);
                            
                                public static final MediaType _POWERPOINT =
                                    new MediaType(__POWERPOINT,true);
                            
                                public static final MediaType _QUICKTIME =
                                    new MediaType(__QUICKTIME,true);
                            
                                public static final MediaType _RTF =
                                    new MediaType(__RTF,true);
                            
                                public static final MediaType _SMS =
                                    new MediaType(__SMS,true);
                            
                                public static final MediaType _STYLESHEET =
                                    new MediaType(__STYLESHEET,true);
                            
                                public static final MediaType _SVG =
                                    new MediaType(__SVG,true);
                            
                                public static final MediaType _TAR =
                                    new MediaType(__TAR,true);
                            
                                public static final MediaType _TARCOMP =
                                    new MediaType(__TARCOMP,true);
                            
                                public static final MediaType _TIFFIMAGE =
                                    new MediaType(__TIFFIMAGE,true);
                            
                                public static final MediaType _TTF =
                                    new MediaType(__TTF,true);
                            
                                public static final MediaType _VISIO =
                                    new MediaType(__VISIO,true);
                            
                                public static final MediaType _WEBAPPPAGE =
                                    new MediaType(__WEBAPPPAGE,true);
                            
                                public static final MediaType _WEBAPPSCRIPT =
                                    new MediaType(__WEBAPPSCRIPT,true);
                            
                                public static final MediaType _WOFF =
                                    new MediaType(__WOFF,true);
                            
                                public static final MediaType _WOFF2 =
                                    new MediaType(__WOFF2,true);
                            
                                public static final MediaType _WORD =
                                    new MediaType(__WORD,true);
                            
                                public static final MediaType _XMLDOC =
                                    new MediaType(__XMLDOC,true);
                            
                                public static final MediaType _XSD =
                                    new MediaType(__XSD,true);
                            
                                public static final MediaType _ZIP =
                                    new MediaType(__ZIP,true);
                            

                                public java.lang.String getValue() { return localMediaType;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localMediaType.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.filecabinet_2017_2.documents.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":MediaType",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "MediaType",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localMediaType==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("MediaType cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localMediaType);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.filecabinet_2017_2.documents.webservices.netsuite.com")){
                return "ns11";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMediaType)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static MediaType fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    MediaType enumeration = (MediaType)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static MediaType fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static MediaType fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return MediaType.Factory.fromString(content,namespaceUri);
                    } else {
                       return MediaType.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static MediaType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            MediaType object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"MediaType" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = MediaType.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = MediaType.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    