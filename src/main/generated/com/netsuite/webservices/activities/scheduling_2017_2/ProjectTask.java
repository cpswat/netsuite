
/**
 * ProjectTask.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.activities.scheduling_2017_2;
            

            /**
            *  ProjectTask bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ProjectTask extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ProjectTask
                Namespace URI = urn:scheduling_2017_2.activities.webservices.netsuite.com
                Namespace Prefix = ns9
                */
            

                        /**
                        * field for CustomForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFormTracker = false ;

                           public boolean isCustomFormSpecified(){
                               return localCustomFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomForm(){
                               return localCustomForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomForm
                               */
                               public void setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomFormTracker = param != null;
                                   
                                            this.localCustomForm=param;
                                    

                               }
                            

                        /**
                        * field for EventId
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localEventId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEventIdTracker = false ;

                           public boolean isEventIdSpecified(){
                               return localEventIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getEventId(){
                               return localEventId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EventId
                               */
                               public void setEventId(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localEventIdTracker = param != null;
                                   
                                            this.localEventId=param;
                                    

                               }
                            

                        /**
                        * field for PercentTimeComplete
                        */

                        
                                    protected double localPercentTimeComplete ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPercentTimeCompleteTracker = false ;

                           public boolean isPercentTimeCompleteSpecified(){
                               return localPercentTimeCompleteTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPercentTimeComplete(){
                               return localPercentTimeComplete;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PercentTimeComplete
                               */
                               public void setPercentTimeComplete(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPercentTimeCompleteTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPercentTimeComplete=param;
                                    

                               }
                            

                        /**
                        * field for Title
                        */

                        
                                    protected java.lang.String localTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTitleTracker = false ;

                           public boolean isTitleSpecified(){
                               return localTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTitle(){
                               return localTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Title
                               */
                               public void setTitle(java.lang.String param){
                            localTitleTracker = param != null;
                                   
                                            this.localTitle=param;
                                    

                               }
                            

                        /**
                        * field for Company
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCompany ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCompanyTracker = false ;

                           public boolean isCompanySpecified(){
                               return localCompanyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCompany(){
                               return localCompany;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Company
                               */
                               public void setCompany(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCompanyTracker = param != null;
                                   
                                            this.localCompany=param;
                                    

                               }
                            

                        /**
                        * field for Contact
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localContact ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContactTracker = false ;

                           public boolean isContactSpecified(){
                               return localContactTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getContact(){
                               return localContact;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Contact
                               */
                               public void setContact(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localContactTracker = param != null;
                                   
                                            this.localContact=param;
                                    

                               }
                            

                        /**
                        * field for Order
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localOrder ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOrderTracker = false ;

                           public boolean isOrderSpecified(){
                               return localOrderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getOrder(){
                               return localOrder;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Order
                               */
                               public void setOrder(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localOrderTracker = param != null;
                                   
                                            this.localOrder=param;
                                    

                               }
                            

                        /**
                        * field for Owner
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localOwner ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOwnerTracker = false ;

                           public boolean isOwnerSpecified(){
                               return localOwnerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getOwner(){
                               return localOwner;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Owner
                               */
                               public void setOwner(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localOwnerTracker = param != null;
                                   
                                            this.localOwner=param;
                                    

                               }
                            

                        /**
                        * field for Parent
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localParent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localParentTracker = false ;

                           public boolean isParentSpecified(){
                               return localParentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getParent(){
                               return localParent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Parent
                               */
                               public void setParent(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localParentTracker = param != null;
                                   
                                            this.localParent=param;
                                    

                               }
                            

                        /**
                        * field for Priority
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localPriority ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPriorityTracker = false ;

                           public boolean isPrioritySpecified(){
                               return localPriorityTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getPriority(){
                               return localPriority;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Priority
                               */
                               public void setPriority(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localPriorityTracker = param != null;
                                   
                                            this.localPriority=param;
                                    

                               }
                            

                        /**
                        * field for EstimatedWork
                        */

                        
                                    protected double localEstimatedWork ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstimatedWorkTracker = false ;

                           public boolean isEstimatedWorkSpecified(){
                               return localEstimatedWorkTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getEstimatedWork(){
                               return localEstimatedWork;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstimatedWork
                               */
                               public void setEstimatedWork(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localEstimatedWorkTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localEstimatedWork=param;
                                    

                               }
                            

                        /**
                        * field for EstimatedWorkBaseline
                        */

                        
                                    protected double localEstimatedWorkBaseline ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEstimatedWorkBaselineTracker = false ;

                           public boolean isEstimatedWorkBaselineSpecified(){
                               return localEstimatedWorkBaselineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getEstimatedWorkBaseline(){
                               return localEstimatedWorkBaseline;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EstimatedWorkBaseline
                               */
                               public void setEstimatedWorkBaseline(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localEstimatedWorkBaselineTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localEstimatedWorkBaseline=param;
                                    

                               }
                            

                        /**
                        * field for ConstraintType
                        */

                        
                                    protected com.netsuite.webservices.activities.scheduling_2017_2.types.ProjectTaskConstraintType localConstraintType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localConstraintTypeTracker = false ;

                           public boolean isConstraintTypeSpecified(){
                               return localConstraintTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.activities.scheduling_2017_2.types.ProjectTaskConstraintType
                           */
                           public  com.netsuite.webservices.activities.scheduling_2017_2.types.ProjectTaskConstraintType getConstraintType(){
                               return localConstraintType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ConstraintType
                               */
                               public void setConstraintType(com.netsuite.webservices.activities.scheduling_2017_2.types.ProjectTaskConstraintType param){
                            localConstraintTypeTracker = param != null;
                                   
                                            this.localConstraintType=param;
                                    

                               }
                            

                        /**
                        * field for StartDate
                        */

                        
                                    protected java.util.Calendar localStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateTracker = false ;

                           public boolean isStartDateSpecified(){
                               return localStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getStartDate(){
                               return localStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDate
                               */
                               public void setStartDate(java.util.Calendar param){
                            localStartDateTracker = param != null;
                                   
                                            this.localStartDate=param;
                                    

                               }
                            

                        /**
                        * field for StartDateBaseline
                        */

                        
                                    protected java.util.Calendar localStartDateBaseline ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateBaselineTracker = false ;

                           public boolean isStartDateBaselineSpecified(){
                               return localStartDateBaselineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getStartDateBaseline(){
                               return localStartDateBaseline;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDateBaseline
                               */
                               public void setStartDateBaseline(java.util.Calendar param){
                            localStartDateBaselineTracker = param != null;
                                   
                                            this.localStartDateBaseline=param;
                                    

                               }
                            

                        /**
                        * field for EndDate
                        */

                        
                                    protected java.util.Calendar localEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndDateTracker = false ;

                           public boolean isEndDateSpecified(){
                               return localEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getEndDate(){
                               return localEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndDate
                               */
                               public void setEndDate(java.util.Calendar param){
                            localEndDateTracker = param != null;
                                   
                                            this.localEndDate=param;
                                    

                               }
                            

                        /**
                        * field for FinishByDate
                        */

                        
                                    protected java.util.Calendar localFinishByDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFinishByDateTracker = false ;

                           public boolean isFinishByDateSpecified(){
                               return localFinishByDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getFinishByDate(){
                               return localFinishByDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param FinishByDate
                               */
                               public void setFinishByDate(java.util.Calendar param){
                            localFinishByDateTracker = param != null;
                                   
                                            this.localFinishByDate=param;
                                    

                               }
                            

                        /**
                        * field for EndDateBaseline
                        */

                        
                                    protected java.util.Calendar localEndDateBaseline ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndDateBaselineTracker = false ;

                           public boolean isEndDateBaselineSpecified(){
                               return localEndDateBaselineTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getEndDateBaseline(){
                               return localEndDateBaseline;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndDateBaseline
                               */
                               public void setEndDateBaseline(java.util.Calendar param){
                            localEndDateBaselineTracker = param != null;
                                   
                                            this.localEndDateBaseline=param;
                                    

                               }
                            

                        /**
                        * field for ActualWork
                        */

                        
                                    protected double localActualWork ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localActualWorkTracker = false ;

                           public boolean isActualWorkSpecified(){
                               return localActualWorkTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getActualWork(){
                               return localActualWork;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ActualWork
                               */
                               public void setActualWork(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localActualWorkTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localActualWork=param;
                                    

                               }
                            

                        /**
                        * field for RemainingWork
                        */

                        
                                    protected double localRemainingWork ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRemainingWorkTracker = false ;

                           public boolean isRemainingWorkSpecified(){
                               return localRemainingWorkTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getRemainingWork(){
                               return localRemainingWork;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RemainingWork
                               */
                               public void setRemainingWork(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localRemainingWorkTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localRemainingWork=param;
                                    

                               }
                            

                        /**
                        * field for Message
                        */

                        
                                    protected java.lang.String localMessage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMessageTracker = false ;

                           public boolean isMessageSpecified(){
                               return localMessageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMessage(){
                               return localMessage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Message
                               */
                               public void setMessage(java.lang.String param){
                            localMessageTracker = param != null;
                                   
                                            this.localMessage=param;
                                    

                               }
                            

                        /**
                        * field for IsMilestone
                        */

                        
                                    protected boolean localIsMilestone ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsMilestoneTracker = false ;

                           public boolean isIsMilestoneSpecified(){
                               return localIsMilestoneTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsMilestone(){
                               return localIsMilestone;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsMilestone
                               */
                               public void setIsMilestone(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsMilestoneTracker =
                                       true;
                                   
                                            this.localIsMilestone=param;
                                    

                               }
                            

                        /**
                        * field for IsOnCriticalPath
                        */

                        
                                    protected java.lang.String localIsOnCriticalPath ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsOnCriticalPathTracker = false ;

                           public boolean isIsOnCriticalPathSpecified(){
                               return localIsOnCriticalPathTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getIsOnCriticalPath(){
                               return localIsOnCriticalPath;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsOnCriticalPath
                               */
                               public void setIsOnCriticalPath(java.lang.String param){
                            localIsOnCriticalPathTracker = param != null;
                                   
                                            this.localIsOnCriticalPath=param;
                                    

                               }
                            

                        /**
                        * field for SlackMinutes
                        */

                        
                                    protected double localSlackMinutes ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSlackMinutesTracker = false ;

                           public boolean isSlackMinutesSpecified(){
                               return localSlackMinutesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getSlackMinutes(){
                               return localSlackMinutes;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SlackMinutes
                               */
                               public void setSlackMinutes(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localSlackMinutesTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localSlackMinutes=param;
                                    

                               }
                            

                        /**
                        * field for LateEnd
                        */

                        
                                    protected java.util.Calendar localLateEnd ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLateEndTracker = false ;

                           public boolean isLateEndSpecified(){
                               return localLateEndTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLateEnd(){
                               return localLateEnd;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LateEnd
                               */
                               public void setLateEnd(java.util.Calendar param){
                            localLateEndTracker = param != null;
                                   
                                            this.localLateEnd=param;
                                    

                               }
                            

                        /**
                        * field for LateStart
                        */

                        
                                    protected java.util.Calendar localLateStart ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLateStartTracker = false ;

                           public boolean isLateStartSpecified(){
                               return localLateStartTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLateStart(){
                               return localLateStart;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LateStart
                               */
                               public void setLateStart(java.util.Calendar param){
                            localLateStartTracker = param != null;
                                   
                                            this.localLateStart=param;
                                    

                               }
                            

                        /**
                        * field for Status
                        */

                        
                                    protected com.netsuite.webservices.activities.scheduling_2017_2.types.ProjectTaskStatus localStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStatusTracker = false ;

                           public boolean isStatusSpecified(){
                               return localStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.activities.scheduling_2017_2.types.ProjectTaskStatus
                           */
                           public  com.netsuite.webservices.activities.scheduling_2017_2.types.ProjectTaskStatus getStatus(){
                               return localStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Status
                               */
                               public void setStatus(com.netsuite.webservices.activities.scheduling_2017_2.types.ProjectTaskStatus param){
                            localStatusTracker = param != null;
                                   
                                            this.localStatus=param;
                                    

                               }
                            

                        /**
                        * field for NonBillableTask
                        */

                        
                                    protected boolean localNonBillableTask ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNonBillableTaskTracker = false ;

                           public boolean isNonBillableTaskSpecified(){
                               return localNonBillableTaskTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getNonBillableTask(){
                               return localNonBillableTask;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NonBillableTask
                               */
                               public void setNonBillableTask(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localNonBillableTaskTracker =
                                       true;
                                   
                                            this.localNonBillableTask=param;
                                    

                               }
                            

                        /**
                        * field for AssigneeList
                        */

                        
                                    protected com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskAssigneeList localAssigneeList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAssigneeListTracker = false ;

                           public boolean isAssigneeListSpecified(){
                               return localAssigneeListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskAssigneeList
                           */
                           public  com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskAssigneeList getAssigneeList(){
                               return localAssigneeList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AssigneeList
                               */
                               public void setAssigneeList(com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskAssigneeList param){
                            localAssigneeListTracker = param != null;
                                   
                                            this.localAssigneeList=param;
                                    

                               }
                            

                        /**
                        * field for PredecessorList
                        */

                        
                                    protected com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskPredecessorList localPredecessorList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPredecessorListTracker = false ;

                           public boolean isPredecessorListSpecified(){
                               return localPredecessorListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskPredecessorList
                           */
                           public  com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskPredecessorList getPredecessorList(){
                               return localPredecessorList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PredecessorList
                               */
                               public void setPredecessorList(com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskPredecessorList param){
                            localPredecessorListTracker = param != null;
                                   
                                            this.localPredecessorList=param;
                                    

                               }
                            

                        /**
                        * field for TimeItemList
                        */

                        
                                    protected com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskTimeItemList localTimeItemList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeItemListTracker = false ;

                           public boolean isTimeItemListSpecified(){
                               return localTimeItemListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskTimeItemList
                           */
                           public  com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskTimeItemList getTimeItemList(){
                               return localTimeItemList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimeItemList
                               */
                               public void setTimeItemList(com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskTimeItemList param){
                            localTimeItemListTracker = param != null;
                                   
                                            this.localTimeItemList=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:scheduling_2017_2.activities.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ProjectTask",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ProjectTask",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localCustomFormTracker){
                                            if (localCustomForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                            }
                                           localCustomForm.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","customForm"),
                                               xmlWriter);
                                        } if (localEventIdTracker){
                                            if (localEventId==null){
                                                 throw new org.apache.axis2.databinding.ADBException("eventId cannot be null!!");
                                            }
                                           localEventId.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","eventId"),
                                               xmlWriter);
                                        } if (localPercentTimeCompleteTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "percentTimeComplete", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPercentTimeComplete)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("percentTimeComplete cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPercentTimeComplete));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTitleTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "title", xmlWriter);
                             

                                          if (localTitle==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTitle);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCompanyTracker){
                                            if (localCompany==null){
                                                 throw new org.apache.axis2.databinding.ADBException("company cannot be null!!");
                                            }
                                           localCompany.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","company"),
                                               xmlWriter);
                                        } if (localContactTracker){
                                            if (localContact==null){
                                                 throw new org.apache.axis2.databinding.ADBException("contact cannot be null!!");
                                            }
                                           localContact.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","contact"),
                                               xmlWriter);
                                        } if (localOrderTracker){
                                            if (localOrder==null){
                                                 throw new org.apache.axis2.databinding.ADBException("order cannot be null!!");
                                            }
                                           localOrder.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","order"),
                                               xmlWriter);
                                        } if (localOwnerTracker){
                                            if (localOwner==null){
                                                 throw new org.apache.axis2.databinding.ADBException("owner cannot be null!!");
                                            }
                                           localOwner.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","owner"),
                                               xmlWriter);
                                        } if (localParentTracker){
                                            if (localParent==null){
                                                 throw new org.apache.axis2.databinding.ADBException("parent cannot be null!!");
                                            }
                                           localParent.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","parent"),
                                               xmlWriter);
                                        } if (localPriorityTracker){
                                            if (localPriority==null){
                                                 throw new org.apache.axis2.databinding.ADBException("priority cannot be null!!");
                                            }
                                           localPriority.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","priority"),
                                               xmlWriter);
                                        } if (localEstimatedWorkTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "estimatedWork", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localEstimatedWork)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("estimatedWork cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstimatedWork));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEstimatedWorkBaselineTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "estimatedWorkBaseline", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localEstimatedWorkBaseline)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("estimatedWorkBaseline cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstimatedWorkBaseline));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localConstraintTypeTracker){
                                            if (localConstraintType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("constraintType cannot be null!!");
                                            }
                                           localConstraintType.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","constraintType"),
                                               xmlWriter);
                                        } if (localStartDateTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "startDate", xmlWriter);
                             

                                          if (localStartDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStartDateBaselineTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "startDateBaseline", xmlWriter);
                             

                                          if (localStartDateBaseline==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("startDateBaseline cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDateBaseline));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEndDateTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "endDate", xmlWriter);
                             

                                          if (localEndDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localFinishByDateTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "finishByDate", xmlWriter);
                             

                                          if (localFinishByDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("finishByDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFinishByDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEndDateBaselineTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "endDateBaseline", xmlWriter);
                             

                                          if (localEndDateBaseline==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("endDateBaseline cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDateBaseline));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localActualWorkTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "actualWork", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localActualWork)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("actualWork cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localActualWork));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRemainingWorkTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "remainingWork", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localRemainingWork)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("remainingWork cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRemainingWork));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMessageTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "message", xmlWriter);
                             

                                          if (localMessage==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("message cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMessage);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsMilestoneTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isMilestone", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("isMilestone cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsMilestone));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsOnCriticalPathTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "isOnCriticalPath", xmlWriter);
                             

                                          if (localIsOnCriticalPath==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("isOnCriticalPath cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localIsOnCriticalPath);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSlackMinutesTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "slackMinutes", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localSlackMinutes)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("slackMinutes cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSlackMinutes));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLateEndTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lateEnd", xmlWriter);
                             

                                          if (localLateEnd==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lateEnd cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLateEnd));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLateStartTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lateStart", xmlWriter);
                             

                                          if (localLateStart==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lateStart cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLateStart));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStatusTracker){
                                            if (localStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                            }
                                           localStatus.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","status"),
                                               xmlWriter);
                                        } if (localNonBillableTaskTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "nonBillableTask", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("nonBillableTask cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNonBillableTask));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAssigneeListTracker){
                                            if (localAssigneeList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("assigneeList cannot be null!!");
                                            }
                                           localAssigneeList.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","assigneeList"),
                                               xmlWriter);
                                        } if (localPredecessorListTracker){
                                            if (localPredecessorList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("predecessorList cannot be null!!");
                                            }
                                           localPredecessorList.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","predecessorList"),
                                               xmlWriter);
                                        } if (localTimeItemListTracker){
                                            if (localTimeItemList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("timeItemList cannot be null!!");
                                            }
                                           localTimeItemList.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","timeItemList"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:scheduling_2017_2.activities.webservices.netsuite.com")){
                return "ns9";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","ProjectTask"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localCustomFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "customForm"));
                            
                            
                                    if (localCustomForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                    }
                                    elementList.add(localCustomForm);
                                } if (localEventIdTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "eventId"));
                            
                            
                                    if (localEventId==null){
                                         throw new org.apache.axis2.databinding.ADBException("eventId cannot be null!!");
                                    }
                                    elementList.add(localEventId);
                                } if (localPercentTimeCompleteTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "percentTimeComplete"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPercentTimeComplete));
                            } if (localTitleTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "title"));
                                 
                                        if (localTitle != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTitle));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                        }
                                    } if (localCompanyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "company"));
                            
                            
                                    if (localCompany==null){
                                         throw new org.apache.axis2.databinding.ADBException("company cannot be null!!");
                                    }
                                    elementList.add(localCompany);
                                } if (localContactTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "contact"));
                            
                            
                                    if (localContact==null){
                                         throw new org.apache.axis2.databinding.ADBException("contact cannot be null!!");
                                    }
                                    elementList.add(localContact);
                                } if (localOrderTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "order"));
                            
                            
                                    if (localOrder==null){
                                         throw new org.apache.axis2.databinding.ADBException("order cannot be null!!");
                                    }
                                    elementList.add(localOrder);
                                } if (localOwnerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "owner"));
                            
                            
                                    if (localOwner==null){
                                         throw new org.apache.axis2.databinding.ADBException("owner cannot be null!!");
                                    }
                                    elementList.add(localOwner);
                                } if (localParentTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "parent"));
                            
                            
                                    if (localParent==null){
                                         throw new org.apache.axis2.databinding.ADBException("parent cannot be null!!");
                                    }
                                    elementList.add(localParent);
                                } if (localPriorityTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "priority"));
                            
                            
                                    if (localPriority==null){
                                         throw new org.apache.axis2.databinding.ADBException("priority cannot be null!!");
                                    }
                                    elementList.add(localPriority);
                                } if (localEstimatedWorkTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "estimatedWork"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstimatedWork));
                            } if (localEstimatedWorkBaselineTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "estimatedWorkBaseline"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEstimatedWorkBaseline));
                            } if (localConstraintTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "constraintType"));
                            
                            
                                    if (localConstraintType==null){
                                         throw new org.apache.axis2.databinding.ADBException("constraintType cannot be null!!");
                                    }
                                    elementList.add(localConstraintType);
                                } if (localStartDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "startDate"));
                                 
                                        if (localStartDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                        }
                                    } if (localStartDateBaselineTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "startDateBaseline"));
                                 
                                        if (localStartDateBaseline != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDateBaseline));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("startDateBaseline cannot be null!!");
                                        }
                                    } if (localEndDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "endDate"));
                                 
                                        if (localEndDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                        }
                                    } if (localFinishByDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "finishByDate"));
                                 
                                        if (localFinishByDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFinishByDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("finishByDate cannot be null!!");
                                        }
                                    } if (localEndDateBaselineTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "endDateBaseline"));
                                 
                                        if (localEndDateBaseline != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDateBaseline));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("endDateBaseline cannot be null!!");
                                        }
                                    } if (localActualWorkTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "actualWork"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localActualWork));
                            } if (localRemainingWorkTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "remainingWork"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRemainingWork));
                            } if (localMessageTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "message"));
                                 
                                        if (localMessage != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMessage));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("message cannot be null!!");
                                        }
                                    } if (localIsMilestoneTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "isMilestone"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsMilestone));
                            } if (localIsOnCriticalPathTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "isOnCriticalPath"));
                                 
                                        if (localIsOnCriticalPath != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsOnCriticalPath));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("isOnCriticalPath cannot be null!!");
                                        }
                                    } if (localSlackMinutesTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "slackMinutes"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSlackMinutes));
                            } if (localLateEndTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "lateEnd"));
                                 
                                        if (localLateEnd != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLateEnd));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lateEnd cannot be null!!");
                                        }
                                    } if (localLateStartTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "lateStart"));
                                 
                                        if (localLateStart != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLateStart));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lateStart cannot be null!!");
                                        }
                                    } if (localStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "status"));
                            
                            
                                    if (localStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                    }
                                    elementList.add(localStatus);
                                } if (localNonBillableTaskTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "nonBillableTask"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNonBillableTask));
                            } if (localAssigneeListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "assigneeList"));
                            
                            
                                    if (localAssigneeList==null){
                                         throw new org.apache.axis2.databinding.ADBException("assigneeList cannot be null!!");
                                    }
                                    elementList.add(localAssigneeList);
                                } if (localPredecessorListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "predecessorList"));
                            
                            
                                    if (localPredecessorList==null){
                                         throw new org.apache.axis2.databinding.ADBException("predecessorList cannot be null!!");
                                    }
                                    elementList.add(localPredecessorList);
                                } if (localTimeItemListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "timeItemList"));
                            
                            
                                    if (localTimeItemList==null){
                                         throw new org.apache.axis2.databinding.ADBException("timeItemList cannot be null!!");
                                    }
                                    elementList.add(localTimeItemList);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ProjectTask parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ProjectTask object =
                new ProjectTask();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ProjectTask".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ProjectTask)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","customForm").equals(reader.getName())){
                                
                                                object.setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","eventId").equals(reader.getName())){
                                
                                                object.setEventId(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","percentTimeComplete").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"percentTimeComplete" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPercentTimeComplete(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPercentTimeComplete(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","title").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"title" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTitle(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","company").equals(reader.getName())){
                                
                                                object.setCompany(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","contact").equals(reader.getName())){
                                
                                                object.setContact(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","order").equals(reader.getName())){
                                
                                                object.setOrder(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","owner").equals(reader.getName())){
                                
                                                object.setOwner(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","parent").equals(reader.getName())){
                                
                                                object.setParent(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","priority").equals(reader.getName())){
                                
                                                object.setPriority(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","estimatedWork").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"estimatedWork" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEstimatedWork(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setEstimatedWork(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","estimatedWorkBaseline").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"estimatedWorkBaseline" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEstimatedWorkBaseline(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setEstimatedWorkBaseline(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","constraintType").equals(reader.getName())){
                                
                                                object.setConstraintType(com.netsuite.webservices.activities.scheduling_2017_2.types.ProjectTaskConstraintType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","startDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"startDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStartDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","startDateBaseline").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"startDateBaseline" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStartDateBaseline(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","endDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"endDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEndDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","finishByDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"finishByDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setFinishByDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","endDateBaseline").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"endDateBaseline" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEndDateBaseline(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","actualWork").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"actualWork" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setActualWork(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setActualWork(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","remainingWork").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"remainingWork" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRemainingWork(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setRemainingWork(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","message").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"message" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMessage(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","isMilestone").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isMilestone" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsMilestone(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","isOnCriticalPath").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"isOnCriticalPath" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsOnCriticalPath(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","slackMinutes").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"slackMinutes" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSlackMinutes(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setSlackMinutes(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","lateEnd").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lateEnd" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLateEnd(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","lateStart").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lateStart" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLateStart(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","status").equals(reader.getName())){
                                
                                                object.setStatus(com.netsuite.webservices.activities.scheduling_2017_2.types.ProjectTaskStatus.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","nonBillableTask").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"nonBillableTask" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNonBillableTask(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","assigneeList").equals(reader.getName())){
                                
                                                object.setAssigneeList(com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskAssigneeList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","predecessorList").equals(reader.getName())){
                                
                                                object.setPredecessorList(com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskPredecessorList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","timeItemList").equals(reader.getName())){
                                
                                                object.setTimeItemList(com.netsuite.webservices.activities.scheduling_2017_2.ProjectTaskTimeItemList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    