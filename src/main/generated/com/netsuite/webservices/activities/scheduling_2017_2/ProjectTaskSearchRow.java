
/**
 * ProjectTaskSearchRow.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.activities.scheduling_2017_2;
            

            /**
            *  ProjectTaskSearchRow bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ProjectTaskSearchRow extends com.netsuite.webservices.platform.core_2017_2.SearchRow
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ProjectTaskSearchRow
                Namespace URI = urn:scheduling_2017_2.activities.webservices.netsuite.com
                Namespace Prefix = ns9
                */
            

                        /**
                        * field for Basic
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchRowBasic localBasic ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBasicTracker = false ;

                           public boolean isBasicSpecified(){
                               return localBasicTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchRowBasic getBasic(){
                               return localBasic;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Basic
                               */
                               public void setBasic(com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchRowBasic param){
                            localBasicTracker = param != null;
                                   
                                            this.localBasic=param;
                                    

                               }
                            

                        /**
                        * field for JobJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic localJobJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localJobJoinTracker = false ;

                           public boolean isJobJoinSpecified(){
                               return localJobJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic getJobJoin(){
                               return localJobJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param JobJoin
                               */
                               public void setJobJoin(com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic param){
                            localJobJoinTracker = param != null;
                                   
                                            this.localJobJoin=param;
                                    

                               }
                            

                        /**
                        * field for PredecessorJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchRowBasic localPredecessorJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPredecessorJoinTracker = false ;

                           public boolean isPredecessorJoinSpecified(){
                               return localPredecessorJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchRowBasic getPredecessorJoin(){
                               return localPredecessorJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PredecessorJoin
                               */
                               public void setPredecessorJoin(com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchRowBasic param){
                            localPredecessorJoinTracker = param != null;
                                   
                                            this.localPredecessorJoin=param;
                                    

                               }
                            

                        /**
                        * field for ProjectTaskAssignmentJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ProjectTaskAssignmentSearchRowBasic localProjectTaskAssignmentJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProjectTaskAssignmentJoinTracker = false ;

                           public boolean isProjectTaskAssignmentJoinSpecified(){
                               return localProjectTaskAssignmentJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ProjectTaskAssignmentSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ProjectTaskAssignmentSearchRowBasic getProjectTaskAssignmentJoin(){
                               return localProjectTaskAssignmentJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ProjectTaskAssignmentJoin
                               */
                               public void setProjectTaskAssignmentJoin(com.netsuite.webservices.platform.common_2017_2.ProjectTaskAssignmentSearchRowBasic param){
                            localProjectTaskAssignmentJoinTracker = param != null;
                                   
                                            this.localProjectTaskAssignmentJoin=param;
                                    

                               }
                            

                        /**
                        * field for ResourceAllocationJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ResourceAllocationSearchRowBasic localResourceAllocationJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localResourceAllocationJoinTracker = false ;

                           public boolean isResourceAllocationJoinSpecified(){
                               return localResourceAllocationJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ResourceAllocationSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ResourceAllocationSearchRowBasic getResourceAllocationJoin(){
                               return localResourceAllocationJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ResourceAllocationJoin
                               */
                               public void setResourceAllocationJoin(com.netsuite.webservices.platform.common_2017_2.ResourceAllocationSearchRowBasic param){
                            localResourceAllocationJoinTracker = param != null;
                                   
                                            this.localResourceAllocationJoin=param;
                                    

                               }
                            

                        /**
                        * field for SuccessorJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchRowBasic localSuccessorJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSuccessorJoinTracker = false ;

                           public boolean isSuccessorJoinSpecified(){
                               return localSuccessorJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchRowBasic getSuccessorJoin(){
                               return localSuccessorJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SuccessorJoin
                               */
                               public void setSuccessorJoin(com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchRowBasic param){
                            localSuccessorJoinTracker = param != null;
                                   
                                            this.localSuccessorJoin=param;
                                    

                               }
                            

                        /**
                        * field for TimeJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TimeBillSearchRowBasic localTimeJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeJoinTracker = false ;

                           public boolean isTimeJoinSpecified(){
                               return localTimeJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TimeBillSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TimeBillSearchRowBasic getTimeJoin(){
                               return localTimeJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimeJoin
                               */
                               public void setTimeJoin(com.netsuite.webservices.platform.common_2017_2.TimeBillSearchRowBasic param){
                            localTimeJoinTracker = param != null;
                                   
                                            this.localTimeJoin=param;
                                    

                               }
                            

                        /**
                        * field for TransactionJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic localTransactionJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTransactionJoinTracker = false ;

                           public boolean isTransactionJoinSpecified(){
                               return localTransactionJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic getTransactionJoin(){
                               return localTransactionJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TransactionJoin
                               */
                               public void setTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic param){
                            localTransactionJoinTracker = param != null;
                                   
                                            this.localTransactionJoin=param;
                                    

                               }
                            

                        /**
                        * field for UserJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic localUserJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUserJoinTracker = false ;

                           public boolean isUserJoinSpecified(){
                               return localUserJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic getUserJoin(){
                               return localUserJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserJoin
                               */
                               public void setUserJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic param){
                            localUserJoinTracker = param != null;
                                   
                                            this.localUserJoin=param;
                                    

                               }
                            

                        /**
                        * field for UserNotesJoin
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic localUserNotesJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localUserNotesJoinTracker = false ;

                           public boolean isUserNotesJoinSpecified(){
                               return localUserNotesJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic getUserNotesJoin(){
                               return localUserNotesJoin;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param UserNotesJoin
                               */
                               public void setUserNotesJoin(com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic param){
                            localUserNotesJoinTracker = param != null;
                                   
                                            this.localUserNotesJoin=param;
                                    

                               }
                            

                        /**
                        * field for CustomSearchJoin
                        * This was an Array!
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] localCustomSearchJoin ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomSearchJoinTracker = false ;

                           public boolean isCustomSearchJoinSpecified(){
                               return localCustomSearchJoinTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[]
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] getCustomSearchJoin(){
                               return localCustomSearchJoin;
                           }

                           
                        


                               
                              /**
                               * validate the array for CustomSearchJoin
                               */
                              protected void validateCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param CustomSearchJoin
                              */
                              public void setCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[] param){
                              
                                   validateCustomSearchJoin(param);

                               localCustomSearchJoinTracker = param != null;
                                      
                                      this.localCustomSearchJoin=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic
                             */
                             public void addCustomSearchJoin(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic param){
                                   if (localCustomSearchJoin == null){
                                   localCustomSearchJoin = new com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[]{};
                                   }

                            
                                 //update the setting tracker
                                localCustomSearchJoinTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(localCustomSearchJoin);
                               list.add(param);
                               this.localCustomSearchJoin =
                             (com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[])list.toArray(
                            new com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[list.size()]);

                             }
                             

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:scheduling_2017_2.activities.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ProjectTaskSearchRow",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ProjectTaskSearchRow",
                           xmlWriter);
                   }

                if (localBasicTracker){
                                            if (localBasic==null){
                                                 throw new org.apache.axis2.databinding.ADBException("basic cannot be null!!");
                                            }
                                           localBasic.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","basic"),
                                               xmlWriter);
                                        } if (localJobJoinTracker){
                                            if (localJobJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("jobJoin cannot be null!!");
                                            }
                                           localJobJoin.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","jobJoin"),
                                               xmlWriter);
                                        } if (localPredecessorJoinTracker){
                                            if (localPredecessorJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("predecessorJoin cannot be null!!");
                                            }
                                           localPredecessorJoin.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","predecessorJoin"),
                                               xmlWriter);
                                        } if (localProjectTaskAssignmentJoinTracker){
                                            if (localProjectTaskAssignmentJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("projectTaskAssignmentJoin cannot be null!!");
                                            }
                                           localProjectTaskAssignmentJoin.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","projectTaskAssignmentJoin"),
                                               xmlWriter);
                                        } if (localResourceAllocationJoinTracker){
                                            if (localResourceAllocationJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("resourceAllocationJoin cannot be null!!");
                                            }
                                           localResourceAllocationJoin.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","resourceAllocationJoin"),
                                               xmlWriter);
                                        } if (localSuccessorJoinTracker){
                                            if (localSuccessorJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("successorJoin cannot be null!!");
                                            }
                                           localSuccessorJoin.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","successorJoin"),
                                               xmlWriter);
                                        } if (localTimeJoinTracker){
                                            if (localTimeJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("timeJoin cannot be null!!");
                                            }
                                           localTimeJoin.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","timeJoin"),
                                               xmlWriter);
                                        } if (localTransactionJoinTracker){
                                            if (localTransactionJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("transactionJoin cannot be null!!");
                                            }
                                           localTransactionJoin.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","transactionJoin"),
                                               xmlWriter);
                                        } if (localUserJoinTracker){
                                            if (localUserJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("userJoin cannot be null!!");
                                            }
                                           localUserJoin.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","userJoin"),
                                               xmlWriter);
                                        } if (localUserNotesJoinTracker){
                                            if (localUserNotesJoin==null){
                                                 throw new org.apache.axis2.databinding.ADBException("userNotesJoin cannot be null!!");
                                            }
                                           localUserNotesJoin.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","userNotesJoin"),
                                               xmlWriter);
                                        } if (localCustomSearchJoinTracker){
                                       if (localCustomSearchJoin!=null){
                                            for (int i = 0;i < localCustomSearchJoin.length;i++){
                                                if (localCustomSearchJoin[i] != null){
                                                 localCustomSearchJoin[i].serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","customSearchJoin"),
                                                           xmlWriter);
                                                } else {
                                                   
                                                        // we don't have to do any thing since minOccures is zero
                                                    
                                                }

                                            }
                                     } else {
                                        
                                               throw new org.apache.axis2.databinding.ADBException("customSearchJoin cannot be null!!");
                                        
                                    }
                                 }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:scheduling_2017_2.activities.webservices.netsuite.com")){
                return "ns9";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","ProjectTaskSearchRow"));
                 if (localBasicTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "basic"));
                            
                            
                                    if (localBasic==null){
                                         throw new org.apache.axis2.databinding.ADBException("basic cannot be null!!");
                                    }
                                    elementList.add(localBasic);
                                } if (localJobJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "jobJoin"));
                            
                            
                                    if (localJobJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("jobJoin cannot be null!!");
                                    }
                                    elementList.add(localJobJoin);
                                } if (localPredecessorJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "predecessorJoin"));
                            
                            
                                    if (localPredecessorJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("predecessorJoin cannot be null!!");
                                    }
                                    elementList.add(localPredecessorJoin);
                                } if (localProjectTaskAssignmentJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "projectTaskAssignmentJoin"));
                            
                            
                                    if (localProjectTaskAssignmentJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("projectTaskAssignmentJoin cannot be null!!");
                                    }
                                    elementList.add(localProjectTaskAssignmentJoin);
                                } if (localResourceAllocationJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "resourceAllocationJoin"));
                            
                            
                                    if (localResourceAllocationJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("resourceAllocationJoin cannot be null!!");
                                    }
                                    elementList.add(localResourceAllocationJoin);
                                } if (localSuccessorJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "successorJoin"));
                            
                            
                                    if (localSuccessorJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("successorJoin cannot be null!!");
                                    }
                                    elementList.add(localSuccessorJoin);
                                } if (localTimeJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "timeJoin"));
                            
                            
                                    if (localTimeJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("timeJoin cannot be null!!");
                                    }
                                    elementList.add(localTimeJoin);
                                } if (localTransactionJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "transactionJoin"));
                            
                            
                                    if (localTransactionJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("transactionJoin cannot be null!!");
                                    }
                                    elementList.add(localTransactionJoin);
                                } if (localUserJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "userJoin"));
                            
                            
                                    if (localUserJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("userJoin cannot be null!!");
                                    }
                                    elementList.add(localUserJoin);
                                } if (localUserNotesJoinTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "userNotesJoin"));
                            
                            
                                    if (localUserNotesJoin==null){
                                         throw new org.apache.axis2.databinding.ADBException("userNotesJoin cannot be null!!");
                                    }
                                    elementList.add(localUserNotesJoin);
                                } if (localCustomSearchJoinTracker){
                             if (localCustomSearchJoin!=null) {
                                 for (int i = 0;i < localCustomSearchJoin.length;i++){

                                    if (localCustomSearchJoin[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                          "customSearchJoin"));
                                         elementList.add(localCustomSearchJoin[i]);
                                    } else {
                                        
                                                // nothing to do
                                            
                                    }

                                 }
                             } else {
                                 
                                        throw new org.apache.axis2.databinding.ADBException("customSearchJoin cannot be null!!");
                                    
                             }

                        }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ProjectTaskSearchRow parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ProjectTaskSearchRow object =
                new ProjectTaskSearchRow();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ProjectTaskSearchRow".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ProjectTaskSearchRow)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                        java.util.ArrayList list11 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","basic").equals(reader.getName())){
                                
                                                object.setBasic(com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","jobJoin").equals(reader.getName())){
                                
                                                object.setJobJoin(com.netsuite.webservices.platform.common_2017_2.JobSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","predecessorJoin").equals(reader.getName())){
                                
                                                object.setPredecessorJoin(com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","projectTaskAssignmentJoin").equals(reader.getName())){
                                
                                                object.setProjectTaskAssignmentJoin(com.netsuite.webservices.platform.common_2017_2.ProjectTaskAssignmentSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","resourceAllocationJoin").equals(reader.getName())){
                                
                                                object.setResourceAllocationJoin(com.netsuite.webservices.platform.common_2017_2.ResourceAllocationSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","successorJoin").equals(reader.getName())){
                                
                                                object.setSuccessorJoin(com.netsuite.webservices.platform.common_2017_2.ProjectTaskSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","timeJoin").equals(reader.getName())){
                                
                                                object.setTimeJoin(com.netsuite.webservices.platform.common_2017_2.TimeBillSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","transactionJoin").equals(reader.getName())){
                                
                                                object.setTransactionJoin(com.netsuite.webservices.platform.common_2017_2.TransactionSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","userJoin").equals(reader.getName())){
                                
                                                object.setUserJoin(com.netsuite.webservices.platform.common_2017_2.EmployeeSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","userNotesJoin").equals(reader.getName())){
                                
                                                object.setUserNotesJoin(com.netsuite.webservices.platform.common_2017_2.NoteSearchRowBasic.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","customSearchJoin").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    list11.add(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic.Factory.parse(reader));
                                                                
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone11 = false;
                                                        while(!loopDone11){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone11 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","customSearchJoin").equals(reader.getName())){
                                                                    list11.add(com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic.Factory.parse(reader));
                                                                        
                                                                }else{
                                                                    loopDone11 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.setCustomSearchJoin((com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                com.netsuite.webservices.platform.common_2017_2.CustomSearchRowBasic.class,
                                                                list11));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    