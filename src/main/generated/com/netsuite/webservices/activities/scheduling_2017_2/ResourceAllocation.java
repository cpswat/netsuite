
/**
 * ResourceAllocation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.activities.scheduling_2017_2;
            

            /**
            *  ResourceAllocation bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ResourceAllocation extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = ResourceAllocation
                Namespace URI = urn:scheduling_2017_2.activities.webservices.netsuite.com
                Namespace Prefix = ns9
                */
            

                        /**
                        * field for Requestedby
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localRequestedby ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRequestedbyTracker = false ;

                           public boolean isRequestedbySpecified(){
                               return localRequestedbyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getRequestedby(){
                               return localRequestedby;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Requestedby
                               */
                               public void setRequestedby(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localRequestedbyTracker = param != null;
                                   
                                            this.localRequestedby=param;
                                    

                               }
                            

                        /**
                        * field for ApprovalStatus
                        */

                        
                                    protected com.netsuite.webservices.activities.scheduling_2017_2.types.ResourceAllocationApprovalStatus localApprovalStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localApprovalStatusTracker = false ;

                           public boolean isApprovalStatusSpecified(){
                               return localApprovalStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.activities.scheduling_2017_2.types.ResourceAllocationApprovalStatus
                           */
                           public  com.netsuite.webservices.activities.scheduling_2017_2.types.ResourceAllocationApprovalStatus getApprovalStatus(){
                               return localApprovalStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ApprovalStatus
                               */
                               public void setApprovalStatus(com.netsuite.webservices.activities.scheduling_2017_2.types.ResourceAllocationApprovalStatus param){
                            localApprovalStatusTracker = param != null;
                                   
                                            this.localApprovalStatus=param;
                                    

                               }
                            

                        /**
                        * field for NextApprover
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localNextApprover ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNextApproverTracker = false ;

                           public boolean isNextApproverSpecified(){
                               return localNextApproverTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getNextApprover(){
                               return localNextApprover;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NextApprover
                               */
                               public void setNextApprover(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localNextApproverTracker = param != null;
                                   
                                            this.localNextApprover=param;
                                    

                               }
                            

                        /**
                        * field for AllocationResource
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localAllocationResource ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAllocationResourceTracker = false ;

                           public boolean isAllocationResourceSpecified(){
                               return localAllocationResourceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getAllocationResource(){
                               return localAllocationResource;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AllocationResource
                               */
                               public void setAllocationResource(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localAllocationResourceTracker = param != null;
                                   
                                            this.localAllocationResource=param;
                                    

                               }
                            

                        /**
                        * field for Project
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localProject ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProjectTracker = false ;

                           public boolean isProjectSpecified(){
                               return localProjectTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getProject(){
                               return localProject;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Project
                               */
                               public void setProject(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localProjectTracker = param != null;
                                   
                                            this.localProject=param;
                                    

                               }
                            

                        /**
                        * field for Notes
                        */

                        
                                    protected java.lang.String localNotes ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNotesTracker = false ;

                           public boolean isNotesSpecified(){
                               return localNotesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getNotes(){
                               return localNotes;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Notes
                               */
                               public void setNotes(java.lang.String param){
                            localNotesTracker = param != null;
                                   
                                            this.localNotes=param;
                                    

                               }
                            

                        /**
                        * field for StartDate
                        */

                        
                                    protected java.util.Calendar localStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateTracker = false ;

                           public boolean isStartDateSpecified(){
                               return localStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getStartDate(){
                               return localStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDate
                               */
                               public void setStartDate(java.util.Calendar param){
                            localStartDateTracker = param != null;
                                   
                                            this.localStartDate=param;
                                    

                               }
                            

                        /**
                        * field for EndDate
                        */

                        
                                    protected java.util.Calendar localEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndDateTracker = false ;

                           public boolean isEndDateSpecified(){
                               return localEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getEndDate(){
                               return localEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndDate
                               */
                               public void setEndDate(java.util.Calendar param){
                            localEndDateTracker = param != null;
                                   
                                            this.localEndDate=param;
                                    

                               }
                            

                        /**
                        * field for AllocationAmount
                        */

                        
                                    protected double localAllocationAmount ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAllocationAmountTracker = false ;

                           public boolean isAllocationAmountSpecified(){
                               return localAllocationAmountTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getAllocationAmount(){
                               return localAllocationAmount;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AllocationAmount
                               */
                               public void setAllocationAmount(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localAllocationAmountTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localAllocationAmount=param;
                                    

                               }
                            

                        /**
                        * field for AllocationUnit
                        */

                        
                                    protected com.netsuite.webservices.activities.scheduling_2017_2.types.ResourceAllocationAllocationUnit localAllocationUnit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAllocationUnitTracker = false ;

                           public boolean isAllocationUnitSpecified(){
                               return localAllocationUnitTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.activities.scheduling_2017_2.types.ResourceAllocationAllocationUnit
                           */
                           public  com.netsuite.webservices.activities.scheduling_2017_2.types.ResourceAllocationAllocationUnit getAllocationUnit(){
                               return localAllocationUnit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AllocationUnit
                               */
                               public void setAllocationUnit(com.netsuite.webservices.activities.scheduling_2017_2.types.ResourceAllocationAllocationUnit param){
                            localAllocationUnitTracker = param != null;
                                   
                                            this.localAllocationUnit=param;
                                    

                               }
                            

                        /**
                        * field for NumberHours
                        */

                        
                                    protected double localNumberHours ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNumberHoursTracker = false ;

                           public boolean isNumberHoursSpecified(){
                               return localNumberHoursTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getNumberHours(){
                               return localNumberHours;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NumberHours
                               */
                               public void setNumberHours(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localNumberHoursTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localNumberHours=param;
                                    

                               }
                            

                        /**
                        * field for PercentOfTime
                        */

                        
                                    protected double localPercentOfTime ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPercentOfTimeTracker = false ;

                           public boolean isPercentOfTimeSpecified(){
                               return localPercentOfTimeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return double
                           */
                           public  double getPercentOfTime(){
                               return localPercentOfTime;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PercentOfTime
                               */
                               public void setPercentOfTime(double param){
                            
                                       // setting primitive attribute tracker to true
                                       localPercentOfTimeTracker =
                                       !java.lang.Double.isNaN(param);
                                   
                                            this.localPercentOfTime=param;
                                    

                               }
                            

                        /**
                        * field for AllocationType
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localAllocationType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAllocationTypeTracker = false ;

                           public boolean isAllocationTypeSpecified(){
                               return localAllocationTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getAllocationType(){
                               return localAllocationType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AllocationType
                               */
                               public void setAllocationType(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localAllocationTypeTracker = param != null;
                                   
                                            this.localAllocationType=param;
                                    

                               }
                            

                        /**
                        * field for CustomForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFormTracker = false ;

                           public boolean isCustomFormSpecified(){
                               return localCustomFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomForm(){
                               return localCustomForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomForm
                               */
                               public void setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomFormTracker = param != null;
                                   
                                            this.localCustomForm=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:scheduling_2017_2.activities.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":ResourceAllocation",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "ResourceAllocation",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localRequestedbyTracker){
                                            if (localRequestedby==null){
                                                 throw new org.apache.axis2.databinding.ADBException("requestedby cannot be null!!");
                                            }
                                           localRequestedby.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","requestedby"),
                                               xmlWriter);
                                        } if (localApprovalStatusTracker){
                                            if (localApprovalStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("approvalStatus cannot be null!!");
                                            }
                                           localApprovalStatus.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","approvalStatus"),
                                               xmlWriter);
                                        } if (localNextApproverTracker){
                                            if (localNextApprover==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nextApprover cannot be null!!");
                                            }
                                           localNextApprover.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","nextApprover"),
                                               xmlWriter);
                                        } if (localAllocationResourceTracker){
                                            if (localAllocationResource==null){
                                                 throw new org.apache.axis2.databinding.ADBException("allocationResource cannot be null!!");
                                            }
                                           localAllocationResource.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","allocationResource"),
                                               xmlWriter);
                                        } if (localProjectTracker){
                                            if (localProject==null){
                                                 throw new org.apache.axis2.databinding.ADBException("project cannot be null!!");
                                            }
                                           localProject.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","project"),
                                               xmlWriter);
                                        } if (localNotesTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "notes", xmlWriter);
                             

                                          if (localNotes==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("notes cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localNotes);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStartDateTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "startDate", xmlWriter);
                             

                                          if (localStartDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEndDateTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "endDate", xmlWriter);
                             

                                          if (localEndDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAllocationAmountTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "allocationAmount", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localAllocationAmount)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("allocationAmount cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAllocationAmount));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAllocationUnitTracker){
                                            if (localAllocationUnit==null){
                                                 throw new org.apache.axis2.databinding.ADBException("allocationUnit cannot be null!!");
                                            }
                                           localAllocationUnit.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","allocationUnit"),
                                               xmlWriter);
                                        } if (localNumberHoursTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "numberHours", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localNumberHours)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("numberHours cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumberHours));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPercentOfTimeTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "percentOfTime", xmlWriter);
                             
                                               if (java.lang.Double.isNaN(localPercentOfTime)) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("percentOfTime cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPercentOfTime));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAllocationTypeTracker){
                                            if (localAllocationType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("allocationType cannot be null!!");
                                            }
                                           localAllocationType.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","allocationType"),
                                               xmlWriter);
                                        } if (localCustomFormTracker){
                                            if (localCustomForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                            }
                                           localCustomForm.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","customForm"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:scheduling_2017_2.activities.webservices.netsuite.com")){
                return "ns9";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","ResourceAllocation"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localRequestedbyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "requestedby"));
                            
                            
                                    if (localRequestedby==null){
                                         throw new org.apache.axis2.databinding.ADBException("requestedby cannot be null!!");
                                    }
                                    elementList.add(localRequestedby);
                                } if (localApprovalStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "approvalStatus"));
                            
                            
                                    if (localApprovalStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("approvalStatus cannot be null!!");
                                    }
                                    elementList.add(localApprovalStatus);
                                } if (localNextApproverTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "nextApprover"));
                            
                            
                                    if (localNextApprover==null){
                                         throw new org.apache.axis2.databinding.ADBException("nextApprover cannot be null!!");
                                    }
                                    elementList.add(localNextApprover);
                                } if (localAllocationResourceTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "allocationResource"));
                            
                            
                                    if (localAllocationResource==null){
                                         throw new org.apache.axis2.databinding.ADBException("allocationResource cannot be null!!");
                                    }
                                    elementList.add(localAllocationResource);
                                } if (localProjectTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "project"));
                            
                            
                                    if (localProject==null){
                                         throw new org.apache.axis2.databinding.ADBException("project cannot be null!!");
                                    }
                                    elementList.add(localProject);
                                } if (localNotesTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "notes"));
                                 
                                        if (localNotes != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNotes));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("notes cannot be null!!");
                                        }
                                    } if (localStartDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "startDate"));
                                 
                                        if (localStartDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                        }
                                    } if (localEndDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "endDate"));
                                 
                                        if (localEndDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                        }
                                    } if (localAllocationAmountTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "allocationAmount"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAllocationAmount));
                            } if (localAllocationUnitTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "allocationUnit"));
                            
                            
                                    if (localAllocationUnit==null){
                                         throw new org.apache.axis2.databinding.ADBException("allocationUnit cannot be null!!");
                                    }
                                    elementList.add(localAllocationUnit);
                                } if (localNumberHoursTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "numberHours"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNumberHours));
                            } if (localPercentOfTimeTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "percentOfTime"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPercentOfTime));
                            } if (localAllocationTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "allocationType"));
                            
                            
                                    if (localAllocationType==null){
                                         throw new org.apache.axis2.databinding.ADBException("allocationType cannot be null!!");
                                    }
                                    elementList.add(localAllocationType);
                                } if (localCustomFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "customForm"));
                            
                            
                                    if (localCustomForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                    }
                                    elementList.add(localCustomForm);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static ResourceAllocation parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            ResourceAllocation object =
                new ResourceAllocation();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"ResourceAllocation".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ResourceAllocation)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","requestedby").equals(reader.getName())){
                                
                                                object.setRequestedby(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","approvalStatus").equals(reader.getName())){
                                
                                                object.setApprovalStatus(com.netsuite.webservices.activities.scheduling_2017_2.types.ResourceAllocationApprovalStatus.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","nextApprover").equals(reader.getName())){
                                
                                                object.setNextApprover(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","allocationResource").equals(reader.getName())){
                                
                                                object.setAllocationResource(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","project").equals(reader.getName())){
                                
                                                object.setProject(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","notes").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"notes" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNotes(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","startDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"startDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStartDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","endDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"endDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEndDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","allocationAmount").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"allocationAmount" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAllocationAmount(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setAllocationAmount(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","allocationUnit").equals(reader.getName())){
                                
                                                object.setAllocationUnit(com.netsuite.webservices.activities.scheduling_2017_2.types.ResourceAllocationAllocationUnit.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","numberHours").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"numberHours" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNumberHours(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setNumberHours(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","percentOfTime").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"percentOfTime" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPercentOfTime(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDouble(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPercentOfTime(java.lang.Double.NaN);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","allocationType").equals(reader.getName())){
                                
                                                object.setAllocationType(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","customForm").equals(reader.getName())){
                                
                                                object.setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    