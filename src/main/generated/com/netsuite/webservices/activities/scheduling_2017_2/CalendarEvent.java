
/**
 * CalendarEvent.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.activities.scheduling_2017_2;
            

            /**
            *  CalendarEvent bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class CalendarEvent extends com.netsuite.webservices.platform.core_2017_2.Record
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = CalendarEvent
                Namespace URI = urn:scheduling_2017_2.activities.webservices.netsuite.com
                Namespace Prefix = ns9
                */
            

                        /**
                        * field for Company
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCompany ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCompanyTracker = false ;

                           public boolean isCompanySpecified(){
                               return localCompanyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCompany(){
                               return localCompany;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Company
                               */
                               public void setCompany(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCompanyTracker = param != null;
                                   
                                            this.localCompany=param;
                                    

                               }
                            

                        /**
                        * field for Contact
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localContact ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localContactTracker = false ;

                           public boolean isContactSpecified(){
                               return localContactTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getContact(){
                               return localContact;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Contact
                               */
                               public void setContact(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localContactTracker = param != null;
                                   
                                            this.localContact=param;
                                    

                               }
                            

                        /**
                        * field for SupportCase
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localSupportCase ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSupportCaseTracker = false ;

                           public boolean isSupportCaseSpecified(){
                               return localSupportCaseTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getSupportCase(){
                               return localSupportCase;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SupportCase
                               */
                               public void setSupportCase(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localSupportCaseTracker = param != null;
                                   
                                            this.localSupportCase=param;
                                    

                               }
                            

                        /**
                        * field for Transaction
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localTransaction ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTransactionTracker = false ;

                           public boolean isTransactionSpecified(){
                               return localTransactionTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getTransaction(){
                               return localTransaction;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Transaction
                               */
                               public void setTransaction(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localTransactionTracker = param != null;
                                   
                                            this.localTransaction=param;
                                    

                               }
                            

                        /**
                        * field for Period
                        */

                        
                                    protected long localPeriod ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPeriodTracker = false ;

                           public boolean isPeriodSpecified(){
                               return localPeriodTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getPeriod(){
                               return localPeriod;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Period
                               */
                               public void setPeriod(long param){
                            
                                       // setting primitive attribute tracker to true
                                       localPeriodTracker =
                                       param != java.lang.Long.MIN_VALUE;
                                   
                                            this.localPeriod=param;
                                    

                               }
                            

                        /**
                        * field for Frequency
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.RecurrenceFrequency localFrequency ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localFrequencyTracker = false ;

                           public boolean isFrequencySpecified(){
                               return localFrequencyTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.RecurrenceFrequency
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.RecurrenceFrequency getFrequency(){
                               return localFrequency;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Frequency
                               */
                               public void setFrequency(com.netsuite.webservices.platform.common_2017_2.types.RecurrenceFrequency param){
                            localFrequencyTracker = param != null;
                                   
                                            this.localFrequency=param;
                                    

                               }
                            

                        /**
                        * field for RecurrenceDowMaskList
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.RecurrenceDowMaskList localRecurrenceDowMaskList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecurrenceDowMaskListTracker = false ;

                           public boolean isRecurrenceDowMaskListSpecified(){
                               return localRecurrenceDowMaskListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.RecurrenceDowMaskList
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.RecurrenceDowMaskList getRecurrenceDowMaskList(){
                               return localRecurrenceDowMaskList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecurrenceDowMaskList
                               */
                               public void setRecurrenceDowMaskList(com.netsuite.webservices.platform.common_2017_2.RecurrenceDowMaskList param){
                            localRecurrenceDowMaskListTracker = param != null;
                                   
                                            this.localRecurrenceDowMaskList=param;
                                    

                               }
                            

                        /**
                        * field for RecurrenceDow
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.RecurrenceDow localRecurrenceDow ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecurrenceDowTracker = false ;

                           public boolean isRecurrenceDowSpecified(){
                               return localRecurrenceDowTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.RecurrenceDow
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.RecurrenceDow getRecurrenceDow(){
                               return localRecurrenceDow;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecurrenceDow
                               */
                               public void setRecurrenceDow(com.netsuite.webservices.platform.common_2017_2.types.RecurrenceDow param){
                            localRecurrenceDowTracker = param != null;
                                   
                                            this.localRecurrenceDow=param;
                                    

                               }
                            

                        /**
                        * field for RecurrenceDowim
                        */

                        
                                    protected com.netsuite.webservices.platform.common_2017_2.types.RecurrenceDowim localRecurrenceDowim ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecurrenceDowimTracker = false ;

                           public boolean isRecurrenceDowimSpecified(){
                               return localRecurrenceDowimTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.common_2017_2.types.RecurrenceDowim
                           */
                           public  com.netsuite.webservices.platform.common_2017_2.types.RecurrenceDowim getRecurrenceDowim(){
                               return localRecurrenceDowim;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecurrenceDowim
                               */
                               public void setRecurrenceDowim(com.netsuite.webservices.platform.common_2017_2.types.RecurrenceDowim param){
                            localRecurrenceDowimTracker = param != null;
                                   
                                            this.localRecurrenceDowim=param;
                                    

                               }
                            

                        /**
                        * field for SeriesStartDate
                        */

                        
                                    protected java.util.Calendar localSeriesStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSeriesStartDateTracker = false ;

                           public boolean isSeriesStartDateSpecified(){
                               return localSeriesStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getSeriesStartDate(){
                               return localSeriesStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SeriesStartDate
                               */
                               public void setSeriesStartDate(java.util.Calendar param){
                            localSeriesStartDateTracker = param != null;
                                   
                                            this.localSeriesStartDate=param;
                                    

                               }
                            

                        /**
                        * field for EndByDate
                        */

                        
                                    protected java.util.Calendar localEndByDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndByDateTracker = false ;

                           public boolean isEndByDateSpecified(){
                               return localEndByDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getEndByDate(){
                               return localEndByDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndByDate
                               */
                               public void setEndByDate(java.util.Calendar param){
                            localEndByDateTracker = param != null;
                                   
                                            this.localEndByDate=param;
                                    

                               }
                            

                        /**
                        * field for NoEndDate
                        */

                        
                                    protected boolean localNoEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNoEndDateTracker = false ;

                           public boolean isNoEndDateSpecified(){
                               return localNoEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getNoEndDate(){
                               return localNoEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param NoEndDate
                               */
                               public void setNoEndDate(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localNoEndDateTracker =
                                       true;
                                   
                                            this.localNoEndDate=param;
                                    

                               }
                            

                        /**
                        * field for SendEmail
                        */

                        
                                    protected boolean localSendEmail ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSendEmailTracker = false ;

                           public boolean isSendEmailSpecified(){
                               return localSendEmailTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getSendEmail(){
                               return localSendEmail;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param SendEmail
                               */
                               public void setSendEmail(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localSendEmailTracker =
                                       true;
                                   
                                            this.localSendEmail=param;
                                    

                               }
                            

                        /**
                        * field for CustomForm
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localCustomForm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFormTracker = false ;

                           public boolean isCustomFormSpecified(){
                               return localCustomFormTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getCustomForm(){
                               return localCustomForm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomForm
                               */
                               public void setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localCustomFormTracker = param != null;
                                   
                                            this.localCustomForm=param;
                                    

                               }
                            

                        /**
                        * field for Title
                        */

                        
                                    protected java.lang.String localTitle ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTitleTracker = false ;

                           public boolean isTitleSpecified(){
                               return localTitleTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getTitle(){
                               return localTitle;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Title
                               */
                               public void setTitle(java.lang.String param){
                            localTitleTracker = param != null;
                                   
                                            this.localTitle=param;
                                    

                               }
                            

                        /**
                        * field for Recurrence
                        */

                        
                                    protected java.lang.String localRecurrence ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecurrenceTracker = false ;

                           public boolean isRecurrenceSpecified(){
                               return localRecurrenceTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getRecurrence(){
                               return localRecurrence;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Recurrence
                               */
                               public void setRecurrence(java.lang.String param){
                            localRecurrenceTracker = param != null;
                                   
                                            this.localRecurrence=param;
                                    

                               }
                            

                        /**
                        * field for Location
                        */

                        
                                    protected java.lang.String localLocation ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLocationTracker = false ;

                           public boolean isLocationSpecified(){
                               return localLocationTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getLocation(){
                               return localLocation;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Location
                               */
                               public void setLocation(java.lang.String param){
                            localLocationTracker = param != null;
                                   
                                            this.localLocation=param;
                                    

                               }
                            

                        /**
                        * field for StartDate
                        */

                        
                                    protected java.util.Calendar localStartDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStartDateTracker = false ;

                           public boolean isStartDateSpecified(){
                               return localStartDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getStartDate(){
                               return localStartDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param StartDate
                               */
                               public void setStartDate(java.util.Calendar param){
                            localStartDateTracker = param != null;
                                   
                                            this.localStartDate=param;
                                    

                               }
                            

                        /**
                        * field for AllDayEvent
                        */

                        
                                    protected boolean localAllDayEvent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAllDayEventTracker = false ;

                           public boolean isAllDayEventSpecified(){
                               return localAllDayEventTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getAllDayEvent(){
                               return localAllDayEvent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AllDayEvent
                               */
                               public void setAllDayEvent(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localAllDayEventTracker =
                                       true;
                                   
                                            this.localAllDayEvent=param;
                                    

                               }
                            

                        /**
                        * field for TimedEvent
                        */

                        
                                    protected boolean localTimedEvent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimedEventTracker = false ;

                           public boolean isTimedEventSpecified(){
                               return localTimedEventTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getTimedEvent(){
                               return localTimedEvent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimedEvent
                               */
                               public void setTimedEvent(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localTimedEventTracker =
                                       true;
                                   
                                            this.localTimedEvent=param;
                                    

                               }
                            

                        /**
                        * field for ReminderType
                        */

                        
                                    protected com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventReminderType localReminderType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReminderTypeTracker = false ;

                           public boolean isReminderTypeSpecified(){
                               return localReminderTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventReminderType
                           */
                           public  com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventReminderType getReminderType(){
                               return localReminderType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReminderType
                               */
                               public void setReminderType(com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventReminderType param){
                            localReminderTypeTracker = param != null;
                                   
                                            this.localReminderType=param;
                                    

                               }
                            

                        /**
                        * field for ReminderMinutes
                        */

                        
                                    protected com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventReminderMinutes localReminderMinutes ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localReminderMinutesTracker = false ;

                           public boolean isReminderMinutesSpecified(){
                               return localReminderMinutesTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventReminderMinutes
                           */
                           public  com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventReminderMinutes getReminderMinutes(){
                               return localReminderMinutes;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ReminderMinutes
                               */
                               public void setReminderMinutes(com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventReminderMinutes param){
                            localReminderMinutesTracker = param != null;
                                   
                                            this.localReminderMinutes=param;
                                    

                               }
                            

                        /**
                        * field for Status
                        */

                        
                                    protected com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventStatus localStatus ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStatusTracker = false ;

                           public boolean isStatusSpecified(){
                               return localStatusTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventStatus
                           */
                           public  com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventStatus getStatus(){
                               return localStatus;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Status
                               */
                               public void setStatus(com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventStatus param){
                            localStatusTracker = param != null;
                                   
                                            this.localStatus=param;
                                    

                               }
                            

                        /**
                        * field for AccessLevel
                        */

                        
                                    protected com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventAccessLevel localAccessLevel ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAccessLevelTracker = false ;

                           public boolean isAccessLevelSpecified(){
                               return localAccessLevelTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventAccessLevel
                           */
                           public  com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventAccessLevel getAccessLevel(){
                               return localAccessLevel;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AccessLevel
                               */
                               public void setAccessLevel(com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventAccessLevel param){
                            localAccessLevelTracker = param != null;
                                   
                                            this.localAccessLevel=param;
                                    

                               }
                            

                        /**
                        * field for Organizer
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localOrganizer ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOrganizerTracker = false ;

                           public boolean isOrganizerSpecified(){
                               return localOrganizerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getOrganizer(){
                               return localOrganizer;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Organizer
                               */
                               public void setOrganizer(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localOrganizerTracker = param != null;
                                   
                                            this.localOrganizer=param;
                                    

                               }
                            

                        /**
                        * field for Message
                        */

                        
                                    protected java.lang.String localMessage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMessageTracker = false ;

                           public boolean isMessageSpecified(){
                               return localMessageTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMessage(){
                               return localMessage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Message
                               */
                               public void setMessage(java.lang.String param){
                            localMessageTracker = param != null;
                                   
                                            this.localMessage=param;
                                    

                               }
                            

                        /**
                        * field for CreatedDate
                        */

                        
                                    protected java.util.Calendar localCreatedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreatedDateTracker = false ;

                           public boolean isCreatedDateSpecified(){
                               return localCreatedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getCreatedDate(){
                               return localCreatedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreatedDate
                               */
                               public void setCreatedDate(java.util.Calendar param){
                            localCreatedDateTracker = param != null;
                                   
                                            this.localCreatedDate=param;
                                    

                               }
                            

                        /**
                        * field for EndDate
                        */

                        
                                    protected java.util.Calendar localEndDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localEndDateTracker = false ;

                           public boolean isEndDateSpecified(){
                               return localEndDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getEndDate(){
                               return localEndDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param EndDate
                               */
                               public void setEndDate(java.util.Calendar param){
                            localEndDateTracker = param != null;
                                   
                                            this.localEndDate=param;
                                    

                               }
                            

                        /**
                        * field for ExclusionDateList
                        */

                        
                                    protected com.netsuite.webservices.activities.scheduling_2017_2.ExclusionDateList localExclusionDateList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localExclusionDateListTracker = false ;

                           public boolean isExclusionDateListSpecified(){
                               return localExclusionDateListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.activities.scheduling_2017_2.ExclusionDateList
                           */
                           public  com.netsuite.webservices.activities.scheduling_2017_2.ExclusionDateList getExclusionDateList(){
                               return localExclusionDateList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExclusionDateList
                               */
                               public void setExclusionDateList(com.netsuite.webservices.activities.scheduling_2017_2.ExclusionDateList param){
                            localExclusionDateListTracker = param != null;
                                   
                                            this.localExclusionDateList=param;
                                    

                               }
                            

                        /**
                        * field for LastModifiedDate
                        */

                        
                                    protected java.util.Calendar localLastModifiedDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localLastModifiedDateTracker = false ;

                           public boolean isLastModifiedDateSpecified(){
                               return localLastModifiedDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.util.Calendar
                           */
                           public  java.util.Calendar getLastModifiedDate(){
                               return localLastModifiedDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param LastModifiedDate
                               */
                               public void setLastModifiedDate(java.util.Calendar param){
                            localLastModifiedDateTracker = param != null;
                                   
                                            this.localLastModifiedDate=param;
                                    

                               }
                            

                        /**
                        * field for Owner
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.RecordRef localOwner ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOwnerTracker = false ;

                           public boolean isOwnerSpecified(){
                               return localOwnerTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.RecordRef
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.RecordRef getOwner(){
                               return localOwner;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Owner
                               */
                               public void setOwner(com.netsuite.webservices.platform.core_2017_2.RecordRef param){
                            localOwnerTracker = param != null;
                                   
                                            this.localOwner=param;
                                    

                               }
                            

                        /**
                        * field for AttendeeList
                        */

                        
                                    protected com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventAttendeeList localAttendeeList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAttendeeListTracker = false ;

                           public boolean isAttendeeListSpecified(){
                               return localAttendeeListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventAttendeeList
                           */
                           public  com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventAttendeeList getAttendeeList(){
                               return localAttendeeList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param AttendeeList
                               */
                               public void setAttendeeList(com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventAttendeeList param){
                            localAttendeeListTracker = param != null;
                                   
                                            this.localAttendeeList=param;
                                    

                               }
                            

                        /**
                        * field for ResourceList
                        */

                        
                                    protected com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventResourceList localResourceList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localResourceListTracker = false ;

                           public boolean isResourceListSpecified(){
                               return localResourceListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventResourceList
                           */
                           public  com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventResourceList getResourceList(){
                               return localResourceList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ResourceList
                               */
                               public void setResourceList(com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventResourceList param){
                            localResourceListTracker = param != null;
                                   
                                            this.localResourceList=param;
                                    

                               }
                            

                        /**
                        * field for TimeItemList
                        */

                        
                                    protected com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventTimeItemList localTimeItemList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTimeItemListTracker = false ;

                           public boolean isTimeItemListSpecified(){
                               return localTimeItemListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventTimeItemList
                           */
                           public  com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventTimeItemList getTimeItemList(){
                               return localTimeItemList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param TimeItemList
                               */
                               public void setTimeItemList(com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventTimeItemList param){
                            localTimeItemListTracker = param != null;
                                   
                                            this.localTimeItemList=param;
                                    

                               }
                            

                        /**
                        * field for CustomFieldList
                        */

                        
                                    protected com.netsuite.webservices.platform.core_2017_2.CustomFieldList localCustomFieldList ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCustomFieldListTracker = false ;

                           public boolean isCustomFieldListSpecified(){
                               return localCustomFieldListTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return com.netsuite.webservices.platform.core_2017_2.CustomFieldList
                           */
                           public  com.netsuite.webservices.platform.core_2017_2.CustomFieldList getCustomFieldList(){
                               return localCustomFieldList;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CustomFieldList
                               */
                               public void setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList param){
                            localCustomFieldListTracker = param != null;
                                   
                                            this.localCustomFieldList=param;
                                    

                               }
                            

                        /**
                        * field for InternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localInternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getInternalId(){
                               return localInternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InternalId
                               */
                               public void setInternalId(java.lang.String param){
                            
                                            this.localInternalId=param;
                                    

                               }
                            

                        /**
                        * field for ExternalId
                        * This was an Attribute!
                        */

                        
                                    protected java.lang.String localExternalId ;
                                

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getExternalId(){
                               return localExternalId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ExternalId
                               */
                               public void setExternalId(java.lang.String param){
                            
                                            this.localExternalId=param;
                                    

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName);
               return factory.createOMElement(dataSource,parentQName);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:scheduling_2017_2.activities.webservices.netsuite.com");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":CalendarEvent",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "CalendarEvent",
                           xmlWriter);
                   }

               
                                            if (localInternalId != null){
                                        
                                                writeAttribute("",
                                                         "internalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId), xmlWriter);

                                            
                                      }
                                    
                                            if (localExternalId != null){
                                        
                                                writeAttribute("",
                                                         "externalId",
                                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId), xmlWriter);

                                            
                                      }
                                     if (localNullFieldListTracker){
                                            if (localNullFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                            }
                                           localNullFieldList.serialize(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList"),
                                               xmlWriter);
                                        } if (localCompanyTracker){
                                            if (localCompany==null){
                                                 throw new org.apache.axis2.databinding.ADBException("company cannot be null!!");
                                            }
                                           localCompany.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","company"),
                                               xmlWriter);
                                        } if (localContactTracker){
                                            if (localContact==null){
                                                 throw new org.apache.axis2.databinding.ADBException("contact cannot be null!!");
                                            }
                                           localContact.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","contact"),
                                               xmlWriter);
                                        } if (localSupportCaseTracker){
                                            if (localSupportCase==null){
                                                 throw new org.apache.axis2.databinding.ADBException("supportCase cannot be null!!");
                                            }
                                           localSupportCase.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","supportCase"),
                                               xmlWriter);
                                        } if (localTransactionTracker){
                                            if (localTransaction==null){
                                                 throw new org.apache.axis2.databinding.ADBException("transaction cannot be null!!");
                                            }
                                           localTransaction.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","transaction"),
                                               xmlWriter);
                                        } if (localPeriodTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "period", xmlWriter);
                             
                                               if (localPeriod==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("period cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriod));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localFrequencyTracker){
                                            if (localFrequency==null){
                                                 throw new org.apache.axis2.databinding.ADBException("frequency cannot be null!!");
                                            }
                                           localFrequency.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","frequency"),
                                               xmlWriter);
                                        } if (localRecurrenceDowMaskListTracker){
                                            if (localRecurrenceDowMaskList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("recurrenceDowMaskList cannot be null!!");
                                            }
                                           localRecurrenceDowMaskList.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","recurrenceDowMaskList"),
                                               xmlWriter);
                                        } if (localRecurrenceDowTracker){
                                            if (localRecurrenceDow==null){
                                                 throw new org.apache.axis2.databinding.ADBException("recurrenceDow cannot be null!!");
                                            }
                                           localRecurrenceDow.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","recurrenceDow"),
                                               xmlWriter);
                                        } if (localRecurrenceDowimTracker){
                                            if (localRecurrenceDowim==null){
                                                 throw new org.apache.axis2.databinding.ADBException("recurrenceDowim cannot be null!!");
                                            }
                                           localRecurrenceDowim.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","recurrenceDowim"),
                                               xmlWriter);
                                        } if (localSeriesStartDateTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "seriesStartDate", xmlWriter);
                             

                                          if (localSeriesStartDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("seriesStartDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSeriesStartDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEndByDateTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "endByDate", xmlWriter);
                             

                                          if (localEndByDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("endByDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndByDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localNoEndDateTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "noEndDate", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("noEndDate cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNoEndDate));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localSendEmailTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "sendEmail", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("sendEmail cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSendEmail));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCustomFormTracker){
                                            if (localCustomForm==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                            }
                                           localCustomForm.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","customForm"),
                                               xmlWriter);
                                        } if (localTitleTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "title", xmlWriter);
                             

                                          if (localTitle==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localTitle);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRecurrenceTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "recurrence", xmlWriter);
                             

                                          if (localRecurrence==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("recurrence cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localRecurrence);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localLocationTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "location", xmlWriter);
                             

                                          if (localLocation==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localLocation);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStartDateTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "startDate", xmlWriter);
                             

                                          if (localStartDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAllDayEventTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "allDayEvent", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("allDayEvent cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAllDayEvent));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTimedEventTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "timedEvent", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("timedEvent cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTimedEvent));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localReminderTypeTracker){
                                            if (localReminderType==null){
                                                 throw new org.apache.axis2.databinding.ADBException("reminderType cannot be null!!");
                                            }
                                           localReminderType.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","reminderType"),
                                               xmlWriter);
                                        } if (localReminderMinutesTracker){
                                            if (localReminderMinutes==null){
                                                 throw new org.apache.axis2.databinding.ADBException("reminderMinutes cannot be null!!");
                                            }
                                           localReminderMinutes.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","reminderMinutes"),
                                               xmlWriter);
                                        } if (localStatusTracker){
                                            if (localStatus==null){
                                                 throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                            }
                                           localStatus.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","status"),
                                               xmlWriter);
                                        } if (localAccessLevelTracker){
                                            if (localAccessLevel==null){
                                                 throw new org.apache.axis2.databinding.ADBException("accessLevel cannot be null!!");
                                            }
                                           localAccessLevel.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","accessLevel"),
                                               xmlWriter);
                                        } if (localOrganizerTracker){
                                            if (localOrganizer==null){
                                                 throw new org.apache.axis2.databinding.ADBException("organizer cannot be null!!");
                                            }
                                           localOrganizer.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","organizer"),
                                               xmlWriter);
                                        } if (localMessageTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "message", xmlWriter);
                             

                                          if (localMessage==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("message cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMessage);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCreatedDateTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "createdDate", xmlWriter);
                             

                                          if (localCreatedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localEndDateTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "endDate", xmlWriter);
                             

                                          if (localEndDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localExclusionDateListTracker){
                                            if (localExclusionDateList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("exclusionDateList cannot be null!!");
                                            }
                                           localExclusionDateList.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","exclusionDateList"),
                                               xmlWriter);
                                        } if (localLastModifiedDateTracker){
                                    namespace = "urn:scheduling_2017_2.activities.webservices.netsuite.com";
                                    writeStartElement(null, namespace, "lastModifiedDate", xmlWriter);
                             

                                          if (localLastModifiedDate==null){
                                              // write the nil attribute
                                              
                                                     throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOwnerTracker){
                                            if (localOwner==null){
                                                 throw new org.apache.axis2.databinding.ADBException("owner cannot be null!!");
                                            }
                                           localOwner.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","owner"),
                                               xmlWriter);
                                        } if (localAttendeeListTracker){
                                            if (localAttendeeList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("attendeeList cannot be null!!");
                                            }
                                           localAttendeeList.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","attendeeList"),
                                               xmlWriter);
                                        } if (localResourceListTracker){
                                            if (localResourceList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("resourceList cannot be null!!");
                                            }
                                           localResourceList.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","resourceList"),
                                               xmlWriter);
                                        } if (localTimeItemListTracker){
                                            if (localTimeItemList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("timeItemList cannot be null!!");
                                            }
                                           localTimeItemList.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","timeItemList"),
                                               xmlWriter);
                                        } if (localCustomFieldListTracker){
                                            if (localCustomFieldList==null){
                                                 throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                            }
                                           localCustomFieldList.serialize(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","customFieldList"),
                                               xmlWriter);
                                        }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:scheduling_2017_2.activities.webservices.netsuite.com")){
                return "ns9";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                
                    attribList.add(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema-instance","type"));
                    attribList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","CalendarEvent"));
                 if (localNullFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com",
                                                                      "nullFieldList"));
                            
                            
                                    if (localNullFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("nullFieldList cannot be null!!");
                                    }
                                    elementList.add(localNullFieldList);
                                } if (localCompanyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "company"));
                            
                            
                                    if (localCompany==null){
                                         throw new org.apache.axis2.databinding.ADBException("company cannot be null!!");
                                    }
                                    elementList.add(localCompany);
                                } if (localContactTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "contact"));
                            
                            
                                    if (localContact==null){
                                         throw new org.apache.axis2.databinding.ADBException("contact cannot be null!!");
                                    }
                                    elementList.add(localContact);
                                } if (localSupportCaseTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "supportCase"));
                            
                            
                                    if (localSupportCase==null){
                                         throw new org.apache.axis2.databinding.ADBException("supportCase cannot be null!!");
                                    }
                                    elementList.add(localSupportCase);
                                } if (localTransactionTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "transaction"));
                            
                            
                                    if (localTransaction==null){
                                         throw new org.apache.axis2.databinding.ADBException("transaction cannot be null!!");
                                    }
                                    elementList.add(localTransaction);
                                } if (localPeriodTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "period"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPeriod));
                            } if (localFrequencyTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "frequency"));
                            
                            
                                    if (localFrequency==null){
                                         throw new org.apache.axis2.databinding.ADBException("frequency cannot be null!!");
                                    }
                                    elementList.add(localFrequency);
                                } if (localRecurrenceDowMaskListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "recurrenceDowMaskList"));
                            
                            
                                    if (localRecurrenceDowMaskList==null){
                                         throw new org.apache.axis2.databinding.ADBException("recurrenceDowMaskList cannot be null!!");
                                    }
                                    elementList.add(localRecurrenceDowMaskList);
                                } if (localRecurrenceDowTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "recurrenceDow"));
                            
                            
                                    if (localRecurrenceDow==null){
                                         throw new org.apache.axis2.databinding.ADBException("recurrenceDow cannot be null!!");
                                    }
                                    elementList.add(localRecurrenceDow);
                                } if (localRecurrenceDowimTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "recurrenceDowim"));
                            
                            
                                    if (localRecurrenceDowim==null){
                                         throw new org.apache.axis2.databinding.ADBException("recurrenceDowim cannot be null!!");
                                    }
                                    elementList.add(localRecurrenceDowim);
                                } if (localSeriesStartDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "seriesStartDate"));
                                 
                                        if (localSeriesStartDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSeriesStartDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("seriesStartDate cannot be null!!");
                                        }
                                    } if (localEndByDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "endByDate"));
                                 
                                        if (localEndByDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndByDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("endByDate cannot be null!!");
                                        }
                                    } if (localNoEndDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "noEndDate"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localNoEndDate));
                            } if (localSendEmailTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "sendEmail"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSendEmail));
                            } if (localCustomFormTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "customForm"));
                            
                            
                                    if (localCustomForm==null){
                                         throw new org.apache.axis2.databinding.ADBException("customForm cannot be null!!");
                                    }
                                    elementList.add(localCustomForm);
                                } if (localTitleTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "title"));
                                 
                                        if (localTitle != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTitle));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("title cannot be null!!");
                                        }
                                    } if (localRecurrenceTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "recurrence"));
                                 
                                        if (localRecurrence != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecurrence));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("recurrence cannot be null!!");
                                        }
                                    } if (localLocationTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "location"));
                                 
                                        if (localLocation != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLocation));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("location cannot be null!!");
                                        }
                                    } if (localStartDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "startDate"));
                                 
                                        if (localStartDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStartDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("startDate cannot be null!!");
                                        }
                                    } if (localAllDayEventTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "allDayEvent"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAllDayEvent));
                            } if (localTimedEventTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "timedEvent"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTimedEvent));
                            } if (localReminderTypeTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "reminderType"));
                            
                            
                                    if (localReminderType==null){
                                         throw new org.apache.axis2.databinding.ADBException("reminderType cannot be null!!");
                                    }
                                    elementList.add(localReminderType);
                                } if (localReminderMinutesTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "reminderMinutes"));
                            
                            
                                    if (localReminderMinutes==null){
                                         throw new org.apache.axis2.databinding.ADBException("reminderMinutes cannot be null!!");
                                    }
                                    elementList.add(localReminderMinutes);
                                } if (localStatusTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "status"));
                            
                            
                                    if (localStatus==null){
                                         throw new org.apache.axis2.databinding.ADBException("status cannot be null!!");
                                    }
                                    elementList.add(localStatus);
                                } if (localAccessLevelTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "accessLevel"));
                            
                            
                                    if (localAccessLevel==null){
                                         throw new org.apache.axis2.databinding.ADBException("accessLevel cannot be null!!");
                                    }
                                    elementList.add(localAccessLevel);
                                } if (localOrganizerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "organizer"));
                            
                            
                                    if (localOrganizer==null){
                                         throw new org.apache.axis2.databinding.ADBException("organizer cannot be null!!");
                                    }
                                    elementList.add(localOrganizer);
                                } if (localMessageTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "message"));
                                 
                                        if (localMessage != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMessage));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("message cannot be null!!");
                                        }
                                    } if (localCreatedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "createdDate"));
                                 
                                        if (localCreatedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCreatedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("createdDate cannot be null!!");
                                        }
                                    } if (localEndDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "endDate"));
                                 
                                        if (localEndDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEndDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("endDate cannot be null!!");
                                        }
                                    } if (localExclusionDateListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "exclusionDateList"));
                            
                            
                                    if (localExclusionDateList==null){
                                         throw new org.apache.axis2.databinding.ADBException("exclusionDateList cannot be null!!");
                                    }
                                    elementList.add(localExclusionDateList);
                                } if (localLastModifiedDateTracker){
                                      elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "lastModifiedDate"));
                                 
                                        if (localLastModifiedDate != null){
                                            elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localLastModifiedDate));
                                        } else {
                                           throw new org.apache.axis2.databinding.ADBException("lastModifiedDate cannot be null!!");
                                        }
                                    } if (localOwnerTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "owner"));
                            
                            
                                    if (localOwner==null){
                                         throw new org.apache.axis2.databinding.ADBException("owner cannot be null!!");
                                    }
                                    elementList.add(localOwner);
                                } if (localAttendeeListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "attendeeList"));
                            
                            
                                    if (localAttendeeList==null){
                                         throw new org.apache.axis2.databinding.ADBException("attendeeList cannot be null!!");
                                    }
                                    elementList.add(localAttendeeList);
                                } if (localResourceListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "resourceList"));
                            
                            
                                    if (localResourceList==null){
                                         throw new org.apache.axis2.databinding.ADBException("resourceList cannot be null!!");
                                    }
                                    elementList.add(localResourceList);
                                } if (localTimeItemListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "timeItemList"));
                            
                            
                                    if (localTimeItemList==null){
                                         throw new org.apache.axis2.databinding.ADBException("timeItemList cannot be null!!");
                                    }
                                    elementList.add(localTimeItemList);
                                } if (localCustomFieldListTracker){
                            elementList.add(new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com",
                                                                      "customFieldList"));
                            
                            
                                    if (localCustomFieldList==null){
                                         throw new org.apache.axis2.databinding.ADBException("customFieldList cannot be null!!");
                                    }
                                    elementList.add(localCustomFieldList);
                                }
                            attribList.add(
                            new javax.xml.namespace.QName("","internalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInternalId));
                                
                            attribList.add(
                            new javax.xml.namespace.QName("","externalId"));
                            
                                      attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localExternalId));
                                

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static CalendarEvent parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            CalendarEvent object =
                new CalendarEvent();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"CalendarEvent".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (CalendarEvent)com.netsuite.webservices.platform.core_2017_2.types.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    // handle attribute "internalId"
                    java.lang.String tempAttribInternalId =
                        
                                reader.getAttributeValue(null,"internalId");
                            
                   if (tempAttribInternalId!=null){
                         java.lang.String content = tempAttribInternalId;
                        
                                                 object.setInternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribInternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("internalId");
                    
                    // handle attribute "externalId"
                    java.lang.String tempAttribExternalId =
                        
                                reader.getAttributeValue(null,"externalId");
                            
                   if (tempAttribExternalId!=null){
                         java.lang.String content = tempAttribExternalId;
                        
                                                 object.setExternalId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribExternalId));
                                            
                    } else {
                       
                    }
                    handledAttributes.add("externalId");
                    
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:core_2017_2.platform.webservices.netsuite.com","nullFieldList").equals(reader.getName())){
                                
                                                object.setNullFieldList(com.netsuite.webservices.platform.core_2017_2.NullField.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","company").equals(reader.getName())){
                                
                                                object.setCompany(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","contact").equals(reader.getName())){
                                
                                                object.setContact(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","supportCase").equals(reader.getName())){
                                
                                                object.setSupportCase(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","transaction").equals(reader.getName())){
                                
                                                object.setTransaction(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","period").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"period" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPeriod(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPeriod(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","frequency").equals(reader.getName())){
                                
                                                object.setFrequency(com.netsuite.webservices.platform.common_2017_2.types.RecurrenceFrequency.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","recurrenceDowMaskList").equals(reader.getName())){
                                
                                                object.setRecurrenceDowMaskList(com.netsuite.webservices.platform.common_2017_2.RecurrenceDowMaskList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","recurrenceDow").equals(reader.getName())){
                                
                                                object.setRecurrenceDow(com.netsuite.webservices.platform.common_2017_2.types.RecurrenceDow.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","recurrenceDowim").equals(reader.getName())){
                                
                                                object.setRecurrenceDowim(com.netsuite.webservices.platform.common_2017_2.types.RecurrenceDowim.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","seriesStartDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"seriesStartDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSeriesStartDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","endByDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"endByDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEndByDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","noEndDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"noEndDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setNoEndDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","sendEmail").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"sendEmail" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSendEmail(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","customForm").equals(reader.getName())){
                                
                                                object.setCustomForm(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","title").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"title" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTitle(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","recurrence").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"recurrence" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setRecurrence(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","location").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"location" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLocation(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","startDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"startDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStartDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","allDayEvent").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"allDayEvent" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAllDayEvent(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","timedEvent").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"timedEvent" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setTimedEvent(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","reminderType").equals(reader.getName())){
                                
                                                object.setReminderType(com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventReminderType.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","reminderMinutes").equals(reader.getName())){
                                
                                                object.setReminderMinutes(com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventReminderMinutes.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","status").equals(reader.getName())){
                                
                                                object.setStatus(com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventStatus.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","accessLevel").equals(reader.getName())){
                                
                                                object.setAccessLevel(com.netsuite.webservices.activities.scheduling_2017_2.types.CalendarEventAccessLevel.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","organizer").equals(reader.getName())){
                                
                                                object.setOrganizer(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","message").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"message" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMessage(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","createdDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"createdDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCreatedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","endDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"endDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setEndDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","exclusionDateList").equals(reader.getName())){
                                
                                                object.setExclusionDateList(com.netsuite.webservices.activities.scheduling_2017_2.ExclusionDateList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","lastModifiedDate").equals(reader.getName())){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"lastModifiedDate" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setLastModifiedDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDateTime(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","owner").equals(reader.getName())){
                                
                                                object.setOwner(com.netsuite.webservices.platform.core_2017_2.RecordRef.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","attendeeList").equals(reader.getName())){
                                
                                                object.setAttendeeList(com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventAttendeeList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","resourceList").equals(reader.getName())){
                                
                                                object.setResourceList(com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventResourceList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","timeItemList").equals(reader.getName())){
                                
                                                object.setTimeItemList(com.netsuite.webservices.activities.scheduling_2017_2.CalendarEventTimeItemList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("urn:scheduling_2017_2.activities.webservices.netsuite.com","customFieldList").equals(reader.getName())){
                                
                                                object.setCustomFieldList(com.netsuite.webservices.platform.core_2017_2.CustomFieldList.Factory.parse(reader));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    