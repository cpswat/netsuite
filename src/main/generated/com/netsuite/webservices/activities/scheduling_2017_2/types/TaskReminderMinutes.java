
/**
 * TaskReminderMinutes.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:34:40 IST)
 */

            
                package com.netsuite.webservices.activities.scheduling_2017_2.types;
            

            /**
            *  TaskReminderMinutes bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class TaskReminderMinutes
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "urn:types.scheduling_2017_2.activities.webservices.netsuite.com",
                "TaskReminderMinutes",
                "ns8");

            

                        /**
                        * field for TaskReminderMinutes
                        */

                        
                                    protected java.lang.String localTaskReminderMinutes ;
                                
                            private static java.util.HashMap _table_ = new java.util.HashMap();

                            // Constructor
                            
                                protected TaskReminderMinutes(java.lang.String value, boolean isRegisterValue) {
                                    localTaskReminderMinutes = value;
                                    if (isRegisterValue){
                                        
                                               _table_.put(localTaskReminderMinutes, this);
                                           
                                    }

                                }
                            
                                    public static final java.lang.String __0minutes =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_0minutes");
                                
                                    public static final java.lang.String __10minutes =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_10minutes");
                                
                                    public static final java.lang.String __12hours =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_12hours");
                                
                                    public static final java.lang.String __15minutes =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_15minutes");
                                
                                    public static final java.lang.String __1day =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_1day");
                                
                                    public static final java.lang.String __1hour =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_1hour");
                                
                                    public static final java.lang.String __1week =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_1week");
                                
                                    public static final java.lang.String __2days =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_2days");
                                
                                    public static final java.lang.String __2hours =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_2hours");
                                
                                    public static final java.lang.String __30minutes =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_30minutes");
                                
                                    public static final java.lang.String __3days =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_3days");
                                
                                    public static final java.lang.String __3hours =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_3hours");
                                
                                    public static final java.lang.String __4hours =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_4hours");
                                
                                    public static final java.lang.String __5hours =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_5hours");
                                
                                    public static final java.lang.String __5minutes =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_5minutes");
                                
                                    public static final java.lang.String __8hours =
                                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString("_8hours");
                                
                                public static final TaskReminderMinutes _0minutes =
                                    new TaskReminderMinutes(__0minutes,true);
                            
                                public static final TaskReminderMinutes _10minutes =
                                    new TaskReminderMinutes(__10minutes,true);
                            
                                public static final TaskReminderMinutes _12hours =
                                    new TaskReminderMinutes(__12hours,true);
                            
                                public static final TaskReminderMinutes _15minutes =
                                    new TaskReminderMinutes(__15minutes,true);
                            
                                public static final TaskReminderMinutes _1day =
                                    new TaskReminderMinutes(__1day,true);
                            
                                public static final TaskReminderMinutes _1hour =
                                    new TaskReminderMinutes(__1hour,true);
                            
                                public static final TaskReminderMinutes _1week =
                                    new TaskReminderMinutes(__1week,true);
                            
                                public static final TaskReminderMinutes _2days =
                                    new TaskReminderMinutes(__2days,true);
                            
                                public static final TaskReminderMinutes _2hours =
                                    new TaskReminderMinutes(__2hours,true);
                            
                                public static final TaskReminderMinutes _30minutes =
                                    new TaskReminderMinutes(__30minutes,true);
                            
                                public static final TaskReminderMinutes _3days =
                                    new TaskReminderMinutes(__3days,true);
                            
                                public static final TaskReminderMinutes _3hours =
                                    new TaskReminderMinutes(__3hours,true);
                            
                                public static final TaskReminderMinutes _4hours =
                                    new TaskReminderMinutes(__4hours,true);
                            
                                public static final TaskReminderMinutes _5hours =
                                    new TaskReminderMinutes(__5hours,true);
                            
                                public static final TaskReminderMinutes _5minutes =
                                    new TaskReminderMinutes(__5minutes,true);
                            
                                public static final TaskReminderMinutes _8hours =
                                    new TaskReminderMinutes(__8hours,true);
                            

                                public java.lang.String getValue() { return localTaskReminderMinutes;}

                                public boolean equals(java.lang.Object obj) {return (obj == this);}
                                public int hashCode() { return toString().hashCode();}
                                public java.lang.String toString() {
                                
                                        return localTaskReminderMinutes.toString();
                                    

                                }

                        

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME);
               return factory.createOMElement(dataSource,MY_QNAME);
            
        }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                
                //We can safely assume an element has only one type associated with it
                
                            java.lang.String namespace = parentQName.getNamespaceURI();
                            java.lang.String _localName = parentQName.getLocalPart();
                        
                            writeStartElement(null, namespace, _localName, xmlWriter);

                            // add the type details if this is used in a simple type
                               if (serializeType){
                                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"urn:types.scheduling_2017_2.activities.webservices.netsuite.com");
                                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           namespacePrefix+":TaskReminderMinutes",
                                           xmlWriter);
                                   } else {
                                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                                           "TaskReminderMinutes",
                                           xmlWriter);
                                   }
                               }
                            
                                          if (localTaskReminderMinutes==null){
                                            
                                                     throw new org.apache.axis2.databinding.ADBException("TaskReminderMinutes cannot be null !!");
                                                
                                         }else{
                                        
                                                       xmlWriter.writeCharacters(localTaskReminderMinutes);
                                            
                                         }
                                    
                            xmlWriter.writeEndElement();
                    

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("urn:types.scheduling_2017_2.activities.webservices.netsuite.com")){
                return "ns8";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(namespace, localPart);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (xmlWriter.getPrefix(namespace) == null) {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace,attName,attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                
                //We can safely assume an element has only one type associated with it
                 return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                            new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localTaskReminderMinutes)
                            },
                            null);

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        
                public static TaskReminderMinutes fromValue(java.lang.String value)
                      throws java.lang.IllegalArgumentException {
                    TaskReminderMinutes enumeration = (TaskReminderMinutes)
                       
                               _table_.get(value);
                           

                    if ((enumeration == null) && !((value == null) || (value.equals("")))) {
                        throw new java.lang.IllegalArgumentException();
                    }
                    return enumeration;
                }
                public static TaskReminderMinutes fromString(java.lang.String value,java.lang.String namespaceURI)
                      throws java.lang.IllegalArgumentException {
                    try {
                       
                                       return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));
                                   

                    } catch (java.lang.Exception e) {
                        throw new java.lang.IllegalArgumentException();
                    }
                }

                public static TaskReminderMinutes fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                                    java.lang.String content) {
                    if (content.indexOf(":") > -1){
                        java.lang.String prefix = content.substring(0,content.indexOf(":"));
                        java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                        return TaskReminderMinutes.Factory.fromString(content,namespaceUri);
                    } else {
                       return TaskReminderMinutes.Factory.fromString(content,"");
                    }
                }
            

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static TaskReminderMinutes parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            TaskReminderMinutes object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();
            

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                   
                while(!reader.isEndElement()) {
                    if (reader.isStartElement()  || reader.hasText()){
                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"TaskReminderMinutes" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                        if (content.indexOf(":") > 0) {
                                            // this seems to be a Qname so find the namespace and send
                                            prefix = content.substring(0, content.indexOf(":"));
                                            namespaceuri = reader.getNamespaceURI(prefix);
                                            object = TaskReminderMinutes.Factory.fromString(content,namespaceuri);
                                        } else {
                                            // this seems to be not a qname send and empty namespace incase of it is
                                            // check is done in fromString method
                                            object = TaskReminderMinutes.Factory.fromString(content,"");
                                        }
                                        
                                        
                             } else {
                                reader.next();
                             }  
                           }  // end of while loop
                        



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    