
/**
 * NetSuiteServiceMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
        package com.netsuite;

        /**
        *  NetSuiteServiceMessageReceiverInOut message receiver
        */

        public class NetSuiteServiceMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        NetSuiteServiceSkeleton skel = (NetSuiteServiceSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)){


        

            if("initialize".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.InitializeResponseE initializeResponse881 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.Initialize wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.Initialize)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.Initialize.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               initializeResponse881 =
                                                   
                                                   
                                                         skel.initialize(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), initializeResponse881, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "initialize"));
                                    } else 

            if("ssoLogin".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.SsoLoginResponseE ssoLoginResponse885 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.SsoLogin wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.SsoLogin)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.SsoLogin.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               ssoLoginResponse885 =
                                                   
                                                   
                                                         skel.ssoLogin(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), ssoLoginResponse885, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "ssoLogin"));
                                    } else 

            if("getItemAvailability".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.GetItemAvailabilityResponseE getItemAvailabilityResponse892 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.GetItemAvailability wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.GetItemAvailability)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.GetItemAvailability.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getItemAvailabilityResponse892 =
                                                   
                                                   
                                                         skel.getItemAvailability(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getItemAvailabilityResponse892, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "getItemAvailability"));
                                    } else 

            if("checkAsyncStatus".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatusResponse checkAsyncStatusResponse899 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatus wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatus)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatus.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               checkAsyncStatusResponse899 =
                                                   
                                                   
                                                         skel.checkAsyncStatus(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), checkAsyncStatusResponse899, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "checkAsyncStatus"));
                                    } else 

            if("searchMore".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.SearchMoreResponseE searchMoreResponse903 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.SearchMore wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.SearchMore)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.SearchMore.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               searchMoreResponse903 =
                                                   
                                                   
                                                         skel.searchMore(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), searchMoreResponse903, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "searchMore"));
                                    } else 

            if("getSelectValue".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.GetSelectValueResponseE getSelectValueResponse910 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.GetSelectValue wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.GetSelectValue)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.GetSelectValue.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getSelectValueResponse910 =
                                                   
                                                   
                                                         skel.getSelectValue(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getSelectValueResponse910, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "getSelectValue"));
                                    } else 

            if("detach".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.DetachResponseE detachResponse917 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.Detach wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.Detach)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.Detach.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               detachResponse917 =
                                                   
                                                   
                                                         skel.detach(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), detachResponse917, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "detach"));
                                    } else 

            if("asyncAddList".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.AsyncAddListResponse asyncAddListResponse924 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.AsyncAddList wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.AsyncAddList)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.AsyncAddList.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               asyncAddListResponse924 =
                                                   
                                                   
                                                         skel.asyncAddList(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), asyncAddListResponse924, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "asyncAddList"));
                                    } else 

            if("changeEmail".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.ChangeEmailResponseE changeEmailResponse929 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.ChangeEmail wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.ChangeEmail)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.ChangeEmail.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               changeEmailResponse929 =
                                                   
                                                   
                                                         skel.changeEmail(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), changeEmailResponse929, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "changeEmail"));
                                    } else 

            if("updateInviteeStatusList".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusListResponseE updateInviteeStatusListResponse936 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusList wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusList)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusList.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               updateInviteeStatusListResponse936 =
                                                   
                                                   
                                                         skel.updateInviteeStatusList(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), updateInviteeStatusListResponse936, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "updateInviteeStatusList"));
                                    } else 

            if("asyncDeleteList".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteListResponse asyncDeleteListResponse943 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteList wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteList)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteList.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               asyncDeleteListResponse943 =
                                                   
                                                   
                                                         skel.asyncDeleteList(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), asyncDeleteListResponse943, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "asyncDeleteList"));
                                    } else 

            if("getCustomizationId".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.GetCustomizationIdResponseE getCustomizationIdResponse950 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.GetCustomizationId wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.GetCustomizationId)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.GetCustomizationId.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getCustomizationIdResponse950 =
                                                   
                                                   
                                                         skel.getCustomizationId(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getCustomizationIdResponse950, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "getCustomizationId"));
                                    } else 

            if("getPostingTransactionSummary".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummaryResponseE getPostingTransactionSummaryResponse957 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummary wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummary)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummary.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getPostingTransactionSummaryResponse957 =
                                                   
                                                   
                                                         skel.getPostingTransactionSummary(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getPostingTransactionSummaryResponse957, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "getPostingTransactionSummary"));
                                    } else 

            if("upsert".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.UpsertResponseE upsertResponse964 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.Upsert wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.Upsert)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.Upsert.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               upsertResponse964 =
                                                   
                                                   
                                                         skel.upsert(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), upsertResponse964, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "upsert"));
                                    } else 

            if("changePassword".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.ChangePasswordResponseE changePasswordResponse969 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.ChangePassword wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.ChangePassword)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.ChangePassword.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               changePasswordResponse969 =
                                                   
                                                   
                                                         skel.changePassword(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), changePasswordResponse969, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "changePassword"));
                                    } else 

            if("getAll".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.GetAllResponseE getAllResponse976 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.GetAll wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.GetAll)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.GetAll.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getAllResponse976 =
                                                   
                                                   
                                                         skel.getAll(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getAllResponse976, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "getAll"));
                                    } else 

            if("asyncSearch".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.AsyncSearchResponse asyncSearchResponse983 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.AsyncSearch wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.AsyncSearch)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.AsyncSearch.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               asyncSearchResponse983 =
                                                   
                                                   
                                                         skel.asyncSearch(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), asyncSearchResponse983, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "asyncSearch"));
                                    } else 

            if("add".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.AddResponseE addResponse990 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.Add wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.Add)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.Add.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               addResponse990 =
                                                   
                                                   
                                                         skel.add(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), addResponse990, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "add"));
                                    } else 

            if("upsertList".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.UpsertListResponseE upsertListResponse997 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.UpsertList wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.UpsertList)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.UpsertList.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               upsertListResponse997 =
                                                   
                                                   
                                                         skel.upsertList(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), upsertListResponse997, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "upsertList"));
                                    } else 

            if("asyncInitializeList".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeListResponse asyncInitializeListResponse4 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeList wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeList)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeList.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               asyncInitializeListResponse4 =
                                                   
                                                   
                                                         skel.asyncInitializeList(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), asyncInitializeListResponse4, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "asyncInitializeList"));
                                    } else 

            if("getCurrencyRate".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRateResponseE getCurrencyRateResponse11 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRate wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRate)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRate.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getCurrencyRateResponse11 =
                                                   
                                                   
                                                         skel.getCurrencyRate(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getCurrencyRateResponse11, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "getCurrencyRate"));
                                    } else 

            if("attach".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.AttachResponseE attachResponse18 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.Attach wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.Attach)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.Attach.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               attachResponse18 =
                                                   
                                                   
                                                         skel.attach(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), attachResponse18, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "attach"));
                                    } else 

            if("searchMoreWithId".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithIdResponseE searchMoreWithIdResponse25 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithId wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithId)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithId.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               searchMoreWithIdResponse25 =
                                                   
                                                   
                                                         skel.searchMoreWithId(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), searchMoreWithIdResponse25, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "searchMoreWithId"));
                                    } else 

            if("addList".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.AddListResponseE addListResponse32 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.AddList wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.AddList)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.AddList.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               addListResponse32 =
                                                   
                                                   
                                                         skel.addList(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), addListResponse32, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "addList"));
                                    } else 

            if("mapSso".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.MapSsoResponseE mapSsoResponse36 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.MapSso wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.MapSso)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.MapSso.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               mapSsoResponse36 =
                                                   
                                                   
                                                         skel.mapSso(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), mapSsoResponse36, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "mapSso"));
                                    } else 

            if("searchNext".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.SearchNextResponseE searchNextResponse40 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.SearchNext wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.SearchNext)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.SearchNext.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               searchNextResponse40 =
                                                   
                                                   
                                                         skel.searchNext(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), searchNextResponse40, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "searchNext"));
                                    } else 

            if("updateList".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.UpdateListResponseE updateListResponse47 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.UpdateList wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.UpdateList)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.UpdateList.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               updateListResponse47 =
                                                   
                                                   
                                                         skel.updateList(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), updateListResponse47, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "updateList"));
                                    } else 

            if("updateInviteeStatus".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusResponseE updateInviteeStatusResponse54 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatus wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatus)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatus.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               updateInviteeStatusResponse54 =
                                                   
                                                   
                                                         skel.updateInviteeStatus(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), updateInviteeStatusResponse54, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "updateInviteeStatus"));
                                    } else 

            if("logout".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.LogoutResponseE logoutResponse57 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.Logout wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.Logout)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.Logout.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               logoutResponse57 =
                                                   
                                                   
                                                         skel.logout(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), logoutResponse57, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "logout"));
                                    } else 

            if("search".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.SearchResponseE searchResponse64 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.Search wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.Search)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.Search.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               searchResponse64 =
                                                   
                                                   
                                                         skel.search(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), searchResponse64, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "search"));
                                    } else 

            if("getAsyncResult".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.GetAsyncResultResponseE getAsyncResultResponse71 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.GetAsyncResult wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.GetAsyncResult)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.GetAsyncResult.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getAsyncResultResponse71 =
                                                   
                                                   
                                                         skel.getAsyncResult(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getAsyncResultResponse71, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "getAsyncResult"));
                                    } else 

            if("initializeList".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.InitializeListResponseE initializeListResponse78 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.InitializeList wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.InitializeList)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.InitializeList.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               initializeListResponse78 =
                                                   
                                                   
                                                         skel.initializeList(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), initializeListResponse78, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "initializeList"));
                                    } else 

            if("asyncUpsertList".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertListResponse asyncUpsertListResponse85 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertList wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertList)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertList.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               asyncUpsertListResponse85 =
                                                   
                                                   
                                                         skel.asyncUpsertList(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), asyncUpsertListResponse85, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "asyncUpsertList"));
                                    } else 

            if("get".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.GetResponseE getResponse92 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.Get wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.Get)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.Get.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getResponse92 =
                                                   
                                                   
                                                         skel.get(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getResponse92, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "get"));
                                    } else 

            if("getList".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.GetListResponseE getListResponse99 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.GetList wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.GetList)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.GetList.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getListResponse99 =
                                                   
                                                   
                                                         skel.getList(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getListResponse99, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "getList"));
                                    } else 

            if("getDeleted".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.GetDeletedResponseE getDeletedResponse106 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.GetDeleted wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.GetDeleted)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.GetDeleted.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getDeletedResponse106 =
                                                   
                                                   
                                                         skel.getDeleted(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getDeletedResponse106, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "getDeleted"));
                                    } else 

            if("update".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.UpdateResponseE updateResponse113 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.Update wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.Update)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.Update.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               updateResponse113 =
                                                   
                                                   
                                                         skel.update(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), updateResponse113, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "update"));
                                    } else 

            if("getSavedSearch".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.GetSavedSearchResponseE getSavedSearchResponse120 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.GetSavedSearch wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.GetSavedSearch)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.GetSavedSearch.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getSavedSearchResponse120 =
                                                   
                                                   
                                                         skel.getSavedSearch(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getSavedSearchResponse120, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "getSavedSearch"));
                                    } else 

            if("delete".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.DeleteResponseE deleteResponse127 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.Delete wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.Delete)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.Delete.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               deleteResponse127 =
                                                   
                                                   
                                                         skel.delete(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), deleteResponse127, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "delete"));
                                    } else 

            if("getServerTime".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.GetServerTimeResponseE getServerTimeResponse133 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.GetServerTime wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.GetServerTime)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.GetServerTime.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getServerTimeResponse133 =
                                                   
                                                   
                                                         skel.getServerTime(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getServerTimeResponse133, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "getServerTime"));
                                    } else 

            if("login".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.LoginResponseE loginResponse137 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.Login wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.Login)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.Login.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               loginResponse137 =
                                                   
                                                   
                                                         skel.login(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), loginResponse137, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "login"));
                                    } else 

            if("getDataCenterUrls".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrlsResponseE getDataCenterUrlsResponse144 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrls wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrls)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrls.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getDataCenterUrlsResponse144 =
                                                   
                                                   
                                                         skel.getDataCenterUrls(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getDataCenterUrlsResponse144, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "getDataCenterUrls"));
                                    } else 

            if("asyncGetList".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.AsyncGetListResponse asyncGetListResponse151 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.AsyncGetList wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.AsyncGetList)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.AsyncGetList.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               asyncGetListResponse151 =
                                                   
                                                   
                                                         skel.asyncGetList(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), asyncGetListResponse151, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "asyncGetList"));
                                    } else 

            if("deleteList".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.DeleteListResponseE deleteListResponse158 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.DeleteList wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.DeleteList)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.DeleteList.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               deleteListResponse158 =
                                                   
                                                   
                                                         skel.deleteList(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), deleteListResponse158, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "deleteList"));
                                    } else 

            if("asyncUpdateList".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateListResponse asyncUpdateListResponse165 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateList wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateList)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateList.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               asyncUpdateListResponse165 =
                                                   
                                                   
                                                         skel.asyncUpdateList(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), asyncUpdateListResponse165, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "asyncUpdateList"));
                                    } else 

            if("getBudgetExchangeRate".equals(methodName)){
                
                com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRateResponseE getBudgetExchangeRateResponse172 = null;
	                        com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRate wrappedParam =
                                                             (com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRate)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRate.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               getBudgetExchangeRateResponse172 =
                                                   
                                                   
                                                         skel.getBudgetExchangeRate(wrappedParam)
                                                    ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), getBudgetExchangeRateResponse172, false, new javax.xml.namespace.QName("urn:platform_2017_2.webservices.netsuite.com",
                                                    "getBudgetExchangeRate"));
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        } catch (InsufficientPermissionFault e) {

            msgContext.setProperty(org.apache.axis2.Constants.FAULT_NAME,"insufficientPermissionFault");
            org.apache.axis2.AxisFault f = createAxisFault(e);
            if (e.getFaultMessage() != null){
                f.setDetail(toOM(e.getFaultMessage(),false));
            }
            throw f;
            }
         catch (InvalidCredentialsFault e) {

            msgContext.setProperty(org.apache.axis2.Constants.FAULT_NAME,"invalidCredentialsFault");
            org.apache.axis2.AxisFault f = createAxisFault(e);
            if (e.getFaultMessage() != null){
                f.setDetail(toOM(e.getFaultMessage(),false));
            }
            throw f;
            }
         catch (InvalidAccountFault e) {

            msgContext.setProperty(org.apache.axis2.Constants.FAULT_NAME,"invalidAccountFault");
            org.apache.axis2.AxisFault f = createAxisFault(e);
            if (e.getFaultMessage() != null){
                f.setDetail(toOM(e.getFaultMessage(),false));
            }
            throw f;
            }
         catch (AsyncFault e) {

            msgContext.setProperty(org.apache.axis2.Constants.FAULT_NAME,"asyncFault");
            org.apache.axis2.AxisFault f = createAxisFault(e);
            if (e.getFaultMessage() != null){
                f.setDetail(toOM(e.getFaultMessage(),false));
            }
            throw f;
            }
         catch (ExceededConcurrentRequestLimitFault e) {

            msgContext.setProperty(org.apache.axis2.Constants.FAULT_NAME,"exceededConcurrentRequestLimitFault");
            org.apache.axis2.AxisFault f = createAxisFault(e);
            if (e.getFaultMessage() != null){
                f.setDetail(toOM(e.getFaultMessage(),false));
            }
            throw f;
            }
         catch (ExceededRecordCountFault e) {

            msgContext.setProperty(org.apache.axis2.Constants.FAULT_NAME,"exceededRecordCountFault");
            org.apache.axis2.AxisFault f = createAxisFault(e);
            if (e.getFaultMessage() != null){
                f.setDetail(toOM(e.getFaultMessage(),false));
            }
            throw f;
            }
         catch (InvalidVersionFault e) {

            msgContext.setProperty(org.apache.axis2.Constants.FAULT_NAME,"invalidVersionFault");
            org.apache.axis2.AxisFault f = createAxisFault(e);
            if (e.getFaultMessage() != null){
                f.setDetail(toOM(e.getFaultMessage(),false));
            }
            throw f;
            }
         catch (ExceededRequestSizeFault e) {

            msgContext.setProperty(org.apache.axis2.Constants.FAULT_NAME,"exceededRequestSizeFault");
            org.apache.axis2.AxisFault f = createAxisFault(e);
            if (e.getFaultMessage() != null){
                f.setDetail(toOM(e.getFaultMessage(),false));
            }
            throw f;
            }
         catch (ExceededRequestLimitFault e) {

            msgContext.setProperty(org.apache.axis2.Constants.FAULT_NAME,"exceededRequestLimitFault");
            org.apache.axis2.AxisFault f = createAxisFault(e);
            if (e.getFaultMessage() != null){
                f.setDetail(toOM(e.getFaultMessage(),false));
            }
            throw f;
            }
         catch (ExceededUsageLimitFault e) {

            msgContext.setProperty(org.apache.axis2.Constants.FAULT_NAME,"exceededUsageLimitFault");
            org.apache.axis2.AxisFault f = createAxisFault(e);
            if (e.getFaultMessage() != null){
                f.setDetail(toOM(e.getFaultMessage(),false));
            }
            throw f;
            }
         catch (UnexpectedErrorFault e) {

            msgContext.setProperty(org.apache.axis2.Constants.FAULT_NAME,"unexpectedErrorFault");
            org.apache.axis2.AxisFault f = createAxisFault(e);
            if (e.getFaultMessage() != null){
                f.setDetail(toOM(e.getFaultMessage(),false));
            }
            throw f;
            }
         catch (InvalidSessionFault e) {

            msgContext.setProperty(org.apache.axis2.Constants.FAULT_NAME,"invalidSessionFault");
            org.apache.axis2.AxisFault f = createAxisFault(e);
            if (e.getFaultMessage() != null){
                f.setDetail(toOM(e.getFaultMessage(),false));
            }
            throw f;
            }
        
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.Initialize param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.Initialize.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.InitializeResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.InitializeResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.Passport param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.Passport.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.TokenPassport param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.TokenPassport.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.PreferencesE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.PreferencesE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.SsoLogin param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.SsoLogin.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.SsoLoginResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.SsoLoginResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.faults_2017_2.InvalidAccountFaultE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.faults_2017_2.InvalidAccountFaultE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.faults_2017_2.InvalidVersionFaultE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.faults_2017_2.InvalidVersionFaultE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetItemAvailability param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetItemAvailability.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetItemAvailabilityResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetItemAvailabilityResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatus param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatus.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatusResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatusResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.faults_2017_2.AsyncFaultE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.faults_2017_2.AsyncFaultE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.SearchMore param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.SearchMore.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.SearchMoreResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.SearchMoreResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetSelectValue param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetSelectValue.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetSelectValueResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetSelectValueResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.Detach param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.Detach.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.DetachResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.DetachResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.AsyncAddList param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncAddList.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.AsyncAddListResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncAddListResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.ChangeEmail param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.ChangeEmail.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.ChangeEmailResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.ChangeEmailResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusList param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusList.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusListResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusListResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteList param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteList.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteListResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteListResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetCustomizationId param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetCustomizationId.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetCustomizationIdResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetCustomizationIdResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummary param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummary.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummaryResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummaryResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.Upsert param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.Upsert.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.UpsertResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.UpsertResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.ChangePassword param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.ChangePassword.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.ChangePasswordResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.ChangePasswordResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetAll param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetAll.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetAllResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetAllResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.AsyncSearch param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncSearch.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.AsyncSearchResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncSearchResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.Add param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.Add.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.AddResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AddResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.UpsertList param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.UpsertList.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.UpsertListResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.UpsertListResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeList param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeList.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeListResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeListResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRate param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRate.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRateResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRateResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.Attach param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.Attach.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.AttachResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AttachResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithId param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithId.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithIdResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithIdResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.AddList param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AddList.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.AddListResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AddListResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.MapSso param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.MapSso.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.MapSsoResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.MapSsoResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.SearchNext param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.SearchNext.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.SearchNextResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.SearchNextResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.UpdateList param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.UpdateList.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.UpdateListResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.UpdateListResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatus param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatus.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.Logout param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.Logout.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.LogoutResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.LogoutResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.Search param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.Search.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.SearchResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.SearchResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetAsyncResult param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetAsyncResult.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetAsyncResultResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetAsyncResultResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.InitializeList param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.InitializeList.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.InitializeListResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.InitializeListResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertList param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertList.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertListResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertListResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.Get param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.Get.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetList param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetList.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetListResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetListResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetDeleted param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetDeleted.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetDeletedResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetDeletedResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.Update param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.Update.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.UpdateResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.UpdateResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetSavedSearch param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetSavedSearch.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetSavedSearchResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetSavedSearchResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.Delete param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.Delete.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.DeleteResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.DeleteResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetServerTime param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetServerTime.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetServerTimeResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetServerTimeResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.Login param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.Login.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.LoginResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.LoginResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrls param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrls.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrlsResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrlsResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.AsyncGetList param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncGetList.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.AsyncGetListResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncGetListResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.DeleteList param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.DeleteList.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.DeleteListResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.DeleteListResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateList param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateList.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateListResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateListResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRate param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRate.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRateResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRateResponseE.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.InitializeResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.InitializeResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.InitializeResponseE wrapinitialize(){
                                com.netsuite.webservices.platform.messages_2017_2.InitializeResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.InitializeResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.SsoLoginResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.SsoLoginResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.SsoLoginResponseE wrapssoLogin(){
                                com.netsuite.webservices.platform.messages_2017_2.SsoLoginResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.SsoLoginResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.GetItemAvailabilityResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetItemAvailabilityResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.GetItemAvailabilityResponseE wrapgetItemAvailability(){
                                com.netsuite.webservices.platform.messages_2017_2.GetItemAvailabilityResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.GetItemAvailabilityResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatusResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatusResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatusResponse wrapcheckAsyncStatus(){
                                com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatusResponse wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatusResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.SearchMoreResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.SearchMoreResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.SearchMoreResponseE wrapsearchMore(){
                                com.netsuite.webservices.platform.messages_2017_2.SearchMoreResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.SearchMoreResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.GetSelectValueResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetSelectValueResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.GetSelectValueResponseE wrapgetSelectValue(){
                                com.netsuite.webservices.platform.messages_2017_2.GetSelectValueResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.GetSelectValueResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.DetachResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.DetachResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.DetachResponseE wrapdetach(){
                                com.netsuite.webservices.platform.messages_2017_2.DetachResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.DetachResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.AsyncAddListResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncAddListResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.AsyncAddListResponse wrapasyncAddList(){
                                com.netsuite.webservices.platform.messages_2017_2.AsyncAddListResponse wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.AsyncAddListResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.ChangeEmailResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.ChangeEmailResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.ChangeEmailResponseE wrapchangeEmail(){
                                com.netsuite.webservices.platform.messages_2017_2.ChangeEmailResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.ChangeEmailResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusListResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusListResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusListResponseE wrapupdateInviteeStatusList(){
                                com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusListResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusListResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteListResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteListResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteListResponse wrapasyncDeleteList(){
                                com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteListResponse wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteListResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.GetCustomizationIdResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetCustomizationIdResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.GetCustomizationIdResponseE wrapgetCustomizationId(){
                                com.netsuite.webservices.platform.messages_2017_2.GetCustomizationIdResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.GetCustomizationIdResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummaryResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummaryResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummaryResponseE wrapgetPostingTransactionSummary(){
                                com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummaryResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummaryResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.UpsertResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.UpsertResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.UpsertResponseE wrapupsert(){
                                com.netsuite.webservices.platform.messages_2017_2.UpsertResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.UpsertResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.ChangePasswordResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.ChangePasswordResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.ChangePasswordResponseE wrapchangePassword(){
                                com.netsuite.webservices.platform.messages_2017_2.ChangePasswordResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.ChangePasswordResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.GetAllResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetAllResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.GetAllResponseE wrapgetAll(){
                                com.netsuite.webservices.platform.messages_2017_2.GetAllResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.GetAllResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.AsyncSearchResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncSearchResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.AsyncSearchResponse wrapasyncSearch(){
                                com.netsuite.webservices.platform.messages_2017_2.AsyncSearchResponse wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.AsyncSearchResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.AddResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AddResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.AddResponseE wrapadd(){
                                com.netsuite.webservices.platform.messages_2017_2.AddResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.AddResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.UpsertListResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.UpsertListResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.UpsertListResponseE wrapupsertList(){
                                com.netsuite.webservices.platform.messages_2017_2.UpsertListResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.UpsertListResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeListResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeListResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeListResponse wrapasyncInitializeList(){
                                com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeListResponse wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeListResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRateResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRateResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRateResponseE wrapgetCurrencyRate(){
                                com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRateResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRateResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.AttachResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AttachResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.AttachResponseE wrapattach(){
                                com.netsuite.webservices.platform.messages_2017_2.AttachResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.AttachResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithIdResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithIdResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithIdResponseE wrapsearchMoreWithId(){
                                com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithIdResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithIdResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.AddListResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AddListResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.AddListResponseE wrapaddList(){
                                com.netsuite.webservices.platform.messages_2017_2.AddListResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.AddListResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.MapSsoResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.MapSsoResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.MapSsoResponseE wrapmapSso(){
                                com.netsuite.webservices.platform.messages_2017_2.MapSsoResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.MapSsoResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.SearchNextResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.SearchNextResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.SearchNextResponseE wrapsearchNext(){
                                com.netsuite.webservices.platform.messages_2017_2.SearchNextResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.SearchNextResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.UpdateListResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.UpdateListResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.UpdateListResponseE wrapupdateList(){
                                com.netsuite.webservices.platform.messages_2017_2.UpdateListResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.UpdateListResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusResponseE wrapupdateInviteeStatus(){
                                com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.LogoutResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.LogoutResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.LogoutResponseE wraplogout(){
                                com.netsuite.webservices.platform.messages_2017_2.LogoutResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.LogoutResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.SearchResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.SearchResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.SearchResponseE wrapsearch(){
                                com.netsuite.webservices.platform.messages_2017_2.SearchResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.SearchResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.GetAsyncResultResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetAsyncResultResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.GetAsyncResultResponseE wrapgetAsyncResult(){
                                com.netsuite.webservices.platform.messages_2017_2.GetAsyncResultResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.GetAsyncResultResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.InitializeListResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.InitializeListResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.InitializeListResponseE wrapinitializeList(){
                                com.netsuite.webservices.platform.messages_2017_2.InitializeListResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.InitializeListResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertListResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertListResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertListResponse wrapasyncUpsertList(){
                                com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertListResponse wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertListResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.GetResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.GetResponseE wrapget(){
                                com.netsuite.webservices.platform.messages_2017_2.GetResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.GetResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.GetListResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetListResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.GetListResponseE wrapgetList(){
                                com.netsuite.webservices.platform.messages_2017_2.GetListResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.GetListResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.GetDeletedResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetDeletedResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.GetDeletedResponseE wrapgetDeleted(){
                                com.netsuite.webservices.platform.messages_2017_2.GetDeletedResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.GetDeletedResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.UpdateResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.UpdateResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.UpdateResponseE wrapupdate(){
                                com.netsuite.webservices.platform.messages_2017_2.UpdateResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.UpdateResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.GetSavedSearchResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetSavedSearchResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.GetSavedSearchResponseE wrapgetSavedSearch(){
                                com.netsuite.webservices.platform.messages_2017_2.GetSavedSearchResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.GetSavedSearchResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.DeleteResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.DeleteResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.DeleteResponseE wrapdelete(){
                                com.netsuite.webservices.platform.messages_2017_2.DeleteResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.DeleteResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.GetServerTimeResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetServerTimeResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.GetServerTimeResponseE wrapgetServerTime(){
                                com.netsuite.webservices.platform.messages_2017_2.GetServerTimeResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.GetServerTimeResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.LoginResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.LoginResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.LoginResponseE wraplogin(){
                                com.netsuite.webservices.platform.messages_2017_2.LoginResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.LoginResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrlsResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrlsResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrlsResponseE wrapgetDataCenterUrls(){
                                com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrlsResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrlsResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.AsyncGetListResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncGetListResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.AsyncGetListResponse wrapasyncGetList(){
                                com.netsuite.webservices.platform.messages_2017_2.AsyncGetListResponse wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.AsyncGetListResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.DeleteListResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.DeleteListResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.DeleteListResponseE wrapdeleteList(){
                                com.netsuite.webservices.platform.messages_2017_2.DeleteListResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.DeleteListResponseE();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateListResponse param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateListResponse.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateListResponse wrapasyncUpdateList(){
                                com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateListResponse wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateListResponse();
                                return wrappedElement;
                         }
                    
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRateResponseE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRateResponseE.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    
                         private com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRateResponseE wrapgetBudgetExchangeRate(){
                                com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRateResponseE wrappedElement = new com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRateResponseE();
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (com.netsuite.webservices.platform.messages_2017_2.Initialize.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Initialize.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.InitializeResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.InitializeResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.SsoLogin.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.SsoLogin.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.SsoLoginResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.SsoLoginResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidAccountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidAccountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidVersionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidVersionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetItemAvailability.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetItemAvailability.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetItemAvailabilityResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetItemAvailabilityResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatus.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatus.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatusResponse.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatusResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.AsyncFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.AsyncFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.SearchMore.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.SearchMore.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.SearchMoreResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.SearchMoreResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetSelectValue.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetSelectValue.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetSelectValueResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetSelectValueResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Detach.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Detach.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DetachResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DetachResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.AsyncAddList.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.AsyncAddList.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.AsyncAddListResponse.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.AsyncAddListResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ChangeEmail.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ChangeEmail.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ChangeEmailResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ChangeEmailResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidAccountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidAccountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidVersionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidVersionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusList.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusList.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusListResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusListResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteList.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteList.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteListResponse.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteListResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetCustomizationId.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetCustomizationId.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetCustomizationIdResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetCustomizationIdResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummary.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummary.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummaryResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummaryResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Upsert.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Upsert.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.UpsertResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.UpsertResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ChangePassword.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ChangePassword.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ChangePasswordResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ChangePasswordResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidAccountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidAccountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidVersionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidVersionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetAll.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetAll.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetAllResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetAllResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.AsyncSearch.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.AsyncSearch.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.AsyncSearchResponse.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.AsyncSearchResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Add.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Add.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.AddResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.AddResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.UpsertList.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.UpsertList.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.UpsertListResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.UpsertListResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeList.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeList.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeListResponse.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeListResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRate.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRate.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRateResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRateResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Attach.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Attach.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.AttachResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.AttachResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithId.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithId.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithIdResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithIdResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.AddList.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.AddList.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.AddListResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.AddListResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.MapSso.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.MapSso.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.MapSsoResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.MapSsoResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidAccountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidAccountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidVersionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidVersionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.SearchNext.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.SearchNext.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.SearchNextResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.SearchNextResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.UpdateList.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.UpdateList.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.UpdateListResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.UpdateListResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatus.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatus.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Logout.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Logout.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.LogoutResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.LogoutResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Search.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Search.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.SearchResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.SearchResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetAsyncResult.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetAsyncResult.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetAsyncResultResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetAsyncResultResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.AsyncFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.AsyncFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.InitializeList.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.InitializeList.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.InitializeListResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.InitializeListResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertList.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertList.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertListResponse.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertListResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Get.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Get.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetList.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetList.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetListResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetListResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetDeleted.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetDeleted.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetDeletedResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetDeletedResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Update.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Update.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.UpdateResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.UpdateResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetSavedSearch.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetSavedSearch.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetSavedSearchResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetSavedSearchResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Delete.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Delete.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DeleteResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DeleteResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetServerTime.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetServerTime.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetServerTimeResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetServerTimeResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Login.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Login.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.LoginResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.LoginResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidAccountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidAccountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidVersionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidVersionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrls.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrls.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrlsResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrlsResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.AsyncGetList.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.AsyncGetList.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.AsyncGetListResponse.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.AsyncGetListResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DeleteList.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DeleteList.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DeleteListResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DeleteListResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateList.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateList.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateListResponse.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateListResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRate.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRate.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRateResponseE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRateResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InsufficientPermissionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidCredentialsFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededConcurrentRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRecordCountFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestSizeFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededRequestLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.ExceededUsageLimitFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.UnexpectedErrorFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.faults_2017_2.InvalidSessionFaultE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.Passport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.Passport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.TokenPassport.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.TokenPassport.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.PreferencesE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.PreferencesE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.class.equals(type)){
                
                           return com.netsuite.webservices.platform.messages_2017_2.DocumentInfoE.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    