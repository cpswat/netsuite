

/**
 * NetSuiteService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package com.netsuite;

    /*
     *  NetSuiteService java interface
     */

    public interface NetSuiteService {
          

        /**
          * Auto generated method signature
          * 
                    * @param initialize809
                
                    * @param passport810
                
                    * @param tokenPassport811
                
                    * @param applicationInfo812
                
                    * @param partnerInfo813
                
                    * @param preferences814
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.InitializeResponseE initialize(

                        com.netsuite.webservices.platform.messages_2017_2.Initialize initialize809,com.netsuite.webservices.platform.messages_2017_2.Passport passport810,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport811,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo812,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo813,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences814)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param initialize809
            
                * @param passport810
            
                * @param tokenPassport811
            
                * @param applicationInfo812
            
                * @param partnerInfo813
            
                * @param preferences814
            
          */
        public void startinitialize(

            com.netsuite.webservices.platform.messages_2017_2.Initialize initialize809,com.netsuite.webservices.platform.messages_2017_2.Passport passport810,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport811,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo812,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo813,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences814,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param ssoLogin816
                
                    * @param applicationInfo817
                
                    * @param partnerInfo818
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.InvalidAccountFault : 
             * @throws com.netsuite.InvalidVersionFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.SsoLoginResponseE ssoLogin(

                        com.netsuite.webservices.platform.messages_2017_2.SsoLogin ssoLogin816,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo817,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo818)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.InvalidAccountFault
          ,com.netsuite.InvalidVersionFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param ssoLogin816
            
                * @param applicationInfo817
            
                * @param partnerInfo818
            
          */
        public void startssoLogin(

            com.netsuite.webservices.platform.messages_2017_2.SsoLogin ssoLogin816,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo817,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo818,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getItemAvailability820
                
                    * @param passport821
                
                    * @param tokenPassport822
                
                    * @param applicationInfo823
                
                    * @param partnerInfo824
                
                    * @param preferences825
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.GetItemAvailabilityResponseE getItemAvailability(

                        com.netsuite.webservices.platform.messages_2017_2.GetItemAvailability getItemAvailability820,com.netsuite.webservices.platform.messages_2017_2.Passport passport821,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport822,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo823,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo824,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences825)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getItemAvailability820
            
                * @param passport821
            
                * @param tokenPassport822
            
                * @param applicationInfo823
            
                * @param partnerInfo824
            
                * @param preferences825
            
          */
        public void startgetItemAvailability(

            com.netsuite.webservices.platform.messages_2017_2.GetItemAvailability getItemAvailability820,com.netsuite.webservices.platform.messages_2017_2.Passport passport821,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport822,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo823,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo824,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences825,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param checkAsyncStatus827
                
                    * @param passport828
                
                    * @param tokenPassport829
                
                    * @param applicationInfo830
                
                    * @param partnerInfo831
                
                    * @param preferences832
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.AsyncFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatusResponse checkAsyncStatus(

                        com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatus checkAsyncStatus827,com.netsuite.webservices.platform.messages_2017_2.Passport passport828,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport829,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo830,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo831,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences832)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.AsyncFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param checkAsyncStatus827
            
                * @param passport828
            
                * @param tokenPassport829
            
                * @param applicationInfo830
            
                * @param partnerInfo831
            
                * @param preferences832
            
          */
        public void startcheckAsyncStatus(

            com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatus checkAsyncStatus827,com.netsuite.webservices.platform.messages_2017_2.Passport passport828,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport829,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo830,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo831,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences832,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param searchMore834
                
                    * @param applicationInfo835
                
                    * @param searchPreferences836
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.SearchMoreResponseE searchMore(

                        com.netsuite.webservices.platform.messages_2017_2.SearchMore searchMore834,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo835,com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE searchPreferences836)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param searchMore834
            
                * @param applicationInfo835
            
                * @param searchPreferences836
            
          */
        public void startsearchMore(

            com.netsuite.webservices.platform.messages_2017_2.SearchMore searchMore834,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo835,
                com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE searchPreferences836,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getSelectValue838
                
                    * @param passport839
                
                    * @param tokenPassport840
                
                    * @param applicationInfo841
                
                    * @param partnerInfo842
                
                    * @param preferences843
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.GetSelectValueResponseE getSelectValue(

                        com.netsuite.webservices.platform.messages_2017_2.GetSelectValue getSelectValue838,com.netsuite.webservices.platform.messages_2017_2.Passport passport839,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport840,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo841,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo842,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences843)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getSelectValue838
            
                * @param passport839
            
                * @param tokenPassport840
            
                * @param applicationInfo841
            
                * @param partnerInfo842
            
                * @param preferences843
            
          */
        public void startgetSelectValue(

            com.netsuite.webservices.platform.messages_2017_2.GetSelectValue getSelectValue838,com.netsuite.webservices.platform.messages_2017_2.Passport passport839,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport840,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo841,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo842,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences843,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param detach845
                
                    * @param passport846
                
                    * @param tokenPassport847
                
                    * @param applicationInfo848
                
                    * @param partnerInfo849
                
                    * @param preferences850
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.DetachResponseE detach(

                        com.netsuite.webservices.platform.messages_2017_2.Detach detach845,com.netsuite.webservices.platform.messages_2017_2.Passport passport846,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport847,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo848,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo849,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences850)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param detach845
            
                * @param passport846
            
                * @param tokenPassport847
            
                * @param applicationInfo848
            
                * @param partnerInfo849
            
                * @param preferences850
            
          */
        public void startdetach(

            com.netsuite.webservices.platform.messages_2017_2.Detach detach845,com.netsuite.webservices.platform.messages_2017_2.Passport passport846,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport847,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo848,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo849,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences850,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param asyncAddList852
                
                    * @param passport853
                
                    * @param tokenPassport854
                
                    * @param applicationInfo855
                
                    * @param partnerInfo856
                
                    * @param preferences857
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.AsyncAddListResponse asyncAddList(

                        com.netsuite.webservices.platform.messages_2017_2.AsyncAddList asyncAddList852,com.netsuite.webservices.platform.messages_2017_2.Passport passport853,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport854,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo855,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo856,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences857)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param asyncAddList852
            
                * @param passport853
            
                * @param tokenPassport854
            
                * @param applicationInfo855
            
                * @param partnerInfo856
            
                * @param preferences857
            
          */
        public void startasyncAddList(

            com.netsuite.webservices.platform.messages_2017_2.AsyncAddList asyncAddList852,com.netsuite.webservices.platform.messages_2017_2.Passport passport853,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport854,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo855,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo856,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences857,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param changeEmail859
                
                    * @param passport860
                
                    * @param applicationInfo861
                
                    * @param partnerInfo862
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.InvalidAccountFault : 
             * @throws com.netsuite.InvalidVersionFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.ChangeEmailResponseE changeEmail(

                        com.netsuite.webservices.platform.messages_2017_2.ChangeEmail changeEmail859,com.netsuite.webservices.platform.messages_2017_2.Passport passport860,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo861,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo862)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.InvalidAccountFault
          ,com.netsuite.InvalidVersionFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param changeEmail859
            
                * @param passport860
            
                * @param applicationInfo861
            
                * @param partnerInfo862
            
          */
        public void startchangeEmail(

            com.netsuite.webservices.platform.messages_2017_2.ChangeEmail changeEmail859,com.netsuite.webservices.platform.messages_2017_2.Passport passport860,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo861,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo862,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param updateInviteeStatusList864
                
                    * @param passport865
                
                    * @param tokenPassport866
                
                    * @param applicationInfo867
                
                    * @param partnerInfo868
                
                    * @param preferences869
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusListResponseE updateInviteeStatusList(

                        com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusList updateInviteeStatusList864,com.netsuite.webservices.platform.messages_2017_2.Passport passport865,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport866,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo867,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo868,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences869)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param updateInviteeStatusList864
            
                * @param passport865
            
                * @param tokenPassport866
            
                * @param applicationInfo867
            
                * @param partnerInfo868
            
                * @param preferences869
            
          */
        public void startupdateInviteeStatusList(

            com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusList updateInviteeStatusList864,com.netsuite.webservices.platform.messages_2017_2.Passport passport865,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport866,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo867,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo868,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences869,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param asyncDeleteList871
                
                    * @param passport872
                
                    * @param tokenPassport873
                
                    * @param applicationInfo874
                
                    * @param partnerInfo875
                
                    * @param preferences876
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteListResponse asyncDeleteList(

                        com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteList asyncDeleteList871,com.netsuite.webservices.platform.messages_2017_2.Passport passport872,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport873,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo874,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo875,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences876)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param asyncDeleteList871
            
                * @param passport872
            
                * @param tokenPassport873
            
                * @param applicationInfo874
            
                * @param partnerInfo875
            
                * @param preferences876
            
          */
        public void startasyncDeleteList(

            com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteList asyncDeleteList871,com.netsuite.webservices.platform.messages_2017_2.Passport passport872,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport873,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo874,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo875,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences876,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getCustomizationId878
                
                    * @param passport879
                
                    * @param tokenPassport880
                
                    * @param applicationInfo881
                
                    * @param partnerInfo882
                
                    * @param preferences883
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.GetCustomizationIdResponseE getCustomizationId(

                        com.netsuite.webservices.platform.messages_2017_2.GetCustomizationId getCustomizationId878,com.netsuite.webservices.platform.messages_2017_2.Passport passport879,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport880,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo881,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo882,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences883)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getCustomizationId878
            
                * @param passport879
            
                * @param tokenPassport880
            
                * @param applicationInfo881
            
                * @param partnerInfo882
            
                * @param preferences883
            
          */
        public void startgetCustomizationId(

            com.netsuite.webservices.platform.messages_2017_2.GetCustomizationId getCustomizationId878,com.netsuite.webservices.platform.messages_2017_2.Passport passport879,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport880,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo881,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo882,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences883,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getPostingTransactionSummary885
                
                    * @param passport886
                
                    * @param tokenPassport887
                
                    * @param applicationInfo888
                
                    * @param partnerInfo889
                
                    * @param preferences890
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummaryResponseE getPostingTransactionSummary(

                        com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummary getPostingTransactionSummary885,com.netsuite.webservices.platform.messages_2017_2.Passport passport886,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport887,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo888,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo889,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences890)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getPostingTransactionSummary885
            
                * @param passport886
            
                * @param tokenPassport887
            
                * @param applicationInfo888
            
                * @param partnerInfo889
            
                * @param preferences890
            
          */
        public void startgetPostingTransactionSummary(

            com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummary getPostingTransactionSummary885,com.netsuite.webservices.platform.messages_2017_2.Passport passport886,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport887,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo888,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo889,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences890,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param upsert892
                
                    * @param passport893
                
                    * @param tokenPassport894
                
                    * @param applicationInfo895
                
                    * @param partnerInfo896
                
                    * @param preferences897
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.UpsertResponseE upsert(

                        com.netsuite.webservices.platform.messages_2017_2.Upsert upsert892,com.netsuite.webservices.platform.messages_2017_2.Passport passport893,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport894,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo895,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo896,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences897)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param upsert892
            
                * @param passport893
            
                * @param tokenPassport894
            
                * @param applicationInfo895
            
                * @param partnerInfo896
            
                * @param preferences897
            
          */
        public void startupsert(

            com.netsuite.webservices.platform.messages_2017_2.Upsert upsert892,com.netsuite.webservices.platform.messages_2017_2.Passport passport893,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport894,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo895,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo896,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences897,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param changePassword899
                
                    * @param passport900
                
                    * @param applicationInfo901
                
                    * @param partnerInfo902
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.InvalidAccountFault : 
             * @throws com.netsuite.InvalidVersionFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.ChangePasswordResponseE changePassword(

                        com.netsuite.webservices.platform.messages_2017_2.ChangePassword changePassword899,com.netsuite.webservices.platform.messages_2017_2.Passport passport900,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo901,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo902)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.InvalidAccountFault
          ,com.netsuite.InvalidVersionFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param changePassword899
            
                * @param passport900
            
                * @param applicationInfo901
            
                * @param partnerInfo902
            
          */
        public void startchangePassword(

            com.netsuite.webservices.platform.messages_2017_2.ChangePassword changePassword899,com.netsuite.webservices.platform.messages_2017_2.Passport passport900,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo901,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo902,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getAll904
                
                    * @param passport905
                
                    * @param tokenPassport906
                
                    * @param applicationInfo907
                
                    * @param partnerInfo908
                
                    * @param preferences909
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.GetAllResponseE getAll(

                        com.netsuite.webservices.platform.messages_2017_2.GetAll getAll904,com.netsuite.webservices.platform.messages_2017_2.Passport passport905,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport906,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo907,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo908,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences909)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getAll904
            
                * @param passport905
            
                * @param tokenPassport906
            
                * @param applicationInfo907
            
                * @param partnerInfo908
            
                * @param preferences909
            
          */
        public void startgetAll(

            com.netsuite.webservices.platform.messages_2017_2.GetAll getAll904,com.netsuite.webservices.platform.messages_2017_2.Passport passport905,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport906,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo907,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo908,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences909,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param asyncSearch911
                
                    * @param passport912
                
                    * @param tokenPassport913
                
                    * @param applicationInfo914
                
                    * @param partnerInfo915
                
                    * @param searchPreferences916
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.AsyncSearchResponse asyncSearch(

                        com.netsuite.webservices.platform.messages_2017_2.AsyncSearch asyncSearch911,com.netsuite.webservices.platform.messages_2017_2.Passport passport912,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport913,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo914,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo915,com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE searchPreferences916)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param asyncSearch911
            
                * @param passport912
            
                * @param tokenPassport913
            
                * @param applicationInfo914
            
                * @param partnerInfo915
            
                * @param searchPreferences916
            
          */
        public void startasyncSearch(

            com.netsuite.webservices.platform.messages_2017_2.AsyncSearch asyncSearch911,com.netsuite.webservices.platform.messages_2017_2.Passport passport912,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport913,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo914,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo915,
                com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE searchPreferences916,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param add918
                
                    * @param passport919
                
                    * @param tokenPassport920
                
                    * @param applicationInfo921
                
                    * @param partnerInfo922
                
                    * @param preferences923
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.AddResponseE add(

                        com.netsuite.webservices.platform.messages_2017_2.Add add918,com.netsuite.webservices.platform.messages_2017_2.Passport passport919,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport920,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo921,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo922,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences923)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param add918
            
                * @param passport919
            
                * @param tokenPassport920
            
                * @param applicationInfo921
            
                * @param partnerInfo922
            
                * @param preferences923
            
          */
        public void startadd(

            com.netsuite.webservices.platform.messages_2017_2.Add add918,com.netsuite.webservices.platform.messages_2017_2.Passport passport919,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport920,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo921,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo922,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences923,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param upsertList925
                
                    * @param passport926
                
                    * @param tokenPassport927
                
                    * @param applicationInfo928
                
                    * @param partnerInfo929
                
                    * @param preferences930
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.UpsertListResponseE upsertList(

                        com.netsuite.webservices.platform.messages_2017_2.UpsertList upsertList925,com.netsuite.webservices.platform.messages_2017_2.Passport passport926,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport927,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo928,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo929,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences930)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param upsertList925
            
                * @param passport926
            
                * @param tokenPassport927
            
                * @param applicationInfo928
            
                * @param partnerInfo929
            
                * @param preferences930
            
          */
        public void startupsertList(

            com.netsuite.webservices.platform.messages_2017_2.UpsertList upsertList925,com.netsuite.webservices.platform.messages_2017_2.Passport passport926,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport927,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo928,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo929,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences930,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param asyncInitializeList932
                
                    * @param passport933
                
                    * @param tokenPassport934
                
                    * @param applicationInfo935
                
                    * @param partnerInfo936
                
                    * @param preferences937
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeListResponse asyncInitializeList(

                        com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeList asyncInitializeList932,com.netsuite.webservices.platform.messages_2017_2.Passport passport933,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport934,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo935,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo936,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences937)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param asyncInitializeList932
            
                * @param passport933
            
                * @param tokenPassport934
            
                * @param applicationInfo935
            
                * @param partnerInfo936
            
                * @param preferences937
            
          */
        public void startasyncInitializeList(

            com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeList asyncInitializeList932,com.netsuite.webservices.platform.messages_2017_2.Passport passport933,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport934,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo935,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo936,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences937,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getCurrencyRate939
                
                    * @param passport940
                
                    * @param tokenPassport941
                
                    * @param applicationInfo942
                
                    * @param partnerInfo943
                
                    * @param preferences944
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRateResponseE getCurrencyRate(

                        com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRate getCurrencyRate939,com.netsuite.webservices.platform.messages_2017_2.Passport passport940,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport941,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo942,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo943,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences944)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getCurrencyRate939
            
                * @param passport940
            
                * @param tokenPassport941
            
                * @param applicationInfo942
            
                * @param partnerInfo943
            
                * @param preferences944
            
          */
        public void startgetCurrencyRate(

            com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRate getCurrencyRate939,com.netsuite.webservices.platform.messages_2017_2.Passport passport940,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport941,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo942,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo943,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences944,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param attach946
                
                    * @param passport947
                
                    * @param tokenPassport948
                
                    * @param applicationInfo949
                
                    * @param partnerInfo950
                
                    * @param preferences951
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.AttachResponseE attach(

                        com.netsuite.webservices.platform.messages_2017_2.Attach attach946,com.netsuite.webservices.platform.messages_2017_2.Passport passport947,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport948,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo949,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo950,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences951)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param attach946
            
                * @param passport947
            
                * @param tokenPassport948
            
                * @param applicationInfo949
            
                * @param partnerInfo950
            
                * @param preferences951
            
          */
        public void startattach(

            com.netsuite.webservices.platform.messages_2017_2.Attach attach946,com.netsuite.webservices.platform.messages_2017_2.Passport passport947,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport948,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo949,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo950,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences951,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param searchMoreWithId953
                
                    * @param passport954
                
                    * @param tokenPassport955
                
                    * @param applicationInfo956
                
                    * @param partnerInfo957
                
                    * @param searchPreferences958
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithIdResponseE searchMoreWithId(

                        com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithId searchMoreWithId953,com.netsuite.webservices.platform.messages_2017_2.Passport passport954,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport955,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo956,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo957,com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE searchPreferences958)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param searchMoreWithId953
            
                * @param passport954
            
                * @param tokenPassport955
            
                * @param applicationInfo956
            
                * @param partnerInfo957
            
                * @param searchPreferences958
            
          */
        public void startsearchMoreWithId(

            com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithId searchMoreWithId953,com.netsuite.webservices.platform.messages_2017_2.Passport passport954,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport955,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo956,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo957,
                com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE searchPreferences958,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param addList960
                
                    * @param passport961
                
                    * @param tokenPassport962
                
                    * @param applicationInfo963
                
                    * @param partnerInfo964
                
                    * @param preferences965
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.AddListResponseE addList(

                        com.netsuite.webservices.platform.messages_2017_2.AddList addList960,com.netsuite.webservices.platform.messages_2017_2.Passport passport961,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport962,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo963,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo964,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences965)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param addList960
            
                * @param passport961
            
                * @param tokenPassport962
            
                * @param applicationInfo963
            
                * @param partnerInfo964
            
                * @param preferences965
            
          */
        public void startaddList(

            com.netsuite.webservices.platform.messages_2017_2.AddList addList960,com.netsuite.webservices.platform.messages_2017_2.Passport passport961,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport962,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo963,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo964,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences965,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param mapSso967
                
                    * @param applicationInfo968
                
                    * @param partnerInfo969
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.InvalidAccountFault : 
             * @throws com.netsuite.InvalidVersionFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.MapSsoResponseE mapSso(

                        com.netsuite.webservices.platform.messages_2017_2.MapSso mapSso967,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo968,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo969)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.InvalidAccountFault
          ,com.netsuite.InvalidVersionFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param mapSso967
            
                * @param applicationInfo968
            
                * @param partnerInfo969
            
          */
        public void startmapSso(

            com.netsuite.webservices.platform.messages_2017_2.MapSso mapSso967,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo968,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo969,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param searchNext971
                
                    * @param applicationInfo972
                
                    * @param searchPreferences973
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.SearchNextResponseE searchNext(

                        com.netsuite.webservices.platform.messages_2017_2.SearchNext searchNext971,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo972,com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE searchPreferences973)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param searchNext971
            
                * @param applicationInfo972
            
                * @param searchPreferences973
            
          */
        public void startsearchNext(

            com.netsuite.webservices.platform.messages_2017_2.SearchNext searchNext971,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo972,
                com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE searchPreferences973,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param updateList975
                
                    * @param passport976
                
                    * @param tokenPassport977
                
                    * @param applicationInfo978
                
                    * @param partnerInfo979
                
                    * @param preferences980
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.UpdateListResponseE updateList(

                        com.netsuite.webservices.platform.messages_2017_2.UpdateList updateList975,com.netsuite.webservices.platform.messages_2017_2.Passport passport976,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport977,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo978,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo979,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences980)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param updateList975
            
                * @param passport976
            
                * @param tokenPassport977
            
                * @param applicationInfo978
            
                * @param partnerInfo979
            
                * @param preferences980
            
          */
        public void startupdateList(

            com.netsuite.webservices.platform.messages_2017_2.UpdateList updateList975,com.netsuite.webservices.platform.messages_2017_2.Passport passport976,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport977,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo978,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo979,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences980,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param updateInviteeStatus982
                
                    * @param passport983
                
                    * @param tokenPassport984
                
                    * @param applicationInfo985
                
                    * @param partnerInfo986
                
                    * @param preferences987
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusResponseE updateInviteeStatus(

                        com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatus updateInviteeStatus982,com.netsuite.webservices.platform.messages_2017_2.Passport passport983,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport984,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo985,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo986,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences987)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param updateInviteeStatus982
            
                * @param passport983
            
                * @param tokenPassport984
            
                * @param applicationInfo985
            
                * @param partnerInfo986
            
                * @param preferences987
            
          */
        public void startupdateInviteeStatus(

            com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatus updateInviteeStatus982,com.netsuite.webservices.platform.messages_2017_2.Passport passport983,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport984,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo985,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo986,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences987,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param logout989
                
                    * @param applicationInfo990
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.LogoutResponseE logout(

                        com.netsuite.webservices.platform.messages_2017_2.Logout logout989,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo990)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param logout989
            
                * @param applicationInfo990
            
          */
        public void startlogout(

            com.netsuite.webservices.platform.messages_2017_2.Logout logout989,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo990,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param search992
                
                    * @param passport993
                
                    * @param tokenPassport994
                
                    * @param applicationInfo995
                
                    * @param partnerInfo996
                
                    * @param searchPreferences997
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.SearchResponseE search(

                        com.netsuite.webservices.platform.messages_2017_2.Search search992,com.netsuite.webservices.platform.messages_2017_2.Passport passport993,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport994,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo995,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo996,com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE searchPreferences997)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param search992
            
                * @param passport993
            
                * @param tokenPassport994
            
                * @param applicationInfo995
            
                * @param partnerInfo996
            
                * @param searchPreferences997
            
          */
        public void startsearch(

            com.netsuite.webservices.platform.messages_2017_2.Search search992,com.netsuite.webservices.platform.messages_2017_2.Passport passport993,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport994,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo995,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo996,
                com.netsuite.webservices.platform.messages_2017_2.SearchPreferencesE searchPreferences997,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getAsyncResult999
                
                    * @param passport0
                
                    * @param tokenPassport1
                
                    * @param applicationInfo2
                
                    * @param partnerInfo3
                
                    * @param preferences4
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.AsyncFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.GetAsyncResultResponseE getAsyncResult(

                        com.netsuite.webservices.platform.messages_2017_2.GetAsyncResult getAsyncResult999,com.netsuite.webservices.platform.messages_2017_2.Passport passport0,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport1,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo2,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo3,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences4)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.AsyncFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getAsyncResult999
            
                * @param passport0
            
                * @param tokenPassport1
            
                * @param applicationInfo2
            
                * @param partnerInfo3
            
                * @param preferences4
            
          */
        public void startgetAsyncResult(

            com.netsuite.webservices.platform.messages_2017_2.GetAsyncResult getAsyncResult999,com.netsuite.webservices.platform.messages_2017_2.Passport passport0,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport1,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo2,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo3,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences4,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param initializeList6
                
                    * @param passport7
                
                    * @param tokenPassport8
                
                    * @param applicationInfo9
                
                    * @param partnerInfo10
                
                    * @param preferences11
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.InitializeListResponseE initializeList(

                        com.netsuite.webservices.platform.messages_2017_2.InitializeList initializeList6,com.netsuite.webservices.platform.messages_2017_2.Passport passport7,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport8,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo9,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo10,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences11)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param initializeList6
            
                * @param passport7
            
                * @param tokenPassport8
            
                * @param applicationInfo9
            
                * @param partnerInfo10
            
                * @param preferences11
            
          */
        public void startinitializeList(

            com.netsuite.webservices.platform.messages_2017_2.InitializeList initializeList6,com.netsuite.webservices.platform.messages_2017_2.Passport passport7,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport8,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo9,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo10,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences11,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param asyncUpsertList13
                
                    * @param passport14
                
                    * @param tokenPassport15
                
                    * @param applicationInfo16
                
                    * @param partnerInfo17
                
                    * @param preferences18
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertListResponse asyncUpsertList(

                        com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertList asyncUpsertList13,com.netsuite.webservices.platform.messages_2017_2.Passport passport14,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport15,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo16,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo17,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences18)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param asyncUpsertList13
            
                * @param passport14
            
                * @param tokenPassport15
            
                * @param applicationInfo16
            
                * @param partnerInfo17
            
                * @param preferences18
            
          */
        public void startasyncUpsertList(

            com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertList asyncUpsertList13,com.netsuite.webservices.platform.messages_2017_2.Passport passport14,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport15,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo16,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo17,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences18,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param get20
                
                    * @param passport21
                
                    * @param tokenPassport22
                
                    * @param applicationInfo23
                
                    * @param partnerInfo24
                
                    * @param preferences25
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.GetResponseE get(

                        com.netsuite.webservices.platform.messages_2017_2.Get get20,com.netsuite.webservices.platform.messages_2017_2.Passport passport21,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport22,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo23,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo24,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences25)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param get20
            
                * @param passport21
            
                * @param tokenPassport22
            
                * @param applicationInfo23
            
                * @param partnerInfo24
            
                * @param preferences25
            
          */
        public void startget(

            com.netsuite.webservices.platform.messages_2017_2.Get get20,com.netsuite.webservices.platform.messages_2017_2.Passport passport21,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport22,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo23,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo24,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences25,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getList27
                
                    * @param passport28
                
                    * @param tokenPassport29
                
                    * @param applicationInfo30
                
                    * @param partnerInfo31
                
                    * @param preferences32
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.GetListResponseE getList(

                        com.netsuite.webservices.platform.messages_2017_2.GetList getList27,com.netsuite.webservices.platform.messages_2017_2.Passport passport28,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport29,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo30,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo31,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences32)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getList27
            
                * @param passport28
            
                * @param tokenPassport29
            
                * @param applicationInfo30
            
                * @param partnerInfo31
            
                * @param preferences32
            
          */
        public void startgetList(

            com.netsuite.webservices.platform.messages_2017_2.GetList getList27,com.netsuite.webservices.platform.messages_2017_2.Passport passport28,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport29,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo30,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo31,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences32,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getDeleted34
                
                    * @param passport35
                
                    * @param tokenPassport36
                
                    * @param applicationInfo37
                
                    * @param partnerInfo38
                
                    * @param preferences39
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.GetDeletedResponseE getDeleted(

                        com.netsuite.webservices.platform.messages_2017_2.GetDeleted getDeleted34,com.netsuite.webservices.platform.messages_2017_2.Passport passport35,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport36,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo37,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo38,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences39)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getDeleted34
            
                * @param passport35
            
                * @param tokenPassport36
            
                * @param applicationInfo37
            
                * @param partnerInfo38
            
                * @param preferences39
            
          */
        public void startgetDeleted(

            com.netsuite.webservices.platform.messages_2017_2.GetDeleted getDeleted34,com.netsuite.webservices.platform.messages_2017_2.Passport passport35,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport36,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo37,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo38,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences39,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param update41
                
                    * @param passport42
                
                    * @param tokenPassport43
                
                    * @param applicationInfo44
                
                    * @param partnerInfo45
                
                    * @param preferences46
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.UpdateResponseE update(

                        com.netsuite.webservices.platform.messages_2017_2.Update update41,com.netsuite.webservices.platform.messages_2017_2.Passport passport42,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport43,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo44,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo45,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences46)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param update41
            
                * @param passport42
            
                * @param tokenPassport43
            
                * @param applicationInfo44
            
                * @param partnerInfo45
            
                * @param preferences46
            
          */
        public void startupdate(

            com.netsuite.webservices.platform.messages_2017_2.Update update41,com.netsuite.webservices.platform.messages_2017_2.Passport passport42,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport43,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo44,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo45,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences46,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getSavedSearch48
                
                    * @param passport49
                
                    * @param tokenPassport50
                
                    * @param applicationInfo51
                
                    * @param partnerInfo52
                
                    * @param preferences53
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.GetSavedSearchResponseE getSavedSearch(

                        com.netsuite.webservices.platform.messages_2017_2.GetSavedSearch getSavedSearch48,com.netsuite.webservices.platform.messages_2017_2.Passport passport49,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport50,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo51,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo52,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences53)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getSavedSearch48
            
                * @param passport49
            
                * @param tokenPassport50
            
                * @param applicationInfo51
            
                * @param partnerInfo52
            
                * @param preferences53
            
          */
        public void startgetSavedSearch(

            com.netsuite.webservices.platform.messages_2017_2.GetSavedSearch getSavedSearch48,com.netsuite.webservices.platform.messages_2017_2.Passport passport49,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport50,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo51,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo52,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences53,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param delete55
                
                    * @param passport56
                
                    * @param tokenPassport57
                
                    * @param applicationInfo58
                
                    * @param partnerInfo59
                
                    * @param preferences60
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.DeleteResponseE delete(

                        com.netsuite.webservices.platform.messages_2017_2.Delete delete55,com.netsuite.webservices.platform.messages_2017_2.Passport passport56,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport57,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo58,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo59,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences60)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param delete55
            
                * @param passport56
            
                * @param tokenPassport57
            
                * @param applicationInfo58
            
                * @param partnerInfo59
            
                * @param preferences60
            
          */
        public void startdelete(

            com.netsuite.webservices.platform.messages_2017_2.Delete delete55,com.netsuite.webservices.platform.messages_2017_2.Passport passport56,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport57,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo58,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo59,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences60,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getServerTime62
                
                    * @param passport63
                
                    * @param tokenPassport64
                
                    * @param applicationInfo65
                
                    * @param partnerInfo66
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.GetServerTimeResponseE getServerTime(

                        com.netsuite.webservices.platform.messages_2017_2.GetServerTime getServerTime62,com.netsuite.webservices.platform.messages_2017_2.Passport passport63,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport64,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo65,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo66)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getServerTime62
            
                * @param passport63
            
                * @param tokenPassport64
            
                * @param applicationInfo65
            
                * @param partnerInfo66
            
          */
        public void startgetServerTime(

            com.netsuite.webservices.platform.messages_2017_2.GetServerTime getServerTime62,com.netsuite.webservices.platform.messages_2017_2.Passport passport63,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport64,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo65,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo66,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param login68
                
                    * @param applicationInfo69
                
                    * @param partnerInfo70
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.InvalidAccountFault : 
             * @throws com.netsuite.InvalidVersionFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.LoginResponseE login(

                        com.netsuite.webservices.platform.messages_2017_2.Login login68,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo69,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo70)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.InvalidAccountFault
          ,com.netsuite.InvalidVersionFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param login68
            
                * @param applicationInfo69
            
                * @param partnerInfo70
            
          */
        public void startlogin(

            com.netsuite.webservices.platform.messages_2017_2.Login login68,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo69,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo70,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getDataCenterUrls72
                
                    * @param passport73
                
                    * @param tokenPassport74
                
                    * @param applicationInfo75
                
                    * @param partnerInfo76
                
                    * @param preferences77
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrlsResponseE getDataCenterUrls(

                        com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrls getDataCenterUrls72,com.netsuite.webservices.platform.messages_2017_2.Passport passport73,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport74,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo75,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo76,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences77)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getDataCenterUrls72
            
                * @param passport73
            
                * @param tokenPassport74
            
                * @param applicationInfo75
            
                * @param partnerInfo76
            
                * @param preferences77
            
          */
        public void startgetDataCenterUrls(

            com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrls getDataCenterUrls72,com.netsuite.webservices.platform.messages_2017_2.Passport passport73,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport74,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo75,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo76,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences77,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param asyncGetList79
                
                    * @param passport80
                
                    * @param tokenPassport81
                
                    * @param applicationInfo82
                
                    * @param partnerInfo83
                
                    * @param preferences84
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.AsyncGetListResponse asyncGetList(

                        com.netsuite.webservices.platform.messages_2017_2.AsyncGetList asyncGetList79,com.netsuite.webservices.platform.messages_2017_2.Passport passport80,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport81,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo82,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo83,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences84)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param asyncGetList79
            
                * @param passport80
            
                * @param tokenPassport81
            
                * @param applicationInfo82
            
                * @param partnerInfo83
            
                * @param preferences84
            
          */
        public void startasyncGetList(

            com.netsuite.webservices.platform.messages_2017_2.AsyncGetList asyncGetList79,com.netsuite.webservices.platform.messages_2017_2.Passport passport80,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport81,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo82,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo83,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences84,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param deleteList86
                
                    * @param passport87
                
                    * @param tokenPassport88
                
                    * @param applicationInfo89
                
                    * @param partnerInfo90
                
                    * @param preferences91
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.DeleteListResponseE deleteList(

                        com.netsuite.webservices.platform.messages_2017_2.DeleteList deleteList86,com.netsuite.webservices.platform.messages_2017_2.Passport passport87,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport88,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo89,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo90,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences91)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param deleteList86
            
                * @param passport87
            
                * @param tokenPassport88
            
                * @param applicationInfo89
            
                * @param partnerInfo90
            
                * @param preferences91
            
          */
        public void startdeleteList(

            com.netsuite.webservices.platform.messages_2017_2.DeleteList deleteList86,com.netsuite.webservices.platform.messages_2017_2.Passport passport87,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport88,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo89,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo90,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences91,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param asyncUpdateList93
                
                    * @param passport94
                
                    * @param tokenPassport95
                
                    * @param applicationInfo96
                
                    * @param partnerInfo97
                
                    * @param preferences98
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateListResponse asyncUpdateList(

                        com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateList asyncUpdateList93,com.netsuite.webservices.platform.messages_2017_2.Passport passport94,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport95,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo96,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo97,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences98)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param asyncUpdateList93
            
                * @param passport94
            
                * @param tokenPassport95
            
                * @param applicationInfo96
            
                * @param partnerInfo97
            
                * @param preferences98
            
          */
        public void startasyncUpdateList(

            com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateList asyncUpdateList93,com.netsuite.webservices.platform.messages_2017_2.Passport passport94,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport95,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo96,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo97,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences98,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getBudgetExchangeRate100
                
                    * @param passport101
                
                    * @param tokenPassport102
                
                    * @param applicationInfo103
                
                    * @param partnerInfo104
                
                    * @param preferences105
                
             * @throws com.netsuite.InsufficientPermissionFault : 
             * @throws com.netsuite.InvalidCredentialsFault : 
             * @throws com.netsuite.ExceededConcurrentRequestLimitFault : 
             * @throws com.netsuite.ExceededRecordCountFault : 
             * @throws com.netsuite.ExceededRequestSizeFault : 
             * @throws com.netsuite.ExceededRequestLimitFault : 
             * @throws com.netsuite.ExceededUsageLimitFault : 
             * @throws com.netsuite.UnexpectedErrorFault : 
             * @throws com.netsuite.InvalidSessionFault : 
         */

         
                     public com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRateResponseE getBudgetExchangeRate(

                        com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRate getBudgetExchangeRate100,com.netsuite.webservices.platform.messages_2017_2.Passport passport101,com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport102,com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo103,com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo104,com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences105)
                        throws java.rmi.RemoteException
             
          ,com.netsuite.InsufficientPermissionFault
          ,com.netsuite.InvalidCredentialsFault
          ,com.netsuite.ExceededConcurrentRequestLimitFault
          ,com.netsuite.ExceededRecordCountFault
          ,com.netsuite.ExceededRequestSizeFault
          ,com.netsuite.ExceededRequestLimitFault
          ,com.netsuite.ExceededUsageLimitFault
          ,com.netsuite.UnexpectedErrorFault
          ,com.netsuite.InvalidSessionFault;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getBudgetExchangeRate100
            
                * @param passport101
            
                * @param tokenPassport102
            
                * @param applicationInfo103
            
                * @param partnerInfo104
            
                * @param preferences105
            
          */
        public void startgetBudgetExchangeRate(

            com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRate getBudgetExchangeRate100,com.netsuite.webservices.platform.messages_2017_2.Passport passport101,
                com.netsuite.webservices.platform.messages_2017_2.TokenPassport tokenPassport102,
                com.netsuite.webservices.platform.messages_2017_2.ApplicationInfoE applicationInfo103,
                com.netsuite.webservices.platform.messages_2017_2.PartnerInfoE partnerInfo104,
                com.netsuite.webservices.platform.messages_2017_2.PreferencesE preferences105,
                

            final com.netsuite.NetSuiteServiceCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        
       //
       }
    