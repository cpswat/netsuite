
/**
 * InvalidAccountFault.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

package com.netsuite;

public class InvalidAccountFault extends java.lang.Exception{

    private static final long serialVersionUID = 1527219035229L;
    
    private com.netsuite.webservices.platform.faults_2017_2.InvalidAccountFaultE faultMessage;

    
        public InvalidAccountFault() {
            super("InvalidAccountFault");
        }

        public InvalidAccountFault(java.lang.String s) {
           super(s);
        }

        public InvalidAccountFault(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public InvalidAccountFault(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(com.netsuite.webservices.platform.faults_2017_2.InvalidAccountFaultE msg){
       faultMessage = msg;
    }
    
    public com.netsuite.webservices.platform.faults_2017_2.InvalidAccountFaultE getFaultMessage(){
       return faultMessage;
    }
}
    