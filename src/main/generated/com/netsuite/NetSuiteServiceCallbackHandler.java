
/**
 * NetSuiteServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package com.netsuite;

    /**
     *  NetSuiteServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class NetSuiteServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public NetSuiteServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public NetSuiteServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for initialize method
            * override this method for handling normal response from initialize operation
            */
           public void receiveResultinitialize(
                    com.netsuite.webservices.platform.messages_2017_2.InitializeResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from initialize operation
           */
            public void receiveErrorinitialize(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for ssoLogin method
            * override this method for handling normal response from ssoLogin operation
            */
           public void receiveResultssoLogin(
                    com.netsuite.webservices.platform.messages_2017_2.SsoLoginResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from ssoLogin operation
           */
            public void receiveErrorssoLogin(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getItemAvailability method
            * override this method for handling normal response from getItemAvailability operation
            */
           public void receiveResultgetItemAvailability(
                    com.netsuite.webservices.platform.messages_2017_2.GetItemAvailabilityResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getItemAvailability operation
           */
            public void receiveErrorgetItemAvailability(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for checkAsyncStatus method
            * override this method for handling normal response from checkAsyncStatus operation
            */
           public void receiveResultcheckAsyncStatus(
                    com.netsuite.webservices.platform.messages_2017_2.CheckAsyncStatusResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from checkAsyncStatus operation
           */
            public void receiveErrorcheckAsyncStatus(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for searchMore method
            * override this method for handling normal response from searchMore operation
            */
           public void receiveResultsearchMore(
                    com.netsuite.webservices.platform.messages_2017_2.SearchMoreResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from searchMore operation
           */
            public void receiveErrorsearchMore(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSelectValue method
            * override this method for handling normal response from getSelectValue operation
            */
           public void receiveResultgetSelectValue(
                    com.netsuite.webservices.platform.messages_2017_2.GetSelectValueResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSelectValue operation
           */
            public void receiveErrorgetSelectValue(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for detach method
            * override this method for handling normal response from detach operation
            */
           public void receiveResultdetach(
                    com.netsuite.webservices.platform.messages_2017_2.DetachResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from detach operation
           */
            public void receiveErrordetach(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for asyncAddList method
            * override this method for handling normal response from asyncAddList operation
            */
           public void receiveResultasyncAddList(
                    com.netsuite.webservices.platform.messages_2017_2.AsyncAddListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from asyncAddList operation
           */
            public void receiveErrorasyncAddList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for changeEmail method
            * override this method for handling normal response from changeEmail operation
            */
           public void receiveResultchangeEmail(
                    com.netsuite.webservices.platform.messages_2017_2.ChangeEmailResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from changeEmail operation
           */
            public void receiveErrorchangeEmail(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateInviteeStatusList method
            * override this method for handling normal response from updateInviteeStatusList operation
            */
           public void receiveResultupdateInviteeStatusList(
                    com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusListResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateInviteeStatusList operation
           */
            public void receiveErrorupdateInviteeStatusList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for asyncDeleteList method
            * override this method for handling normal response from asyncDeleteList operation
            */
           public void receiveResultasyncDeleteList(
                    com.netsuite.webservices.platform.messages_2017_2.AsyncDeleteListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from asyncDeleteList operation
           */
            public void receiveErrorasyncDeleteList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCustomizationId method
            * override this method for handling normal response from getCustomizationId operation
            */
           public void receiveResultgetCustomizationId(
                    com.netsuite.webservices.platform.messages_2017_2.GetCustomizationIdResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCustomizationId operation
           */
            public void receiveErrorgetCustomizationId(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPostingTransactionSummary method
            * override this method for handling normal response from getPostingTransactionSummary operation
            */
           public void receiveResultgetPostingTransactionSummary(
                    com.netsuite.webservices.platform.messages_2017_2.GetPostingTransactionSummaryResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPostingTransactionSummary operation
           */
            public void receiveErrorgetPostingTransactionSummary(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for upsert method
            * override this method for handling normal response from upsert operation
            */
           public void receiveResultupsert(
                    com.netsuite.webservices.platform.messages_2017_2.UpsertResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from upsert operation
           */
            public void receiveErrorupsert(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for changePassword method
            * override this method for handling normal response from changePassword operation
            */
           public void receiveResultchangePassword(
                    com.netsuite.webservices.platform.messages_2017_2.ChangePasswordResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from changePassword operation
           */
            public void receiveErrorchangePassword(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAll method
            * override this method for handling normal response from getAll operation
            */
           public void receiveResultgetAll(
                    com.netsuite.webservices.platform.messages_2017_2.GetAllResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAll operation
           */
            public void receiveErrorgetAll(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for asyncSearch method
            * override this method for handling normal response from asyncSearch operation
            */
           public void receiveResultasyncSearch(
                    com.netsuite.webservices.platform.messages_2017_2.AsyncSearchResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from asyncSearch operation
           */
            public void receiveErrorasyncSearch(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for add method
            * override this method for handling normal response from add operation
            */
           public void receiveResultadd(
                    com.netsuite.webservices.platform.messages_2017_2.AddResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from add operation
           */
            public void receiveErroradd(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for upsertList method
            * override this method for handling normal response from upsertList operation
            */
           public void receiveResultupsertList(
                    com.netsuite.webservices.platform.messages_2017_2.UpsertListResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from upsertList operation
           */
            public void receiveErrorupsertList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for asyncInitializeList method
            * override this method for handling normal response from asyncInitializeList operation
            */
           public void receiveResultasyncInitializeList(
                    com.netsuite.webservices.platform.messages_2017_2.AsyncInitializeListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from asyncInitializeList operation
           */
            public void receiveErrorasyncInitializeList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrencyRate method
            * override this method for handling normal response from getCurrencyRate operation
            */
           public void receiveResultgetCurrencyRate(
                    com.netsuite.webservices.platform.messages_2017_2.GetCurrencyRateResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrencyRate operation
           */
            public void receiveErrorgetCurrencyRate(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for attach method
            * override this method for handling normal response from attach operation
            */
           public void receiveResultattach(
                    com.netsuite.webservices.platform.messages_2017_2.AttachResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from attach operation
           */
            public void receiveErrorattach(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for searchMoreWithId method
            * override this method for handling normal response from searchMoreWithId operation
            */
           public void receiveResultsearchMoreWithId(
                    com.netsuite.webservices.platform.messages_2017_2.SearchMoreWithIdResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from searchMoreWithId operation
           */
            public void receiveErrorsearchMoreWithId(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for addList method
            * override this method for handling normal response from addList operation
            */
           public void receiveResultaddList(
                    com.netsuite.webservices.platform.messages_2017_2.AddListResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from addList operation
           */
            public void receiveErroraddList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for mapSso method
            * override this method for handling normal response from mapSso operation
            */
           public void receiveResultmapSso(
                    com.netsuite.webservices.platform.messages_2017_2.MapSsoResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from mapSso operation
           */
            public void receiveErrormapSso(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for searchNext method
            * override this method for handling normal response from searchNext operation
            */
           public void receiveResultsearchNext(
                    com.netsuite.webservices.platform.messages_2017_2.SearchNextResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from searchNext operation
           */
            public void receiveErrorsearchNext(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateList method
            * override this method for handling normal response from updateList operation
            */
           public void receiveResultupdateList(
                    com.netsuite.webservices.platform.messages_2017_2.UpdateListResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateList operation
           */
            public void receiveErrorupdateList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for updateInviteeStatus method
            * override this method for handling normal response from updateInviteeStatus operation
            */
           public void receiveResultupdateInviteeStatus(
                    com.netsuite.webservices.platform.messages_2017_2.UpdateInviteeStatusResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from updateInviteeStatus operation
           */
            public void receiveErrorupdateInviteeStatus(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for logout method
            * override this method for handling normal response from logout operation
            */
           public void receiveResultlogout(
                    com.netsuite.webservices.platform.messages_2017_2.LogoutResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from logout operation
           */
            public void receiveErrorlogout(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for search method
            * override this method for handling normal response from search operation
            */
           public void receiveResultsearch(
                    com.netsuite.webservices.platform.messages_2017_2.SearchResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from search operation
           */
            public void receiveErrorsearch(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAsyncResult method
            * override this method for handling normal response from getAsyncResult operation
            */
           public void receiveResultgetAsyncResult(
                    com.netsuite.webservices.platform.messages_2017_2.GetAsyncResultResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAsyncResult operation
           */
            public void receiveErrorgetAsyncResult(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for initializeList method
            * override this method for handling normal response from initializeList operation
            */
           public void receiveResultinitializeList(
                    com.netsuite.webservices.platform.messages_2017_2.InitializeListResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from initializeList operation
           */
            public void receiveErrorinitializeList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for asyncUpsertList method
            * override this method for handling normal response from asyncUpsertList operation
            */
           public void receiveResultasyncUpsertList(
                    com.netsuite.webservices.platform.messages_2017_2.AsyncUpsertListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from asyncUpsertList operation
           */
            public void receiveErrorasyncUpsertList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for get method
            * override this method for handling normal response from get operation
            */
           public void receiveResultget(
                    com.netsuite.webservices.platform.messages_2017_2.GetResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from get operation
           */
            public void receiveErrorget(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getList method
            * override this method for handling normal response from getList operation
            */
           public void receiveResultgetList(
                    com.netsuite.webservices.platform.messages_2017_2.GetListResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getList operation
           */
            public void receiveErrorgetList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDeleted method
            * override this method for handling normal response from getDeleted operation
            */
           public void receiveResultgetDeleted(
                    com.netsuite.webservices.platform.messages_2017_2.GetDeletedResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDeleted operation
           */
            public void receiveErrorgetDeleted(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for update method
            * override this method for handling normal response from update operation
            */
           public void receiveResultupdate(
                    com.netsuite.webservices.platform.messages_2017_2.UpdateResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from update operation
           */
            public void receiveErrorupdate(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSavedSearch method
            * override this method for handling normal response from getSavedSearch operation
            */
           public void receiveResultgetSavedSearch(
                    com.netsuite.webservices.platform.messages_2017_2.GetSavedSearchResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSavedSearch operation
           */
            public void receiveErrorgetSavedSearch(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for delete method
            * override this method for handling normal response from delete operation
            */
           public void receiveResultdelete(
                    com.netsuite.webservices.platform.messages_2017_2.DeleteResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from delete operation
           */
            public void receiveErrordelete(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getServerTime method
            * override this method for handling normal response from getServerTime operation
            */
           public void receiveResultgetServerTime(
                    com.netsuite.webservices.platform.messages_2017_2.GetServerTimeResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getServerTime operation
           */
            public void receiveErrorgetServerTime(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for login method
            * override this method for handling normal response from login operation
            */
           public void receiveResultlogin(
                    com.netsuite.webservices.platform.messages_2017_2.LoginResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from login operation
           */
            public void receiveErrorlogin(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDataCenterUrls method
            * override this method for handling normal response from getDataCenterUrls operation
            */
           public void receiveResultgetDataCenterUrls(
                    com.netsuite.webservices.platform.messages_2017_2.GetDataCenterUrlsResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDataCenterUrls operation
           */
            public void receiveErrorgetDataCenterUrls(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for asyncGetList method
            * override this method for handling normal response from asyncGetList operation
            */
           public void receiveResultasyncGetList(
                    com.netsuite.webservices.platform.messages_2017_2.AsyncGetListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from asyncGetList operation
           */
            public void receiveErrorasyncGetList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for deleteList method
            * override this method for handling normal response from deleteList operation
            */
           public void receiveResultdeleteList(
                    com.netsuite.webservices.platform.messages_2017_2.DeleteListResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deleteList operation
           */
            public void receiveErrordeleteList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for asyncUpdateList method
            * override this method for handling normal response from asyncUpdateList operation
            */
           public void receiveResultasyncUpdateList(
                    com.netsuite.webservices.platform.messages_2017_2.AsyncUpdateListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from asyncUpdateList operation
           */
            public void receiveErrorasyncUpdateList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBudgetExchangeRate method
            * override this method for handling normal response from getBudgetExchangeRate operation
            */
           public void receiveResultgetBudgetExchangeRate(
                    com.netsuite.webservices.platform.messages_2017_2.GetBudgetExchangeRateResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBudgetExchangeRate operation
           */
            public void receiveErrorgetBudgetExchangeRate(java.lang.Exception e) {
            }
                


    }
    