
/**
 * InvalidVersionFault.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

package com.netsuite;

public class InvalidVersionFault extends java.lang.Exception{

    private static final long serialVersionUID = 1527219035248L;
    
    private com.netsuite.webservices.platform.faults_2017_2.InvalidVersionFaultE faultMessage;

    
        public InvalidVersionFault() {
            super("InvalidVersionFault");
        }

        public InvalidVersionFault(java.lang.String s) {
           super(s);
        }

        public InvalidVersionFault(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public InvalidVersionFault(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(com.netsuite.webservices.platform.faults_2017_2.InvalidVersionFaultE msg){
       faultMessage = msg;
    }
    
    public com.netsuite.webservices.platform.faults_2017_2.InvalidVersionFaultE getFaultMessage(){
       return faultMessage;
    }
}
    